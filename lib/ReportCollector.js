var CHARTS_LIB_NAME = 'Chart.min.js',
    CHARTS_LIB_PATH = './lib/' + CHARTS_LIB_NAME,
    METADATA_FILE_NAME = 'metadata.json',
    CSS_MAIN = 'main.css',
    CSS_TABLE = 'table.css',
    CSS_PATHS = ['./lib/css/' + CSS_MAIN, './lib/css/' + CSS_TABLE],
    TOP_LEVEL_REPORT_NAME = 'index.html';

var templates = require('./templates.js'),
    file = require("file"),
    fs = require('fs'),
    helpers = require('../test/helpers.js');

var ReportCollector = function(path) {
    var reportsPath = path + '/',
        reports = [],
        totals = {
        features: {
            name: '',
            total: 0,
            passed: 0,
            failed: 0
        },
        scenarios: {
            total: 0,
            passed: 0,
            failed: 0
        },
        steps: {
            total: 0,
            passed: 0,
            failed: 0
        }
    };

    var findReports = function() {
        file.walkSync(reportsPath, function(dirPath, dirs, files) {
            files.forEach(function(file) {
                if(file !== TOP_LEVEL_REPORT_NAME && file.indexOf('.html') > -1) {
                    var dirName = dirPath.replace() === reportsPath ? '' :
                                  dirPath.replace('\\', '/').split('/').slice(-1)[0] + '/';
                    reports.push({
                        dirPath: dirName, file: file, fileName: undefined
                    });
                }
            });
        });
    };

    var collectTotals = function(metadataObject){
        totals.features.total += metadataObject.totals.features.total;
        totals.features.passed += metadataObject.totals.features.passed;
        totals.features.failed += metadataObject.totals.features.failed;
        totals.scenarios.total += metadataObject.totals.scenarios.total;
        totals.scenarios.passed += metadataObject.totals.scenarios.passed;
        totals.scenarios.failed += metadataObject.totals.scenarios.failed;
        totals.steps.total += metadataObject.totals.steps.total;
        totals.steps.passed += metadataObject.totals.steps.passed;
        totals.steps.failed += metadataObject.totals.steps.failed;
    };

    var collectFileName = function(metadataObject, report){
        report.fileName = metadataObject.name;
    };

    var collectPassedOrFailed = function(metadataObject, report){
        report.passed = metadataObject.passed;
    };

    var collectBrowserName = function(metadataJsonObject, report) {
        report.browser = metadataJsonObject.browser;
    };

    var collectDataFromReportsMetadataFile = function() {
        reports.forEach(function(report){
            var metadataPath = file.path.abspath('./' + reportsPath + report.dirPath + METADATA_FILE_NAME);
            var dataJson = fs.readFileSync(metadataPath);
            var metadataJsonObject = JSON.parse(dataJson);
            collectTotals(metadataJsonObject);
            collectFileName(metadataJsonObject, report);
            collectPassedOrFailed(metadataJsonObject, report);
            collectBrowserName(metadataJsonObject, report);
        });
    };

    var getReportsTemplates = function () {
        return templates.reportUrl({reports: reports});
    };

    var copyChartsLibIntoDestFolder = function() {
        var targetChartsLibPath = reportsPath + CHARTS_LIB_NAME;
        helpers.copyFile(CHARTS_LIB_PATH, targetChartsLibPath);
    };

    var getFailedObject = function(value){
        return {
            value: value,
            color:"#e70a0f",
            highlight: "#FF5A5E",
            label: "Failed"
        }
    };

    var getPassedObject = function(value){
        return {
            value: value,
            color: "#009800",
            highlight: "#6dcc6c",
            label: "Passed"
        }
    };

    var getChartDataAsString = function() {
        return {
            steps: JSON.stringify([
                getFailedObject(totals.steps.failed),
                getPassedObject(totals.steps.passed)
            ]),
            features: JSON.stringify([
                getFailedObject(totals.features.failed),
                getPassedObject(totals.features.passed)
            ]),
            scenarios: JSON.stringify([
                getFailedObject(totals.scenarios.failed),
                getPassedObject(totals.scenarios.passed)
            ])

        };
    };

    var copyCssFiles = function() {
        CSS_PATHS.forEach(function(cssPath){
            var targetCssPath = reportsPath + cssPath.split('/').slice(-1);
            helpers.copyFile(cssPath, targetCssPath);
        });
    };

    findReports();

    this.collect = function() {
        collectDataFromReportsMetadataFile();
        copyChartsLibIntoDestFolder();
        copyCssFiles();

        var output = getReportsTemplates(),
            chartData = getChartDataAsString(),
            chart = templates.chart({chartData: chartData});

        fs.appendFileSync(reportsPath + TOP_LEVEL_REPORT_NAME,
            templates.reportTemplate({
                report: output,
                total: totals,
                chartsLibPath: CHARTS_LIB_NAME,
                chart: chart,
                styles: [CSS_MAIN, CSS_TABLE],
                calcPercentage: function(val, total){ return (val * 100 / total).toFixed(1); }
            }),
            {encoding: 'utf8'},
            function(err) {
                if(err) {
                    console.error('Error writing to file:' + reportsPath + TOP_LEVEL_REPORT_NAME);
                    throw err;
                }
            });
    }
};

module.exports = ReportCollector;

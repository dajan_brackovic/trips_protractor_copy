/*
* FIXME: this waiter is hack, to wait for last screenshot, delete it after resolving protractor bug
* https://github.com/angular/protractor/issues/1938
*
* */

var q = require('q');

var deferred = q.defer();

exports.resolve = function() {
    deferred.resolve.apply(deferred, arguments);
};

exports.teardown = function() {
    return deferred.promise;
};

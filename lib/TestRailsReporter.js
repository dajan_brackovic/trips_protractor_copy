var http = require('http');
var fs = require('fs')
var sprintf = require("sprintf").sprintf

var testRailUsername = require('../test/suites/features/foh/FareFinderSuite.js').conf.testRail

var TestRailsReporter = function () {

    this.updateTestSpec = function (specTestRailsId, result) {

        var post_data = sprintf("{ \"custom_test_case_status\" : %s }", result);


        // An object of options to indicate where to post to
        var post_options = {
            hostname: '10.11.19.145',
            port: '80',
            path: "/index.php?/api/v2/update_case/" + specTestRailsId,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic ' + new Buffer(testRailUsername.username + ':' + testRailUsername.password).toString('base64')
            }
        };

        // Set up the request
        var post_req = http.request(post_options, function(res) {
            console.log("in request");
            res.setEncoding('utf8');
            res.on('data', function (chunk) {
                console.log('Response: ' + chunk);
            });


        });

        // post the data
        post_req.write(post_data);
        console.log("requesting with " + post_options.path);
        console.log("requesting with " + post_options.hostname);
        console.log("requesting with " + post_options.headers.Authorization);
        console.log("requesting with " + post_data);
        post_req.end();


    }




};

module.exports = TestRailsReporter;
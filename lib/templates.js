var _ = require('lodash');

var header = false;

var templates = module.exports = {
    reportTemplate: function (params) {
            return _.template(
                '<html>' +
                '<head>' +
                    '<title>Ryanair Automation Report</title>' +
                    '<meta charset="utf-8">' +
                    '<% if (typeof chartsLibPath !== "undefined") { %>' +
                        '<script type="text/javascript" src="<%- chartsLibPath %>"></script>' +
                    '<% } %>' +
                    '<% if (typeof styles !== "undefined") { %>' +
                        '<% _.forEach(styles, function(css) { %>' +
                            '<link rel="stylesheet" href="<%- css %>"/>' +
                        '<% }); %>' +
                '<% } %>' +
                '</head>' +
                '<body>' +
                    '<div id="header">' +
                        'RYANAIR AUTOMATION RESULTS  ' +
                    '</div>' +
                    '<p id = "date" > Report Generated at: ' +  new Date() +  '<a id="rerun" href="run2-reports/index.html">Rerun Report</a>    <%= total.runNumber %> </p>'+
                '<table id="resultsTable">' +
                        '<thead>' +
                            '<tr><th></th><th>Total</th><th>Passed</th><th>Failed</th>' +
                                '<% if (typeof chart !== "undefined") { %>' +
                                    '<th>Success %</th>' +
                                    '<th>Failure %</th>' +
                                    '<th>Charts</th>' +
                                '<% } %>' +
                            '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                            '<tr>' +
                                '<td>Features</td><td id="feature_total"> <%= total.features.total %></td><td id="feature_passed"> <%= total.features.passed %> </td><td id="feature_failed"> <%= total.features.failed %> </td>' +
                                '<% if (typeof chart !== "undefined") { %>' +
                                    '<td><%= calcPercentage(total.features.passed, total.features.total) %></td>' +
                                    '<td><%= calcPercentage(total.features.failed, total.features.total) %></td>' +
                                    '<td><canvas id="features" height="100"></canvas></td>' +
                                '<% } %>' +
                            '</tr>' +
                            '<tr>' +
                                '<td>Scenarios</td><td id="scenario_total"> <%= total.scenarios.total %></td><td id="scenario_passed"> <%= total.scenarios.passed %> </td><td id="scenario_failed"><%= total.scenarios.failed %> </td>' +
                                '<% if (typeof chart !== "undefined") { %>' +
                                    '<td><%= calcPercentage(total.scenarios.passed, total.scenarios.total) %></td>' +
                                    '<td><%= calcPercentage(total.scenarios.failed, total.scenarios.total) %></td>' +
                                    '<td><canvas id="scenarios" height="100"></canvas></td>' +
                                '<% } %>' +
                            '</tr>' +
                            '<tr>' +
                                '<td>Steps</td><td id="steps_total"> <%= total.steps.total %></td><td id="steps_passed"> <%= total.steps.passed %> </td><td id="steps_failed"> <%= total.steps.failed %> </td>' +
                                '<% if (typeof chart !== "undefined") { %>' +
                                    '<td><%= calcPercentage(total.steps.passed, total.steps.total) %></td>' +
                                    '<td><%= calcPercentage(total.steps.failed, total.steps.total) %></td>' +
                                    '<td><canvas id="steps" height="100"></canvas></td>' +
                                '<% } %>' +
                            '</tr>' +
                        '</tbody>' +
                    '</table>' +
                    '<div id="results"> ' +
                        '<% if (typeof chart !== "undefined") { %>' +
                            '<%= chart %>' +
                        '<% } %>' +
                        '<%= report %> ' +
                    '</div>' +
                '</body>' +
                '</html>'
            )(params);
    },

    chart: function(params){
        return _.template(
            '<script>' +
                'var ctxFeatures = document.getElementById("features").getContext("2d");' +
                'var ctxScenarios = document.getElementById("scenarios").getContext("2d");' +
                'var ctxSteps = document.getElementById("steps").getContext("2d");' +
                'new Chart(ctxFeatures).Pie(<%= chartData.features %>, {});' +
                'new Chart(ctxScenarios).Pie(<%= chartData.scenarios %>, {});' +
                'new Chart(ctxSteps).Pie(<%= chartData.steps %>, {});' +
            '</script>'
        )(params);
    },

    reasonsTemplate: function (params) {
        return _.template(
            '<ul>' +
                '<% _.forEach(reasons, function(reason) { %>' +
                    '<li><%- reason.message %></li>' +
                '<% }); %>' +
            '</ul>'
        )(params);
    },

    reportUrl: function (params) {
        return _.template(
            '<table id="reportList" class="zui-table zui-table-vertical">' +
                '<thead>' +
                    '<tr>' +
                        '<th>Report</th>' +
                        '<th>Browser</th>' +
                        '<th>Status</th>' +
                    '</tr>' +
                '</thead>' +
                '<tbody>' +
                    '<% _.forEach(reports, function(report) { %>' +
                        '<tr>' +
                            '<td>' +
                                '<a href="<%- report.dirPath + report.file %>">' +
                                    '<%- report.fileName %>' +
                                '</a>' +
                            '</td>' +
                            '<td>' +
                                '<span>' +
                                    '<%- report.browser %>' +
                                '</span>' +
                            '</td>' +
                            '<td>' +
                                '<% if (report.passed == true) { %>' +
                                    '<span class="passed">&#10003;</span>' +
                                '<% } else {%>' +
                                    '<span class="failed">&#10007;</span>' +
                                '<% } %>' +
                            '</td>' +
                        '</tr>' +
                    '<% }); %>' +
                '</tbody>' +
            '</table>'
        )(params);
    },

    linkTemplate: function() {
        return _.template(
            '<li>' +
                '<%= mark %>' +
                '<a href="<%= filename %>"><%= name %></a> ' +
                '<%= reason %>' +
            '</li>' +
            '<% _.forEach(additionalMessages, function(msg) { %>' +
                '<li class="indent"><%- msg %></li>' +
            '<% }); %>' +
            '<li class="indent">Duration = (<%= duration %> s)</li>'
        );
    },

    nonLinkTemplate: function(){
        return _.template(
            '<li title="No screenshot was created for this test case.">' +
                '<%= mark %>' +
                '<%= name %> ' +
                '<%= reason %>' +
            '</li>' +
            '<% _.forEach(additionalMessages, function(msg) { %>' +
                '<li class="indent"><%- msg %></li>' +
            '<% }); %>' +
            '<li class="indent">Duration = (<%= duration %> s)</li>'
        );
    },

    getFailedPassed: function(params){
        return _.template(
            '<hr>' +
            '<b><%= title %> = </b>' +
            '<b>( ' +
                '</b><span class="success"><%= passed %></span> ' +
                '<b>:</b> <span class="failure"><%= failed %></span><b> ' +
            ')</b>'
        )(params);
    },

    getResults: function(params) {
        return _.template(
            '<ul style="list-style-type:none">' +
                '<hr>' +
                '<h4><%= suiteFullName %> (<%= duration %> s)</h4>'+
                '<%= output %>' +
                '<% if (failed == 0) { %>' +
                    '<span class="success"><b> SUCCESS</b></span>'+
                '<% } else { %>' +
                    '<span class="failure"><b> FAILURE</b></span>'+
                '<% } %>' +
            '</ul>'
        )(params);
    }

};


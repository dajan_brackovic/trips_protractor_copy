var DEFAULT_DESTINATION = 'test-reports/',
    METADATA_FILE_NAME = 'metadata.json',
    CSS_MAIN = 'main.css',
    CSS_TABLE = 'table.css',
    RERUN_NUMBER = '',
    CSS_PATHS = ['./lib/css/' + CSS_MAIN, './lib/css/' + CSS_TABLE];

var fs = require('fs'),
    mkdirp = require('mkdirp'),
    _ = require('lodash'),
    path = require('path'),
    hat = require('hat'),
    helpers = require('../test/helpers.js'),
    TestRailsReporter = require('./TestRailsReporter'),
    waitPlugin = require('./waitPlugin.js'),
    templates = require('./templates.js');

var reRuns = [];

var runTotals = {
    runNumber: RERUN_NUMBER,
    features: {
        name: '',
        total: 0,
        passed: 0,
        failed: 0,
        skipped: 0
    },
    scenarios: {
        total: 0,
        passed: 0,
        failed: 0,
        skipped: 0
    },
    steps: {
        total: 0,
        passed: 0,
        failed: 0,
        skipped: 0
    },
    foh: {
        total: 0,
        passed: 0,
        failed: 0
    },
    trips: {
        total: 0,
        passed: 0,
        failed: 0
    },
    myfr: {
        total: 0,
        passed: 0,
        failed: 0
    },
    skipped: {
        total: 0
    }


};

var testRailsReporter = new TestRailsReporter();

require('string.prototype.startswith');

function Jasmine2ScreenShotReporter(opts) {
    'use strict';

    var self = this,
        suites = {},   // suite clones
        specs = {},   // tes spec clones
        runningSuite = null, // currently running suite
        currentSpec = null, //reference to current spec
        metaData = {},
        numScreen = 0,

    // report marks
        marks = {
            pending: '<span class="pending">~</span>',
            failed: '<span class="failed">&#10007;</span>',
            passed: '<span class="passed">&#10003;</span>'
        },
    // when use use fit, jasmine never calls suiteStarted / suiteDone, so make a fake one to use
        fakeFocusedSuite = {
            id: 'focused',
            description: 'focused specs',
            fullName: 'focused specs'
        };

    // write data into opts.dest as filename
    var writeScreenshot = function (data, filename) {
        var stream = fs.createWriteStream(opts.dest + filename);
        stream.write(new Buffer(data, 'base64'));
        stream.end();
    };

    var writeMetadata = function () {
        try {
            var stream, metadataPath;
            if (metaData) {
                metadataPath = path.join(opts.dest, METADATA_FILE_NAME);
                mkdirp(path.dirname(metadataPath), function (err) {
                    if (err) {
                        throw new Error('Could not create directory for ' + metadataPath);
                    }
                    stream = fs.createWriteStream(metadataPath);
                    stream.write(JSON.stringify(metaData, null, '\t'));
                    stream.end();
                });
            }
        } catch (e) {
            console.error('Couldn\'t save metadata: ' + METADATA_FILE_NAME);
        }
    };

    // returns suite clone or creates one
    var getSuiteClone = function (suite) {
        suites[suite.id] = _.extend((suites[suite.id] || {}), suite);
        return suites[suite.id];
    };

    // returns spec clone or creates one
    var getSpecClone = function (spec) {
        specs[spec.id] = _.extend((specs[spec.id] || {}), spec);
        return specs[spec.id];
    };

    // returns duration in seconds
    var getDuration = function (obj) {
        if (!obj._started || !obj._finished) {
            return 0;
        }
        var duration = (obj._finished - obj._started) / 1000;
        return (duration < 1) ? duration : Math.round(duration);
    };

    var pathBuilder = function (spec, suites, capabilities) {
        return hat();
    };

    var isSpecValid = function (spec) {
        // Don't screenshot skipped specs
        var isSkipped = opts.ignoreSkippedSpecs && spec.status === 'pending';
        // Screenshot only for failed specs
        var isIgnored = opts.captureOnlyFailedSpecs && spec.status !== 'failed';

        return !isSkipped && !isIgnored;
    };

    var isSpecReportable = function (spec) {
        return (opts.reportOnlyFailedSpecs && spec.status === 'failed') || !opts.reportOnlyFailedSpecs;
    };

    var hasValidSpecs = function (suite) {
        var validSuites = false;
        var validSpecs = false;

        if (suite._suites.length) {
            validSuites = _.any(suite._suites, function (s) {
                return hasValidSpecs(s);
            });
        }

        if (suite._specs.length) {
            validSpecs = _.any(suite._specs, function (s) {
                return isSpecValid(s) || isSpecReportable(s);
            });
        }

        return validSuites || validSpecs;
    };

    var getDestination = function () {
        return (opts.dest || DEFAULT_DESTINATION) + '/';
    };

    var getDestinationWithUniqueDirectory = function () {
        return getDestination() + hat() + '/';
    };

    function isScreenshot(spec) {
        return spec.filename;
    }

    var getTemplate = function (spec) {
        return isScreenshot(spec) ? templates.linkTemplate() : templates.nonLinkTemplate();
    };

    var saveReport = function (output) {
        var total = {
            pending: 0,
            failed: 0,
            passed: 0
        };
        var featureName = '';
        _.each(suites, function (suite) {
            var run_result = printResults(suite);
            output += run_result['output'];

            if (run_result['output'] != '') {
                if (run_result['passed']) {
                    total['passed'] += 1;
                } else {
                    total['failed'] += 1;
                }
                featureName = run_result['name']
            }
        });

        var result = getFailedPassedString(total, featureName + ' total result');

        var splittedFeatureName = featureName.split('|')[1];
        if (splittedFeatureName !== undefined) {
            runTotals.features.name = splittedFeatureName.trim();
        } else {
            runTotals.features.name = 'N/A';
        }

        runTotals.features.total = runTotals.features.passed + runTotals.features.failed;
        runTotals.scenarios.total = runTotals.scenarios.passed + runTotals.scenarios.failed;


        // Ideally this shouldn't happen, but some versions of jasmine will allow it
        _.each(specs, function (spec) {
            var run_result = printSpec(spec);
            output += run_result['output'];
        });

        fs.appendFileSync(
            opts.dest + opts.filename,
            templates.reportTemplate({report: output, total: runTotals, styles: [CSS_MAIN, CSS_TABLE]}),
            {encoding: 'utf8'},
            function (err) {
                if (err) {
                    console.error('Error writing to file:' + opts.dest + opts.filename);
                    throw err;
                }
            }
        );
    };

    var setBrowserMetadata = function () {
        browser.getCapabilities().then(function (browserCapabilities) {
            metaData.browser = browserCapabilities.get('browserName');
        });
    };

    var setTotalsMetadata = function () {
        metaData.totals = runTotals;
    };

    var setFileNameMetadataIfNotSet = function (suite) {
        metaData.name = metaData.name || suite.fullName;
    };

    var setPassedMetadata = function () {
        metaData.passed = isPassed(runTotals.steps.failed);
    };

    var copyCss = function () {
        CSS_PATHS.forEach(function (cssPath) {
            var targetCssPath = opts.dest + cssPath.split('/').slice(-1);
            helpers.copyFile(cssPath, targetCssPath);
        });
    };

    opts = opts || {};
    opts.preserveDirectory = opts.preserveDirectory || true;
    opts.dest = opts.preserveDirectory ? getDestinationWithUniqueDirectory() : getDestination();
    opts.filename = opts.filename || 'shane.html';
    opts.ignoreSkippedSpecs = opts.ignoreSkippedSpecs || false;
    opts.reportOnlyFailedSpecs = opts.hasOwnProperty('reportOnlyFailedSpecs') ? opts.reportOnlyFailedSpecs : true;
    opts.captureOnlyFailedSpecs = opts.captureOnlyFailedSpecs || false;
    opts.pathBuilder = opts.pathBuilder || pathBuilder;
    RERUN_NUMBER = opts.runNumber

    this.addMessageToSpec = function (msg) {
        if (currentSpec != null) {
            currentSpec.additionalMessages ? currentSpec.additionalMessages.push(msg) : currentSpec.additionalMessages = [msg,];
        }
    };

    this.jasmineStarted = function () {
        console.log("jasmineStarted ");
        setBrowserMetadata();
        mkdirp(opts.dest, function (err) {
            var files;

            if (err) {
                throw new Error('Could not create directory ' + opts.dest);
            }

            files = fs.readdirSync(opts.dest);

            _.each(files, function (file) {
                var filepath = opts.dest + file;
                if (fs.statSync(filepath).isFile()) {
                    fs.unlinkSync(filepath);
                }
            });

            copyCss();
        });
    };

    this.suiteStarted = function (suite) {
        console.log("suiteStarted " + suite.description);
        suite = getSuiteClone(suite);
        suite._suites = [];
        suite._specs = [];
        suite._started = Date.now();
        suite._parent = runningSuite;
        setFileNameMetadataIfNotSet(suite);

        if (runningSuite) {
            runningSuite._suites.push(suite);
        }

        runningSuite = suite;
    };

    this.suiteDone = function (suite) {
        console.log("suiteDone " + suite.description);
        suite = getSuiteClone(suite);
        if (suite._parent === undefined) {
            // disabled suite (xdescribe) -- suiteStarted was never called
            self.suiteStarted(suite);
        }
        suite._finished = Date.now();
        runningSuite = suite._parent;

    };

    this.specStarted = function (spec) {
        //console.log("specStarted -> " + spec.fullName);
        if (!runningSuite) {
            // focused spec (fit) -- suiteStarted was never called
            self.suiteStarted(fakeFocusedSuite);
        }
        spec = currentSpec = getSpecClone(spec);
        spec._started = Date.now();
        spec._suite = runningSuite;
        runningSuite._specs.push(spec);
    };

    this.specDone = function (spec) {
        //console.log("specDone -> " + spec.fullName);
        //console.log("Spec done --->" + JSON.stringify(spec))

        runTotals.steps.total += 1;
        var file;
        spec = getSpecClone(spec);
        spec._finished = Date.now();

        if (!isSpecValid(spec)) {
            spec.skipPrinting = true;
            return;
        }

        if (spec.status === 'passed') {
            runTotals.steps.passed++;
        } else if (spec.status === 'failed') {
            runTotals.steps.failed++;
        }

        file = opts.pathBuilder(spec, suites);
        spec.filename = file + '.png';


        browser.takeScreenshot().then(function (png) {
            var screenshotPath;
            screenshotPath = path.join(opts.dest, spec.filename);
            mkdirp(path.dirname(screenshotPath), function (err) {
                if (err) {
                    throw new Error('Could not create directory for ' + screenshotPath);
                }
                writeScreenshot(png, spec.filename);
                // Dirty hack, please read waitPlugin.js docString
                numScreen += 1;
                if (numScreen >= Object.keys(specs).length) {
                    waitPlugin.resolve();
                }
                // end dirty hack
            });
        });
    };


    function parseSpecID(specDescription) {
        var regEx = new RegExp('(C[0-9]{1,6})', 'g')
        var result = specDescription.match(regEx);
        console.log("found failed spec --->> " + JSON.stringify(result) + " on " + specDescription);
        reRuns.push(result);
        console.log("after a fail specDescription " + specDescription + " | " + reRuns.toString());
        writeSpecsFileAppend(result);
    }

    function writeSpecsFileAppend(list) {
        console.log("writing list of " + list);
        var stream = fs.createWriteStream("rerun.txt", {flags: 'a'});
        stream.once('open', function (fd) {
            stream.write(list+"\n");
            stream.end();
        });

    }


    function getFailedPassedString(total, title) {
        runTotals.features.passed = total['passed'];
        runTotals.features.failed = total['failed'];
        return templates.getFailedPassed({
            title: title,
            passed: total['passed'],
            failed: total['failed']
        });
    }

    this.jasmineDone = function () {
        console.log("jasmineDone");
        var output = '';

        if (runningSuite) {
            // focused spec (fit) -- suiteDone was never called
            self.suiteDone(fakeFocusedSuite);
        }

        saveReport(output);
        setTotalsMetadata();
        setPassedMetadata();
        writeMetadata();

    };

    function printSpec(spec) {
        var suiteName = spec._suite ? spec._suite.fullName : '';
        var template = getTemplate(spec);

        if (spec.isPrinted || (spec.skipPrinting && !isSpecReportable(spec))) {
            return {output: ''};
        }

        spec.isPrinted = true;

        return {
            output: template({
                mark: marks[spec.status],
                name: spec.fullName.replace(suiteName, '').trim(),
                reason: printReasonsForFailure(spec),
                filename: encodeURIComponent(spec.filename),
                duration: getDuration(spec),
                additionalMessages: spec.additionalMessages
            }), status: spec.status
        };

    }

    this.getRerunList = function () {
        return reRuns;
    };

    function systemCmd(cmd) {
        var sys = require('sys')
        var exec = require('child_process').exec;

        console.log('running cmd:  ' + cmd)
        exec(cmd, function (error, stdout, stderr) {
            console.log('stdout: ' + stdout);
            console.log('stderr: ' + stderr);
            if (error !== null) {
                console.log('exec error: ' + error);
            }
        });
    }

    var isPassed = function (failedCound) {
        return failedCound == 0;
    };

    function printResults(suite) {
        var params = {
            suiteFullName: suite.fullName,
            duration: getDuration(suite),
            pending: 0,
            failed: 0,
            passed: 0,
            output: ''
        };

        if (suite.isPrinted || !hasValidSpecs(suite)) {
            return {output: ''};
        }

        if (suite._suites.length) {

            _.each(suite._suites, function (childSuite) {
           //     console.log("----> suite : " + JSON.stringify(childSuite));
                var run_result = printResults(childSuite);
                params.output += run_result['output'];
                if (run_result['passed']) {
                    params['passed'] += 1;
                    runTotals.scenarios.passed += 1;
                } else {
                    params['failed'] += 1;
                    runTotals.scenarios.failed += 1;
                    parseSpecID(suite.fullName);

                }
            });
            params.output += getFailedPassedString(params, 'Scenarios Results: ');

        } else {
            _.each(suite._specs, function (spec) {
                spec = specs[spec.id];
                var run_result = printSpec(spec);
                params.output += run_result['output'];
                params[run_result['status']] += 1
            });
            params.output += getFailedPassedString(params, 'Steps');
        }

        suite.isPrinted = true;

        return {output: templates.getResults(params), passed: isPassed(params['failed']), name: suite.fullName};
    }

    function printReasonsForFailure(spec) {
        if (spec.status !== 'failed') {
            return '';
        }
        return templates.reasonsTemplate({reasons: spec.failedExpectations});
    }

    return this;
}

module.exports = Jasmine2ScreenShotReporter;

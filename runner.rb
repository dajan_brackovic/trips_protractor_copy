system("clear")
# rm -fr ./test-reports/*; npm install; npm run postinstall; protractor test/protractor.conf.js --TripsSmokeSuite --firefox --uat

@vms17 = ['52', '53']
@vms18 = ['101', '102', '103', '104', '212', '213', '214', '215']

@suite = ARGV[0]
@browser = ARGV[1]
@env = ARGV[2]
@reruns = ARGV[3] || 1


@rerunSpecs = []

if ARGV.length < 3
  puts "The correct number of arguments have not been passed in"
  puts "Eg of correct format in order this script to work is:"
  puts "ruby runner.rb TripsSmokeSuite firefox uat"
  system exit
end


def run_cmd
  cmd = "rm -fr ./test-reports/*; npm install; protractor test/protractor.conf.js --#{@suite } --#{@browser} --#{@env} --1"
  puts "\n run cmd: #{cmd}"
  system cmd
end

def rerun_cmd
  puts "\n**********************************"
  puts "doing rerun of #{@rerunSpecs}"
  puts "amount to rerun #{@rerunSpecs.size}"
  puts "**********************************\n\n"

  cmd = "protractor test/protractor.conf.js --Rerun --#{@browser} --#{@env} --2"
  system cmd
end


def cleanBrowsers
  @vms17.each { |host|
    puts "\nkilling browsers on #{host}\n"
    system("curl -X GET http://10.11.17.#{host}:4567/cleanBrowsers;")
       system("curl -X GET http://10.11.17.#{host}:4567/cleanSessions;")
     }

     @vms18.each { |host|
       puts "\nkilling browsers on #{host}\n"
       system("curl -X GET http://10.11.18.#{host}:4567/cleanBrowsers;")
       system("curl -X GET http://10.11.18.#{host}:4567/cleanSessions;")
     }
end


def createSuiteFromTxtFile
  specs = []

  File.readlines('rerun.txt').each do |line|
    if (line.include? ',')
      linedSpecs = line.split(',')
      linedSpecs.each do
        specs << line.gsub("\n", "") + ".js"
      end
    end
    specs << line.gsub("\n", "") + ".js"
  end
  files = Dir["./test/specs/**/*.js"]
  files.each do |f|
    specs.each do |s|
      @rerunSpecs << f.gsub("/test", "") if f.include? s
    end
  end

  @rerunSpecs =@rerunSpecs.uniq

  # write rerun suite
  File.open('test/suites/Rerun.js', 'a') do |file|
    file.write("exports.conf = {\n");
    file.write("  suite: [\n");
    @rerunSpecs.each_with_index do |ff, index|
      if (index + 1) == @rerunSpecs.size
        file.write("      '" + ff + "'" + "\n");
      else
        file.write("      '" + ff + "'" + ",\n");
      end
    end
    file.write("\n ]};");

  end

end

#cleanBrowsers
system ("rm test/suites/Rerun.js")
system ("rm rerun.txt")
system ("rm -fr run1-reports/")
system ("rm -fr run2-reports/")

run_cmd
puts "first run finished......\n"
system("mkdir run1-reports")
system("mv test-reports/* run1-reports")

createSuiteFromTxtFile
#cleanBrowsers
system ("rm rerun.txt")

  @reruns.times do
      rerun_cmd
  end

system ("rm test/suites/Rerun.js")
createSuiteFromTxtFile

system("mkdir run2-reports")
system("mv test-reports/* run2-reports")
system("mv run1-reports/* test-reports/")
system("rm -rf run1-reports")
system("mkdir test-reports/run2-reports")
system("mv run2-reports/* test-reports/run2-reports")

puts "final fails are ::::::::>"
system("cat test/suites/Rerun.js")
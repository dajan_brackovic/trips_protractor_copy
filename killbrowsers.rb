system("clear")

@vms17 = ['52', '53']
@vms18 = ['101', '102', '103', '104', '212', '213', '214', '215']

def cleanBrowsers
  @vms17.each { |host|
    puts "\nkilling browsers on #{host}\n"
    system("curl -X GET http://10.11.17.#{host}:4567/cleanBrowsers;")
    system("curl -X GET http://10.11.17.#{host}:4567/cleanSessions;")
  }

  @vms18.each { |host|
    puts "\nkilling browsers on #{host}\n"
    system("curl -X GET http://10.11.18.#{host}:4567/cleanBrowsers;")
    system("curl -X GET http://10.11.18.#{host}:4567/cleanSessions;")
  }
end

cleanBrowsers

var file = require("file"),
    fs = require('fs');

var helpers = module.exports = {
    deleteFolderRecursive: function(path) {
        if(fs.existsSync(path)) {
            fs.readdirSync(path).forEach(function(file, index) {
                var curPath = path + "/" + file;
                if(fs.lstatSync(curPath).isDirectory()) {
                    helpers.deleteFolderRecursive(curPath);
                } else { // delete file
                    fs.unlinkSync(curPath);
                }
            });
            fs.rmdirSync(path);
        }
    },

    copyFile: function(source, target){
        var sourceAbs = file.path.abspath(source);
        fs.writeFileSync(target, fs.readFileSync(sourceAbs));
    },

    saveFile: function(path, content){
        fs.appendFileSync(path,
            content,
            {encoding: 'utf8'},
            function(err) {
                if(err) {
                    console.error('Error writing to file:' + path);
                    throw err;
                }
            });
    },

    buildStackTraces: function(protractorErrors){
        return protractorErrors.reduce(function(sum, el){
            return sum + '\n\n' + el.stack;
        });
    }
};






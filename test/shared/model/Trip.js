var BookingContact = require('./bookingContact/BookingContact.js');
var Journey = require('./journey/Journey.js');

this.journey;
this.bookingContact;

function Trip(paxMap, origin, destination, outDate, returnDate) {
    this.journey = new Journey(paxMap, origin, destination, outDate, returnDate);
    this.bookingContact = new BookingContact();
}

module.exports = Trip;


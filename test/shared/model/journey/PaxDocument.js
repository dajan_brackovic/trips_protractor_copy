
this.docNumber;
this.nationality;
this.dateOfBirth;
this.exiprtyDate;
this.dateOfIssue;
this.countryOfIssue;
this.paxType;


function PaxDocument(paxType) {
    this.paxType = paxType;


    this.setDetails = function () {
        this.docNumber = '564646465465';
        this.nationality = 'Irish';
        this.countryOfIssue = 'Ireland';
        this.exiprtyDate = "03/03/2020";
        this.dateOfIssue = "03/03/2014";

        if (this.paxType === 'ADT') {
            this.dateOfBirth = "03/03/1980";
        } else if (this.paxType === 'TEEN') {
            this.dateOfBirth =  "03/03/1998";
        } else if (this.paxType === 'CHD') {
            this.dateOfBirth =  "03/03/2010";
        } else if (this.paxType === 'INF') {
            this.dateOfBirth =  "03/03/2015";
        }
    }
}

module.exports = PaxDocument;

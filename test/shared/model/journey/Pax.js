var faker = require('faker');

var PaxDocument = require('./PaxDocument');
var Ssrs = require('./ssrs/Ssrs');

function Pax(paxType) {
    this.firstName;
    this.lastName;
    this.title;
    this.paxType = paxType;
    this.travelDocument;
    this.hasIfant;
    this.sSrs;


    this.setDetails = function () {
        this.firstName = faker.name.firstName();
        this.lastName = faker.name.lastName();
        this.title = this.getRandomTitle(paxType);
        var doc = new PaxDocument(paxType);
        doc.setDetails();
        this.travelDocument = doc;
        this.sSrs = new Ssrs();

    }

    this.getRandomTitle = function (paxType) {
        var adultTitles = [
            'Mr',
            'Ms',
            'Mrs'
        ];
        var nonAdultTitles = [
            'Mr',
            'Ms'
        ];

        if (paxType === 'ADT') {
            return adultTitles[Math.floor(Math.random() * adultTitles.length)];
        }
        else if (paxType === 'CHD') {
            return "Child";
        } else {
            return nonAdultTitles[Math.floor(Math.random() * nonAdultTitles.length)];
        }

    }
}

module.exports = Pax;


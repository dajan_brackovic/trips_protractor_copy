var PaxList = require('./PaxList.js');

this.departureTime;
this.arrivalTime;
this.flightNumber;
this.direction;
this.flightTime;
this.origin;
this.destination;
this.paxList;
this.outDate;
this.returnDate;

function Journey(paxMap, origin, destination, outDate, returnDate) {
    this.origin = origin;
    this.destination = destination;
    this.outDate = outDate;
    this.returnDate = returnDate;
    this.paxList = new PaxList(paxMap);
}

module.exports = Journey;

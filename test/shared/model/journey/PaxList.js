var Pax = require('./Pax.js');

function PaxList(paxMap) {
    var paxList = [];

    for (i = 0; i < paxMap.ADT; i++) {
        var p = new Pax('ADT');
        p.setDetails();
        paxList.push(p);
    }

    for (i = 0; i < paxMap.TEEN; i++) {
        var p = new Pax('TEEN');
        p.setDetails();
        paxList.push(p);
    }

    for (i = 0; i < paxMap.CHD; i++) {
        var p = new Pax('CHD');
        p.setDetails();
        paxList.push(p);
    }

    for (i = 0; i < paxMap.INF; i++) {
        var p = new Pax('INF');
        p.setDetails();
        paxList.push(p);
    }

    //for (var i = 0; i < paxList.length; i++) {
    //    var pax = (paxList[i]);
    //    console.log("FirstName : " + i + " " + pax.firstName);
    //    console.log("LastName : " + i + " " + pax.lastName);
    //    console.log("Type : " + i + " " + pax.paxType);
    //    console.log("DOB : " + i + " " + pax.travelDocument.dateOfBirth);
    //    console.log("DocNumber : " + i + " " + pax.travelDocument.docNumber);
    //    console.log("DocNationality : " + i + " " + pax.travelDocument.nationality);
    //
    //}

    return paxList;

}

module.exports = PaxList;

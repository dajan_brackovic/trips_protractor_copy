this.type;
this.price;
this.number;

function Seat(type) {

    this.premiumSeats = ['1A', '2A'];
    this.premium2Seats = ['11A', '12A'];
    this.standardSeats = ['21A', '22A', '21B', '22C', '21D', '22E'];


    this.type = type;
    switch (type) {
        case 'Premium':
            this.price = '9.00';
            this.number = this.premiumSeats[Math.floor(Math.random() * this.premiumSeats.length)];
            break;
        case 'Premium2':
            this.price = '15.00';
            this.number = this.premium2Seats[Math.floor(Math.random() * this.premium2Seats.length)];
            break;
        default:
            this.price = '5.00';
            this.number = this.standardSeats[Math.floor(Math.random() * this.standardSeats.length)];
    }


}

module.exports = Seat;

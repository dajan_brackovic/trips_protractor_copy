this.type;
this.price;

function Bag(type) {
    this.type = type;
    switch (type) {
        case '15kg':
            this.price = '15.00';
            break;
        case '25kg':
            this.price = '20.00';
            break;
        case '15kg15kg':
            this.price = '30.00';
            break;
        case '20kg20kg':
            this.price = '40.00';
            break;
        default:
            this.price = '15.00';
    }
}

module.exports = Bag;

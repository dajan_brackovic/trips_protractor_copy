var Faker = require('faker');
var PhoneNumber = require('./PhoneNumber');
var MasterCard = require('./MasterCard');
var Address = require('./Address');

this.phoneNumber;
this.firstName;
this.lastName;
this.email;
this.address;
this.card;

function BookingContact() {
    this.phoneNumber = new PhoneNumber();
    this.firstName = Faker.name.firstName();
    this.lastName = Faker.name.lastName();
    this.email = Faker.internet.email();
    this.address = new Address();
    this.card = new MasterCard('Successful');

}

module.exports = BookingContact;

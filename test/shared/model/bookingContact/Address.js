var Faker = require('faker');

this.addressLine1;
this.addressLine2;
this.city;
this.postCode;
this.country;


function Address() {
    this.addressLine1 = Faker.address.streetName();
    this.addressLine2 = Faker.address.streetAddress();
    this.city = Faker.address.city();
    this.postCode = Faker.address.zipCode();
    this.country = Faker.address.country();
}

module.exports = Address;


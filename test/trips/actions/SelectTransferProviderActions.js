var Pages = require('../../Pages')
var pages = new Pages();
var params = browser.params.conf;
var selectTransferProviderActions = function(){

    var selectTransferProviderPage = pages.selectTransferProviderPage;

    this.expectTitleAirportTransfers = function(textOfTitle){
        expect((selectTransferProviderPage.titleAirportTransfers()).getText()).toEqual(textOfTitle);
    };
    this.expectHeaderMessage = function(textOfMessage){
        expect((selectTransferProviderPage.airportTransfersHeaderMessage()).getText()).toEqual(textOfMessage);
    };
    this.expectOutboundCardTitle = function(textOfOutboundCardTitle){
        expect((selectTransferProviderPage.outboundCardTitle()).getText()).toEqual(textOfOutboundCardTitle);
    };
    this.expectInboundCardTitle = function(textOfInboundCardTitle){
        expect((selectTransferProviderPage.inboundCardTitle()).getText()).toEqual(textOfInboundCardTitle);
    };
    this.expectTitleOfFlight = function(index){
        expect((selectTransferProviderPage.titleOfFlight(index)).isPresent()).toBeTruthy();
    };
    this.expectCloseButtonX = function(){
        expect((selectTransferProviderPage.closeButtonX()).isPresent()).toBeTruthy();
    };
    this.clickCloseButtonX = function(){
        selectTransferProviderPage.closeButtonX().click();
    };
    this.expectCancelButton = function(index){
        expect((selectTransferProviderPage.cancelButton(index)).isPresent()).toBeTruthy();
    };
    this.clickCancelButton = function(index){
        selectTransferProviderPage.cancelButton(index).click();
    };

    this.clickBtnCancelTransfer = function () {
        selectTransferProviderPage.btnCancelTransfer().click();
    };

    this.expectConfirmButton = function(index){
        expect((selectTransferProviderPage.confirmButton(index)).isPresent()).toBeTruthy();
    };
    this.expectDisabledConfirmButton = function(index){
        expect((selectTransferProviderPage.confirmButton(index)).isEnabled()).not.toBeTruthy();
    };
    this.expectTotalPrice = function(){
        expect((selectTransferProviderPage.totalPriceText()).isPresent()).toBeTruthy();
        expect((selectTransferProviderPage.totalPriceAmount()).isPresent()).toBeTruthy();
    };
    this.clickSelectProviderDropDown = function(){
        selectTransferProviderPage.selectProviderDropDownPlus().click();
    };
    this.clickSelectProviderDropDownPlus1 = function(){
        selectTransferProviderPage.selectProviderDropDownPlus1().click();
    };
    this.clickSelectProviderDropDownDefault = function(){
      selectTransferProviderPage.selectProviderDropDownDefault().click();
    };
    this.clickSelectRouteRadioMarker = function(index){
        selectTransferProviderPage.selectRouteRadioMarker(index).click();
    };
    this.expectEnabledConfirmButton = function(index){
        expect((selectTransferProviderPage.confirmButton(index)).isEnabled()).toBeTruthy();
    };
    this.clickMinusButtonDecrement = function(index){
        //browser.executeScript('arguments[0].scrollIntoView()', selectTransferProviderPage.minusButtonDecrement().getWebElement());
        selectTransferProviderPage.minusButtonDecrement(index).click();
    };
    this.clickPlusButtonIncrement = function(index){
        selectTransferProviderPage.plusButtonIncrement(index).click();
    };
    this.expectTotalPriceUpdate = function(){
        expect(selectTransferProviderPage.totalPriceAmount().getText().then(function (text) {
            return text.substring(0);
        })).toMatch(params.regEx.price);
    };

    this.assertOnTransfersOnPriceBreakdown = function(){
        selectTransferProviderPage.confirmButton().click();
        actions.priceBreakDownActions.clickOnOpenPriceBreakDown();
        expect(pages.priceBreakDownPage.airportTransfersItem().isDisplayed()).toBeTruthy();
        pages.priceBreakDownPage.airportTransfersItem().getText().then(function(text){
            reporter.addMessageToSpec("Transfer Info: " + text);
        });
    };

    this.assertOnTransfersOnPaymentPage = function(){
        pages.tripsExtrasPage.btnExtrasContinue().click();
        expect(pages.priceBreakDownPage.airportTransfersItem().isDisplayed()).toBeTruthy();
        pages.priceBreakDownPage.airportTransfersItem().getText().then(function(text){
            reporter.addMessageToSpec("Transfer Info: " + text);
        });
    };

    this.clickRadioMarkerReturnText = function(index){
        //browser.executeScript('arguments[0].scrollIntoView()', selectTransferProviderPage.radioMarkerReturn().getWebElement());
        selectTransferProviderPage.radioMarkerReturnText(index).click();
    };
    this.expectTransferProviderText = function(index, message){
        expect((selectTransferProviderPage.transferProviderText(index)).isPresent()).toBeTruthy();
        expect((selectTransferProviderPage.transferProviderText(index)).getText()).toEqual(message);
    };
    this.expectTransferProviderDropDown = function(){
        expect((selectTransferProviderPage.transferProviderDropDown()).isPresent()).toBeTruthy();
    };
    this.expectTransferProviderName = function(index){
        expect((selectTransferProviderPage.transferProviderName(index)).isPresent()).toBeTruthy();
    };
    this.expectSelectRuteTypeSingleText = function(index, message){
        expect((selectTransferProviderPage.radioMarkerSingleText(index)).isPresent()).toBeTruthy();
        expect((selectTransferProviderPage.radioMarkerSingleText(index)).getText()).toEqual(message);
    };
    this.expectSelectRuteTypeReturnText = function(index, message){
        expect((selectTransferProviderPage.radioMarkerReturnText(index)).isPresent()).toBeTruthy();
        expect((selectTransferProviderPage.radioMarkerReturnText(index)).getText()).toEqual(message);
    };
    this.expectSelectRute = function(index){
        expect((selectTransferProviderPage.selectRouteRadioMarker(index)).isPresent()).toBeTruthy();
        expect((selectTransferProviderPage.selectRouteText(index)).isPresent()).toBeTruthy();
        expect((selectTransferProviderPage.selectRoutePrice(index)).isPresent()).toBeTruthy();
        expect((selectTransferProviderPage.selectRoutePerPerson(index)).isPresent()).toBeTruthy();
    };
    this.expectNumberOfPassengers  = function(index){
        expect((selectTransferProviderPage.numberOfPassengers(index)).isPresent()).toBeTruthy();
        expect((selectTransferProviderPage.minusButtonDecrement(index)).isPresent()).toBeTruthy();
        expect((selectTransferProviderPage.plusButtonDisabled(index)).isPresent()).toBeTruthy();
    };
    this.expectNoTransferProvided = function(message){
        expect((selectTransferProviderPage.noTransferProvided()).isPresent()).toBeTruthy();
        expect((selectTransferProviderPage.noTransferProvided()).getText()).toContain(message);
    };
    this.expectSelectProviderDropDownDefault = function(message){
        expect((selectTransferProviderPage.selectProviderDropDownDefault()).getText()).toEqual(message);
    };
    this.expectDownloadIcon = function(index){
        expect((selectTransferProviderPage.downloadIcon(index)).isPresent()).toBeTruthy();
    };
    this.expectDownloadLink = function(index, message){
        expect((selectTransferProviderPage.downloadLink(index)).isPresent()).toBeTruthy();
        expect((selectTransferProviderPage.downloadLink(index)).getText()).toEqual(message);
    };
    this.clickConfirmButton = function(index){
        selectTransferProviderPage.confirmButton(index).click();
    };
    this.expectCheckIfSelectRouteIsSelected = function(index){
        expect((selectTransferProviderPage.checkIfSelectRouteIsSelected(index)).isPresent()).toBeTruthy();
    };
    this.expectMinusButtonEnabled = function(index){
        expect((selectTransferProviderPage.minusButtonDecrement(index)).isEnabled()).toBeTruthy();
    };
    this.expectPlusButtonEnabled = function(index){
        expect((selectTransferProviderPage.plusButtonIncrement(index)).isEnabled()).toBeTruthy();
    };
    this.expectAmountOfPassengers = function(index, number){
        expect((selectTransferProviderPage.numberOfPassengers(index)).isEnabled()).toBeTruthy();
        expect((selectTransferProviderPage.numberOfPassengers(index)).getText()).toEqual(number);
    };
    this.expectAmountOfPassengers0 = function(index, number){
        expect((selectTransferProviderPage.numberOfPassengers0(index)).isEnabled()).toBeTruthy();
        expect((selectTransferProviderPage.numberOfPassengers0(index)).getText()).toEqual(number);
    };
    this.expectPlusButtonDisabled = function(index){
        expect((selectTransferProviderPage.plusButtonDisabled(index)).isEnabled()).toBeTruthy();
    };
    this.expectMinusButtonDisabled = function(index){
        expect((selectTransferProviderPage.minusButtonDisabled(index)).isEnabled()).toBeTruthy();
    };
    this.clickRowNumberFlex = function(index){
        selectTransferProviderPage.rowNumberFlex(index).click();
    };
    this.clickMinusButtonDecrement1 = function(index){
        selectTransferProviderPage.minusButtonDecrement1(index).click();
    };
    this.clickPlusButtonIncrement1 = function(index){
        selectTransferProviderPage.plusButtonIncrement1(index).click();
    };
    this.expectMaxNumberOfPassengersMessage = function(index, message){
        expect((selectTransferProviderPage.maxNumberOfPassengersMessage(index)).isPresent()).toBeTruthy();
        expect((selectTransferProviderPage.maxNumberOfPassengersMessage(index)).getText()).toEqual(message);
    };
    this.expectMaxNumberOfPassengersMessageMessageClose = function(index){
        expect((selectTransferProviderPage.maxNumberOfPassengersMessageClose(index)).isPresent()).toBeTruthy();
    };
    this.expectMaxNumberOfPassengersMessageTextBeforeIncrement = function(index, message){
        expect((selectTransferProviderPage.maxNumberOfPassengersMessage(index)).isPresent()).toBeTruthy();
        expect((selectTransferProviderPage.maxNumberOfPassengersMessageText(index)).getText()).toEqual(message);
    };

    this.expectGreenCheckIcon = function(index){
        expect((selectTransferProviderPage.greenCheckIcon(index)).isPresent()).toBeTruthy();
    };
    this.expectTransferFormMessageText = function(index){
        expect((selectTransferProviderPage.transferFormMessageText(index)).isPresent()).toBeTruthy();
    };
    this.expectTransferFormMessageAmount = function(index, number, rute){
        expect((selectTransferProviderPage.transferFormMessageAmount(index)).getText()).toContain(number);
        expect((selectTransferProviderPage.transferFormMessageAmount(index)).getText()).toContain(rute);
    };

};
module.exports = selectTransferProviderActions;

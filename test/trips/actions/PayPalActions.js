var Pages = require('../../Pages')
var pages = new Pages();

var PayPalActions = function() {
    var payPalPage = pages.payPalPage;
    var EC = protractor.ExpectedConditions;

    this.clickPayWithMyPayPalAccount = function () {
        var button = payPalPage.payWithMyPayPalAccountLink();
        var isClickable = EC.elementToBeClickable(button);
        browser.wait(isClickable, 5000); //wait for an element to become clickable
        button.click();
    };

    this.enterEmailAndPassword = function () {
        browser.switchTo().frame('injectedUl');
        payPalPage.fieldEmail().sendKeys("ryanair-test-customer@paypal.com");
        payPalPage.fieldPassword().sendKeys("12341234");
        payPalPage.btnLogIn().click();
        browser.switchTo().defaultContent();
    };

    this.clickBtnContinue = function () {
        browser.wait(EC.presenceOf(payPalPage.btnContinuePayPal()), 30000);
        payPalPage.btnContinuePayPal().click();
        browser.sleep(5000);
    };
};

module.exports = PayPalActions;
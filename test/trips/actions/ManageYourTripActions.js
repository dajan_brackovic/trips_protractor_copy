var Pages = require('../../Pages')
var pages = new Pages();
var PriceBreakDownActions = require('./PriceBreakDownActions.js');
var priceBreakDownActions = new PriceBreakDownActions();

var ManageYourTripActions = function () {
    var manageYourTripPage = pages.manageYourTripPage;
    var tripsHomePage = pages.tripsHomePage;

    // Change Your Flight Actions

    this.clickChangeYourFlightLink = function () {
        manageYourTripPage.changeYourFlightLink().click();
    };

    this.clickChangeFlightsSearchButton = function () {
        manageYourTripPage.changeFlightsSearchButton().click();
    };

    this.selectAFlight = function (indexStandard, indexBusiness, type, wayType) {
        element.all(by.xpath("//*[@type='outbound'] //*[@class='error-info']")).count().then(function (countOutError) {
            element.all(by.xpath("//*[@type='inbound'] //*[@class='error-info']")).count().then(function (countInError) {
                if (type === "standard") {
                    if (wayType === "twoway") {
                        tripsHomePage.cardPriceStandartFare("outbound", indexStandard).click();
                        tripsHomePage.cardPriceStandartFare("inbound", indexStandard).click();
                    }
                    else {
                        tripsHomePage.cardPriceStandartFare("outbound", indexStandard).click();
                    }
                }
                else {
                    if (wayType === "twoway") {
                        tripsHomePage.cardPriceBusinessFare("outbound", indexBusiness).click();
                        tripsHomePage.cardPriceBusinessFare("inbound", indexBusiness).click();
                    }
                    else {
                        tripsHomePage.cardPriceBusinessFare("outbound", indexBusiness).click();
                    }
                }
               // tripsHomePage.btnHomeBottomContinue().click();
            });
        });
    }

    this.clickChangeFlightOutBoundCheckBox = function () {
        manageYourTripPage.changeFlightOutBoundCheckBox().click();
    };

    this.clickChangeFlightInBoundCheckBox = function () {
        manageYourTripPage.changeFlightInBoundCheckBox().click();
    };


    // Change Name Actions

    this.clickChangeNameLink = function () {
        browser.sleep(1000);
        manageYourTripPage.changeNameLink().click();
    };

    this.clickSelectNameToChangeCheckBox = function () {
        browser.sleep(1000);
        manageYourTripPage.selectNameToChangeCheckBox().click();
    };

    this.selectChangeNameTitleDropdown = function (index) {
        manageYourTripPage.changeNameTitleDropdown(index).click();
    };

    this.enterChangeNameFirstNameField = function (ChangeFirstName) {
        manageYourTripPage.changeNameFirstNameField().clear();
        manageYourTripPage.changeNameFirstNameField().sendKeys(ChangeFirstName);
    };

    this.enterChangeNameLastNameField = function (ChangeLastName) {
        manageYourTripPage.changeNameLastNameField().clear();
        manageYourTripPage.changeNameLastNameField().sendKeys(ChangeLastName);
    };

    this.clickChangeNameContinueBtn = function () {
        manageYourTripPage.changeNameContinueBtn().click();
    };

    this.changeNameIsCharedCorrectly = function () {
        var total = manageYourTripPage.changeNameTotal().getText().then(function (total) {
            manageYourTripPage.changeNameContinueBtn().click();
            priceBreakDownActions.clickOnOpenPriceBreakDown();
            var priceOfSeat = pages.priceBreakDownPage.priceTotalFlightValue().getText().then(function (priceOfChangeName) {
                expect(priceOfChangeName).toEqual(total);
                reporter.addMessageToSpec("Change Name Card Total: " + total + " PriceBreakDown Change Name Total: " + priceOfChangeName);
            });
        });
    };

};

module.exports = ManageYourTripActions;

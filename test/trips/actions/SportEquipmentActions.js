var Pages = require('../../Pages')
var pages = new Pages();

var sportEquipmentActions = function () {

    var sportEquipmentPage = pages.sportEquipmentPage;
    var pt = pages.potentialTripPage;

    this.expectSportEquipmentTypeSinglePrice = function(index ,price){
        expect((sportEquipmentPage.sportEquipmentTypeSinglePrice(index)).getText()).toContain(price);
    };
    this.expectDropDownIsClosed = function(){
        expect((sportEquipmentPage.openedDropDown()).isPresent()).not.toBeTruthy();
    };
    this.expectPassengerNameIsCorrect = function(i, firstName, lastName){
        expect((sportEquipmentPage.passengerNameAll(i)).getText()).toEqual(firstName + " " + lastName);
    };
    this.expectMinusButtonIsDisabled = function(i,j) {
        expect(sportEquipmentPage.minusButtonDisabledSport(i, j).isPresent()).toBeTruthy();
    };
    this.expectSportEqipmetInformationText = function() {
        expect((sportEquipmentPage.sportEqipmetInformationText()).isDisplayed()).toBeTruthy();
    };
    this.manageInformationCardWithLink = function(){
        sportEquipmentPage.informationLinkViewTypes().click();
    };
    this.expectSportTypeTitlePlaceholder = function(index, title){
        expect(sportEquipmentPage.sportEquipmentTypeTitle(index).isDisplayed()).toBe(true);
    };
    this.expectSportTypeTitleValue = function(index, title){

        expect(sportEquipmentPage.sportEquipmentTypeTitle(index).getAttribute('value')).toBe(title);
        sportEquipmentPage.sportEquipmentTypeTitle(index).getAttribute('value').then(function(value){
            expect(value).toBe(title);
        });
    };
    this.isXWorkingPromise = function(){
        sportEquipmentPage.clickCloseButtonX().then(function(){
            browser.sleep(1000);
            pt.openCard(7).click();
        });
    };
    this.isCancelWorkingPromise = function(){
        sportEquipmentPage.cancelButton(1).click().then(function(){
            browser.sleep(1000);
            pt.openCard(7).click();
        });
    };
    this.isESCWorkingPromise = function(){
        browser.actions().sendKeys(protractor.Key.ESCAPE).perform().then(function(){
            browser.sleep(1000);
            pt.openCard(7).click();
        });
    };
    this.openSportEquipmentDropdown = function(i){
        sportEquipmentPage.sportEquipmentTypeSelection(i).click();
    };
    this.selectPlusButtonSameForBothFlights = function (i) {
        sportEquipmentPage.plusButtonSameForBothFlights(i).click();
    };
    this.selectPlusButtonSameForBothFlightsSport = function (i,j) {
        sportEquipmentPage.plusButtonSameForBothFlightsSport(i,j).click();
    };
    this.selectMinusButtonSameForBothFlightsSport = function (i,j) {
        sportEquipmentPage.minusButtonSameForBothFlightsSport(i,j).click();
    };
    this.expectSelectSportEquipmentTypeIsDisplayed = function (numberOfSelection) {
        expect((sportEquipmentPage.sportEqupmentTypeClickCount()).count()).toEqual(numberOfSelection);
    };
    this.expectMoreInfoInCollapsedState = function (i) {
        expect((sportEquipmentPage.moreInformationME()).isPresent()).not.toBeTruthy();
    };
    this.selectPlusButtonFlightBack = function (i) {
        sportEquipmentPage.plusButtonFlightBack(i).click();
    };
    this.selectPlusButtonFlightOut = function (i) {
        sportEquipmentPage.plusButtonFlightOut(i).click();
    };
    this.selectMinusButtonSameForBothFlights = function (i) {
        sportEquipmentPage.minusButtonSameForBothFlights(i).click();
    };
    this.selectMinusButtonFlightBack = function (i) {
        sportEquipmentPage.minusButtonFlightBack(i).click();
    };
    this.selectMinusButtonFlightOut = function (i) {
        sportEquipmentPage.minusButtonFlightOut(i).click();
    };
    this.plusButtonValidation = function (i) {
        expect((sportEquipmentPage.plusButtonSameForBothFlights(i)).isPresent()).toBeTruthy();
    };
    this.minusButtonValidation = function (i) {
        expect((sportEquipmentPage.minusButtonSameForBothFlights(i)).isPresent()).toBeTruthy();
    };
    this.plusButtonDisabledSFBF = function (i) {
        expect((sportEquipmentPage.plusButtonDisabled(i)).isPresent()).toBeTruthy();
    };
    this.minusButtonDisabledSFBF = function (i) {
        expect((sportEquipmentPage.minusButtonDisabled(i)).isEnabled()).toBeTruthy();
    };
    this.selectCheckBoxSFBF = function (i) {
        sportEquipmentPage.checkBoxSFBF(i).click();
    };
    this.expectCheckBoxSFBFisChecked = function (i) {
        expect((sportEquipmentPage.checkBoxSFBFSelected(i)).isPresent()).toBeTruthy();
    };
    this.expectCheckBoxSFBFisCheckedHigh = function (i,j) {
        expect((sportEquipmentPage.checkBoxSFBFSelectedSport(i,j)).isPresent()).toBeTruthy();
    };
    this.expectCheckBoxSFBFisUnChecked = function (i) {
        expect((sportEquipmentPage.checkBoxSFBFUnSelected(i)).isPresent()).toBeTruthy();
    };
    this.expectCheckBoxSFBFisUnCheckedHigh = function (i,j) {
        expect((sportEquipmentPage.checkBoxSFBFUnSelectedSport(i,j)).isPresent()).toBeTruthy();
    };
    this.expectSportEquipmentAmountValidation = function () {
        expect((sportEquipmentPage.amountSportEquipment()).isPresent()).toBeTruthy();
        expect((sportEquipmentPage.amountSportEquipment()).isDisplayed()).toBeTruthy();
    };
    this.expectInboundFlightSportOption = function () {
        expect((sportEquipmentPage.SportLogo).count()).toEqual(2);
        expect((sportEquipmentPage.plusFirstButton).count()).toEqual(2);
    };
    this.expectSportEquipmentAmount = function (i,p) {
        expect((sportEquipmentPage.amountSportEquipment(i).getText())).toEqual(p);
    };
    this.expectSportEquipmentAmountLeft = function (i,p) {
        expect((sportEquipmentPage.amountSportEquipmentLeft(i).getText())).toContain(p);
    };
    this.expectSportEquipmentAmountRight = function (i,p) {
        expect((sportEquipmentPage.amountSportEquipmentRight(i).getText())).toContain(p);
    };
    this.expectEquipmentInformationLabel = function (value) {
        expect((sportEquipmentPage.labelAddSportEquipment()).getText()).toEqual(value);
    };
    this.expectSportEquipmentTitle = function (value) {
        expect((sportEquipmentPage.titleSportEquiment()).getText()).toEqual(value);
    };
    this.expectSportEquipmentLableIcon = function () {
        expect((sportEquipmentPage.labelAddSportEquipmentIcon()).isPresent()).toBeTruthy();
    };
    this.expectCardIsInCollapsedState = function() {
        expect((sportEquipmentPage.plusInformationButton()).isDisplayed()).toBeTruthy();
    };
    this.expectCardIsInExpandedState = function() {
        expect((sportEquipmentPage.minusInformationButton()).isDisplayed()).toBeTruthy();
    };
    this.expandInformationCard = function(){
        sportEquipmentPage.plusInformationButton().click();
    };
    this.expectInformationCardIsDisplayed = function(){
        expect(sportEquipmentPage.informationCardME().isDisplayed()).toBeTruthy();
    };
    this.collapseInformationCard = function(){
        sportEquipmentPage.minusInformationButton().click();
    };
    this.expectSportEquipmentInformationText = function(value){
        expect((sportEquipmentPage.sportEquipmentInformationCardText()).getText()).toContain(value);
    };
    this.expectSportEquipmentInformationTextIsHidden = function(){
        expect((sportEquipmentPage.sportEquipmentInformationCardText()).isPresent()).not.toBeTruthy();
    };
    this.clickCancel = function(){
        sportEquipmentPage.cancelButton(1).click();
    };
    this.clickESC = function(){
        browser.actions().sendKeys(protractor.Key.ESCAPE).perform();
    };
    this.clickTAB = function(){
        browser.actions().sendKeys(protractor.Key.TAB).perform();
    };
    this.expectTotalPriceIsDisplayed = function(){
        expect(sportEquipmentPage.cornerPrice().isPresent()).toBeTruthy();
    };
    this.expectXButtonIsDisplayed = function(){
        expect(sportEquipmentPage.closeButtonX().isDisplayed()).toBeTruthy();
    };
    this.expectBackButtonIsDisplayed = function(){
        expect(sportEquipmentPage.backButton().isDisplayed()).toBeTruthy();
    };
    this.expectPassengerCardIsDisplayed = function(i){
        expect(sportEquipmentPage.passengerCard(i).isDisplayed()).toBeTruthy();
    };
    this.clickCloseButtonX = function(){
        sportEquipmentPage.closeButtonX().click();
    };
    this.expectCancelButtonIsDisplayed = function(){
        expect(sportEquipmentPage.cancelButton(1).isDisplayed()).toBeTruthy();
    };
    this.expectInboundTextIsDisplayed = function(){
        expect(sportEquipmentPage.inboundText().isDisplayed()).toBeTruthy();
    };
    this.expectOutboundTextIsDisplayed = function(){
        expect(sportEquipmentPage.outboundText().isDisplayed()).toBeTruthy();
    };
    this.expectCentralPriceDisplayed = function(i){
        expect(sportEquipmentPage.centralPrice().isDisplayed()).toBeTruthy();
    };
    this.expectSportEquipmentAmountNN = function (k, i) {
        expect((sportEquipmentPage.amountSportEquipment(k).getText())).toEqual(i);
    };
    this.expectSportEquipmentAmountNNN = function (k, i) {
        expect((sportEquipmentPage.amountSportEquipmentSeted(k).getText())).toEqual(i);
    };
    this.expectCentralPriceLeftDisplayed = function(i){
        expect(sportEquipmentPage.centralPriceLeft(i).isPresent()).toBeTruthy();
    };
    this.expectConfirmButtonIsDisplayed = function(){
        expect(sportEquipmentPage.confirmButton().isPresent()).toBeTruthy();
    };
    this.expectCentralPriceRightDisplayed = function(i){
        expect(sportEquipmentPage.centralPriceRight(i).isPresent()).toBeTruthy();
    };
    this.expectCentralPrice = function(i,j,p){
        expect(sportEquipmentPage.centralPriceSport(i,j).getText()).toContain(p);
    };
    this.expectCentralPriceNN = function(p){
        expect(sportEquipmentPage.centralPriceNN().getText()).toContain(p);
    };
    this.expectCentralPriceNNN = function(p){
        expect(sportEquipmentPage.centralPriceNNN().getText()).toContain(p);
    };
    this.expectCentralPriceLeft = function(i,p){
        expect(sportEquipmentPage.centralPriceLeft(i).getText()).toContain(p);
    };
    this.expectCentralPriceRight = function(i,p){
        expect(sportEquipmentPage.centralPriceRight(i).getText()).toContain(p);
    };
    this.expectSportEquipmentAmountLeftValidation = function (i) {
        expect((sportEquipmentPage.amountSportEquipmentLeft(i)).isPresent()).toBeTruthy();
    };
    this.expectSportEquipmentAmountRightValidation = function (i) {
        expect((sportEquipmentPage.amountSportEquipmentRight(i)).isPresent()).toBeTruthy();
    };
    this.expectLabelSFBFIsDisplayed = function (i) {
        expect((sportEquipmentPage.checkBoxSFBF(i)).isPresent()).toBeTruthy();
    };
    this.expectSportEquipmentAmountValidation = function () {
        expect((sportEquipmentPage.amountSportEquipment(i)).isPresent()).toBeTruthy();
    };
    this.expectPlusDisableToolTipShown = function () {
        expect((sportEquipmentPage.toolTipDisablePlusShown()).isPresent()).toBeTruthy();
    };
    this.expectPlusDisableToolTipHidden = function () {
        expect((sportEquipmentPage.toolTipDisablePlusHide()).isPresent()).toBeTruthy();
    };
    this.expectPlusDisableToolTipText = function (i,p) {
        expect((sportEquipmentPage.toolTipDisablePlusText(i))).toEqual(p);
    };
    this.expectPlusDisableToolTipTextAmount = function (p) {
        expect((sportEquipmentPage.toolTipDisablePlusTextAmount())).toEqual(p);
    };
    this.expectInboundText = function(i){
        expect(sportEquipmentPage.inboundText().getText()).toEqual(i);
    };
    this.expectOutboundText = function(i){
        expect(sportEquipmentPage.outboundText().getText()).toEqual(i);
    };
    this.expectSFBFText = function(i){
        expect(sportEquipmentPage.textSFBF().getText()).toEqual(i);
    };
    this.expectPassengerTypeAndNumber = function (i, k) {
        expect((sportEquipmentPage.passengerTypeAndNumber(i)).getText()).toEqual(k);
    };
    this.expectQuantityCardisDisplayed = function(i){
        expect(sportEquipmentPage.quantityCard(i).isDisplayed()).toBeTruthy();
    };
    this.expectPassengerLogoIsDisplayed = function(i){
        expect(sportEquipmentPage.passengerLogo(i).isPresent()).toBeTruthy();
    };
    this.expectTotalPriceToBe = function (p) {
        expect(sportEquipmentPage.cornerPrice().getText().then(function (text) {
            return text.substring(9);
        })).toContain(p);
    };
    this.expectSportLogoIsDisplayed = function(i){
        expect(sportEquipmentPage.sportLogo(i).isPresent()).toBeTruthy();
    };
    this.expectDropdownButtonIsDisplayed = function(i){
        expect(sportEquipmentPage.dropdownButton(i).isPresent()).toBeTruthy();
    };
    this.expectCheckBoxIsDisplayed = function(i){
        expect(sportEquipmentPage.checkBoxSFBF(i).isDisplayed()).toBeTruthy();
    };
    this.expectTotalPrice = function(p){
        expect(sportEquipmentPage.cornerPrice().getText()).toContain(p);
    };
    this.expectSportEquipmentAmountLeftValidation = function (i) {
        expect((sportEquipmentPage.amountSportEquipmentLeft(i)).isPresent()).toBeTruthy();
    };
    this.expectSportEquipmentAmountRightValidation = function (i) {
        expect((sportEquipmentPage.amountSportEquipmentRight(i)).isPresent()).toBeTruthy();
    };
    this.confirmButtonValidation = function () {
        expect((sportEquipmentPage.confirmButton()).isPresent()).toBeTruthy();
    };
    this.clickConfirmButton = function () {
        sportEquipmentPage.confirmButtonClick().click();
    };
    this.expectConfirmButtonIsDisabled = function () {
        expect((sportEquipmentPage.confirmButtonDisabled()).isPresent()).toBeTruthy();
    };
    this.expectConfirmButtonIsEnabled = function () {
       expect((sportEquipmentPage.confirmButtonEnabled()).isPresent()).toBeTruthy();
    };
    this.expectSportEquipmentAmount0 = function (i,j,k) {
        expect((sportEquipmentPage.amountSportEquipmentSport(i,j).getText())).toEqual(k);
    };
    this.expectSportEquipmentAmount1 = function (k,i) {
        expect((sportEquipmentPage.amountSportEquipmentSeted(k).getText())).toEqual(i);
    };
    this.selectSportEquipmentType = function (i,j,k) {
        sportEquipmentPage.sportSelectEqupmentType(i,j,k).click();
    };
    this.selectSportEquipmentTypeGetText = function (indexOfPassinger,indexOfSelection,indexOfOption, text) {
        expect((sportEquipmentPage.sportSelectEqupmentTypeGetText(indexOfPassinger,indexOfSelection,indexOfOption)).getText()).toContain(text);
    };
    this.selectSportEquipmentTypeClick = function (i,j) {
        sportEquipmentPage.sportEqupmentTypeClick(i,j).click();
    };
    this.plusButtonEnabled = function (i) {
        expect((sportEquipmentPage.plusButtonEnablePage(i)).isPresent()).toBeTruthy();
    };
    this.selectAddAnotherTypeOfEquipment = function (i) {
        sportEquipmentPage.addAnotherTypeOfEquipment(i).click();
    };
    this.addAnotherTypeOfEquipmentDisplayed = function (i) {
        expect(sportEquipmentPage.addAnotherTypeOfEquipment(i).isPresent()).toBeTruthy();
    };
    this.expectSportEquipmentType = function (i,j) {
        expect(sportEquipmentPage.sportEqupmentTypeClick(i,j).isPresent()).toBeTruthy();
        expect(sportEquipmentPage.sportEqupmentTypeClick(i,j).isDisplayed()).toBeTruthy();
    };
    this.expectAddAnotherTypeOfEquipmentDisabled = function (indexOfPassinger) {
        expect(sportEquipmentPage.addAnotherTypeOfEquipmentDisable(indexOfPassinger).isEnabled()).toBeTruthy();
    };
    this.expectAddAnotherTypeOfEquipmentEnabled = function (indexOfPassinger) {
        expect(sportEquipmentPage.addAnotherTypeOfEquipmentEnabled(indexOfPassinger).isEnabled()).toBeTruthy();
    };
    this.expectSportEqupmentTypeDisabled = function (indexOfPassinger,indexOfSelection,indexOfOption) {
        expect(sportEquipmentPage.sportSelectEqupmentType(indexOfPassinger,indexOfSelection,indexOfOption).isEnabled()).toBeTruthy();
    };
    this.expectSportEqupmentTypeEnabled = function (indexOfPassinger,indexOfSelection,indexOfOption) {
        expect(sportEquipmentPage.sportSelectEqupmentType(indexOfPassinger,indexOfSelection,indexOfOption).isEnabled()).toBeTruthy();
    };
    this.expectInfantIconIsDisplayed = function (indexOfPassengerCard) {
        sportEquipmentPage.infantIcon(indexOfPassengerCard).click();
    };
    this.selectPlusButtonSameForBothFlightsSportLeft = function (indexOfPassenger,indexOfSelection) {
        sportEquipmentPage.plusButtonSameForBothFlightsSportLeft(indexOfPassenger,indexOfSelection).click();
    };
    this.selectMinusButtonSameForBothFlightsSportLeft = function (indexOfPassenger,indexOfSelection) {
        sportEquipmentPage.minusButtonSameForBothFlightsSportLeft(indexOfPassenger,indexOfSelection).click();
    };
    this.selectPlusButtonSameForBothFlightsSportRight = function (indexOfPassenger,indexOfSelection) {
        sportEquipmentPage.plusButtonSameForBothFlightsSportRight(indexOfPassenger,indexOfSelection).click();
    };
    this.selectMinusButtonSameForBothFlightsSportRight = function (indexOfPassenger,indexOfSelection) {
        sportEquipmentPage.minusButtonSameForBothFlightsSportRight(indexOfPassenger,indexOfSelection).click();
    };
    this.expectSportEquipmentTypeIsDisplayed = function (indexOfPassenger,indexOfSelection) {
        expect(sportEquipmentPage.sportEqupmentTypeClick(indexOfPassenger,indexOfSelection).isDisplayed()).toBeTruthy();
    };
    this.expectMaximumMessageIsDisplayed = function (indexOfPassenger) {
        expect(sportEquipmentPage.maximumReachedEquipment(indexOfPassenger).isDisplayed()).toBeTruthy();
    };
    this.expectMaximumMessageReached = function(indexOfPassenger,textDisplayed){
        expect(sportEquipmentPage.maximumMessage(indexOfPassenger).getText()).toContain(textDisplayed);
    };
    this.expectSportActiveEquipmentPurchased = function(typeOfEquipment,inbound, outbound) {
        expect((sportEquipmentPage.sportActiveInboundEquipment(typeOfEquipment).getText())).toContain(inbound);
        expect((sportEquipmentPage.sportActiveOutboundEquipment(typeOfEquipment).getText())).toContain(outbound);
    };
};
module.exports = sportEquipmentActions;
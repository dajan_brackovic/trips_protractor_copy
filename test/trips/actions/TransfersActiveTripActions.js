var Pages = require('../../Pages')
var pages = new Pages();

var transferActiveTripsActions = function () {
    var transfersActiveTripPage = pages.transfersActiveTripPage;

    this.openParkingOnActiveTrip = function(){
        transfersActiveTripPage.parkingCard().click();
    };
    this.expectAirportTransfersTitle = function(message){
        expect((transfersActiveTripPage.airportTransfersTitle()).getText()).toEqual(message);
    };
    this.expectAddedTransferFroAllPassengersMessage = function(message){
        expect((transfersActiveTripPage.addedTransferFroAllPassengersMessage()).getText()).toEqual(message);
    };
    this.expectAmountOfTransfersMessage = function(message){
        expect((transfersActiveTripPage.amountOfTransfersMessage()).getText()).toContain(message);
    };
    this.expectAmountOfTransfersMessage1 = function(message){
        expect((transfersActiveTripPage.amountOfTransfersMessage1()).getText()).toContain(message);
    };
};

module.exports = transferActiveTripsActions;
var Pages = require('../../Pages')
var pages = new Pages()
var sprintf = require("sprintf").sprintf;

var CheckInAction = function () {
    var checkInPage = pages.checkInPage;
    var checkInActions = this;

    this.todoSimpleCeckIn = function () {
        reporter.addMessageToSpec("Step not implemented yet.")
    }

    this.verifySimpleCheckIn = function () {
        reporter.addMessageToSpec("Step not implemented yet.")
    }

    this.selectAllPax = function () {
        checkInPage.paxListForCheckIn().count().then(function (listOfPax) {
            if (listOfPax > 2) {
                checkInPage.selectEveryone().click();
                checkInPage.btnContinueCheckIn().click();
            }
            else {
                checkInPage.paxListForCheckIn().get(0).click();
                checkInPage.paxListForCheckIn().get(1).click();
                checkInPage.btnContinueCheckIn().click();
            }
        })

    };

    this.addDocsForMultiPax = function (numAdult, numTeen, numChild, numInfant, index) {
        var nonInfantPax = numAdult + numTeen + numChild;

        for (var i = 0; i < nonInfantPax; i++) {
            this.selectNationalityDropDown();
            this.enterDateOfBirth(index);
            this.completeCheckInDocs();
        }
    };

    this.addDocsForMultiPaxBal = function (totalPax) {

        for (var i = 0; i < totalPax; i++) {
            this.selectNationalityDropDown();
            this.enterDateOfBirthBal(3);
        }
    };

    this.completeCheckInDocs = function (numAdult, numTeen, numChild, numInfant) {
        this.enterDocumentType(1);
        this.enterDocumentNumberField();
        this.enterCountryOfIssueDropDown();
        this.enterExpiryDate();
        this.clickBtnContinueCheckIn()
    };


    this.selectNationalityDropDown = function () {
        checkInPage.nationalityDropDown(17).click(); // 17 is the index for Ireland
    };

    this.enterDateOfBirth = function (index) {
        checkInPage.dateOfBirthDay(2).click();
        checkInPage.dateOfBirthMonth(3).click();
        checkInPage.dateOfBirthYear(index).click();
    };

    this.enterDateOfBirthBal = function (index) {

        checkInPage.disabledDateDropDown().isPresent().then(function (isPresent) {
            if (isPresent) {
                checkInPage.btnContinueCheckIn().click();
            }
            else {
                checkInPage.dateOfBirthDay(2).click();
                checkInPage.dateOfBirthMonth(3).click();
                checkInPage.dateOfBirthYear(index).click();
                checkInPage.btnContinueCheckIn().click();
            }
        });

    };

    this.countDocumentType = function () {
        element.all(by.css("[ng-model='doc.model.docType'] option")).count().then(function (originalCount) {
            var pass = checkInPage.documentType(originalCount - 1).getText().then(function (pass) {
                expect(pass).toContain("Passport");
                reporter.addMessageToSpec(" Type of doc present: " + pass)
            });
        });
    };

    this.enterDocumentType = function (index) {
        checkInPage.documentType(index).click();
    };

    this.enterDocumentNumberField = function () {
        checkInPage.documentNumberField().sendKeys("1235678A");
    };

    this.enterCountryOfIssueDropDown = function () {
        checkInPage.countryOfIssueDropDown(17).click();
    };

    this.enterExpiryDate = function () {
        checkInPage.expiryDateDay(2).click();
        checkInPage.expiryDateDayMonth(3).click();
        checkInPage.expiryDateDayYear(5).click();
    };

    this.clickBtnContinueCheckIn = function () {
        browser.executeScript('arguments[0].scrollIntoView()', checkInPage.btnContinueCheckIn().getWebElement());
        checkInPage.btnContinueCheckIn().click();
        browser.sleep(10000);
    };

    // My RyanAir Action

    this.assertOnPreSelectedIdDocument = function () {
        checkInPage.preSelectedIdDocument().getAttribute("selected").then (function (value) {
            expect(value).toBe("true");
            reporter.addMessageToSpec("selected: " + value);
        });

        expect(checkInPage.preSelectedIdDocument().getText()).toContain("Passport - IRE1234567")
    };

    this.clickContinueBtnForMyRyanairSavedPax = function (totalPax) {
        for (var i = 1; i <= totalPax; i++) {
            this.clickBtnContinueCheckIn();
        };
    };

    this.clickSelectFlightCheckbox = function () {
        checkInPage.selectFlightCheckbox().click();
    };

    this.unSelectFlightCheckboxRtOut = function () {
        checkInPage.selectFlightCheckboxRtOut().click();
    };

    this.unSelectFlightCheckboxRtIn = function () {
        checkInPage.selectFlightCheckboxRtIn().click();
    };

    this.verifyInboundCheckinNotOpen = function () {
        expect(checkInPage.checkInNotOpenWarning().isPresent()).toBe(true);
    };

    this.assertOnBookingRef = function (bookingRefActiveTrip) {
        checkInPage.viewAllBoardingPasses().isPresent().then(function (isPresent) {
            if (isPresent) {
                checkInPage.viewAllBoardingPasses().click();
            }
        });
        expect(checkInPage.bpBpPageBookingRef().getText()).toEqual(bookingRefActiveTrip);
        checkInPage.bpBpPageBookingRef().getText().then(function (bookingRefBoardingPass) {
            bookingRefActiveTrip.then(function (bookingRefActiveTrip) {
                reporter.addMessageToSpec("Boarding Pass Bookng Ref: " + bookingRefBoardingPass);
                reporter.addMessageToSpec("Active Trip Bookng Ref: " + bookingRefActiveTrip);
            });
        });
    };

    this.verifySpecialAssistanceOnBP = function (tripway) {
        //TODO - Uncomment when bug FRW-3537 is resolved
        //expect(checkInPage.bpBpPageHeaderMessage().getText()).toContain("SPECIAL ASSISTANCE");
        expect(checkInPage.bpBpPageLuggage().get(0).getText()).toContain("BDGR");
        expect(checkInPage.bpBpPageLuggage().get(0).getText()).toContain("WCAP");
        if (tripway === "twoway") {
            expect(checkInPage.bpBpPageLuggage().get(1).getText()).toContain("BDGR");
            expect(checkInPage.bpBpPageLuggage().get(1).getText()).toContain("WCAP");
        }

        checkInPage.bpBpPageHeaderMessage().getText().then(function (headerMessage) {
            checkInPage.bpBpPageLuggage().getText().then(function (details) {
                reporter.addMessageToSpec("Special Assistance header message on Boarding Pass: " + headerMessage);
                reporter.addMessageToSpec("Special Assistance details on Boarding Pass: " + details);
            });
        });

    };

    this.verifyBizPlusOnBP = function () {
         expect(checkInPage.bpBpPageHeaderMessage().getText()).toContain("BIZ+");
    };


    this.assertOnBordingPass = function () {
        expect(checkInPage.boardingPassPage().isPresent()).toBe(true);
    };

    this.assertSeatsOnBp = function (selectedSeat) {
        selectedSeat.then(function (selectedSeat) {
            var selectedSeat1 = selectedSeat.substring(0, 3);
            console.log(selectedSeat1);
            expect(checkInPage.bpBpPageSeatNumber().get(0).getText()).toContain(selectedSeat1);
        })

        checkInPage.bpBpPageSeatNumber().getText().then(function (boardingSeatNum) {
            reporter.addMessageToSpec("Boarding Pass Seat: " + boardingSeatNum);
        });
        selectedSeat.then(function (selectedSeat) {
            reporter.addMessageToSpec("Selected Seat: " + selectedSeat);
        });
    };

    this.assertSeatsOnBpReturnInbound = function (selectedSeat) {
        selectedSeat.then(function (selectedSeat) {
            var selectedSeat1 = selectedSeat.substring(0, 3);
            console.log(selectedSeat1);
            expect(checkInPage.bpBpPageSeatNumber().get(1).getText()).toContain(selectedSeat1);
        })

        checkInPage.bpBpPageSeatNumber().getText().then(function (boardingSeatNum) {
            reporter.addMessageToSpec("Boarding Pass Seat: " + boardingSeatNum);
        });
        selectedSeat.then(function (selectedSeat) {
            reporter.addMessageToSpec("Selected Seat: " + selectedSeat);
        });
    };

    this.assertSeatOnCheckIn = function (selectedSeat) {
        selectedSeat.then(function (selectedSeat) {
            var selectedSeat1 = selectedSeat.substring(0, 3);
            console.log(selectedSeat1);
            expect(checkInPage.selectedSeatNumber().get(0).getText()).toContain(selectedSeat1);
        })
    }

    this.assertSeatOnCheckInReturn = function (selectedSeatOut, selectedSeatIn) {
        selectedSeatOut.then(function (selectedSeatOut) {
            var selectedSeatOut1 = selectedSeatOut.substring(0, 3);
            console.log(selectedSeatOut1);
            expect(checkInPage.selectedSeatNumber().get(0).getText()).toContain(selectedSeatOut1);
        })
        selectedSeatIn.then(function (selectedSeatIn) {
            var selectedSeatIn1 = selectedSeatIn.substring(0, 3);
            console.log(selectedSeatIn1);
            expect(checkInPage.selectedSeatNumber().get(1).getText()).toContain(selectedSeatIn1);
        })
    }

    this.assertSeatsOnBpReturn = function (selectedSeatOut, selectedSeatIn) {
        selectedSeatOut.then(function (selectedSeatOut) {
            var selectedSeatOut1 = selectedSeatOut.substring(0, 3);
            console.log(selectedSeatOut1);
            expect(checkInPage.bpBpPageSeatNumber().get(0).getText()).toContain(selectedSeatOut1);
        })

        selectedSeatIn.then(function (selectedSeatIn) {
            var selectedSeatIn1 = selectedSeatIn.substring(0, 3);
            console.log(selectedSeatIn1);
            expect(checkInPage.bpBpPageSeatNumber().get(1).getText()).toContain(selectedSeatIn1);
        })

        checkInPage.bpBpPageSeatNumber().get(0).getText().then(function (boardingSeatNumOut) {
            reporter.addMessageToSpec("Boarding Pass Seat Outbound: " + boardingSeatNumOut);
        });

        checkInPage.bpBpPageSeatNumber().get(1).getText().then(function (boardingSeatNumIn) {
            reporter.addMessageToSpec("Boarding Pass Seat Inbound: " + boardingSeatNumIn);
        });

        selectedSeatOut.then(function (selectedSeat) {
            reporter.addMessageToSpec("Selected Seat Outbound: " + selectedSeat);
        });

        selectedSeatIn.then(function (selectedSeat) {
            reporter.addMessageToSpec("Selected Seat Inbound: " + selectedSeat);
        });
    };

    this.selectCheckboxSeat = function (tripWay) {
        checkInPage.checkInSeatOut().click();
        if (tripWay === "twoway") {
            checkInPage.checkInSeatIn().click();
        }
    };

    this.selectCheckInSeatCheckboxInbound = function () {
        checkInPage.checkInSeatIn().click();
    };

    this.selectCheckInSeatCheckboxOutbound = function () {
        checkInPage.checkInSeatOut().click();
    };

    this.assertOnAutoAllocation = function (tripWay, totalPax) {
        checkInPage.bpBpPageSeatNumber().count().then(function (NumberOfSeatsOnBp) {
            if (tripWay === "twoway") {
                expect(NumberOfSeatsOnBp).toEqual(totalPax * 2);
            }
            else {
                expect(NumberOfSeatsOnBp).toEqual(totalPax);
            }
        });
    };

    this.assertBagsOnBpMultiPax = function (tripWay, totalPax) {
        checkInPage.bpBpPageLuggage().count().then(function (NumberOfBagsOnBp) {
            if (tripWay === "twoway") {
                expect(NumberOfBagsOnBp).toEqual(totalPax * 2);
            }
            else {
                expect(NumberOfBagsOnBp).toEqual(totalPax);
            }
        });
    };

    this.assertOnFlightNotReadyForCheckIn = function () {
        expect(checkInPage.flightNotOpenForCheckIn().isDisplayed()).toBeTruthy();
    };

    this.assertOnNumberOfBoardingPass = function (NumberOfBoardingPass) {
        checkInPage.boardingPassCard().count().then(function (NumberOfBp) {
            expect(NumberOfBp).toEqual(NumberOfBoardingPass);
        })
    };

    this.countNumberOfBoardingPassesInList = function (totalPax) {
        checkInPage.paxListBoardingPasses().count().then(function (NumberOfBpInList) {
            reporter.addMessageToSpec("Number of Boarding Passes in List: " + NumberOfBpInList);
            expect(NumberOfBpInList).toEqual(totalPax);
        })
        checkInPage.viewAllBoardingPasses().click();
    };

    this.assertBagsOnBp = function (tripWay, addedBags) {
        expect(checkInPage.bpBpPageLuggage().get(0).getText()).toContain(addedBags);
        if (tripWay === "twoway") {
            expect(checkInPage.bpBpPageLuggage().get(1).getText()).toContain(addedBags);
        }
    };

    this.assertSpanishDiscountOnBp = function (tripWay, option) {
        expect(checkInPage.bpBpPageLuggage().get(0).getText()).toContain(option);
        if (tripWay === "twoway") {
            expect(checkInPage.bpBpPageLuggage().get(1).getText()).toContain(option);
        }
    };

    this.assertSpanishDiscountOnBpForMultiPax = function (option) {
        checkInPage.bpBpPageLuggage().getText().then(function(listOfDiscountOnBp){
            expect(listOfDiscountOnBp[0]).toContain(option);
        })
    };

    this.clickXOut = function () {
        checkInPage.xOut().click();
    };

    this.clickBtnFaildPaymentOk = function () {
        checkInPage.btnFaildPaymentOk().click();
    };

    this.clickSelectEveryOneForCheckIn = function () {
        checkInPage.selectEveryOneForCheckIn().click();
    };

    this.assertOnChangedName = function (changeFirstName, changeLastName) {
        expect(checkInPage.bpBpPagePaxFirstName().getText()).toContain(changeFirstName);
        expect(checkInPage.bpBpPagePaxSecondName().getText()).toContain(changeLastName);
    };

    this.closeBoardingPassWindow = function () {
        checkInPage.btnContinueCheckIn().click();
        browser.sleep(2000);
    };

    this.assertOnPriorityBp = function (tripWay, wordPriority) {
        expect(checkInPage.priorityBp().get(0).getText()).toContain(wordPriority);
        if (tripWay === "twoway") {
            expect(checkInPage.priorityBp().get(1).getText()).toContain(wordPriority);
        }
    };

    this.assertOnSeatMessaging = function (text) {
        expect(checkInPage.seatMessaging().getText()).toContain(text);
    }

    this.assertOnPriorityBpByMarket = function (tripWay) {
        var wordPriority = browser.getCurrentUrl().then(function (url) {
            if (url.indexOf("/gb/en/") != -1) {
                console.log("-----testing /gb/en/ ");
                return "PRIORITY";
            }
            else if (url.indexOf("/es/es/") != -1) {
                console.log("---------testing /es/es/");
                return "PRIORIDAD";
            }
            else if (url.indexOf("/pl/pl/") != -1) {
                console.log("---------testing /pl/pl/");
                return "PRIORYTET";
            }
            else if (url.indexOf("/it/it/") != -1) {
                console.log("---------testing /it/it/");
                return "PRIORITA";
            }
            else if (url.indexOf("/es/es/") != -1) {
                console.log("---------testing /es/es/")
                return "PRIORITY";
            }
            //......
            //TODO: test more countries, you may need to add more cases here
            else {
                console.log("---------testing a default english language market")
                return "PRIORITY";
            }
        });
        expect(checkInPage.priorityBp().get(0).getText()).toContain(wordPriority);
        if (tripWay === "twoway") {
            expect(checkInPage.priorityBp().get(1).getText()).toContain(wordPriority);
        }


    };

    this.assertPriorityMorePassengers = function (tripWay, num, wordPriority) {
        for (var i = 0; i < num; i++) {
            expect(checkInPage.priorityBp().get(i).getText()).toContain(wordPriority);
        }
        if (tripWay === "twoway") {
            var doublenum = num * 2;
            for (var i = 1; i < doublenum; i += 2) {
                expect(checkInPage.priorityBp().get(doublenum).getText()).toContain(wordPriority);
            }
        }
        ;

    };
};
module.exports = CheckInAction;

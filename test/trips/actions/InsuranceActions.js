var params = browser.params.conf;
var Pages = require('../../Pages');
var pages = new Pages();

var insuranceActions = function () {

    var insurancePage = pages.insurancePage;

    this.expectTitleForInsurancePolicy = function (title) {
        expect(insurancePage.titleInsurancePolicySite().getText()).toContain(title);
    };
    this.clickToOpenInsurancePDFinNewTab = function (country) {
        insurancePage.downloadPolicy().click();
        browser.sleep(5000);
        browser.getAllWindowHandles().then(function (handles) {
            var secondWindowHandle = handles[1];
            var firstWindowHandle = handles[0];
            //the focus moves on the newest opened tab
            browser.switchTo().window(secondWindowHandle).then(function () {
                browser.sleep(1000);
                //check if the right page is opened
                expect(browser.driver.getCurrentUrl()).toContain(country);
                browser.switchTo().window(firstWindowHandle).then(function () {
                    browser.driver.close();
                });
                browser.switchTo().window(secondWindowHandle)
            });
        });
    };
    this.expectConfirmButtonIsDisabled = function () {
        expect(insurancePage.confirmButtonDisabled().isPresent()).toBeTruthy();
    };
    this.expectInsuranceAdded = function (numberOfEquipment) {
        expect(insurancePage.insuranceAdded().getText()).toContain(numberOfEquipment);
    };
    this.expectInsuranceSideDrawerTitle = function () {
        expect(insurancePage.titleInsurance().getText()).toBe(params.insurance.header)
    };
    this.expectInsuranceSideDrawerTitleFalse = function () {
        expect(insurancePage.titleInsurance().isPresent()).toBeFalsy();
    };
    this.clickCloseButtonX = function () {
        insurancePage.closeButtonX().click();
    };
    this.clickCancelButton = function (index) {
        insurancePage.cancelButton(index).click();
    };
    this.closeWithESC = function () {
        browser.actions().sendKeys(protractor.Key.ESCAPE).perform();
    };
    this.expectInformationMessage1 = function () {
        expect(insurancePage.informationText1().getText()).toBe(params.insurance.messageBefore);
    };
    this.expectInformationMessage2 = function () {
        expect(insurancePage.informationText2().getText()).toBe(params.insurance.messageAfter);
    };
    this.expectInformationMessageChildTeen = function () {
        expect(insurancePage.informationChildTeen().getText()).toBe(params.insurance.informationChildTeen);
    };
    this.expectComparasionTable = function () {
        expect(insurancePage.comparasionTable().isDisplayed()).toBeTruthy();
    };
    this.clickViewPolicyPlusButton = function () {
        insurancePage.viewPolicy().click();
    };
    this.expectPolicyDropDown = function () {
        expect(insurancePage.policyDropDown().isDisplayed()).toBeTruthy();
    };
        this.expectPassengerTypeAndNumberNoInfant = function (index, name) {
            expect((insurancePage.passengerTypeAndNumber(index)).getText()).toContain(name);
        };
        this.expectPassengerTypeAndNumber = function (index, name) {
            expect((insurancePage.passengerTypeAndNumber(index)).getText()).toContain(name);
            expect(insurancePage.insuranceInfantIcon(index).isDisplayed()).toBeTruthy();
        };
    this.clickDownloadPolicyButton = function () {
        insurancePage.downloadPolicy().click();
    };
    this.chooseCountryOfResidence = function (countryName) {
        var select = insurancePage.policyDropDown();
        select.$('[label = "' + countryName + '"]').click();
    };
    this.selectCountryOfResidenceForPassanger = function (indexOfPassanger, countryName) {
        insurancePage.countryOfResidenceForPassanger(indexOfPassanger).$('[label = "' + countryName + '"]').click();
    };
    this.expectPolicyPDFIconInsurancePage = function () {
        expect(insurancePage.policyPDFIcon().isDisplayed()).toBeTruthy();
    };
    this.expectStandardInsuranceButtonIsSelected = function () {
        expect(insurancePage.standardInsuranceButtonSelected().isDisplayed()).toBeTruthy();
    };
    this.clickStandardInsuranceButton = function (index) {
        insurancePage.standardInsuranceButton(index).click();
    };
    this.expectInsurancePlusButtonIsSelected = function () {
        expect(insurancePage.insurancePlusButtonSelected().isDisplayed()).toBeTruthy();
    };
    this.clickInsurancePlusButton = function (index) {
        insurancePage.insurancePlusButton(index).click();
    };
    this.expectPriceOnStandardInsuranceButton = function (actualPrice, price) {
        expect(insurancePage.priceOnInsuranceButtons(2).getText(actualPrice)).toEqual(price);
    };
    this.expectPriceOnInsurancePlusButton = function (i, k) {
        expect(insurancePage.priceOnInsuranceButtons(3).getText(actualPrice)).toEqual(price);
    };
    this.expectTotalPriceToBe = function (price) {
        expect(insurancePage.cornerPrice().getText().then(function (text) {
            return text.substring(9);
        })).toContain(price);
    };
    this.expectTotalPriceToBeF = function () {
        expect(insurancePage.cornerPrice().getText().then(function (text) {
            return text.substring(9);
        })).toMatch(params.regEx.price);
    };
    this.expectTotalPriceToBeGreaterThenZero = function () {
        expect(insurancePage.cornerPrice().getText().then(function (text) {
            return text.substring(9);
        })).toMatch(params.regEx.price);
    };
    this.expectConfirmButtonIsDisplayed = function () {
        expect(insurancePage.confirmButton().isPresent()).toBeTruthy();
    };
    this.expectCancelButtonIsDisplayed = function () {
        expect(insurancePage.cancelButton(1).isDisplayed()).toBeTruthy();
    };
    this.expectStandardInsuranceButtonDisabled = function () {
        expect(insurancePage.standardInsuranceButton().isDisplayed()).toBeFalsy();
    };
    this.expectInsurancePlusPurchased = function (index) {
        expect((insurancePage.insurancePlusAddedActive(index).getText())).toContain(params.insurance.insurancePlusPurchasedText);
    };
    this.expectStandardInsurancePurchased = function (index) {
        expect((insurancePage.standardInsuranceAddedActive(index).getText())).toContain(params.insurance.standardInsurancePurchasedText);
    };
    this.expectInsurancePlusButtonDisabled = function () {
        expect(insurancePage.insurancePlusButton().isDisplayed()).toBeFalsy();
    };
    this.clickCancel = function () {
        insurancePage.cancelButton(1).click();
    };
    this.expectPriceOnInsurancePlusButton = function () {
        expect(insurancePage.priceOnInsuranceButtons(1).isDisplayed()).toBeTruthy();
    };
    this.expectPriceOnStandardInsuranceButton = function () {
        expect(insurancePage.priceOnInsuranceButtons(1).isDisplayed()).toBeTruthy();
    };
    this.clickYesPleaseButton = function () {
        insurancePage.yesPleaseButton().click();
    };
    this.clickNoThanksButton = function () {
        insurancePage.noThanksButton().click();
    };
    this.expectQuestionTextOnInsurancePopup = function () {
        expect(insurancePage.questionOnInsurancePopup()).toContain(params.insurance.countryOfResidenceMessage);
    };
    this.clickToClearAll = function () {
        insurancePage.clearAllLink().click();
    };
    this.expectYesThanksIsSelected = function () {
        expect(insurancePage.yesPleaseButton().isDisplayed()).toBeTruthy();
    };
    this.expectSamePriceOnInsuranceButtons = function (index1, index2) {
        expect(insurancePage.priceOnInsuranceButtons(index1).getText()).toEqual(insurancePage.priceOnInsuranceButtons(index2).getText());
    };
    this.expectNoPriceOnInsuranceButtons = function () {
        expect(insurancePage.insuranceButtonWithOutPrice(0).isPresent()).toBeTruthy();
        expect(insurancePage.insuranceButtonWithOutPrice(1).isPresent()).toBeTruthy();
    };
    this.expectDiscountPriceOnInsuranceButton = function (index) {
        expect(insurancePage.priceOnInsuranceButtons(index).getText().then(function (text) {
            return text.substring(2);
        })).toMatch(params.regEx.price);
    };
    this.clickConfirmButton = function () {
        insurancePage.confirmButton().click();
    };
    this.expectDiscountMessageOnInsurance = function (index, text) {
        expect(insurancePage.insuranceDiscountMessage(index).getText()).toContain(text);
    };
    this.expectConfirmButtonIsEnabled = function () {
        expect(insurancePage.confirmButton().isPresent()).toBeTruthy();
    };
    this.expectInsuranceIsAddedText = function () {
        expect(insurancePage.insuranceAddedHeader().getText()).toContain(params.insurance.headerPotentialInsurance);
    };
    this.getPriceFromCorner = function () {
        return insurancePage.cornerPrice().getText().then(function (text) {
            return text.substring(9);
        });
    };
    this.expectCurrentPriceIsDifferentThenChanged = function (changedPrice) {
        expect(insurancePage.cornerPrice().getText().then(function (text) {
            return text.substring(9);
        })).not.toEqual(changedPrice);
    };
    this.expectEqualPrices = function (currentPrice) {
        expect(insurancePage.cornerPrice().getText().then(function (text) {
            return text.substring(9);
        })).toEqual(currentPrice);
    };


};
module.exports = insuranceActions;
var Pages = require('../../Pages')
var pages = new Pages();
var Trip = require('../../shared/model/Trip');
var params = browser.params.conf;

var paymentActions = function () {
    var paymentPage = pages.paymentPage;

    this.paymentActiveTrip = function(paxMap, origin, destination, outBoundDaysFromNow, x){
        trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, x);
        this.skipExtras();
        this.addPaxNameForAllPAX(trip.journey.paxList);
        actions.addPaxActions.clickBtnAddPaxSave();
        browser.sleep(500);
        this.addContact();
        trip.bookingContact.card.cardNumber = "5407581111111115";
        this.makeDCCCardPayment(trip.bookingContact.card);
        browser.sleep(500);
        this.clickPaymentContinue();
        browser.sleep(10000);
    };


    this.makeDCCCardPayment = function (card) {
        paymentPage.selectPaymentTypeDropDownOption(card.type).click();
        paymentPage.textFieldCardNumber().sendKeys(card.cardNumber);
        this.enterCardDetails(card);
    };

    this.enterCardDetails = function (card) {
        paymentPage.selectExpiryMonthDropDownOption(card.expiryDateMonth).click();
        paymentPage.selectExpiryYearDropDownOption(card.expiryDateYear).click();
        paymentPage.textFieldCVV().sendKeys(card.cvv);
        paymentPage.textFieldCardHoldersName().sendKeys("Automation User");
        paymentPage.textFieldCity().sendKeys("Dublin");
        paymentPage.textFieldPostCode().sendKeys("01");
        paymentPage.textFieldStreetName().sendKeys("Swords Road");
    };

    this.clickPaymentContinue = function (){
        paymentPage.btnPaymentContinue().click();
    };

    this.addPaxNameForAllPAX = function (paxList) {

        var elementIndex = 1;
        for (var i = 0; i < paxList.length; i++) {
            if (paxList[i].paxType === 'ADT' || paxList[i].paxType === 'TEEN' || paxList[i].paxType === 'CHD') {
                paymentPage.selectTitleDropDownOption(paxList[i].title, elementIndex).click();
                paymentPage.textFieldFirstName(elementIndex).sendKeys(paxList[i].firstName);
                paymentPage.textFieldLastName(elementIndex).sendKeys(paxList[i].lastName);
                elementIndex++;
            }
        }
        var elementIndex = 1;
        for (var i = 0; i < paxList.length; i++) {
            if (paxList[i].paxType === 'INF') {
                paymentPage.infantFirst(elementIndex).sendKeys(paxList[i].firstName);
                paymentPage.infantLast(elementIndex).sendKeys(paxList[i].lastName);
                paymentPage.infantDAY(elementIndex, "15").click();
                paymentPage.infantMONTH(elementIndex, "Jun").click();
                paymentPage.infantYEAR(elementIndex, "2015").click();
                elementIndex++;
            }
        }
    };

    this.clickBtnAddPaxSave = function (){
        browser.executeScript('window.scrollTo(0,500);');
        paymentPage.btnAddPaxSave().click();
    };

    this.skipExtras = function() {
        paymentPage.btnExtrasContinue().click();
    };

    this.addContact = function () {
        paymentPage.textFieldEmailAddress().sendKeys("auto@ryanair.ie");
        paymentPage.selectContactCountryDropDownOption("Ireland").click();
        paymentPage.textFieldPhoneNumber().sendKeys("12345678");
        paymentPage.btnAddPaxPageContinue().click();
    };
    this.expectCheckOutPaymentPageTitle = function(text){
        expect((paymentPage.checkOutPaymentPageTitle()).getText()).toContain(text);
    };
    this.expectCheckOutForPriceBreakdown = function(index){
        expect((paymentPage.checkOutForPriceBreakdown(index)).isDisplayed()).toBeTruthy();
    };
    this.expectPriceBreakDownFlightOut = function(index, textAmount, textName, indexOfName){
        expect((paymentPage.priceBreakDownAdultFare(index)).getText()).toContain(textAmount);
        expect((paymentPage.priceBreakDownPrice(index)).getText().then(function (text) {
            return text.substring(2);
        })).toMatch(params.regEx.price);
        expect((paymentPage.priceBreakDownFlightOutName(indexOfName)).getText()).toContain(textName);
    };
    this.expectPriceBreakDownExtraEquipment = function(index, text, amount, price){
        expect((paymentPage.priceBreakDownExtraEquipmentTitle(index)).getText()).toContain(text);
        expect((paymentPage.priceBreakDownExtraEquipmentAmount(index)).getText()).toContain(amount);
        expect((paymentPage.priceBreakDownExtraEquipmentPrice(index)).getText().then(function (text) {
            return text.substring(2);
        })).toMatch(params.regEx.price);
    };
    this.expectPriceBreakDownInsurance = function(index, text, amount, price){
        expect((paymentPage.priceBreakDownInsuranceTitle(index)).getText()).toContain(text);
        expect((paymentPage.priceBreakDownInsuranceAmount(index)).getText()).toContain(amount);
        expect((paymentPage.priceBreakDownInsurancePrice(index)).getText().then(function (text) {
            return text.substring(2);
        })).toMatch(params.regEx.price);
    };
    this.expectPriceBreakDownParking = function(index, text, place, price){
        expect((paymentPage.priceBreakDownParkingTitle(index)).getText()).toContain(text);
        expect((paymentPage.priceBreakDownParkingPlace(index)).getText()).toContain(place);
        expect((paymentPage.priceBreakDownParkingPrice(index)).getText().then(function (text) {
            return text.substring(2);
        })).toMatch(params.regEx.price);
    };
};

module.exports = paymentActions;

var Pages = require('../../Pages')
var pages = new Pages();

var manageTripsActions = function () {
    var manageTripsPage = pages.manageTripsPage;

    this.manageTripAccess = function (reservationNumber, reservationMail) {
        actions.fOHActions.goToPage();
        browser.sleep(3000);
        manageTripsPage.manageTripDropDown().click();
        browser.sleep(3000);
        manageTripsPage.reservationNumber().sendKeys(reservationNumber);
        manageTripsPage.reservationEMail().sendKeys(reservationMail);
        manageTripsPage.manageTripGoButton().click();
    };
    this.manageTripAccessCard = function (reservationNumber, cardNumber) {
        actions.fOHActions.goToPage();
        browser.sleep(1000);
        manageTripsPage.manageTripDropDown().click();
        browser.sleep(1000);
        manageTripsPage.manageTripMailCheckBox().click();
        manageTripsPage.reservationNumber().sendKeys(reservationNumber);
        manageTripsPage.reservationCardNumber().sendKeys(cardNumber);
        manageTripsPage.manageTripGoButton().click();
    };

    this.manageTripAccessCardFromBookingRef = function (bookingRefActiveTrip, lastFourCreditCardDigits) {
        manageTripsPage.manageTripDropDown().click();
        manageTripsPage.manageTripMailCheckBox().click();
        manageTripsPage.reservationNumber().sendKeys(bookingRefActiveTrip);
        manageTripsPage.reservationCardNumber().sendKeys(lastFourCreditCardDigits);
        manageTripsPage.manageTripGoButton().click();
    };

    this.manageTripAccessCardFromFlightDetails = function (outBoundDaysFromNow, origin, destination, bookingRefActiveTrip) {
        manageTripsPage.manageTripDropDown().click();
        manageTripsPage.checkBoxFlightDetails().click();
        manageTripsPage.calenderFieldFlyingOutOn().click();

        var today = new Date();
        var day = today.getUTCDate();
        var dd = day + outBoundDaysFromNow - 1;
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var flyingDay = dd + '-' + mm + '-' + yyyy;
        reporter.addMessageToSpec("Flying Date: --> " + flyingDay);
        console.log(flyingDay);
        browser.sleep(2000);
        manageTripsPage.flyingOutOn(flyingDay).click();
        manageTripsPage.fieldEmailAddress().sendKeys("auto@ryanair.ie");
        manageTripsPage.fieldFrom().sendKeys(origin);
        manageTripsPage.fieldFrom().sendKeys(protractor.Key.ENTER);
        manageTripsPage.fieldTo().sendKeys(destination);
        manageTripsPage.firstAirportOptionInDropDown().click();
        browser.sleep(2000);
        manageTripsPage.manageTripGoButton().click();

        manageTripsPage.retrieveFromMultipleBookings().isPresent().then(function (isPresent) {
            if (isPresent) {
                manageTripsPage.yourBooking(bookingRefActiveTrip).click();
                manageTripsPage.btnContinueRetrieveFromMultipleBookings().click();
            }
        });
    };


    // My Fr Manage Trip Actions

    this.clickManageTripDropDown = function () {
        manageTripsPage.manageTripDropDown().click();
    };

    this.assertOnSavedTripCard = function (boolean, noOfSavedTrips) {
        expect(manageTripsPage.savedTripContent().get(noOfSavedTrips - 1).isPresent()).toBe(boolean);
        expect(manageTripsPage.savedTripContent().count()).toEqual(noOfSavedTrips);
    };

    this.assertSavedTripCardIsEmpty = function () {
        expect(manageTripsPage.savedTripContent().count()).toEqual(0);
    };

    this.assertOnUpcomingTripCardIsPresent = function (destination) {
        expect(manageTripsPage.upcomingTripsCards().get(0).isPresent()).toBe(true);
        expect(manageTripsPage.upcomingTripsCardsDestination().getText()).toContain(destination);
    };

    this.assertOnSavedTripDestination = function (destination) {
        expect(manageTripsPage.savedTripDestination().get(0).getText()).toContain(destination);
    };

    this.clickOnSavedTripDestination = function () {
        manageTripsPage.savedTripDestination().get(0).click();
    };

    this.assertOnSavedTripContent = function (boolean) {
        expect(manageTripsPage.savedTripDestination().get(0).isPresent()).toBe(boolean);
        expect(manageTripsPage.savedTripDeparture().get(0).isPresent()).toBe(boolean);
        expect(manageTripsPage.savedTripDate().get(0).isPresent()).toBe(boolean);
        expect(manageTripsPage.savedTripPrice().get(0).isPresent()).toBe(boolean);
    };

    this.clickBtnRemoveSavedTrip = function () {
        manageTripsPage.btnRemoveSavedTrip().count().then(function (originalCount) {
            for (var i = 0; i < originalCount; i++) {
                manageTripsPage.btnRemoveSavedTrip().get(0).click();
            }
        });
    };

    this.assertOnUpcomingTripsCards = function () {
        expect(manageTripsPage.upcomingTripsCards().get(0).isPresent()).toBe(true);
        expect(manageTripsPage.upcomingTripsCardsImage().get(0).isPresent()).toBe(true);
        expect(manageTripsPage.upcomingTripsCardsOrigin().get(0).getText()).toContain("Dublin");
        expect(manageTripsPage.upcomingTripsCardsDestination().get(0).getText()).toContain("London");
        expect(manageTripsPage.upcomingTripsCardsJourneyDate().get(0).isPresent()).toBe(true);
        expect(manageTripsPage.upcomingTripsCardsCheckInOpenDate().get(0).isPresent()).toBe(true);
    };

    this.clickOnUpcomingTripsCardImage = function () {
        manageTripsPage.upcomingTripsCardsImage().click();
    };

    this.clickHeldFareCard = function () {
        manageTripsPage.heldFareCard().get(0).click();
    };

    this.assertOnHeldFareCard = function (noOfHeldFareCard) {
        expect(manageTripsPage.heldFareCard().count()).toBe(noOfHeldFareCard);
    };

    this.assertOnHeldFareTimeLeft = function (boolean) {
        expect(manageTripsPage.heldFareTimeLeft().get(0).isPresent()).toBe(boolean);
    };

    this.assertOnManageMyTripContent = function () {
        expect(manageTripsPage.manageMyTripHeader().isPresent()).toBe(true);
        expect(manageTripsPage.upcomingTripsTitle().isPresent()).toBe(true);
        expect(manageTripsPage.savedTripsTitle().isPresent()).toBe(true);
    };
};

module.exports = manageTripsActions;

var Pages = require('../../Pages')
var pages = new Pages();

var LowCostParkingActions = function () {

    var lowCostParkingPage = pages.lowCostParkingPage;


    this.expectBasketIsNotDisplayed = function () {
        expect((lowCostParkingPage.basketParking()).isDisplayed()).not.toBeTruthy();
    };
    this.expectTitleTermsAndConditionsLinkIsDisplayed = function () {
        expect((lowCostParkingPage.termsAndConditionsLink()).isPresent()).toBeTruthy();
    };
    this.expectTermsAndConditionsLinkIsCorrect = function (link) {
        expect(lowCostParkingPage.termsAndConditionsLink().getAttribute('href')).toBe(link);
        lowCostParkingPage.termsAndConditionsLink().getAttribute('href').then(function (value) {
            expect(value).toBe(link);
        });
    };
    this.clickOnTerAndConditionsLink = function () {
        lowCostParkingPage.termsAndConditionsLink().click();
    };
    this.expectParking28DaysMessage = function () {
        expect(lowCostParkingPage.parking28Days().isPresent()).toBe(true);
        lowCostParkingPage.parking28Days().getText().then(function(message){
            reporter.addMessageToSpec("Parking message: " + message);
        })
    };
    this.changeParkingOption = function (i) {
        lowCostParkingPage.parkingOption(i).click();
    };
    this.expectSelectedParkingOptionIsDisplayed = function () {
        expect(lowCostParkingPage.selectedParkingOption().isDisplayed()).toBeTruthy();
    };
    this.clickConfirmButton = function () {
        lowCostParkingPage.confirmButton().click();
    };
    this.logoParkViaValidation = function () {
        expect(lowCostParkingPage.parkingParkViaLogo().isPresent()).toBeTruthy();
    };
    this.logoL4PValidation = function () {
        expect(lowCostParkingPage.parkingL4PLogo().isPresent()).toBeTruthy();
    };
    this.linkCloudValidation = function () {
        expect(lowCostParkingPage.parkingCloudLink().isPresent()).toBeTruthy();
    };
    this.linkL4PValidation = function () {
        expect(lowCostParkingPage.parkingL4PLink().isPresent()).toBeTruthy();
    };
    this.changeParkingProviderDown = function () {
        lowCostParkingPage.secondParkingProvider().click();
    };
    this.changeParkingProviderUp = function () {
        lowCostParkingPage.firstParkingProvider().click();
    };
    this.clickParkingProvider = function () {
        lowCostParkingPage.parkingProviderDropdown().click();
    };
    this.expectLowCostParkingTitle = function (value) {
        expect(lowCostParkingPage.titleLowCostParking().isPresent()).toBeTruthy();
        expect(lowCostParkingPage.titleLowCostParking().getText()).toEqual(value);
    };

    this.expectParkingProviderTitle = function () {
        expect(lowCostParkingPage.parkingProviderTitle().isPresent()).toBeTruthy();
    };

    this.expectMultiParkingProvider = function () {
        expect(lowCostParkingPage.parkingProviderOptionUnSelected().isPresent()).toBeTruthy();
    };

    this.expectOneParkingProvider = function () {
        expect(lowCostParkingPage.parkingProviderOptionUnSelected().isPresent()).not.toBeTruthy();
    };
    this.expectpParkingProviderLogoIsDisplayed = function () {
        expect(lowCostParkingPage.parkingProviderLogo().isDisplayed()).toBeTruthy();
    };
    this.expectpParkingProviderLogoIsDisplayedDAA = function () {
        expect(lowCostParkingPage.parkingProviderLogoDAA().isPresent()).toBeTruthy();
    };
    this.expectpParkingProviderLogoIsDisplayedCloud = function () {
        expect(lowCostParkingPage.parkingProviderLogoCloud().isPresent()).toBeTruthy();
    };
    this.expectListOfBenefits = function () {
        expect(lowCostParkingPage.listOfBenefits().isPresent()).toBeTruthy();
        expect(lowCostParkingPage.listOfBenefits().isDisplayed()).toBeTruthy();
        expect(lowCostParkingPage.listOfBenefitsCheck().isPresent()).toBeTruthy();
    };

    this.expectListOfParkingOptions = function () {
        expect(lowCostParkingPage.listOfParkingOptions().isDisplayed()).toBeTruthy();
    };

    this.expectParkingProviderInLockedState = function () {
        expect(lowCostParkingPage.parkingProviderLockedState().isDisplayed()).toBeTruthy();
    };

    this.expectParkingProviderdropDownIsDisplayed = function (index) {
        expect(lowCostParkingPage.parkingProviderDropdown(index)).toBeTruthy();
    };

    this.expectNameAndPricePerDay = function (i) {
        expect(lowCostParkingPage.nameOfParkingOption(i).isPresent()).toBeTruthy();
        expect(lowCostParkingPage.priceOfParkingOption(i).isPresent()).toBeTruthy();
    };

    this.expectInformationIcon = function (i) {
        expect(lowCostParkingPage.informationIcon(i).isPresent()).toBeTruthy();
    };
    this.expectTotalPriceValidation = function () {
        expect((lowCostParkingPage.totalPrice()).isPresent()).toBeTruthy();
        expect((lowCostParkingPage.totalPrice()).isDisplayed()).toBeTruthy();
    };

    this.closeSideDrawer = function () {
        lowCostParkingPage.closeDrawerButton().click();
    };

    this.expectCloseX = function () {
        expect((lowCostParkingPage.closeDrawerButton()).isPresent()).toBeTruthy();
    };

    this.expectSideDrawerOpen = function () {
        expect(lowCostParkingPage.parkingProviderTitle().isPresent()).toBeTruthy();
        expect(lowCostParkingPage.parkingProviderTitle().isDisplayed()).toBeTruthy();
    };

    this.expectSideDrawerClosed = function () {
        expect(lowCostParkingPage.registrationNumber().isPresent()).not.toBeTruthy();
    };
    this.expectRegistrationNumberInput = function () {
        expect(lowCostParkingPage.registrationNumber().isPresent()).toBeTruthy();
    };
    this.enterRegistrationNumber = function (value) {
        lowCostParkingPage.registrationNumber().sendKeys(value);
    };

    this.expectRegistrationFieldValidation = function () {
        expect(lowCostParkingPage.registrationNumber().isPresent()).toBeTruthy();
    };

    this.expectRegistrationNumberCharacters = function (inputText) {
        var sad = lowCostParkingPage.registrationNumber().getAttribute('value');
        expect(sad).toEqual(inputText);
        lowCostParkingPage.registrationNumber().clear();
    };

    this.expectWrongRegistrationNumberCharacters = function (inputText) {
        lowCostParkingPage.registrationNumber().sendKeys(inputText)
        var sad = lowCostParkingPage.registrationNumber().getAttribute('value');
        expect(sad).not.toEqual(inputText);
        lowCostParkingPage.registrationNumber().clear();
    };

    this.expectDatePickerValidation = function () {
        lowCostParkingPage.datePicker().click();
        expect(lowCostParkingPage.datePicker().isPresent()).toBeTruthy();
    };

    this.expectDatePickerIsNotPresent = function () {
        expect(lowCostParkingPage.datePicker().isPresent()).not.toBeTruthy();
    };

    this.expectParkingProviderIsDisplayed = function () {
        expect(lowCostParkingPage.parkingProviderTitle().isPresent()).toBeTruthy();
        expect(lowCostParkingPage.parkingProviderTitle().isDisplayed()).toBeTruthy();
    };

    this.expectConfirmButton = function () {
        expect(lowCostParkingPage.confirmButton().isPresent()).toBeTruthy();
    };

    this.selectDate = function (dateForParking) {
        lowCostParkingPage.datePicker().click();
        lowCostParkingPage.selectDay(dateForParking).click();
    };

    this.expectDateInDatepickerIsDisplayed = function (i) {
        var sad = lowCostParkingPage.selectDateTextField().getText();
        expect(sad).toContain(i);
    };

    this.clickInformationIcon = function (i) {
        lowCostParkingPage.informationIcon(i).click();
    };
    this.expectInformationIcon = function (i) {
        expect(lowCostParkingPage.informationIcon(i)).toBeTruthy();
    };

    this.expectInformationIconPopUpHidden = function () {
        expect(lowCostParkingPage.informationIconPopUpHidden().isPresent()).toBeTruthy();
    };

    this.expectInformationIconPopUpShown = function () {
        expect((lowCostParkingPage.informationIconPopUpHidden()).isPresent()).not.toBeTruthy();
        expect((lowCostParkingPage.informationIconPopUp()).isPresent()).toBeTruthy();
    };

    this.expectInformationIconPopUpItemsNames = function (i, value) {
        expect((lowCostParkingPage.informationIconPopUpItemsNames(i)).getText()).toContain(value);
    };
    this.clickDownloadPDFButton = function (i) {
        lowCostParkingPage.informationIconDownloadPDF().click();
        browser.sleep(1000);
        browser.getAllWindowHandles().then(function (handles) {
            var secondWindowHandle = handles[1];
            var firstWindowHandle = handles[0];
            //the focus moves on the newest opened tab
            browser.switchTo().window(secondWindowHandle).then(function () {
                browser.sleep(1000);
                //check if the right page is opened
                expect(browser.driver.getCurrentUrl()).toContain(i);
            });
        });
    };
    this.expectInformationIconDownloadPDFButton = function () {
        expect((lowCostParkingPage.informationIconDownloadPDF()).isPresent()).toBeTruthy();
        expect((lowCostParkingPage.informationIconDownloadPDF()).isDisplayed()).toBeTruthy();
    };
    this.expectInformationIconDownloadPDFNewPage = function (link, value) {
        expect(link).toEqual(value);
    };
    this.expectInformationIconTitle = function (value) {
        expect((lowCostParkingPage.informationIconPopUpTitle()).getText()).toContain(value);
    };
    this.clickInfoButton = function (i) {
        lowCostParkingPage.informationIcon(i).click();
    };
    this.clickOptionStayParkingProvider = function (i) {
        lowCostParkingPage.unSelectedOptionOfParkingType(i).click();
    };
    this.returnfirstStateParkingPrice = function () {
        var firstState = lowCostParkingPage.totalPrice().getText().then(function (firstText) {
            return firstText;
        });
    };

    this.assertOnUpdateParkingPrice = function (firstState) {
        expect(firstState).not.toEqual(lowCostParkingPage.totalPrice().getText());
    }

    this.expectParkingDateTotalPriceIsChanged = function (dateForParking) {
        var firstState = lowCostParkingPage.totalPrice().getText().then(function (text) {
            return text;
        });
        this.selectDate(dateForParking);
        var secondState = lowCostParkingPage.totalPrice().getText().then(function (text) {
            return text;
        });
        expect(firstState).not.toEqual(secondState);
    };

    this.informationMessageRegistrationNumber = function (char, value0, value1) {
        this.enterRegistrationNumber(char);
        actions.lowCostParkingActions.clickConfirmButton();
        expect(lowCostParkingPage.informationMessageRN().getText()).toEqual(value0);
        lowCostParkingPage.registrationNumber().clear();
        expect(lowCostParkingPage.informationMessageRN().getText()).toEqual(value1);
        actions.lowCostParkingActions.clickConfirmButton();
    };
    this.expectChangeNumberOFParkingProviders = function (i, j) {
        var firstStatePrice = lowCostParkingPage.priceOfParkingOptionCount();
        var firstStateName = lowCostParkingPage.priceOfParkingOptionCount();
        expect(firstStateName.count()).toEqual(i);
        expect(firstStatePrice.count()).toEqual(i);
        this.changeParkingProviderDown();
        var secondStatePrice = lowCostParkingPage.priceOfParkingOptionCount();
        var secondStateName = lowCostParkingPage.priceOfParkingOptionCount();
        expect(secondStatePrice.count()).toEqual(j);
        expect(secondStateName.count()).toEqual(j);
        this.changeParkingProviderUp();
    };
    this.expectParkingProviderNameDisplayed = function (indexOfParking, value) {
        expect(lowCostParkingPage.parkingProviderDropdown(indexOfParking).getText()).toContain(value);
    };
    this.expectTotalPriceToBe = function (price) {
        expect(lowCostParkingPage.cornerPrice().getText().then(function (text) {
            return text.substring(7);
        })).toContain(price);
    };
    this.expectTotalPriceValidationCorner = function () {
        expect((lowCostParkingPage.totalPriceCorner()).isPresent()).toBeTruthy();
        expect((lowCostParkingPage.totalPriceCorner()).isDisplayed()).toBeTruthy();
    };

    this.expectTotalPriceToBeGreaterThenZero = function () {
        expect(lowCostParkingPage.cornerPrice().getText().then(function (text) {
            return text.substring(9);
        })).toMatch(params.regEx.price);
    };
};
module.exports = LowCostParkingActions;
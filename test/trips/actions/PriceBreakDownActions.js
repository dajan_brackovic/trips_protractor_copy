var Pages = require('../../Pages')
var pages = new Pages();
var sprintf = require("sprintf").sprintf;

var PriceBreakDownActions = function () {
    var priceBreakDownPage = pages.priceBreakDownPage;
    var tripsHomePage = pages.tripsHomePage;
    var EC = protractor.ExpectedConditions;

    this.clickOnOpenPriceBreakDown = function () {
        browser.executeScript('window.scrollTo(1000,0);');
        browser.wait(EC.presenceOf( priceBreakDownPage.openDropPriceBreakDown()), 5000);
        priceBreakDownPage.openDropPriceBreakDown().click();
    };

    this.assertNoFlightMessageText = function (textMessage) {
        expect(priceBreakDownPage.textMessageNoFlightPriceBrakeDown().getText()).toEqual(textMessage);
    };

    this.assertOnNoFlightIcon = function () {
        expect(priceBreakDownPage.noFlightIcon().isDisplayed());
    };

    this.assertStandardPriceFlightCartAndPriceBeakDown = function(destination)
    {
        expect(priceBreakDownPage.priceOutFlightValue().getText()).toMatch(tripsHomePage.getSelectedFarePrice(destination));
         priceBreakDownPage.priceOutFlightValue().getText().then(function(number){
             tripsHomePage.getSelectedFarePrice(destination).then(function(number1){
                 console.log("Flight price: ",number1);
                 console.log("PBDown price:",number);
                 reporter.addMessageToSpec(sprintf("Selected Flight price: %s, PriceBreakDown price: %s", number1, number));
             });
          });
    };

    this.assertStandardPriceFlightCartAndPriceBeakDownReturn = function(destination)
    {
        expect(priceBreakDownPage.priceInFlightValue().getText()).toMatch(tripsHomePage.getSelectedFarePrice(destination));
        priceBreakDownPage.priceInFlightValue().getText().then(function(number){
            tripsHomePage.getSelectedFarePrice(destination).then(function(number1){
                console.log("Return Flight price:",number1);
                console.log("Return PBDown price",number);
                reporter.addMessageToSpec(sprintf("Selected Flight price: %s, PriceBreakDown price: %s", number1, number));
            });
        });
    };

    this.assertBusinessPriceFlightCartAndPriceBeakDownReturn = function(destination)
    {
        expect(priceBreakDownPage.priceInBusinessFlightValue().getText()).toMatch(tripsHomePage.getSelectedFarePrice(destination));
        priceBreakDownPage.priceInBusinessFlightValue().getText().then(function(number){
            tripsHomePage.getSelectedFarePrice(destination).then(function(number1){
                console.log("Flight price:",number1);
                console.log("PBDown price",number);
                reporter.addMessageToSpec(sprintf("Selected Flight price: %s, PriceBreakDown price: %s", number1, number));
            });
        });
    };

    this.assertOnTotalPriceBeakDownReturn = function()
    {
        priceBreakDownPage.priceOutFlightValue().getText().then(function(outValue){
            priceBreakDownPage.priceInFlightValue().getText().then(function(inValue){
                priceBreakDownPage.priceTotalFlightValue().getText().then(function(totalValue){
                    var outFloatVal = parseFloat(outValue.substring(2)),
                        inFloatVal = parseFloat(inValue.substring(2)),
                        totalFloatVal = parseFloat(totalValue.substring(2));
                    reporter.addMessageToSpec(sprintf("outFloatVal: %s + inFloatVal: %s = totalFloatVal: %s", outFloatVal, inFloatVal, totalFloatVal));
                    expect(totalFloatVal).toBeCloseTo(outFloatVal + inFloatVal, 2);
                })
            })
        });
    };

    this.assertOnTotalPriceBeakDownOneWay = function () {
        priceBreakDownPage.priceOutFlightValue().getText().then(function (outValue) {
            priceBreakDownPage.priceTotalFlightValue().getText().then(function (totalValue) {
                var outFloatVal = parseFloat(outValue.substring(2)),
                    totalFloatVal = parseFloat(totalValue.substring(2));
                reporter.addMessageToSpec(sprintf("outFloatVal: %s  = totalFloatVal: %s", outFloatVal, totalFloatVal));
                expect(totalFloatVal).toEqual(outFloatVal);
            })

        });
    };

    this.assertOnTotalPriceBeakDownBusinessReturn = function(destination)
    {
        priceBreakDownPage.priceOutFlightValue().getText().then(function(outValue){
            priceBreakDownPage.priceInBusinessFlightValue().getText().then(function(inValue){
                priceBreakDownPage.priceTotalFlightValue().getText().then(function(totalValue){
                    var outFloatVal = parseFloat(outValue.substring(2)),
                        inFloatVal = parseFloat(inValue.substring(2)),
                        totalFloatVal = parseFloat(totalValue.substring(2));

                    console.log("outFloatVal: " + outFloatVal);
                    console.log("inFloatVal: " + inFloatVal);
                    console.log("totalFloatVal: " + totalFloatVal);

                    reporter.addMessageToSpec(sprintf("outFloatVal: %s + inFloatVal: %s = totalFloatVal: %s", outFloatVal, inFloatVal, totalFloatVal));
                    expect(totalFloatVal).toBeCloseTo(outFloatVal + inFloatVal, 2);
                })
            })
        });
    }

    this.assertDepartArrivalTimes = function()
    {
        expect(priceBreakDownPage.flightOUTInfo()).toContain(tripsHomePage.cardFlightNumberSelected("outbound").getText());
        expect(priceBreakDownPage.flightRETURNInfo()).toContain(tripsHomePage.cardFlightNumberSelected("inbound").getText());
    }

    this.assertFlightNumber = function()
    {
        expect(priceBreakDownPage.flightOUTInfo()).toContain(tripsHomePage.cardDepartureTimeSelected("outbound").getText());
        expect(priceBreakDownPage.flightOUTInfo()).toContain(tripsHomePage.cardArrivalTimeSelected("outbound").getText());
        expect(priceBreakDownPage.flightRETURNInfo()).toContain(tripsHomePage.cardDepartureTimeSelected("inbound").getText());
        expect(priceBreakDownPage.flightRETURNInfo()).toContain(tripsHomePage.cardArrivalTimeSelected("inbound").getText());
    }


    // Price Breakdown Payment page

    this.assertOnListOfSelectedItems = function (extraItem) {
        expect(priceBreakDownPage.listOfSelectedItems().getText()).toContain(extraItem);
    };

    this.assertOnListOfSelectedItemsInBold = function (extraItem) {
        expect(priceBreakDownPage.listOfSelectedItemsInBold().getText()).toContain(extraItem);
    };

    this.assertOnParking = function () {
        expect(priceBreakDownPage.parkingAdded().isPresent());
    };

    this.clickXOutSelectedSeat = function () {
        priceBreakDownPage.xOutSelectedSeat().click();
    };

    this.assetOnBagNotPresent = function () {
        expect(priceBreakDownPage.bagCard().isPresent()).toBe(false);
    };

    this.assetOnSeatNotPresent = function () {
        expect(priceBreakDownPage.seatCard().isPresent()).toBe(false);
    };

    this.assetOnTotalPriceOnHoldFare = function (tripWay) {

        if(tripWay === "twoway") {
            expect(priceBreakDownPage.totalPriceWholeNumber().getText()).toContain("5");
        }

        else {
            expect(priceBreakDownPage.totalPriceWholeNumber().getText()).toContain("2.50");
        }

    };

    this.assertOnHotelsInfoPriceBkDownPaymentPage = function () {
        expect(priceBreakDownPage.hotelInfoPriceBkDownPaymentPage().isDisplayed()).toBeTruthy();
    };

    this.assertHotelsInfoNotOnPriceBkDownPaymentPage = function () {
        expect(priceBreakDownPage.totalHotelsInfoOnPriceBreakdown().isPresent()).toBe(false);
    };

    this.assertOnCarHireInfoPriceBkDownPaymentPage = function () {
        expect(priceBreakDownPage.priceBreakDownCarHireText().isDisplayed()).toBeTruthy();
    };

// PriceBreakDown On Potential Trip Page Car Hire

    this.assertOnCarHireTotalPrice = function (totalCarHirePrice) {
        expect(priceBreakDownPage.totalPriceCarHireOnPriceBreakDown().getText()).toContain(totalCarHirePrice);
    };

    // PriceBreakDown On Potential Trip Page Hotels

    this.assertOnHotelsInfoPriceBkDown = function (hotelName, hotelPrice) {
        browser.sleep(1000);
        actions.priceBreakDownActions.clickOnOpenPriceBreakDown();

        expect(priceBreakDownPage.hotelNamePriceBkDown().getText()).toContain(hotelName);
        expect(priceBreakDownPage.hotelTotalPriceBkDown().getText()).toContain(hotelPrice);

        priceBreakDownPage.totalHotelsInfoOnPriceBreakdown().getText().then(function(info){
            priceBreakDownPage.hotelTotalPriceBkDown().getText().then(function(total){
                reporter.addMessageToSpec("Hotels Info: " + info);
                reporter.addMessageToSpec("Hotels Total: " + total);
            })
        })
    };

    this.assertHotelsInfoNotOnPriceBkDown = function () {
        browser.sleep(1000);
        actions.priceBreakDownActions.clickOnOpenPriceBreakDown();
        expect(priceBreakDownPage.totalHotelsInfoOnPriceBreakdown().isPresent()).toBe(false);
    };

    this.clickCheckBoxPayPalOnPriceBreakDown = function () {
        priceBreakDownPage.checkBoxPayPalOnPriceBreakDown().click();
    };


    this.assertOnCarHireInsuranceDetails = function () {
        expect(priceBreakDownPage.carHireInsuranceDetails().isDisplayed()).toBeTruthy();
    };

    this.assertOnCarHirePresent = function (boolean) {
        expect(priceBreakDownPage.priceBreakDownCarHireText().isPresent()).toBe(boolean);
    };

    this.assertOnPayByMethodFee = function () {
        expect(priceBreakDownPage.payByMethodFeeText().get(0).getText()).toContain("Free");

        priceBreakDownPage.payByMethodFeeText().get(1).getText().then(function (currency) {
            priceBreakDownPage.payByMethodFeeText().get(2).getText().then(function (currency2) {
                var creditCardFee = parseFloat(currency.substring(2));
                var payPalFee = parseFloat(currency2.substring(2));
                console.log(creditCardFee);
                console.log(payPalFee);
                reporter.addMessageToSpec(sprintf("CreditCard Fee: %s  PayPal Fee: %s", creditCardFee, payPalFee));
                expect(creditCardFee).toMatch(/\d+(\.\d{1,2})/);
                expect(payPalFee).toMatch(/\d+(\.\d{1,2})/);

            });
        });
    };

    this.assertOnItalianFrenchPayByMethodFee = function (url) {
        if(url === "it/it") {
            expect(priceBreakDownPage.payByMethodFeeText().get(0).getText()).toContain("Sconto");
        }
        else {
            expect(priceBreakDownPage.payByMethodFeeText().get(0).getText()).toContain("R\u00E9duction");
        }
        priceBreakDownPage.payByMethodFeeText().get(0).getText().then(function (text) {
            reporter.addMessageToSpec(sprintf("Debit Card Info: %s  ", text));
            console.log("text = " + text);
        });
    };

    this.clickCheckBoxCreditCardOnPriceBreakDown = function () {
        priceBreakDownPage.checkBoxCreditCardOnPriceBreakDown().click();
    };

    this.assertOnPaymentFeeLabel = function (blnDisplayed) {
        if (blnDisplayed === true) {
            expect(priceBreakDownPage.paymentFeeLabel().getText()).toContain("Payment fee");
        }
        else{
            expect(priceBreakDownPage.paymentFeeLabel().count()).toBe(0);
        }
    };



    this.assertTotalPriceWhenCcOptionSelected = function (originalTotalPrice) {
        originalTotalPrice.then(function (oldTotal) {

            priceBreakDownPage.paymentFeeValue().getText().then(function (paymentFee) {
                priceBreakDownPage.priceTotalFlightValue().getText().then(function (totalValue) {
                    var newPaymentFee = parseFloat(paymentFee.substring(2)),
                        newTotalVal = parseFloat(totalValue.substring(2)),
                        oldTotalVal = parseFloat(oldTotal.substring(2));

                    expect(newTotalVal).toBeGreaterThan(oldTotalVal);
                    expect(oldTotalVal * 2 / 100).toBeCloseTo(newPaymentFee);
                    reporter.addMessageToSpec(sprintf("Flight Total: %s + CC Payment Fee: %s = Total: %s", oldTotalVal, newPaymentFee, newTotalVal));
                });
            });
        });
    };

    this.returnCreditCardFee = function () {
        return priceBreakDownPage.paymentFeeValue().getText();
    };

    this.returnTotal = function () {
        return priceBreakDownPage.priceTotalFlightValue().getText();
    };


    this.assertOnCurrency = function (currency) {
        expect(priceBreakDownPage.priceTotalFlightValue().getText()).toContain(currency);
        priceBreakDownPage.priceTotalFlightValue().getText().then(function (number) {
            reporter.addMessageToSpec(sprintf("Currency: %s, PriceBreakDown price: %s", currency, number));
        });
    };


    // Actions for UK Child Discount C29017

    this.assertOnUkChildDiscountLabel = function (boolean) {
        expect(priceBreakDownPage.ukChildDiscountLabel().isPresent()).toBe(boolean);
        if (boolean) {
            expect(priceBreakDownPage.ukChildDiscountLabel().getText()).toContain("UK APD Discount");
        }
        else {
            reporter.addMessageToSpec("Child Discount is not Present");
        }
    };

    this.assertOnUkChildDiscountLabelUkDomestic = function (boolean) {
        expect(priceBreakDownPage.ukChildDiscountLabelUkDomestic().get(0).isPresent()).toBe(boolean);
        expect(priceBreakDownPage.ukChildDiscountLabelUkDomestic().get(1).isPresent()).toBe(boolean);
        if (boolean) {
            expect(priceBreakDownPage.ukChildDiscountLabelUkDomestic().get(0).getText()).toContain("UK APD Discount");
            expect(priceBreakDownPage.ukChildDiscountLabelUkDomestic().get(1).getText()).toContain("UK APD Discount");
        }
        else {
            reporter.addMessageToSpec("Child Discount is not Present");
        }
    };

    this.assertOnTotalWhenUkChildDiscountPresentReturn = function () {
        priceBreakDownPage.listOfPricesInPriceBreakdown().get(0).getText().then(function (adultFareOut) {
            priceBreakDownPage.listOfPricesInPriceBreakdown().get(1).getText().then(function (childFareOut) {
                priceBreakDownPage.listOfPricesInPriceBreakdown().get(2).getText().then(function (childDiscountOut) {
                    priceBreakDownPage.listOfPricesInPriceBreakdown().get(3).getText().then(function (adultFareIn) {
                        priceBreakDownPage.listOfPricesInPriceBreakdown().get(4).getText().then(function (childFareIn) {
                            priceBreakDownPage.priceTotalFlightValue().getText().then(function (totalValue) {
                                var adultFareOutVal = parseFloat(adultFareOut.substring(2)),
                                    childFareOutVal = parseFloat(childFareOut.substring(2)),
                                    childDiscountOutVal = parseFloat(childDiscountOut.substring(3)),
                                    adultFareInVal = parseFloat(adultFareIn.substring(2)),
                                    childFareInVal = parseFloat(childFareIn.substring(2)),
                                    totalFloatVal = parseFloat(totalValue.substring(2));
                                reporter.addMessageToSpec(sprintf("Adult Out: %s + Child Out: %s + Adult In: %s + Child In: %s - Child Discount %s = totalFloatVal: %s", adultFareOutVal, childFareOutVal, adultFareInVal, childFareInVal, childDiscountOutVal, totalFloatVal));
                                expect(totalFloatVal).toBeCloseTo(adultFareOutVal + childFareOutVal + adultFareInVal + childFareInVal - childDiscountOutVal, 2);
                            })
                        })
                    })
                })
            })
        });
    };

    this.assertOnTotalWhenUkChildDiscountPresentOneWay = function () {
        priceBreakDownPage.listOfPricesInPriceBreakdown().get(0).getText().then(function (adultFareOut) {
            priceBreakDownPage.listOfPricesInPriceBreakdown().get(1).getText().then(function (childFareOut) {
                priceBreakDownPage.listOfPricesInPriceBreakdown().get(2).getText().then(function (childDiscountOut) {
                    priceBreakDownPage.priceTotalFlightValue().getText().then(function (totalValue) {
                        var adultFareOutVal = parseFloat(adultFareOut.substring(2)),
                            childFareOutVal = parseFloat(childFareOut.substring(2)),
                            childDiscountOutVal = parseFloat(childDiscountOut.substring(3)),
                            totalFloatVal = parseFloat(totalValue.substring(2));
                        reporter.addMessageToSpec(sprintf("Adult Out: %s + Child Out: %s - Child Discount %s = totalFloatVal: %s", adultFareOutVal, childFareOutVal, childDiscountOutVal, totalFloatVal));
                        expect(totalFloatVal).toBeCloseTo(adultFareOutVal + childFareOutVal - childDiscountOutVal, 2);
                    })

                })
            })
        });
    };

    this.verify2PerCentDebitCardDiscount = function (originalTotalPrice) {
        priceBreakDownPage.checkBoxDebitCardOnPriceBreakDown().click();
        browser.sleep(2000);
        originalTotalPrice.then(function(originalTotal){
            priceBreakDownPage.priceTotalFlightValue().getText().then(function (totalValue) {
                var newTotalVal = parseFloat((totalValue.replace("," , ".").substring(0,totalValue.length - 2)));
                var oldTotalVal = parseFloat((originalTotal.replace("," , ".").substring(0,totalValue.length - 2)));
                console.log("oldTotalVal: " + oldTotalVal);
                console.log("newTotalVal: " + newTotalVal);
                expect(newTotalVal).toBeLessThan(oldTotalVal);
                expect(newTotalVal * 1.02).toBeCloseTo(oldTotalVal, 2);
                reporter.addMessageToSpec(sprintf("Total with Credit Card: %s. Total with Debit Card: %s", oldTotalVal, newTotalVal));
            });
        });
    };


};
module.exports = PriceBreakDownActions;
var Pages = require('../../Pages');
var pages = new Pages();
var Faker = require('faker');

var MyRyanairActions = function () {
    var myRyanairPage = pages.myRyanairPage;
    var EC = protractor.ExpectedConditions;

    this.clickUserName = function () {
        myRyanairPage.userName().click();
        expect(myRyanairPage.linkDashboard().isDisplayed()).toBeTruthy();
        expect(myRyanairPage.linkSettings().isDisplayed()).toBeTruthy();
        expect(myRyanairPage.linkLogOut().isDisplayed()).toBeTruthy();
    };

    this.clickLinkDashboard = function () {
        myRyanairPage.linkDashboard().click();
    };

    this.assertOnTabs = function () {
        expect(myRyanairPage.dashboardTab().isDisplayed()).toBeTruthy();
        expect(myRyanairPage.passengersTab().isDisplayed()).toBeTruthy();
        expect(myRyanairPage.paymentsTab().isDisplayed()).toBeTruthy();
        expect(myRyanairPage.preferencesTab().isDisplayed()).toBeTruthy();
        expect(myRyanairPage.rewardsTab().isDisplayed()).toBeTruthy();
    };

    this.clickDashboardTab = function () {
        myRyanairPage.dashboardTab().click();
    };

    this.clickPassengersTab = function () {
        myRyanairPage.passengersTab().click();
    };

    this.clickAddDocumentBtn = function () {
        myRyanairPage.addDocumentBtn().click();
    };

    this.addDocumentDetails = function () {
        myRyanairPage.fieldDocumentNumber().sendKeys(actions.tripHelper.getRandomDocumentNumber());
        myRyanairPage.firstDayOfExpiryDate().click();
        myRyanairPage.janMonthOfExpiryDate().click();
        myRyanairPage.year2020OfExpiryDate().click();
        myRyanairPage.optionIrelandInCountryOfIssue().click();
    };

    this.updateExpiryDatePassportDetails = function () {
        myRyanairPage.dropDownDateOfBirthDay(actions.tripHelper.getRandomNumber(1, 27));
        myRyanairPage.dropDownDateOfBirthMonth(actions.tripHelper.getRandomNumber(1, 12));
        myRyanairPage.dropDownDateOfBirthYear(actions.tripHelper.getRandomNumber(2, 50));
    };

    this.clickBtnUpdateDocumentDetails = function () {
        myRyanairPage.btnUpdateDocumentDetails().click();
    };

    this.editDocumentDetails = function () {
        myRyanairPage.fieldDocumentNumber().clear();
        myRyanairPage.fieldDocumentNumber().sendKeys(actions.tripHelper.getRandomDocumentNumber());
        myRyanairPage.dropDownDateOfBirthDay(actions.tripHelper.getRandomNumber(5, 26)).click();
        myRyanairPage.dropDownDateOfBirthMonth(actions.tripHelper.getRandomNumber(3, 12)).click();
        myRyanairPage.dropDownDateOfBirthYear(actions.tripHelper.getRandomNumber(2, 10)).click();
        myRyanairPage.optionEditIrelandInCountryOfIssue(actions.tripHelper.getRandomNumber(2, 10)).click();
    };

    this.addProfileDocumentDetails = function () {
        myRyanairPage.fieldDocumentNumber().sendKeys(actions.tripHelper.getRandomDocumentNumber());
        myRyanairPage.optionIrelandInCountryOfIssue().click();
    };

    this.addProfileDropDownDocumentType = function (option) {
        myRyanairPage.dropDownProfileDocumentType(option).click();
    };

    this.editProfileDropDownDocumentType = function (option) {
        myRyanairPage.dropDownEditProfileDocumentType(option).click();
    };

    this.clickBtnAddDocumentDetails = function () {
        myRyanairPage.btnAdd().click();
    };

    this.clickBtnUpdateDocumentDetails = function () {
        myRyanairPage.btnUpdateDocumentDetails().click();
    };

    this.selectProfileDocumentDetails = function () {
        myRyanairPage.btnSelectProfileDocument().click();
    };

    this.removeProfileDocumentDetails = function () {
        myRyanairPage.btnRemoveProfileDocument().click();
    };

    this.clickBtnShowPassportDetails = function () {
        myRyanairPage.btnShowPassportDetails().click();
    };

    this.assertProfileDropDownDocumentTypeText = function () {
        expect(myRyanairPage.dropDownProfileDocumentTypeAssert().getText()).toContain("Passport Card");

        myRyanairPage.dropDownProfileDocumentTypeAssert().getText().then(function (text) {
            reporter.addMessageToSpec("text = " + text)
        });
    };

    this.clickBtnRemove = function () {
        myRyanairPage.btnRemove().click();
    };

    this.clickEditProfile = function () {
        myRyanairPage.editProfile().click();
    };

    this.clickEditProfileButton = function () {
        myRyanairPage.editProfileButton().click();
    };

    this.clickProfileAddDocButton = function () {
        myRyanairPage.profileAddDocButton().click();
    };

    this.selectCallingCodeField = function () {
        myRyanairPage.callingCodeField(actions.tripHelper.getRandomNumber(19, 40)).click();
    };

    this.clickSaveProfile = function () {
        myRyanairPage.saveProfileButton().click();
    };

    this.inputPhoneNumber = function () {
        myRyanairPage.phoneNumberBox().clear();
        myRyanairPage.phoneNumberBox().sendKeys(actions.tripHelper.getRandomMobileNumber());

    };

    this.assertOnMyRyanairProgressinPercentage = function (percentage) {
        expect(myRyanairPage.myRyanairProgressinPercentage().getText()).toContain(percentage);
        reporter.addMessageToSpec("Profile Completed in % = " + percentage);
    };

    this.clickBtnAddACompanion = function () {
        myRyanairPage.btnAddACompanion().click();
    };

    this.clickBtnAddCompanionsCompanionPanel = function () {
        myRyanairPage.btnAddCompanionsCompanionPanel().click();
    };

    this.clickTypeFriendCompanion = function () {
        myRyanairPage.typeFriendCompanion().click();
    };

    this.selectDropDownTitle = function (title) {
        myRyanairPage.dropDownTitle(title).click()
    };

    this.inputFieldFirstName = function (FirstName) {
        myRyanairPage.fieldFirstName().clear();
        myRyanairPage.fieldFirstName().sendKeys(FirstName);
    };

    this.inputFieldLastName = function (LastName) {
        myRyanairPage.fieldLastName().clear();
        myRyanairPage.fieldLastName().sendKeys(LastName);
    };

    this.selectDropDownDateOfBirthDay = function (dayIndex) {
        myRyanairPage.dropDownDateOfBirthDay(dayIndex).click();
    };

    this.selectDropDownDateOfBirthMonth = function (monthIndex) {
        myRyanairPage.dropDownDateOfBirthMonth(monthIndex).click();
    };

    this.selectDropDownDateOfBirthYear = function (yearIndex) {
        myRyanairPage.dropDownDateOfBirthYear(yearIndex).click();
    };

    this.selectDropDownNationality = function () {
        myRyanairPage.dropDownSelectNationality(97).click();
    };

    this.selectRandomDropDownNationality = function () {
        myRyanairPage.dropDownSelectNationality(actions.tripHelper.getRandomNumber(2, 28)).click();
    };

    this.clickBtnSaveCompanion = function () {
        myRyanairPage.btnSaveCompanion().click();
    };

    this.clickBtnRemoveCompanion = function () {
        myRyanairPage.btnRemoveCompanion().click();
    };

    this.returnMyRyanairProgressinPercentage = function () {
        return myRyanairPage.myRyanairProgressinPercentage().getText();
    };

    this.assertOnMyRyanairPercentage = function (progressPercentage) {
        expect(myRyanairPage.myRyanairProgressinPercentage().getText()).toEqual(progressPercentage);
    };

    this.assertOnMyRyanairPercentageIsGreater = function (progressPercentage) {
        expect(myRyanairPage.myRyanairProgressinPercentage().getText()).toBeGreaterThan(progressPercentage);
    };

    this.verifyAddDocumentButtonIsPresent = function () {
        expect(myRyanairPage.btnAddTravelDoc().isPresent()).toBe(true);

        myRyanairPage.btnAddTravelDoc().getText().then(function (text) {
            reporter.addMessageToSpec("Button txt = " + text)
        });
    };

    // payment tab actions

    this.clickPaymentsTab = function () {
        myRyanairPage.paymentsTab().click();
    };
    this.assertOnPaymentView = function () {
        expect(myRyanairPage.paymentView().isPresent()).toBe(true);
    };

    this.clickPaymentMethod = function () {
        myRyanairPage.paymentMethod().click();
    };

    this.assertExistingDefaultPayment = function () {
        expect(myRyanairPage.starDefaultPayment().isPresent()).toBe(true);
    };

    this.assertOnPaymentsNumber = function (numberOfPayments) {
        expect(myRyanairPage.paymentsCardNumber().count()).toBe(numberOfPayments);
    };

    this.clickPaymentMethod = function () {
        myRyanairPage.paymentMethod().click();
    };

    this.assertOnLoginForm = function () {
        expect(myRyanairPage.filedPaymentTabSecurityPassword().isPresent()).toBe(true);
    };

    this.assertOnCardType = function (cardType,cardPrefix) {
        myRyanairPage.fieldCardNumber().sendKeys(cardPrefix);
        var dropDownCardTypeList = myRyanairPage.dropDownCardTypeList();
        for(var i=1; i < dropDownCardTypeList.length; i++) {
            expect(dropDownCardTypeList[i].getText()).toContain(cardType);
        }
    };

    this.addNewPayment = function (cardType,cardNumber,cardHolderName) {
        myRyanairPage.addPayment().click();
        this.clickPaymentMethod();
        this.enterPaymentDetails(cardType,cardNumber,cardHolderName);
        this.enterBillingAddress();
        this.confirmDetails();
    };

    this.enterPaymentDetails = function (cardType,cardNumber,cardHolderName) {
        myRyanairPage.dropDownCardType(cardType).click();
        myRyanairPage.fieldCardNumber().sendKeys(cardNumber);
        myRyanairPage.dropDownExpiryYear(3).click();
        myRyanairPage.dropDownExpiryMonth(1).click();
        myRyanairPage.fieldCardholderName().sendKeys(cardHolderName);
        myRyanairPage.dropDownCardUsage(1).click();
        myRyanairPage.btnContinue().click();
    };

    this.editPaymentDetails = function (cardNumber,cardType) {
        myRyanairPage.editPayment().click();
        myRyanairPage.fieldCardNumber().clear();
        myRyanairPage.fieldCardNumber().sendKeys(cardNumber);
        myRyanairPage.dropDownCardType(cardType).click();
        myRyanairPage.btnSavePaymentDetails().click();
    };

    this.assertOnAddedCardNumber = function () {
        expect(myRyanairPage.addedCardNumber().getText()).toContain("1198");
    };

    this.enterBillingAddress = function () {
        myRyanairPage.dropDownBillingAddressCountry(17).click();
        myRyanairPage.fieldBillingAddressCity().sendKeys("Dublin");
        myRyanairPage.fieldBillingAddressPostcode().sendKeys("D4");
        myRyanairPage.fieldBillingAddressLine1().sendKeys("Ryanair Hq");
        myRyanairPage.fieldBillingAddressLine2().sendKeys("Swords");
        myRyanairPage.btnBillingAddressContinue().click();
    };

    this.enterCustomBillingAddress = function (country, city, postCode, AddressLine1, AddressLine2) {
        myRyanairPage.dropDownBillingAddressCountry(country).click();
        myRyanairPage.fieldBillingAddressCity().sendKeys(city);
        myRyanairPage.fieldBillingAddressPostcode().sendKeys(postCode);
        myRyanairPage.fieldBillingAddressLine1().sendKeys(AddressLine1);
        myRyanairPage.fieldBillingAddressLine2().sendKeys(AddressLine2);
        myRyanairPage.btnBillingAddressContinue().click();
    };

    this.confirmDetails = function () {
        myRyanairPage.btnConfirmDetailsComplete().click();
    };

    this.enterFiledPaymentTabSecurityPassword = function (password) {
        myRyanairPage.filedPaymentTabSecurityPassword().sendKeys(password);
        myRyanairPage.btnOkPaymentTabSecurityPassword().click();
    };

    this.clickBtnRemoveSavedPaymentDetails = function () {
        browser.wait(EC.presenceOf(myRyanairPage.btnRemoveSavedPaymentDetails()), 5000);
        myRyanairPage.btnRemoveSavedPaymentDetails().click();
        browser.wait(EC.presenceOf(myRyanairPage.btnConfirmRemove()), 5000);
        myRyanairPage.btnConfirmRemove().click();
    };

    this.assertOnFormForgotPassword = function () {
        expect(myRyanairPage.formForgotPassword().isPresent()).toBe(true);
    };

    this.assertOnDeletingDefaultPayment = function () {
        expect(myRyanairPage.starDefaultPayment().isPresent()).toBe(true);
        var newDefaultPaymentLastDigits = myRyanairPage.paymentsCardNumber().then(function (cardsNumbers) {
            return cardsNumbers[1].getText().then( function (cardNumber) {
                return  cardNumber.substring(cardNumber.length - 14, cardNumber.length - 10);
            });
        });
        myRyanairPage.btnRemoveSavedPaymentDetails().click();
        expect(myRyanairPage.accountPaymentConfirmDeleteDefaultMessage().isPresent()).toBe(true);
        var firstPaymentInListLastDigits = myRyanairPage.dropDownNewDefaultPayment(1).getText().then(function (cardNumber) {
            return cardNumber.substring(cardNumber.length - 4, cardNumber.length);
        });
        expect(firstPaymentInListLastDigits).toEqual(newDefaultPaymentLastDigits);
        expect(myRyanairPage.btnConfirmRemoveDefaultPayment().isPresent()).toBe(true);
        myRyanairPage.btnConfirmRemoveDefaultPayment().click();
        expect(myRyanairPage.starDefaultPayment().isPresent()).toBe(true);
        var firstPaymentCardNumberLastDigits = myRyanairPage.paymentsCardNumber().then(function (cardsNumbers) {
            return cardsNumbers[0].getText().then( function (cardNumber) {
                return  cardNumber.substring(cardNumber.length - 14, cardNumber.length - 10);
            });
        });
        expect(firstPaymentCardNumberLastDigits).toEqual(newDefaultPaymentLastDigits);
    };

    this.assertOnChangeDefaultPayment = function (cardType, newDefaultCardNumber, newDefaultPayment) {
        expect(myRyanairPage.starDefaultPayment().isPresent()).toBe(true);
        var newDefaultPaymentLastDigits = myRyanairPage.paymentsCardNumber().then(function (cardsNumbers) {
            return cardsNumbers[newDefaultPayment].getText().then( function (cardNumber) {
                return  cardNumber.substring(cardNumber.length - 14, cardNumber.length - 10);
            });
        });
        expect(newDefaultCardNumber.substring(12,16)).toEqual(newDefaultPaymentLastDigits);
        myRyanairPage.editPayment().then(function (editButtons) {
            editButtons[newDefaultPayment].click();
        });
        expect(myRyanairPage.fieldCardNumber().isPresent()).toBe(true);
        myRyanairPage.setDefaultPayment().click();
        myRyanairPage.fieldCardNumber().clear();
        myRyanairPage.fieldCardNumber().sendKeys(newDefaultCardNumber);
        myRyanairPage.dropDownCardType(cardType).click();
        myRyanairPage.btnSavePaymentDetails().click();
        var currentDefaultPaymentLastDigits = myRyanairPage.paymentsCardNumber().then(function (cardsNumbers) {
            return cardsNumbers[0].getText().then(function (cardNumber) {
                return cardNumber.substring(cardNumber.length - 14, cardNumber.length - 10);
            });
        });
        expect(currentDefaultPaymentLastDigits).toEqual(newDefaultPaymentLastDigits);
    };

    this.clickOnPassengersTab = function () {
        myRyanairPage.btnPassengersMyRyanair().click();
    };

    this.clickOnEditCompanionsField = function () {
        myRyanairPage.btnEditCompanion().click();
    };

    this.clickOnRemoveCompanionsTravelDoc = function () {
        myRyanairPage.btnRemoveCompanionTravelDoc().click();
    };

    this.clickOnEditPassportDetails = function () {
        myRyanairPage.btnEditPassportDetails().click();
    };

    this.clickSaveCompanionField = function () {
        myRyanairPage.saveCompanionField().click();
    };

    this.addTravelDocument = function () {
        myRyanairPage.btnAddTravelDoc().click();
    };

    this.addTravelDocumentFromDashboard = function () {
        myRyanairPage.dashboardTabAddTravelDoc().click();
        myRyanairPage.profileTabAddDoc().click();
    };
    this.assertDocumentExpiryDateIsNotPresent = function () {
        expect(myRyanairPage.dropDownDocumentExpiryDate().isPresent()).toBe(false);
    };

    this.assertOnFavouritePassengerList = function () {
        expect(myRyanairPage.passengerFavouriteNameList().first().getText()).toContain("Evalyn Johnson");
        expect(myRyanairPage.passengerFavouriteNameList().last().getText()).toContain("Ivy Swaniawski");
    };

    this.selectDropDownNationalityDoc = function (option) {
        myRyanairPage.dropDownNationalityDoc(option).click();
    };
    this.profileSelectDropDownNationalityDoc = function (option) {
        myRyanairPage.profileDropDownNationalityDoc(option).click();
    };

    this.selectProfileDropDownNationalityDoc = function (option) {
        myRyanairPage.profileDropDownNationalityDoc(option).click();
    };

    this.selectDropDownDocumentType = function (option) {
        myRyanairPage.dropDownDocumentType(option).click();
    };
    this.profileSelectDropDownDocumentType = function (option) {
        myRyanairPage.profileDropDownDocumentType(option).click();
    };

    this.assertDropDownDocumentType = function (option) {
        expect(myRyanairPage.selectDropDocumentType().count()).toBe(option);
    };

    this.assertProfileDropDownDocumentType = function (option) {
        expect(myRyanairPage.selectProfileDropDocumentType().count()).toBe(option);
    };

    this.selectDropDownExpiryDate = function () {
        myRyanairPage.dropDownDocumentExpiryDay();
    };

    this.selectDropDownExpiryMonth = function () {
        myRyanairPage.dropDownDocumentExpiryMonth();
    };

    this.selectDropDownExpiryYear = function () {
        myRyanairPage.dropDownDocumentExpiryYear();
    };

    this.inputRandomDocumentNumber = function () {
        myRyanairPage.fieldCompanionDocumentNumber().clear();
        myRyanairPage.fieldCompanionDocumentNumber().sendKeys(actions.tripHelper.getRandomDocumentNumber());
    };

    this.profileInputRandomDocumentNumber = function () {
        myRyanairPage.profileDocumentNumber().clear();
        myRyanairPage.profileDocumentNumber().sendKeys(actions.tripHelper.getRandomDocumentNumber());
    };

    this.selectDateOfBirth = function () {
        myRyanairPage.dropDownDateOfBirth(actions.tripHelper.getRandomNumber(5, 28)).click();
        myRyanairPage.dropDownMonthOfBirth(actions.tripHelper.getRandomNumber(2, 12)).click();
        myRyanairPage.dropDownYearOfBirth(actions.tripHelper.getRandomNumber(2, 28)).click();
    };

    this.selectDocumentExpiryDate = function () {
        myRyanairPage.dropDownDocumentExpiryDay(actions.tripHelper.getRandomNumber(5, 26)).click();
        myRyanairPage.dropDownDocumentExpiryMonth(actions.tripHelper.getRandomNumber(3, 12)).click();
        myRyanairPage.dropDownDocumentExpiryYear(actions.tripHelper.getRandomNumber(3, 10)).click();
        myRyanairPage.dropDownDocumentCountryOfIssue(actions.tripHelper.getRandomNumber(2, 28)).click();
    };

    this.profileSelectDocumentExpiryDate = function () {
        myRyanairPage.profileDropDownDocumentExpiryDay(actions.tripHelper.getRandomNumber(5, 26)).click();
        myRyanairPage.profileDocumentExpiryMonth(actions.tripHelper.getRandomNumber(3, 12)).click();
        myRyanairPage.profileDocumentExpiryYear(actions.tripHelper.getRandomNumber(3, 10)).click();
        myRyanairPage.profileDocumentCountryOfIssue(actions.tripHelper.getRandomNumber(2, 28)).click();

    };

    this.profileClickAddButton = function () {
        myRyanairPage.profileBtnAdd().click();
    };

    this.selectDropDownCountryOfIssue = function () {
        myRyanairPage.dropDownDocumentCountryOfIssue(actions.tripHelper.getRandomNumber(2, 28)).click();
    };

    this.profileSelectDropDownCountryOfIssue = function () {
        myRyanairPage.profileDocumentCountryOfIssue(actions.tripHelper.getRandomNumber(2, 28)).click();
    };

    //Preferences Actions
    this.openPreferences = function () {
        browser.wait(EC.elementToBeClickable(myRyanairPage.preferencesTab()), 5000);
        myRyanairPage.preferencesTab().click();
    };

    this.openUpcomingTripPreferences = function () {
        myRyanairPage.editUpcomingTripPreferences().click();
        browser.wait(EC.presenceOf(myRyanairPage.btnEditPreferencesBottom()), 5000);
        myRyanairPage.btnEditPreferencesBottom().click();
    };

    this.validateSavedPreference = function (index, answer) {
        expect(myRyanairPage.lblSavedAnswers().get(index).getText()).toContain(answer);

        myRyanairPage.lblSavedAnswers().get(index).getText().then(function (text) {
            index = index + 1;
            reporter.addMessageToSpec("Expected Answer " + index + ": " + answer + ", Saved Answer " + index + ": " + text);
        });
    };

    this.modifyUpcomingTripPreferences = function () {
        myRyanairPage.upcomingQuestion1Answer3().click();
        myRyanairPage.upcomingQuestion2Answer4().click();
        myRyanairPage.yesNoQuestionAnswerNo().click();
        myRyanairPage.yesNoQuestionAnswerNo().click();
        myRyanairPage.btnClosePreferences().click();
    };

    this.revertUpcomingTripPreferences = function () {
        myRyanairPage.upcomingQuestion1Answer1().click();
        myRyanairPage.upcomingQuestion2Answer1().click();
        myRyanairPage.yesNoQuestionAnswerYes().click();
        myRyanairPage.yesNoQuestionAnswerYes().click();
        myRyanairPage.btnClosePreferences().click();
    };

    this.fillAddCompanionFields = function () {
        actions.myRyanairActions.clickTypeFriendCompanion();
        actions.myRyanairActions.selectDropDownTitle("Mr");
        actions.myRyanairActions.inputFieldFirstName(Faker.name.firstName());
        actions.myRyanairActions.inputFieldLastName(Faker.name.lastName());
        actions.myRyanairActions.selectDropDownDateOfBirthDay(10);
        actions.myRyanairActions.selectDropDownDateOfBirthMonth(2);
        actions.myRyanairActions.selectDropDownDateOfBirthYear(27);
        actions.myRyanairActions.selectDropDownNationality("Irish");
        actions.myRyanairActions.clickBtnSaveCompanion();
    };

    this.editProfileFields = function () {
        actions.myRyanairActions.selectDropDownTitle("Mr");
        actions.myRyanairActions.inputFieldFirstName(Faker.name.firstName());
        actions.myRyanairActions.inputFieldLastName(Faker.name.lastName());
        actions.myRyanairActions.selectDropDownDateOfBirthDay(actions.tripHelper.getRandomNumber(2, 28));
        actions.myRyanairActions.selectDropDownDateOfBirthMonth(actions.tripHelper.getRandomNumber(2, 11));
        actions.myRyanairActions.selectDropDownDateOfBirthYear(actions.tripHelper.getRandomNumber(19, 40));
        actions.myRyanairActions.selectCallingCodeField();
        actions.myRyanairActions.inputPhoneNumber();
        actions.myRyanairActions.selectRandomDropDownNationality();
        actions.myRyanairActions.clickSaveProfile();
    };

    this.fillAddCompanionFieldsUnsaved = function () {
        actions.myRyanairActions.clickTypeFriendCompanion();
        actions.myRyanairActions.selectDropDownTitle("Mr");
        actions.myRyanairActions.inputFieldFirstName(Faker.name.firstName());
        actions.myRyanairActions.inputFieldLastName(Faker.name.lastName());
        actions.myRyanairActions.selectDropDownDateOfBirthDay(10);
        actions.myRyanairActions.selectDropDownDateOfBirthMonth(2);
        actions.myRyanairActions.selectDropDownDateOfBirthYear(27);
        actions.myRyanairActions.selectDropDownNationality("British");
    };

    this.fillAddRandomCompanionFields = function () {
        actions.myRyanairActions.selectDropDownTitle("Mr");
        actions.myRyanairActions.inputFieldFirstName(Faker.name.firstName());
        actions.myRyanairActions.inputFieldLastName(Faker.name.lastName());
        actions.myRyanairActions.selectDropDownDateOfBirthDay(actions.tripHelper.getRandomNumber(2, 28));
        actions.myRyanairActions.selectDropDownDateOfBirthMonth(actions.tripHelper.getRandomNumber(2, 11));
        actions.myRyanairActions.selectDropDownDateOfBirthYear(actions.tripHelper.getRandomNumber(19, 40));
        actions.myRyanairActions.selectRandomDropDownNationality();
    };

    this.fillAddAllCompanionFields = function (numberOfCompanions) {
        for (var i = 0; i < numberOfCompanions; i++) {
            actions.myRyanairActions.fillAddCompanionFields();
            if (i === 9) {
                console.log("i = 9");
            }
            else {
                actions.myRyanairActions.clickBtnAddCompanionsCompanionPanel();
            }
        }
    };

    this.removeAllCompanionFields = function (numberOfCompanion) {
        for (var i = 0; i < numberOfCompanion; i++) {
            actions.myRyanairActions.clickPassengersTab();
            actions.myRyanairActions.clickBtnRemoveCompanion();
        }
    };

    this.removeAllCompanions = function () {
        myRyanairPage.listBtnRemoveCompanion().count().then(function (numberOfCompanions) {
            console.log("Number of Companions: " + numberOfCompanions);
            for (var i = 0; i < numberOfCompanions; i++) {
                actions.myRyanairActions.clickBtnRemoveCompanion();
            }
        });
    };

    this.assertAddCompanionsDisabled = function () {
        myRyanairPage.addCompanionsDisabled().getAttribute("disabled").then(function (value) {
            expect(value).toBe("true");
            reporter.addMessageToSpec("disabled: " + value);
        });
    };

    this.addSpecificPaxTypeCompanion = function (paxType, yearIndex) {
        actions.myRyanairActions.clickTypeFriendCompanion();
        actions.myRyanairActions.inputFieldFirstName(Faker.name.firstName());
        actions.myRyanairActions.inputFieldLastName(Faker.name.lastName());
        actions.myRyanairActions.selectDropDownDateOfBirthDay(2);
        actions.myRyanairActions.selectDropDownDateOfBirthMonth(2);
        actions.myRyanairActions.selectDropDownDateOfBirthYear(yearIndex);
        actions.myRyanairActions.selectDropDownNationality("Irish");

        switch (paxType) {
            case "adult":
                expect(myRyanairPage.adultPaxTypeLabel().isPresent()).toBe(true);
                myRyanairPage.adultPaxTypeLabel().getText().then(function (text) {
                    reporter.addMessageToSpec("Adult Pax Text label: " + text);
                });
                expect(myRyanairPage.dropDownTitleOptions().count()).toBe(4);
                myRyanairPage.dropDownTitleIndexOption(1).click();
                break;
            case "teen":
                expect(myRyanairPage.teenPaxTypeLabel().isPresent()).toBe(true);
                myRyanairPage.teenPaxTypeLabel().getText().then(function (text) {
                    reporter.addMessageToSpec("Teen Pax Text label: " + text);
                });
                expect(myRyanairPage.dropDownTitleOptions().count()).toBe(3);
                myRyanairPage.dropDownTitleIndexOption(1).click();
                break;
            case "child":
                expect(myRyanairPage.childPaxTypeLabel().isPresent()).toBe(true);
                myRyanairPage.childPaxTypeLabel().getText().then(function (text) {
                    reporter.addMessageToSpec("Child Pax Text label: " + text);
                });
                expect(myRyanairPage.dropDownTitleOptions().count()).toBe(2);
                myRyanairPage.dropDownTitleIndexOption(1).click();
                break;
            case "infant":
                expect(myRyanairPage.infantPaxTypeLabel().isPresent()).toBe(true);
                myRyanairPage.infantPaxTypeLabel().getText().then(function (text) {
                    reporter.addMessageToSpec("Infant Pax Text label: " + text);
                });
                expect(myRyanairPage.dropDownTitleSelectOnly().isDisplayed()).toBeFalsy();
                break;
            default:
                console.log("Invalid Pax Type argument specified.");
        }
        actions.myRyanairActions.clickBtnSaveCompanion();
    };

    this.clickShowAdvancedPreferences = function () {
        myRyanairPage.btnshowAdvancedPreferences().click();
    }

//Send Preferences Question and it will click the Complete/Change based on your question
    this.clickPreferenceChangeorComplete = function (whichQuestion) {
        myRyanairPage.btnPreferencesQuestionChangeComplete(whichQuestion).click();
    }

    this.clickPreferenceAnswer = function (selectAnswer, blnNotHighlitedOne) {
        myRyanairPage.btnPreferenceSelectAnswer(selectAnswer, blnNotHighlitedOne).click();
    }

    this.clickPreferenceSaveAndClose = function () {
        myRyanairPage.btnPreferenceSaveAndClose().click();
    };

    this.getPreferenceAnswer = function (whichQuestion) {
        return myRyanairPage.lblPreferenceAnswer(whichQuestion).getAttribute('innerHTML').then(function (txt) {
            return txt;
        });
    };

    this.verifyPreferenceAnswers = function (previousAnswer, currentAnswer) {
        expect(previousAnswer).not.toBe(currentAnswer);
    };

    this.clickPlanningATripPreference = function () {
        myRyanairPage.btnPlanningATripPreference().click();
    };

    this.clickEditPreferences = function () {
        myRyanairPage.btnEditPreferences().click();
    };

    this.answerAllQuestions = function () {
        for (var i = 0; i < 100; i++) {
            //console.log(i);
            myRyanairPage.questionType().getAttribute("question-type").then(function (questionType) {
                    //console.log("questionType"+questionType);
                    if (questionType === "MULTIPLE_SELECTION") {
                        browser.wait(EC.presenceOf(myRyanairPage.answerOptions().get(0)), 5000);
                        myRyanairPage.answerOptions().get(0).click().
                            then(function () {
                                return myRyanairPage.answerOptions().get(1).click();
                            }).then(function () {
                                return browser.sleep(1000);
                            }).
                            then(function () {
                                browser.wait(EC.presenceOf(myRyanairPage.saveAnswer()), 5000);
                                return myRyanairPage.saveAnswer().click();
                            }).then(function () {
                                return browser.sleep(1000);
                            });
                    }
                    else if (questionType === "YES_NO") {
                        browser.wait(EC.presenceOf(myRyanairPage.answerYesNo().get(0)), 5000);
                        myRyanairPage.answerYesNo().get(0).click().then(function () {
                            return browser.sleep(2000);
                        });
                    }
                    else if (questionType === "RATING") {
                        browser.wait(EC.presenceOf(myRyanairPage.answerStars().get(2)), 5000);
                        myRyanairPage.answerStars().get(2).click().
                            then(function () {
                                return browser.sleep(1000);
                            }).
                            then(function () {
                                myRyanairPage.saveAnswer().click();
                            })
                            .then(function () {
                                return browser.sleep(1000);
                            });
                    }
                    else if (questionType === "MULTIPLE_CHOICE") {
                        browser.wait(EC.presenceOf(myRyanairPage.answerOptions().get(0)), 5000);
                        myRyanairPage.answerOptions().get(0).click().then(function () {
                            return browser.sleep(1000);
                        });
                    }
                    else if (questionType === "COMPLETED") {
                        //console.log("Qunestions completed already");
                        expect(myRyanairPage.answeredAllQuestionsMessage().isDisplayed()).toBe(true);
                    }
                }
            );
        }


    };

    this.addUserInfo = function () {
        actions.myRyanairActions.inputFieldFirstName("Myryanair");
        actions.myRyanairActions.inputFieldLastName("Tester");

    };


    this.clickSaveUserInfoContinue = function () {
        myRyanairPage.btnSaveContinue().click();
    };


    this.verifyPreferenceAfterAnsweringQuestions = function () {
        myRyanairPage.btnshowAdvancedPreferences().click();
        browser.executeScript('arguments[0].scrollIntoView()', myRyanairPage.changePreferenceLinks().get(0).getWebElement());
        myRyanairPage.changePreferenceLinks().count().then(function (count) {
            console.log("count:" + count);
            expect(count > 0);
        });
    };

    this.clickUnlockedRewardCardFromPreference = function () {

        myRyanairPage.rewardSectionCompletedPreference().click();
        expect(myRyanairPage.rewardTabRewardMessage().isDisplayed()).toBe(true);
    };

    this.verify4CardsInDashboard = function () {
        expect(myRyanairPage.profileCard().isDisplayed()).toBe(true);
        expect(myRyanairPage.questionCard().isDisplayed()).toBe(true);
        expect(myRyanairPage.progressCard().isDisplayed()).toBe(true);
        expect(myRyanairPage.rewardsCard().isDisplayed()).toBe(true);
    };

    this.verifyCheckListStatusKeyDetails = function (checked) {
        if (checked) {
            expect(myRyanairPage.tickIconKeyDetails().isDisplayed()).toBe(true);
        }
        else {
            expect(myRyanairPage.crossIconKeyDetails().isDisplayed()).toBe(true);
        }

    };

    this.verifyCheckListStatusPhoneNumber = function (checked) {
        if (checked) {
            expect(myRyanairPage.tickIconPhoneNumber().isDisplayed()).toBe(true);
        }
        else {
            expect(myRyanairPage.crossIconPhoneNumber().isDisplayed()).toBe(true);
        }

    };

    this.verifyCheckListStatusTravelDocument = function (checked) {
        if (checked) {
            expect(myRyanairPage.tickIconTravelDocument().isDisplayed()).toBe(true);
        }
        else {
            expect(myRyanairPage.crossIconTravelDocument().isDisplayed()).toBe(true);
        }

    };

    this.keepBuildProfileByAddingPhone = function () {
        myRyanairPage.keepBuildMyProfile().click();
        myRyanairPage.editProfileButton().click();
        actions.myRyanairActions.selectCallingCodeField();
        actions.myRyanairActions.inputPhoneNumber();
        actions.myRyanairActions.clickSaveProfile();
    };

    this.clickPlanningATripPreference = function () {
        myRyanairPage.btnPlanningATripPreference().click();
    }

    this.clickEditPreferences = function () {
        myRyanairPage.btnEditPreferences().click();
    }

    this.getEditPreferencesCurrentQuestion = function () {
        return myRyanairPage.lblEditPreferencesCurrentQuestion().getAttribute('innerHTML').then(function (txt) {
            return txt;
        });
    }

    this.getEditPreferencesActiveQuestionOnHeader = function () {
        return myRyanairPage.lblEditPreferencesActiveQuestionOnHeader().getAttribute('innerHTML').then(function (txt) {
            return txt;
        });
    }

    this.verifyPreferenceQuestionsAreSame = function (headerQuestion, currentQuestion) {
        reporter.addMessageToSpec("Header question is : " + headerQuestion + " and Current question is : " + currentQuestion);
        expect(headerQuestion).toBe(currentQuestion);
    }

    this.clickHolidayEditPreferences = function () {
        myRyanairPage.btnHolidayEditPreferences().click();
    };

    this.clickNextPreference = function () {
        myRyanairPage.btnNextPreference().click();
    };

    this.clickPreferenceStar = function (starNumber) {
        var currStarNumber = 1;
        if (starNumber === 0) {
            currStarNumber = actions.tripHelper.getRandomNumber(0, 4);
        } else {
            currStarNumber = starNumber - 1;
        }
        myRyanairPage.btnPreferenceStar().get(currStarNumber).click();
    };

    this.validatePreferencesHolidayQuestionsComplete = function (completeMessage) {
        expect(myRyanairPage.lblPreferenceHolidayQuestionsCompleted().getText()).toContain(completeMessage);
    };

    this.verifyProfileProgressInPercentage = function (percentage) {
        myRyanairPage.dashboardTabPercentage(percentage).getText().then(function (percentageValue) {
            expect(percentageValue).toBe(percentage);
            console.log("percentageValue = " + percentageValue);
        });
    };


};

module.exports = MyRyanairActions;



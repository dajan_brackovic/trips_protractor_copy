var Pages = require('../../Pages')
var pages = new Pages();
var params = browser.params.conf;


var potentialTripActions = function () {

    var potentialTripPage = pages.potentialTripPage;
    var pt = pages.lowCostParkingPage;
    var EC = protractor.ExpectedConditions;


    this.modifyButtonCheckInsurance = function () {
        expect(potentialTripPage.clickAddInsurance().getText()).toEqual("Modify");
    };
    this.expectInformationLabelOnCardInsurance = function (labelText) {
        expect(potentialTripPage.informationLabelOnCardInsurance().getText()).toEqual(labelText);
    };
    this.expectParkingCardIsNotDisabled = function() {
        browser.wait(EC.presenceOf(potentialTripPage.openParkingCard()),5000);
        expect(potentialTripPage.openParkingCard().isPresent()).toBe(true);
    };
    this.checkAddToTripButtonCheckParking = function () {
        browser.wait(EC.presenceOf(potentialTripPage.openParkingCard()), 5000);
        expect(potentialTripPage.openParkingCard().isPresent()).toBe(true);
    };
    this.clickOpenSamsoniteInNewTab = function () {
        browser.wait(EC.presenceOf(potentialTripPage.openSamsoniteBags()), 5000);
        pages.potentialTripPage.openSamsoniteBags().click();
        browser.sleep(1000);
        //TODO Page no longer opens in new tab but leaving this code here in case they revert to New tab
        //browser.getAllWindowHandles().then(function (handles) {
        //    var secondWindowHandle = handles[1];
        //    var firstWindowHandle = handles[0];
        //    //the focus moves on the newest opened tab
        //    browser.switchTo().window(secondWindowHandle).then(function () {
        //        browser.sleep(1000);
        //        //check if the right page is opened
        //        expect(browser.driver.getCurrentUrl()).toContain("samsonite");
        //        browser.switchTo().window(firstWindowHandle).then(function () {
        //            browser.driver.close();
        //        });
        //        browser.switchTo().window(secondWindowHandle)
        //    });
        //});
    };
    this.modifyButtonAirportTransfers = function(){
        expect((potentialTripPage.openAirportTransfersCard()).getText()).toEqual('Modify');
    };
    this.expectInformationLabelOnCardMusic = function (labelText) {
        expect(potentialTripPage.informationLabelOnCardMusic().getText()).toEqual(labelText);
    };
    this.modifyButtonCheckMusic = function () {
        expect(potentialTripPage.openMusicEquipmentCard().getText()).toEqual("Modify");
    };
    this.expectTotalPriceBrakeDown = function (amount) {
        expect(potentialTripPage.totalPriceBreakDownOpened().getText()).toContain(amount);
    };
    this.checkAddToTripButtonCheckMusic = function () {
        expect(potentialTripPage.openMusicEquipmentCard().getText()).toEqual("Add to Trip");
    };
    this.clickAddParking = function () {
        browser.wait(EC.presenceOf(potentialTripPage.openParkingCard()), 5000);
    //    browser.executeScript('arguments[0].scrollIntoView()', potentialTripPage.openParkingCard().getWebElement());
        potentialTripPage.openParkingCard().click();
        browser.sleep(2000);
    };
    this.isInsuranceIconDisplayed = function () {
        expect(potentialTripPage.insuranceIcon().isDisplayed()).toBeTruthy();
    };
    this.expectAddInsuranceButtonIsDisplayed = function () {
        expect(potentialTripPage.openInsuranceCard().isDisplayed()).toBeTruthy();
    };
    this.isPotentionalTravelTitleDisplayed = function () {
        expect(potentialTripPage.potentionalTravelTitle().isDisplayed()).toBeTruthy();
    };
    this.clickAddBabyEquipment = function () {
        browser.wait(EC.presenceOf(potentialTripPage.openBabyEquipmentCard()), 5000);
        browser.executeScript('arguments[0].scrollIntoView()', potentialTripPage.openBabyEquipmentCard().getWebElement());
        potentialTripPage.openBabyEquipmentCard().click();
    };
    this.clickAddMusicEquipment = function () {
        browser.wait(EC.presenceOf(potentialTripPage.openMusicEquipmentCard()), 5000);
        browser.executeScript('arguments[0].scrollIntoView()', potentialTripPage.openMusicEquipmentCard().getWebElement());
        potentialTripPage.openMusicEquipmentCard().click();
        browser.sleep(2000);
    };
    this.clickAddInsurance = function () {
        browser.wait(EC.presenceOf(potentialTripPage.openInsuranceCard()), 5000);
        potentialTripPage.openInsuranceCard().click();
    };
    this.expectProductCardUpdatedAirportTransfers = function (indexOfItem, numberOFRows, indexOfRepeater, nameOfProduct) {
        expect(potentialTripPage.priceBreakDownCartItem().count()).toEqual(numberOFRows);
        expect(potentialTripPage.priceBreakDownCartItemPriceAirportTransfers(indexOfRepeater).getText()).toMatch(params.regEx.price);
        expect(potentialTripPage.priceBreakDownCartItemNameAirportTransfers(indexOfItem).getText()).toEqual(nameOfProduct);
        expect(potentialTripPage.priceTotalCost().getText()).toContain(potentialTripPage.totalPriceBreakDownOpened().getText());
    };
    this.clickAddSportEquipment = function () {
        browser.wait(EC.presenceOf(potentialTripPage.openSportEquipmentCard()), 5000);
        browser.executeScript('arguments[0].scrollIntoView()', potentialTripPage.openSportEquipmentCard().getWebElement());
        potentialTripPage.openSportEquipmentCard().click();
        browser.sleep(2000);
    };
    this.modifyButtonCheckSport = function(){
        expect(potentialTripPage.openSportEquipmentCard().getText()).toEqual("Modify");
    };
    this.modifyButtonCheckBaby = function(){
        expect(potentialTripPage.openBabyEquipmentCard().getText()).toEqual("Modify");
    };
    this.modifyButtonCheckMusic = function(){
        expect(potentialTripPage.openMusicEquipmentCard().getText()).toEqual("Modify");
    };
    this.modifyButtonCheckInsurance = function(){
        expect(potentialTripPage.openInsuranceCard().getText()).toEqual("Modify");
    };
    this.modifyButtonCheckParking = function(){
        expect(potentialTripPage.openParkingCard().getText()).toEqual("Modify");
    };
    this.expectInformationLabelOnCard = function (labelText) {
        expect(potentialTripPage.informationLabelOnCard().getText()).toEqual(labelText);
    };


    this.expectInformationLabelOnCardInsurance = function (labelText) {
        expect(potentialTripPage.informationLabelOnCardInsurance().getText()).toEqual(labelText);
    };


    this.expectInformationLabelOnCardMusic = function (labelText) {
        expect(potentialTripPage.informationLabelOnCardMusic().getText()).toEqual(labelText);
    };

    this.expectInformationLabelOnCardParking = function (labelText) {
        expect(potentialTripPage.informationLabelOnCardParking().getText()).toEqual(labelText);
    };
    this.expectInformationLabelOnCardBaby = function(labelText){
        expect(potentialTripPage.informationLabelOnCardBaby().getText()).toEqual(labelText);
    };
    this.expectProductCardUpdated = function (indexOfItem, numberOFRows, priceOfProduct, nameOfProduct) {
        expect(potentialTripPage.priceBreakDownCartItem().count()).toEqual(numberOFRows);
        actions.priceBreakDownActions.assertOnListOfSelectedItemsInBold(nameOfProduct);
        expect(potentialTripPage.priceBreakDownCartItemPrice(indexOfItem).getText()).toContain(priceOfProduct);
       // expect(potentialTripPage.priceTotalCost().getText()).toContain(potentialTripPage.totalPriceBreakDownOpened().getText());
    };
    this.expectProductCardUpdatedInsurance = function (indexOfItem, numberOFRows, nameOfProduct) {
        expect(potentialTripPage.priceBreakDownCartItem().count()).toEqual(numberOFRows);
        actions.priceBreakDownActions.assertOnListOfSelectedItemsInBold(nameOfProduct);
        expect(potentialTripPage.priceBreakDownCartItemPrice(indexOfItem).getText().then(function (text) {
            return text.substring(2);
        })).toMatch(params.regEx.price);
        //expect(potentialTripPage.priceTotalCost().getText()).toContain(potentialTripPage.totalPriceBreakDownOpened().getText());
    };
    this.expectProductCardUpdatedParking = function (indexOfItem, numberOFRows, nameOfProduct) {
        expect(potentialTripPage.priceBreakDownCartItem().count()).toEqual(numberOFRows);
        expect(potentialTripPage.priceBreakDownCartItemPrice(indexOfItem).getText().then(function (text) {
            return text.substring(2);
        })).toMatch(params.regEx.price);
        expect(potentialTripPage.priceBreakDownCartItemNameInsurance(indexOfItem).getText()).toEqual(nameOfProduct);
        expect(potentialTripPage.priceTotalCost().getText()).toContain(potentialTripPage.totalPriceBreakDownOpened().getText());
    };
    this.expectPopUpBasketCart = function () {
        expect(potentialTripPage.popUpBasketCart().isPresent()).toBeTruthy();
    };
    this.openPriceBreakDownClick = function () {
        potentialTripPage.arrowPriceBreakDown().click();
    };
    this.expectInformationMessageAddedPriceShown = function () {
        expect((potentialTripPage.informationMessageAddedPriceAnythingElse()).isPresent()).toBeTruthy();
    };
    this.expectInformationMessageAddedPriceHidden = function () {
        expect(potentialTripPage.informationMessageAddedPriceAnythingElse().IsPresent()).not.toBeTruthy();
    };
    this.expectInformationMessageAddedPriceMessageText = function (informationMessage) {
        expect(potentialTripPage.informationMessageAddedPriceAnythingElse().getText()).toEqual(informationMessage);
    };
    this.expectThatShoppingBasketPriceIsUpdatedBaby = function () {
        var firstState = potentialTripPage.priceTotalCost().getText().then(function (text) {
            return text;
        });
        this.clickAddBabyEquipment(5);
        browser.sleep(1000);
        this.selectPlusButtonSameForBothFlights(0);
        this.clickConfirmButton();
        var secondState = potentialTripPage.priceTotalCost().getText().then(function (text) {
            return text;
        });
        expect(firstState).not.toEqual(secondState);
    };
    this.expectThatShoppingBasketPriceIsUpdatedMusic = function () {
        var firstState = potentialTripPage.priceTotalCost().getText().then(function (text) {
            return text;
        });
        this.clickAddMusicEquipment();
        browser.sleep(1000);
        this.selectPlusButtonSameForBothFlightsMusic(0);
        this.clickConfirmButtonMusic();
        var secondState = potentialTripPage.priceTotalCost().getText().then(function (text) {
            return text;
        });
        expect(firstState).not.toEqual(secondState);
    };
    this.expectThatShoppingBasketPriceIsUpdatedParking = function (value) {
        var firstState = potentialTripPage.priceTotalCost().getText().then(function (text) {
            return text;
        });
        this.clickAddParking();
        this.enterRegistrationNumber(value);
        this.changeParkingOption(1);
        this.clickConfirmButtonParking();
        var secondState = potentialTripPage.priceTotalCost().getText().then(function (text) {
            return text;
        });
        expect(firstState).not.toEqual(secondState);
    };
    this.expectThatShoppingBasketPriceIsUpdatedSport = function (value) {
        var firstState = potentialTripPage.priceTotalCost().getText().then(function (text) {
            return text;
        });
        this.clickAddSportEquipment(6);
        this.selectPlusButtonSameForBothFlightsSport(0);
        this.clickConfirmButtonSport();
        var secondState = potentialTripPage.priceTotalCost().getText().then(function (text) {
            return text;
        });
        expect(firstState).not.toEqual(secondState);
    };
    this.selectPlusButtonSameForBothFlights = function (i) {
        pages.babyEquipmentPage.plusButtonSameForBothFlights(i).click();
    };
    this.clickConfirmButton = function () {
        pages.babyEquipmentPage.confirmButtonClick().click();
    };
    this.clickConfirmButtonParking = function () {
        pages.lowCostParkingPage.confirmButton().click();
    };

    this.selectPlusButtonSameForBothFlightsMusic = function (i) {
        pages.musicEquipmentPage.plusButtonSameForBothFlights(i).click();
    };
    this.clickConfirmButtonMusic = function () {
        pages.musicEquipmentPage.confirmButtonClick().click()
    };
    this.selectPlusButtonSameForBothFlightsSport = function (i) {
        pages.musicEquipmentPage.plusButtonSameForBothFlights(i).click();
    };
    this.clickConfirmButtonSport = function () {
        pages.sportEquipmentPage.confirmButtonClick().click()
    };
    this.enterRegistrationNumber = function (value) {
        pt.registrationNumber().sendKeys(value);
    };
    this.changeParkingOption = function (indexOfParking) {
        pt.parkingOption(indexOfParking).click();
    };
    this.clickOpenSamsonite = function () {
        pages.potentialTripPage.openSamsoniteBags().click()
    };
    this.clickAirportTransfersCard = function(){
        potentialTripPage.openAirportTransfersCard().click();
        browser.sleep(2000);
    };
    this.expectPriceTotalCost = function (priceValue){
        expect(potentialTripPage.priceTotalCost().getText()).toContain(priceValue);
    };
    this.expectAddTransfersButtonIsDisplayed = function () {
        expect(potentialTripPage.openTransfersCard().isDisplayed()).toBeTruthy();
    };
    this.expectAddBabyButtonIsDisplayed = function () {
        expect(potentialTripPage.openBabyEquipmentCard().isDisplayed()).toBeTruthy();
    };
};
module.exports = potentialTripActions;
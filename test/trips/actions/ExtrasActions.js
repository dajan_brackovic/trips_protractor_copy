var Pages = require('../../Pages')
var pages = new Pages();

var ExtrasActions = function() {
    var tripsExtraPage = pages.tripsExtrasPage;
    var hotelsPage = pages.hotelsPage;
    var EC = protractor.ExpectedConditions;

    this.skipExtras = function() {
        browser.executeScript('window.scrollTo(1000,0);');
        tripsExtraPage.btnExtrasContinue().click();
    };

    this.xOutReserveSeatPopUp = function () {
        tripsExtraPage.reserveSeatEarlyPopUp().isDisplayed().then(function () {
            tripsExtraPage.ReserveSeatEarlyPopUpCloseBtn().click();
        }).thenCatch(function (error) {
            console.log(error);
        });

    };

    this.addSeat = function() {
        tripsExtraPage.btnSeat().click();
    };

    this.addBag = function() {
        tripsExtraPage.btnBag().click();
        browser.sleep(2000);
    };

    this.addBaby = function() {
        browser.executeScript('window.scrollTo(0,1500);');
        tripsExtraPage.btnBaby().click();
        browser.sleep(2000);
    };

    this.addHotels = function() {
        browser.executeScript('window.scrollTo(0,1000);');
        tripsExtraPage.btnHotels().click();
        browser.sleep(8000);
    };

    this.removeHotels = function() {
        tripsExtraPage.btnRemoveHotels().click();
        browser.sleep(2000);
    };


    this.clickFirstCarInlist = function() {
        browser.sleep(8000);
        tripsExtraPage.listOfCars().get(0).click();
    };

    this.clickModifyCarHire= function() {
        browser.sleep(2000);
        browser.wait(EC.presenceOf( tripsExtraPage.modifyCarHire()), 5000);
        tripsExtraPage.modifyCarHire().click();
    }

    this.addPriorityBoarding = function() {
        tripsExtraPage.bntAddPriorityBoarding().click();
    };

    this.verifyPriorityBoardingAdded = function () {
        expect(tripsExtraPage.textPriorityBoardingAdded().isDisplayed()).toBe(true);
        expect(tripsExtraPage.tickIconPriorityBoardingAdded().isDisplayed()).toBe(true);
    };

    this.removePriorityBoardingAndVerify = function () {
        tripsExtraPage.bntRemovePriorityBoarding().click();
        expect(tripsExtraPage.bntAddPriorityBoarding().isDisplayed()).toBe(true);
        expect(tripsExtraPage.textPriorityBoardingCardBenefits().get(0).isDisplayed()).toBe(true);
        expect(tripsExtraPage.textPriorityBoardingCardBenefits().get(1).isDisplayed()).toBe(true);
        expect(tripsExtraPage.textPriorityBoardingCardBenefits().get(2).isDisplayed()).toBe(true);

    };
};

module.exports = ExtrasActions;
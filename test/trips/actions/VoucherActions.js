var Pages = require('../../Pages')
var pages = new Pages();

var voucherPage = pages.vouchersPage;

var VoucherActions = function () {

    this.goToPage = function(){
        voucherPage.get();
        actions.fOHActions.handlePreloader();
    };

    this.selectTheme = function (index) {
        voucherPage.theme(index).click();
    };

    this.clickedAmountField = function() {
        voucherPage.ddAmountField().click();
    };

    this.selectVchAmount = function(index) {
        voucherPage.VchAmount(index).click();
    };

    this.selectVchCurrency = function(index) {
        voucherPage.VchCurrency(index).click();
        reporter.addMessageToSpec("Voucher Amount: 25 Euros");
    };

    this.countCurrenyOptions = function(index) {
        var currency = element.all(by.css('div.core-select > select > option'));
        expect(currency.count()).toEqual(11);
    };

    this.clickedMessageField = function() {
        voucherPage.ddMessageField().click();
    };

    this.enterMessageTextBox = function(text){
        voucherPage.messageTextBox().sendKeys(text);
        reporter.addMessageToSpec("Special Message: " + text );
    };

    this.clickBtnContinue= function() {
        voucherPage.btnAcceptTsAndCs().click();
        voucherPage.btnContinue().click();
    };

    this.enterYourName = function(name){
        voucherPage.yourName().sendKeys(name);
    };


    this.enterYourEmail = function(email){
        voucherPage.yourEmail().sendKeys(email);
        reporter.addMessageToSpec("From: " + email);
    };

    this.selectOfferLabel = function(){
        voucherPage.offerLabel().click();
    };

    this.selectCountryCode = function(index){
        voucherPage.voucherCountryCode(index).click();
    };

    this.enterPhoneNumber = function(number){
        voucherPage.phoneNumber().sendKeys(number);
    };

    this.enterFirstName = function(name){
        voucherPage.voucherFirstName().sendKeys(name);
    };

    this.enterLastName = function(name){
        voucherPage.voucherLastName().sendKeys(name);
    };

    this.enterEmailVoucher = function(email){
        voucherPage.emailVoucher().sendKeys(email);
        reporter.addMessageToSpec("To: " + email);
    };

    this.clickContinueBtn = function(){
        voucherPage.continueBtn().click();
    };

    this.clickRedeemVoucher = function(){
        voucherPage.redeemVoucher().click();
    };

    this.enterVoucherNumber = function(validnumber){
        voucherPage.voucherNumber().sendKeys(validnumber);
        reporter.addMessageToSpec("Voucher No. :" + validnumber)
    };

    this.clickRedeemBtn = function(){
        voucherPage.redeemBtn().click();
    };

    this.giftFromValue = function () {
        voucherPage.giftFrom().getText().then(function(from){
            reporter.addMessageToSpec('FROM:' + from);
        });
    };

    this.giftToValue = function () {
        voucherPage.giftTo().getText().then(function(to){
            reporter.addMessageToSpec('TO:' + to);
        });
    };

    this.sendToValue = function () {
        voucherPage.sendTo().getText().then(function(send){
            reporter.addMessageToSpec('SEND TO:' + send);
        });
    };

    this.referenceValue = function () {
        voucherPage.reference().getText().then(function(ref){
            reporter.addMessageToSpec("Voucher Reference: " + ref);
        });
    };

    this.returnVoucherNo = function () {
        return voucherPage.reference().getText();
    };

    this.confirmationTextValue = function () {
        voucherPage.confirmationText().getText().then(function(text){
            reporter.addMessageToSpec("Order Confirmed Message: " + text);
        });
    };

    this.giftVoucherValue = function () {
        voucherPage.giftVoucher().getText().then(function(value){
            reporter.addMessageToSpec('GIFT VOUCHER:' + value);
        });
    };

    this.voucherValue = function () {
        return voucherPage.giftVoucher().getText();
    };

    this.giftVoucherHandlingFeeValue = function () {
        voucherPage.giftVoucherHandlingFee().getText().then(function(fee){
            reporter.addMessageToSpec('VOUCHER HANDLING FEE:' + fee);
        });
    };

    this.totalPaid1Value = function () {
        voucherPage.totalPaid1().getText().then(function(paid){
            reporter.addMessageToSpec('Total price:' + paid);
        });
    };

    this.assertOnFirstNameField = function(){
        expect(voucherPage.yourName().isDisplayed());
    };

    this.assertOnConfirmationText = function(){
        expect(voucherPage.confirmationText().isDisplayed());
    };

    this.assertOnValidVoucher = function(vchValue){
        expect(voucherPage.voucherField().getText()).toContain(vchValue);
    };

};
module.exports = VoucherActions;
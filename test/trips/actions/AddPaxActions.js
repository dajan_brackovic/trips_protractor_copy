var Pages = require('../../Pages')
var pages = new Pages();
var sprintf = require("sprintf").sprintf;
var Faker = require('faker');


var AddPaxActions = function () {

    var tripsPaxPage = pages.tripsPaxPage;
    var EC = protractor.ExpectedConditions;

    this.addPaxNameForAllPAX = function (paxList) {

        var elementIndex = 0;
        for (var i = 0; i < paxList.length; i++) {
            if (paxList[i].paxType === 'ADT' || paxList[i].paxType === 'TEEN') {
                tripsPaxPage.selectTitleDropDownOption(elementIndex).click();
                tripsPaxPage.textFieldFirstName(elementIndex).sendKeys(paxList[i].firstName);
                tripsPaxPage.textFieldLastName(elementIndex).sendKeys(paxList[i].lastName);
            }
            if (paxList[i].paxType === 'CHD') {
                tripsPaxPage.textFieldFirstName(elementIndex).sendKeys(paxList[i].firstName);
                tripsPaxPage.textFieldLastName(elementIndex).sendKeys(paxList[i].lastName);
            }
            elementIndex++;
        }
        var elementIndex = 0;
        for (var i = 0; i < paxList.length; i++) {
            if (paxList[i].paxType === 'INF') {
                tripsPaxPage.infantFirst(elementIndex).sendKeys(paxList[i].firstName);
                tripsPaxPage.infantLast(elementIndex).sendKeys(paxList[i].lastName);
                tripsPaxPage.infantDAY(elementIndex).click();
                tripsPaxPage.infantMONTH(elementIndex).click();
                tripsPaxPage.infantYEAR(elementIndex, 2).click();//for infant select 2nd year in options drop-down
                elementIndex++;
            }
        }
    };

    this.clickBtnAddPaxBalearic = function () {
        browser.executeScript('window.scrollTo(0,500);');
        tripsPaxPage.btnAddPaxBalearic().click();
    };

    this.clickBtnAddPaxSave = function () {
        browser.executeScript('window.scrollTo(0,500);');
        tripsPaxPage.btnAddPaxSave().click();
    };

    this.spanishaddPaxNameForAllPAXFor50 = function (paxList, dniVal, muniVal, numInfants) {
        this.addPaxNameForAllPAX(paxList);

        for (var i = 0; i < paxList.length; i++) {
            //for every Adult & Teen do this -
            if (paxList[i].paxType === 'ADT' || paxList[i].paxType === 'TEEN') {
                tripsPaxPage.balearicProofDNI(i).click();
                tripsPaxPage.balearicDocumentNumber(i).sendKeys(dniVal[i]);
                tripsPaxPage.balearicMunicipalityValue(i, muniVal).click();
            }
        }

        var elementIndex = 0;
        for (var j = 0; j < paxList.length; j++) {
            //for every Child do this -
            if (paxList[j].paxType === 'CHD') {
                tripsPaxPage.balearicProofUnder14(j).click();
                tripsPaxPage.infantDAY(elementIndex + numInfants).click(); //include the amount of infants when calculating elementIndex as they have...
                tripsPaxPage.infantMONTH(elementIndex + numInfants).click(); //...already been used to fill in the Day and Month fields for Infant DOBs.
                tripsPaxPage.infantYEAR(elementIndex, 5).click();
                tripsPaxPage.balearicMunicipalityValue(j, muniVal).click();
                elementIndex++;
            }
        }
    };

    this.spanishaddPaxNameForAllPAXFor60 = function (paxList, dniVal, muniVal, commVal, famCerVal, numInfants) {
        this.addPaxNameForAllPAX(paxList);

        for (var i = 0; i < paxList.length; i++) {
            //for every Adult & Teen do this -
            if (paxList[i].paxType === 'ADT' || paxList[i].paxType === 'TEEN') {
                tripsPaxPage.balearicProofDNI(i).click();
                tripsPaxPage.balearicDocumentNumber(i).sendKeys(dniVal[i]);
                tripsPaxPage.balearicMunicipalityValue(i, muniVal).click();
                tripsPaxPage.balearicCommunityValue(i, commVal).click();
                tripsPaxPage.balearicFamilyCertificate(i).sendKeys(famCerVal);
            }
        }

        var elementIndex = 0;
        for (var j = 0; j < paxList.length; j++) {
            //for every Child do this -
            if (paxList[j].paxType === 'CHD') {
                tripsPaxPage.balearicProofUnder14(j).click();
                tripsPaxPage.infantDAY(elementIndex + numInfants).click(); //include the amount of infants when calculating elementIndex as they have...
                tripsPaxPage.infantMONTH(elementIndex + numInfants).click(); //...already been used to fill in the Day and Month fields for Infant DOBs.
                tripsPaxPage.infantYEAR(elementIndex, 5).click();
                tripsPaxPage.balearicMunicipalityValue(j, muniVal).click();
                tripsPaxPage.balearicCommunityValue(j, commVal).click();
                tripsPaxPage.balearicFamilyCertificate(j).sendKeys(famCerVal);
                elementIndex++;
            }
        }
    };

    this.documentCheck = function () {
        tripsPaxPage.btnCheck().click();
        tripsPaxPage.btnCheckContinue().click();
        expect(tripsPaxPage.validatedMessage().isDisplayed());
        browser.sleep("1000");
    };

    this.documentCheckFirstSecond = function () {
        tripsPaxPage.btnCheck().click();
        this.assertOnNotValidatedMessage();
        tripsPaxPage.btnEditPax().click();
    };

    this.documentCheckThird = function () {
        tripsPaxPage.btnCheck().click();
        this.assertOnNotValidatedMessage();
        tripsPaxPage.btnCheckContinue().click();
    };

    this.assertOnNotValidatedMessage = function () {
        expect(tripsPaxPage.notValidatedMessage().isDisplayed());
    };

    this.clickBtnEditPaxDetail = function () {
        tripsPaxPage.btnEditPaxDetail().click();
    };

    this.spanishaddPaxNameForAllPAX = function (paxList, dniVal, commVal, famCerVal, numInfants) {
        this.addPaxNameForAllPAX(paxList);

        for (var i = 0; i < paxList.length; i++) {
            //for every Adult & Teen do this -
            if (paxList[i].paxType === 'ADT' || paxList[i].paxType === 'TEEN') {
                tripsPaxPage.balearicProofDNI(i).click();
                tripsPaxPage.balearicDocumentNumber(i).sendKeys(dniVal[i]);
                tripsPaxPage.balearicCommunityValue(i, commVal).click();
                tripsPaxPage.balearicFamilyCertificate(i).sendKeys(famCerVal);
            }
        }

        //include the amount of infants when calculating elementIndex as they have already been used to fill in Infant DOBs.
        var elementIndex = 0;
        for (var j = 0; j < paxList.length; j++) {
            //for every Child do this -
            if (paxList[j].paxType === 'CHD') {
                tripsPaxPage.balearicProofUnder14(j).click();
                tripsPaxPage.infantDAY(elementIndex + numInfants).click(); //include the amount of infants when calculating elementIndex as they have...
                tripsPaxPage.infantMONTH(elementIndex + numInfants).click(); //...already been used to fill in the Day and Month fields for Infant DOBs.
                tripsPaxPage.infantYEAR(elementIndex, 5).click();
                tripsPaxPage.balearicCommunityValue(j, commVal).click();
                tripsPaxPage.balearicFamilyCertificate(j).sendKeys(famCerVal);
                elementIndex++;
            }
        }
    };

    //checkoutPage

    this.assertMyFrSignupOptionPopUp= function () {
        expect(tripsPaxPage.myFrSignupPopUp().getText()).toContain("Make your next booking");

        tripsPaxPage.myFrSignupPopUp().getText().then(function (text) {
            reporter.addMessageToSpec("PopUp text = " + text)
        });

    };

    this.assertOnMyFrSignupNotPresent = function () {
        expect(tripsPaxPage.myFrSignupPopUp().isDisplayed()).toBeFalsy();
    };


    this.clickMyFrSignupPopUp = function () {
        tripsPaxPage.myFrSignupPopUpRadioBtn().click();
    };

    this.assertOnMyFrSignupNotPresent = function () {
        expect(tripsPaxPage.myFrSignupPopUp().isDisplayed()).toBeFalsy();
    };

    this.addContact = function () {
        tripsPaxPage.textFieldEmailAddress().clear();
        tripsPaxPage.textFieldEmailAddress().sendKeys("auto@ryanair.ie");
        tripsPaxPage.selectContactCountryDropDownOption("Ireland").click();
        tripsPaxPage.textFieldPhoneNumber().clear();
        tripsPaxPage.textFieldPhoneNumber().sendKeys("12345678");
    };

    this.addContactUnregisteredEmail = function () {
        tripsPaxPage.textFieldEmailAddress().clear();
        tripsPaxPage.textFieldEmailAddress().sendKeys(actions.tripHelper.getRandomEmail());
        tripsPaxPage.selectContactCountryDropDownOption("Ireland").click();
        tripsPaxPage.textFieldPhoneNumber().clear();
        tripsPaxPage.textFieldPhoneNumber().sendKeys("12345678");
    };

    this.addContactForLoggedUser = function () {
        tripsPaxPage.btnAddPaxPageContinue().click();
    };

    this.addContactForLoggedUserWithNoPhoneNumber = function () {
        browser.sleep(2000);
        tripsPaxPage.selectCountryDropDownOption(11).click();
        tripsPaxPage.textFieldPhoneNumber().clear();
        tripsPaxPage.textFieldPhoneNumber().sendKeys("12345678");
        tripsPaxPage.btnAddPaxPageContinue().click();
    };

    this.addContactForNewUserWithNoPhoneNumber = function () {
        browser.sleep(2000);
        tripsPaxPage.selectContactCountryDropDownOption(11).click();
        tripsPaxPage.textFieldPhoneNumber().clear();
        tripsPaxPage.textFieldPhoneNumber().sendKeys("12345678");
    };

    this.makePayPalPayment = function () {
        //browser.sleep(5000);
        tripsPaxPage.btnPayPal().click();
        browser.sleep(5000);
        this.clickPaymentContinue();
        browser.sleep(5000);
    };

    this.makeCardPayment = function (card) {
        tripsPaxPage.selectPaymentTypeDropDownOption(card.type).click();
        tripsPaxPage.textFieldCardNumber().sendKeys(card.cardNumber);
        tripsPaxPage.selectPaymentTypeDropDownOption(card.type).click();
        this.enterCardDetails(card);
    };

    this.makeCardPaymentVoucher = function (card) {
        tripsPaxPage.textFieldCardNumber().sendKeys(card.cardNumber);
        tripsPaxPage.selectPaymentTypeDropDownOption(card.type).click();
        this.enterCardDetails(card);
    };

    this.makeCardPaymentActive = function (card) {
        tripsPaxPage.textFieldCardNumber().sendKeys(card.cardNumber);
        this.enterCardDetails(card);
        tripsPaxPage.selectPaymentTypeDropDownOption(card.type).click();
        this.enterBillingAddress();
        this.clickPaymentContinue();
    };

    this.makeCardPaymentEnterBilling = function (card) {
        tripsPaxPage.textFieldCardNumber().sendKeys(card.cardNumber);
        this.enterCardDetails(card);
        tripsPaxPage.selectPaymentTypeDropDownOption(card.type).click();
        this.enterBillingAddressAtCheckIn();
        this.clickPaymentContinue();
    };

    this.makeCardPaymentHoldFare = function (card) {
        tripsPaxPage.textFieldCardNumber().sendKeys(card.cardNumber);
        this.enterCardDetails(card);
        tripsPaxPage.selectPaymentTypeDropDownOption(card.type).click();
        this.clickPaymentContinueHoldFare();
    };

    this.makeCardPaymentWithoutContinuing = function (card) {
        tripsPaxPage.textFieldCardNumber().sendKeys(card.cardNumber);
        tripsPaxPage.selectPaymentTypeDropDownOption(card.type).click();
        this.enterCardDetails(card);
    };

    this.makeDCCCardPayment = function (card) {
        tripsPaxPage.textFieldCardNumber().sendKeys(card.cardNumber);
        tripsPaxPage.selectPaymentTypeDropDownOption(card.type).click();
        this.enterCardDetails(card);
    };

    this.enterCardDetails = function (card) {
        tripsPaxPage.selectExpiryYearDropDownOption(card.expiryDateYear).click();
        tripsPaxPage.selectExpiryMonthDropDownOption(card.expiryDateMonth).click();
        tripsPaxPage.textFieldCVV().sendKeys(card.cvv);
        tripsPaxPage.textFieldCardHoldersName().sendKeys("Automation User");
    };

    this.enterBillingAddress = function () {
        tripsPaxPage.textAddressLine1().sendKeys("21 Sun Lane");
        tripsPaxPage.textAddressLine2().sendKeys("Swords Road");
        tripsPaxPage.textFieldCity().sendKeys("Dublin");
        tripsPaxPage.selectBillingCountryDropDownOption("Ireland").click();
        tripsPaxPage.textFieldPostCode().sendKeys("01");
    };

    this.enterBillingAddressAtCheckIn = function () {
        tripsPaxPage.textAddressLine1().sendKeys("21 Sun Lane");
        tripsPaxPage.textAddressLine2().sendKeys("Swords Road");
        tripsPaxPage.textFieldCity().sendKeys("Dublin");
        tripsPaxPage.textFieldPostCode().sendKeys("01");
    };

    this.assertOnPresenceOfInvalidCardMessage = function () {
        browser.executeScript('arguments[0].scrollIntoView()', tripsPaxPage.invalidcardNumber().getWebElement());
        expect(tripsPaxPage.invalidcardNumber().isDisplayed());
    };

    this.assertOnDeclinedPaymentError = function () {
        browser.executeScript('arguments[0].scrollIntoView()', tripsPaxPage.paymentError().getWebElement());
        expect(tripsPaxPage.paymentError().isDisplayed());
    };

    this.assertOnDeclinedPaymentDialog = function () {
        expect(tripsPaxPage.paymentErrorDialog().isDisplayed());
    };

    this.assertOnPaymentDeclinedPopUpButton = function () {
        expect(tripsPaxPage.paymentDeclinedPopUpButton().isDisplayed()).toBeTruthy();
    };

    this.clickOnPaymentDeclinedPopUpButton = function () {
        tripsPaxPage.paymentDeclinedPopUpButton().click();
    };

    // VAT Methods
    this.enterVatDetails = function () {
        tripsPaxPage.btnAddVatDetails().click();
    }

    this.enterVatNumber = function () {
        tripsPaxPage.fieldVatNumber().sendKeys("IE1234567T");
    }

    this.enterBusinessName = function () {
        tripsPaxPage.fieldBusinessName().sendKeys("ABC");
    }

    this.clickLabelSameAddress = function () {
        tripsPaxPage.labelSameAddress().click();
    }

    this.enterAddressDetailsForVat = function () {
        tripsPaxPage.countryDropdownOptionsVat(17).click();
        tripsPaxPage.fieldVatCity().sendKeys("Dublin");
        tripsPaxPage.fieldVatPostCode().sendKeys("01");
        tripsPaxPage.fieldVatAddress1().sendKeys("Swords Road");
    };

    this.assertOnVatErrorMessage = function () {
        browser.executeScript('arguments[0].scrollIntoView()', tripsPaxPage.lblVatErrorMessage().getWebElement());
        expect(tripsPaxPage.lblVatErrorMessage().isDisplayed()).toBeTruthy();
    };


    this.clickPaymentContinue = function () {
        browser.wait(EC.presenceOf(tripsPaxPage.paymentTermsAndConditions()), 5000); // Waits for the element to be present on the dom
        browser.executeScript('window.scrollTo(0,1500);');
        tripsPaxPage.paymentTermsAndConditions().click();
        tripsPaxPage.btnPaymentContinue().click();
    };

    this.clickPaymentContinueOnly = function () {
        tripsPaxPage.btnPaymentContinue().click();
    };

    this.clickPaymentContinueActive = function () {
        tripsPaxPage.paymentTermsAndConditions().click();
        tripsPaxPage.btnPaymentContinueActive().click();
    };

    this.clickPaymentContinueHoldFare = function () {
        tripsPaxPage.paymentTermsAndConditions().click();
        tripsPaxPage.btnPaymentContinueHoldFare().click();
    };

    this.clickPaymentContinueVoucher = function () {
        tripsPaxPage.paymentTermsAndConditions().click();
        tripsPaxPage.btnPaymentContinueVoucher().click();
    };

    this.numberPNRGetValue = function () {
        tripsPaxPage.numberPNR().getText().then(function (pnr) {
            console.log(pnr);
            return pnr;
        });
    }

    this.assertOnPaxDetailsRequired = function () {
        expect(tripsPaxPage.requiredPaxDetails().isDisplayed());
    }

    this.verifyDccField = function (originCurrency, cardCurrency) {
        expect(pages.tripsPaxPage.dccField().getText()).toContain(originCurrency);
        expect(pages.tripsPaxPage.dccField().getText()).toContain(cardCurrency);
        reporter.addMessageToSpec(sprintf("Original Currency:  %s, Card Conversion Currency: %s", originCurrency, cardCurrency));
    }


    // Special Assistance Actions

    this.clickLabelSpecialAssistance = function () {
        browser.sleep(5000);
        tripsPaxPage.labelSpecialAssistance().click();
        expect(tripsPaxPage.labelSpecialAssistanceTitle().isDisplayed()).toBeTruthy();
    };

    this.clickViewMoreInfoOpen = function () {
        tripsPaxPage.viewMoreInfo().click();
        expect(tripsPaxPage.viewMoreInfoPara().isDisplayed()).toBeTruthy();
    };

    this.clickViewMoreInfoClose = function () {
        tripsPaxPage.viewMoreInfo().click();
        expect(tripsPaxPage.viewMoreInfoPara().isPresent()).toBeFalsy();
    };

    this.clickSpecialAssistancePaxCheckBox = function () {
        tripsPaxPage.specialAssistancePaxCheckBox().click();
        expect(tripsPaxPage.specialAssistanceDropDown().isDisplayed()).toBeTruthy();
    };

    this.countAndSelectListOfOptionsInSpecialAssistanceDropDown = function (tripWay) {
        tripsPaxPage.specialAssistanceDropDown().click();
        if(tripWay === "oneway") {
            expect(tripsPaxPage.listOfOptionsInSpecialAssistanceDropDown().count()).toEqual(10);
        }
        else {
            expect(tripsPaxPage.listOfOptionsInSpecialAssistanceDropDown().count()).toEqual(20);
        }
        tripsPaxPage.listOfOptionsInSpecialAssistanceDropDown2option().click();
    };

    this.selectSpecialAssistanceFromReturnDropDown = function () {
        tripsPaxPage.specialAssistanceDropDown().get(1).click();
        tripsPaxPage.returnSpecialAssistanceDropDown().get(1).click();
    };

    this.clickSpecialAssistanceWheelChairQuestionYes = function () {
        tripsPaxPage.specialAssistanceWheelChairQuestionYes().click();
        expect(tripsPaxPage.optionsOfWheelChair().isDisplayed()).toBeTruthy();
        tripsPaxPage.selectFirstWheelChair().click();
    };

    this.clickSpecialAssistanceWheelChairReturn = function () {
        tripsPaxPage.specialAssistanceWheelChairReturn().click();
        expect(tripsPaxPage.optionsOfWheelChair().isDisplayed()).toBeTruthy();
    };


    this.editLastSavedPaxNameMyRyanair = function (totalPax) {
        tripsPaxPage.textFieldFirstName(totalPax-1).clear();
        tripsPaxPage.textFieldFirstName(totalPax-1).sendKeys(Faker.name.firstName());
        tripsPaxPage.textFieldLastName(totalPax-1).clear();
        tripsPaxPage.textFieldLastName(totalPax-1).sendKeys(Faker.name.lastName());
    };


    this.clickLogInMyFr = function (userName, password) {
        tripsPaxPage.btnLogInMyFr().click();
        actions.fOHActions.enterUsername(userName);
        actions.fOHActions.enterPassword(password);
        actions.fOHActions.clickLoginBtn();
        browser.sleep(1000);
    };

    // Duplicate Name error Asserstion

    this.assertOnDuplicatePaxNameError = function () {
        browser.sleep(1000);
        expect(tripsPaxPage.duplicatePaxNameError().isDisplayed()).toBeTruthy();
    };

    // My Ryanair Payment Actions

    this.addSavedPaxNameMyRyanair = function (paxList) {

        var elementIndex = 0;
        for (var i = 0; i < paxList.length; i++) {
            if (paxList[i].paxType === 'ADT' || paxList[i].paxType === 'TEEN') {
                tripsPaxPage.textFieldFirstName(elementIndex).click();
                //tripsPaxPage.firstAddedUserInPassengerDetails(elementIndex).click();
                browser.sleep(1000);
                browser.actions().sendKeys(protractor.Key.ENTER).perform();
            }
            if (paxList[i].paxType === 'CHD') {
                tripsPaxPage.textFieldFirstName(elementIndex).sendKeys(paxList[i].firstName);
                tripsPaxPage.textFieldLastName(elementIndex).sendKeys(paxList[i].lastName);
            }
            elementIndex++;
        }
        var elementIndex = 0;
        for (var i = 0; i < paxList.length; i++) {
            if (paxList[i].paxType === 'INF') {
                tripsPaxPage.infantFirst(elementIndex).sendKeys(paxList[i].firstName);
                tripsPaxPage.infantLast(elementIndex).sendKeys(paxList[i].lastName);
                tripsPaxPage.infantDAY(elementIndex).click();
                tripsPaxPage.infantMONTH(elementIndex).click();
                tripsPaxPage.infantYEAR(elementIndex, 2).click();
                elementIndex++;
            }
        }
    };

    this.addSavedPaxNameNewUserMyRyanair = function (paxList) {
        var elementIndex = 0;
        for (var i = 0; i < paxList.length; i++) {
            console.log("paxList.length:" + paxList.length);
            if (paxList[i].paxType === 'ADT' || paxList[i].paxType === 'TEEN') {
                tripsPaxPage.selectTitleDropDownOption(elementIndex).click();
                tripsPaxPage.textFieldFirstName(elementIndex).click();
                tripsPaxPage.textFieldFirstName(elementIndex).sendKeys(paxList[i].firstName);
                tripsPaxPage.textFieldLastName(elementIndex).click();
                tripsPaxPage.textFieldLastName(elementIndex).sendKeys(paxList[i].lastName);

            }
            if (paxList[i].paxType === 'CHD') {
                tripsPaxPage.textFieldFirstName(elementIndex).sendKeys(paxList[i].firstName);
                tripsPaxPage.textFieldLastName(elementIndex).sendKeys(paxList[i].lastName);
            }
            elementIndex++;
        }
        var elementIndex = 0;
        for (var i = 0; i < paxList.length; i++) {
            if (paxList[i].paxType === 'INF') {
                tripsPaxPage.infantFirst(elementIndex).sendKeys(paxList[i].firstName);
                tripsPaxPage.infantLast(elementIndex).sendKeys(paxList[i].lastName);
                tripsPaxPage.infantDAY(elementIndex).click();
                tripsPaxPage.infantMONTH(elementIndex).click();
                tripsPaxPage.infantYEAR(elementIndex, 2).click();
                elementIndex++;
            }
        }
    };

    this.makeCardPaymentMyRyanairSavedCard = function (card) {
        tripsPaxPage.textFieldCVVmyFr().sendKeys(card.cvv);
    };

    // Price BreakDown Actions On Payment Page

    this.clickOnXAddedItems = function () {
        tripsPaxPage.xAddedItems().count().then(function (originalCount) {
            console.log("Original Count:->" + originalCount);
            for (var i = 0; i < originalCount ; i++) {
                tripsPaxPage.xAddedItems().count().then(function (loopCount) {
                    console.log("Loop Count:->" + loopCount);
                });
                tripsPaxPage.xAddedItems().get(0).click();
                browser.sleep(2000);
            }
        });
    };

    this.assertOnNoItemsOnPriceBreakdownIncludingFlights = function (NoOfProductsCardPresentOnPriceBreakdown) {
        expect(tripsPaxPage.itemsOnPriceBreakdownIncludingFlights().count()).toEqual(NoOfProductsCardPresentOnPriceBreakdown)
    };

    this.assertOnTotalPriceOnPaymentPage = function (totalPrice) {
        pages.priceBreakDownPage.totalPriceWholeNumber().getText().then(function(paymentPageTotal){
            expect(totalPrice).toEqual(paymentPageTotal);
        });
        pages.priceBreakDownPage.totalPriceWholeNumber().getText().then(function (paymentPageTotal) {
            totalPrice.then(function (totalPrice) {
                reporter.addMessageToSpec("Payment Page Total: " + paymentPageTotal);
                reporter.addMessageToSpec("Price Breakdown Total: " + totalPrice);
            });
        });

    };

    this.assertOnCreditCardFeeOnPaymentPage = function (tripWay, creditCardFee) {
        pages.priceBreakDownPage.ccPaymentFee(tripWay).getText().then(function(ccFeePaymentPage){
            expect(creditCardFee).toEqual(ccFeePaymentPage);
        });
        pages.priceBreakDownPage.ccPaymentFee(tripWay).getText().then(function (ccFeePaymentPage) {
            creditCardFee.then(function (creditCardFee) {
                reporter.addMessageToSpec("Payment Page Credit Card Fee: " + ccFeePaymentPage);
                reporter.addMessageToSpec("Price Breakdown Credit Card Fee: " + creditCardFee);
            });
        });

    };

    this.assertOnUkChildDiscountOnPaymentPage = function (tripWay) {
        if (tripWay === "twoway") {
            expect(pages.priceBreakDownPage.ukChildDiscountLabelUkDomestic().count()).toBe(2);
            expect(pages.priceBreakDownPage.ukChildDiscountLabelUkDomestic().get(0).getText()).toContain("UK APD Discount");
            expect(pages.priceBreakDownPage.ukChildDiscountLabelUkDomestic().get(1).getText()).toContain("UK APD Discount");
        }
        else{
            expect(pages.priceBreakDownPage.ukChildDiscountLabelUkDomestic().count()).toBe(1);
            expect(pages.priceBreakDownPage.ukChildDiscountLabelUkDomestic().get(0).getText()).toContain("UK APD Discount");
        }
    };

    // Car Hire Lead Driver Actions

    this.enterFieldLeadDriverName = function () {
        tripsPaxPage.fieldLeadDriverFirstName().sendKeys("Automation");
        tripsPaxPage.fieldLeadDriverLastName().sendKeys("User");
    };

    // Contact Details For My Fr Saved Details Actions

    this.assertOnSavedEmailAddress = function (userName) {
        expect(tripsPaxPage.savedEmailAddress().getText()).toContain(userName);
    };

    this.assertOnSavedPhoneNumber = function () {
        expect(tripsPaxPage.savedPhoneNumber().getText()).toContain("123456789");
    };

    this.clickChangeEmailLink = function () {
        tripsPaxPage.changeEmailLink().click();
    };

    this.editEmailAddress = function () {
        tripsPaxPage.textFieldEmailAddress().clear();
        tripsPaxPage.textFieldEmailAddress().sendKeys("auto@ryanair.ie");
    };

    this.clickAddNewNumberLink = function () {
        tripsPaxPage.addNewNumberLink().click();
    };

    this.editPhoneNumber = function () {
        tripsPaxPage.textFieldPhoneNumber().clear();
        tripsPaxPage.textFieldPhoneNumber().sendKeys("123000000");
    };

    this.assertOnSavedCard = function () {
        expect(tripsPaxPage.savedCard().isPresent()).toBe(true);
        expect(tripsPaxPage.savedCardLastFourDigits().getText()).toContain("1001");
    };

    this.assertOnSavedAddress = function () {
        expect(tripsPaxPage.savedBillingAddressMyFr().getText()).toContain("Ryanair Hq");
        expect(tripsPaxPage.savedBillingAddressMyFr().getText()).toContain("Swords");
        expect(tripsPaxPage.savedBillingAddressMyFr().getText()).toContain("Dublin");
        expect(tripsPaxPage.savedBillingAddressMyFr().getText()).toContain("Ireland");
    };
    this.verifyPriceBreakDownContainPriorityBoarding = function() {
        expect(tripsPaxPage.pricePriorityBoardingAdded().isDisplayed()).toBe(true);
    };

    this.isPriceBreakDownContainPriorityBoarding = function(bool) {
        expect(tripsPaxPage.pricePriorityBoardingAdded().isDisplayed()).toBe(bool);
    };

};



module.exports = AddPaxActions;
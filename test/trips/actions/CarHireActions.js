var Pages = require('../../Pages')
var pages = new Pages();

var CarHireActions = function() {
    var carHirePage = pages.carHirePage;
    var EC = protractor.ExpectedConditions;

    this.enterDriverNameCarHire = function () {
        carHirePage.fieldFirstName().sendKeys("Automation");
        carHirePage.fieldSurname().sendKeys("User");
    };

    this.enterDriverNameCarHireModify = function () {
        carHirePage.fieldFirstName().clear();
        carHirePage.fieldFirstName().sendKeys("Automation");
        carHirePage.fieldSurname().clear();
        carHirePage.fieldSurname().sendKeys("Modify");
    };

    this.clickCheckboxExtraLiability = function () {
        browser.sleep(3000);
        if(carHirePage.checkboxExtraLiability().isPresent()){
            carHirePage.checkboxExtraLiability().click();
        }

        else {
            carHirePage.xCarHireSideDrawer().click();
        }
        browser.sleep(2000);
    };

    this.clickBtnConfirmCarHire = function () {
        carHirePage.btnConfirmCarHire().click();
        browser.sleep(1000);
    };

    this.clickBtnCancelCarHire = function () {
        browser.wait(EC.presenceOf(carHirePage.btnCancelCarHire()), 5000);
        carHirePage.btnCancelCarHire().click();
    };

    this.returnTotalPriceCarHire = function () {
        return carHirePage.totalPriceCarHire().getText();
    };

    this.clickBtnRemoveCarHire = function () {
        carHirePage.btnRemoveCarHire().click();
        browser.sleep(1000);
    };

// CarHire On Active Trip

    this.clickAddCarHireActive = function () {
        browser.getAllWindowHandles().then(function (handles) {
            var secondWindowHandle = handles[1];
            var firstWindowHandle = handles[0];
            //the focus moves on the newest opened tab
            browser.switchTo().window(secondWindowHandle).then(function () {
                //check if the right page is opened
                expect(browser.driver.getCurrentUrl()).toContain("car-hire.ryanair.com/");
                browser.ignoreSynchronization = true;

                expect(carHirePage.ryanairLogoOnCarHirePage().isPresent()).toBeTruthy();

                browser.switchTo().window(firstWindowHandle).then(function () {
                    browser.driver.close();
                });
                browser.switchTo().window(secondWindowHandle)
            });
        });
    };

};

module.exports = CarHireActions;
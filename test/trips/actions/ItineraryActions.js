var Pages = require('../../Pages');
var pages = new Pages();
var configFile = require('./../../protractor.conf.js')

var ItineraryActions = function () {
    var itineraryPage = pages.itineraryPage;

    this.assertOnReservationNumber = function () {
        pages.tripsSummaryPage.labelBookingRef().getText().then(function (pnr) {
            actions.bookingSummaryActions.clickViewYourTripItinerary();
            itineraryPage.reservationNumber().getText().then(function (resNumber) {
                expect(pnr).toEqual(resNumber);
                reporter.addMessageToSpec("Reference No.: " + pnr + " Reservation No.: " + resNumber);

            });
        });
    };

    this.assertOnAddedExtrasItemsList = function (extraItem) {
        expect(itineraryPage.addedExtrasItemsList().getText()).toContain(extraItem);
    };
};

module.exports = ItineraryActions;
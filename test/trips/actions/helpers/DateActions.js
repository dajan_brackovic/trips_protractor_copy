var DateActions = function () {

    this.getDayNumberDaysFromNow = function (daysFromNow) {
        var d = new Date();
        d.setDate(d.getDate() + daysFromNow);
        var dayNumber = d.getDate();
        return dayNumber;
    };


    this.checkFlightConnectionTime = function (flightDepartureTime) {
        return checkTime(flightDepartureTime, 60);
    }

    this.checkFlightIsBookable = function (flightDepartureTime) {
        return checkTime(flightDepartureTime, 360);
    }

}

//var checkTime = function (flightDepartureTime, timeDifferenceMins) {
//    console.log("Checking Flight available " + flightDepartureTime);
//    var flightHour = flightDepartureTime.split(":")[0]
//    var flightMins = flightDepartureTime.split(":")[1];
//
//    var now = new Date();
//    var flightTime = new Date();
//
//    flightTime.setHours(flightHour);
//    flightTime.setMinutes(flightMins);
//
//    if (flightTime < now) {
//        return false;
//    }
//
//    var timeDiff = (flightTime.getTime() - now.getTime())  / 60000;
//
//    console.log("time diff > " + timeDiff);
//
//    if (Math.abs(timeDiff) > (timeDifferenceMins)) {
//        return true;
//    }
//    return false;
//}

module.exports = DateActions;

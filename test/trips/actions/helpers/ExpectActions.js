
var ExpectActions = function () {

    this.expectTextEquals = function(element, expectedText){
        element.getText().then(function (actualText) {
            console.log("FOUND " + actualText);
            expect(expectedText).toEqual(actualText);
        })
    }
}

module.exports = ExpectActions;

var WaitHelper = function () {

    var EC = protractor.ExpectedConditions;

    this.waitForPageTitle = function(title){
        browser.driver.wait(EC.titleContains(title), 5000);
        browser.driver.getCurrentUrl().then(function(url){
            reporter.addMessageToSpec("Current Page Url: " + url);
        });
    }
};

module.exports = WaitHelper;












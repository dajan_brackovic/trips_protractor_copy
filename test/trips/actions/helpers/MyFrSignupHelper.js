var request = require('request');

var MyFrSignupHelper = function () {

    this.createNewUser = function (email, password) {
        var env;
        var body;
        browser.driver.getCurrentUrl().then(function (url) {
            console.log("Current URL: " + url);
            if (url.indexOf("sit") > -1) {
                env = "sit";
            }
            else if (url.indexOf("uat") > -1) {
                env = "uat";
            }
            else {
                env = "Error - Environment not detected correctly."
            }

            body = JSON.stringify({
                env: env,
                random: "false",
                email: email,
                password: password
            });
            console.log(">>>>>>>>>>> Body is " + body);

            httpPost("http://10.11.17.50:4567/signup", body);

        });
    };

    this.activateUser = function (email, password) {
        var env;
        var body;
        browser.driver.getCurrentUrl().then(function (url) {
            console.log("Current URL: " + url);
            if (url.indexOf("sit") > -1) {
                env = "sit";
            }
            else if (url.indexOf("uat") > -1) {
                env = "uat";
            }
            else {
                env = "Error - Environment not detected correctly."
            }

            body = JSON.stringify({
                env: env,
                email: email,
                password: password
            });
            console.log(">>>>>>>>>>> Body is " + body);
            httpPost("http://10.11.17.50:4567/activation", body);
        });
    };

    function httpPost(url, body) {
        var defer = protractor.promise.defer();
        request.post({
            url: url,
            headers: {'Content-Type': 'application/json'},
            body: body
        }, function (error, response, body) {
            if (error) {
                console.log("--> e " + error);
            }
            console.log(">>>>>>>>>>> Error : " + error);
            console.log(">>>>>>>>>>> Response : " + response);
            console.log(">>>>>>>>>>> Body : " + body);
        });
        return defer.promise;
    }

};

module.exports = MyFrSignupHelper;



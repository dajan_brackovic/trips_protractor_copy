var Faker = require('faker');

var TripHelper = function () {


    this.getRandomDate = function (constraint) {
        var offset = 0;
        switch (constraint) {
            case "NONE":
                offset = getRandomInt(2, 5);
                break;
            case "PARTIAL":
                offset = getRandomInt(8, 28);
                break;
            case "FULL":
                offset = getRandomInt(32, 35);
                break;
            default:
                offset = getRandomInt(2, 5);

        }
        return offset;
    };

    this.getRandomDateGreaterThanOutbound = function (constraint, outboundDate) {
        var offset = 0;
        switch (constraint) {
            case "NONE":
                offset = getRandomInt(outboundDate + 1, 6);
                break;
            case "PARTIAL":
                offset = getRandomInt(outboundDate + 1, 29);
                break;
            case "FULL":
                offset = getRandomInt(outboundDate + 1, 36);
                break;
            default:
                offset = getRandomInt(outboundDate + 1, 6);

        }
        return offset;
    };

    this.getRandomHoldFareDate = function () {
        var offset = getRandomInt(15,28);
        return offset;
    };

    this.getRandomEmail = function () {
        var randomInt = getRandomInt(1,99999);
        var name = (Faker.name.firstName()).toLowerCase();
        return name + randomInt + '@ryanair.ie';
    };

    this.getRandomDocumentNumber = function () {
        var randomInt = getRandomInt(1,99999);
        return 'DYA' + randomInt ;
    };

    this.getRandomMobileNumber = function () {
        var randomInt = getRandomInt(1,99999);
        return '5245' + randomInt ;
    };

    this.getRandomNumber = function (lower, upper) {
        var randomNumber = getRandomInt(lower, upper);
        return randomNumber;
    };

    this.getRandomAirportFromArray = function () {
       // var destinationArray = ["Dub", "Krk", "Bud", "Lis", "Sxf", "Bcn", "Gdn", "Rome", "Riga", "Bgy", "Base", "Buch", "Pra", "Bill", "Hhn", "Tsf"]; // from Stn daily flight
        var destinationArray = ["Dub", "Lis", "Sxf", "Bcn", "Gdn", "Cia", "Bgy", "Base", "Pra", "Bill", "Hhn", "Tsf"];
        var i = getRandomInt(0,11);
        return destinationArray[i];
    };

};

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}


module.exports = TripHelper;

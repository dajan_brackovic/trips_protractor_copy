var Pages = require('../../Pages')
var pages = new Pages();
var params = browser.params.conf;

var musicEquipmentActions = function () {

    var musicEquipmentPage = pages.musicEquipmentPage;
    var pt = pages.potentialTripPage;

    this.isXWorkingPromise = function(){
        musicEquipmentPage.clickCloseButtonX().then(function(){
            browser.sleep(1000);
            pt.openCard(7).click();
        });
    };
    this.isCancelWorkingPromise = function(){
        musicEquipmentPage.cancelButton(1).click().then(function(){
            browser.sleep(1000);
            pt.openCard(7).click();
        });
    };
    this.isESCWorkingPromise = function(){
        browser.actions().sendKeys(protractor.Key.ESCAPE).perform().then(function(){
            browser.sleep(1000);
            pt.openCard(7).click();
        });
    };
    this.selectPlusButtonSameForBothFlights = function (i) {
        musicEquipmentPage.plusButtonSameForBothFlights(i).click();
    };
    this.expectMoreInfoInCollapsedState = function (i) {
        expect((musicEquipmentPage.moreInformationME()).isPresent()).not.toBeTruthy();
    };
    this.selectPlusButtonFlightBack = function (i) {
        musicEquipmentPage.plusButtonFlightBack(i).click();
    };
    this.selectPlusButtonFlightOut = function (i) {
        musicEquipmentPage.plusButtonFlightOut(i).click();
    };
    this.selectMinusButtonSameForBothFlights = function (i) {
        musicEquipmentPage.minusButtonSameForBothFlights(i).click();
    };
    this.selectMinusButtonFlightBack = function (i) {
        musicEquipmentPage.minusButtonFlightBack(i).click();
    };
    this.selectMinusButtonFlightOut = function (i) {
        musicEquipmentPage.minusButtonFlightOut(i).click();
    };
    this.plusButtonValidation = function (i) {
        expect((musicEquipmentPage.plusButtonSameForBothFlights(i)).isPresent()).toBeTruthy();
    };
    this.minusButtonValidation = function (i) {
        expect((musicEquipmentPage.minusButtonSameForBothFlights(i)).isPresent()).toBeTruthy();
    };
    this.plusButtonDisabledSFBF = function (i) {
        expect((musicEquipmentPage.plusButtonDisabled(i)).isPresent()).toBeTruthy();
    };
    this.minusButtonDisabledSFBF = function (i) {
        expect((musicEquipmentPage.minusButtonDisabled(i)).isPresent()).toBeTruthy();
    };
    this.selectCheckBoxSFBF = function (i) {
        musicEquipmentPage.checkBoxSFBF(i).click();
    };
    this.expectCheckBoxSFBFisChecked = function (i) {
        expect((musicEquipmentPage.checkBoxSFBFSelected(i)).isEnabled()).toBeTruthy();
    };
    this.expectCheckBoxSFBFisUnChecked = function (i) {
        expect((musicEquipmentPage.checkBoxSFBFUnSelected(i)).isPresent()).toBeTruthy();
    };
    this.expectMusicEquipmentAmountValidation = function () {
        expect((musicEquipmentPage.amountMusicEquipment()).isPresent()).toBeTruthy();
        expect((musicEquipmentPage.amountMusicEquipment()).isDisplayed()).toBeTruthy();
    };
    this.expectInboundFlightMusicOption = function () {
        expect((musicEquipmentPage.MusicLogo).count()).toEqual(2);
        expect((musicEquipmentPage.plusFirstButton).count()).toEqual(2);
    };
    this.expectMusicEquipmentAmount = function (i,price) {
        expect((musicEquipmentPage.amountMusicEquipment(i).getText())).toEqual(price);
    };
    this.expectMusicEquipmentAmountLeft = function (i,price) {
        expect((musicEquipmentPage.amountMusicEquipmentLeft(i).getText())).toEqual(price);
    };
    this.expectMusicEquipmentAmountRight = function (i,price) {
        expect((musicEquipmentPage.amountMusicEquipmentRight(i).getText())).toEqual(price);
    };
    this.expectEquipmentInformationLabel = function (value) {
        expect((musicEquipmentPage.labelAddMusicEquipment()).getText()).toContain(value);
    };
    this.expectMusicEquipmentTitle = function (value) {
        expect((musicEquipmentPage.titleMusicEquiment()).getText()).toContain(value);
    };
    this.expectMusicEquipmentLableIcon = function () {
        expect((musicEquipmentPage.labelAddMusicEquipmentIcon()).isPresent()).toBeTruthy();
    };
    this.expectCardIsInCollapsedState = function() {
        expect((musicEquipmentPage.plusInformationButton()).isDisplayed()).toBeTruthy();
    };
    this.expectCardIsInExpandedState = function() {
        expect((musicEquipmentPage.minusInformationButton()).isDisplayed()).toBeTruthy();
    };
    this.expandInformationCard = function(){
        musicEquipmentPage.plusInformationButton().click();
    };
    this.expectInformationCardIsDisplayed = function(){
        expect(musicEquipmentPage.informationCardME().isDisplayed()).toBeTruthy();
    };
    this.collapseInformationCard = function(){
        musicEquipmentPage.minusInformationButton().click();
    };
    this.expectMusicEquipmentInformationText = function(value){
        expect((musicEquipmentPage.musicEquipmentInformationCardText()).getText()).toContain(value);
    };
    this.expectMusicEquipmentInformationTextIsHidden = function(){
        expect((musicEquipmentPage.musicEquipmentInformationCardText()).isPresent()).not.toBeTruthy();
    };
    this.clickCancel = function(){
        musicEquipmentPage.cancelButton(1).click();
    };
    this.clickESC = function(){
        browser.actions().sendKeys(protractor.Key.ESCAPE).perform();
    };
    this.clickTAB = function(){
        browser.actions().sendKeys(protractor.Key.TAB).perform();
    };
    this.expectTotalPriceIsDisplayed = function(){
        expect(musicEquipmentPage.cornerPrice().isPresent()).toBeTruthy();
    };
    this.expectXButtonIsDisplayed = function(){
        expect(musicEquipmentPage.closeButtonX().isDisplayed()).toBeTruthy();
    };
    this.expectBackButtonIsDisplayed = function(){
        expect(musicEquipmentPage.backButton().isDisplayed()).toBeTruthy();
    };
    this.expectPassengerCardIsDisplayed = function(i){
        expect(musicEquipmentPage.passengerCard(i).isDisplayed()).toBeTruthy();
    };
    this.clickCloseButtonX = function(){
        musicEquipmentPage.closeButtonX().click();
    };
    this.expectCancelButtonIsDisplayed = function(){
        expect(musicEquipmentPage.cancelButton(1).isDisplayed()).toBeTruthy();
    };
    this.expectInboundTextIsDisplayed = function(i){
        expect(musicEquipmentPage.inboundText(i).isPresent()).toBeTruthy();
    };
    this.expectOutboundTextIsDisplayed = function(i){
        expect(musicEquipmentPage.outboundText(i).isPresent()).toBeTruthy();
    };
    this.expectCentralPriceDisplayed = function(i){
        expect(musicEquipmentPage.centralPrice().isDisplayed()).toBeTruthy();
    };
    this.expectMusicEquipmentAmountNN = function (k, i) {
        expect((musicEquipmentPage.amountMusicEquipment(k).getText())).toEqual(i);
    };
    this.expectMusicEquipmentAmountNNN = function (k, i) {
        expect((musicEquipmentPage.amountMusicEquipmentSeted(k).getText())).toEqual(i);
    };
    this.expectCentralPriceLeftDisplayed = function(i){
        expect(musicEquipmentPage.centralPriceLeft(i).isPresent()).toBeTruthy();
    };
    this.expectConfirmButtonIsDisplayed = function(){
        expect(musicEquipmentPage.confirmButton().isPresent()).toBeTruthy();
    };
    this.expectCentralPriceRightDisplayed = function(i){
        expect(musicEquipmentPage.centralPriceRight(i).isPresent()).toBeTruthy();
    };
    this.expectCentralPrice = function(i,p){
        expect(musicEquipmentPage.centralPrice(i).getText()).toContain(p);
    };
    this.expectCentralPriceNN = function(p){
        expect(musicEquipmentPage.centralPriceNN().getText()).toContain(p);
    };
    this.expectCentralPriceNNN = function(p){
        expect(musicEquipmentPage.centralPriceNNN().getText()).toContain(p);
    };
    this.expectCentralPriceLeft = function(index,price){
        expect(musicEquipmentPage.centralPriceLeft(index).getText().then(function (text) {
            return text.substring(2);
        })).toContain(price);
    };
    this.expectCentralPriceRight = function(i,p){
        expect(musicEquipmentPage.centralPriceRight(i).getText()).toContain(p);
    };
    this.expectMusicEquipmentAmountLeftValidation = function (i) {
        expect((musicEquipmentPage.amountMusicEquipmentLeft(i)).isPresent()).toBeTruthy();
    };
    this.expectMusicEquipmentAmountRightValidation = function (i) {
        expect((musicEquipmentPage.amountMusicEquipmentRight(i)).isPresent()).toBeTruthy();
    };
    this.expectMusicEquipmentAmountValidation = function () {
        expect((musicEquipmentPage.amountMusicEquipment(i)).isPresent()).toBeTruthy();
    };
    this.expectPlusDisableToolTipShown = function () {
        expect((musicEquipmentPage.toolTipDisablePlusShown()).isPresent()).toBeTruthy();
    };
    this.expectPlusDisableToolTipHidden = function () {
        expect((musicEquipmentPage.plusButtonDisabled(0)).isPresent()).toBeTruthy();
    };
    this.expectPlusDisableToolTipText = function (i,p) {
        expect((musicEquipmentPage.toolTipDisablePlusText(i))).toEqual(p);
    };
    this.expectPlusDisableToolTipTextAmount = function (p) {
        expect((musicEquipmentPage.toolTipDisablePlusTextAmount())).toEqual(p);
    };
    this.expectInboundText = function(i){
        expect(musicEquipmentPage.inboundText(0).getText()).toEqual(i);
    };
    this.expectOutboundText = function(i){
        expect(musicEquipmentPage.outboundText(0).getText()).toEqual(i);
    };
    this.expectPassengerTypeAndNumber = function (i, k) {
        expect((musicEquipmentPage.passengerTypeAndNumber(i)).getText()).toEqual(k);
    };
    this.expectQuantityCardisDisplayed = function(i){
        expect(musicEquipmentPage.quantityCard(i).isDisplayed()).toBeTruthy();
    };
    this.expectPassengerLogoIsDisplayed = function(i){
        expect(musicEquipmentPage.passengerLogo(i).isDisplayed()).toBeTruthy();
    };
    this.expectTotalPriceToBe = function (price) {
        expect(musicEquipmentPage.cornerPrice().getText().then(function (text) {
            return text.substring(7);
        })).toContain(price);
    };
    this.expectMusicLogoIsDisplayed = function(i){
        expect(musicEquipmentPage.musicLogo(i).isPresent()).toBeTruthy();
    };
    this.expectCheckBoxIsDisplayed = function(i){
        expect(musicEquipmentPage.checkBoxSFBF(i).isDisplayed()).toBeTruthy();
    };
    this.expectTotalPrice = function(p){
        expect(musicEquipmentPage.cornerPrice().getText()).toContain(p);
    };
    this.expectMusicEquipmentAmountLeftValidation = function (i) {
        expect((musicEquipmentPage.amountMusicEquipmentLeft(i)).isPresent()).toBeTruthy();
    };
    this.expectMusicEquipmentAmountRightValidation = function (i) {
        expect((musicEquipmentPage.amountMusicEquipmentRight(i)).isPresent()).toBeTruthy();
    };
    this.confirmButtonValidation = function () {
        expect((musicEquipmentPage.confirmButton()).isPresent()).toBeTruthy();
    };
    this.clickConfirmButton = function () {
        musicEquipmentPage.confirmButton().click();
    };
    this.expectConfirmButtonIsDisabled = function () {
        expect((musicEquipmentPage.confirmButton()).isPresent()).toBeTruthy();
    };
    this.expectConfirmButtonEnabled = function () {
       expect((musicEquipmentPage.confirmButton()).isEnabled()).toBeTruthy();
    };
    this.expectMusicEquipmentAmount0 = function (k,i) {
        expect((musicEquipmentPage.amountMusicEquipment(k).getText())).toEqual(i);
    };
    this.expectMusicEquipmentAmount1 = function (k,i) {
        expect((musicEquipmentPage.amountMusicEquipmentSeted(k).getText())).toEqual(i);
    };
    this.selectPlusButtonSameForBothFlightsDisable = function (i) {
        musicEquipmentPage.plusButtonDisabled(i).click();
    };
    this.expectIconForPassangerMusic = function (i) {
        expect(musicEquipmentPage.iconForPassanger(i).isDisplayed()).toBeTruthy();
    };
    this.expectInformationTextMusic = function () {
        expect((musicEquipmentPage.informationText().getText())).toEqual(params.music.info);
    };
    this.expectMoreInformationTextMusic = function () {
        expect((musicEquipmentPage.moreInformationME().getText())).toEqual(params.music.extraInfo);
    };
    this.expectPlusButtonEnabled = function (i) {
        expect(musicEquipmentPage.plusButtonDisabled(i).isEnabled()).toBeTruthy();
    };
    this.expectMinusButtonIsDisabled = function() {
        expect(musicEquipmentPage.minusButtonDisabled().isDisabled()).toBeTruthy();
    };
    this.expectMusicEquipmentAdded = function(numberOfEquipment) {
        expect(musicEquipmentPage.musicEquipmentAdded().getText()).toContain(numberOfEquipment)
    };
    this.expectPassengerTypeAndNumberInfantIcon = function (indexOfPassanger, nameOfPassanger) {
        expect((musicEquipmentPage.passengerTypeAndNumber(indexOfPassanger)).getText()).toContain(nameOfPassanger);
        expect((musicEquipmentPage.musicInfantIcon(indexOfPassanger).isDisplayed())).toBeTruthy();
    };
    this.minusButtonEnabled = function (index) {
        expect((musicEquipmentPage.minusButtonDisabled(index)).isEnabled()).not.toBeTruthy();
    };
    this.expectMusicActiveEquipmentPurchased = function(inbound, outbound) {
        expect((musicEquipmentPage.musicActiveInboundEquipment().getText())).toContain(inbound);
        expect((musicEquipmentPage.musicActiveOutboundEquipment().getText())).toContain(outbound);
    };
    this.expectConfirmButtonIsEnabled = function () {
        expect((musicEquipmentPage.confirmButtonEnabled()).isPresent()).toBeTruthy();
    };
    this.expectMaximumMessageIsDisplayed = function (indexOfPassenger) {
        expect(musicEquipmentPage.maximumReachedEquipment(indexOfPassenger).isDisplayed()).toBeTruthy();
    };
    this.expectMaximumMessageReached = function(indexOfPassenger,textDisplayed) {
        expect(musicEquipmentPage.maximumMessage(indexOfPassenger).getText()).toContain(textDisplayed);
    };

    

    this.assertOnAmountOfEquipmentFlightOut = function (index, i) {
        expect((musicEquipmentPage.amountOfEquipmentFlightOut(index).getText())).toEqual(i);
    };

    this.assertOnAmountOfEquipmentFlightBack = function (index, i) {
        expect((musicEquipmentPage.amountOfEquipmentFlightBack(index).getText())).toEqual(i);
    };
};
module.exports = musicEquipmentActions;
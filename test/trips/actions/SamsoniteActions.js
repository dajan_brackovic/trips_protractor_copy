var Pages = require('../../Pages')
var pages = new Pages();

var SamsoniteActions = function () {

    var samsonitePage = pages.samsonitePage;
    var fohHomePage = pages.fOHHomePage;

    this.goToSamsonitePage = function(){
        actions.fOHActions.goToPage();
        browser.driver.getCurrentUrl().then(function(url) {
            browser.get(url + 'samsonite/buy-now');
            actions.fOHActions.handlePreloader();
        });
    };

    this.clickSamsoniteFromFooter = function () {
        actions.fOHActions.goToPage();
        fohHomePage.footerSamsoniteLink().click();
    };

    this.expectTitleTermsAndConditions = function (title) {
        expect(samsonitePage.titleTermsAndConditions().getText()).toContain(title);
    };
    this.clickToTermsAndConditions = function (i) {
        actions.fOHActions.handlePreloader();
        browser.sleep(1000);
        browser.executeScript('arguments[0].scrollIntoView()', samsonitePage.linkTermsAndConditions().getWebElement());
        samsonitePage.linkTermsAndConditions().click();
        browser.sleep(1000);
        browser.getAllWindowHandles().then(function (handles) {
            var secondWindowHandle = handles[1];
            var firstWindowHandle = handles[0];
            //the focus moves on the newest opened tab
            browser.switchTo().window(secondWindowHandle).then(function () {
                browser.sleep(1000);
                //check if the right page is opened
                expect(browser.driver.getCurrentUrl()).toContain(i);
                browser.switchTo().window(firstWindowHandle).then(function () {
                    browser.driver.close();
                });
                browser.switchTo().window(secondWindowHandle)
            });
        });
    };
    this.expectCheckOutIsEnabled = function () {
        expect((samsonitePage.clickCheckOutSamsonite()).isEnabled()).toBeTruthy();
    };
    this.expectCheckOutIsDisabled = function () {
        expect((samsonitePage.clickCheckOutSamsonite()).isEnabled()).not.toBeTruthy();
    };
    this.closeWithESC = function () {
        browser.actions().sendKeys(protractor.Key.ESCAPE).perform();
    };
    this.expectBagType = function (index, bagName) {
        expect(samsonitePage.bagType(index).getText()).toContain(bagName);
    };
    this.navigateAway = function () {
        samsonitePage.ryanairLogo().click();
    };
    this.expectPopUpIsDisplayed = function () {
        expect((samsonitePage.popUp()).isPresent()).toBeTruthy();
    };
    this.expectBagHolderIsDisplayed = function (index) {
        expect((samsonitePage.bagHolder(index)).isPresent()).toBeTruthy();
    };
    this.expectFullscreenBagImage = function () {
        expect((samsonitePage.fullscreenBagImage()).isPresent()).toBeTruthy();
    };
    this.clickAddSamsoniteBags = function (index) {
        samsonitePage.clickAddSamsoniteBags(index).click();
    };
    this.clickRemoveSamsoniteBags = function (index) {
        samsonitePage.clickRemoveSamsoniteBags(index).click();
    };
    this.clickCheckOut = function (index) {
        samsonitePage.clickCheckOutSamsonite().click();
    };
    this.expectSubtotalSamsonite = function (amount) {
        expect((samsonitePage.subtotalSamsonite()).getText()).toContain(amount);
    };
    this.clickExpandQuestions = function (index) {
        samsonitePage.expandQuiestions(index).click();
    };
    this.expectAmountSamsonite = function (index, amount) {
        expect((samsonitePage.amountOfSamsonite(index)).getText()).toEqual(amount);
    };

    this.clickToGetLargerImage = function (index) {
        samsonitePage.largerImage(index).click();
    };
    this.expectPriceSamsonite = function (index, amount) {
        expect((samsonitePage.priceByBag(index)).getText()).toContain(amount);
    };
    this.clickOnBagImage = function (index) {
        samsonitePage.bagImage(index).click();
    };
    this.expectSamsoniteFAQAnswer1IsDisplayed = function () {
        expect((samsonitePage.samsoniteFAQAnswer1()).isPresent()).toBeTruthy();
    };
    this.expectSamsoniteFAQAnswer2IsDisplayed = function () {
        expect((samsonitePage.samsoniteFAQAnswer2()).isPresent()).toBeTruthy();
    };
    this.expectSamsoniteFAQAnswer3IsDisplayed = function () {
        expect((samsonitePage.samsoniteFAQAnswer3()).isPresent()).toBeTruthy();
    };

    this.fillDeliveryDetails = function(){
        samsonitePage.samsonitePaymentFirstName().sendKeys("FirstName");
        samsonitePage.samsonitePaymentLastName().sendKeys("LastName");
        samsonitePage.samsonitePaymentEmail().sendKeys("auto@ryanair.ie");
        samsonitePage.samsonitePaymentPhoneCodeDropdown().sendKeys("Ireland");
        samsonitePage.samsonitePaymentPhoneNumber().sendKeys("65432188");
        samsonitePage.samsonitePaymentAdressLine1().sendKeys("Address");
        samsonitePage.samsonitePaymentCity().sendKeys("Dublin");
        samsonitePage.samsonitePaymentCountryDropdown().sendKeys("Ireland");
        samsonitePage.samsonitePaymentPostcode().sendKeys("31260");
    };

    this.fillPaymentDetails = function(card){
        samsonitePage.textFieldCardNumber().sendKeys(card.cardNumber);
        samsonitePage.selectPaymentTypeDropDownOption(card.type).click();
        samsonitePage.selectExpiryMonthDropDownOption(card.expiryDateMonth).click();
        samsonitePage.selectExpiryYearDropDownOption(card.expiryDateYear).click();
        samsonitePage.textFieldCVV().sendKeys(card.cvv);
        samsonitePage.textFieldCardHoldersName().sendKeys("Automation User");
        samsonitePage.textFieldStreetName().sendKeys("Swords Road");
        samsonitePage.textFieldCity().sendKeys("Dublin");
        samsonitePage.textFieldPostCode().sendKeys("01");
        samsonitePage.paymentTermsAndConditions().click();
        samsonitePage.btnPaymentContinue().click();
    };

    this.verifySuccessfulOrder = function(card){
        expect(samsonitePage.orderSuccessful().isDisplayed()).toBe(true);

        samsonitePage.orderSuccessful().getText().then(function(text){
            reporter.addMessageToSpec("Order Succcessful: " + text);
        })
    };



};
module.exports = SamsoniteActions;
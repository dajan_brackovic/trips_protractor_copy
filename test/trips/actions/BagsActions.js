var Pages = require('../../Pages')
var pages = new Pages()
var sprintf = require("sprintf").sprintf;

var BagsActions = function () {
    var bagspage = pages.bagsPage;
    var EC = protractor.ExpectedConditions;

    this.clickBtnIncrementNormalBag = function () {
        bagspage.btnIncrementNormalBag().click();
    };

    this.clickBtnIncrementLargeBag = function () {
        bagspage.btnIncrementLargeBag().click();
    };

    this.clickBtnDecrementNormalBag = function () {
        bagspage.btnDecrementNormalBag().click();
    };

    this.clickBtnDecrementLargeBag = function () {
        bagspage.btnDecrementLargeBag().click();
    };

    this.clickBagsBtnConfirm = function () {
        bagspage.bagsBtnConfirm().click();
    };

    this.clickBagsBtnCancel = function () {
        browser.wait(EC.presenceOf(bagspage.bagsBtnCancel()), 5000);
        bagspage.bagsBtnCancel().click();
    };

    this.assertOnBagsSubTotalAndTotal = function () {
        var subTotal = bagspage.bagsSubTotal().getText().then(function (subTotal) {
            var total = bagspage.bagsTotal().getText().then(function (total) {
                expect(subTotal).toEqual(total);
                reporter.addMessageToSpec("Sub Total: " + subTotal + " Total: " + total);
            });
        });
    };

    this.assertOnAdultAndChildBagPrice = function () {
        var adultBag = bagspage.bagsSubTotalList(1).getText().then(function (adultBagPrice) {
            var childBag = bagspage.bagsSubTotalList(3).getText().then(function (childBagPrice) {
                var adultBagPriceVal = parseFloat(adultBagPrice.substring(2));
                var childBagPriceVal = parseFloat(childBagPrice.substring(2));
                expect(adultBagPriceVal).toEqual(childBagPriceVal*2);
                reporter.addMessageToSpec("Adult Bag Price: " + adultBagPriceVal + " Child Bag Price: " + childBagPriceVal);
            });
        });
    };

    this.assertOnDiscountMessageForChildBag = function () {
        expect(bagspage.discountMessageForChildBag().isDisplayed()).toBeTruthy();
    };

    this.addOneNormalBag = function () {
        this.clickBtnIncrementNormalBag();
        var normalBagsSelected = bagspage.numberOfNormalBagsSelected().getText().then(function (normalBagsSelected) {
            reporter.addMessageToSpec("No. Of Normal Bags Selected: " + normalBagsSelected);
        });
    };

    this.addOneNormalBagForInboundSpanishDomestic = function () {
        bagspage.btnIncrementNormalBagForInboundSpanishDomestic().click();
    };

    this.clickBtnIncrement = function (index) {
        bagspage.btnIncrement(index).click();
    };

    this.selectOneLargeAndOneNormalBagForEachPax = function () {
        bagspage.btnIncrementList().count().then(function (originalCount) {
            for (var i = 0; i <= originalCount - 1; i++) {
                actions.bagsActions.clickBtnIncrement(i);
            }
        });
    };

    // Special action for C632431 in Active Trip suite
    this.selectOneLargeAndOneNormalBagForMultiPax = function (startIndex, endIndex) {
        bagspage.btnIncrementList().count().then(function (originalCount) {
            for (var i = startIndex; i < originalCount - endIndex; i++) {
                actions.bagsActions.clickBtnIncrement(i);
            }
        });
    };


    this.addOneLargeBag = function () {
        this.clickBtnIncrementLargeBag();
        var largeBagsSelected = bagspage.numberOfLargeBagsSelected().getText().the(function (largeBagsSelected) {
            reporter.addMessageToSpec("No. Of Normal Bags Selected: " + largeBagsSelected);
        });
    };

    this.addOneNormalOneLargeBag = function () {
        this.clickBtnIncrementNormalBag();
        this.clickBtnIncrementLargeBag();
        var normalBagsSelected = bagspage.numberOfNormalBagsSelected().getText().then(function (normalBagsSelected) {
            var largeBagsSelected = bagspage.numberOfLargeBagsSelected().getText().then(function (largeBagsSelected) {
                reporter.addMessageToSpec("No. Of Large Bags Selected: " + largeBagsSelected + " No. Of Normal Bags Selected: " + normalBagsSelected);
            });
        });
    };

    this.assertBagsOnPriceBreakDown = function () {
        var total = bagspage.bagsTotal().getText().then(function (total) {
            bagspage.bagsBtnConfirm().click();
            actions.priceBreakDownActions.clickOnOpenPriceBreakDown();
            var priceOfNormalBag = pages.priceBreakDownPage.normalBagsValue().getText().then(function (priceOfNormalBag) {
                var priceOfLargeBag = pages.priceBreakDownPage.largeBagsValue().getText().then(function (priceOfLargeBag) {
                    var totalVal = parseFloat(total.substring(2)),
                        priceOfNormalBagVal = parseFloat(priceOfNormalBag.substring(2)),
                        priceOfLargeBagVal = parseFloat(priceOfLargeBag.substring(2));
                    reporter.addMessageToSpec(sprintf("PriceBreakDown Normal Bags Value: %s + PriceBreakDown Large Bags Value: %s = Bags Card Total: %s", priceOfNormalBagVal, priceOfLargeBagVal, totalVal));
                    //TODO fix this expect when bags id is added
                    //expect(priceOfNormalBagVal + priceOfLargeBagVal).toEqual(totalVal);
                });
            });
        });
    };

};

module.exports = BagsActions;

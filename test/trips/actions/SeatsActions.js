var Pages = require('../../Pages')
var pages = new Pages()
var PriceBreakDownActions = require('./PriceBreakDownActions.js');
var priceBreakDownActions = new PriceBreakDownActions();

var SeatsActions = function() {
    var seatspage = pages.seatsPage;
    var EC = protractor.ExpectedConditions;
    var seatsActions = this;

    this.selectA1Seat = function() {
        seatspage.a1Seat().click();
    };

    this.clickBtnNext = function() {
        seatspage.btnNext().click();
    };

    this.clickBtnCancel = function() {
        seatspage.btnCancel().click();
    };

    this.clickBtnConfirm = function() {
        seatspage.btnConfirm().click();
    };

    this.clickBtnConfirmCheckIn = function() {
        seatspage.btnConfirmCheckIn().click();
    };

    this.selectOneSeat = function( tripWay ) {
        seatspage.anyAvailableSeat().click();
        seatspage.btnNext().click();
        if(tripWay === "oneway") {
            var out = seatspage.seatOut().getText().then(function (out) {
                reporter.addMessageToSpec("Seat Out: " + out);
            });
        }
    };

    this.selectOneSeatCheckIn = function( tripWay ) {
        seatspage.anyAvailableSeat().click();
        seatspage.btnNextCheckIn().click();
        if(tripWay === "oneway") {
            var out = seatspage.seatOut().getText().then(function (out) {
                reporter.addMessageToSpec("Seat Out: " + out);
            });
        }
    };

    this.selectOneSeatPremiumCheckIn = function( tripWay ) {
        seatspage.anyAvailablePremiumSeat().click();
        seatspage.btnNextCheckIn().click();
        if(tripWay === "oneway") {
            var out = seatspage.seatOut().getText().then(function (out) {
                reporter.addMessageToSpec("Seat Out: " + out);
            });
        }
    };

    this.selectOneSeatPremium = function( tripWay ) {
        seatspage.anyAvailablePremiumSeat().click();
        seatspage.btnNext().click();
        if(tripWay === "oneway") {
            var out = seatspage.seatOut().getText().then(function (out) {
                reporter.addMessageToSpec("Seat Out: " + out);
            });
        }
    };

    this.returnOneWaySeat = function () {
        return seatspage.seatOut().getText();
    };

    this.selectMultipleSeats = function( tripWay, numAdult, numTeen, numChild, numInfants ) {
        reporter.addMessageToSpec("Adults: " + numAdult + " Teens: " + numTeen + " Children: " + numChild + " Infants: " + numInfants);
        var paxWithSeats = (numAdult + numTeen + numChild);
        var adultsWithInfants = numInfants;
        var paxWithoutInfants = (numAdult + numTeen + numChild) - numInfants;

        for(var i = 0; i < adultsWithInfants; i++){
            seatspage.listAnyAvailableInfantSeat(i).click();
        }

        for(var i = 0; i < paxWithoutInfants; i++) {
            seatspage.listAnyAvailableSeat(i).click();
        }
        seatspage.btnNext().click();

        if(tripWay === "oneway") {
            for (i = 0; i < paxWithSeats; i++) {
                var out = seatspage.seatOutList(i).getText().then(function (out) {
                    reporter.addMessageToSpec("Seat Out: " + out);
                });
            }
        }
    };

    this.selectSameSeatReturn = function () {
        seatspage.sameSeatReturn().isDisplayed().then(function (isDisplayed) {
            if (isDisplayed) {
                seatspage.sameSeatReturn().click();
                var out = seatspage.seatOut().getText().then(function (out) {
                    var back = seatspage.seatBack().getText().then(function (back) {
                        reporter.addMessageToSpec("Seat Out: " + out + " Seats Back: " + back);
                    });
                });
            }
            else {
                seatspage.anyAvailableSeat().click();
                seatspage.btnNext().click();
            }
        })
    };

    this.selectSameSeatReturnActive = function () {
        seatspage.btnNext().click();
        seatspage.sameSeatReturn().isDisplayed().then(function (isDisplayed) {
            if (isDisplayed) {
                seatspage.sameSeatReturn().click();
            }
            else {
                seatspage.anyAvailableSeat().click();
                seatspage.btnNext().click();
            }
        })
    };

    this.selectSamePremiumSeatReturn = function () {
        seatspage.sameSeatReturn().isDisplayed().then(function (isDisplayed) {
            if (isDisplayed) {
                seatspage.sameSeatReturn().click();
                var out = seatspage.seatOut().getText().then(function (out) {
                    var back = seatspage.seatBack().getText().then(function (back) {
                        reporter.addMessageToSpec("Seat Out: " + out + " Seats Back: " + back);
                    });
                });
            }
            else {
                seatspage.anyAvailablePremiumSeat().click();
                seatspage.btnNext().click();
            }
        })
    };

    this.returnTwoWaySeatOut = function () {
        return seatspage.seatOut().getText();
    };

    this.returnTwoWaySeatIn = function () {
        return seatspage.seatBack().getText();
    };

    this.selectMultiSameSeatReturn = function (numAdult, numTeen, numChild, numInfants) {
        var paxWithSeats = numAdult + numTeen + numChild;
        seatspage.sameSeatReturn().isDisplayed().then(function (isDisplayed) {
            if (isDisplayed) {
                seatspage.sameSeatReturn().click();
            }
            else {
                seatsActions.selectMultiDiffSeatsOnReturnPlane(numAdult, numTeen, numChild, numInfants);
            }

            for (var j = 0; j < paxWithSeats; j++) {
                var out = seatspage.seatOutList(j).getText().then(function (out) {
                    reporter.addMessageToSpec("Seat Out: " + out);
                });

                var back = seatspage.seatBackList(j).getText().then(function (back) {
                    reporter.addMessageToSpec("Seat Back: " + back);
                });
            }
        })
    };

    this.selectDiffSeatReturn = function () {
        seatspage.diffSeatReturn().isDisplayed().then(function (isDisplayed) {
            if (isDisplayed) {
                seatspage.diffSeatReturn().click();
                seatspage.vacantSeats().count().then(function (vacantSeats) {
                    browser.executeScript('arguments[0].scrollIntoView()', seatspage.vacantSeats().get(vacantSeats - 1).getWebElement());
                    seatspage.vacantSeats().get(vacantSeats - 1).click();
                    seatspage.btnNext().click();

                    var out = seatspage.seatOut().getText().then(function (out) {
                        var back = seatspage.seatBack().getText().then(function (back) {
                            reporter.addMessageToSpec("Seat Out: " + out + " Seats Back: " + back);
                        });
                    });

                });
            }
            else {
                seatspage.anyAvailableSeat().click();
                seatspage.btnNext().click();
            }
        })
    };

    this.selectMultiDiffSeatReturn = function (numAdult, numTeen, numChild, numInfants) {
        seatspage.diffSeatReturn().isDisplayed().then(function (isDisplayed) {
            if (isDisplayed) {
                seatspage.diffSeatReturn().click();
                seatsActions.selectMultiDiffSeatsOnReturnPlane(numAdult, numTeen, numChild, numInfants);
            }
            else {
                seatsActions.selectMultiDiffSeatsOnReturnPlane(numAdult, numTeen, numChild, numInfants);
            }
        })
    };

    this.selectMultiDiffSeatsOnReturnPlane = function( numAdult, numTeen, numChild, numInfants ){
        var paxWithSeats = (numAdult + numTeen + numChild);
        var adultsWithInfants = numInfants;
        var paxWithoutInfants = (numAdult + numTeen + numChild) - numInfants;

        seatspage.vacantInfantSeats().count().then(function (count) {
            for (var i = 1; i <= adultsWithInfants; i++) {
                browser.executeScript('arguments[0].scrollIntoView()', seatspage.listVacantInfantSeats(count - i).getWebElement());
                seatspage.listVacantInfantSeats(count - i).click();
            }
        });

        seatspage.vacantAdultSeats().count().then(function (count) {
            for (var i = 1; i <= paxWithoutInfants; i++) {
                browser.executeScript('arguments[0].scrollIntoView()', seatspage.listVacantAdultSeats(count - i).getWebElement());
                seatspage.listVacantAdultSeats(count - i).click();
            }
        });
        seatspage.btnNext().click();

        for (var j = 0; j < paxWithSeats; j++) {
            var out = seatspage.seatOutList(j).getText().then(function (out) {
                reporter.addMessageToSpec("Seat Out: " + out);
            });

            var back = seatspage.seatBackList(j).getText().then(function (back) {
                reporter.addMessageToSpec("Seat Back: " + back);
            });
        }
    };


    this.confirmSeat = function () {
        var total = seatspage.seatsTotalVal().getText().then(function (total) {
            seatspage.btnConfirm().click();
            priceBreakDownActions.clickOnOpenPriceBreakDown();
            var priceOfSeat = pages.priceBreakDownPage.seatsValue().getText().then(function (priceOfSeat) {
                //TODO Temporarily commented out while ID is added for Seat Price
                //expect(priceOfSeat).toEqual(total);
                reporter.addMessageToSpec("Seat Card Total: " + total + " PriceBreakDown Seats Total: " + priceOfSeat);
            });
        });
    };

    this.confirmBusinessSeatPrice = function () {
        var total = seatspage.seatsTotalVal().getText().then(function (total) {
            seatspage.btnConfirm().click();
            actions.extrasActions.skipExtras();
            pages.priceBreakDownPage.businessSeatsValue().getText().then(function (priceOfSeat) {
                //TODO Temporarily commented out while ID is added for Seat Price
                //expect(priceOfSeat).toContain(total);
                reporter.addMessageToSpec("Seat Card Total: " + total + " PriceBreakDown Seats Total: " + priceOfSeat);
            });
        });
    };

    this.clickConfirmSeatsButton = function(){
        seatspage.btnConfirm().click();
    };

    this.assertOnSeatMapDisplayed = function () {
        expect(seatspage.seatMap().isDisplayed()).toBeTruthy();
    };

    this.clickBtnPriorityBoarding = function () {
        browser.wait(EC.presenceOf( seatspage.btnPriorityBoarding()), 5000);
        seatspage.btnPriorityBoarding().click();
    };

    this.clickBtnRemovePriorityBoarding = function () {
        browser.wait(EC.presenceOf( seatspage.btnPriorityBoarding()), 5000);
        seatspage.btnRemovePriorityBoarding().click();
    };

    this.confirmSeatAndPriorityBoardingAdded = function (seatsTotal) {

        expect(seatsTotal).toBeLessThan(seatspage.seatsTotalVal().getText());

            seatspage.btnConfirm().click();
            priceBreakDownActions.clickOnOpenPriceBreakDown();
            var priceOfSeat = pages.priceBreakDownPage.seatsValue().getText().then(function (priceOfSeat) {
                //TODO Temporarily commented out while ID is added for Seat Price
               // expect(priceOfSeat).toEqual(seatsTotal);
                reporter.addMessageToSpec("Seat Card Total: " + seatsTotal + " PriceBreakDown Seats Total: " + priceOfSeat);
            });

        expect(pages.priceBreakDownPage.priorityBoardingOnPriceBreakDown().isPresent()).toBe(true);
    };

    this.returnSeatsTotal = function () {
        return seatspage.seatsTotalVal().getText()
    };


    this.assertOnTotalPriceOFSeatAndPriorityBoardingAdded = function (seatsTotal) {
        expect(seatsTotal).toBeLessThan(seatspage.seatsTotalVal().getText());
    };

    this.assertOnTotalPriceOFSeatAndPriorityBoardingRemoved = function (seatsTotal) {
        expect(seatsTotal).toEqual(seatspage.seatsTotalVal().getText());
    };


};

module.exports = SeatsActions;
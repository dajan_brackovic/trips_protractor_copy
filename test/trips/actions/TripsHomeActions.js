var Pages = require('../../Pages')
var pages = new Pages();

var HomeActions = function () {
    var tripsHomePage = pages.tripsHomePage;
    var fohHomePage = pages.fOHHomePage;
    var EC = protractor.ExpectedConditions;

    var listFlightCards = [];
    var totalAvailability = 0;

    this.goHome = function () {
        tripsHomePage.gotoHome();
    }

    this.selectOneWayFlights = function () {
        var EC = protractor.ExpectedConditions;
        browser.wait(EC.presenceOf(tripsHomePage.fareSelection(1)), 10000);
        tripsHomePage.fareSelection(1).click();
        tripsHomePage.btnHomeBottomContinue().click();
    };


    this.selectReturnFlights = function (type) {
        element.all(by.repeater('flight in flights track by flight.flightKey | orderBy:myOrder')).count().then(function (count) {
            if (type === 'standard') {
                var fareIndex = 1;
                var returnIndex = (count) + fareIndex;
                tripsHomePage.fareSelection(fareIndex).click();
                tripsHomePage.fareSelection(returnIndex).click();
            } else {
                var fareIndex = 2;
                var returnIndex = (count) + fareIndex;
                tripsHomePage.fareSelection(fareIndex).click();
                tripsHomePage.fareSelection(returnIndex).click();
            }
        });
        tripsHomePage.btnHomeBottomContinue().click();
    }

    this.selectAFlight = function (indexStandard, indexBusiness, type, wayType) {
        element.all(by.xpath("//*[@type='outbound'] //*[@class='error-info']")).count().then(function (countOutError) {
            element.all(by.xpath("//*[@type='inbound'] //*[@class='error-info']")).count().then(function (countInError) {

                browser.wait(EC.presenceOf(tripsHomePage.getBusinessPlus()), 5000); // Waits for the element getBusinessPlus to be present on the dom

                if (type === "standard") {
                    if (wayType === "twoway") {
                        tripsHomePage.cardPriceStandartFare("outbound", indexStandard).click();
                        tripsHomePage.cardPriceStandartFare("inbound", indexStandard).click();
                    }
                    else {
                        tripsHomePage.cardPriceStandartFare("outbound", indexStandard).click();
                    }
                }
                else {
                    if (wayType === "twoway") {
                        tripsHomePage.cardPriceBusinessFare("outbound", indexBusiness).click();
                        tripsHomePage.cardPriceBusinessFare("inbound", indexBusiness).click();
                    }
                    else {
                        tripsHomePage.cardPriceBusinessFare("outbound", indexBusiness).click();
                    }
                }

            });
        });
        this.clickBtnHomeBottomContinue();
    };

    this.selectAFlightOnly = function (indexStandard, indexBusiness, type, wayType) {
        element.all(by.xpath("//*[@type='outbound'] //*[@class='error-info']")).count().then(function (countOutError) {
            element.all(by.xpath("//*[@type='inbound'] //*[@class='error-info']")).count().then(function (countInError) {
                browser.wait(EC.presenceOf(tripsHomePage.getBusinessPlus()), 5000); // Waits for the element getBusinessPlus to be present on the dom
                if (type === "standard") {
                    if (wayType === "twoway") {
                        tripsHomePage.cardPriceStandartFare("outbound", indexStandard).click();
                        tripsHomePage.cardPriceStandartFare("inbound", indexStandard).click();
                    }
                    else {
                        tripsHomePage.cardPriceStandartFare("outbound", indexStandard).click();
                    }
                }
                else {
                    if (wayType === "twoway") {
                        tripsHomePage.cardPriceBusinessFare("outbound", indexBusiness).click();
                        tripsHomePage.cardPriceBusinessFare("inbound", indexBusiness).click();
                    }
                    else {
                        tripsHomePage.cardPriceBusinessFare("outbound", indexBusiness).click();
                    }
                }
            });
        });
        actions.extrasActions.xOutReserveSeatPopUp();
    }

    this.clickBtnHomeBottomContinue = function () {
        tripsHomePage.btnHomeBottomContinue().click();
        actions.extrasActions.xOutReserveSeatPopUp();
        browser.sleep(2000);
    };

    this.assertOnBtnHomeBottomContinue = function (boolean) {
        expect(tripsHomePage.btnHomeBottomContinue().isEnabled()).toBe(boolean);
    };

    this.clickNoFlightCarousel = function () {
        browser.sleep(5000);
        tripsHomePage.selectANoFlightCarousel().click();
    };

    this.getTextassertNoFlightsMessage = function () {
        return tripsHomePage.assertNoFlightsMessage().getText();
    }

    this.checkAvailabilityAmount = function (expectedAmount) {
        element.all(by.repeater('flight in flights track by flight.flightKey | orderBy:myOrder')).count().then(function (count) {
            expect(count).toBeGreaterThan(expectedAmount);
        });
    }

    this.getFareType = function () {
        return tripsHomePage.fareType(1).getText();
    }

    this.clickCardType = function (cardIndex) {
        tripsHomePage.cardStandardFareCard(cardIndex).click();
    }


    this.clickPriceBreakdown = function () {
        tripsHomePage.priceBreakdownDropdown().click();
    }


    this.checkPriceDropdown = function () {
        expect(tripsHomePage.fareTotalPriceContainer().isDisplayed()).toBeTruthy();

    };

    this.checkPriceCreditCardOption = function () {
        expect(tripsHomePage.creditCardSelector().isDisplayed()).toBeTruthy();

    };

    this.checkPriceCreditCardFee = function () {
        expect(tripsHomePage.creditCardFee().isDisplayed()).toBeTruthy();
        //TODO match text to regular expression

    };


    this.clickCreditFee = function () {
        browser.driver.actions().mouseMove(tripsHomePage.creditCardSelector()).perform();
        tripsHomePage.creditCardSelector().click();
    };

    this.checkCreditCardFee = function () {
        tripsHomePage.lastItemInList().getText();
        console.log("tripsHomePage.lastItemInList().getText()")

        expect(tripsHomePage.lastItemInList().getText().equalTo('Payment Fee'));
    };

    this.assertFlightCardOutBoundInformation = function () {
        expect(tripsHomePage.outboundFlightCardInfoBoxTime().getText()).toContain(tripsHomePage.cardDepartureTimeSelected("outbound").getText());
        expect(tripsHomePage.outboundFlightCardInfoBoxTime().getText()).toContain(tripsHomePage.cardArrivalTimeSelected("outbound").getText());
        expect(tripsHomePage.outboundFlightCardInfoBoxFlightNumber().getText()).toContain(tripsHomePage.cardFlightNumberSelected("outbound").getText());
    }

    this.assertFlightCardInBoundInformation = function () {
        expect(tripsHomePage.inboundFlightCardInfoBoxTime().getText()).toContain(tripsHomePage.cardDepartureTimeSelected("inbound").getText());
        expect(tripsHomePage.inboundFlightCardInfoBoxTime().getText()).toContain(tripsHomePage.cardArrivalTimeSelected("inbound").getText());
        expect(tripsHomePage.inboundFlightCardInfoBoxFlightNumber().getText()).toContain(tripsHomePage.cardFlightNumberSelected("inbound").getText());
    }

    this.setCards = function () {
        var index = 1;
        var destination = "outBound";
        var card = require('./helpers/tripshome/AvailabilityCard.js')
        card.FLIGHT_NO = tripsHomePage.cardFlightNo(destination, index).getText();
        card.ORIGIN_STATION = tripsHomePage.cardOrigin(destination, index).getText();
        card.DEPARTURE_TIME = tripsHomePage.cardDepartureTime(destination, index).getText();
        card.FLIGHT_TIME = tripsHomePage.cardFlightTime(destination, index).getText();
        card.DESTINATION_STATION = tripsHomePage.cardDestination(destination, index).getText();
        card.ARRIVAL_TIME = tripsHomePage.cardArrivalTime(destination, index).getText();
        card.STANDARD_FARE_PRICE = tripsHomePage.cardStandardFareCard(destination, index).getText();
        card.BUSINESS_FARE_PRICE = tripsHomePage.cardBusinessFareCard(destination, index).getText();

        //card.toString1();


        card.FLIGHT_NO.then(function (text) {
            console.log("FLIGHT NO -> " + text);
        })

        card.ORIGIN_STATION.then(function (text) {
            console.log("ORIGIN -> " + text);
        })

        card.DEPARTURE_TIME.then(function (text) {
            console.log("TIME -> " + text);
        })

        card.ARRIVAL_TIME.then(function (text) {
            console.log("ARRIVAL TIME -> " + text);
        })
    }

    this.clickBtnModifyTrip = function () {
        tripsHomePage.btnModifyTrip().click();
    }

    this.assertOnModifyTripButton = function (boolean) {
        expect(tripsHomePage.btnModifyTrip().isPresent()).toBe(boolean);
    }

    this.assertOnModifyTripAirports = function (origin, destination) {
        var flyFromAirport = tripsHomePage.flyFrom().getText();
        var flyToAirport = tripsHomePage.flyTo().getText();
        expect(origin).toEqual(flyFromAirport);
        expect(destination).toEqual(flyToAirport);
        reporter.addMessageToSpec("Modified Origin: " + origin + " Modified Destination: " + destination);
    }

    this.modifyFromTwoWayToOneWay = function () {
        actions.fOHActions.clickOneWayBtn();
        expect(fohHomePage.lblFlyBack().isPresent()).toBeFalsy();
    }

    this.assertOnModifyFromTwoWayToOneWay = function () {
        expect(tripsHomePage.returnFlightDiv().isDisplayed()).toBeFalsy();
    }

    this.clickBtnHoldFare = function () {
        tripsHomePage.btnHoldFare().click();
    };

    this.assertOnBtnHoldFareisNotPresent = function () {
        expect(tripsHomePage.btnHoldFare().isDisplayed()).toBeFalsy();
    };

    this.assertOnBtnHoldFareisPresent = function () {
        expect(tripsHomePage.btnHoldFare().isDisplayed()).toBeTruthy();
    };

    this.assertOnHoldFareDrowerTotal = function (tripWay) {

        tripsHomePage.holdFareDrowerTotal().getText().then(function( text){
            reporter.addMessageToSpec("Hold Fare Fee: " + text);
        });
        //TODO removing this expect as it causing test to fail for now
        //if (tripWay === "oneway") {
        //    expect(tripsHomePage.holdFareDrowerTotal().getText()).toContain("2.50");
        //}
        //else {
        //    expect(tripsHomePage.holdFareDrowerTotal().getText()).toContain("5");
        //}

    };

    this.clickBtnSaveTrip = function () {
        tripsHomePage.btnSaveTrip().click();
    };

    this.assertOnColoredSavedTripIcon = function (boolean) {
        expect(tripsHomePage.coloredSavedTripIcon().isPresent()).toBe(boolean);
    };

    this.assertOnBtnSaveTrip = function (boolean) {
        expect(tripsHomePage.btnSaveTrip().isEnabled()).toBe(boolean);
    };

    this.assertOnFlightInfoBox = function (boolean, tripWay) {
        expect(tripsHomePage.flightInfoBox().get(0).isPresent()).toBe(boolean);

        if (tripWay = "twoway") {
            expect(tripsHomePage.flightInfoBox().get(1).isPresent()).toBe(boolean)
        }
    };

    this.assertSixHoursDepartureRule = function () {
       // browser.wait(EC.presenceOf(tripsHomePage.getBusinessPlus()), 5000); // Waits for the element getBusinessPlus to be present on the dom
        browser.sleep(5000);
        tripsHomePage.availableCardDepartureTime().getText().then(function (flightTime) {
            console.log(flightTime);
            var flightTimeVal = parseFloat(flightTime.substring(0, 2));
            console.log(flightTimeVal);

            var today = new Date();
            console.log("Today-->" + today);
            var currentHour = today.getHours();
            console.log("currentHour-->" + currentHour);
            var sixHourDeparture = currentHour + 6;

            if (sixHourDeparture === flightTimeVal) {
                expect(sixHourDeparture).toEqual(flightTimeVal);
            }
            else if (sixHourDeparture > flightTimeVal){
                expect(sixHourDeparture).toBeGreaterThan(flightTimeVal);
            }
            else {
                console.log("No Flight left Today");
                reporter.addMessageToSpec("No Flight left Today");
            }

        });
    };
};

module.exports = HomeActions;
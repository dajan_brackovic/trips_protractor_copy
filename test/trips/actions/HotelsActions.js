var Pages = require('../../Pages');
var pages = new Pages();
var EC = protractor.ExpectedConditions;

var HotelsActions = function () {
    var hotelsPage = pages.hotelsPage;
    var params = browser.params.conf;

    this.addHotelToBooking = function (index) {
        browser.switchTo().frame('booking_inpath');
        browser.ignoreSynchronization = true;

        hotelsPage.numberHotelInList(index).click();
        var hotelName = hotelsPage.hotelName().getText();
        hotelsPage.hotelName().getText().then(function (text) {
            reporter.addMessageToSpec("Hotel Name: " + text);
        });
        hotelsPage.numberPlusButtonInList(0).click();
        browser.sleep(5000);

        browser.switchTo().defaultContent();
        browser.waitForAngular();
        browser.ignoreSynchronization = false;
        return hotelName;
    };

    this.modifyAndAddHotelToBooking = function (index) {
        browser.switchTo().frame('booking_inpath');
        browser.ignoreSynchronization = true;

        hotelsPage.roomNightsInfo().getText().then(function (text) {
            reporter.addMessageToSpec("Original Number of Nights: " + text);
        });

        hotelsPage.openCalendarCheckIn().click();
        hotelsPage.selectDay(2).click();
        hotelsPage.searchButton().click();
        browser.sleep(3000);

        expect(hotelsPage.roomNightsInfo().getText()).toContain("8");
        hotelsPage.roomNightsInfo().getText().then(function (text) {
            reporter.addMessageToSpec("Modified Number of Nights: " + text);
        });

        hotelsPage.numberHotelInList(index).click();
        var hotelName = hotelsPage.hotelName().getText();
        hotelsPage.hotelName().getText().then(function (text) {
            reporter.addMessageToSpec("Hotel Name: " + text);
        });
        hotelsPage.numberPlusButtonInList(0).click();
        browser.sleep(5000);

        browser.switchTo().defaultContent();
        browser.waitForAngular();
        browser.ignoreSynchronization = false;
        return hotelName;
    };


    this.confirmHotelOnBooking = function () {
        var hotelPrice = hotelsPage.priceAmount().getText();
        hotelsPage.priceAmount().getText().then(function (text) {
            reporter.addMessageToSpec("Hotel Amount: " + text);
        });
        hotelsPage.confirmButton().click();
        return hotelPrice;
    };

    this.clickBtnCancelHotel = function () {
        hotelsPage.btnCancelHotel().click();
    };

    //Hotel actions on active trip

    this.clickAddHotelActive = function () {
        browser.switchTo().frame(0);
        browser.ignoreSynchronization = true;
        hotelsPage.addHotelActive().click();
        browser.getAllWindowHandles().then(function (handles) {
            var secondWindowHandle = handles[1];
            var firstWindowHandle = handles[0];
            //the focus moves on the newest opened tab
            browser.switchTo().window(secondWindowHandle).then(function () {
                //check if the right page is opened
                expect(browser.driver.getCurrentUrl()).toContain("booking.com/");
                browser.ignoreSynchronization = true;

                expect(hotelsPage.ryanairLogo().isPresent()).toBeTruthy();
                expect(hotelsPage.partnershipLogo().isPresent()).toBeTruthy();

                browser.switchTo().window(firstWindowHandle).then(function () {
                    browser.driver.close();
                });
                browser.switchTo().window(secondWindowHandle)
            });
        });

    };

};

module.exports = HotelsActions;
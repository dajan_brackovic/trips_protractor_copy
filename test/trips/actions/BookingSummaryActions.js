var Pages = require('../../Pages')
var pages = new Pages();

var BookingSummaryActions = function () {
    var tripsSummaryPage = pages.tripsSummaryPage;
    var potentialTripPage = pages.potentialTripPage;
    var message = "reference";
    var EC = protractor.ExpectedConditions;

    this.verifyConfirmationMessage = function () {
        browser.wait(EC.presenceOf(tripsSummaryPage.labelBookingRef()), 30000);
        expect(tripsSummaryPage.labelBookingRef().isDisplayed()).toBeTruthy();
        tripsSummaryPage.labelBookingRef().getText().then(function (text) {
            reporter.addMessageToSpec("Booking Ref: --> " + text);
            console.log("Booking Ref: --> " + text);
            expect(text).toMatch(/([A-Z])\w{5}/);
        });
    };


    this.verifyConfirmationMessageOnHoldFare = function () {
        expect(tripsSummaryPage.labelHoldFareRef().isDisplayed()).toBeTruthy();
        tripsSummaryPage.labelHoldFareRef().getText().then(function (text) {
            reporter.addMessageToSpec("Hold Fare Ref: --> " + text);
            console.log("Hold Fare Ref: --> " + text);
        });
    };

    this.verifyConfirmationMessageOnCarHire = function () {
        expect(tripsSummaryPage.labelBookingNoCarHire().isDisplayed()).toBeTruthy();
        tripsSummaryPage.labelBookingNoCarHire().getText().then(function (text) {
            reporter.addMessageToSpec("Car Hire Ref: --> " + text);
            console.log("Car Hire Ref: --> " + text);
        });
    };

    this.verifyConfirmationMessageOnCarHireModify = function () {
        expect(tripsSummaryPage.labelBookingNoCarHireModify().isDisplayed()).toBeTruthy();
        tripsSummaryPage.labelBookingNoCarHireModify().getText().then(function (text) {
            reporter.addMessageToSpec("Car Hire Ref: --> " + text);
            console.log("Car Hire Ref: --> " + text);
        });
    };

    this.returnPnr = function () {
        return tripsSummaryPage.labelBookingRef().getText();
    };


    this.returnHoldFare = function () {
        return tripsSummaryPage.labelHoldFareRef().getText();
    };

    this.assertOnHoldFarePnr = function (bookingRefHoldFare, bookingRefActiveTrip) {
        expect(bookingRefHoldFare).toContain(bookingRefActiveTrip);
    };

    this.clickViewYourTripItinerary = function () {
        tripsSummaryPage.viewYourTripItinerary().click();
    };

    this.clickCheckInButton = function () {
        tripsSummaryPage.checkInButton().click();
    };

    this.assertOnCheckInButton = function (boolean) {
        expect(tripsSummaryPage.checkInButton().isPresent()).toBe(boolean);
    };

    this.assertOnCheckInUnavailable = function (boolean) {
        expect(tripsSummaryPage.checkInUnavailable().isPresent()).toBe(boolean);
    };

    this.clickManageYourTripButton = function () {
        tripsSummaryPage.manageYourTripButton().click();
    };

    this.clickCheckOutButton = function () {
        tripsSummaryPage.checkOutButton().click();
    };

    this.clickRyanairLogo = function () {
        tripsSummaryPage.ryanairLogo().click();
    };

    this.clickTopContinueBtn = function () {
        tripsSummaryPage.topContinueBtn().click();
    };

    this.assertOnAddSeatButtonNotPresent = function () {
        expect(pages.tripsExtrasPage.btnSeat().isPresent()).toBe(false);
    };

    this.assertOnAddedBags = function () {
        expect(tripsSummaryPage.addedBagsOnActiveTrip().isPresent()).toBe(true);
    };

    this.assertOnAddBagButtonNotPresent = function () {
        expect(pages.tripsExtrasPage.btnBag().isPresent()).toBe(false);
    };

    this.assertOnCheckInButtonNotPresent = function () {
        expect(tripsSummaryPage.checkInButton().isPresent()).toBe(false);
    };

    this.clickAddSportEquipment = function () {
        browser.wait(EC.presenceOf(potentialTripPage.openSportEquipmentCard()), 5000);
        potentialTripPage.openSportEquipmentCard().click();
    };

    this.clickAddParking = function () {
        potentialTripPage.openParkingCard().click();
    };

    this.verifyCheckinClosed = function () {
        expect(tripsSummaryPage.checkInClosedHeadline().isPresent()).toBe(true);
        expect(tripsSummaryPage.checkInClosedSubline().isPresent()).toBe(true);
    };

    this.assertOnCarHireNotPresentOnActiveTrip = function () {
        expect(pages.tripsExtrasPage.listOfCars().count()).toBe(0);
    };

    this.assertOnRetrieveBooking = function (bookingRefActiveTrip) {
        expect(bookingRefActiveTrip).toEqual(tripsSummaryPage.bookingRefInfoBox().getText());
    };

    this.assertOnActiveTripMainImage = function () {
        expect(tripsSummaryPage.activeTripMainImage().isPresent()).toBe(true);
    };

    // Special action for C632431 in Active Trip suite
    this.assertOnInfoMessageNumberOfBagsAddedOnBagsCard = function (index) {
        expect(tripsSummaryPage.infoMessageNumberOfBagsAddedOnBagsCard().getText()).toContain(index);
    };

};

module.exports = BookingSummaryActions;
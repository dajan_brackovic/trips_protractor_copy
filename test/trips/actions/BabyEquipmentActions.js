var Pages = require('../../Pages')
var pages = new Pages();
var params = browser.params.conf;

var BabyEquipmentActions = function () {
      
    var babyEquipmentPage = pages.babyEquipmentPage;

    this.selectPlusButtonSameForBothFlights = function (i) {
        babyEquipmentPage.plusButtonSameForBothFlights(i).click();
    };
    this.selectPlusButtonFlightBack = function (i) {
        babyEquipmentPage.plusButtonFlightBack(i).click();
    };
    this.selectPlusButtonMorePassengers = function(i){
        babyEquipmentPage.plusButtonMorePassengers(i).click();
    };
    this.selectPlusButtonMorePassengersOneWay = function(indexOfPasenger){
        babyEquipmentPage.plusButtonMorePassengersOneWay(indexOfPasenger).click();
    };
    this.selectPlusButtonFlightOut = function (i) {
        babyEquipmentPage.plusButtonFlightOut(i).click();
    };
    this.selectMinusButtonSameForBothFlights = function (i) {
        babyEquipmentPage.minusButtonSameForBothFlights(i).click();
    };
    this.selectMinusButtonFlightBack = function (i) {
        babyEquipmentPage.minusButtonFlightBack(i).click();
    };
    this.selectMinusButtonFlightOut = function (i) {
        babyEquipmentPage.minusButtonFlightOut(i).click();
    };
    this.plusButtonValidation = function (i) {
        expect((babyEquipmentPage.plusButtonSameForBothFlights(i)).isPresent()).toBeTruthy();
        expect((babyEquipmentPage.plusButtonSameForBothFlights(i)).isDisplayed()).toBeTruthy();
    };
    this.minusButtonValidation = function (i) {
        expect((babyEquipmentPage.minusButtonSameForBothFlights(i)).isPresent()).toBeTruthy();
        expect((babyEquipmentPage.minusButtonSameForBothFlights(i)).isDisplayed()).toBeTruthy();
    };
    this.plusButtonEnabled = function (i) {
        expect((babyEquipmentPage.plusButtonMorePassengers(i)).isPresent()).toBeTruthy();
    };
    this.minusButtonEnabled = function (i) {
        expect((babyEquipmentPage.minusButtonDisabled(i)).isEnabled()).toBeTruthy();
    };
    this.plusButtonDisabledSFBF = function (i) {
        expect((babyEquipmentPage.plusButtonDisabled(i)).isEnabled()).toBeTruthy();
    };
    this.minusButtonDisabledSFBF = function (i) {
        expect((babyEquipmentPage.minusButtonDisabled(i)).isEnabled()).toBeTruthy();
    };
    this.selectCheckBoxSFBF = function (i) {
        babyEquipmentPage.checkBoxSFBF(i).click();
    };
    this.expectCheckBoxSFBFisChecked = function (i) {
        expect((babyEquipmentPage.checkBoxSFBFSelected(i)).isPresent()).toBeTruthy();
    };
    this.expectCheckBoxSFBFisUnChecked = function (i) {
        expect((babyEquipmentPage.checkBoxSFBFUnSelected(i)).isPresent()).toBeTruthy();
    };
    this.expectBabyEquipmentAmountValidation = function () {
        expect((babyEquipmentPage.amountBabyEquipment()).isPresent()).toBeTruthy();
        expect((babyEquipmentPage.amountBabyEquipment()).isDisplayed()).toBeTruthy();
    };
    this.expectInboundFlightBabyOption = function () {
        expect((babyEquipmentPage.babyLogo).count()).toEqual(2);
        expect((babyEquipmentPage.plusFirstButton).count()).toEqual(2);
    };
    this.expectBabyEquipmentAmount = function (k,i) {
        expect((babyEquipmentPage.amountBabyEquipment(k).getText())).toEqual(i);
    };
    this.expectBabyEquipmentAmountNN = function (k, i) {
        expect((babyEquipmentPage.amountBabyEquipmentNN(k).getText())).toEqual(i);
    };
    this.expectBabyEquipmentAmountSet = function (k, i) {
        expect((babyEquipmentPage.amountBabyEquipmentSet(k).getText())).toEqual(i);
    };
    this.expectBabyEquipmentAmountLeft = function (i,p) {
        expect((babyEquipmentPage.amountBabyEquipmentLeft(i).getText())).toEqual(p);
    };
    this.expectBabyEquipmentAmountRight = function (i,p) {
        expect((babyEquipmentPage.amountBabyEquipmentRight(i).getText())).toEqual(p);
    };
    this.expectEquipmentInformationLabel = function (value) {
        expect((babyEquipmentPage.labelAddBabyEquipment()).getText()).toEqual(value);
    };
    this.expectBabyEquipmentTitle = function (value) {
        expect((babyEquipmentPage.titleBabyEquiment()).getText()).toEqual(value);
    };
    this.expectBabyEquipmentLableIcon = function () {
        expect((babyEquipmentPage.labelAddBabyEquipmentIcon()).isPresent()).toBeTruthy();
    };
    this.expectTotalPrice = function(){
        expect(babyEquipmentPage.cornerPrice().getText().then(function (text) {
            return text.substring(9);
        })).toMatch(params.regEx.price);
    };
    this.expectTotalPriceToBe = function(p){
        expect(babyEquipmentPage.cornerPrice().getText()).toEqual(p);
    };
    this.expectMinusButtonIsDisabled = function(index){
        expect(babyEquipmentPage.minusButtonDisabled(index).isPresent()).toBeTruthy();
    };
    this.expectPassengerNameIsDisplayed = function(i){
        expect(babyEquipmentPage.passengerName(i).isDisplayed()).toBeTruthy();
        expect(babyEquipmentPage.passengerName(i).isPresent()).toBeTruthy();
    };
    this.expectPassengerLogoIsDisplayed = function(i){
        expect(babyEquipmentPage.passengerLogo(i).isDisplayed()).toBeTruthy();
    };
    this.expectBabyLogoIsDisplayed = function(i){
        expect(babyEquipmentPage.babyLogo(i).isPresent()).toBeTruthy();
    };
    this.expectCheckBoxIsDisplayed = function(i){
        expect(babyEquipmentPage.checkBoxSFBF(i).isDisplayed()).toBeTruthy();
    };
    this.expectQuantityCardisDisplayed = function(i){
        expect(babyEquipmentPage.quantityCard(i).isDisplayed()).toBeTruthy();
    };
    this.expectLabelSFBFIsDisplayed = function(i){
        expect(babyEquipmentPage.labelSameForBothFlights(i).isDisplayed()).toBeTruthy();
    };
    this.expectInboundText = function(i){
        expect(babyEquipmentPage.inboundText().getText()).toEqual(i);
    };
    this.expectOutboundText = function(i){
        expect(babyEquipmentPage.outboundText().getText()).toEqual(i);
    };
    this.expectInformationCardIsDisplayed = function(){
        expect(babyEquipmentPage.informationCardBE().isDisplayed()).toBeTruthy();
    };
    this.expectTotalPriceIsDisplayed = function(){
        expect(babyEquipmentPage.cornerPrice().isDisplayed()).toBeTruthy();
    };
    this.expectXButtonIsDisplayed = function(){
        expect(babyEquipmentPage.closeButtonX().isDisplayed()).toBeTruthy();
    };
    this.expectCancelButtonIsDisplayed = function(){
        expect(babyEquipmentPage.cancelButton(1).isDisplayed()).toBeTruthy();
    };
    this.clickCloseButtonX = function(){
        babyEquipmentPage.closeButtonX().click();
    };
    this.clickCancel = function(){
        babyEquipmentPage.cancelButton(1).click();
    };
    this.clickESC = function(){
        browser.actions().sendKeys(protractor.Key.ESCAPE).perform();
    };
    this.clickTAB = function(){
        browser.actions().sendKeys(protractor.Key.TAB);
    };
    this.expectBackButtonIsDisplayed = function(){
        expect(babyEquipmentPage.backButton().isDisplayed()).toBeTruthy();
    };
    this.expectPassengerCardIsDisplayed = function(i){
        expect(babyEquipmentPage.passengerCard(i).isDisplayed()).toBeTruthy();
    };
    this.expectInboundTextIsDisplayed = function(){
        expect(babyEquipmentPage.inboundText().isDisplayed()).toBeTruthy();
    };
    this.expectOutboundTextIsDisplayed = function(){
        expect(babyEquipmentPage.outboundText().isDisplayed()).toBeTruthy();
    };
    this.expectCentralPriceDisplayed = function(i){
        expect(babyEquipmentPage.centralPrice().isDisplayed()).toBeTruthy();
    };
    this.expectCentralPriceLeftDisplayed = function(i){
        expect(babyEquipmentPage.centralPriceLeft(i).isPresent()).toBeTruthy();
    };
    this.expectCentralPriceRightDisplayed = function(i){
        expect(babyEquipmentPage.centralPriceRight(i).isPresent()).toBeTruthy();
    };
    this.expectCentralPrice = function(i,p){
        expect(babyEquipmentPage.centralPrice(i).getText()).toContain(p);
    };
    this.expectCentralPriceNN = function(p){
        expect(babyEquipmentPage.centralPriceNN().getText()).toContain(p);
    };
    this.expectCentralPriceNNFirst = function(p){
        expect(babyEquipmentPage.centralPriceFirst().getText()).toContain(p);
    };
    this.expectCentralPriceLeft = function(i,p){
        expect(babyEquipmentPage.centralPriceLeft(i).getText()).toContain(p);
    };
    this.expectCentralPriceRight = function(i,p){
        expect(babyEquipmentPage.centralPriceRight(i).getText()).toContain(p);
    };
     this.expectBabyEquipmentAmountLeftValidation = function (i) {
        expect((babyEquipmentPage.amountBabyEquipmentLeft(i)).isPresent()).toBeTruthy();
    };
    this.expectBabyEquipmentAmountRightValidation = function (i) {
        expect((babyEquipmentPage.amountBabyEquipmentRight(i)).isPresent()).toBeTruthy();
    };
    this.expectBabyEquipmentAmountValidation = function () {
        expect((babyEquipmentPage.amountBabyEquipment(i)).isPresent()).toBeTruthy();
    };
    this.expectPlusDisableToolTipShown = function () {
        expect((babyEquipmentPage.toolTipDisablePlusShown()).isPresent()).toBeTruthy();
    };
    this.expectPlusDisableToolTipHidden = function () {
        expect((babyEquipmentPage.toolTipDisablePlusHide()).isPresent()).toBeTruthy();
    };
    this.expectPlusDisableToolTipText = function (i,p) {
        expect((babyEquipmentPage.toolTipDisablePlusText(i)).getText()).toEqual(p);
    };
    this.expectPlusDisableToolTipTextAmount = function (p) {
        expect((babyEquipmentPage.toolTipDisablePlusTextAmount()).getText()).toEqual(p);
    };
     this.expectConfirmButtonIsDisabled = function () {
        expect((babyEquipmentPage.confirmButton()).isEnabled()).not.toBeTruthy();
    };
    this.expectConfirmButtonIsEnabled = function () {
       expect((babyEquipmentPage.confirmButton()).isEnabled()).toBeTruthy();
    };
    this.clickConfirmButton = function () {
        babyEquipmentPage.confirmButtonClick().click();
    };
    this.expectPassengerTypeAndNumber = function (i, k) {
        expect((babyEquipmentPage.passengerTypeAndNumber(i)).getText()).toEqual(k);
    };
    this.confirmButtonValidation = function () {
        expect((babyEquipmentPage.confirmButtonClick()).isPresent()).toBeTruthy();
    };
    this.expectBabyEquipmentAmount0 = function (k,i) {
        expect((babyEquipmentPage.amountBabyEquipmentNN(k).getText())).toEqual(i);
    };
    this.expectBabyEquipmentAmountZero = function (k,i) {
        expect((babyEquipmentPage.amountBabyEquipmentZero(k).getText())).toEqual(i);
    };
    this.expectBabyEquipmentAmount1 = function (k,i) {
        expect((babyEquipmentPage.amountBabyEquipment(k).getText())).toEqual(i);
    };
    this.selectPlusButtonSameForBothFlightsDisable = function (i) {
        babyEquipmentPage.plusButtonDisabled(i).click();
    };
    this.expectInfantIconIsDisplayed = function (indexOfPassengerCard) {
        babyEquipmentPage.infantIcon(indexOfPassengerCard).click();
    };
    this.expectMaximumMessageIsDisplayed = function (indexOfPassenger) {
        expect(babyEquipmentPage.maximumReachedEquipment(indexOfPassenger).isDisplayed()).toBeTruthy();
    };
    this.expectMaximumMessageReached = function(indexOfPassenger,textDisplayed){
        expect(babyEquipmentPage.maximumMessage(indexOfPassenger).getText()).toContain(textDisplayed);
    };
    this.expectBabyActiveEquipmentPurchased = function(inbound, outbound) {
        expect((babyEquipmentPage.babyActiveInboundEquipment().getText())).toContain(inbound);
        expect((babyEquipmentPage.babyActiveOutboundEquipment().getText())).toContain(outbound);
    };
};
module.exports = BabyEquipmentActions;
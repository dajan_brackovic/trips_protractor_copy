var TripsExtrasPage = function () {


    this.btnExtrasContinue = function () {
        return element(by.css("button.core-btn-primary.core-btn-block.core-btn-medium"));
    };

    this.btnSeat = function () {
        return element(by.css("#seats_card_btn1"));
    };

    this.btnBag = function () {
        return element(by.id("bags_card_btn1"));
    };

    this.btnBaby = function () {
        return element(by.id("baby_card_btn1"));
    };

    this.listOfCars = function () {
        return element.all(by.css("div.ct-vehicle-price button.core-btn-ghost"));
    };

    this.btnHotels = function () {
        return element(by.css("a.accommodation-banner-content"));
    };

    this.btnRemoveHotels = function () {
        return element(by.css("[ng-click='line.onRemove()']"));
    };

    this.modifyCarHire = function () {
        return element(by.css("div.ct-vehicle-price > div:nth-child(2) > button"));
    };
    //general flight info
    this.departureAirport = function () {
        return element(by.css(".starting-point"));
    };
    this.destinationAirport = function () {
        return element(by.css(".destination"));
    };
    this.priceAmount = function () {
        return element(by.css(".price-number"));
    };
    this.checkOutButton = function () {
        return element(by.css(".core-btn-primary.core-btn-block.core-btn-medium"));
    };

    // Reserve Seat Early Pop Up

    this.reserveSeatEarlyPopUp = function () {
        return element(by.css("[translate='trips.potential.reserved_seats.popup.title']"));
    };

    this.ReserveSeatEarlyPopUpCloseBtn = function () {
        return element(by.css("[icon-id='icons.glyphs.close']"));
    };
    //
    this.bntAddPriorityBoarding = function () {
        return element(by.css("[id^='priority_card']"));
    };

    this.textPriorityBoardingAdded = function () {
        return element(by.css("[translate='trips.potential.pb.added']"));
    };

    this.tickIconPriorityBoardingAdded = function () {
        return element(by.css("core-icon+div[translate='trips.potential.pb.added']"));
    };

    this.bntRemovePriorityBoarding = function () {
        return element(by.css("[translate='trips.potential.pb.button.remove']"));
    };

    this.textPriorityBoardingCardBenefits= function () {
        return element.all(by.css(".checkbox-list>li>span[translate^='trips.potential.pb.ul']"));
    };
};

module.exports = TripsExtrasPage;
var BagsPage = function () {

    this.btnIncrementNormalBag = function () {
        return element.all(by.css("button.core-btn.inc.core-btn-wrap")).get(0);
    };

    this.btnIncrement = function (index) {
        return element.all(by.css("button.core-btn.inc.core-btn-wrap")).get(index);
    };

    this.btnIncrementList = function (index) {
        return element.all(by.css("button.core-btn.inc.core-btn-wrap"));
    };

    this.bagsSubTotalList = function (index) {
        return element(by.css("bags-per-person:nth-child("+index+") > div > div.bags-info > span"));
    };

    this.discountMessageForChildBag = function () {
        return element(by.css("[translate='trips.extra.bags.discount_message']"));
    };

    this.btnIncrementNormalBagForInboundSpanishDomestic = function () {
        return element.all(by.css("button.core-btn.inc.core-btn-wrap")).get(2);
    };

    this.btnIncrementLargeBag = function () {
        return element.all(by.css("button.core-btn.inc.core-btn-wrap")).get(1);
    };

    this.btnDecrementNormalBag = function () {
        return element.all(by.css("button.core-btn.dec.core-btn-wrap")).get(0);
    };

    this.btnDecrementLargeBag = function () {
        return element.all(by.css("button.core-btn.dec.core-btn-wrap")).get(1);
    };

    this.bagsBtnConfirm = function () {
        return element.all(by.css("button[translate='trips.side.confirm']")).get(1);
    };

    this.bagsBtnCancel = function () {
        return element(by.css("div.action-section button[translate='trips.side.cancel']"));
    };

    this.bagsSubTotal = function () {
        return element(by.css("span.total-bags-price"));
    };

    this.bagsTotal = function () {
        return element(by.css("span.amount"));
    };

    this.addedBags = function () {
        return element(by.css("p[translate='trips.potential.you_have_added_bags']"));
    };

    this.numberOfNormalBagsSelected = function () {
        return element.all(by.css('div.value-display.ng-binding.ng-scope.set')).get(0);
    };

    this.numberOfLargeBagsSelected = function () {
        return element.all(by.css('div.value-display.ng-binding.ng-scope.set')).get(1);
    };
};

module.exports = BagsPage;
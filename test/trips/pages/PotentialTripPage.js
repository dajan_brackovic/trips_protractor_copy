var PotentialTripPage = function(){

    this.informationLabelOnCardInsurance = function () {
        return element(by.css('[translate="trips.potential.you_have_added_insurance"] strong'));
    };
    this.openParkingCard = function () {
        return element(by.css("#parking_card_btn1"));
    };
    this.openBabyEquipmentCard = function () {
        return element(by.id("baby_card_btn1"));
    };
    this.openSportEquipmentCard = function () {
        return element(by.id("sports_card_btn1"));
    };
    this.openMusicEquipmentCard = function () {
        return element(by.id("musc_card_btn1"));
    };
    this.openInsuranceCard = function () {
        return element(by.id("insurance_card_btn1"));
    };
    this.insuranceIcon = function(){
        return element(by.css('[class="icon-extra"]'));
    };
    this.potentionalTravelTitle = function(){
        return element(by.css('[translate="trips.potential.travel.title"]'));
    };
    this.arrowPriceBreakDown = function () {
        return element(by.css('[class="basket-arrow"]'));
    };
    this.priceBreakDownCartItemName = function (indexOfRow) {
        return element.all(by.css('[class="list-item clearfix ng-scope"]')).get(indexOfRow).element(by.css('[class="ng-scope"]'));
    };
    this.priceBreakDownCartItemNameInsurance = function (indexOfRow) {
        return element.all(by.css('[class="list-item clearfix ng-scope"]')).get(indexOfRow).element(by.css('[class="item-label ng-scope"]'));
    };
    this.priceBreakDownCartItemPrice = function (indexOfRow) {
        return element.all(by.css('.item-price.has-remove-icon')).get(indexOfRow);
    };

    this.priceBreakDownCartItem = function () {
        return element.all(by.css('[class="list-item clearfix ng-scope"]'));
    };
    this.priceTotalCost = function () {
        return element(by.css("div.trips-basket.trips-total span span div div.price-number.ng-binding"));
    };
    this.priceBreakDownClosed = function () {
        return element(by.css('[class="cart"]'));
    };
    this.priceBreakDownOpened = function () {
        return element(by.css('[class="cart cart-opened"]'));
    };
    this.priceBreakDownCartItemPriceAirportTransfers = function (indexOfRepeater) {
        return element.all(by.repeater("item in model")).get(indexOfRepeater).element(by.css('strong'));
    };
    this.priceBreakDownCartItemNameAirportTransfers = function (indexOfRow) {
        return element.all(by.css('[class="list-item clearfix ng-scope"]')).get(indexOfRow).element(by.css('[class="item-label ng-scope"]'));
    };
    this.informationMessageAddedPriceAnythingElse = function () {
        return element(by.css("#booking-selection > div > div > h2"));
    };
    this.babyImage = function () {
        return element(by.css('[xlink:href="#illustrations.product-baby"]'));
    };
    this.musicImage = function () {
        return element(by.css('[xlink:href="#illustrations.product-music"]'));
    };
    this.openAirportTransfersCard = function(){
        return element(by.css("[translate='trips.potential.transfers.title']"));
    };
    this.plusButtonSameForBothFlights = function (i) {
        return element.all(by.css('[ng-click="increment()"]')).get(i);
    };
    this.confirmButtonClick = function () {
        return element(by.css("button.ryn-btn-primary-yellow.baby-equipment-button"));
    };
    this.checkout = function () {
        return element(by.css('[translate="trips.summary.buttons.btn_checkout"]'));
    };
    this.informationLabelOnCard = function () {
       // return element(by.css("div.content-column-extra > div.content-column-extra-active > p > strong"));
        return element(by.css("div[translate='trips.potential.you_have_added_sports_plural'] strong"));
    };

    this.informationLabelOnCardParking = function () {
       // return element(by.css("div.content-column-parking > div.checked-item-two-thirds > div"));
        return element(by.css("div[translate='trips.potential.you_have_added_parking']"));
    };
    this.informationLabelOnCardBaby = function () {
      //  return element(by.css("li.product-area-one-third-full-height > div > div > div.content-column-extra > div.content-column-extra-active > p > strong"));
        return element(by.css("div[translate='trips.potential.you_have_added_baby_plural'] strong"));
    };
    this.popUpBasketCart = function () {
        return browser.driver.findElement(by.css('[class="show-basket-pop ng-scope"]'));
    };
    this.informationLabelOnCardMusic = function () {
       // return element(by.css('[translate="trips.potential.you_have_added_musc_plural"]'));
        return element(by.css("div[translate='trips.potential.you_have_added_musc_plural'] strong"));
    };
    this.totalPriceBreakDownOpened = function () {
        return element(by.css('[class="price-units"]'));
    };
    this.openSamsoniteBags = function () {
       // return element(by.css('[translate="trips.partner.offer.samsonite.button.redirect"]'));
        return element(by.css("#_card_btn1")); // This id is more likely to change
    };
    this.openTransfersCard = function () {
        return element(by.id("transfers_card_btn2"));
    };
};
module.exports = PotentialTripPage;
var TransfersActiveTripsPage = function(){

    this.parkingCard = function (){
        return element(by.css('[translate="trips.potential.parking.parking.title"]'));
    };
    this.airportTransfersTitle = function(){
        return element(by.css('[translate="trips.potential.transfers.title"]'));
    };
    this.addedTransferFroAllPassengersMessage = function(){
        return element(by.css('[translate="trips.active.travel.max.summary.transfers"]'));
    };
    this.amountOfTransfersMessage = function(){
        return element(by.css('[translate="trips.active.travel.max.msg.transfers"]'));
    };
    this.amountOfTransfersMessage1 = function(){
        return element(by.css('[translate="trips.active.travel.purchased.transfers"]'));
    };
	
};
module.exports = TransfersActiveTripsPage;
var SportEquipmentPage = function () {

    this.sportEquipmentTypeSinglePrice = function(index){
        return element.all(by.repeater("equipment in ::vm.equipmentList")).get(index).element(by.css("span.price"));
    };
    this.openedDropDown = function(){
        return element(by.css('[ng-show="vm.dropDownShown"]')).element(by.css('[class="ng-hide"]'));
    };
    this.passengerNameAll = function(i){
        return element.all(by.css('[class="persons-name"]')).get(i);
    };
    this.toolTipFull = function(){
        return element(by.css('[class="tooltip-callout-content top"]'));
    };
    this.sportEquipmentTypeSelection = function(i){
        return element.all(by.css('[placeholder="Select equipment type"]')).get(i);
    };
    this.sportEquipmentTypeTitle = function(i){
        return element.all(by.css('[placeholder="Select equipment type"]')).get(i);
    };
    this.dropdownButton = function(i){
        return element.all(by.css("div.core-select input.core-input")).get(i);
    };
    this.sportEqipmetInformationText = function () {
        return element(by.css('[class="more-info-equipment-opened"]'));
    };
    this.informationLinkViewTypes = function(){
        return element(by.css('[translate="trips.sports-equipment.view-types"]'));
    };
    this.bike = function(){
        return element(by.css('[translate="trips.bike_equipment.title"]'));
    };
    this.largeSportsItem = function(){
        return element(by.css('[translate="trips.large_sports_item.title"]'));
    };
    this.golfEquipment = function(){
        return element(by.css('[translate="trips.golf_equipment.title"]'));
    };
    this.skis = function(){
        return element(by.css('[translate="trips.skis_equipment.title"]'));
    };
    this.sportsEquipment = function(){
        return element(by.css('[translate="trips.sports_equipment.title"]'));
    };
    this.selectEquipmentType = function(){
        return element(by.css('[label="Select equipment type"]'));
    };
    this.plusButtonSameForBothFlights = function (i) {
        return element.all(by.css('button.core-btn.inc.core-btn-wrap')).get(i);
    };
    this.plusButtonFlightBack = function (i) {
        return element.all(by.css('[ng-click="model.incPrice()"]')).get(i);
    };
    this.minusButtonDisabled = function(i){
        return element.all(by.css("button.core-btn.dec.core-btn-wrap[disabled='disabled']")).get(i);
    };
    this.minusButtonDisabledSport = function(indexOfPassinger,indexOfMinus){
        return element.all(by.css('[class="modal-equipment"]')).get(indexOfPassinger).element(by.css("div.equipment-info > div:nth-child("+indexOfMinus+") > div > div > div.ng-scope.single-holder-normal > div > div > button.btn.dec.disabled"));
    };
    this.passengerCard = function(i){
        return element.all(by.css('[class="modal-equipment"]')).get(i);
    };
    this.plusButtonFlightOut = function (i) {
        return element.all(by.css('[ng-click="model.incPrice()"]')).get(i);
    };
    this.plusButtonMorePassengers = function(i){
        return element.all(by.css('[ng-click="model.incPrice()"]')).get(i);
    };
    this.minusButtonSameForBothFlights = function (i) {
        return element.all(by.css('button.core-btn.dec.core-btn-wrap')).get(i);
    };
    this.minusButtonFlightBack = function (i) {
        return element.all(by.css('[ng-click="vm.flightBackCard.incPrice()"]')).get(i);
    };
    this.minusButtonFlightOut = function (i) {
        return element.all(by.css('[ng-click="vm.flightOutCard.incPrice()"]')).get(i);
    };
    this.amountSportEquipment = function (k) {
        return element.all(by.css('[class="value-display ng-binding ng-scope"]')).get(k);
    };
    this.amountSportEquipmentSport = function (indexOfPassinger,indexOfAmount) {
        return element.all(by.css('[class="modal-equipment"]')).get(indexOfPassinger).element(by.css("div:nth-child("+indexOfAmount+") > div.equipment-info > div > div > div > div:nth-child(4) > div > div > div"));
    };
    this.amountSportEquipmentSeted = function (k) {
        return element.all(by.css('[class="value-display ng-binding ng-scope set"]')).get(k);
    };
    this.informationCardME = function(){
        return element(by.css('[class="equipment-information"]'));
    };
    this.userIconImage1 = function () {
        return element(by.css());
    };
    this.moreInformationME = function () {
        return element(by.css('[translate="trips.extra.sport_equipment.more_info_description"]'));
    };
    this.userIconImage2 = function () {
        return element(by.css());
    };
    this.userIconName1 = function () {
        return element(by.css());
    };
    this.userIconName2 = function () {
        return element(by.css());
    };
    this.titleSportEquiment = function () {
        return element(by.css("div.dialog-header h1 div span"));
    };
    this.closeButton = function () {
        return element(by.css());
    };
    this.closeButtonX = function () {
        return element(by.css('[ng-click="closeThisDialog(\'x\')"]'));
    };
    this.cancelButton = function (i) {
        return element.all(by.css('[translate="trips.side.cancel"]')).get(i);
    };
    this.confirmButton = function () {
        return element(by.buttonText("Confirm"));
    };
    this.confirmButtonClick = function () {
        return element.all(by.css("button[translate='trips.side.confirm']")).get(1);
    };
    this.labelAddSportEquipment = function () {
        return element(by.css("div.alert.modal-alert-info > span"));
    };
    this.labelAddSportEquipmentIcon = function () {
        return element(by.css("core-icon div svg use"));
    };
    this.cornerPrice = function () {
        return element.all(by.css('[class="amount ng-scope"]')).last();
    };
    this.plusInformationButton = function(){
      return element(by.css('[icon-id="icons.glyphs.plus"]'));
    };
    this.minusInformationButton = function(){
        return element(by.css('[icon-id="icons.glyphs.minus"]'));
    };
    this.sportEquipmentInformationCardText = function(){
        return element(by.id('ngdialog1-aria-describedby'));
    };
    this.passengerLogo = function(i){
        return element.all(by.css('[icon-id="icons.glyphs.person"]')).get(i);
    };
    this.quantityCard = function(i){
        return element.all(by.css('[class="equipment-info"]')).get(i);
    };
    this.passengerTypeAndNumber = function(i){
        return element.all(by.css('[class="persons-name"]')).get(i);
    };
    this.centralPriceNN = function(){
        return element.all(by.css('[class="price ng-binding"]')).last();
    };
    this.centralPriceNNN = function(){
        return element.all(by.css('[class="price ng-binding"]')).get(0);
    };
    this.sportLogo = function (i) {
        return element.all(by.css('[class="icon-baby ng-isolate-scope"]')).get(i);
    };
    this.checkBoxSFBF = function (i) {
        return element.all(by.css("div.same-flight")).get(i);
    };
    this.inboundText = function(){
        return element(by.css('[translate="trips.extra.flight_out"]'));
    };
    this.textSFBF = function(){
        return element(by.css('[translate="trips.extra.same_for_both_flights"]'));
    };
    this.outboundText = function(){
        return element(by.css('[translate="trips.extra.flight_back"]'));
    };
     this.toolTipDisablePlusHide = function(){
        return element(by.css('[class="tooltip-callout-content top ng-hide"]'));
    };
    this.toolTipDisablePlusShown = function(){
        return element(by.css("div.tooltip-callout-content.top"));
    };
    this.toolTipDisablePlusText = function(i){
        return element(by.css("a.amount-change-increase-disabled div.tooltip-callout.top.ng-hide")).$("span:nth-child("+i+")");
    };
    this.toolTipDisablePlusTextAmount = function(){
         return element(by.css("a.amount-change-increase-disabled div.tooltip-callout.top.ng-hide")).$("span.bag-amount");
    };
    this.plusButtonDisabled = function(i){
        return element.all(by.css('[class="btn inc disabled"]')).get(i);
    };
    this.plusButtonEnablePage = function(i){
        return element.all(by.css('[class="btn inc"]')).get(i);
    };
    this.checkBoxSFBFSelected = function(i){
        return element.all(by.css('[class="ng-scope single-holder-normal ng-hide"]')).get(i);
    };
    this.checkBoxSFBFUnSelected = function(i){
        return element.all(by.css('[class="ng-scope single-holder-normal"]')).get(i);
    };
    this.checkBoxSFBFSelectedSport = function(indexOfPassinger,index){
        return element.all(by.css('[class="modal-equipment"]')).get(indexOfPassinger).element(by.css("div.equipment-info > div:nth-child("+index+") > div > div > div.ng-scope.single-holder-normal"));
    };
    this.checkBoxSFBFUnSelectedSport = function(indexOfPassinger,index){
        return element.all(by.css('[class="modal-equipment"]')).get(indexOfPassinger).element(by.css("div.equipment-info > div:nth-child("+index+") > div > div > div.ng-scope.single-holder-normal.ng-hide"));
    };
    this.centralPrice = function(i){
        return element.all(by.css("div.ng-scope.single-holder-normal > div > span.price.ng-binding")).get(i);
    };
    this.centralPriceSport = function(indexOfPassinger,indexOfAmount){
        return element.all(by.css('[class="modal-equipment"]')).get(indexOfPassinger).element(by.css("div.equipment-info > div:nth-child("+indexOfAmount+") > div > div > div.ng-scope.single-holder-normal > div > span.price.ng-binding"));
    };
    this.centralPriceRight = function(i){
        return element.all(by.css("div.ng-scope.equipment-holder-double-normal > div > span.price.ng-binding")).get(i);
    };
    this.centralPriceLeft = function(i){
        return element.all(by.css('div.ng-scope.equipment-holder-double-bordered-right > div > span.price.ng-binding')).get(i);
    };
    this.amountSportEquipmentRight = function (i) {
        return element.all(by.css("div.equipment-holder-double-normal")).get(i);
    };
    this.amountSportEquipmentLeft = function (i) {
        return element.all(by.css("div.equipment-holder-double-bordered-right")).get(i);
    };
     this.confirmButtonDisabled = function(){
         return element.all(by.css("button.core-btn-primary[disabled='disabled']")).get(1);
    };
    this.confirmButtonEnabled = function(){
         return element.all(by.css("button.core-btn-primary[ng-disabled='!vm.isCtaActive()']")).get(1);
    };
    this.sportEqupmentTypeClick = function(indexOfPassinger,indexOfAdd){
        return element.all(by.css('[class="modal-equipment"]')).get(indexOfPassinger).all(by.css('[ng-click="vm.toggleDropDown()"]')).get(indexOfAdd);
    };
    this.sportEqupmentTypeClickCount = function(){
        return element.all(by.repeater('equipment in ::vm.equipmentList'));
    };
    this.sportSelectEqupmentType = function (indexOfPassinger,indexOfSelection,indexOfOption){
        return element.all(by.css('[class="modal-equipment"]')).get(indexOfPassinger).all(by.repeater('equipmentCard in vm.passenger.equipmentCards')).get(indexOfSelection).all(by.repeater('equipment in ::vm.equipmentList')).get(indexOfOption);
    };
    this.sportSelectEqupmentTypeGetText = function (indexOfPassinger,indexOfSelection,indexOfOption){
        return element.all(by.css('[class="modal-equipment"]')).get(indexOfPassinger).all(by.repeater('equipmentCard in vm.passenger.equipmentCards')).get(indexOfSelection).all(by.css('[translate="trips.extra.already_selected_equipment"]')).get(indexOfOption);
    };
    this.addAnotherTypeOfEquipment = function(indexOfPassinger){
        return element.all(by.css('[class="modal-equipment"]')).get(indexOfPassinger).element(by.css('[ng-click="vm.addEquipmentCard()"]'));
    };
    this.addAnotherTypeOfEquipmentEnabled = function(indexOfPassinger,indexOfAddEquipment){
        return element.all(by.css('[class="modal-equipment"]')).get(indexOfPassinger).element(by.css('[class="equipment-footer"]')).element(by.css('[class]'));
    };
    this.addAnotherTypeOfEquipmentDisable = function(indexOfPassinger){
        return element.all(by.css('[class="modal-equipment"]')).get(indexOfPassinger).element(by.css('[class="equipment-footer"]')).element(by.css('[class="disabled-field"]'));
    };
    this.plusButtonSameForBothFlightsSport = function (indexOfPassinger,indexOfAddEquipment) {
        return element.all(by.css('[class="modal-equipment"]')).get(indexOfPassinger).all(by.css("div.equipment-info > div:nth-child("+indexOfAddEquipment+") > div > div > div.ng-scope.single-holder-normal > div > div > button.btn.inc"));
    };
    this.minusButtonSameForBothFlightsSport = function (indexOfPassinger,indexOfAddEquipment) {
        return element.all(by.css('[class="modal-equipment"]')).get(indexOfPassinger).all(by.css("div.equipment-info > div:nth-child("+indexOfAddEquipment+") > div > div > div.ng-scope.single-holder-normal > div > div > button.btn.dec"));
    };
    this.infantIcon = function(indexOfPassengerCard){
        return element.all(by.css('[class="has-inf ng-isolate-scope"]')).get(indexOfPassengerCard);
    };
    this.plusButtonSameForBothFlightsSportLeft = function (indexOfPassinger) {
        return element.all(by.css('[class="modal-equipment"]')).get(indexOfPassinger).all(by.css("div.ng-scope.equipment-holder-double-bordered-right > div > div > button.btn.inc"));
    };
    this.minusButtonSameForBothFlightsSportLeft = function (indexOfPassinger) {
        return element.all(by.css('[class="modal-equipment"]')).get(indexOfPassinger).all(by.css("div.ng-scope.equipment-holder-double-bordered-right > div > div > button.btn.dec"));
    };
    this.plusButtonSameForBothFlightsSportRight = function (indexOfPassinger) {
        return element.all(by.css('[class="modal-equipment"]')).get(indexOfPassinger).all(by.css("div.ng-scope.equipment-holder-double-normal > div > div > button.btn.inc"));
    };
    this.minusButtonSameForBothFlightsSportRight = function (indexOfPassinger) {
        return element.all(by.css('[class="modal-equipment"]')).get(indexOfPassinger).all(by.css("div.ng-scope.equipment-holder-double-normal > div > div > button.btn.dec"));
    };
    this.maximumReachedEquipment = function (indexOfPassinger) {
        return element.all(by.css('[class="modal-equipment"]')).get(indexOfPassinger).element(by.css('[class="equipment-info"]'));
    };
    this.maximumMessage = function (indexOfPassinger) {
        return element.all(by.css('[class="modal-equipment"]')).get(indexOfPassinger).element(by.css("div.equipment-info > div.maximum-message > p"));
    };
    this.disableCount = function(){
        return element.all(by.css('[class="disabled-field"]'));
    };
    this.sportActiveOutboundEquipment = function (nameOFEquipment) {
        return element(by.css('[translate="trips.extra.'+nameOFEquipment+'_sold.flight_out"]'))
    };
    this.sportActiveInboundEquipment = function (nameOFEquipment) {
        return element(by.css('[translate="trips.extra.'+nameOFEquipment+'_sold.flight_back"]'))
    };
};
module.exports = SportEquipmentPage;
var SelectTransferProviderPage = function(){

    this.titleAirportTransfers = function(){
      return element(by.css('[translate="trips.extra.transfer.drawer.title"]'));
    };
    this.airportTransfersHeaderMessage = function(){
      return element(by.css('[translate="trips.extra.transfer.drawer.header_text"]'));
    };
    this.outboundCardTitle = function(){
      return element(by.css('[translate="trips.extra.transfer.drawer.outbound_card_title"]'));
    };
    this.inboundCardTitle = function(){
        return element(by.css('[translate="trips.extra.transfer.drawer.inbound_card_title"]'));
    };
    this.titleOfFlight = function(index){
      return element.all(by.css('[class="transfer-form-title"]')).get(index);
    };
    this.transferProviderText = function(index){
        return element.all(by.css('[translate="trips.extra.transfer.drawer.transfer_provider"]')).get(index);
    };
    this.transferProviderDropDown = function(){
        return element(by.model("transferSubform.selectedItem"));
    };
    this.transferProviderName = function(index){
        return element.all(by.css('[class="provider-name ng-scope"]')).get(index);
    };
    this.downloadIcon = function(index){
        return element.all(by.css('[icon-id="icons.glyphs.download"]')).get(index);
    };
    this.downloadLink = function(index){
        return element.all(by.css('[class="download-link"]')).get(index);
    };
    this.rowNumber = function(index){
        return element.all(by.css('[class="row-number"]')).get(index);
    };
    this.selectRuteTypeText = function(index){
        return element.all(by.css('[translate="trips.extra.transfer.drawer.select_route_type"]')).get(index);
    };
    this.selectRuteTypeSingleRadioMarker = function(index){
        return element.all(by.css('[class="transfer-radio-button ng-untouched ng-valid ng-dirty ng-valid-parse"]')).get(index).element(by.css('[class="radio-marker"]'));
    };
    this.selectRuteTypeReturnRadioMarker = function(index){
        return element.all(by.css('[class="transfer-radio-button ng-untouched ng-valid ng-dirty"]')).get(index).element(by.css('[class="radio-marker"]'));
    };
    this.radioMarkerSingleText = function(index){
        return element.all(by.css('[translate="trips.extra.transfer.drawer.single"]')).get(index);
    };
    this.radioMarkerReturnText = function(index){
        return element.all(by.css('[translate="trips.extra.transfer.drawer.return"]')).get(index);
    };
    this.selectRouteTextTitle = function(index){
        return element.all(by.css('[trips.extra.transfer.drawer.select_route]')).get(index);
    };
    this.selectRouteRadioMarker = function(index){
        return element.all(by.repeater("item in transferSubform.offers | filter:transferSubform.isSingleOffer")).get(index).element(by.css('[class="radio-marker"]'));
    };
    this.selectRouteText = function(index){
        return element.all(by.repeater("item in transferSubform.offers | filter:transferSubform.isSingleOffer")).get(index).element(by.css('[ng-if="transferSubform.provider.length > 1"]'));
    };
    this.selectRoutePrice = function(index){
        return element.all(by.repeater("item in transferSubform.offers | filter:transferSubform.isSingleOffer")).get(index).element(by.css('[class="route-amount ng-binding"]'));
    };
    this.selectRoutePerPerson = function(index){
        return element.all(by.repeater("item in transferSubform.offers | filter:transferSubform.isSingleOffer")).get(index).element(by.css('[class="per-person ng-scope"]'));
    };
    this.rowNumberFlex = function(index){
        return element.all(by.css('[class="row-number flex-item"]')).get(index);
    };
    this.numberOfPassengersText = function(index){
        return element.all(by.css('[translate="trips.extra.transfer.drawer.no_of_passengers"]')).get(index);
    };
    this.numberOfPassengers = function(index){
        return element.all(by.css('[class="value-display ng-binding ng-scope set"]')).get(index);
    };
    this.numberOfPassengers0 = function(index){
        return element.all(by.css('[class="value-display ng-binding ng-scope"]')).get(index);
    };
    this.minusButtonDisabled = function(index){
        return element.all(by.css('[class="btn dec disabled"]')).get(index);
    };
    this.plusButtonDisabled = function(index){
        return element.all(by.css('[class="btn inc disabled"]')).get(index);
    };
    this.plusButtonIncrement = function(index){
        return element.all(by.css('button.core-btn.inc.core-btn-wrap')).get(index);
    };
    this.plusButtonIncrement1 = function(index){
        return element.all(by.css('[ng-click="increment()"]')).get(index);
    };
    this.minusButtonDecrement = function(index){
        return element.all(by.css('button.core-btn.dec.core-btn-wrap')).get(index);
    };
    this.termsAndConditions = function(index){
        return element.all(by.css('[translate="trips.extra.transfer.drawer.terms_and_condition"]')).get(index);
    };
    this.closeButtonX = function () {
        return element(by.css('[ng-click="closeThisDialog(\'x\')"]'));
    };
    this.cancelButton = function(index){
        return element.all(by.css('[translate="trips.extra.transfer.drawer.cancel"]')).get(index);
    };

    this.btnCancelTransfer = function () {
        return element(by.css("button.core-link[translate='trips.extra.transfer.drawer.cancel']"));
    };

    this.confirmButton = function(){
        return element.all(by.css("button[translate='trips.extra.transfer.drawer.confirm']")).get(1);
    };
    this.confirmButton1 = function(index){
        return element.all(by.css('[translate="trips.extra.transfer.drawer.confirm"]')).get(index);
    };
    this.totalPriceText = function(){
        return element(by.css('[translate="trips.extra.bags.total_price"]'));
    };
    this.totalPriceAmount = function(){
        return element(by.css('[class="footer-container"]')).element(by.css('[class="amount ng-binding"]'));
    };
    this.selectProviderDropDownPlus = function(){
        return element(by.model("transferSubform.selectedItem")).$('[value = "PLUS"]');
    };
    this.selectProviderDropDownPlus1 = function(){
        return element(by.model("transferSubform.selectedItem")).$('[value = "PLUS1"]');
    };
    this.selectProviderDropDownDefault = function(){
        return element(by.model("transferSubform.selectedItem")).element(by.css('[selected="selected"]'));
    };
    this.noTransferProvided = function(){
        return element(by.css('[translate="trips.extra.transfer.drawer.no_transfer"]'));
    };
    this.checkIfSelectRouteIsSelected = function(index){
       return element.all(by.css('[class="route-radio-wrapper ng-scope selected"]')).get(index);
    };
    this.maxNumberOfPassengersMessage = function(index){
        return element.all(by.css('[class="tooltip-callout-content top ng-hide"]')).get(index).element(by.css('[translate="trips.extra.transfer.drawer.tooltip_message"]'));
    };
    this.maxNumberOfPassengersMessageText = function(index){
        return element.all(by.css("div.spinner > tooltip-callout > div > ng-transclude > span")).get(index);
    };
    this.maxNumberOfPassengersMessageClose = function(index){
        return element.all(by.css('[class="tooltip-callout-content top ng-hide"]')).get(index);
    };
    this.minusButtonDecrement1 = function(index){
        return element.all(by.css('[ng-click="decrement()"]')).get(index);
    };
    this.greenCheckIcon = function(index){
        return element.all(by.css('[icon-id="icons.glyphs.circle-tick"]')).get(index);
    };
    this.transferFormMessageText = function(index){
        return element.all(by.css('[class="transfer-form-message"]')).get(index);
    };
    this.transferFormMessageAmount = function(index){
        return element.all(by.css("div.transfer-form > div.transfer-form-body > div.transfer-form-message > span")).get(index);
    };

};
module.exports = SelectTransferProviderPage;

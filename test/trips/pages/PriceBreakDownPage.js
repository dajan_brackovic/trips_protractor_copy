var PriceBreakDownPage = function(){

    this.openDropPriceBreakDown = function () {
        return element(by.css('.basket-arrow'));
    };

    this.checkBoxPayPalOnPriceBreakDown = function () {
        return element(by.css("div.payment-method.PP"));
    };

    this.checkBoxCreditCardOnPriceBreakDown = function () {
        return element(by.css("div.payment-method.CC input"));
    };

    this.checkBoxDebitCardOnPriceBreakDown = function () {
        return element(by.css("div.payment-method.DC"));
    };

    this.paymentFeeLabel = function () {
        return element.all(by.css("span[translate='trips.fees.payment_fee']"));

    };

    this.paymentFeeValue = function () {
        return element.all(by.css("div.fee-amount")).get(1);
    };

    this.textMessageNoFlightPriceBrakeDown = function () {
        return element(by.css(".no-flights-description"));
    };

    this.priceOutFlightValue = function () {
        return element.all(by.css(".item-price")).get(0);
    };

    this.priceInFlightValue = function () {
        return element.all(by.css(".item-price")).get(1);
    };

    this.priceInBusinessFlightValue = function () {
        return element.all(by.css("div.trip-item")).get(1).element(by.css("strong.item-price"));
    };

    this.flightOUTInfo = function () {
        return element.all(by.css(".dest-info")).get(0).getText();
    };

    this.flightRETURNInfo = function () {
        return element.all(by.css(".dest-info")).get(1).getText();
    };

    this.priceTotalFlightValue = function () {
        //return element.all(by.css("section > div.total-price-breakdown > strong")).get(0);
        return element(by.css("div.overall-total span.price-amt"));
    };

    this.noFlightIcon = function (){
        return element(by.css("div.trips-no-flights-selected > div:nth-child(1) > core-icon > div > svg"));
    };

    this.payByMethodFee = function () {
        //return element.all(by.css(".method-type-price"));
        return element.all(by.model("pms.model"));
    };

    this.payByMethodFeeText = function () {
        //return element.all(by.css(".method-type-price"));
        return element.all(by.css("div.fee-amount"));
    };

    this.seatsValue = function (){
      //  return element(by.css("div.trip-item:not(.ng-isolate-scope) strong"));
        return element.all(by.css("div.trip-item:not(.ng-isolate-scope) strong")).get(1);
    }

    this.businessSeatsValue = function (){
        return element.all(by.css("div.business-discounts li")).get(2);
    }

    this.normalBagsValue = function (){
       // return element.all(by.css("div.trip-item.ng-isolate-scope[model='listItem.bagsTotal'] strong")).get(0);
       // return element.all(by.css("[model='listItem.bagsTotal'] strong")).get(0);
        return element.all(by.css("strong.item-price.has-remove-icon")).get(0);
    }

    this.largeBagsValue = function (){
       // return element.all(by.css("div.trip-item.ng-isolate-scope[model='listItem.bagsTotal'] strong")).get(1);
       // return element.all(by.css("[model='listItem.bagsTotal'] strong")).get(1);
        return element.all(by.css("strong.item-price.has-remove-icon")).get(1);
    }

    this.airportTransfersItem = function (){
        return element.all(by.css("div[type='transfer']"));
    };

    this.priorityBoardingOnPriceBreakDown = function () {
        return element(by.css("[translate='trips.basket.group.item.priority-boarding']"));
    };

    // Price BreakDown On Payment Page

    this.listOfSelectedItems = function () {
        return element.all(by.css("li.list-item div li.sub-item span"));
    }

    this.listOfSelectedItemsInBold = function () {
        return element.all(by.css("li.list-item div h3"));
    }

    this.parkingAdded = function () {
        return element(by.css("[ng-if='listItem.hasParking'] [translate='trips.extra.price_breakdown_label.PARKING']"));
    }

    this.bagCard = function () {
        return element(by.css("[model='listItem.bagsTotal']"));
    };

    this.seatCard = function () {
        return element(by.css("div.trip-item[type='journey']:not([model='booking.journeys[0]'])"));
    };

    this.totalPriceWholeNumber = function () {
        return element(by.css("div.overall-total span.price-amt"));
    };

    this.hotelInfoPriceBkDownPaymentPage = function () {
        return element(by.css("div[ng-switch-when='accommodation']"));
    };

    this.carHireInfoPriceBkDownPaymentPage = function () {
        return element(by.css("h3[translate='trips.basket.group.cars.heading']"));
    };

    this.carHireInsuranceDetails = function () {
        return element(by.css("div.breakdown-list-container > ul > li:nth-child(1) > div > div > ul > li:nth-child(2)"));
    };

    this.ccPaymentFee = function (tripway) {
        if(tripway === "oneway") {
            return element.all(by.css("strong.item-price")).get(1);
        }
        else{
            return element.all(by.css("strong.item-price")).get(2);
        }
    };

    // x  out selected seat

    this.xOutSelectedSeat = function () {
        return element(by.css("a.remove-item.ng-scope"));
    };

    //Car Hire Total

    this.totalPriceCarHireOnPriceBreakDown = function () {
        return element.all(by.css("span.price-amt")).get(1);
    };

    this.totalPriceOnPriceBreakDownWhenCarHireIsNotPresent = function () {
        return element.all(by.css("span[translate='trips.basket.group.cars.total-price']"));
    };

    this.priceBreakDownCarHireText = function () {
        return element(by.css("span[translate='trips.basket.group.cars.total-price']"));
    };

    //Hotels Info.
    this.totalHotelsInfoOnPriceBreakdown = function () {
        return element(by.css("div[ng-switch-when='accommodation']"));
    };

    this.hotelNamePriceBkDown = function () {
        return element(by.css("div[ng-switch-when='accommodation'] span.item-name"));
    };

    this.hotelTotalPriceBkDown = function () {
        return element.all(by.css("div.basket-subtotal span.price-amt")).get(1);
    };

    this.totalPriceInBold = function () {
        return element.all(by.css("div.basket-subtotal span.price-amt"));
    };

    this.checkInOutDates = function () {
        return element(by.css("h6.item-label.hotel-name"));
    };

    this.bookingDotComLogo = function () {
        return element(by.css("div.powered-logo"));
    };

    // Locators for UK Child Discount C29017

    this.listOfPricesInPriceBreakdown = function () {
        return element.all(by.css(".item-price"));
    };

    this.ukChildDiscountLabel = function () {
        return element(by.css("[translate='trips.breakdown.fats.CB4']"));
    };

    this.ukChildDiscountLabelUkDomestic = function () {
        return element.all(by.css("[translate='trips.breakdown.fats.CB4']"));
    };


};
module.exports = PriceBreakDownPage;
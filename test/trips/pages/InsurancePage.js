var InsurancePage = function () {

    this.temp = function () {
        return element(by.css());
    };
    this.tempAll = function (i) {
        return element.all(by.css('[ng-click="model.incPrice()"]')).get(i);
    };
    this.titleInsurancePolicySite = function () {
        return browser.driver.findElement(by.css("h1"));
    };
    this.insuranceAdded = function(){
        return element(by.css("body > div.FR > main > div.body-section > div > div:nth-child(1) > div:nth-child(2) > div.carousel-wrapper > div.section-content > ul > li:nth-child(3) > div > div > div.content-column-extra > div.content-column-extra-active > p > strong"));
    };
    this.titleInsurance = function () {
        return element(by.css('[translate="trips.extra.insurance.title"]'));
    };
    this.closeButtonX = function () {
        return element(by.css('[class="dialog-close pull-right ng-isolate-scope"]'));
    };
    this.cancelButton = function (i) {
        return element.all(by.css('[translate="trips.side.cancel"]')).get(i);
    };
    this.confirmButton = function () {
        return element.all(by.css('[ng-click="vm.confirmInsurance()"]')).get(1);
    };
    this.confirmButtonClick = function () {
        return element.all(by.css('[ng-click="vm.confirmInsurance()"]')).last();
    };
    this.cornerPrice = function () {
        return element.all(by.css('[class="amount ng-binding"]')).last();
    };
    this.confirmButtonDisabled = function () {
        return element(by.css('[class="primary primary-disabled"]'));
    };
    this.confirmButtonEnabled = function () {
        return element(by.css("button.primary"));
    };
    this.informationText1 = function () {
        return element(by.css('[translate="trips.extra.insurance.message.title"]'))
    };
    this.informationText2 = function () {
        return element(by.css('[translate="trips.extra.insurance.message.subtitle"]'))
    };
    this.informationChildTeen= function () {
        return element(by.css('[translate="trips.extra.insurance.discount_alert"]'))
    };
    this.comparasionTable = function () {
        return element(by.css('[class="insurance-info"]'));
    };
    this.viewPolicy = function () {
        return element(by.css('[translate="trips.extra.insurance.view_policy"]'));
    };
    this.policyDropDown = function () {
        return element(by.css('[ng-change="vm.onCountryChanged()"]'));
    };
    this.passengerTypeAndNumber = function (i) {
        return element.all(by.css('[class="persons-name"]')).get(i);
    };
    this.downloadPolicy = function () {
        return element(by.css('[translate="trips.extra.insurance.policy_link_IE"]'));
    };
    this.insuranceInfantIcon = function (i) {
        return element.all(by.css('[class="plus-infant ng-isolate-scope"]')).get(i);
    };
    this.countryOfResidenceForPassanger = function (i) {
        return element.all(by.model("vm.selectedCountry")).get(i);
    };
    this.policyPDFIcon = function (i) {
        return element(by.css('[icon-id="icons.glyphs.document"]'));
    };
    this.standardInsuranceButtonSelected = function () {
        return element(by.css('[class="confirm-insurance selected"]'));
    };
    this.standardInsuranceButton = function (i) {
        return element.all(by.css('[translate="trips.extra.insurance.insurance_package"]')).get(i);
    };
    this.insurancePlusButtonSelected = function () {
        return element(by.css('[class="confirm-insurance right selected"]'));
    };
    this.insurancePlusButton = function (i) {
        return element.all(by.css('[ng-click="vm.selectPackage(vm.passenger.type, vm.packageTypes[1])"]')).get(i);
    };
    this.priceOnInsuranceButtons = function (i) {
        return element.all(by.css('[class="package-price"]')).get(i);
    };
    this.cornerPrice = function () {
        return element.all(by.css('[class="amount ng-scope"]')).last();
    };
    //this.priceOnInsuranceButtons = function (i) {
    //    return element.all(by.css('[class="package-title ng-scope"]')).get(i);
    //};
    this.yesPleaseButton = function () {
        return browser.driver.findElement(by.css('button[translate="trips.extra.insurance.modal.yes"]'));
    };
    this.noThanksButton = function () {
        return browser.driver.findElement(by.css('[class="pick-seats-btn different-btn ng-scope"]'));
    };
    this.questionOnInsurancePopup = function () {
        return browser.driver.findElement(by.css('[translate="trips.extra.insurance.modal.question"]')).getText();
    };
    this.clearAllLink = function(){
      return element(by.css('[ng-click="!vm.clearAll()"]'));
    };
    this.insuranceButtonWithOutPrice = function(i){
        return element.all(by.css('[disabled="disabled"]')).get(i);
    };
    this.insuranceDiscountMessage = function(i){
        return element.all(by.css('[ng-show="vm.hasDiscount"]')).get(i);
    };
    this.insurancePlusAddedActive = function(index){
        return element.all(by.css('[translate="trips.extra.insurance.sold_insurance_plus"]')).get(index);
    };
    this.standardInsuranceAddedActive = function(index){
        return element.all(by.css('[translate="trips.extra.insurance.sold_insurance_plus"]')).get(index);
    };
    this.insuranceAddedHeader = function(){
        return element.all(by.css('[translate="trips.header.title.extras_INS"]'));
    };


};

module.exports = InsurancePage;
var paymentPage = function(){

    this.btnExtrasContinue = function () {
        return element(by.css(".pull-right.ng-scope"));
    };
    this.btnPaymentContinue = function () {
        return element.all(by.css("button.core-btn-primary.core-btn-medium")).get(2);
    };
    this.selectExpiryYearDropDownOption = function (option) {
        return element(By.xpath('//option[text() = \'' + option + '\']'));
    };
    this.textFieldCVV = function () {
        return element(by.model("payment.securityCode"));
    };
    this.textFieldCardHoldersName = function () {
        return element(by.model("payment.cardHolderName"));
    };
    this.textFieldCity = function () {
        return element(by.model("address.city"));
    };
    this.textFieldPostCode = function () {
        return element(by.model("address.postcode"));
    };
    this.textFieldStreetName = function () {
        return element(by.model("address.street"));
    };
    this.selectExpiryMonthDropDownOption = function (option) {
        return element(By.xpath("//select[@ng-model='payment.expiry.month'] //option[@label="+option+"]"));
    };
    this.selectPaymentTypeDropDownOption = function (option) {
        return element(By.xpath('//option[text() = \'' + option + '\']'));
    };
    this.textFieldCardNumber = function () {
        return element(by.model("payment.cardNumber"));
    };
    this.infantFirst = function (indexFirst) {
        return element(By.xpath("(//input[@name='infantFirstName'])[" + indexFirst + "]"));
    };
    this.infantLast = function (indexLast) {
        return element(By.xpath("(//input[@name='infantLastName'])[" + indexLast + "]"));
    };
    this.infantDAY = function (indexDay, textToSelect) {
        return element(By.xpath("(//*[@name='day'])[" + indexDay + "]/option[@label ='"+textToSelect+"']"));
    };
    this.infantMONTH = function (indexMonth, textToSelect) {
        return element(By.xpath("(//*[@name='month'])[" + indexMonth + "]/option[@label ='"+textToSelect+"']"));
    };
    this.infantYEAR = function (indexYear, textToSelect) {
        return element(By.xpath("(//*[@name='year'])[" + indexYear + "]/option[@label ='"+textToSelect+"']"));
    };
    this.btnAddPaxSave = function () {
        return element(By.css("button[ng-click='addPassengersForm.$valid && vm.savePassengers()']"));
    };
    this.textFieldEmailAddress = function () {
        return element(by.css("form[name=contactForm] input[name=emailAddress]"));
    };
    this.selectCountryDropDown = function () {
        return element(by.model("cForm.contact.Country"));
    };
    this.selectCountryDropDownOption = function (option) { // Ireland
        return element(by.cssContainingText("form[name=contactForm] select option", option));
    };
    this.selectTitleDropDownOption = function (option, index) {
        return element(By.xpath('(//*[@name="paxTitle"])[' + index + '] //option[text() = \'' + option + '\']'));
    };
    this.textFieldFirstName = function (index) {
        return element(By.xpath("(//*[@name ='firstName'])[" + index + "]"));
    };
    this.textFieldLastName = function (index) {
        return element(By.xpath("(//*[@name ='surname'])[" + index + "]"));
    };
    this.textFieldPhoneNumber = function () {
        return element(by.model("cForm.contact.PhoneNumber"));
    };
    this.btnAddPaxPageContinue = function () {
        return element(by.css("form[name=contactForm] div.action-block button"));
    };
    this.checkOutPaymentPageTitle = function(){
        return element(by.css('[translate="trips.checkout.header.title"]'));
    };
    this.checkOutForPriceBreakdown = function(index){
        return element.all(by.css('[translate="trips.summary.heading.price_breakdown"]')).get(index);
    };
    this.priceBreakDownFlightOutName = function(index){
        return element.all(by.css('[class="item-label ng-binding"]')).get(index);
    };
    this.priceBreakDownAdultFare = function(index){
        return element.all(by.repeater('item in model.fares')).get(index).element(by.css('[class="item-label ng-binding"]'));
    };
    this.priceBreakDownPrice = function(index){
        return element.all(by.repeater('item in model.fares')).get(index).element(by.css('[class="item-price ng-binding"]'));
    };
    this.priceBreakDownExtraEquipmentTitle = function(index){
        return element.all(by.repeater('code in listItem.equipmentCodes')).get(index).element(by.css('[class="item-label ng-binding"]'));
    };
    this.priceBreakDownExtraEquipmentAmount = function(index){
        return element.all(by.repeater('code in listItem.equipmentCodes')).get(index).element(by.css('[class="sub-items ng-scope"]')).element(by.css('[class="item-label ng-binding"]'));
    };
    this.priceBreakDownExtraEquipmentPrice = function(index){
        return element.all(by.repeater('code in listItem.equipmentCodes')).get(index).element(by.css('[class="sub-items ng-scope"]')).element(by.css('[class="item-price ng-binding"]'));
    };
    this.priceBreakDownInsuranceTitle = function(index){
        return element.all(by.css('[ng-if="lockFareTotalPrice === 0 && listItem.hasInsurance"]')).get(index).element(by.css('[translate="trips.extra.insurance.insurance_label"]'));
    };
    this.priceBreakDownInsuranceAmount = function(index){
        return element.all(by.css('[ng-if="lockFareTotalPrice === 0 && listItem.hasInsurance"]')).get(index).element(by.repeater('item in model')).element(by.css('[translate="trips.extra.package_name_RYAN31"]'));
    };
    this.priceBreakDownInsurancePrice = function(index){
        return element.all(by.css('[ng-if="lockFareTotalPrice === 0 && listItem.hasInsurance"]')).get(index).element(by.repeater('item in model')).element(by.css('[class="item-price ng-binding"]'));
    };
    this.priceBreakDownParkingTitle = function(index){
        return element.all(by.css('[ng-if="lockFareTotalPrice === 0 && listItem.hasParking"]')).get(index).element(by.css('[translate="trips.extra.price_breakdown_label.PARKING"]'));
    };
    this.priceBreakDownParkingPlace = function(index){
        return element.all(by.css('[ng-if="lockFareTotalPrice === 0 && listItem.hasParking"]')).get(index).element(by.css('[class="item-label ng-scope"]'));
    };
    this.priceBreakDownParkingPrice = function(index){
        return element.all(by.css('[ng-if="lockFareTotalPrice === 0 && listItem.hasParking"]')).get(index).element(by.css('[class="item-price ng-binding"]'));
    };
};

module.exports = paymentPage;
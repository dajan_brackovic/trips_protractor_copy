var ManageTripsPage = function(){


    this.manageTripDropDown = function() {
       // return element.all(by.repeater("menu in getMenuLinks().leftMenu")).get(1);
        return element(by.id("manage-trips"));
    };
    this.reservationNumber = function() {
        return element(by.css("[ng-model='myTrips.info.reservationNum']"));
    };
    this.reservationEMail = function() {
        return element(by.model("myTrips.info.email"));
    };
    this.manageTripGoButton = function() {
        return element(by.css('[translate="trips.my_trips.go"]'));
    };
    this.manageTripMailCheckBox = function() {
        return element(by.css("input[id^='radio-card']"));
    };
    this.reservationCardNumber = function() {
        return element(by.model("myTrips.info.cardNum"));
    };



    this.checkBoxFlightDetails = function () {
        return element(by.css("input[id^='radio-flight']"));
    };

    this.calenderFieldFlyingOutOn = function () {
        return element(by.css("div[id^='date']"));
    };

    this.flyingOutOn = function (index) {
        return element(by.css("li [data-id='" + index + "'] span"));
    };


    this.fieldEmailAddress = function () {
        return element(by.css("[id^='flightEmail']"));
    };

    this.fieldFrom = function () {
        return element(by.css("input[aria-labelledby='label-from-input']"));
    };

    this.fieldTo = function () {
        return element(by.css("input[aria-labelledby='label-to-input']"));
    };

    this.firstAirportOptionInDropDown = function () {
        return element(by.css("span.country.ng-binding.ng-scope"));
    };


    this.retrieveFromMultipleBookings = function () {
        return element(by.css("[translate='trips.check-in.flight-heading']"));
    };

    this.yourBooking = function (bookingRef) {
        return element(by.cssContainingText('.pnr.ng-binding', bookingRef));
    };

    this.btnContinueRetrieveFromMultipleBookings = function () {
        return element(by.css("button.core-btn-primary"));
    };


    // My Fr Manage Trip


    this.manageMyTripHeader = function () {
        return element(by.css("#my-trips-header"));
    };

    this.upcomingTripsTitle = function () {
        return element(by.css("[translate='MYRYANAIR.MY_TRIPS.EMPTY_UNCOMING_TRIPS.UNCOMING_TRIPS']"));
    };

    this.savedTripsTitle = function () {
        return element(by.css("[translate='MYRYANAIR.MY_TRIPS.EMPTY_SAVED_TRIPS.SAVED_TRIPS']"));
    };

    this.savedTripContent = function () {
        return element.all(by.css("saved-trip-card"));
    };

    this.savedTripDestination = function () {
        return element.all(by.css("saved-trip-card .card-trip-content-destination"));
    };

    this.savedTripDeparture = function () {
        return element.all(by.css("saved-trip-card .card-trip-content-departure"));
    };

    this.savedTripDate = function () {
        return element.all(by.css("saved-trip-card .card-trip-content-date"));
    };

    this.savedTripPrice = function () {
        return element.all(by.css("saved-trip-card .card-trip-footer-price"));
    };

    this.btnRemoveSavedTrip = function () {
        return element.all(by.css("[icon-id='icons.glyphs.trash']"));
    };

    this.upcomingTripsCards = function () {
        return element.all(by.css("[ng-click='upcomingTripCard.openActiveTrip()']"));
    };

    this.upcomingTripsCardsImage = function () {
        return element.all(by.css("upcoming-trip-card .card-trip-image"));
    };

    this.upcomingTripsCardsOrigin = function () {
        return element.all(by.css("upcoming-trip-card .card-trip-content-departure"));
    };

    this.upcomingTripsCardsDestination = function () {
        return element.all(by.css("upcoming-trip-card .card-trip-content-destination"));
    };

    this.upcomingTripsCardsJourneyDate = function () {
        return element.all(by.css("upcoming-trip-card .card-trip-content-date"));
    };

    this.upcomingTripsCardsCheckInOpenDate = function () {
        return element.all(by.css("div [translate='MYRYANAIR.MY_TRIPS.UPCOMING.CARD.CHECK_IN_OPENS']"));
    };

    this.heldFareCard = function () {
        return element.all(by.css('[translate="MYRYANAIR.MY_TRIPS.UPCOMING.CARD.LOCKED"]'));
    };

    this.heldFareTimeLeft = function () {
        return element.all(by.css('[translate="MYRYANAIR.MY_TRIP.UPCOMING.CARD.LOCKED_EXPIRY"]'))
    };

};
module.exports = ManageTripsPage;
var HoldFarePage = function () {


    this.btnConfirmHoldFare = function () {
        return element(by.css("button.core-btn-primary[translate='trips.seats.confirm']"));
    };

    this.btnCancelHoldFare = function () {
        return element(by.css("button[translate='trips.seats.btn_cancel']"));
    };
};

module.exports = HoldFarePage;
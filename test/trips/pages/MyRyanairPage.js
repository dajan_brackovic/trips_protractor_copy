var MyRyanairPage = function () {


    this.userName = function () {
        return element(by.css("span.username"));
    };

    // DropDown Options
    this.linkDashboard = function () {
        return element(by.css("div.user-dropdown.active ul li a[translate='MYRYANAIR.LAYOUT.HEADER.MENU.DASHBOARD']"));
    };

    this.linkSettings = function () {
        return element(by.css("div.user-dropdown.active ul li a[translate='MYRYANAIR.LAYOUT.HEADER.MENU.SETTINGS']"));
    };

    this.linkLogOut = function () {
        return element(by.css("div.user-dropdown.active ul li a[translate='MYRYANAIR.LAYOUT.HEADER.MENU.LOGOUT']"));
    };

    //My RyanAir Page

    this.editProfile = function () {
        return element(by.css("[ng-click='goToProfile()']"));
    };

    this.editProfileButton = function () {
        return element(by.css("[ng-click='vm.edit()']"));
    };

    this.profileAddDocButton = function () {
        return element(by.css(".core-btn-link-n.icon-l"));
    };

    this.callingCodeField = function (index) {
        return element(by.css("[ng-model='vm.editProfile.countryCallingCode'] option:nth-child(" + index + ")"));
    };

    this.phoneNumberBox = function () {
        return element(by.css("[ng-model='vm.editProfile.phoneNumber']"));
    };

    this.saveProfileButton = function () {
        return element(by.css("[translate='MYRYANAIR.ACCOUNT.PASSENGERS.PROFILE.PROFILE_EDIT.SAVE']"));
    };

    this.dashboardTab = function () {
        return element(by.css("ul.side-nav li span[translate='MYRYANAIR.ACCOUNT.DASHBOARD_ACCOUNT']"));
    };

    this.passengersTab = function () {
        return element(by.css("ul.side-nav li span[translate='MYRYANAIR.ACCOUNT.PASSENGERS_ACCOUNT']"));
    };

    this.paymentsTab = function () {
        return element(by.css("ul.side-nav li span[translate='MYRYANAIR.ACCOUNT.PAYMENTS_ACCOUNT']"));
    };

    this.preferencesTab = function () {
        return element(by.css("ul.side-nav li span[translate='MYRYANAIR.ACCOUNT.PREFERENCES_ACCOUNT']"));
    };

    this.rewardsTab = function () {
        return element(by.css("ul.side-nav li span[translate='MYRYANAIR.ACCOUNT.REWARDS_ACCOUNT']"));
    };

    this.addDocumentBtn = function () {
        return element(by.css("button.core-btn-link-n"));
    };

    this.dropDownSelectNationality = function (index) {
        return element(by.css("select[id^=nationality] option:nth-child(" + index + ")"));
    };

    this.optionPassportInDropDownDocumentType = function () {
        return element(by.css("select[ng-model='addValue.type'] option[value='string:P']"));
    };

    this.fieldDocumentNumber = function () {
        return element(by.css("input[id^='number']"));
    };

    this.firstDayOfExpiryDate = function () {
        return element(by.css("select[ng-model='val.date'] option[value='number:1']"));
    };

    this.janMonthOfExpiryDate = function () {
        return element(by.css("select[ng-model='val.month'] option[value='number:1']"));
    };

    this.year2020OfExpiryDate = function () {
        return element(by.css("select[ng-model='val.year'] option[value='number:2020']"));
    };

    this.optionIrelandInCountryOfIssue = function () {
        return element(by.css("select[ng-model='addValue.countryOfIssue'] option[value='string:IE']"));
    };

    this.optionEditIrelandInCountryOfIssue = function (index) {
        return element(by.css("select[ng-model='editValue.countryOfIssue'] option:nth-child(" + index + ")"));
    };

    this.btnAdd = function () {
        return element(by.css("button.core-btn-primary.core-btn-phone-full"));
    };

    this.btnUpdateDocumentDetails = function () {
        return element(by.css("[translate='MYRYANAIR.ACCOUNT.PASSENGERS.DOCUMENTS.DOCUMENT_VIEW.UPDATE']"));
    };

    this.btnSelectProfileDocument = function () {
        return element(by.css("button.core-btn-link"));
    };

    this.btnRemoveProfileDocument = function () {
        return element(by.css("[translate='MYRYANAIR.ACCOUNT.PASSENGERS.DOCUMENTS.DOCUMENT_VIEW.REMOVE']"));
    };

    this.btnShowPassportDetails = function () {
        return element(by.css("button.core-btn-link"));
    };

    this.btnRemove = function () {
        return element(by.css("button[translate='MYRYANAIR.ACCOUNT.PASSENGERS.DOCUMENTS.DOCUMENT_VIEW.REMOVE']"));
    };

    this.myRyanairProgressinPercentage = function () {
        return element(by.css("div.percentage"));
    };

    this.btnPassengersMyRyanair = function () {
        return element(by.css("[translate='MYRYANAIR.ACCOUNT.PASSENGERS_ACCOUNT']"));
    };

    this.btnEditCompanion = function () {
        return element(by.css("[icon-id='icons.glyphs.edit']"));
    };

    this.btnRemoveCompanionTravelDoc = function () {
        return element(by.css("[translate='MYRYANAIR.REMOVE']"));
    };

    this.btnEditPassportDetails = function () {
        return element(by.css("[ng-click='vm.editDocument(obj.key)']"));
    };

    this.saveCompanionField = function () {
        return element(by.css("[translate='MYRYANAIR.COMPANION.SAVE_COMPANION']"));
    };

    this.editCompanionLastName = function () {
        return element(by.model("vm.companionClone.lastName"));
    };

    // Add Companion

    this.btnAddACompanion = function () {
        return element(by.css("[translate='MYRYANAIR.ACCOUNT.PASSENGERS.PROFILE_CARD.PROFILE_CARD_BASIC.ADD_COMPANION']"));
    };

    this.btnAddCompanionsCompanionPanel = function () {
        return element(by.css("button[translate='MYRYANAIR.ACCOUNT.PASSENGERS.COMPANIONS.ADD_NEW_COMPANION']"));
    };

    this.typeFamilyCompanion = function () {
        return element(by.css("[translate='MYRYANAIR.COMPANION.FAMILY']"));
    };

    this.typeFriendCompanion = function () {
        return element(by.css("[translate='MYRYANAIR.COMPANION.FRIEND']"));
    };

    this.typeColleagueCompanion = function () {
        return element(by.css("[translate='MYRYANAIR.COMPANION.COLLEAGUE']"));
    };

    this.dropDownTitle = function (title) {
        return element(by.css("select[id^='title'] option[label=" + title + "]"));
    };

    this.dropDownTitleOptions = function () {
        return element.all(by.css("select[id^='title'] option"));
    };

    this.dropDownTitleSelectOnly = function () {
        return element(by.css("select[id^='title']"));
    };

    this.dropDownTitleIndexOption = function (index) {
        return element.all(by.css("select[id^='title'] option")).get(index);
    };

    this.fieldFirstName = function () {
        return element(by.css("input[id^='firstName']"));
    };

    this.fieldLastName = function () {
        return element(by.css("input[id^='lastName']"));
    };

    this.dropDownDateOfBirthDay = function (dayIndex) {
        return element(by.css("[ng-model='val.date'] option:nth-child(" + dayIndex + ")")); //  index 2 is day 1.
    };

    this.dropDownDateOfBirthMonth = function (monthIndex) {
        return element(by.css("[ng-model='val.month'] option:nth-child(" + monthIndex + ")")); // index 2 is month January.
    };

    this.dropDownDateOfBirthYear = function (yearIndex) {
        return element(by.css("[ng-model='val.year'] option:nth-child(" + yearIndex + ")")); // index 2 is year [current year].
    };

    this.btnSaveCompanion = function () {
        return element(by.css("[translate='MYRYANAIR.COMPANION.SAVE_COMPANION']"));
    };

    this.btnRemoveCompanion = function () {
        return element(by.css("[ng-click='vm.deleteCompanion()']"));
    };

    this.btnAddTravelDoc = function () {
        return element(by.css(".core-btn-link-n.icon-l.core-btn-phone-full"));
    };

    this.dropDownDateOfBirth = function (index) {
        return element.all(by.css("select[ng-model='val.date'] option:nth-child(" + index + ")"));
    };

    this.dropDownMonthOfBirth = function (index) {
        return element.all(by.css("select[ng-model='val.month'] option:nth-child(" + index + ")"));
    };

    this.dropDownDocumentExpiryDate = function () {
        return element(by.css("select[id^='expiryDate']"));
    };

    this.dropDownYearOfBirth = function (index) {
        return element(by.css("select[ng-model='val.year'] option:nth-child(" + index + ")"));
    };

    this.dropDownDocumentExpiryDay = function (index) {
        return element.all(by.css("select[ng-model='val.date'] option:nth-child(" + index + ")")).get(1);
    };

    this.dropDownDocumentExpiryMonth = function (index) {
        return element.all(by.css("select[ng-model='val.month'] option:nth-child(" + index + ")")).get(1);
    };

    this.dropDownDocumentExpiryYear = function (index) {
        return element.all(by.css("select[ng-model='val.year'] option:nth-child(" + index + ")")).get(1);
    };

    this.dropDownDocumentCountryOfIssue = function (index) {
        return element(by.css("select[ng-model='vm.documentClone.countryOfIssue'] option:nth-child(" + index + ")"));
    };

    this.dropDownNationalityDoc = function (option) {
        return element(by.css("select[ng-model='vm.documentClone.nationality'] option:nth-child(" + option + ")"));
    };

    this.profileDropDownNationalityDoc = function (option) {
        return element(by.css("select[ng-model='addValue.nationality'] option:nth-child(" + option + ")"));
    };

    this.dropDownDocumentType = function (option) {
        return element(by.css("select[ng-model='vm.documentClone.type'] option:nth-child(" + option + ")"));
    };

    this.dropDownProfileDocumentType = function (option) {
        return element(by.css("select[ng-model='addValue.type'] option:nth-child(" + option + ")"));
    };

    this.dropDownProfileDocumentTypeAssert = function (option) {
        return element(by.css("select[ng-model='addValue.type']"));
    };

    this.dropDownEditProfileDocumentType = function (option) {
        return element(by.css("select[ng-model='editValue.type'] option:nth-child(" + option + ")"));
    };

    this.selectDropDocumentType = function () {
        return element.all(by.css("select[ng-model='vm.documentClone.type'] option"));
    };

    this.selectProfileDropDocumentType = function () {
        return element.all(by.css("select[ng-model='addValue.type'] option"));
    };

    this.fieldCompanionDocumentNumber = function () {
        return element(by.css("input[id^=documentNumber]"));
    };

    this.listBtnRemoveCompanion = function () {
        return element.all(by.css("[ng-click='vm.deleteCompanion()']"));
    };

    this.passengerFavouriteNameList = function () {
        return element.all(by.css("span[ng-bind^='vm.companion.firstName']"))
    };

    // My RyanAir Payments tab

    this.paymentView = function () {
        return element(by.css("[translate='MYRYANAIR.ACCOUNT.PAYMENT.PAYMENT_METHODS']"));
    };

    this.paymentMethod = function () {
        return element(by.css(".payment-method"));
    };

    this.expiryCardDate = function () {
        return element(by.css("[translate='MYRYANAIR.ACCOUNT.PAYMENT.EXPIRY_DATE']"));
    };

    this.addPayment = function () {
        return element(by.css("[translate='MYRYANAIR.ACCOUNT.PAYMENT.NEW_PAYMENT_METHOD']"));
    };

    this.dropDownCardTypeList = function () {
        return element(by.css("[id^='creditCardType']"));
    };

    this.dropDownCardType = function (index) {
        return element(by.css("[id^='creditCardType'] option:nth-child(" + index + ")")); // index 6 is master card
    };

    this.fieldCardNumber = function () {
        return element(by.css("input[id^='cardNumber']"));
    };

    this.dropDownExpiryMonth = function (monthIndex) {
        return element(by.css("select[id^='expiryMonth'] option:nth-child(" + monthIndex + ")")); // index 1 is day January.
    };

    this.dropDownExpiryYear = function (yearIndex) {
        return element(by.css("select[id^='year'] option:nth-child(" + yearIndex + ")")); // index 1 is day 2015.
    };

    this.fieldCardholderName = function () {
        return element(by.css("input[id^='cardholdersName']"));
    };

    this.dropDownCardUsage = function (index) {
        return element(by.css("select[id^='cardUsage'] option:nth-child(" + index + ")")); // index 1 is Personal and index 2 is Business.
    };

    this.btnContinue = function () {
        return element(by.css("[translate='MYRYANAIR.ACCOUNT.PAYMENTS.WIZARD.WIZARD_STEP_DETAILS.CARD_NEXT']"));
    };

    this.btnSavePaymentDetails = function () {
        return element(by.css("[translate='MYRYANAIR.ACCOUNT.PAYMENT.EDIT_CREDITCARD.SAVE']"));
    };

    this.dropDownBillingAddressCountry = function (index) {
        return element(by.css("[id^='billingAddressCountry'] option:nth-child(" + index + ")")); // index 93 is Ireland.
    };

    this.fieldBillingAddressCity = function () {
        return element(by.css("[id^='billingAddressCity']"));
    };

    this.fieldBillingAddressPostcode = function () {
        return element(by.css("[id^='billingAddressPostcode']"));
    };

    this.fieldBillingAddressLine1 = function () {
        return element(by.css("[id^='billingAddressAddressLine1']"));
    };

    this.fieldBillingAddressLine2 = function () {
        return element(by.model("ctrl.model.addressLine2"));
    };

    this.fieldBillingAddressLine3 = function () {
        return element(by.model("vm.billingAddress.street3"));
    };

    this.btnBillingAddressContinue = function () {
        return element(by.css("[translate='MYRYANAIR.ACCOUNT.PAYMENTS.WIZARD.WIZARD_STEP_ADDRESS.NEXT']"));
    };

    this.btnConfirmDetailsComplete = function () {
        return element(by.css("[translate='MYRYANAIR.ACCOUNT.PAYMENTS.WIZARD.WIZARD_STEP_CONFIRM.CARD_COMPLETE']"));
    };

    this.filedPaymentTabSecurityPassword = function () {
        return element(by.css("input[id^='paymentLogin']"));
    };

    this.btnOkPaymentTabSecurityPassword = function () {
        return element(by.css("[translate='MYRYANAIR.ACCOUNT.PAYMENTS.LOGIN.OK']"));
    };

    this.btnRemoveSavedPaymentDetails = function () {
        return element(by.css("[ng-click='vm.removePayment(item)']"));
    };

    this.btnConfirmRemove = function () {
        return element(by.css("[translate='MYRYANAIR.ACCOUNT.PAYMENT.DELETE_REMOVE']"));
    };

    this.starDefaultPayment = function () {
        return element(by.css("[icon-id='icons.glyphs.star']"));
    };

    this.setDefaultPayment = function () {
        return element(by.css('[ng-click="vm.setDefaultPaypal(item)"]'));
    };

    this.paymentsCardNumber = function () {
        return element.all(by.css('.info .label'));
    };

    this.accountPaymentConfirmDeleteDefaultMessage = function () {
        return element(by.css("[translate='MYRYANAIR.ACCOUNT.PAYMENT.DELETE_DEFAULT_MESSAGE']"));
    };

    this.dropDownNewDefaultPayment = function (index) {
        return element(by.css("[ng-model='vm.newDefault'] option:nth-child(" + index + ")"));
    };

    this.btnConfirmRemoveDefaultPayment = function () {
        return element(by.css("[translate='MYRYANAIR.ACCOUNT.PAYMENT.DELETE_DEFAULT_OK']"));
    };

    this.editPayment = function () {
        return element.all(by.css('[icon-id="icons.glyphs.edit"]'));
    };

    this.addedCardNumber = function () {
        return element(by.css("div.info .label"));
    };


    // Forgot Password Page

    this.formForgotPassword = function () {
        return element(by.css("#forgotPasswordForm"));
    };

    //MyRyanair Preferences Tab
    this.editPlanningTripPreferences = function () {
        return element(by.id("BTN_PLANNING_TRIP"));
    };

    this.editUpcomingTripPreferences = function () {
        return element(by.id("BTN_UPCOMING_TRIP"));
    };

    this.btnUpdateDocumentDetails = function () {
        return element(by.css("[translate='MYRYANAIR.ACCOUNT.PASSENGERS.DOCUMENTS.DOCUMENT_VIEW.UPDATE']"));
    };

    this.editAtAirportPreferences = function () {
        return element(by.id("BTN_AT_AIRPORT"));
    };

    this.editOnThePlanePreferences = function () {
        return element(by.id("BTN_ON_THE_PLANE"));
    };

    this.editAtDestinationPreferences = function () {
        return element(by.id("BTN_AT_YOUR_DESTINATION"));
    };

    this.editAtDestinationPreferences = function () {
        return element(by.id("BTN_AT_YOUR_DESTINATION"));
    };

    this.btnNextQuestion = function () {
        return element(by.css("button[ng-click='vm.nextQuestion()']"));
    };

    this.btnEditPreferencesBottom = function () {
        return element(by.css("button[translate='MYRYANAIR.ACCOUNT.PREFERENCE.EDIT_PREFERENCE']"));
    };

    this.lblSavedAnswers = function () {
        return element.all(by.css("span.answer"));
    };

    this.upcomingQuestion1Answer1 = function () {
        return element.all(by.css("span[translate$='QUESTION5.ANSWER1']"));
    };

    this.upcomingQuestion1Answer3 = function () {
        return element.all(by.css("span[translate$='QUESTION5.ANSWER3']"));
    };

    this.upcomingQuestion2Answer1 = function () {
        return element.all(by.css("span[translate$='QUESTION6.ANSWER1']"));
    };

    this.upcomingQuestion2Answer4 = function () {
        return element.all(by.css("span[translate$='QUESTION6.ANSWER4']"));
    };

    this.yesNoQuestionAnswerNo = function () {
        return element.all(by.css("span[translate$='NO']"));
    };

    this.yesNoQuestionAnswerYes = function () {
        return element.all(by.css("span[translate$='YES']"));
    };

    this.btnClosePreferences = function () {
        return element.all(by.css("button[translate$='PREFERENCE.CLOSE']"));
    };

    //MyRyanair
    this.addCompanionsDisabled = function () {
        return element(by.css("button[ng-click='vm.addNewCompanion()']"));
    };

    this.adultPaxTypeLabel = function () {
        return element(by.css("translate[translate='MYRYANAIR.COMPANION.ADULT']"));
    };

    this.infantPaxTypeLabel = function () {
        return element(by.css("translate[translate='MYRYANAIR.COMPANION.INFANT']"));
    };

    this.childPaxTypeLabel = function () {
        return element(by.css("translate[translate='MYRYANAIR.COMPANION.CHILD']"));
    };

    this.teenPaxTypeLabel = function () {
        return element(by.css("translate[translate='MYRYANAIR.COMPANION.TEEN']"));
    };


    this.btnshowAdvancedPreferences = function () {
        return element(by.css("button span[translate='MYRYANAIR.ACCOUNT.PREFERENCE.SHOW_ADVANCED_PREFERENCES']"));
    };

    this.btnPreferencesQuestionChangeComplete = function (whichQuestion) {
        var xPathbtn = "//div/span[text()='" + whichQuestion + "']/../button[1]";
        return element(by.xpath(xPathbtn));
    };

    //Function to return the answer button as element
    //selectAnswer = String with 'Yes'/'No'... and empty '' if blnNotHighlitedOne
    //Example : actions.myRyanairActions.btnPreferenceSelectAnswer("",true);
    this.btnPreferenceSelectAnswer = function (selectAnswer, blnNotHighlitedOne) {
        var myXPath = "";
        if (blnNotHighlitedOne === true) {
            //myXPath = "//p[text()='" + whichQuestion + "']/../button[@class='core-btn  core-btn-ghost answer-noimg' or @class='core-btn ng-scope active core-btn-ghost answer-noimg'][1]";
            myXPath = "//button[@class='core-btn  core-btn-ghost answer-noimg' or @class='core-btn ng-scope active core-btn-ghost answer-noimg'][1]";
        }
        else {
            myXPath = "//span[text()='" + selectAnswer + "']/.."

        }
        return element(by.xpath(myXPath));
    };

    this.btnPreferenceSaveAndClose = function () {
        return element(by.css('.core-btn-primary'));
    };

    this.lblPreferenceAnswer = function (whichQuestion) {
        return element(by.xpath("(//span[text()='" + whichQuestion + "']/following-sibling::span)[1]"));
    }
    this.btnPreferenceSaveAndClose = function () {
        return element(by.css('.core-btn-primary'));
    };

    this.lblEditPreferencesCurrentQuestion = function () {
        return element(by.xpath("//li/button[@translate='MYRYANAIR.ACCOUNT.PREFERENCE.CURRENT']/../span[1]"));
    }

    this.btnPlanningATripPreference = function () {
        return element(by.css("button#BTN_PLANNING_TRIP"));
    };

    this.btnEditPreferences = function () {
        return element(by.css("button[translate='MYRYANAIR.ACCOUNT.PREFERENCE.EDIT_PREFERENCE']"));
    };

    this.lblEditPreferencesActiveQuestionOnHeader = function () {
        return element(by.css("div[ng-if='vm.activeQuestion.id'] p[translate='MYRYANAIR.PREFERENCES.QUESTIONS.QUESTION1']"));
    };

    this.btnHolidayEditPreferences = function () {
        return element(by.css("button[id='BTN_AT_YOUR_DESTINATION']"));
    };

    this.btnHolidayEditPreferences = function () {
        return element(by.css("button[id='BTN_AT_YOUR_DESTINATION']"));
    };

    this.btnNextPreference = function () {
        return element(by.css("button[ng-click='vm.nextQuestion()']"));
    };
    //if starNumber is sent then that particular star will be clicked else clicks random star
    this.btnPreferenceStar = function () {
        return element.all(by.css("core-icon[icon-id='icons.glyphs.star']"));
    };

    this.lblPreferenceHolidayQuestionsCompleted = function () {
        return element(by.xpath("//p[contains(.,'preferences complete.')]"));
    };


    this.questionTexts = function () {
        return element(by.css(".question-text"));
    };
    this.questionSqure = function () {
        return element(by.css(".preferences-question>div:nth-child(1)"));
    };


    this.answerOptions = function () {
        return element.all(by.css("[translate*='ANSWER']"));
    };

    this.answerYesNo = function () {
        return element.all(by.css("[translate^='MYRYANAIR.ACCOUNT.SETTINGS.DEACTIVATE_ACCOUNT.DEACTIVATE_ACCOUNT_DIALOG']"));//[ng-click^='vm.changeAnswer']>span
    };

    this.answerStars = function () {
        return element.all(by.css(".core-btn.answer-star"));
    };

    this.answer = function () {
        return element.all(by.css("[translate*='MYRYANAIR.PREFERENCES.QUESTIONS.ANSWER']"));
    };


    this.questionType = function () {
        return element(by.css(".preferences-question>div[question-type]"));
    };

    this.saveAnswer = function () {
        return element(by.css("[translate='MYRYANAIR.ACCOUNT.DASHBOARD.SAVE']"));
    };

    this.answeredAllQuestionsMessage = function () {
        return element(by.css(".completed[translate='MYRYANAIR.ACCOUNT.DASHBOARD.MESSAGE1']"));
    };

    this.btnSaveContinue = function () {
        return element(by.css(".core-btn-primary.core-btn-phone-full"));
    };

    this.textMultiple = function () {
        return element(by.css(".multi-questions"));
    };

    this.changePreferenceLinks = function () {
        return element.all(by.css(".core-btn-link.action[translate='MYRYANAIR.ACCOUNT.PREFERENCE.CHANGE']"));
    };

    this.rewardSectionCompletedPreference = function () {
        return element(by.css(".rewards-text-wraper[translate='MYRYANAIR.ACCOUNT.PREFERENCE.COMPLETED']"));
    }

    this.rewardTabRewardMessage = function () {
        return element(by.css("div[translate='MYRYANAIR.ACCOUNT.REWARDS.VOUCHERS.NO_VAUCHERS_DESCRIPTION']"));
    }

    //4 cards in dashboard tab
    this.profileCard = function () {
        return element(by.css(".profile-card"));
    }
    this.questionCard = function () {
        return element(by.css(".question-card"));
    }
    this.progressCard = function () {
        return element(by.css(".progress-card"));
    }
    this.rewardsCard = function () {
        return element(by.css(".rewards-card"));
    }

    this.tickIconKeyDetails = function () {
        return element(by.css(":not(.ng-hide)[icon-id*='tick']~span[translate*='KEEP_DETAILS']"));
    }
    this.crossIconKeyDetails = function () {
        return element(by.css(":not(.ng-hide)[icon-id*='close']~span[translate*='KEEP_DETAILS']"));
    }

    this.tickIconPhoneNumber = function () {
        return element(by.css(":not(.ng-hide)[icon-id*='tick']~span[translate*='PHONE_NUMBER']"));
    }
    this.crossIconPhoneNumber = function () {
        return element(by.css(":not(.ng-hide)[icon-id*='close']~span[translate*='PHONE_NUMBER']"));
    }

    this.tickIconTravelDocument = function () {
        return element(by.css(":not(.ng-hide)[icon-id*='tick']~span[translate*='TRAVEL_DOCUMENTS']"));
    }
    this.crossIconTravelDocument = function () {
        return element(by.css(":not(.ng-hide)[icon-id*='close']~span[translate*='TRAVEL_DOCUMENTS']"));
    }

    this.keepBuildMyProfile = function () {
        return element(by.css(".core-btn-primary[translate*='KEEP_BUILDING_MY_PROFILE']"));
    }

    this.dashboardTabPercentage = function (percentage) {
        return element(by.css("div[style*='" + percentage + "']>div.percentage"));
    };

    this.dashboardTabAddTravelDoc = function () {
        return element(by.css("[translate='MYRYANAIR.ACCOUNT.DASHBOARD.PROFILE_PROGRESS.PROFILE_PROGRESS_BASIC.ADD_TRAVEL_DOCUMENT']"));
    };

    this.profileTabAddDoc = function () {
        return element(by.css(".core-btn-link-n.icon-l"));
    };

    this.profileDropDownNationalityDoc = function (option) {
        return element.all(by.css("select[id^=nationality]>option")).get(option);
    };

    this.profileDropDownDocumentType = function (option) {
        return element.all(by.css("select[id^='type']>option")).get(option);
    };

    this.profileDocumentNumber = function () {
        return element(by.css("input[id^='number']"));
    };

    this.profileDropDownDocumentExpiryDay = function (index) {
        return element.all(by.css("[name='day']>option")).get(index);
    };

    this.profileDocumentExpiryMonth = function (index) {
        return element.all(by.css("[name='month']>option")).get(index);
    };

    this.profileDocumentExpiryYear = function (index) {
        return element.all(by.css("[name='year']>option")).get(index);
    };

    this.profileDocumentCountryOfIssue = function (index) {
        return element.all(by.css("select[id^='country']>option")).get(index);
    };

    this.profileBtnAdd = function () {
        return element(by.css("[translate='MYRYANAIR.ACCOUNT.PASSENGERS.DOCUMENTS.DOCUMENT_ADD.ADD']"));
    };




};

module.exports = MyRyanairPage;

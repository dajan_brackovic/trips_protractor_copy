/**
 * Created by nnikic on 7/14/2015.
 */
var AddPassengerNamePage = function(){

    this.continueButtonToAddPass = function(option, index) {
        return element(By.xpath('//*[@id="booking-selection"]/article/div[2]/section/div[2]/button'));
    };

    this.selectTitleDropDownOption = function() {
        return element(By.model('pax.name.title'));
    };
    this.selectMr = function(){
       // return element(By.css('#passengers > div.details-holder > form > div:nth-child(3) > div > div > ng-form > div.form-input.select.title-field > label > div > select > option:nth-child(2)'));
        return element.all(By.css('#passengers > div.details-holder > form > div:nth-child(4) > div > div > ng-form > div.form-input.select.title-field > label > div > select > option:nth-child(2)'));
    };

    this.textFieldFirstName = function(index) {
        return element(By.model('pax.name.first'));
        // return element(by.css("#passengers > div > div > div.content.ng-isolate-scope > div > div.pax-panel.expanded > div > form > div > ng-form > div.form-input.text > label > input["+ index +"]"));
    };

    this.textFieldLastName = function() {
        return element(By.model('pax.name.last'));
        //return element(by.css("#passengers > div > div > div.content.ng-isolate-scope > div > div.pax-panel.expanded > div > form > div > ng-form > span > label > input"));
    };

    this.btnAddPassNameSave = function() {
        return element(by.css('[ng-click="addPassengersForm.$valid && vm.savePassengers()"]'));
    };
    this.continueButtonToAddPass = function(option, index) {
        return element(By.xpath('//*[@id="booking-selection"]/article/div[2]/section/div[2]/button'));
    };
};

module.exports = AddPassengerNamePage;

var CarHirePage = function () {


    this.fieldFirstName = function () {
        return element(by.model("formData.firstname"));
    };

    this.fieldSurname = function () {
        return element(by.model("formData.surname"));
    };

    this.checkboxExtraLiability = function () {
        return element(by.css("div.ct-checkbox.ng-valid label"));
    };

    this.btnConfirmCarHire = function () {
        return element(by.css("div.action-section button.core-btn-primary"));
    };

    this.btnCancelCarHire = function () {
        return element(by.css("div.action-section button.core-link"));
    };

    this.totalPriceCarHire = function (){
        return element(by.css("div.price span.amount"));
    };

    this.xCarHireSideDrawer = function () {
        return element(by.css("core-icon.dialog-close.pull-right"));
    };

    this.btnRemoveCarHire = function () {
        return element(by.css("[ng-click='vm.removeCarRentalSelection()']"));
    };


    this.ryanairLogoOnCarHirePage = function () {
        return element(by.css(".logo"));
    };
};

module.exports = CarHirePage;
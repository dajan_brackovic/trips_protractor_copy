var ManageYourTripPage = function(){


    // Change your Flight
    this.changeYourFlightLink = function() {
        return element(by.css("span.item-content [translate='mytrips.manage_your_trip.side_drawer.manage.change_flight']"));
    };

    this.changeFlightsSearchButton = function () {
        return element(by.css("button.primary.ng-scope"));
    };

    this.changeFlightOutBoundCheckBox = function () {
        return element.all(by.css("div.checkbox")).get(0);
    };

    this.changeFlightInBoundCheckBox = function () {
        return element.all(by.css("div.checkbox")).get(1);
    };

    // Change Name
    this.changeNameLink = function () {
        return element(by.css("span.item-content [translate='mytrips.manage_your_trip.side_drawer.manage.change_name']"));
    };

    this.selectNameToChangeCheckBox = function () {
        return element(by.css("div.big-checkbox"));
    };

    this.changeNameTitleDropdown = function (index) {
        return element(by.css("select[id^='title-'] option:nth-child("+index+")"));
    };

    this.changeNameFirstNameField = function () {
        return element(by.css("input[id^='firstname-']"));
    };

    this.changeNameLastNameField = function () {
        return element(by.css("input[id^='lastname-']"));
    };

    this.changeNameContinueBtn = function () {
        return element(by.css("div.manage-trips [translate='trips.side.confirm']"));
    };

    this.changeNameTotal = function () {
        return element(by.css("span.amount.ng-scope strong"));
    };


};
module.exports = ManageYourTripPage;
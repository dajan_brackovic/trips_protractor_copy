var SeatsPage = function () {


    this.a1Seat = function () {
        return element(by.css("div:nth-child(11) > div:nth-child(1)"));
    };

    this.reservedSeats = function () {
        return element(by.css("core-icon[icon-id='icons.glyphs.seat-map-reserved']"));
    };

    this.vacantSeats = function () {
        return element.all(by.css("div.seat.ng-scope.standard:not(.reserved)"));
    };

    this.listVacantAdultSeats = function (index) {
        return element.all(by.css("div.seat.ng-scope.standard:not(.reserved):not(.infant)")).get(index);
    };

    this.vacantAdultSeats = function () {
        return element.all(by.css("div.seat.ng-scope.standard:not(.reserved):not(.infant)"));
    };

    this.listVacantInfantSeats = function (index) {
        return element.all(by.css("div.seat.ng-scope.standard.infant:not(.reserved)")).get(index);
    };

    this.vacantInfantSeats = function () {
        return element.all(by.css("div.seat.ng-scope.standard.infant:not(.reserved)"));
    };

    // Standard Seats
    this.anyAvailableSeat = function () {
        return element.all(by.css("div.seat.ng-scope.standard:not(.reserved):not(.infant),.div.seat.ng-scope.standard")).get(0);
    };

    this.listAnyAvailableSeat = function (index) {
        return element.all(by.css("div.seat.ng-scope.standard:not(.reserved):not(.infant),.div.seat.ng-scope.standard")).get(index);
    };

    this.anyAvailableInfantSeat = function () {
        return element.all(by.css("div.seat.ng-scope.standard.infant:not(.reserved),.div.seat.ng-scope.standard")).get(0);
    };

    this.listAnyAvailableInfantSeat = function (index) {
        return element.all(by.css("div.seat.ng-scope.standard.infant:not(.reserved),.div.seat.ng-scope.standard")).get(index);
    };

    //Premium Seats
    this.anyAvailablePremiumSeatXL = function () {
        return element.all(by.css("div.seat.ng-scope.premium.emergency-exit:not(.reserved)")).get(0);
    };

    this.anyAvailablePremiumSeatExceptXLAndInfant = function () {
        return element.all(by.css("div.seat.ng-scope.premium:not(.reserved):not(.emergency-exit):not(.infant)")).get(0);
    };

    this.anyAvailablePremiumSeat = function () {
        return element.all(by.css("div.seat.ng-scope.premium:not(.reserved)")).get(0);
    };

    this.anyAvailableInfantPremiumSeat = function () {
        return element.all(by.css("div.seat.ng-scope.premium.infant:not(.reserved)")).get(0);
    };

    // Return Seat
    this.sameSeatReturn = function () {
        return element(by.css("button[translate='trips.seats.same_seats_yes']"));
    };

    this.diffSeatReturn = function () {
        return element(by.css("button[translate='trips.seats.same_seats_no']"));
    };

    // Seats Selections
    this.seatOut = function () {
        return element(by.css("td.out > span.ng-binding"));
    };

    this.seatOutList = function (index) {
        return element.all(by.css("td.out > span.ng-binding")).get(index);
    };

    this.seatBack = function () {
        return element(by.css("td.back > span.ng-binding"));
    };

    this.seatBackList = function (index) {
        return element.all(by.css("td.back > span.ng-binding")).get(index);
    };

    this.btnNext = function () {
        return element.all(by.css("button.core-btn-primary[translate='trips.seats.next']")).get(1);
    };

    this.btnNextCheckIn = function () {
        return element(by.css("button.core-btn-primary.ng-scope [translate='trips.seats.next']"));
    };

    this.btnCancel = function () {
        return element(by.css("div.action-section button[translate='trips.seats.btn_cancel']"));
    };

    this.btnConfirm = function () {
        return element.all(by.css("button.core-btn-primary[translate='trips.seats.confirm']")).get(1);
    };

    this.btnConfirmCheckIn = function () {
        return element(by.css("button.core-btn-primary [translate='trips.seats.confirm']"));
    };

    this.seatsTotalVal = function () {
        //return element(by.css("span.amount> strong"));
        return element(by.css("div.price span.amount strong"));
    };

    // Seat Map
    this.seatMap = function () {
        return element(by.css("div.seat-modal"));
    };


    // Priority Boarding

    this.btnPriorityBoarding = function () {
        return element(by.css("button.add-priority"));
    };

    this.btnRemovePriorityBoarding = function () {
        return element(by.css("button.remove-priority"));
    };


};

module.exports = SeatsPage;
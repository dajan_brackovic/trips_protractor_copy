var SamsonitePage = function () {


    this.titleTermsAndConditions = function () {
        return element(by.css("h1"));
    };
    this.linkTermsAndConditions = function () {
        return element.all(by.css('[target="_blank"]')).get(1);
    };
    this.bagType = function (index) {
        return element.all(by.css('[class="bag-type"]')).get(index);
    };
    this.ryanairLogo = function(){
        return element(by.css('[class="ryanair-logo"]'));
    };
    this.popUp = function(){
        return element(by.css('[class="choose"]'));
    };
    this.bagImage = function(index){
        return element.all(by.css('[class="bag-image"]')).get(index);
    };
    this.samsoniteFAQAnswer1 = function(){
        return element(by.id('answer-1'));
    };
    this.samsoniteFAQAnswer2 = function(){
        return element(by.id('answer-2'));
    };
    this.samsoniteFAQAnswer3 = function(){
        return element(by.id('answer-3'));
    };
    this.fullscreenBagImage = function(){
        return element(by.css('[class="enlarge-samsonite modal-dialog mobile-fullscreen"]'));
    };
    this.bagHolder = function(index){
        return element.all(by.css('[class="single-bag-holder"]')).get(index);
    };
    this.clickAddSamsoniteBags = function (index) {
        return element.all(by.css('[ng-click="increment()"]')).get(index);
    };
    this.clickRemoveSamsoniteBags = function (index) {
        return element.all(by.css('[ng-click="decrement()"]')).get(index);
    };
    this.clickCheckOutSamsonite = function () {
        return element(by.css("[ng-click='continueToPayment()']"));
    };
    this.subtotalSamsonite = function () {
        return element(by.css("body > div.FR > main > div > div:nth-child(5) > p"));
    };
    this.expandQuiestions = function (index) {
        return element.all(by.css('[class="icon-expand"]')).get(index);
    };
    this.amountOfSamsonite = function (index) {
        return element.all(by.css('.value-display')).get(index);
    };
    this.largerImage = function (index) {
        return element.all(by.css('[ng-dialog="sam_image_expand"]')).get(index);
    };
    this.priceByBag = function (index) {
        return element.all(by.css('[class="price"]')).get(index);
    };
    this.samsonitePaymentFirstName = function() {
        return element(by.model("cForm.contact.firstName"));
    };
    this.samsonitePaymentLastName = function() {
        return element(by.model("cForm.contact.lastName"));
    };
    this.samsonitePaymentEmail= function(){
        return element(by.model("cForm.contact.emailAddress"));
    };
    this.samsonitePaymentPhoneCodeDropdown = function(){
        return element(by.model("ctrl.selectedCountry"));
    };
    this.samsonitePaymentPhoneNumber= function(){
        return element(by.model("ctrl.model.number"));
    };
    this.samsonitePaymentAdressLine1 = function(){
        return element(by.id('deliveryAddressAddressLine1'));
    };
    this.samsonitePaymentAdressLine2 = function(){
        return element(by.id('deliveryAddressAddressLine2'));
    };
    this.samsonitePaymentCity = function(){
        return element(by.model("ctrl.model.city"));
    };
    this.samsonitePaymentPostcode= function(){
        return element(by.model("ctrl.model.postcode"));
    };
    this.samsonitePaymentCountryDropdown= function(){
        return element(by.id('deliveryAddressCountry'));
    };

    // payment
    this.btnCreditCardSelect = function () {
        return element(by.model("pms.model"));
    };

    this.selectPaymentTypeDropDown = function () {
        return element(by.model("cm.payment.cardType"));
    };

    this.selectPaymentTypeDropDownOption = function (option) {
        return element(By.xpath('//option[text() = \'' + option + '\']'));
    };

    this.textFieldCardNumber = function () {
        return element(by.model("cm.payment.cardNumber"));
    };

    this.selectExpiryMonthDropDownOption = function (option) {
        return element(By.css("select.expiry-month-select  option:nth-child(" + option + ")"));
    };

    this.selectExpiryYearDropDown = function () {
        return element(by.css("select.expiry-year-select option:nth-child(2)"))
    };

    this.selectExpiryYearDropDownOption = function (option) {
        return element(By.xpath('//option[text() = \'' + option + '\']'));
    };

    this.textFieldCVV = function () {
        return element(by.model("cm.payment.securityCode"));
    };

    this.textFieldCardHoldersName = function () {
        return element(by.model("cm.payment.cardHolderName"));
    };

    this.textFieldCity = function () {
        return element(by.id('billingAddressCity'));
    };

    this.textFieldPostCode = function () {
        return element(by.id('billingAddressPostcode'));
    };

    this.textFieldStreetName = function () {
        return element(by.id('billingAddressAddressLine1'));
    };

    this.btnPaymentContinue = function () {
        return element(by.css("[translate='common.components.payment_forms.pay_now']"));
    };

    this.paymentTermsAndConditions = function () {
        return element(by.css("[id^='acceptTerms']"));
    };

    //successful order
    this.orderSuccessful = function () {
        return element(by.css("div.message-success-title"));
    };

};

module.exports = SamsonitePage;
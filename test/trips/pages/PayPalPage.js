var PayPalPage = function () {


    this.payWithMyPayPalAccountLink = function () {
        return element(by.css("#loadLogin"));
    };

    this.fieldEmail = function () {
        return element(by.id("email"));
    };

    this.fieldPassword = function () {
        return element(by.id("password"));
    };

    this.btnLogIn = function () {
        return element(by.css("#btnLogin"));
    };

    this.btnContinuePayPal = function () {
        return element(by.id("confirmButtonTop"));
    };

};

module.exports = PayPalPage;
var TripsSummaryPage = function () {


    this.labelBookingMessage = function () {
        return element(by.css("confirmation-banner div p"));
    };


    this.labelBookingRef = function () {
        return element(by.css("confirmation-banner div p strong"));
    };

    this.viewYourTripItinerary = function () {
        return element(by.css("confirmation-banner div p a"));
    };

    this.manageYourTripItinerary = function () {
        return element(by.css("button.trip-btn[ui-sref='booking.active.manage-trip']"));
    };

    this.manageYourTripButton = function () {
        return element(by.css("div[ui-sref='booking.active.manage-trip.home']"));
    };

    this.checkInButton = function () {
      //  return element(by.css("button [translate='trips.active.card.checkin_button']"));
        return element(by.css("button[translate='trips.active.card.checkin_button']"));
    };

    this.ryanairLogo = function () {
        return element(by.css("a.ryanair-logo"));
    };

    this.addedBagsOnActiveTrip = function () {
        return element(by.css("[translate='trips.active.travel.purchased.bags']"));
    };

    this.labelHoldFareRef = function () {
        return element(by.css("confirmation-banner div p span:nth-child(2)"));
    };

    this.checkOutButton = function () {
        return element(by.css("button.checkout-button"));
    };

    this.labelBookingNoCarHire = function () {
        return element(by.css("li.product-area-one-third-full-height.ct-floatr span span"));
    };

    this.labelBookingNoCarHireModify = function () {
        return element(by.css("li.product-area-one-third-full-height span span"));
    };

    this.topContinueBtn = function () {
        return element(by.css("button.core-btn-primary.core-btn-block.ng-scope"));
    };

    this.checkInClosedHeadline = function () {
        return element(by.css("[translate='trips.active.checkin.check-in-on']"));
    };

    this.checkInClosedSubline = function () {
        return element(by.css("[translate='trips.active.checkin.open30']"));
    };

    this.checkInUnavailable = function () {
        return element(by.css("[translate='trips.active.checkin-card.unavailable']"));
    };

    this.bookingRefInfoBox = function () {
        return element(by.css("[translate='trips.active.countdown.pnr.desktop'] span"));
    };

    this.infoMessageNumberOfBagsAddedOnBagsCard = function () {
        return element(by.css("[translate='trips.active.travel.purchased.bags']"));
    };

    this.activeTripMainImage = function () {
        return element(by.css(".active-trip-animation"))
    };
    this.activeTripRoute = function () {
        return element(by.css(".active-trip-card-content-header"))
    };
    this.activeTripDate = function () {
        return element(by.css(".active-trip-card-content-date"))
    };



};

module.exports = TripsSummaryPage;
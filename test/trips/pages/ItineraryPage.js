var ItineraryPage = function () {


    this.reservationNumber = function () {
        return element(by.css('.reservation-number strong.ng-binding'));
    };

    this.flightStatus = function () {
        return element(by.css('.flight-status strong'));
    };

    //Flight Information
    this.flightInformationOutboundFlight = function () {
        return element.all(by.css(".destinations")).get(0);
    };

    this.flightInformationInboundFlight = function () {
        return element.all(by.css(".destinations")).get(1);
    };

    this.flightInformationOutboundFlightDay = function () {
        return element.all(by.css("li.flight div:not(.flight-description):not(.plane-icon):not(.destinations):not([bindonce='iconIsReady']) span.ng-binding")).get(0);
    };

    this.flightInformationOutboundFlightTime = function () {
        return element.all(by.css("li.flight div:not(.flight-description):not(.plane-icon):not(.destinations):not([bindonce='iconIsReady']) span.ng-binding")).get(1);
    };

    this.flightInformationOutboundFlightNumber = function () {
        return element.all(by.css("li.flight div:not(.flight-description):not(.plane-icon):not(.destinations):not([bindonce='iconIsReady']) span.ng-binding")).get(2);
    };

    this.flightInformationInboundFlightDay = function () {
        return element.all(by.css("li.return-flight div:not(.flight-description):not(.plane-icon):not(.destinations):not([bindonce='iconIsReady']) span.ng-binding")).get(0);
    };

    this.flightInformationInboundFlightTime = function () {
        return element.all(by.css("li.return-flight div:not(.flight-description):not(.plane-icon):not(.destinations):not([bindonce='iconIsReady']) span.ng-binding")).get(1);
    };

    this.flightInformationInboundFlightNumber = function () {
        return element.all(by.css("li.return-flight div:not(.flight-description):not(.plane-icon):not(.destinations):not([bindonce='iconIsReady']) span.ng-binding")).get(2);
    };

    //Passengers
    this.passengersNames = function (index) {
        return element.all(by.css("li.ng-scope span[translate='trips.itinerary.passenger-info.display-name']:not(.passenger-name)")).get(index); // index 0 is first passenger
    };

    //Flight Products
    this.numberOfBagsSelectedOutbound = function (index) {
        return element(by.css("div.part-left div.passenger-seat .gray-text.ng-binding"));
    };

    this.numberOfBagsSelectedInbound = function (index) {
        return element(by.css("div.part-right div.passenger-seat .gray-text.ng-binding"));
    };

    this.seatNumberOutbound = function (index) {
        return element(by.css("div.part-left div.passenger-seat .ng-binding:not(.gray-text)"));
    };

    this.seatNumberInbound = function (index) {
        return element(by.css("div.part-right div.passenger-seat .ng-binding:not(.gray-text)"));
    };

    //Price Breakdown
    this.priceBreakdownOutboundFlight = function () {
        return element.all(by.css('div.trip-item h6.item-label')).get(0);
    };

    this.priceBreakdownOutboundFlightDetails = function () {
        return element.all(by.css('div.trip-item small')).get(0);
    };

    this.priceBreakdownInboundFlight = function () {
        return element.all(by.css('div.trip-item h6.item-label')).get(1);
    };

    this.priceBreakdownInboundFlightDetails = function () {
        return element.all(by.css('div.trip-item small')).get(1);
    };

    // Product Summary

    this.assignedSeat = function () {
        return element(by.css("[translate='trips.itinerary.products.SEAT.desc']"));
    };

    this.normalBag = function () {
        return element(by.css("[translate='trips.itinerary.products.BAG.desc']"));
    };

    this.largeBag = function () {
        return element(by.css("[translate='trips.itinerary.products.BBG.desc']"));
    };

    this.babyEquipment = function () {
        return element(by.css("[translate='trips.itinerary.products.BABY.desc']"));
    };

    this.sportsEquipment = function () {
        return element(by.css("[translate='trips.itinerary.products.BIKE.desc']"));
    };

    this.musicEquipment = function () {
        return element(by.css("[translate='trips.itinerary.products.MUSC.desc']"));
    };

    this.parking = function () {
        return element(by.css("[translate='trips.itinerary.products.PARKING.desc']"));
    };

    this.addedExtrasItemsList = function () {
        return element.all(by.css("[ng-repeat='product in breakdown.products'] .item-label"));
    };

};

module.exports = ItineraryPage;
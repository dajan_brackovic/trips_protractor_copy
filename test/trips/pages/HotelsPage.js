var HotelsPage = function () {


    this.hotelsIFrame = function () {
        return element(by.css("iframe[id='booking_inpath']"));
    };

    this.hotelName = function () {
        return element.all(by.css("h1.hotel-header--hotel-name"));
    };

    this.numberHotelInList = function (index) {
        return element.all(by.css("a.button.hotel-card--view-rooms")).get(index);
    };

    this.numberPlusButtonInList = function (index) {
        return element.all(by.css("div.room-selector--button.room-selector--button-plus")).get(index);
    };

    this.roomPrice = function () {
        return element.all(by.css("div.price--price")).get(0);
    };

    this.confirmButton = function () {
        return element.all(by.css("div.action-section button.core-btn-primary"));
    };

    this.btnCancelHotel = function () {
        return element(by.css("[translate='trips.side.cancel']"));
    };

    this.priceAmount = function () {
        return element.all(by.css("div.price span.amount")).get(1);
    };

    this.openCalendarCheckIn = function () {
        return element(by.id("checkin_date"));
    };

    this.selectDay = function (index) {
       return element.all(by.css("div.pika-lendar td:not(.is-disabled):not(.is-empty):not(.is-today)")).get(index);
    };

    this.searchButton = function () {
        return element(By.css("button.searchbox--button"));
    };

    this.roomNightsInfo = function () {
        return element(By.css("div.hotel-card--price-title"));
    };

    // Hotel active trip elements
    this.addHotelActive = function () {
        return element(by.css("a > div.hotelImage"));
    };

    this.ryanairLogo = function () {
        return element(by.css("div.logo"));
    };

    this.partnershipLogo = function () {
        return element(by.css("#powered-by-wrapper"));
    };

};

module.exports = HotelsPage;

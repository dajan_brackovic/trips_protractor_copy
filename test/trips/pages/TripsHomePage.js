var TripsHomePage = function () {

    this.gotoHome = function () {
        browser.get('/#/booking/home');
    };

    this.dateFlightSelection = function (index) {
        return element(by.xpath("(//*[@class='date ng-binding'])[" + index + "]"));
    };

    this.fareSelection = function (index) {
        return element(by.xpath("(//span[@translate='trips.flight_list_table.table_heading.standard-fare'])[" + index + "]"));
    };
    //selected/highlighted option when bring over from foh timetable
    this.selectedDepartureTime = function () {
        return element(by.css("div.starting-point > span.time"));
    };

    this.selectedArrivalTime = function () {
        return element(by.css("div.destination > span.time"));
    };

    this.selectedPriceInCarousel = function () {
        return element.all(by.css("div.selected > span.price"));
    };

    this.selectedTripSummary = function () {
        return element.all(by.css("div.trip-summary-info>h1>translate>span"));
    };

    this.oneWayTripTypeText = function () {
        return element.all(by.css("span[translate='trips.one_way']"));
    };

    this.selectedDate = function () {
        return element(by.css("div.active > div > div.date"));
    };

    this.selectedPriceInTable = function () {
        return element(by.css("div.active > div > div.fare"));
    };

    this.tableFlights = function () {
        return element.all(by.css("#outbound > div > div.flight-list-wrapper"));
    };

    this.tableRowFlightDetails = function () {
        return element(by.css("#outbound > div > div.flight-list-wrapper > div.flights-table-wrapper > div > div.card.centered-card.flights.desktop > div:nth-child(1)"));
    };

    this.fareType = function (rowIndex) {
        return this.tableRowFlightDetails(rowIndex).element(by.xpath("//*[@translate='trips.flight_list_table.table_heading.standard-fare']"));
    };

    this.btnHomeBottomContinue = function () {
        return element(by.id("continue"));
    };

    // Selected Flight Card info box

    this.flightInfoBox = function () {
        return element.all(by.css(".flight"));
    };

    this.outboundFlightCardInfoBoxTime = function () {
        return element(by.css("[class=flight] .flight-date"));
    };

    this.inboundFlightCardInfoBoxTime = function () {
        return element(by.css(".flight-right .flight-date"));
    };

    this.outboundFlightCardInfoBoxFlightNumber = function () {
        return element(by.css("[class=flight] .flight-number"));
    };

    this.inboundFlightCardInfoBoxFlightNumber = function () {
        return element(by.css(".flight-right .flight-number"));
    };

    //Select NoFlight available on carousel

    this.selectANoFlightCarousel = function () {
        return element.all(by.css(".item-not-available")).get(1);
    };

    this.assertNoFlightsMessage = function () {
        return element(by.css("[translate='trips.notifications.messages.no_flight_message']"));
    };

    // availabilityCard
    this.cardFlightNumber = function (destination, cardIndex) {
        return element.all(by.css("#" + destination + " .starting-point .highlighted")).get(cardIndex);
    }

    this.cardFlightNumberSelected = function (destination) {
        return element.all(by.css("#" + destination + " .flight.selected .starting-point .highlighted"));
    }

    this.cardOrigin = function (destination, cardIndex) {
        return element.all(by.css("#" + destination + " .starting-point .town")).get(cardIndex);
    }

    this.cardDepartureTime = function (destination, cardIndex) {
        return element.all(by.css("#" + destination + " .starting-point .time")).get(cardIndex);
    }
    this.cardDepartureTimeSelected = function (destination) {
        return element(by.css("#" + destination + " .flight.selected .starting-point .time"));
    }
    this.cardArrivalTimeSelected = function (destination) {
        return element(by.css("#" + destination + " .flight.selected .destination .time"));
    }

    this.cardFlightTime = function (destination, cardIndex) {
        return element.all(by.css("#" + destination + " .destination .highlighted")).get(cardIndex);
    }

    this.cardDestination = function (destination, cardIndex) {
        return element.all(by.css("#" + destination + " .destination .town")).get(cardIndex);
    }

    this.cardArrivalTime = function (destination, cardIndex) {
        return element.all(by.css("#" + destination + " .destination .time")).get(cardIndex);
    }

    this.cardStandardFareCard = function (destination, cardIndex) {
        return element.all(by.css("#" + destination + " .standard-fare")).get(cardIndex);
    }

    this.cardBusinessFareCard = function (destination, cardIndex) {
        return element.all(by.css("#" + destination + " .business-fare")).get(cardIndex);
    }

    this.cardPriceStandartFare = function (destination, cardIndex) {
        browser.sleep(2000);
        return element.all(by.css("#" + destination + " .flight:not(.disabled) .price:not(.ng-hide)")).get(cardIndex);
    }

    this.getSelectedFarePrice = function (destination) {
        return element(by.css("#" + destination + " .fare.selected .price")).getText();
    }

    this.cardPriceBusinessFare = function (destination, cardIndex) {
        browser.sleep(2000);
        return element.all(by.css("#" + destination + " .flight:not(.disabled) .price[ng-hide='!flight.businessFare']")).get(cardIndex);

    }

    // outbound selected flight card
    this.outboundSelectionCardOrigin = function () {
        return element(by.xpath("(//*[@id='booking-selection']//*[@class='location ng-binding'])[" + 1 + "]"));

    }

    this.outboundSelectionCardDestination = function () {
        return element(by.xpath("(//*[@id='booking-selection']//*[@class='location ng-binding'])[" + 2 + "]"));
    }

    this.outboundSelectionCardFlightTimes = function () {
        return element(by.xpath("(//*[@id=\"booking-selection\"]/div/div/div[1]/div/div/span[3])"));
    }

    this.outboundSelectionCardFlightNumber = function () {
        return element(by.xpath("(//*[@id=\"booking-selection\"]/div/div/div[1]/div/div/span[4])"));
    }

    // inbound selected flight card
    this.inboundSelectionCardOrigin = function () {
        return element(by.xpath('(//*[@id="booking-selection"]/div/div/div[2]/div/div/span[1])'));
    }

    this.inboundSelectionCardDestination = function () {
        return element(by.xpath('(//*[@id="booking-selection"]/div/div/div[2]/div/div/span[2])'));
    }

    this.inboundSelectionCardFlightTimes = function () {
        return element(by.xpath('(//*[@id="booking-selection"]/div/div/div[2]/div/div/span[3])'));
    }

    this.inboundSelectionCardFlightNumber = function () {
        return element(by.xpath('(//*[@id="booking-selection"]/div/div/div[2]/div/div/span[4])'));
    }

    // total fare amount
    this.fareTotalPrice = function () {
        return element(by.xpath('(//*[@id="booking-selection"]/article/div[2]/section/div[1]/span/span/div/div[2])'))
    }

    //Modify trip

    this.btnModifyTrip = function () {
        return element(by.css("[translate='trips.summary.heading.links.modify_search']"));
    };

    this.flyFrom = function () {
        return element(by.css("div.flight-title translate span[class*='starting']"));
    }

    this.flyTo = function () {
        return element(by.css("div.flight-title translate span[class*='destination']"));
    }

    this.returnFlightDiv = function () {
        return element(by.id("inbound"));
    }


    // Hold Fare

    this.btnHoldFare = function () {
        return element(by.css("div.holdable"));
    }

    this.holdFareDrowerTotal = function () {
        return element(by.css("div.price strong"))
    }




    this.getBusinessPlus = function () {
        return element(by.css("a[ui-sref='booking.home.business-plus-info']"));
    };


    // Save Trip

    this.btnSaveTrip = function () {
        return element(by.css("[icon-id='icons.glyphs.heart'] use"));
    };

    this.coloredSavedTripIcon = function () {
        return element(by.css(".full-heart"));
    };



    this.availableCardDepartureTime = function () {
        return element(by.css("#outbound .flight:not(.error-message):not(.disabled) .flight-holder .time"));
    };


};

module.exports = TripsHomePage;

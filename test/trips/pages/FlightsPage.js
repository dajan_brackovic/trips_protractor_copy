var FlightsPage = function(){

    this.get = function () {
        browser.get('');
    };

	this.returnTrip = element(by.css("div.flight-search-type-option.return > label > span.rad > span"));
	this.oneWayTrip = element(by.css("div.flight-search-type-option.one-way > label > span.rad > span"));
	this.fromInput = element(by.model("$parent.value"));
	this.fromStateIreland = element.all(by.repeater('option in options')).get(13);
	this.fromStateItaly = element.all(by.repeater('option in options')).get(15);
	this.fromTownDublin = element.all(by.repeater('option in choices')).get(2);
    this.fromTownRomeC = element.all(by.repeater('option in choices')).get(18);
	this.toStateItaly = element(by.css("#search-container > div > form > div.col-flight-search-left > div.row-airports.ng-isolate-scope > div > div.col-destination-airport > div.popup-destination-airport.ng-isolate-scope.opened > div > div.content-box.arrow_box > div.content.ng-scope > div > div.pane.left > div > div:nth-child(12)"));
    this.toStateIreland = element(by.css(".//*[@id='search-container']/div/form/div[1]/div[2]/div/div[2]/div[3]/div/div[2]/div[2]/div/div[1]/div/div[9]"));
    this.toTownDublin = element(by.css("#search-container > div > form > div.col-flight-search-left > div.row-airports.ng-isolate-scope > div > div.col-destination-airport > div.popup-destination-airport.ng-isolate-scope.opened > div > div.content-box.arrow_box > div.content.ng-scope > div > div.pane.right > div > div.option.ng-binding.ng-scope"));

    this.toTownRomeC = element(by.css("#search-container > div > form > div.col-flight-search-left > div.row-airports.ng-isolate-scope > div > div.col-destination-airport > div.popup-destination-airport.ng-isolate-scope.opened > div > div.content-box.arrow_box > div.content.ng-scope > div > div.pane.right > div > div:nth-child(10)"));
	this.firstContinueButton = element(by.css('[class="btn-smart-search-go"]'));
	this.fromStateUK = element.all(by.repeater('option in options')).get(27);
	this.fromTownLondonL = element.all(by.repeater('option in choices')).get(11);
	this.toStateIreland = element(by.css("div.col-destination-airport > div.popup-destination-airport.ng-isolate-scope.opened > div > div.content-box.arrow_box > div.content.ng-scope > div > div.pane.left > div > div:nth-child(4)"));
	this.toTownDublin = element(by.css("div.col-destination-airport > div.popup-destination-airport.ng-isolate-scope.opened > div > div.content-box.arrow_box > div.content.ng-scope > div > div.pane.right > div > div:nth-child(3)"));
	this.dropDownPassengers = element(by.css('[ng-click="toggleDropdown()"]'));
	this.addPassenger = element(by.css('[ng-click="increment()"]'));
	this.closePopUp =element(by.css('[class="dropdown-handle ng-scope opened"]'));
	//this.closePopUp = element(by.css('[class="scrim"]'));

    this.addPassengerType = function(i){
         return element.all(by.css('[ng-click="increment()"]')).get(i);
 }
    this.closeInfantPopUp = element(by.css('[class="btn btn-primary-yellow"]'));
	this.flyOut = element(by.model('vm.startDate'));
	this.flyOutDate = element(by.xpath('//*[@id="row-dates-pax"]/div[2]/div/div[1]/div/div[3]/div/div[2]/div[2]/core-datepicker/div[1]/ul/li[2]/ul[2]/li[9]/span'));
	this.flyBack = element(by.model("vm.endDate"));
	this.flyBackDate = element(by.xpath('//*[@id="row-dates-pax"]/div[2]/div/div[2]/div/div[3]/div/div[2]/div[2]/core-datepicker/div[1]/ul/li[2]/ul[2]/li[10]/span'));
	this.letsGoButton = element(by.buttonText("Let's Go"));
	this.standardFareButton = element(by.css('#outbound > div > div.flight-list-wrapper > div.flights-table-wrapper > div > div.card.centered-card.flights.desktop > div:nth-child(1) > div.flight-holder > div.flight-prices > div:nth-child(2) > div'));
	this.standardFareButtonFlyOut = element(by.css('#outbound > div > div.flight-list-wrapper > div.flights-table-wrapper > div > div.card.centered-card.flights.desktop > div:nth-child(1) > div.flight-holder > div.flight-prices > div:nth-child(2) > div'));
	this.standardFareButtonFlyBack = element(by.css('#inbound > div > div.flight-list-wrapper > div.flights-table-wrapper > div > div.card.centered-card.flights.desktop > div:nth-child(1) > div.flight-holder > div.flight-prices > div:nth-child(2) > div > span.price.ng-binding'));
	this.continueButton = element(by.id('continue'));
	this.listOfBenefits = element(by.css('[class="parking-benefits"]'));
	this.allTickets = element.all(by.css('[class="price ng-binding"]'));
	
};
module.exports = FlightsPage;
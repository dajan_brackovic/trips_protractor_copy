var BabyEquipmentPage = function () {

    this.plusButtonSameForBothFlights = function (i) {
        return element.all(by.css('button.core-btn.inc.core-btn-wrap')).get(i);
    };
    this.plusButtonFlightBack = function (i) {
        return element.all(by.css('[ng-click="model.incPrice()"]')).get(i);
    };
    this.plusButtonFlightOut = function (i) {
        return element.all(by.css('[ng-click="model.incPrice()"]')).get(i);
    };
    this.plusButtonMorePassengers = function(i){
        return element.all(by.css("div.single-holder-normal > div > div > button.btn.inc")).get(i);
    };
    this.plusButtonMorePassengersOneWay = function(indexOfPasenger){
        return element.all(by.css('["class="persons-name"]')).get(indexOfPasenger).element(by.css('[class="btn inc"]'));
    };
    this.minusButtonSameForBothFlights = function (i) {
        return element.all(by.css('button.core-btn.dec.core-btn-wrap')).get(i);
    };
    this.minusButtonFlightBack = function (i) {
        return element.all(by.css('[ng-click="vm.flightBackCard.incPrice()"]')).get(i);
    };
    this.minusButtonFlightOut = function (i) {
        return element.all(by.css('[ng-click="vm.flightOutCard.incPrice()"]')).get(i);
    };
    this.amountBabyEquipment = function (k) {
        return element.all(by.css('[class="value-display ng-binding ng-scope set"]')).get(k);
        //return $('[class="amount ng-binding"]');
    };
    this.amountBabyEquipmentSet = function (k) {
        return element.all(by.css('[class="value-display set"]')).get(k);
    };
    this.amountBabyEquipmentZero = function (k) {
        return element.all(by.css('[class="value-display"]')).get(k);
    };
    this.amountBabyEquipmentNN = function (k) {
        return element.all(by.css('[class="value-display ng-binding ng-scope"]')).get(k);
    };
    this.amountBabyEquipmentRight = function (i) {
        return element.all(by.css("div.equipment-holder-double-normal")).get(i);
    };
    this.amountBabyEquipmentLeft = function (i) {
        return element.all(by.css("div.equipment-holder-double-bordered-right")).get(i);
    };
    this.titleBabyEquiment = function () {
        return element(by.css("div.dialog-header > h1 > div > span"));
    };
    this.checkBoxSFBF = function (i) {
        return element.all(by.css('[class="optional-checkboxes"]')).get(i);
    };
    this.checkBoxSFBFSelected = function (i) {
        return element.all(by.css('[class="ng-scope single-holder-normal"]')).get(i);
    };
    this.checkBoxSFBFUnSelected = function (i) {
        return element.all(by.css('[class="ng-scope single-holder-normal ng-hide"]')).get(i);
    };
    this.closeButtonX = function () {
        return element(by.css('[ng-click="closeThisDialog(\'x\')"]'));
    };
    this.cancelButton = function (i) {
        return element.all(by.css('button[translate="trips.side.cancel"]')).get(i);
    };
    this.confirmButton = function () {
        return element(by.buttonText("Confirm"));
    };
    this.confirmButtonClick = function () {
        return element.all(by.css("button[translate='trips.side.confirm']")).get(1);
    };
    this.backButton = function () {
        return element(by.css('[translate="trips.side.back"]'));
    };
    this.babyLogo = function (i) {
        return element.all(by.css('[class="equipment-icon ng-isolate-scope"]')).get(i);
    };
    this.cornerPrice = function () {
        return element.all(by.css('[class="amount ng-scope"]')).last();
    };
    this.centralPrice = function(i){
        return element.all(by.css('[class="price ng-binding"]')).get(i);
    };
    this.centralPriceNN = function(){
        return element.all(by.css('[class="price ng-binding"]')).last();
    };
    this.centralPriceFirst = function(){
        return element.all(by.css('[class="price ng-binding"]'));
    };
    this.centralPriceRight = function(i){
        return element.all(by.css('div.equipment-holder-double-normal div span.price.ng-binding')).get(i);
    };
    this.centralPriceLeft = function(i){
        return element.all(by.css('div.equipment-holder-double-bordered-right div span.price.ng-binding')).get(i);
    };
    this.labelAddBabyEquipment = function () {
        return element(by.css("div.dialog-body div div div.alert.modal-alert-info span"));
    };
    this.labelAddBabyEquipmentIcon = function () {
        return element(by.css("core-icon div svg use"));
    };
    this.minusButtonDisabled = function(i){
        return element.all(by.css('[class="btn dec disabled"]')).get(i);
    };
    this.plusButtonDisabled = function(i){
        return element.all(by.css('[class="btn inc disabled"]')).get(i);
    };
    this.passengerTypeAndNumber = function(i){
        return element.all(by.css('[class="persons-name"]')).get(i);
    };
    this.passengerLogo = function(i){
        return element.all(by.css('[icon-id="icons.glyphs.person"]')).get(i);
    };
    this.labelSameForBothFlights = function(i){
        return element.all(by.css('[class="optional-checkboxes ng-scope"]')).get(i);
    };
    this.outboundText = function(){
        return element(by.css('[translate="trips.extra.flight_back"]'));
    };
    this.inboundText = function(){
        return element(by.css('[translate="trips.extra.flight_out"]'));
    };
    this.informationCardBE = function(){
        return element(by.css('[class="alert modal-alert-info"]'));
    };
    this.passengerCard = function(i){
        return element.all(by.css('[class="equipment"]')).get(i);
    };
    this.toolTipDisablePlusHide = function(){
        return element(by.css('[class="tooltip-callout-content top ng-hide"]'));
    };
    this.toolTipDisablePlusShown = function(){
        return element(by.css("div.tooltip-callout-content.top"));
    };
    this.toolTipDisablePlusText = function(i){
        return element(by.css("a.amount-change-increase-disabled div.maximum-bags-popup.ng-scope.ng-hide")).$("span:nth-child("+i+")");
    };
    this.toolTipDisablePlusTextAmount = function(){
         return element(by.css("a.amount-change-increase-disabled div.maximum-bags-popup.ng-scope.ng-hide")).$("span.bag-amount");
    };
    this.quantityCard = function(i){
    return element.all(by.css('[class="equipment-info"]')).get(i);
    };
    this.confirmButtonDisabled = function(){
         return element(by.css("button.primary-disabled"));
    };
    this.confirmButtonEnabled = function(){
         return element(by.css("button.primary"));
    };
    this.infantIcon = function(indexOfPassengerCard){
        return element.all(by.css('[class="has-inf ng-isolate-scope"]')).get(indexOfPassengerCard);
    };
    this.maximumReachedEquipment = function (indexOfPassinger) {
        return element.all(by.css('[class="modal-equipment"]')).get(indexOfPassinger).element(by.css('[class="maximum-message ng-scope"]'));
    };
    this.maximumMessage = function (indexOfPassinger) {
        return element.all(by.css('[class="modal-equipment"]')).get(indexOfPassinger).element(by.css("div.maximum-message.ng-scope > p"));
    };
    this.babyActiveOutboundEquipment = function () {
        return element(by.css('[translate="trips.extra.BABY_purchased_items_outbound"]'))
    };
    this.babyActiveInboundEquipment = function () {
        return element(by.css('[translate="trips.extra.BABY_purchased_items_inbound"]'))
    };
};

module.exports = BabyEquipmentPage;

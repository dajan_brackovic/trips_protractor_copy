var MusicEquipmentPage = function () {

    this.plusButtonSameForBothFlights = function (i) {
        return element.all(by.css('button.core-btn.inc.core-btn-wrap')).get(i);
    };
    this.plusButtonFlightBack = function (i) {
        return element.all(by.css('[ng-click="model.incPrice()"]')).get(i);
    };
    this.minusButtonDisabled = function(i){
        return element.all(by.css("button.core-btn.dec.core-btn-wrap[disabled='disabled']")).get(i);
    };
    this.passengerCard = function(i){
        return element.all(by.css('[class="persons-name"]')).get(i);
    };
    this.plusButtonFlightOut = function (i) {
        return element.all(by.css('[ng-click="model.incPrice()"]')).get(i);
    };
    this.plusButtonMorePassengers = function(i){
        return element.all(by.css('[ng-click="model.incPrice()"]')).get(i);
    };
    this.minusButtonSameForBothFlights = function (i) {
        return element.all(by.css('button.core-btn.dec.core-btn-wrap')).get(i);
    };
    this.minusButtonFlightBack = function (i) {
        return element.all(by.css('[ng-click="vm.flightBackCard.incPrice()"]')).get(i);
    };
    this.minusButtonFlightOut = function (i) {
        return element.all(by.css('[ng-click="vm.flightOutCard.incPrice()"]')).get(i);
    };
    this.amountMusicEquipment = function (k) {
        return element.all(by.css('[class="value-display ng-binding ng-scope"]')).get(k);
    };
    this.amountMusicEquipmentSeted = function (k) {
        return element.all(by.css('[class="value-display ng-binding ng-scope set"]')).get(k);
    };
    this.informationCardME = function(){
        return element(by.css('[ng-click="vm.showMusicInfo()"]'));
    };
    this.userIconImage1 = function () {
        return element(by.css());
    };
    this.moreInformationME = function () {
        return element(by.css('[translate="trips.extra.music_equipment.more_info_description"]'));
    };
    this.userIconImage2 = function () {
        return element(by.css());
    };
    this.userIconName1 = function () {
        return element(by.css());
    };
    this.userIconName2 = function () {
        return element(by.css());
    };
    this.titleMusicEquiment = function () {
        return element(by.css("div.dialog-header > h1 > div > span"));
    };
    this.closeButton = function () {
        return element(by.css());
    };
    this.closeButtonX = function () {
        return element(by.css('[ng-click="closeThisDialog(\'x\')"]'));
    };
    this.cancelButton = function (i) {
        return element.all(by.css('[translate="trips.side.cancel"]')).get(i);
    };
    this.confirmButton = function () {
        return element.all(by.css('[translate="trips.side.confirm"]')).get(1);
    };
    this.confirmButtonClick = function () {
        return element.all(by.css('[translate="trips.side.confirm"]')).get(0);
    };
    this.backButton = function () {
        return element(by.css());
    };
    this.alertInfo = function () {
        return element(by.css());
    };
    this.musicLogo1 = function () {
        return element(by.css());
    };
    this.musicLogo2 = function () {
        return element(by.css());
    };
    this.labelAddMusicEquipment = function () {
        return element(by.css("div.alert.modal-alert-info > span"));
    };
    this.labelAddMusicEquipmentIcon = function () {
        return element(by.css("core-icon div svg use"));
    };
    this.cornerPrice = function () {
        return element.all(by.css('[class="amount ng-scope"]')).last();
    };
    this.plusInformationButton = function(){
      return element(by.css('[icon-id="icons.glyphs.plus"]'));
    };
    this.minusInformationButton = function(){
        return element(by.css('[icon-id="icons.glyphs.minus"]'));
    };
    this.musicEquipmentInformationCardText = function(){
        return element(by.id('ngdialog1-aria-describedby'));
    };
    this.passengerLogo = function(i){
        return element.all(by.css('[icon-id="icons.glyphs.person"]')).get(i);
    };
    this.quantityCard = function(i){
        return element.all(by.css('[class="equipment-info"]')).get(i);
    };
    this.passengerTypeAndNumber = function(i){
        return element.all(by.css('[class="persons-name"]')).get(i);
    };
    this.centralPriceNN = function(){
        return element.all(by.css('[class="price ng-binding"]')).last();
    };
    this.centralPriceNNN = function(){
        return element.all(by.css('[class="price ng-binding"]')).get(0);
    };
    this.musicLogo = function (i) {
        return element.all(by.css('[class="equipment-icon ng-isolate-scope"]')).get(i);
    };
    this.checkBoxSFBF = function (i) {
        return element.all(by.css("input[id^='same-flight']")).get(i);
    };
    this.inboundText = function(index){
        return element.all(by.css('[translate="trips.extra.flight_out"]')).get(index);
    };
    this.outboundText = function(index){
        return element.all(by.css('[translate="trips.extra.flight_back"]')).get(index);
    };
     this.toolTipDisablePlusHide = function(){
        return element(by.css('[class="tooltip-callout-content top ng-hide"]'));
    };
    this.toolTipDisablePlusShown = function(){
        return element(by.css('[class="tooltip-callout-content top"]'));
    };
    this.toolTipDisablePlusText = function(i){
        return element(by.css("a.amount-change-increase-disabled div.tooltip-callout.top.ng-hide")).$("span:nth-child("+i+")");
    };
    this.toolTipDisablePlusTextAmount = function(){
         return element(by.css("a.amount-change-increase-disabled div.tooltip-callout.top.ng-hide")).$("span.bag-amount");
    };
    this.plusButtonDisabled = function(i){
        return element.all(by.css('[class="btn inc disabled"]')).get(i);
    };
    this.checkBoxSFBFSelected = function(i){
        return element.all(by.css('[class="ng-scope single-holder-normal"]')).get(i);
    };
    this.checkBoxSFBFUnSelected = function(i){
        return element.all(by.css('[class="ng-scope single-holder-normal ng-hide"]')).get(i);
    };
    this.centralPrice = function(i){
        return element.all(by.css('[class="price ng-binding"]')).get(i);
    };
    this.centralPriceRight = function(i){
        return element.all(by.css("div.ng-scope.equipment-holder-double-normal > div > span.price.ng-binding")).get(i);
    };
    this.centralPriceLeft = function(i){
        return element.all(by.css('div.ng-scope.equipment-holder-double-bordered-right div span.price.ng-binding')).get(i);
    };
    this.amountMusicEquipmentRight = function (i) {
        return element.all(by.css("div.equipment-holder-double-normal")).get(i);
    };
    this.amountMusicEquipmentLeft = function (i) {
        return element.all(by.css("div.equipment-holder-double-bordered-right")).get(i);
    };
     this.confirmButtonDisabled = function(){
         return element(by.css("button.primary-disabled"));
    };
    this.confirmButtonEnabled = function(){
         return element(by.css("button.primary"));
    };
    this.iconForPassanger = function(i){
        return element.all(by.css('[icon-id="icons.glyphs.person"]')).get(i);
    };
    this.informationText = function(){
        return element(by.css('[translate="trips.extra.music_equipment.alert"]'));
    };
    this.minusButtonDisabledMusic = function(indexOfPassinger,indexOfMinus){
        return element.all(by.css('[class="modal-equipment"]')).get(indexOfPassinger).element(by.css("div.equipment-info > div:nth-child("+indexOfMinus+") > div > div > div.ng-scope.single-holder-normal > div > div > button.btn.dec.disabled"));
    };
    this.musicEquipmentAdded = function(){
        return element(by.css("div[translate='trips.potential.you_have_added_musc_plural'] strong"))
    };
    this.musicInfantIcon = function (i) {
        return element.all(by.css('[icon-id="icons.glyphs.baby"]')).get(i);
    };
    this.musicActiveOutboundEquipment = function () {
        return element(by.css('[translate="trips.extra.MUSC_purchased_items_outbound"]'))
    };
    this.musicActiveInboundEquipment = function () {
        return element(by.css('[translate="trips.extra.MUSC_purchased_items_inbound"]'))
    };
    this.maximumReachedEquipment = function (indexOfPassinger) {
        return element.all(by.css('[class="modal-equipment"]')).get(indexOfPassinger).element(by.css('[class="maximum-message ng-scope"]'));
    };
    this.maximumMessage = function (indexOfPassinger) {
        return element.all(by.css('[class="modal-equipment"]')).get(indexOfPassinger).element(by.css("div.maximum-message.ng-scope > p"));
    };


    this.amountOfEquipmentFlightOut = function (index) {
        return element.all(by.css('[class="ng-scope equipment-holder-double-bordered-right"] [class="value-display ng-binding ng-scope set"]')).get(index);
    };

    this.amountOfEquipmentFlightBack = function (index) {
        return element.all(by.css('[class="ng-scope equipment-holder-double-normal"] [class="value-display ng-binding ng-scope"]')).get(index);
    }

};

module.exports = MusicEquipmentPage;
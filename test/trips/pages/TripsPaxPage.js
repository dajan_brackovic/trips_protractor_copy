var TripsPaxPage = function () {


    this.btnAddPax = function () {
        return element(by.css(
            "#passengers > div > div > div.content.ng-isolate-scope > div > div.pax-title > div.right.ng-scope > button.add-pax-button.ng-scope"
        ));
    };

    //aduLT
    this.selectTitleDropDownOption = function (index) {
//        return element(By.xpath('(//*[@name="paxTitle"])[' + index + '] //option[text() = \'' + option + '\']'));
          return element.all(by.css("select[id^='title'] option:nth-child(2)")).get(index);
    };

    this.textFieldFirstName = function (index) {
        return element.all(By.model("passenger.name.first")).get(index);
    };

    this.textFieldLastName = function (index) {
        return element.all(By.model("passenger.name.last")).get(index);
    };

    this.btnAddPaxBalearic = function () {
        return element(By.css("button[ng-show='paxForm.lockedState']"));
    };

    // Document Check

    this.btnCheck = function(){
        return element(by.css("span[translate='trips.pax.domestic.submit']"));
    };

    this.btnEditPax = function(){
        return element(by.css("button.edit"));
    };

    this.btnCheckContinue = function(){
        return element(by.css("button[ng-click='closeThisDialog()']"));
    };

    this.validatedMessage = function(){
        return element(by.css("span.status.success"));
    };

    this.notValidatedMessage = function(){
        return element(by.css("span.status.failed"));
    };

    this.btnEditPaxDetail = function(){
        return element(by.css("button.edit"));
    };

    //INFANT NAME
    this.infantFirst = function (indexFirst) {
        return element.all(By.model("passenger.infant.name.first")).get(indexFirst);

    }
    this.infantLast = function (indexLast) {
        return element.all(By.model("passenger.infant.name.last")).get(indexLast);
    }
    this.infantDAY = function (index) {
        return element.all(by.css("select[name='day'] option:nth-child(2)")).get(index);
    }
    this.infantMONTH = function (index) {
        return element.all(by.css("select[name='month'] option:nth-child(2)")).get(index);
    }
    this.infantYEAR = function (index, option) {
        return element.all(by.css("select[name='year'] option:nth-child(" + option + ")")).get(index);
    }

//Balearic
    this.balearicFirstname = function () {
        return element(by.model("pax.name.first"));
    };

    this.balearicSurname = function () {
        return element(by.model("pax.name.middle"));
    };

    this.balearicProofResidencyDropdown = function () {
        return element(by.model("pax.residentInfo.docType"));
    };

    this.balearicProofDNI = function (index) {
        return element.all(by.css("[name^='docType'] option:nth-child(2)")).get(index);
    };

    this.balearicProofUnder14 = function (index) {
        return element.all(by.css("[name^='docType'] option:nth-child(4)")).get(index);
    };

    this.balearicProofValue = function (value) {
        return element(by.css("[name='docType'] [label=" + value + "]"));
    };

    this.balearicDocumentNumber = function (index) {
        return element.all(by.css("input[id^='docNum']")).get(index);
    };

    this.balearicCommunityDropDown = function () {
        return element(by.model("pax.residentInfo.community"));
    };

    this.balearicCommunityValue = function (index, value) {
        return element.all(by.css("[name^='community'] [label="+ value +"]")).get(index);
    };

    this.balearicFamilyCertificate = function (index) {
        return element.all(by.css("input[id^='largeFamilyCert']")).get(index);
    };

    this.balearicMunicipalityValue = function (index, value) {
        return element.all(by.css("[name^='municipality'] [label="+ value +"]")).get(index);
    };


    // contact
    this.textFieldEmailAddress = function () {
        return element(By.model("cForm.contact.emailAddress"));
    };

    this.changePhoneNumber = function () {
        return $("form[name=contactFormForLoggedUser] a[translate='trips.checkout.contact.change_phone_number']");
    };


    this.selectContactCountryDropDownOption = function (option) { // Ireland
        return element.all(By.css(("select[name='phoneNumberCountry'] option[label='Malta']")));
    };

    this.selectCountryDropDownOptionLoggedUser = function (option) { // Ireland
        return element(by.cssContainingText("form[name=contactFormForLoggedUser] select option", option));
    };

    this.textFieldPhoneNumber = function () {
        return element(by.model("ctrl.model.number"));
    };

    this.textFieldPhoneNumberLoggedUser = function () {
        return $("form[name='contactFormForLoggedUser'] input[ng-model='cForm.contact.PhoneNumber']");
    };

    this.btnSavePhoneNumber = function () {
        return $("form[name='contactFormForLoggedUser'] button[translate='trips.checkout.contact.save']");
    };

   // payment
    this.savedEmailAddress = function () {
        return element(by.css("[ng-if='cForm.displayEmailView'] .ng-binding"));
    };

    this.savedPhoneNumber = function () {
        return element(by.css("[ng-if='cForm.displayPhoneView'] .ng-binding"));
    };

    this.changeEmailLink = function () {
        return element(by.css("[translate='trips.checkout.contact.change_email']"));
    };

    this.addNewNumberLink = function () {
        return element(by.css("[translate='trips.contact.change_phone_number']"));
    };

    this.btnSaveEmailAddress = function () {
        return element(by.css("div.full-width-form.change-email [translate='trips.checkout.contact.save']"));
    };

    this.btnSavePhoneNumber = function () {
        return element(by.css("div.full-width-form [translate='trips.checkout.contact.save']"));
    };

    this.btnCreditCardSelect = function () {
        return element(by.css("span[translate='common.components.payment_forms.method_card']"));
    };

    this.btnPayPalSelect = function () {
        return element(by.css("span[translate='common.components.payment_forms.method_paypal']"));
    };

    this.selectPaymentTypeDropDown = function () {
        return element(by.css("select[name='cardType']"));
    };

    this.selectPaymentTypeDropDownOption = function (option) {
        return element(By.css("[ng-model='cm.payment.cardType'] option[label='" + option + "'] "));
    };

    this.textFieldCardNumber = function () {
        return element(by.css("[ng-model='cm.payment.cardNumber']"));
    };

    this.selectExpiryMonthDropDownOption = function (option) {
        return element(By.css("select.expiry-month-select  option:nth-child(" + option + ")"));
    };

    this.selectExpiryYearDropDown = function () {
        return element(by.css(
            "#checkout > div.center-section > section.ng-scope.active > div > div > div > div.content.ng-isolate-scope > div > form > div.form-input.expiry-year.ng-invalid > div > select"
        ));
    };

    this.selectExpiryYearDropDownOption = function (option) {
       return element(By.cssContainingText('select.expiry-year-select option' , option ));
    };

    this.textFieldCVV = function () {
        return element(by.model("cm.payment.securityCode"));
    };

    this.textFieldCVVmyFr = function () {
         return element(by.css("input[id^='card-cvv']"));
    };

    this.textFieldCardHoldersName = function () {
        return element(by.model("cm.payment.cardHolderName"));

    };

    //Billing address

    this.savedBillingAddressMyFr = function () {
        return element(by.css("div.billing-address"));
    };

    this.textFieldCity = function () {
        return element(by.model("ctrl.model.city"));
    };

    this.textFieldPostCode = function () {
        return element(by.model("ctrl.model.postcode"));

    };

    this.textAddressLine1 = function () {
        return element(by.model("ctrl.model.addressLine1"));
    };

    this.textAddressLine2 = function () {
        return element(by.model("ctrl.model.addressLine2"));
    };

    this.selectBillingCountryDropDownOption = function (option) { // Ireland
        return element(By.css("div.country select option[label='Ireland']"));
    };

    this.btnPaymentContinue = function () {
        return element(by.css("[translate='common.components.payment_forms.pay_now']"));
    };

    this.btnPaymentContinueActive = function () {
        return element(by.css('[ng-click="submitVat() && scrollToError() && payment.form.$valid && submitPayment()"]'));
    };

    this.btnPaymentContinueHoldFare = function () {
        return element(by.css("[translate='common.components.payment_forms.pay_now']"));
    };

    this.btnPaymentContinueVoucher = function () {
        return element(by.css(".core-btn-primary.core-btn-medium"));
    };

    // PayPal
    this.btnPayPal = function () {
        return element.all(by.css(".payment-methods [ng-model='pms.model']")).get(1);
    };

    this.paymentTermsAndConditions = function () {
        return element(by.model("pm.termsAccepted"));
    };

    //VAT Details field
    this.btnAddVatDetails = function () {
       return element(by.css("span[translate='common.components.payment_forms.add_vat_details']"));
    };

    this.fieldVatNumber = function () {
        return element(by.model("ctrl.model.vatNumber"));
    };

    this.fieldBusinessName = function () {
        return element(by.model("ctrl.model.businessName"));
    };

    this.labelSameAddress = function () {
        return element(by.css("section[ng-form='vat.form'] label[for='same-address']"));
    };

    this.countryDropdown = function () {
        return element(by.css("section select[ng-model='address.country']"));
    };

    this.countryDropdownOptions = function (index) {
        return element.all(by.css("section select[ng-model='address.country'] option")).get(index);
    };

    this.countryDropdownOptionsVat = function (index) {
        return element(by.css("#vatDetailsAddressCountry option:nth-child("+ index +")"));
    };

    this.fieldVatCity = function () {
        return element(by.id("vatDetailsAddressCity "))
    };

    this.fieldVatPostCode = function () {
        return element(by.id("vatDetailsAddressPostcode"));
    };

    this.fieldVatAddress1 = function () {
        return element(by.id("vatDetailsAddressAddressLine1"));
    };

    this.lblVatErrorMessage = function () {
        return element(by.css("span[translate='common.components.payment_forms.vat.error_explain_not_domestic']"));
    };

    //Payment successful

    this.numberPNR = function () {
        return element(by.css("strong"));
    };

    //Payment Unsuccessful
    this.paymentError = function () {
        return element(by.css(".error"));
    };

    this.paymentDeclinedPopUpButton = function () {
        return element(by.css("#ngdialog2 > div.ngdialog-content > div.modal-dialog > div > div.dialog-body > div > button"));
    };

    this.paymentErrorDialog = function () {
        return element(by.css(".dialog-header [translate='common.components.failed-payment.title']"));
    };

    this.invalidcardNumber = function () {
        browser.executeScript("window.scrollTo(0,1400);");
        return element(by.css("span[translate='common.components.payment_forms.error_card_number_luhn']"));
    };

    // Required Passenger Details For Balearic Discounts
    this.requiredPaxDetails = function(){
        return element.all(by.css("span[ng-message='required']"));
    }

    //Dcc
    this.dccField = function(){
        return element(by.css(".dcc"));
    };

    //Special Assistance
    this.labelSpecialAssistance = function(){
       return element(by.css("label[for='toggle-single'] span"));
    };

    this.labelSpecialAssistanceTitle = function(){
        return element(by.css("h1[translate='trips.special_assistance.main_title']"));
    };

    this.viewMoreInfo = function(){
        return element(by.css("a.core-btn-link.icon-l"));
    };

    this.viewMoreInfoPara = function(){
        return element(by.css("div.more-info-equipment-opened"));
    };

    this.specialAssistancePaxCheckBox = function(){
        return element(by.css("label.is-special-assistance-required span"));
    };

    this.specialAssistanceDropDown = function(){
        return element(by.css("div.core-select.selected-assistance-type"));
    };

    this.listOfOptionsInSpecialAssistanceDropDown = function (){
        return element.all(by.repeater("assistanceType in ctrl.assistanceAvailability.assistance")); // should be 10 elements
    };

    this.returnSpecialAssistanceDropDown = function (){
        return element.all(by.css("div.inbound-flight-wrapper li.option-element"));
    };

    this.listOfOptionsInSpecialAssistanceDropDown2option = function (){
        return element.all(by.repeater("assistanceType in ctrl.assistanceAvailability.assistance")).get(1);
    };

    this.specialAssistanceWheelChairQuestion = function(){
        return element(by.css("span[translate='trips.special_assistance.wheelchair_title']"));
    };

    this.specialAssistanceWheelChairQuestionYes= function(){
        return element(by.css("input[id*='outboundFlightSpecialAssistance_wheelchairSpecialAssistance_isRequired_yes']"));
    };

    this.specialAssistanceWheelChairReturn= function(){
        return element(by.css("div.inbound-flight-wrapper label[for^='wheelchairYes']"));
    };

    this.optionsOfWheelChair = function (){
        return element.all(by.repeater("wheelchairType in ctrl.assistanceAvailability")); // should be 4 elements
    };

    this.selectFirstWheelChair = function (){
        return element(by.css("input[id*='WCAP']"));
    };

    // Duplicate Name Error

    this.duplicatePaxNameError = function () {
        return element(by.css(".info-text[translate='trips.pax.duplicate_name']"));
    };


    // My Ryanair Payment Page

    this.firstAddedUserInPassengerDetails = function (index) {
        return element.all(by.css("[ng-repeat^='companion in vm.filteredList']")).get(index);
    };

    this.btnLogInMyFr = function () {
        return element(by.css('[translate="trips.checkout.passengers.login"]'));
    };

    this.savedCard = function () {
        return element(by.css("div.saved-card"));
    };

    this.savedCardLastFourDigits = function () {
        return element(by.css(".credit-card-number.ng-scope"));
    };

    this.myFrSignupPopUp = function () {
        return element(by.css(".new-account-wrapper"));
    };

    this.myFrSignupPopUpRadioBtn = function () {
        return element(by.css("label.long-checkbox-label"));
    };

    // Price BreakDown On Payment Page

    this.xAddedItems = function () {
        return element.all(by.css("a[ng-click='item.onRemove()']"));
    };

    this.itemsOnPriceBreakdownIncludingFlights = function () {
        return element.all(by.css("div.trip-item"));
    };


    // Car Hire Lead Driver

    this.fieldLeadDriverFirstName = function () {
        return element(by.model("ld.model.first"));
    };

    this.fieldLeadDriverLastName = function () {
        return element(by.model("ld.model.last"));
    };

    //ticket information
    this.price = function () {
        return element(by.css(".item-price"));
    };
    this.date = function () {
        return element(by.css(".dest-info>span"));
    };

    this.pricePriorityBoardingAdded = function () {
        return element(by.css("[translate='trips.basket.group.item.priority-boarding']+a+strong.item-price"));
    };

};

module.exports = TripsPaxPage;
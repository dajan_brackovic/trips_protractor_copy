var CheckInPage = function () {

 // Select All Pax
    this.selectEveryone = function (index) {
        return element(by.css("a.select-all"));
    };

    this.paxListForCheckIn = function () {
        return element.all(by.css("[ng-model='vm.selectedPax[$index].selected']"));
    };

// Add Id Documents

    this.nationalityDropDown = function (index) {
        return element.all(by.css("[ng-model='doc.model.nationality'] option")).get(index); // 92 is Ireland
    };

    this.disabledDateDropDown = function () {
        return element(by.css("div.core-select [ng-model='selectedDate.date'][disabled='disabled']"));
    };

    this.dateOfBirthDay = function (index) {
        return element(by.css("ng-form > div > div:nth-child(2) > div.date-field > core-date-dropdowns > ng-form > div > div:nth-child(1) > div > select > option:nth-child(" + index + ")"));  // index 2 is 1st day of the month
    };

    this.dateOfBirthMonth = function (index) {
        return element(by.css("ng-form > div > div:nth-child(2) > div.date-field > core-date-dropdowns > ng-form > div > div:nth-child(2) > div > select > option:nth-child(" + index + ")")); // index 2 is January
    };

    this.dateOfBirthYear = function (index) {
        return element.all(by.css("[ng-model='selectedDate.year'] option")).get(index); // index 1 is 1915, and index 85 is 1999
    };

    this.documentType = function (index) {
        return element.all(by.css("[ng-model='doc.model.docType'] option")).get(index); // index 2 is passport
    };

    this.documentNumberField = function () {
        return element(by.css("[ng-model='doc.model.docNumber']"));
    };

    this.countryOfIssueDropDown = function (index) {
        return element.all(by.css("[ng-model='doc.model.countryOfIssue'] option")).get(index); // 92 is Ireland
    };

    this.expiryDateDay = function (index) {
        return element(by.css("ng-form > div > div:nth-child(6) > div.date-field > core-date-dropdowns > ng-form > div > div:nth-child(1) > div > select > option:nth-child(" + index + ")")); // index 2 is 1st day of the month
    };

    this.expiryDateDayMonth = function (index) {
        return element(by.css("ng-form > div > div:nth-child(6) > div.date-field > core-date-dropdowns > ng-form > div > div:nth-child(2) > div > select > option:nth-child(" + index + ")")); // index 2 is January
    };

    this.expiryDateDayYear = function (index) {
        return element(by.css("ng-form > div > div:nth-child(6) > div.date-field > core-date-dropdowns > ng-form > div > div:nth-child(3) > div > select > option:nth-child(" + index + ")")); // index 2 is 2015
    };

    this.btnContinueCheckIn = function () {
        return element(by.css("button.core-btn-primary:not(.core-btn-block)[ng-click='confirm();']"));
    };

    this.btnCancel = function () {
        return element(by.css("button.secondary"));
    };

    this.preSelectedIdDocument = function () {
        return element(by.css("[ng-model='doc.selectedDocument'] option:nth-child(1)"));
    };

    //Select Seats

    this.selectFlightCheckbox = function () {
        return element(by.css("div.clearfix.first.passenger input")); //one way flight
    };

    this.selectFlightCheckboxRtOut = function () {
        return element(by.css("div.clearfix.first.passenger"));
    };

    this.selectFlightCheckboxRtIn = function () {
        return element(by.css(".select-passenger input[ng-model='seats.inboundSelected']")); // return inbound
    };

    this.checkInNotOpenWarning = function () {
        return element(by.css("div.checkin_not_open"));
    };

    this.selectedSeatNumber = function () {
       // return element.all(by.css('div.clearfix.ng-binding'));
        return element.all(by.css('div.seat-number.ng-binding'));
    };

    this.flightNotOpenForCheckIn = function () {
        return element(by.css("[translate='trips.checkin.seats.checkin_not_open']"));
    };


    // Boarding Pass
    this.boardingPassPage = function () {
        return element(by.id("boarding-pass"));
    };

    this.boardingPassCard = function () {
        return element.all(by.css("state.boarding-pass"));
    };

    this.bpImagePage = function () {
        return element.all(by.css("div.one-page")).get(0);
    };

    this.bpBpPage = function () {
        return element.all(by.css("div.one-page")).get(1);
    };

    this.bpFoldPage = function () {
        return element.all(by.css("div.upsidedown-page")).get(0);
    };

    this.bpTravelPlanPage = function () {
        return element.all(by.css("div.upsidedown-page")).get(1);
    };

    this.bpBpPagePaxFirstName = function () {
        return element.all(by.css("div.main-info .passenger")).get(0);
    };

    this.bpBpPagePaxSecondName = function () {
        return element.all(by.css("div.main-info .passenger")).get(1);
    };

    this.bpBpPageBookingRef = function () {
        return element.all(by.css("div.detail span.value")).get(1);
    };

    this.bpBpPageHeaderMessage = function () {
       // return element(by.css("div.header-message span"));
        return element(by.css("div.main-info h2"));
    };

    this.bpBpPageSeatNumber = function () {
        return element.all(by.css("state span.seat-value"));
    };

    this.bpBpPageSeq = function () {
        return element(by.css("div.main-info  > div.detail:nth-child(7) .value"));
    };

    this.bpBpPageBarCode = function () {
        return element(by.css("div.banner"));
    };

    this.bpBpPageOriginShort = function () {
        return element.all(by.css("div.destination div.city .short")).get(0);     //Example DUB
    };

    this.bpBpPageOriginFull = function () {
        return element.all(by.css("div.destination div.city .highlighted")).get(0);     //Example Dublin
    };

    this.bpBpPageDestinationShort = function () {
        return element.all(by.css("div.destination div.city .short")).get(1);     //Example STN
    };

    this.bpBpPageDestinationFull = function () {
        return element.all(by.css("div.destination div.city .short")).get(1);     //Example London Stansted
    };

    this.bpBpPageFlightNumber = function () {
        return element(by.css("div.destination div.path .path-name"));
    };

    this.bpBpPageDate = function () {
        return element.all(by.css("div.dates div.date .value")).get(0);
    };

    this.bpBpPageGateCloses = function () {
        return element.all(by.css("div.dates div.date .value")).get(1);
    };

    this.bpBpPageDeparts = function () {
        return element.all(by.css("div.dates div.date .value")).get(2);
    };

    this.bpBpPageLuggage = function () {
        return element.all(by.css("span.luggage"));
    };

    this.boardingPassPrint = function () {
        return element(by.css("div.links [translate='trips.checkin.boarding.print']"));
    };

    this.boardingPassDownload = function () {
        return element(by.css("div.links [translate='trips.checkin.boarding.download']"));
    };

    //boarding pass
    this.bookingRefBoardingPass = function () {
        return element.all(by.css("span.value")).get(1); // return inbound
    };

    this.paxListBoardingPasses = function(){
        return element.all(by.repeater("pax in passengers"));
    };

    this.viewAllBoardingPasses = function(){
        return element(by.css("div.view-all button.btn-pass"));
    };

    // CheckIn Seats

    this.checkInSeatOut = function () {
       // return element(by.model("pax.selectSeatOut"));
        return element(by.css("[for='outbound-paid'] span"));
    };

    this.checkInSeatIn = function () {
       // return element(by.model("pax.selectSeatIn"));
        return element(by.css("[for='paid'] span"));
    };

    this.seatMessaging = function () {
        return element(by.css("div.description"));
    };

    // x out on seat map

    this.xOut = function () {
        return element(by.css(".dialog-close.pull-right.ng-isolate-scope"));
    };


    // continue button after declined payment

    this.btnFaildPaymentOk = function () {
        return element(by.css("#ngdialog3 div button"));
    };


    // Select Passengers

    this.selectEveryOneForCheckIn = function () {
        return element(by.css("div.passenger-list a"));
    };

    // Priority

    this.priorityBp = function () {
        return element.all(by.css("div.main-info h2"));
    };

};

module.exports = CheckInPage;
 var VoucherPage = function () {

     this.get = function () {
         browser.get('gift-vouchers/buy-now');
     };

    this.theme = function (index) {
        return $( "div.slide:nth-child(" + index + ") ");
    };

     this.ddAmountField = function(){
         return $("li:nth-child(2) > div > fieldset > span > core-icon > div > svg");
     };

     this.VchAmount = function (index) {
        return $("div.custom-radio-wrapper:nth-child(" + index + ") > label:nth-child(1) > span:nth-child(2)");
    };

     this.VchCurrency = function (index) {
         return element(by.css("div.core-select > select > option:nth-child(" + index +")"));
    };

     this.ddMessageField = function(){
         return element(by.css("li:nth-child(3) > div > fieldset > span > core-icon > div > svg"));
     };

     this.messageTextBox= function(){
         return element(by.model("vc.selection.message"));//ref id: 13
     };

     this.btnContinue = function () {
        return $("button.ng-scope");
     };

     this.btnAcceptTsAndCs = function () {
         return element(by.css("input.core-checkbox"));
     };

     this.yourName = function(){
         return element(by.css("input[id^='contactFirstName']"));//ref id: 14
     };

     this.yourEmail = function(){
         return element(by.css("input[id^='emailAddress']"));//ref id: 15

     };

     this.offerLabel = function(){
         return element(by.css("input[id^='offers']"));//ref id: 16
     };

     this.voucherCountryCode = function(index){
         return element(by.css("select[ng-model='ctrl.selectedCountry'] option:nth-child(" + index + ")"));//ref id: 12
     };

     this.phoneNumber = function(){
         return element(by.model("ctrl.model.number"));//ref id: 17
     };

     this.voucherFirstName = function(){
         return element(by.css("input[id^='recipientFirstName']"));//ref id: 18

     };

     this.voucherLastName = function(){
         return element(by.css("input[id^='recipientLastName']"));//ref id: 19
     };

     this.emailVoucher = function(){
         return element (by.model("pm.voucherRecipient.emailAddress"));//ref id:20
     };

     this.continueBtn = function(){
         return element(by.css("button.core-btn-primary.core-btn-medium"));//ref id: 21
     };

     this.redeemVoucher = function(){
         return element(by.css("a.core-link"));
     };

     this.voucherNumber = function(){
         return element(by.model("$ctrl.voucherNumber"));
     };

     this.redeemBtn = function(){
         return element(by.css("button[translate='trips.vouchers.breakdown.voucher_redeem_button']"));
     };
     this.voucherField = function(){
         return $('strong.item-price:nth-child(3)');
     };

     this.invalidVoucherError = function(){
         return element(by.css("[translate='trips.vouchers.breakdown.errors.reference_format']"));
     };

     // Voucher Summary

     this.giftFrom = function () {
         return element.all(by.css(".summary-info.ng-binding")).get(0);
     };

     this.giftTo = function () {
         return element.all(by.css(".summary-info.ng-binding")).get(1);
     };

     this.sendTo = function () {
         return element.all(by.css(".summary-info.ng-binding")).get(2);
     };

     this.reference = function () {
         return element.all(by.repeater('voucher in vs.summary.vouchers'));
     };

     this.confirmationText = function () {
         return element(by.css("[translate='vouchers.summary.success_message']"));
     };

     this.giftVoucher = function () {
         return element(by.css("span.voucher-price"));
     };

     this.giftVoucherHandlingFee = function () {
         return element.all(by.css("strong")).get(0);
     };

     this.totalPaid = function () {
         return element.all(by.css("strong")).get(3);
     };

     this.totalPaid1 = function () {
         return element (by.css(".price-amt"));//ref id: 22
     };

};
module.exports = VoucherPage;



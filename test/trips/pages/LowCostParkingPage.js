var LowCostParkingPage = function () {

    this.parking28Days = function () {
        return element(by.css('[translate="trips.parking.outside-bound-days-alert"]'));
    };
    this.parkingParkViaLogo = function () {
        return element(by.css('[src="/content/dam/ryanair/parking-pdfs-and-logos/logosparking/CLOUD_LTN.jpg"]'));
    };
    this.parkingL4PLogo = function () {
        return element(by.css('[src="/content/dam/ryanair/parking-pdfs-and-logos/logosparking/L4P_LTN.jpg"]'));
    };
    this.parkingCloudLink = function () {
        return element(by.css('[href="/content/dam/ryanair/parking-pdfs-and-logos/tnc/DAA_BLUE.pdf"]'));
    };
    this.parkingL4PLink = function () {
        return element(by.css('[href="https://parkcloud.blob.core.windows.net/waitingforgoodlink"]'));
    };
    this.parkingProviderLogoDAA = function () {
        return element(by.css('[src="/content/dam/ryanair/parking-pdfs-and-logos/logosparking/L4P_LTN.jpg"]'));
    };
    this.parkingProviderLogoCloud = function () {
        return element(by.css('[src="/content/dam/ryanair/parking-pdfs-and-logos/logosparking/CLOUD_LTN.jpg"]'));
    };
    this.parkingProviderLogo = function () {
        return element(by.css('[class="provider-logo ng-scope"]'));
    };
    this.titleLowCostParking = function () {
        return element(by.css('[translate="trips.parking.title"]'));
    };
    this.selectedParkingOption = function (indexOfOption) {
        return element(by.css('div.list-of-providers > ul > li.selected'));
    };
    this.selectParkingOption = function () {
        return element(by.css('[ng-if="option.id !== vm.selectedParkingOption.id"]'));
    };
    this.stayCheckSelected = function () {
        return element(by.css('[ng-if="option.id === vm.selectedParkingOption.id"]'));
    };
    this.datePicker = function () {
        return element(by.css("span[ng-click='vm.openDatePicker()']"));
    };
    this.selectDateTextField = function () {
        return element(by.css("div [start-date='vm.returnDate']"));
    };
    this.parkingProviderTitle = function () {
        return element(by.css("div.dialog-body > div > div > div.parking-benefits > h3"));
    };
    this.parkingProviderDropdown = function (indexOfValue) {
        return element(by.model('vm.selectedParkingProvider')).$('[label="' + indexOfValue + '"]');
    };
    this.parkingProviderOptionSelected = function () {
        return element(by.css('[class="ng-scope nya-bs-option selected"]'));
    };
    this.firstParkingProvider = function () {
        return element(by.css('[label="ParkVia"]'));
    };
    this.secondParkingProvider = function () {
        return element.all(by.repeater("option in vm.selectedParkingProvider.offers")).get(1);
    };
    this.parkingProviderOptionUnSelected = function () {
        return element(by.xpath('//*[@id="ngdialog3"]/div[2]/div/div/div/div[2]/div/div/div[1]/div/div[2]/select/option[2]'));
    };
    this.parkingProviderOptionLocked = function () {
        return element(by.css('[class="ng-pristine ng-invalid ng-invalid-required ng-touched ng-untouched ng-valid"]'));
    };
    this.mainParkingTitle = function () {
        return element(by.css("div.modal-header.ng-scope > h2 > span"));
    };
    this.closeDrawerButton = function () {
        return element(by.css('[class="dialog-close pull-right ng-isolate-scope"]'));
    };
    this.parkingBenefitHeader = function () {
        return element(by.css('div.parking-benefits > h3'));
    };
    this.parkingBenefitText = function () {
        return element(by.css('div.parking-benefits > p'));
    };

    this.totalText = function () {
        return element(by.css('div.sum-header > h3 > span'));
    };
    this.totalPrice = function () {
        return element(by.css('div.list-of-providers div div.sum-header h3 b'));
    };
    this.registrationNumber = function () {
        return element(by.model("vm.registrationNumber"));
    };
    this.termsAndConditionsLink = function () {
        return element.all(by.css('[target="_blank"]')).get(5);
    };
    this.confirmButton = function () {
        return element(by.css('[translate="trips.side.confirm"]'));
    };
    this.parkingPriceBusterPerDay = function () {
        return element(by.css()).get(1);
    };
    this.listOfBenefits = function () {
        return element(by.css('[class="parking-benefits ng-scope"]'));
    };
    this.listOfBenefitsCheck = function () {
        return element(by.css('[class="parking-benefits ng-scope"]'));
    };
    this.listOfParkingOptions = function () {
        return element.all(by.repeater('option in vm.selectedParkingProvider.options'));
    };
    this.listOfSingleParkingOptions = function () {
        return element.all(by.repeater('option in vm.selectedParkingProvider.offers'));
    };
    this.parkingOption = function (i) {
        return element.all(by.repeater('option in vm.selectedParkingProvider.offers')).get(i);
    };
    this.nameOfParkingOption = function (i) {
        return element.all(by.css('[class="parking-providers-city-list"]')).get(i);
    };
    this.parkingProviderLockedState = function () {
        return element(by.css('[class="ng-pristine ng-invalid ng-invalid-required ng-touched ng-untouched ng-valid placeholder-selected"]'));
    };
    this.parkingProviderOpenState = function () {
        return element(by.css('[class="ng-pristine ng-invalid ng-invalid-required ng-touched ng-valid"]'));
    };
    this.priceOfParkingOption = function (i) {
        return element.all(by.css('[class="parking-providers-price"]')).get(i);
    };
    this.priceOfParkingOptionCount = function (i) {
        return element.all(by.css('[class="parking-providers-price"]'));
    };
    this.nameOfParkingOptionCount = function () {
        return element.all(by.css('[class="parking-providers-city-list"]'));

    };
    this.informationIcon = function (i) {
        return element.all(by.css('[class="parking-providers-info-icon"]')).get(i);
    };
    this.totalPriceCorner = function () {
        return element(by.css("div.footer-container div.price span"));
    };
    this.selectDay = function (date) {
        return element.all(by.css("div.content-box ul.days li:not(.disabled):not(.blank)")).get(date);
    };
    this.openCalendar = function () {
        return element(by.css('[class="ng-isolate-scope date-picker-wrapper opened"]'));
    };
    this.informationIconPopUpHidden = function () {
        return element(by.css('[class="ng-isolate-scope ng-hide"]'));
    };
    this.informationIconPopUp = function () {
        return element(by.css('[class="parking-info-popup ng-scope"]'));
    };
    this.informationIconPopUpItemsNames = function (i) {
        return element(by.css("div.parking-info-popup.ng-scope ul li:nth-child(" + i + ") span"));
    };
    this.informationIconPopUpTitle = function () {
        return element(by.css("div.parking-info-popup.ng-scope h1"));
    };
    this.informationIconDownloadPDF = function () {
        return element(by.css("parking-info div.parking-info-popup.ng-scope a"));
    };
    this.selectedOptionOfParkingType = function () {
        return element(by.css("div.list-of-providers ul li.ng-scope.selected"));
    };
    this.unSelectedOptionOfParkingType = function (index) {
        return element.all(by.css('[ng-class="{selected: option.key === vm.selectedParkingOption.key}"]')).get(index);
    };
    this.informationMessageRN = function () {
        return element(by.css('[class="has-error-label ng-scope"]'));
    };
    this.cornerPrice = function () {
        return element.all(by.css('[class="amount ng-scope"]')).last();
    };
    this.totalPriceParkingOption = function () {
        return element(by.css('[translate="trips.parking.total"]'));
    };
    this.basketParking = function () {
        return element(by.css('[class="trips-breakdown pull-left"]'));
    };
    this.totalPriceCorner = function () {
        return element(by.css('[class="amount ng-scope"]'));
    };

};
module.exports = LowCostParkingPage;
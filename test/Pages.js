var BabyEquipmentPage = require('./trips/pages/BabyEquipmentPage');
var MusicEquipmentPage = require('./trips/pages/MusicEquipmentPage');
var FlightsPage = require('./trips/pages/FlightsPage');
var FOHHomePage = require('./foh/pages/FOHHomePage');
var LowCostParkingPage = require('./trips/pages/LowCostParkingPage');
var PotentialTripPage = require('./trips/pages/PotentialTripPage');
var TripsExtrasPage = require('./trips/pages/TripsExtrasPage');
var TripsHomePage = require('./trips/pages/TripsHomePage');
var TripsPaxPage = require('./trips/pages/TripsPaxPage');
var TripsSummaryPage = require('./trips/pages/TripsSummaryPage');
var VouchersPage = require('./trips/pages/VoucherPage');
var AddPassengerNamePage = require('./trips/pages/AddPassengerNamePage');
var PriceBreakDownPage = require('./trips/pages/PriceBreakDownPage');
var InsurancePage = require('./trips/pages/InsurancePage');
var SportEquipmentPage = require('./trips/pages/SportEquipmentPage');
var PaymentPage = require('./trips/pages/PaymentPage.js');
var SeatsPage = require('./trips/pages/SeatsPage');
var BagsPage = require('./trips/pages/BagsPage');
var ItineraryPage = require('./trips/pages/ItineraryPage');
var CarHirePage = require('./trips/pages/CarHirePage');
var CheckInPage = require('./trips/pages/CheckInPage');
var TransfersActiveTripPage = require('./trips/pages/TransfersActiveTripPage');
var ManageTripsPage = require('./trips/pages/ManageTripsPage');
var ManageYourTripPage = require('./trips/pages/ManageYourTripPage');
var SamsonitePage = require('./trips/pages/SamsonitePage');
var PayPalPage = require('./trips/pages/PayPalPage');
var HoldFarePage = require('./trips/pages/HoldFarePage');
var SelectTransferProviderPage = require('./trips/pages/SelectTransferProviderPage');
var HotelsPage = require('./trips/pages/HotelsPage');
var MyRyanairPage = require('./trips/pages/MyRyanairPage');
// foh
var FareFinderHomePage = require('./foh/pages/farefinder/FareFinderHomePage');
var FareFinderResultsPage = require('./foh/pages/farefinder/FareFinderResultsPage');
var FareFinderResultsDetailsPage = require('./foh/pages/farefinder/FareFinderResultsDetailsPage');
var TimeTableHomepage = require('./foh/pages/timetable/TimeTableHomepage');
var MapHomepage = require('./foh/pages/map/MapHome');
var RouteListpage = require('./foh/pages/map/RouteListPage');
var FlightInfoByNumber = require('./foh/pages/flightinfo/FlightInfoByNumber');
var FlightInfoByRoute = require('./foh/pages/flightinfo/FlightInfoByRoute');





var Pages = function () {
    this.sportEquipmentPage = new SportEquipmentPage();
    this.insurancePage = new InsurancePage();
    this.potentialTripPage = new PotentialTripPage();
    this.babyEquipmentPage = new BabyEquipmentPage();
    this.musicEquipmentPage = new MusicEquipmentPage();
    this.fOHHomePage = new FOHHomePage();
    this.lowCostParkingPage = new LowCostParkingPage();
    this.potentialTripPage = new PotentialTripPage();
    this.tripsExtrasPage = new TripsExtrasPage();
    this.tripsHomePage = new TripsHomePage();
    this.tripsPaxPage = new TripsPaxPage();
    this.tripsSummaryPage = new TripsSummaryPage();
    this.vouchersPage = new VouchersPage();
    this.priceBreakDownPage = new PriceBreakDownPage();
    this.seatsPage = new SeatsPage();
    this.bagsPage = new BagsPage();
    this.itineraryPage = new ItineraryPage();
    this.checkInPage = new CheckInPage();
    this.carHirePage = new CarHirePage();
    this.fareFinderHomePage = new FareFinderHomePage();
    this.fareFinderResultsPage = new FareFinderResultsPage();
    this.fareFinderResultsDetailsPage = new FareFinderResultsDetailsPage();
    this.timeTableHomepage = new TimeTableHomepage();
    this.paymentPage = new PaymentPage();
    this.manageTripsPage = new ManageTripsPage();
    this.manageYourTripPage = new ManageYourTripPage();
    this.transfersActiveTripPage = new TransfersActiveTripPage();
    this.timeTableHomepage = new TimeTableHomepage();
    this.mapHomepage = new MapHomepage();
    this.routeListPage = new RouteListpage();
    this.samsonitePage = new SamsonitePage();
    this.flightInfoByRoute=new FlightInfoByRoute();
    this.flightInfoByNumber= new FlightInfoByNumber();
    this.payPalPage = new PayPalPage();
    this.selectTransferProviderPage = new SelectTransferProviderPage();
    this.holdFarePage = new HoldFarePage();
    this.hotelsPage = new HotelsPage();
    this.myRyanairPage = new MyRyanairPage();

}
module.exports = Pages;
var sprintf = require("sprintf").sprintf;
var specId = "|myRyanair |fohintegration| C652442 ";

function sharedDescribe(menuIndex, subMenuIndex, pageUrl, sideMenuIdex, sideSubMenuIndex) {
    describe("", function () {
        it('Navigate to the homepage', function () {
            actions.fOHActions.goToPage();
        });

        it('Click the top menu item - market', function () {
            actions.fOHActions.clickLinkFromMenuLinkUser(menuIndex, subMenuIndex);
        });
        it(sprintf("Verify the page url changes and page is shown", pageUrl), function () {
            expect(browser.driver.getCurrentUrl()).toContain(pageUrl);
            actions.fOHActions.goToPage();
        });
        it(sprintf("Resize the window to be 768px to 991px"), function () {
            browser.driver.manage().window().setSize(768, 991);
            browser.waitForAngular();
        });
        it('Click the corresponding side menu options', function () {
            actions.fOHActions.clickLinkFromSideMenu(sideMenuIdex, sideSubMenuIndex);
        });
        it(sprintf("Verify the page url changes and page is shown", pageUrl), function () {
            expect(browser.driver.getCurrentUrl()).toContain(pageUrl);
            browser.driver.manage().window().maximize();
        });

    });
}

describe(specId + "MYC 1132 Selecting market and language", function () {
    //market  column 1
    sharedDescribe(2, 0, "/master/en/", -1, 0);
    sharedDescribe(2, 1, "/at/de/", -1, 1);
    sharedDescribe(2, 2, "/be/fr/", -1, 2);
    sharedDescribe(2, 3, "/be/nl/", -1, 3);
    sharedDescribe(2, 4, "/bg/en/", -1, 4);
    sharedDescribe(2, 5, "/hr/en/", -1, 5);
    sharedDescribe(2, 6, "/cy/en/", -1, 6);
    sharedDescribe(2, 7, "/cz/en/", -1, 7);
    sharedDescribe(2, 8, "/dk/en/", -1, 8);
    sharedDescribe(2, 9, "/dk/da/", -1, 9);

    sharedDescribe(2, 10, "/ee/en/", -1, 10);
    sharedDescribe(2, 11, "/fi/en/", -1, 11);
    sharedDescribe(2, 12, "/fr/fr/", -1, 12);
    sharedDescribe(2, 13, "/de/de/", -1, 13);
    sharedDescribe(2, 14, "/gb/en/", -1, 14);
    sharedDescribe(2, 15, "/gr/en/", -1, 15);
    sharedDescribe(2, 16, "/gr/el/", -1, 16);
    sharedDescribe(2, 17, "/hu/en/", -1, 17);
    sharedDescribe(2, 18, "/hu/hu/", -1, 18);
    sharedDescribe(2, 19, "/ie/en/", -1, 19);

    //market  column 3
    sharedDescribe(2, 20, "/it/it/", -1, 20);
    sharedDescribe(2, 21, "/lv/en/", -1, 21);
    sharedDescribe(2, 22, "/lt/en/", -1, 22);
    sharedDescribe(2, 23, "/lt/lt/", -1, 23);
    sharedDescribe(2, 24, "/mt/en/", -1, 24);
    sharedDescribe(2, 25, "/ma/fr/", -1, 25);
    sharedDescribe(2, 26, "/nl/nl/", -1, 26);
    sharedDescribe(2, 27, "/no/en/", -1, 27);
    sharedDescribe(2, 28, "/no/no/", -1, 28);
    sharedDescribe(2, 29, "/pl/pl/", -1, 29);

    //market  column 4
    sharedDescribe(2, 30, "/pt/pt/", -1, 30);
    sharedDescribe(2, 31, "/ro/en/", -1, 31);
    sharedDescribe(2, 32, "/ro/ro/", -1, 32);
    sharedDescribe(2, 33, "/sk/en/", -1, 33);
    sharedDescribe(2, 34, "/es/es/", -1, 34);
    sharedDescribe(2, 35, "/es/ca/", -1, 35);
    sharedDescribe(2, 36, "/se/en/", -1, 36);
    sharedDescribe(2, 37, "/se/sv/", -1, 37);
    sharedDescribe(2, 38, "/us/en/", -1, 38);

});
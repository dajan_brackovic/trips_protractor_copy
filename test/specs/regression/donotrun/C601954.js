var Trip = require('../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;


function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
        var outBoundDaysFromNow = 14; //today index === 1
        var origin = "stn";
        var destination = "dub";
        var fareType = "standard";
        var tripWay = "oneway";

        var bookFlight = function (paxMap, done) {
            actions.userActions.signUp().then(function(user){
                actions.userActions.activate(user).then(function(inf){
                    reporter.addMessageToSpec('New user registered, email = [' + user.email + ']; password = [' + user.password + '];');
                    var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
                    actions.fOHActions.goToPage();
                    actions.fOHActions.login(user.email, user.password);
                    actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
                    actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
                    actions.extrasActions.skipExtras();
                    actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
                    actions.addPaxActions.clickBtnAddPaxSave();
                    actions.addPaxActions.addContactForLoggedUser();
                    actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
                    done();
                });
            });
        };

        xdescribe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

            it(sprintf('Given I make a standard oneWay trip with 7 < outbound < 30days from now' +
                       ' with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function (done) {
                // TODO MAKE BOOKING TO SEATS
                var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
                bookFlight(paxMap, done);
            });

            it('Then I should get a booking ref', function () {
                // TODO expect booking ref
                actions.bookingSummaryActions.verifyConfirmationMessage();
            });

            it('When I do checkIN', function () {
                // TODO Make a Simple Check IN
                actions.checkInActions.todoSimpleCeckIn();
            });

            it('Then I verify check IN', function () {
                // TODO expect check IN
                actions.checkInActions.verifySimpleCheckIn();
            });
        });

        afterAll(function(){
            actions.fOHActions.logout();
        });
}

xdescribe('MYFR | Simple Booking | C601954 | 7 < outbound < 30 | Standard', function () {

    sharedDescribe(1, 0, 0, 0);

    sharedDescribe(1, 1, 0, 1);

    sharedDescribe(1, 0, 1, 1);

    sharedDescribe(4, 3, 0, 0);

});

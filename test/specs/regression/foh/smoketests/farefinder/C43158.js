var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "C43158"

function sharedDescribe(from, to, budgetAmount,flyOut, tripLength) {
    describe(sprintf("Ensure the user can choose a fly out date using all of the possible options (%s)", flyOut), function () {
        it('Given Im on the fare finder results page', function () {
            actions.fareFinderResultsActions.goToPage();
        });

        it(sprintf("When I search for Fare finder flight from %s to %s with budget %s flyOut %s and length %s", from, to, budgetAmount, flyOut, tripLength), function () {
            actions.fareFinderResultsActions.searchFareFinderFlightWithTripTime(from, to, budgetAmount, flyOut, tripLength);
        });

        it(sprintf("Verify fields %s to %s with budget %s flyOut %s and length %s are filled as expected", from, to, budgetAmount, flyOut, tripLength), function () {
            actions.fareFinderResultsActions.verifyAllFareFinderSearchResults(from, to, budgetAmount, flyOut, tripLength);
        });

        it(sprintf("Then I should get list of flights on fare finder results page under %s", budgetAmount), function () {
            actions.fareFinderResultsActions.verifyListOfFlightsHaveValuesAndAreUnderBudget(budgetAmount);
        });
    });
}

describe(specId + " | FareFinder Initial Search | Ensure the user can choose a fly out date using all of the possible options", function () {
    sharedDescribe("Fuerteventura", "Madrid", "50", "specificDate", "8-11");
    sharedDescribe("Fuerteventura", "Madrid", "50", "dateRange", "oneWay");
    sharedDescribe("Fuerteventura", "Madrid", "50", "entireMonth", "15-21");
    sharedDescribe("Fuerteventura", "Madrid", "50", "Anytime", "Anytime");
});



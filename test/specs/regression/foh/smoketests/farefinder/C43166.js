var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "C43166"

function sharedDescribe(from, to, budgetAmount, flightIndex, flyOut, tripLength) {
    describe("Ensure that the continue button brings the customer to the booking page", function () {
        it('Given Im on the fare finder results page', function () {
            actions.fareFinderResultsActions.goToPage();
        });

        it(sprintf("When I search for Fare finder flight from %s to %s with budget %s flyOut %s and length %s", from, to, budgetAmount, flyOut, tripLength), function () {
            actions.fareFinderResultsActions.searchFareFinderFlightWithTripTime(from, to, budgetAmount, flyOut, tripLength);
        });

        it(sprintf("Verify fields %s to %s with budget %s flyOut %s and length %s are filled as expected", from, to, budgetAmount, flyOut, tripLength), function () {
            actions.fareFinderResultsActions.verifyAllFareFinderSearchResults(from, to, budgetAmount, flyOut, tripLength);
        });

        it(sprintf("Select %s flight from results",flightIndex), function () {
            actions.fareFinderResultsActions.clickFlightResultByIndex(flightIndex);
        });

        it('Press the continue button in the top right corner of the page', function () {
            actions.fareFinderResultsDetailsActions.clickContinueButton();
        });

        it('Verify  flight booking page is approached', function () {
            actions.fOHActions.verifyPageOpened("booking/home");
        });

    });
}

describe(specId + " | FareFinder Initial Search | Ensure that the continue button brings the customer to the booking page", function () {
    sharedDescribe("Alicante", "Anywhere", "50", "1", "next3Months", "8-11");
});

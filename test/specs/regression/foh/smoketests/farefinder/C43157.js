var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "C43157"

function sharedDescribe(from, to, budgetAmount,flyOut, tripLength) {
    describe(sprintf("Ensure the user can filter the budget using a specific budget, in expanded view %s - %s", from, to, budgetAmount), function () {
        it('Given Im on the fare finder results page', function () {
            actions.fareFinderResultsActions.goToPage();
        });

        it(sprintf("When I search for Fare finder flight from %s to %s with budget %s flyOut %s and length %s", from, to, budgetAmount, flyOut, tripLength), function () {
            actions.fareFinderResultsActions.searchFareFinderFlightWithTripTime(from, to, budgetAmount, flyOut, tripLength);
        });

        it(sprintf("Verify fields %s to %s with budget %s flyOut %s and length %s are filled as expected", from, to, budgetAmount, flyOut, tripLength), function () {
            actions.fareFinderResultsActions.verifyAllFareFinderSearchResults(from, to, budgetAmount, flyOut, tripLength);
        });

        it(sprintf("Then I should get list of flights on fare finder results page under %s", budgetAmount), function () {
            actions.fareFinderResultsActions.verifyListOfFlightsHaveValuesAndAreUnderBudget(budgetAmount);
        });
    });
}

describe(specId + " | FareFinder Initial Search | Ensure the user can filter the budget using a specific budget, in expanded view", function () {
    sharedDescribe("Birmingham", "Dublin", "240", "specificDate", "8-11");
});



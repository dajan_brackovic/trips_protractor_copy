var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "C43161"

function sharedDescribe(from, to, budgetAmount, flyOut, tripLength, filter) {
    describe('Ensure that the results can be filtered using the trip types options', function () {
        it('Given Im on the fare finder results page', function () {
            actions.fareFinderResultsActions.goToPage();
        });

        it(sprintf("When I search for Fare finder flight from %s to %s with budget %s flyOut %s and length %s", from, to, budgetAmount, flyOut, tripLength), function () {
            actions.fareFinderResultsActions.searchFareFinderFlightWithTripTime(from, to, budgetAmount, flyOut, tripLength);
        });

        it(sprintf("Verify fields %s to %s with budget %s flyOut %s and length %s are filled as expected", from, to, budgetAmount, flyOut, tripLength), function () {
            actions.fareFinderResultsActions.verifyAllFareFinderSearchResults(from, to, budgetAmount, flyOut, tripLength);
        });

        it(sprintf("And I set Fare finder filter %s", filter), function () {
            actions.fareFinderResultsActions.setSearchFilter(filter);
        });

        it(sprintf("Then I should get list of flights on fare finder results page under %s", budgetAmount), function () {
            actions.fareFinderResultsActions.verifyListOfFlightsHaveValuesAndAreUnderBudget(budgetAmount);
        });
    });
}

describe(specId + " | FareFinder Initial Search | Ensure that the results can be filtered using the trip types options", function () {
    sharedDescribe("Birmingham", "Anywhere", "155", "Anytime", "oneWay", "City Break");
    sharedDescribe("Birmingham", "Anywhere", "155", "Anytime", "oneWay", "Family");
    sharedDescribe("Birmingham", "Anywhere", "155", "Anytime", "oneWay", "Nightlife");
    sharedDescribe("Birmingham", "Anywhere", "155", "Anytime", "oneWay", "Beach");
    sharedDescribe("Birmingham", "Anywhere", "155", "Anytime", "oneWay", "Outdoor");
    sharedDescribe("Birmingham", "Anywhere", "155", "Anytime", "oneWay", "Golf");
});



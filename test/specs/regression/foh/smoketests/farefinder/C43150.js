var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "C43150"

function sharedDescribe(from, to, budgetAmount) {
    describe(sprintf("Ensure the user can search for flights to/from specific airport, region, country %s - %s in compact view", from, to, budgetAmount), function () {
        it('Given Im on the fare finder page', function () {
            actions.fareFinderActions.goToPage();
        });

        it(sprintf("When I search for Fare finder flight from %s to %s with budget %s", from, to, budgetAmount), function () {
            actions.fareFinderActions.searchFareFinderFlight(from, to, budgetAmount);
        });

        it(sprintf("And I should get %s to %s with %s on results page", from, to, budgetAmount), function () {
            actions.fareFinderResultsActions.verifyFareFinderSearchResults(from, to, budgetAmount)
        });

        it(sprintf("When the user presses Lets Go, the lowest fares to the selected airport/region/country are displayed for %s", to), function () {
            actions.fareFinderResultsActions.verifyLowestFareAirport(to);
        });
    });
}

describe(specId + " | FareFinder Initial Search | Ensure the user can search for flights to/from specific airport, region, country in compact view", function () {
    sharedDescribe("Dublin", "London (STN)", "50");
});

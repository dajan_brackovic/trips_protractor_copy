var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "C43151"

function sharedDescribe(from, to, budgetAmount) {
    describe(sprintf("Ensure the user can search for flight from a specific airport to anywhere %s - %s in compact view", from, to, budgetAmount), function () {
        it('Given Im on the fare finder page', function () {
            actions.fareFinderActions.goToPage();
        });

        it(sprintf("When I search for Fare finder flight from %s to %s with budget %s", from, to, budgetAmount), function () {
            actions.fareFinderActions.searchFareFinderFlight(from, to, budgetAmount);
        });

        it(sprintf("And I should get %s to %s with %s on results page", from, to, budgetAmount), function () {
            actions.fareFinderResultsActions.verifyFareFinderSearchResults(from, to, budgetAmount)
        });

        it(sprintf("Then I should get list of flights on fare finder results page under %s", budgetAmount), function () {
            actions.fareFinderResultsActions.verifyListOfFlightsHaveValuesAndAreUnderBudget(budgetAmount);
        });
    });
}

describe(specId + " | FareFinder Initial Search | Ensure the user can search for flight from a specific airport to anywhere in compact view", function () {
    sharedDescribe("Barcelona", "Anywhere", "50");
});


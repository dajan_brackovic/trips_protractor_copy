var sprintf = require("sprintf").sprintf;

//var specId ="FOH | "+ "C43164+C43163"
var specId ="FOH | "+ "C43164";

function sharedDescribe(from, to, budgetAmount,flyOut, tripLength) {
    describe('Ensure that user can switch between chart view and calendar view (view by month)', function () {
        it('Given Im on the fare finder results page', function () {
            actions.fareFinderResultsActions.goToPage();
        });

        it(sprintf("When I search for Fare finder flight from %s to %s with budget %s flyOut %s and length %s", from, to, budgetAmount, flyOut, tripLength), function () {
            actions.fareFinderResultsActions.searchFareFinderFlightWithTripTime(from, to, budgetAmount, flyOut, tripLength);
        });

        it(sprintf("Verify fields %s to %s with budget %s flyOut %s and length %s are filled as expected", from, to, budgetAmount, flyOut, tripLength), function () {
            actions.fareFinderResultsActions.verifyAllFareFinderSearchResults(from, to, budgetAmount, flyOut, tripLength);
        });

        it("Select first flight from results", function () {
            actions.fareFinderResultsActions.clickFlightResultByIndex("1");
        });

        it("Verify alternate between the chart view and view by month options", function () {
            actions.fareFinderResultsDetailsActions.verifyAlternateBetweenViews();
        });
    });
}

describe(specId + " | FareFinder Initial Search | Ensure that user can switch between chart view and calendar view (view by month)", function () {
    sharedDescribe("Dublin", "Anywhere", "40", "Anytime", "oneWay");
    sharedDescribe("Bordeaux", "Anywhere", "40", "Anytime", "oneWay");


});



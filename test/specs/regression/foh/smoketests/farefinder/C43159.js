var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "C43159"

function sharedDescribe(from, to, budgetAmount,flyOut, tripLength) {
    describe('Ensure that the user can fill the fly back field using either by choosing next three months option, the trip length option, or choosing one way (no return)', function () {
        it('Given Im on the fare finder results page', function () {
            actions.fareFinderResultsActions.goToPage();
        });

        it(sprintf("When I search for Fare finder flight from %s to %s with budget %s flyOut %s and length %s", from, to, budgetAmount, flyOut, tripLength), function () {
            actions.fareFinderResultsActions.searchFareFinderFlightWithTripTime(from, to, budgetAmount, flyOut, tripLength);
        });

        it(sprintf("Verify fields %s to %s with budget %s flyOut %s and length %s are filled as expected", from, to, budgetAmount, flyOut, tripLength), function () {
            actions.fareFinderResultsActions.verifyAllFareFinderSearchResults(from, to, budgetAmount, flyOut, tripLength);
        });

        it(sprintf("Then I should get list of flights on fare finder results page under %s", budgetAmount), function () {
            actions.fareFinderResultsActions.verifyListOfFlightsHaveValuesAndAreUnderBudget(budgetAmount);
        });
    });
}

describe(specId + " | FareFinder Initial Search | Ensure that the user can fill the fly back field using either by choosing next three months option, the trip length option, or choosing one way (no return)", function () {
    sharedDescribe("Cork", "Cork", "50", "next3Months", "8-11");
    sharedDescribe("Cork", "Cork", "50", "next3Months", "oneWay");
});



var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "C43155"

function sharedDescribe(from, to, budgetAmount,flyOut, tripLength) {
    describe(sprintf("Ensure the user can search for flight from a specific airport to anywhere%s - %s in expanded view", from, to, budgetAmount), function () {
        it('Given Im on the fare finder results page', function () {
            actions.fareFinderResultsActions.goToPage();
        });

        it(sprintf("When I search for Fare finder flight from %s to %s with budget %s flyOut %s and length %s", from, to, budgetAmount, flyOut, tripLength), function () {
            actions.fareFinderResultsActions.searchFareFinderFlightWithTripTime(from, to, budgetAmount, flyOut, tripLength);
        });

        it(sprintf("Verify fields %s to %s with budget %s flyOut %s and length %s are filled as expected", from, to, budgetAmount, flyOut, tripLength), function () {
            actions.fareFinderResultsActions.verifyAllFareFinderSearchResults(from, to, budgetAmount, flyOut, tripLength);
        });

        it(sprintf("Then I should get list of flights on fare finder results page under %s", budgetAmount), function () {
            actions.fareFinderResultsActions.verifyListOfFlightsHaveValuesAndAreUnderBudget(budgetAmount);
        });
    });
}

describe(specId + " | FareFinder Initial Search | Ensure the user can search for flight from a specific airport to anywhere in expanded view", function () {
    sharedDescribe("Alicante", "Anywhere", "50", "next3Months", "oneWay");
    sharedDescribe("Alicante", "Anywhere", "50", "specificDate", "8-11");
    sharedDescribe("Alicante", "Anywhere", "50", "entireMonth", "8-11");
    sharedDescribe("Alicante", "Anywhere", "50", "dateRange", "oneWay");
    sharedDescribe("Alicante", "Anywhere", "50", "Anytime", "Anytime");
});



var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "C43165"

function sharedDescribe(from, to, budgetAmount, flightIndex, flyOut, tripLength) {
    describe('Ensure that user can switch between chart view and calendar view (view by month)', function () {
        it('Given Im on the fare finder results page', function () {
            actions.fareFinderResultsActions.goToPage();
        });

        it(sprintf("When I search for Fare finder flight from %s to %s with budget %s flyOut %s and length %s", from, to, budgetAmount, flyOut, tripLength), function () {
            actions.fareFinderResultsActions.searchFareFinderFlightWithTripTime(from, to, budgetAmount, flyOut, tripLength);
        });

        it(sprintf("Verify fields %s to %s with budget %s flyOut %s and length %s are filled as expected", from, to, budgetAmount, flyOut, tripLength), function () {
            actions.fareFinderResultsActions.verifyAllFareFinderSearchResults(from, to, budgetAmount, flyOut, tripLength);
        });

        it(sprintf("Select %s flight from results",flightIndex), function () {
            actions.fareFinderResultsActions.clickFlightResultByIndex(flightIndex);
        });

        it("I want to select outbound chart view", function () {
            actions.fareFinderResultsDetailsActions.selectChartViewOutbound();
        });

        it("I want to select second month in outbound chart view and check that it was selected", function () {
            actions.fareFinderResultsDetailsActions.verifyOutboundMonthSelection();
        });

    });
}

describe(specId + " | FareFinder Initial Search | Ensure that user can switch between chart view and calendar view (view by month)", function () {
    sharedDescribe("Dublin", "Anywhere", "50", "1", "next3Months", "8-11");
});



var sprintf = require("sprintf").sprintf;

//var specId ="FOH | "+ "C43162+C38864+C38865"
var specId ="FOH | "+ "C43162 ";

function sharedDescribe(from1, to1, budgetAmount1,from2, to2, budgetAmount2) {
    describe('Ensure that the recently viewed link displays recently viewed searches', function () {
        it('Given Im on the fare finder page', function () {
            actions.fareFinderActions.goToPage();
        });

        it(sprintf("When I search for Fare finder flight from %s to %s with budget %s", from1, to1, budgetAmount1), function () {
            actions.fareFinderActions.searchFareFinderFlight(from1, to1, budgetAmount1);
        });

        it(sprintf("And I should get %s to %s with %s on results page", from1, to1, budgetAmount1), function () {
            actions.fareFinderResultsActions.verifyFareFinderSearchResults(from1, to1, budgetAmount1)
        });

        it("Select first flight from results", function () {
            actions.fareFinderResultsActions.clickFlightResultByIndex("1");
        });

        it('Given Im on the fare finder page', function () {
            actions.fareFinderActions.goToPage();
        });

        it(sprintf("When I search for Fare finder flight from %s to %s with budget %s", from2, to2, budgetAmount2), function () {
            actions.fareFinderActions.searchFareFinderFlight(from2, to2, budgetAmount2);
        });

        it(sprintf("And I should get %s to %s with %s on results page", from2, to2, budgetAmount2), function () {
            actions.fareFinderResultsActions.verifyFareFinderSearchResults(from2, to2, budgetAmount2)
        });

        it(sprintf("Then I should check when recently viewed is selected, the previous search from %s to %s should be displayed", from1, to1), function () {
            actions.fareFinderResultsActions.verifyRecentlyViewedResults(from1,to1);
        });

        it('Delete one from history', function () {
            actions.fareFinderResultsActions.deleteLastRecentViewItem();
        });
    });
}

describe(specId + " | FareFinder Initial Search | Ensure that the recently viewed link displays recently viewed searches", function () {
    sharedDescribe("Dublin", "Birmingham", "40", "Alicante", "Liverpool", "40");
});



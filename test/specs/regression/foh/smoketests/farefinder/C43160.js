var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "C43160"

function sharedDescribe(from, to, budgetAmount,flyOut, tripLength) {
    describe('Ensure that user can switch between the different views, when looking at results (list view, tile view, map view)', function () {
        it('Given Im on the fare finder results page', function () {
            actions.fareFinderResultsActions.goToPage();
        });

        it(sprintf("When I search for Fare finder flight from %s to %s with budget %s flyOut %s and length %s", from, to, budgetAmount, flyOut, tripLength), function () {
            actions.fareFinderResultsActions.searchFareFinderFlightWithTripTime(from, to, budgetAmount, flyOut, tripLength);
        });

        it(sprintf("Verify fields %s to %s with budget %s flyOut %s and length %s are filled as expected", from, to, budgetAmount, flyOut, tripLength), function () {
            actions.fareFinderResultsActions.verifyAllFareFinderSearchResults(from, to, budgetAmount, flyOut, tripLength);
        });

        it(sprintf("Then I should get list of flights on fare finder results page under %s", budgetAmount), function () {
            actions.fareFinderResultsActions.verifyListOfFlightsHaveValuesAndAreUnderBudget(budgetAmount);
        });

        it("Then I should select Map View and results are displayed", function () {
            actions.fareFinderResultsActions.selectView("Map");
            actions.fareFinderResultsActions.verifyViewSelected("Map");
        });

        it("Then I should select Tile View  results are displayed", function () {
            actions.fareFinderResultsActions.selectView("Tile");
            actions.fareFinderResultsActions.verifyViewSelected("Tile");
        });

        it("Then I should select List View  results are displayed", function () {
            actions.fareFinderResultsActions.selectView("List");
            actions.fareFinderResultsActions.verifyViewSelected("List");
        });
    });
}

describe(specId + " | FareFinder Initial Search | Ensure that user can switch between the different views, when looking at results (list view, tile view, map view)", function () {
    sharedDescribe("Barcelona", "Seville", "50", "next3Months", "oneWay");
});



var sprintf = require("sprintf").sprintf;
//var specId ="FOH | "+ "C49997+C80072"
var specId ="FOH | "+ "C49997";

function sharedDescribe(from, to, currency) {
    describe(sprintf(" %s to %s with currency %s", from, to, currency), function () {
        it('Given Im on map page', function () {
            actions.mapHomeActions.goToPage();
        });
        it(sprintf("When I search for flight from %s to %s", from, to), function () {
            actions.mapHomeActions.searchFlightWithBothAirportsSpecified(from, to);
        });
        it(sprintf("Book flight from map view, verify that in farefinder results page", from, to, currency), function () {
            actions.mapHomeActions.bookFlightFromMapViewVerifyInFfResultsDetailsPage(from, to, currency);
        });

    });

}

describe(specId + " | Book from map view after search flight with departure and destination airport both selected,from, to, currency", function () {
    sharedDescribe("Dublin", "Alicante", "\u20ac");

});

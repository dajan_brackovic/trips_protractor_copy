var sprintf = require("sprintf").sprintf;
//var specId ="FOH | "+ "C49995+C49998"
var specId ="FOH | "+ "C49995";

function sharedDescribe(from, to, currency) {
    describe(sprintf(" %s to %s with budget %s", from, to, currency), function () {
        it('Given Im on map page', function () {
            actions.mapHomeActions.goToPage();
        });
        it(sprintf("When I search for flight from %s to %s", from, to), function () {
            actions.mapHomeActions.searchFlightWithBothAirportsSpecified(from, to);
        });

        it(sprintf("Switch airports in search field"), function () {
            actions.mapHomeActions.switchSearchFieldsValues();
        });
        it(sprintf("Book flight from map view, verify that in farefinder results page", from, to, currency), function () {
            actions.mapHomeActions.bookFlightFromMapViewVerifyInFfResultsDetailsPage(to, from, currency);
        });

    });

}

describe(specId + " | Book from map view after search flight with departure and destination airport both selected,from, to, currency", function () {
    sharedDescribe("Dublin", "Alicante", "\u20ac");


});

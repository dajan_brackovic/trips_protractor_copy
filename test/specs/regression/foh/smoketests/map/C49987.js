var sprintf = require("sprintf").sprintf;
//var specId ="FOH | "+ "C49987+C49988+C49995+C49996+C50003"
var specId ="FOH | "+ "C49987"

function sharedDescribe(from, to, currency) {
    describe(sprintf(" %s to %s with budget %s", from, to, currency), function () {
        it('Given Im on map page', function () {
            actions.mapHomeActions.goToPage();
        });
        it(sprintf("When I search for flight from %s to %s", from, to), function () {
            actions.mapHomeActions.searchFlightWithBothAirportsSpecified(from, to);
        });

        it(sprintf("Book flight from map view, verify that in farefinder results page", from, to, currency), function () {
            actions.mapHomeActions.bookFlightFromChartViewVerifyInFfResultsDetailsPage(from, to, currency);
        });

    });

}

describe(specId + " | Book from chart view after search flights with departure and destination airport both selected ,from, to, currency(***Return when bug DF2372 fixed***)", function () {
    sharedDescribe("Bremen", "Madrid", "\u20ac");
    sharedDescribe("London (STN)", "barcelona", "\u00A3");
    //TODO:
    //The left hand fare finder Destination list is updated to display the destination mini card for selected flight BGY-NYO
    //The map is updated: the pane from BGY to NYO and tool-tip with price and Book CTA is displayed
});

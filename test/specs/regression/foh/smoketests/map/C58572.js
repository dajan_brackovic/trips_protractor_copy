var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "C58572"

function sharedDescribe(from, to, budgetAmount, flyOut, tripLength) {
    describe(sprintf("Search from farefinder and view results and book flight from map view %s %s %s %s %s", from, to, budgetAmount, flyOut, tripLength), function () {
        it('Given Im on the fare finder page', function () {
            actions.fareFinderActions.goToPage();
        });

        it(sprintf("Search by default"), function () {
            actions.fareFinderActions.goDefaultSearch();
        });

        it(sprintf("Select map view and verify the default selection on map"), function () {
            actions.fareFinderResultsActions.selectMapViewVerifyTheSelectionDefault();
        });

        it(sprintf("Change budget to be smaller, verify the results number is reduced", budgetAmount), function () {
            actions.fareFinderResultsActions.changeToSmallerBudgetAndVerifyTheChangeOfResults(budgetAmount);
        });

    });
}

describe(specId + " | Search from farefinder and view results and book flight from map view", function () {
    sharedDescribe("null", "null", "27", "null", "null");


});



var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "C58574"

function sharedDescribe(from, to, budgetAmount, flyOut, tripLength) {
    describe(sprintf("Search from farefinder and view results and book flight from map view %s %s %s %s %s", from, to, budgetAmount, flyOut, tripLength), function () {
        it('Given Im on the fare finder page', function () {
            actions.fareFinderActions.goToPage();
        });

        it(sprintf("Search by default"), function () {
            actions.fareFinderActions.goDefaultSearch();
        });

        it(sprintf("Select map view and verify the default selection on map"), function () {
            actions.fareFinderResultsActions.selectMapViewVerifyTheSelectionDefault();
        });

        it(sprintf("Change the flyout to be %s and trip length to be %s, verify the results number is reduced", flyOut,tripLength), function () {
            actions.fareFinderResultsActions.changeFlyOutFlyBackVerifyTheChangeOfResults(flyOut,tripLength);
        });

    });
}

describe(specId + " | Search from farefinder and view results and book flight from map view", function () {
    sharedDescribe("null", "null", "null", "entireMonth", "8-11");
});



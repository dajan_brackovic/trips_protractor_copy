var sprintf = require("sprintf").sprintf;
//var specId ="FOH | "+ "C58567+C58569+C58570+C58571"
var specId ="FOH | "+ "C58567"

function sharedDescribe(toCode, toCode2) {
    describe(sprintf("Click destination  code %s on map",  toCode), function () {
        it('Given Im on the fare finder page', function () {
            actions.fareFinderActions.goToPage();
        });

        it(sprintf("Search by default"), function () {
            actions.fareFinderActions.goDefaultSearch();
        });

        it(sprintf("Select map view and verify the default selection on map"), function () {
            actions.fareFinderResultsActions.selectMapViewVerifyTheSelectionDefault();
        });

        it(sprintf("Click any destination airport", toCode), function () {
            actions.fareFinderResultsActions.clickAnyDestinationOnMap(toCode);
        });

        it(sprintf("Click any destination airport", toCode2), function () {
            actions.fareFinderResultsActions.clickAnyDestinationOnMap(toCode2);
        });
        it(sprintf("Book from the map view"), function () {
            actions.fareFinderResultsActions.verifySelectionThenBookFromMap();
        });

    });
}

describe(specId + " | Select destination from map view", function () {
    sharedDescribe("BRS", "BVA");

});

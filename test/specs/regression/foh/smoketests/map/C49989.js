var sprintf = require("sprintf").sprintf;
//var specId ="FOH | "+ "C49989+C49990+C49991+C49992"
var specId ="FOH | "+ "C49989"


function sharedDescribe(from, month, budget, type, currency) {
    describe(sprintf("Search flight from %s in the %sth available month and budget %s", from, month, budget), function () {
        it('Given Im on map page', function () {
            actions.mapHomeActions.goToPage();
        });

        it(sprintf("When I search flight from %s in the %sth available month and budget %s and trip type %s", from, month, budget, type), function () {
            actions.mapHomeActions.searchFlightsWithMoreFilters(from, month, budget, type);
        });
        it(sprintf("Verify the search results are correctly shown in terms of budget %s and currency type %s", budget, currency), function () {
            actions.mapHomeActions.verifyResultsListUnderBudget(budget, currency);
        });

    });

}

describe(specId + " | Search flights with more filters, from, month, budget, type, currency", function () {
    sharedDescribe("Dublin", "1", "44", "0", "1", "\u20ac");

});

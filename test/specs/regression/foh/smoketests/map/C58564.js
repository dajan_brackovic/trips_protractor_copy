var sprintf = require("sprintf").sprintf;
//var specId ="FOH | "+ "C58564+C58565+C58568+C58576"
var specId ="FOH | "+ "C58564";

function sharedDescribe(from, to, budgetAmount, flyOut, tripLength) {
    describe(sprintf("Search from farefinder and view results and book flight from map view %s %s %s %s %s",from, to, budgetAmount, flyOut, tripLength), function () {
        it('Given Im on the fare finder page', function () {
            actions.fareFinderActions.goToPage();
        });

        it(sprintf("Search by default"), function () {
            actions.fareFinderActions.goDefaultSearch();
        });

        it(sprintf("Select map view and verify the default selection on map"), function () {
            actions.fareFinderResultsActions.selectMapViewVerifyTheSelectionDefault();
        });

        it(sprintf("Then I search for Fare finder flight from %s to %s with budget %s flyOut %s and length %s", from, to, budgetAmount, flyOut, tripLength), function () {
            actions.fareFinderResultsActions.searchFareFinderFlightWithTripTime(from, to, budgetAmount, flyOut, tripLength);
        });

        it(sprintf("Select map view and verify the selected airports on map"), function () {
            actions.fareFinderResultsActions.verifyTheSelection();

        });

        it(sprintf("Book from the map view"), function () {
            actions.fareFinderResultsActions.bookFlightFromMapViewVerifyInFfResultsDetailsPage(from, to);
        });

    });
}

describe(specId + " | Search from farefinder and view results and book flight from map view", function () {
    sharedDescribe("London (STN)", "Dublin", "150", "specificDate", "8-11");
    sharedDescribe("London (STN)", "Dublin", "50", "dateRange", "oneWay");


});



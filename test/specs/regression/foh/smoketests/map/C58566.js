var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "C58566"

function sharedDescribe(toairport, toCode) {
    describe(sprintf("Click destination %s code %s on map", toairport, toCode), function () {
        it('Given Im on the fare finder page', function () {
            actions.fareFinderActions.goToPage();
        });

        it(sprintf("Search by default"), function () {
            actions.fareFinderActions.goDefaultSearch();
        });

        it(sprintf("Select map view and verify the default selection on map"), function () {
            actions.fareFinderResultsActions.selectMapViewVerifyTheSelectionDefault();
        });

        it(sprintf("Click any destination airport", toCode), function () {
            actions.fareFinderResultsActions.clickAnyDestinationOnMap(toCode);
        });
        it(sprintf("Book from the map view"), function () {
            actions.fareFinderResultsActions.verifySelectionThenBookFromMap();
        });

    });
}

describe(specId + " | Select destination from map view", function () {
    sharedDescribe("Bristol", "BRS");

});

var sprintf = require("sprintf").sprintf;
var specId = "FOH | " + "C50002"


function sharedDescribe(from, to, currency) {
    describe(sprintf(" %s to %s with budget %s", from, to, currency), function () {
        it('Given Im on map page', function () {
            actions.mapHomeActions.goToPage();
        });
        it(sprintf("When I search for flight from %s to %s", from, to), function () {
            actions.mapHomeActions.searchFlightWithBothAirportsSpecified(from, to);
        });
        it('Select List View then select back map view', function () {
            actions.mapHomeActions.switchBetweenListAndMapView();
        });

        it(sprintf("Book flight from map view, verify that in farefinder results page", from, to, currency), function () {
            actions.mapHomeActions.bookFlightFromChartViewVerifyInFfResultsDetailsPage(from, to, currency);
        });

    });

}

describe(specId + " | Book from chart view after search flights with departure and destination airport both selected ,from, to, currency", function () {
    sharedDescribe("Dublin", "London (STN)", "\u20ac");
    sharedDescribe("London (STN)", "Barcelona", "\u00A3");
});

var sprintf = require("sprintf").sprintf;
// /var specId ="FOH | "+ "C49993+C49990+C49991+C49992"
var specId ="FOH | "+ "C49993"

function sharedDescribe(from, month, budget, type, currency) {
    describe(sprintf("Search flight from %s in the %sth available month and budget %s", from, month, budget), function () {
        it('Given Im on map page', function () {
            actions.mapHomeActions.goToPage();
        });
        it(sprintf("Set filter then clear filter", from, month, budget, type), function () {
            actions.mapHomeActions.setThenClearFilter(from, month, budget, type);
        });

    });

}

describe(specId + " | Search flights with more filters, from, month, budget, type, currency", function () {
    sharedDescribe("Rome (CIA)", "1", "10", "0", "1", "\u20ac");

});

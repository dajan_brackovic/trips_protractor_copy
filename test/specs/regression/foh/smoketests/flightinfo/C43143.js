var sprintf = require("sprintf").sprintf;
var specId ="FOH | "+ "C43143 ";


function sharedDescribe(from, to, fromAirport) {
    describe(sprintf("Search flights by entering from [%s] to [%s]in flight info page and get the results for fromAirport [%s] to all airports ", from, to, fromAirport), function () {
        it('Given I on FOH flights page', function () {
            actions.flightInfoByRouteActions.goToPage();
        });

        it(sprintf("Search flights from departure airport [%s] to destination airport[%s]", from, to), function () {
            actions.flightInfoByRouteActions.searchFlight(from, to);
            actions.flightInfoByRouteActions.submitSearch();
        });

        it(sprintf("Verify the search results show correct departure airport", from, to, fromAirport), function () {
            actions.flightInfoByRouteActions.verifyResultsDepartureAirports(from, to, fromAirport);
        });

    });

}

describe(specId + " | Verify flight info search by route, from, to, fromAirport, toAirport", function () {
    sharedDescribe("ALC", "All Airports", "Alicante");

});










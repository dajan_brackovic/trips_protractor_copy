var sprintf = require("sprintf").sprintf;
//var specId ="FOH | "+ "C43146 C43137 C43148"
var specId ="FOH | "+ "C43146 ";

function sharedDescribe(from, to) {
    describe(sprintf("Search flights by entering from [%s] to [%s]in flight info page and get the results with fillter %s", from, to), function () {
        it('Given I on FOH flights page', function () {
            actions.flightInfoByRouteActions.goToPage();
        });

        it(sprintf("Search flights from departure airport [%s] to destination airport[%s]", from, to), function () {
            actions.flightInfoByRouteActions.searchFlight(from, to);
            actions.flightInfoByRouteActions.submitSearch();
        });

        it(sprintf("Click later flights, verify the page changes and hte flights changes"), function () {
            actions.flightInfoByRouteActions.verifyPagination();
        });
    });

}

describe(specId + "| Verify date header, from, to", function () {
    sharedDescribe("DUB", "All Airports");

});










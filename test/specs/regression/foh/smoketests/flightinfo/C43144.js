var sprintf = require("sprintf").sprintf;
//var specId ="FOH | "+ "C43144+C39393"
var specId ="FOH | "+ "C43144 ";

function sharedDescribe(from, to, filter) {
    describe(sprintf("Search flights by entering from [%s] to [%s]in flight info page and get the results with fillter %s", from, to, filter), function () {
        it('Given I on FOH flights page', function () {
            actions.flightInfoByRouteActions.goToPage();
        });

        it(sprintf("Search flights from departure airport [%s] to destination airport[%s]", from, to), function () {
            actions.flightInfoByRouteActions.searchFlight(from, to);
            actions.flightInfoByRouteActions.submitSearch();
        });

        it(sprintf("Change the filter to be %s", filter), function () {
            actions.flightInfoByRouteActions.changeFilterThenVerifyTheChange(filter);
        });

        it(sprintf("Change the filter to be"), function () {
            actions.flightInfoByRouteActions.changeFilterSameTotalFlights();
        });


    });

}

describe(specId + " | Verify flight info search by route then adjust the filter to see the change, from, to, filter", function () {
    sharedDescribe("ALC", "STN", "All Day");
 });










var sprintf = require("sprintf").sprintf;
var specId ="FOH | "+ "C43145 "
var today = new Date();

function sharedDescribe(from, to) {
    describe(sprintf("Search flights by entering from [%s] to [%s]in flight info page and get the results with fillter %s", from, to), function () {
        it('Given I on FOH flights page', function () {
            actions.flightInfoByRouteActions.goToPage();
        });

        it(sprintf("Search flights from departure airport [%s] to destination airport[%s]", from, to), function () {
            actions.flightInfoByRouteActions.searchFlight(from, to);
            actions.flightInfoByRouteActions.submitSearch();
        });

        it(sprintf("Change the filter to be"), function () {
            var dateString = today.toDateString();
            actions.flightInfoByRouteActions.checkDateOfHeader(dateString);
        });
    });

}

describe(specId + " | Verify date header, from, to", function () {
    sharedDescribe("ALC", "All Airports");

});










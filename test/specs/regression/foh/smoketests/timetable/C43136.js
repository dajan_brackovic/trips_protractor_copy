var sprintf = require("sprintf").sprintf;
var specId ="FOH | "+ "C43136 "

function sharedDescribe(from, to, column, currency) {
    describe(sprintf("Select flight from weekly view fly out calendar and verify in booking, [%s][%s][%s][%s]  ", from, to, column, currency), function () {
        it('Given I on FOH timetable page', function () {
            actions.timetableActions.goToPage();
        });

        it(sprintf("Fill in from and to airports"), function () {
            actions.timetableActions.searchTimeTable(from, to);

        });

        it(sprintf("Attempt to select a past date"), function () {
            actions.timetableActions.selectDateBeforeToday();

        });

        it(sprintf("Verify nothing was actually  selected"), function () {
            actions.timetableActions.verifyNothingChangesInFareWidgetDates();
        });

    });

}

describe(specId + '| Select flight from weekly view fly back calendar and verify in booking, from, to, column, currency ', function () {
    sharedDescribe("Dublin", "Alicante", "3", "EUR");

});
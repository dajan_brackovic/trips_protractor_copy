var sprintf = require("sprintf").sprintf;
var specId ="FOH | "+ "C43135 ";

function sharedDescribe(from, to, column, currency) {
    describe(sprintf("Select flight from weekly view fly out calendar and verify in booking, [%s][%s][%s][%s]  ", from, to, column, currency), function () {
        it('Given I on FOH timetable page', function () {
            actions.timetableActions.goToPage();
        });

        it(sprintf("Fill in from and to airports"), function () {
            actions.timetableActions.searchTimeTable(from, to);

        });

        it(sprintf("Click on weekly view in fly out section"), function () {
            actions.timetableActions.clickWeeklyViewButtonFlyback();

        });

        it(sprintf("Select next week in fly back calendar"), function () {
            actions.timetableActions.selectNextWeekInFlyback();

        });

        it(sprintf("Click on weekly view in fly out section"), function () {
            actions.timetableActions.clickMonthlyViewButtonFlyback();

        });

        it(sprintf("Select next week in fly back calendar"), function () {
            actions.timetableActions.selectNextMonthInFlyback();

        });

        it(sprintf("Click on weekly view in fly out section"), function () {
            actions.timetableActions.clickWeeklyViewButtonFlyback();

        });

        it(sprintf("Select a valid day then verify the data including currency"), function () {
            actions.timetableActions.selectFlyBackWeeklyCalendarVerifyInBookingDetails(from, to, column, currency);

        });

    });

}

describe(specId + ' | Select flight from weekly view fly back calendar and verify in booking, from, to, column, currency ', function () {
    sharedDescribe("Dublin", "Alicante", "4", "EUR");

});
var sprintf = require("sprintf").sprintf;
var specId ="FOH | "+ "C43141 "

function sharedDescribe(from, to, monthIndex,dayIndex, currency) {
    describe(sprintf("Select a valid fare from fly back monthly calendar  from %s to %s, using month index %s and  day index  %s", from, to, monthIndex, currency), function () {
        it('Given I on FOH timetable page', function () {
            actions.timetableActions.goToPage();
        });

        it(sprintf("Fill in from and to airports and search"), function () {
            actions.timetableActions.searchTimeTable(from, to);
        });

        it(sprintf("Select next 5 months without clicking the right arrow"), function () {
            for (var count = 0; count < 5; count++) {
                actions.timetableActions.selectNextMonthInFlyout();
            }
        });

        it(sprintf("Clicking the right arrow "), function () {
            for (var count = 0; count < 1; count++) {
                actions.timetableActions.clickRightArrowOutbound();
            }
        });
        it(sprintf("Clicking the right arrow "), function () {
            for (var count = 0; count < 1; count++) {
                actions.timetableActions.clickLeftArrowOutbound();
            }
        });

        it(sprintf("Verify booking details correctly shown"), function () {
            actions.timetableActions.selectFlyOutMonthlyCalendarVerifyInBookingDetails(from, to, monthIndex, dayIndex, currency);
        });
    });

}

describe(specId + ' | Select flight from monthly view fly back calendar and verify in booking, from, to, monthIndex, dayIndex,currency', function () {
    sharedDescribe("Dublin", "Alicante", "2", "2", "EUR");

});
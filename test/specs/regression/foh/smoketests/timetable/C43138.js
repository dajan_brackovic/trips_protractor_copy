var sprintf = require("sprintf").sprintf;
var specId ="FOH | "+ "C43138 ";

function sharedDescribe(from, to, column, currency) {
    describe(sprintf("Select flight from weekly view fly out calendar and verify in booking, [%s][%s][%s][%s]  ", from, to, column, currency), function () {
        it('Given I on FOH timetable page', function () {
            actions.timetableActions.goToPage();
        });

        it(sprintf("Fill in from and to airports and search"), function () {
            actions.timetableActions.searchTimeTable(from, to);

        });
        it(sprintf("Click on weekly view in fly out section "), function () {
            actions.timetableActions.clickWeeklyViewButtonFlyout();

        });
        it(sprintf("Select next week in flyout section"), function () {
            actions.timetableActions.selectNextWeekInFlyout();

        });
        it(sprintf("Select a valid day then verify the data including currency"), function () {
            actions.timetableActions.selectFlyOutWeeklyCalendarVerifyInBookingDetails(from, to, column, currency);

        });

    });

}

describe(specId + ' | Select flight from weekly view fly out view and verify in booking, from, to, column, currency ', function () {
    sharedDescribe("Dublin", "Alicante", "2", "EUR");

});
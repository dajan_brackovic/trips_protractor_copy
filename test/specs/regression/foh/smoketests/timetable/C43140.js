var sprintf = require("sprintf").sprintf;
var specId ="FOH | "+ "C43140 ";

function sharedDescribe(from, to, monthIndex, currency) {
    describe(sprintf("Select a valid fare from fly back monthly calendar  from %s to %s, using month index %s and  day index  %s", from, to, monthIndex, currency), function () {
        it('Given I on FOH timetable page', function () {
            actions.timetableActions.goToPage();
        });

        it(sprintf("Fill in from and to airports and search"), function () {
            actions.timetableActions.searchTimeTable(from, to);
        });
        it(sprintf("Select next month in Flyback section"), function () {
            actions.timetableActions.selectNextMonthInFlyback();
        });
        it(sprintf("Verify booking details correctly shown"), function () {
            actions.timetableActions.selectFlyBackMonthlyCalendarVerifyInBookingDetails(from, to, monthIndex, currency);
        });
    });

}

describe(specId + ' | Select flight from monthly view fly back calendar and verify in booking, from, to, monthIndex, currency', function () {
    sharedDescribe("Dublin", "Edinburgh", "2", "EUR");

});
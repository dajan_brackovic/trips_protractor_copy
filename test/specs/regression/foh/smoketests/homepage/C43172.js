var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "C43172"

function sharedDescribe(needCarIn, country, returnLocation, pickUpDate, pickUpTimeHour, pickUpTimeMin, dropOffDate, dropAtTimeHour, dropAtTimeMin) {
    var fohHomeActions = actions.fOHActions;

    describe("Ensure that you can search for cars with a different return to location", function () {
        it('Navigate to the homepage, to the flight booking widget', function () {
            fohHomeActions.goToPage();
        });

        it('Select the Cars tab on the booking widget', function () {
            fohHomeActions.clickCarTab();
        });

        it(sprintf("Select valid data for car search", needCarIn, country, returnLocation, pickUpDate, pickUpTimeHour, pickUpTimeMin, dropOffDate, dropAtTimeHour, dropAtTimeMin), function () {
            actions.fOHActions.enterNeedCarIn(needCarIn);
            actions.fOHActions.enterReturnLocation(returnLocation);
            actions.fOHActions.selectPickUpDate(pickUpDate);
            actions.fOHActions.selectDropOffDate(dropOffDate);
            //TODO: actions.fOHActions.selectPickUpTime(pickup);
            //TODO: actions.fOHActions.selectDropTime(dropOffTimeIndex);

        });
        //TODO: new window cannot be closed for now
        //it('When Click on Cars Tab', function () {
        //    actions.fOHActions.letsGoCarSearch();
        //});
        //
        //it("Verify that page redirected to car-hire.ryanair.com", function () {
        //    actions.fOHActions.verifyNonAngularPageOpened("https://car-hire.ryanair.com", "Ryanair -   Car Hire");
        //});
    });
}

describe(specId + " | Home page | Ensure that you can search for cars with a different return to location", function () {
    sharedDescribe("stn", "United Kingdom", "Edinburgh", "1", "11", "30", "4", "11", "30");

});


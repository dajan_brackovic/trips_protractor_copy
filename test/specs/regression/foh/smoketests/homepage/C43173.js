var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "C43173"

function sharedDescribe() {
    var fohHomeActions = actions.fOHActions;
    var FLIGHTS = "Flights";
    var HOTELS = "Hotels";
    var CARS = "Cars";

    describe("Ensure that you can switch from tab to tab - flights/hotels/cars", function () {
        it('Navigate to the homepage, to the flight booking widget', function () {
            fohHomeActions.goToPage();
        });

        it('Select the Hotels tab on the booking widget', function () {
            fohHomeActions.clickHotelsTab();
        });

        it('Pressing the Hotels tab will bring up the Hotel Booking widget', function () {
            fohHomeActions.verifyTabSelected(HOTELS);
        });

        it('Select the Cars tab on the booking widget', function () {
            fohHomeActions.clickCarTab();
        });

        it('Pressing the Cars tab will bring up the Car Booking widget', function () {
            fohHomeActions.verifyTabSelected(CARS);
        });

        it('Select the Flights tab on the booking widget', function () {
            fohHomeActions.clickFlightsTab();
        });

        it('Pressing the Flights tab will bring up the Flight Booking Widget', function () {
            fohHomeActions.verifyTabSelected(FLIGHTS);
        });

    });
}

describe(specId + " | Home page | Ensure that you can switch from tab to tab - flights/hotels/cars", function () {
    sharedDescribe();
});


var sprintf = require("sprintf").sprintf;
var specId = "FOH | " + "C609602";

function sharedDescribe(index, pageUrl) {
    describe("", function () {
        it('Navigate to the homepage, to the flight booking widget', function () {
            actions.fOHActions.goToPage();
        });

        it(sprintf("Click the %s link", index), function () {
            actions.fOHActions.clickWholeCardLink(index);
        });
        it(sprintf("Verify page %s opened and accessed properly", pageUrl), function () {
            actions.fOHActions.verifyPageSuccessOpened(pageUrl);
            actions.fOHActions.goToPage();
        });

    });
}

describe(specId + " | Whole Card Link | access top menu links - plan a trip", function () {
    sharedDescribe(0, "plan-trip/travel-extras/reserved-seating");
    sharedDescribe(1, "cheap-flight-destinations");
    sharedDescribe(2, "timetable");
    sharedDescribe(3, "flight-info/route");

});










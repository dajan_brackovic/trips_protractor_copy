var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "C43168"

function sharedDescribe(from, to, numberOfPassengers) {
    var fohHomeActions = actions.fOHActions;

    describe("Ensure that you can search for return trip", function () {
        it('Navigate to the homepage, to the flight booking widget', function () {
            fohHomeActions.goToPage();
        });

        it('Ensure that the Return option is selected by default', function () {
            fohHomeActions.verifyReturnBtnSelected();
        });

        it(sprintf("In the From field select %s. In the To field select %s and verify entered values", from, to), function () {
            fohHomeActions.searchFlightReturn(from, to);

        });

        it('The widget should automatically expand', function () {
            fohHomeActions.verifyDatesPassengersWidgetExpanded();
        });

        it('In the fly out and fly back fields, select a departure and arrival date from the dropdown calendar', function () {
            fohHomeActions.chooseDatesReturn(7, 2);
        });

        it(sprintf("Select %s Passenger", numberOfPassengers), function () {
            fohHomeActions.addPAXNumber(numberOfPassengers);
        });

        it('Verify that route is selected correctly', function () {
            fohHomeActions.verifyRouteSelected(from,to);
            fohHomeActions.clickSomeWhereToClosePopup();
        });

        it('Press Lets Go', function () {
            fohHomeActions.clickLetsGoBtn();
        });

        it('Verify trip page is approached', function () {
            fohHomeActions.verifyPageOpened("/booking/home");
        });
    });
}

describe(specId + " | Home page | Ensure that you can search for return trip", function () {
    sharedDescribe("London (STN)", "Berlin (SXF)", "1");

});
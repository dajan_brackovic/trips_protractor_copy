var sprintf = require("sprintf").sprintf;
//var specId = "FOH | " + "C609602ctabutton"
var specId = "FOH | " + "C609602";

function sharedDescribe(index, pageUrl) {
    describe("", function () {
        it('Navigate to the homepage, to the flight booking widget', function () {
            actions.fOHActions.goToPage();
        });

        it(sprintf("Click the %s link", index), function () {
            actions.fOHActions.clickCtaButton(index);
        });
        it(sprintf("Verify page %s opened and accessed properly", pageUrl), function () {
            actions.fOHActions.verifyPageSuccessOpened(pageUrl);
            actions.fOHActions.goToPage();
        });

    });
}

describe(specId + " | cta button | access top menu links - plan a trip", function () {
    sharedDescribe(1, "/cheap-flight-destinations");
    sharedDescribe(3, "/flight-info/route");
    sharedDescribe(2, "/timetable");


});










var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "C43177"

function sharedDescribe(from, to, numberOfPassengers, discount) {
    var fohHomeActions = actions.fOHActions;


    describe("Ensure that the Spanish Domestic Discounts apply", function () {
        it('Navigate to the homepage, to the flight booking widget', function () {
            fohHomeActions.goToPage();
        });

        it('Ensure that the Return option is selected by default', function () {
            fohHomeActions.verifyReturnBtnSelected();
        });

        it(sprintf("In the From field select %s. In the To field select %s and verify entered values", from, to), function () {
            fohHomeActions.searchFlightReturn(from, to);
        });
        it('In the fly out and fly back fields, select a departure and arrival date from the dropdown calendar', function () {
            fohHomeActions.chooseDatesReturn(8, 9);
        });

        it(sprintf("Select %s Passenger", numberOfPassengers), function () {
            fohHomeActions.addPAXNumber(numberOfPassengers);
        });

        it('The widget should automatically expand, which will also contain the Select Discount field.', function () {
            fohHomeActions.verifyDatesPassengersWidgetExpanded();
            fohHomeActions.verifySpanishDiscountAppeared();
        });

        it('Select an applicable discount', function () {
            fohHomeActions.selectSpanishDiscount(5);
        });

        it('Verify that route is selected correctly', function () {
            fohHomeActions.verifyRouteSelected(from,to);
            fohHomeActions.clickSomeWhereToClosePopup();
        });

        it('Press Lets Go', function () {
            fohHomeActions.clickLetsGoBtn();
        });

        it('Verify trip page is approached', function () {
            fohHomeActions.verifyPageOpened("/booking/home");
        });
    });
}

describe(specId + " | Home page | Ensure that the Spanish Domestic Discounts apply", function () {
    sharedDescribe("Madrid", "Mallorca", "1", "5");
});




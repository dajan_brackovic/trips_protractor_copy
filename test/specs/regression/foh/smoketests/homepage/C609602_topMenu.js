var sprintf = require("sprintf").sprintf;
var specId = "FOH | " + "C609602";

function sharedDescribe(menuIndex, subMenuIndex, pageUrl) {
    describe("", function () {
        it('Navigate to the homepage, to the flight booking widget', function () {
            actions.fOHActions.goToPage();
        });

        it('Ensure that the Return option is selected by default', function () {
            actions.fOHActions.clickLinkFromMenuLinkOption(menuIndex, subMenuIndex);
        });
        it(sprintf("Verify page %s opened properly", pageUrl), function () {
            actions.fOHActions.verifyPageSuccessOpened(pageUrl);
        });
        it('Click again to see it still working', function () {
            actions.fOHActions.clickLinkFromMenuLinkOption(menuIndex, subMenuIndex);
        });
    });
}

describe(specId + " | access top menu links - plan a trip", function () {
    sharedDescribe(0, 1, "/gb/en/");
    sharedDescribe(0, 2, "/cheap-flights");
    sharedDescribe(0, 3, "/cheap-flight-destinations");
    sharedDescribe(0, 9, "/plan-trip/travel-extras/gift-vouchers");
    sharedDescribe(0, 10, "/plan-trip/flying-with-us/family-extra");
    sharedDescribe(0, 11, "plan-trip/flying-with-us/business-plus");
    sharedDescribe(0, 12, "/plan-trip/flying-with-us/groups");
    sharedDescribe(0, 13, "/plan-trip/flying-with-us/our-app");
    sharedDescribe(0, 14, "/plan-trip/flying-with-us/my-ryanair");
    sharedDescribe(0, 15, "/plan-trip/travel-extras/reserved-seating");

});










var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "C43176"

function sharedDescribe(defaultAirport, budget) {
    var fohHomeActions = actions.fOHActions;
    var ffActions = actions.fareFinderActions;


    describe("Ensure that the Find More Deals link brings you to the correct page", function () {
        it('Navigate to the homepage, to the flight booking widget', function () {
            fohHomeActions.goToPage();
        });

        it(sprintf('Scroll to the Best Deals From section.The default option should be the closest airport or %s if the users is location is more than 200km from the nearest airport',defaultAirport), function () {
            fohHomeActions.verifyBestFlightDealSelection(defaultAirport);
            //TODO add IP configuration
        });

        it(sprintf("Click Find more details"), function () {
            fohHomeActions.clickFindMoreDeals();
        });

        it(sprintf("Verify that ff compact view is opened and the origin is brought over to this page %s", defaultAirport), function () {
            fohHomeActions.verifyPageOpened("/cheap-flights");
            ffActions.verifyBudgetValue(budget);
            ffActions.verifyAirportSelectedFF(defaultAirport);
            //TODO checking default from and to values at FF
        });
    });
}

describe(specId + " | Home page | Ensure that the Find More Deals link brings you to the correct page", function () {
    sharedDescribe("Dublin", "50");
});




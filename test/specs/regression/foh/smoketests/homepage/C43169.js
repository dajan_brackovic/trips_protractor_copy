var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "C43169"

function sharedDescribe(from, to, numberOfPassengers) {
    var fohHomeActions = actions.fOHActions;

    describe("Ensure that you can search for a one way trip", function () {
        it(sprintf("Search for one-way trip.In the From field select %s. In the To field select %s and verify entered values", from, to), function () {
            fohHomeActions.searchFlightOneWay(from,to);
        });

        it('Verify that when one-way is selected, the Fly Back field will disappear', function () {
            fohHomeActions.verifyFlyBackDisappeared();
        });

        it('The widget should automatically expand', function () {
            fohHomeActions.verifyDatesPassengersWidgetExpanded();
        });

        it('In the fly out select a departure date', function () {
            fohHomeActions.chooseDatesOneWay("7");
        });

        it(sprintf("Select %s Passenger", numberOfPassengers), function () {
            fohHomeActions.addPAXNumber(numberOfPassengers);
        });

        it('Verify that route is selected correctly', function () {
            fohHomeActions.verifyRouteSelected(from,to);
            fohHomeActions.clickSomeWhereToClosePopup();
        });

        it('Press Lets Go', function () {
            fohHomeActions.clickLetsGoBtn();
        });

        it('Verify trip page is approached', function () {
            fohHomeActions.verifyPageOpened("/booking/home");
        });
    });
}

describe(specId + " | Home page | Ensure that you can search for a one way trip", function () {
    sharedDescribe("London (STN)", "Dublin", "1");

});

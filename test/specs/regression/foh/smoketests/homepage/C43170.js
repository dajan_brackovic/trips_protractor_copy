var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "C43170"

function sharedDescribe(destination) {
    var fohHomeActions = actions.fOHActions;

    describe("Ensure that you can search for hotels", function () {
        it('Navigate to the homepage, to the flight booking widget', function () {
            fohHomeActions.goToPage();
        });

        it('Select the Hotels tab on the booking widget', function () {
            fohHomeActions.clickHotelsTab();
        });

        it(sprintf("In the Destination field, type  %s.", destination), function () {
            fohHomeActions.setDestinationHotelName(destination);
        });

        it('In the check-in field, select a date on the dropdown calendar.In the check-out field, select a date on the dropdown calendar', function () {
            fohHomeActions.chooseHotelBookingDates(1,5);
        });
        //Todo: Disable it for the time being
        //it('Select Lets Go', function () {
        //    fohHomeActions.performHotelSearch();
        //});
        //
        //it('Verify  hotel booking page is approached', function () {
        //    fohHomeActions.verifyNewNonAngularPageOpened("http://hotels.ryanair.com","Booking.com");
        //    fohHomeActions.clickHotelsTab();
        //});
    });
}

describe(specId + " | Home page | Ensure that you can search for hotels", function () {
    sharedDescribe("Alicante");
});


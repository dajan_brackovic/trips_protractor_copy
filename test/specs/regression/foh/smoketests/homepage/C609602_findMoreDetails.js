var sprintf = require("sprintf").sprintf;
var specId = "FOH | " + "C609602";

function sharedDescribe(link) {
    describe("", function () {
        it('Navigate to the homepage, to the flight booking widget', function () {
            actions.fOHActions.goToPage();
        });

        it('Ensure that the Return option is selected by default', function () {
            actions.fOHActions.clickFindMoreDeals();
        });
        it(sprintf("Verify page %s opened properly"), function () {
            actions.fOHActions.verifyPageSuccessOpened("/cheap-flights");
            actions.fOHActions.goToPage();
        });

    });
}

describe(specId + " | access top menu links - plan a trip", function () {
    sharedDescribe("cheap-flights"); //remain homepage

});










var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "C43175"

function sharedDescribe(defaultAirport, category1, category2, category3) {
    var fohHomeActions = actions.fOHActions;

    describe("Ensure that you can filter the fare by the categories", function () {
        it('Navigate to the homepage, to the flight booking widget', function () {
            fohHomeActions.goToPage();
        });

        it(sprintf('Scroll to the Best Deals From section.The default option should be the closest airport or %s if the users is location is more than 200km from the nearest airport',defaultAirport), function () {
            fohHomeActions.verifyBestFlightDealSelection(defaultAirport);
            //TODO add IP configuration
        });

        it(sprintf('Select the various filters by categories %s category and verify results changed',category1), function () {
            fohHomeActions.verifyResultsChangedForCategory(category1);
        });

        it(sprintf('Select the various filters by categories %s category and verify results changed',category2), function () {
            fohHomeActions.verifyResultsChangedForCategory(category2);
        });

        it(sprintf('Select the various filters by categories %s category and verify results changed',category3), function () {
            fohHomeActions.verifyResultsChangedForCategory(category3);
        });
    });
}

describe(specId + " | Home page | Ensure that you can filter the fare by the categories", function () {
    sharedDescribe("Dublin", "City Break", "Family", "Beach");
});




var specId ="FOH | "+ "C698093 "

function sharedDescribe() {


    describe("Ensure that the Cheap Flights from X widget appears by default", function () {
        it('Navigate to the homepage, to the flight booking widget', function () {
            actions.fOHActions.goToPage();
        });

        it('Select second option and ensure Cheap Flight FareFinder Page opens', function () {
            actions.fOHActions.clickOnCheapFlightsWidget(2);
        });

    });
}

describe(specId + " | Home page | Ensure that Cheap Flights from X widget is loaded", function () {
    sharedDescribe();
});

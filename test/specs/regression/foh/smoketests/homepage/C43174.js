var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "C43174"

function sharedDescribe(airport) {
    var fohHomeActions = actions.fOHActions;

    describe("Ensure that the best deals from shows the correct airport", function () {
        it('Navigate to the homepage, to the flight booking widget', function () {
            fohHomeActions.goToPage();
        });

        it(sprintf('Scroll to the Best Deals From section.The default option should be the closest airport or %s if the users is location is more than 200km from the nearest airport',airport), function () {
            fohHomeActions.verifyBestFlightDealSelection(airport);
            //TODO add IP configuration
        });
    });
}

describe(specId + " | Home page | Ensure that the best deals from shows the correct airport", function () {
    sharedDescribe("Dublin");
});



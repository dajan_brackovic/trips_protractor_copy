var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "C43171"

function sharedDescribe(needCarIn, country, pickUpDate, pickUpTimeHour, pickUpTimeMin, dropOffDate, dropAtTimeHour, dropAtTimeMin) {
    var fohHomeActions = actions.fOHActions;

    describe("Ensure that you can search for cars with same return to location", function () {
        it('Navigate to the homepage, to the flight booking widget', function () {
            fohHomeActions.goToPage();
        });

        it('Select the Cars tab on the booking widget', function () {
            fohHomeActions.clickCarTab();
        });

        it(sprintf("Select valid data for car search", needCarIn, country, pickUpDate, pickUpTimeHour, pickUpTimeMin, dropOffDate, dropAtTimeHour, dropAtTimeMin), function () {
            actions.fOHActions.enterNeedCarIn(needCarIn);
            actions.fOHActions.selectPickUpDate(pickUpDate);
            actions.fOHActions.selectDropOffDate(dropOffDate);
            //TODO: actions.fOHActions.selectPickUpTime(pickup);
            //TODO: actions.fOHActions.selectDropTime(dropOffTimeIndex);

        });
        //TODO: new window cannot be closed for now
        //it('When Click on Cars Tab', function () {
        //    actions.fOHActions.letsGoCarSearch();
        //});
        //
        //it("Verify that page redirected to car-hire.ryanair.com", function () {
        //    actions.fOHActions.verifyNonAngularPageOpened("https://car-hire.ryanair.com", "Ryanair -   Car Hire");
        //    fohHomeActions.clickCarTab();
        //});
    });
}

describe(specId + " | Home page | Ensure that you can search for cars with same return to location", function () {
    sharedDescribe("London (STN)", "United Kingdom", "1", "11", "30", "4", "11", "30");
});


var sprintf = require("sprintf").sprintf;
var specId ="FOH | "+ "40508"


function sharedDescribe(from, to, fromAirport, toAirport) {
    describe(sprintf("Search flights by entering from [%s] to [%s]in flight info page and get the results for fromAirport [%s] to toAirport [%s] ", from, to, fromAirport, toAirport), function () {
            it('Given I on FOH flights page', function () {
                actions.flightInfoByRouteActions.goToPage();
            });

            it(sprintf("Search flights from departure airport [%s] to destination airport[%s]", from, to), function () {
                actions.flightInfoByRouteActions.searchFlight(from, to);
                actions.flightInfoByRouteActions.submitSearch();
            });

            it(sprintf("Verify the search results show correct from and to information by comparing with api data", from, to, fromAirport, toAirport), function () {
                actions.flightInfoByRouteActions.verifyResultsAirportsNameCorrectly(from, to, fromAirport, toAirport);
            });

        }
    )
    ;

}

describe(specId + "Verify flight info search by route, from, to, fromAirport, toAirport", function () {
    sharedDescribe("STN", "ALC", "London (STN)", "Alicante");
    sharedDescribe("ALC", "STN", "Alicante", "London (STN)");
});





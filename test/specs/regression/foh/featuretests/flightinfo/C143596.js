describe('Search flight with invalid inputs', function () {

    var flightInfoByRouteActions = actions.flightInfoByRouteActions;

    describe(' blank blank ', function () {
        it('Given I am in live flight info by route page', function () {
            flightInfoByRouteActions.goToPage();
        });

        it('Fill in departure airport and destination airport with invalid input', function () {
            flightInfoByRouteActions.searchFlight("  ", "  ");
            flightInfoByRouteActions.submitSearch();
        });

        it('Verify propert alert shown ', function () {
            flightInfoByRouteActions.verifyAlertForEmptyFrom();
        });

    });

    describe(' ali blank ', function () {
        it('Given I am in live flight info by route page', function () {
            flightInfoByRouteActions.goToPage();
        });

        it('Fill in departure airport and destination airport with invalid input', function () {
            flightInfoByRouteActions.searchFlight("ali", "  ");
            flightInfoByRouteActions.submitSearch();

        });

        it('Verify propert alert shown ', function () {
            flightInfoByRouteActions.verifyAlertForEmptyTo();
        });

    });

    describe('Alicante blank ', function () {
        it('Given I am in live flight info by route page', function () {
            flightInfoByRouteActions.goToPage();
        });

        it('Fill in departure airport and destination airport with invalid input', function () {
            flightInfoByRouteActions.searchFlight("ali", "  ");
            flightInfoByRouteActions.submitSearch();

        });

        it('Verify propert alert shown ', function () {
            flightInfoByRouteActions.verifyAlertForEmptyTo();
        });

    });
    describe('AAAAA blank ', function () {
        it('Given I am in live flight info by route page', function () {
            flightInfoByRouteActions.goToPage();
        });

        it('Fill in departure airport and destination airport with invalid input', function () {
            flightInfoByRouteActions.searchFlight("AAAAA", "  ");
            flightInfoByRouteActions.submitSearch();

        });

        it('Verify propert alert shown ', function () {
            flightInfoByRouteActions.verifyAlertForInvalidFrom();
        });

    });

    describe('AAAAA AAAAA ', function () {
        it('Given I am in live flight info by route page', function () {
            flightInfoByRouteActions.goToPage();
        });

        it('Fill in departure airport and destination airport with invalid input', function () {
            flightInfoByRouteActions.searchFlight("AAAAA", "AAAAA");
            flightInfoByRouteActions.submitSearch();

        });

        it('Verify propert alert shown ', function () {
            flightInfoByRouteActions.verifyAlertForInvalidFrom();
        });

    });

    describe('AAAAA Ali ', function () {
        it('Given I am in live flight info by route page', function () {
            flightInfoByRouteActions.goToPage();
        });

        it('Fill in departure airport and destination airport with invalid input', function () {
            flightInfoByRouteActions.searchFlight("AAAAA", "Ali");
            flightInfoByRouteActions.submitSearch();

        });

        it('Verify propert alert shown ', function () {
            flightInfoByRouteActions.verifyAlertForInvalidFrom();
        });

    });
    describe('AAAAA Alicante ', function () {
        it('Given I am in live flight info by route page', function () {
            flightInfoByRouteActions.goToPage();
        });

        it('Fill in departure airport and destination airport with invalid input', function () {
            flightInfoByRouteActions.searchFlight("AAAAA", "Alicante");
            flightInfoByRouteActions.submitSearch();

        });

        it('Verify propert alert shown ', function () {
            flightInfoByRouteActions.verifyAlertForInvalidFrom();
        });

    });

    describe('blank bbbb', function () {
        it('Given I am in live flight info by route page', function () {
            flightInfoByRouteActions.goToPage();
        });

        it('Fill in departure airport and destination airport with invalid input', function () {
            flightInfoByRouteActions.searchFlight("  ", "bbbb");
            flightInfoByRouteActions.submitSearch();

        });

        it('Verify propert alert shown ', function () {
            flightInfoByRouteActions.verifyAlertForEmptyFrom();
        });

    });

    describe('bbbb bbbb ', function () {
        it('Given I am in live flight info by route page', function () {
            flightInfoByRouteActions.goToPage();
        });

        it('Fill in departure airport and destination airport with invalid input', function () {
            flightInfoByRouteActions.searchFlight("bbbb", "bbbb");
            flightInfoByRouteActions.submitSearch();

        });

        it('Verify propert alert shown ', function () {
            flightInfoByRouteActions.verifyAlertForInvalidFrom();
        });

    });

    describe('Ali bbbb ', function () {
        it('Given I am in live flight info by route page', function () {
            flightInfoByRouteActions.goToPage();
        });

        it('Fill in departure airport and destination airport with invalid input', function () {
            flightInfoByRouteActions.searchFlight("Ali", "bbbb");
            flightInfoByRouteActions.submitSearch();

        });

        it('Verify propert alert shown ', function () {
            flightInfoByRouteActions.verifyAlertForInvalidTo();
        });

    });
    describe('Alicante bbbb  ', function () {
        it('Given I am in live flight info by route page', function () {
            flightInfoByRouteActions.goToPage();
        });

        it('Fill in departure airport and destination airport with invalid input', function () {
            flightInfoByRouteActions.searchFlight("Alicante", "bbbb");
            flightInfoByRouteActions.submitSearch();

        });

        it('Verify propert alert shown ', function () {
            flightInfoByRouteActions.verifyAlertForInvalidTo();
        });

    });





});



var sprintf = require("sprintf").sprintf;
var specId ="FOH | "+ "C39371"

function sharedDescribe(flightNumber, from, to) {
    describe(sprintf("Search flights by entering flight number %s", flightNumber), function () {
            it('Given I on FOH flights page', function () {
                actions.flightInfoByNumberActions.goToPage();
            });

            it(sprintf("Get the flight number from one api call and search it in, search flights by entering flight number %s in api response", flightNumber), function () {
                actions.flightInfoByNumberActions.searchFlightByFlightNumber(flightNumber);
                actions.flightInfoByNumberActions.submitSearch();

            });
            it(sprintf("Verify the search results shown correctly with %s", flightNumber), function () {
                actions.flightInfoByNumberActions.verifyResultsCorrectlyShownCorrectly(flightNumber, from, to);
            });

        }
    )
    ;

}

describe(specId + 'Search flights by entering flight number, flightNumber', function () {
    sharedDescribe("7124","Dublin", "Lanzarote");
    sharedDescribe("1908","Dublin", "Gdansk");
    sharedDescribe("8166","London (STN)", "Tenerife Sth");
});
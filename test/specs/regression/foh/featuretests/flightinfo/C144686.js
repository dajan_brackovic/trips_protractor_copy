var sprintf = require("sprintf").sprintf;


describe('Search flight with invalid inputs', function () {

    describe('blank ', function () {
        it('Given I on FOH flights page', function () {
            actions.flightInfoByNumberActions.goToPage();
        });

        it(sprintf("Enter invalid flight number"), function () {
            actions.flightInfoByNumberActions.searchFlightByFlightNumber("  ");
            actions.flightInfoByNumberActions.submitSearch();

        });

        it('Verify proper alert shown ', function () {
            actions.flightInfoByNumberActions.verifyAlertForEmptyFlight();
        });

    });

    describe('asdfsad ', function () {
        it('Given I on FOH flights page', function () {
            actions.flightInfoByNumberActions.goToPage();
        });

        it(sprintf("Enter invalid flight number"), function () {
            actions.flightInfoByNumberActions.searchFlightByFlightNumber("asdfsad");
            actions.flightInfoByNumberActions.submitSearch();

        });

        it('Verify proper alert shown ', function () {
            actions.flightInfoByNumberActions.verifyAlertForEmptyFlight();
        });

    });


    describe('fr ', function () {
        it('Given I on FOH flights page', function () {
            actions.flightInfoByNumberActions.goToPage();
        });

        it(sprintf("Enter invalid flight number"), function () {
            actions.flightInfoByNumberActions.searchFlightByFlightNumber("fr");
            actions.flightInfoByNumberActions.submitSearch();

        });

        it('Verify proper alert shown ', function () {
            actions.flightInfoByNumberActions.verifyAlertForEmptyFlight();
        });

    });


    describe('23412341234 ', function () {
        it('Given I on FOH flights page', function () {
            actions.flightInfoByNumberActions.goToPage();
        });

        it(sprintf("Enter invalid flight number"), function () {
            actions.flightInfoByNumberActions.searchFlightByFlightNumber("23412341234");
            actions.flightInfoByNumberActions.submitSearch();

        });

        it('Verify proper alert shown ', function () {
            actions.flightInfoByNumberActions.verifyAlertForEmptyFlight();
        });

    });

});



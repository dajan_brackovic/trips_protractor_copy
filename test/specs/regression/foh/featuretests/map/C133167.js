var sprintf = require("sprintf").sprintf;
var specId ="FOH | "+ "133167"


function sharedDescribe(from, to, currency) {
    describe(sprintf(" %s to %s with budget %s", from, to, currency), function () {
        it('Given Im on map page', function () {
            actions.mapHomeActions.goToPage();
        });
        it(sprintf("When I search for flight from %s to %s", from, to), function () {
            actions.mapHomeActions.searchFlightWithBothAirportsSpecified(from, to);
        });
        it(sprintf("Switch airports in search field"), function () {
            actions.mapHomeActions.switchSearchFieldsValues();
        });
        it(sprintf("Book flight from map view, verify that in farefinder results page", from, to, currency), function () {
            actions.mapHomeActions.bookFlightFromChartViewVerifyInFfResultsDetailsPage(to, from, currency);
        });

    });

}

describe(specId + " |Switch airports in search field and book from chart view ,from, to, currency,from, to, currency", function () {
    sharedDescribe("Dublin", "Alicante", "\u20ac");
    sharedDescribe("Dublin", "London (STN)", "\u00A3");
    sharedDescribe("Dublin", "Gdansk", "\u007A\u0142");
    sharedDescribe("London (STN)", "Alicante", "\u20ac");
    sharedDescribe("London (STN)", "Edinburgh", "\u00A3");
});
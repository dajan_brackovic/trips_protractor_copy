var sprintf = require("sprintf").sprintf;
var specId ="FOH | "+ "133144"


function sharedDescribe(from, to, currency) {
    describe(sprintf(" %s to %s with budget %s", from, to, currency), function () {
        it('Given Im on map page', function () {
            actions.mapHomeActions.goToPage();
        });
        it(sprintf("When I search for flight from %s to %s", from, to), function () {
            actions.mapHomeActions.searchFlightWithBothAirportsSpecified(from, to);
        });

        it(sprintf("Switch airports in search field"), function () {
            actions.mapHomeActions.switchSearchFieldsValues();
        });
        it(sprintf("Book flight from map view, verify that in farefinder results page", from, to, currency), function () {
            actions.mapHomeActions.bookFlightFromMapViewVerifyInFfResultsDetailsPage(to, from, currency);
        });

    });

}

describe(specId + " | Book from map view after search flight with departure and destination airport both selected,from, to, currency", function () {
    sharedDescribe("Dublin", "Alicante", "\u20ac");
    //TODO: option not always available sharedDescribe("Dublin", "Cardiff", "\u00A3");
    sharedDescribe("Dublin", "Gdansk", "\u007A\u0142");
    sharedDescribe("London (STN)", "Alicante", "\u20ac");
    sharedDescribe("London (STN)", "Edinburgh", "\u00A3");//TODO: API data match issue

});

var sprintf = require("sprintf").sprintf;
var specId ="FOH | "+ "138088"
var toAirport = "";


function sharedDescribe(from, month, budget, type, resultIndex, currency) {
    describe(sprintf("Search flight from [%s] in the [%s]th available month and budget [%s]", from, month, budget), function () {
        it('Given Im on map page', function () {
            actions.mapHomeActions.goToPage();
        });

        it(sprintf("When I search flight from [%s] in the [%s]th available month and budget [%s] and trip type [%s]", from, month, budget, type), function () {
            actions.mapHomeActions.searchFlightsWithMoreFilters(from, month, budget, type);
        });
        it(sprintf("Select one result, verify it in chart view , from [%s]month[%s]resultIndex[%s]currency[%s]", from, month, resultIndex, currency), function () {
            actions.mapHomeActions.selectResultAndVerifyFieldsInChartViewFields(from, month, resultIndex, currency);
        });

        it(sprintf("Book flight from chart view with selected result, verify in ff results page all data are correctly matched"), function () {
            actions.mapHomeActions.bookFlightFromChartViewVerifyInFfResultsDetailsPage(from);
        });

    });

}

describe(specId + " | Search flights with more filters and select one results to book, verify further page data load, from, month, budget, type, resultIndex, currency)", function () {
    //sharedDescribe("London (STN)", "1", "44", "0", "1", "\u00A3");
    //sharedDescribe("London (STN)", "2", "22", "0", "1", "\u00A3");
    sharedDescribe("Dublin", "1", "44", "0", "1", "\u20ac");
    sharedDescribe("Gdansk", "2", "111", "0", "1", "\u007A\u0142");

});

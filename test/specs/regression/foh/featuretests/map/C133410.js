var sprintf = require("sprintf").sprintf;
var specId ="FOH | "+ "133410"


function sharedDescribe(from, month, budget, type, currency) {
    describe(sprintf("Search flight from %s in the %sth available month and budget %s", from, month, budget), function () {
        it('Given Im on map page', function () {
            actions.mapHomeActions.goToPage();
        });

        it(sprintf("When I search flight from %s in the %sth available month and budget %s and trip type %s", from, month, budget, type), function () {
            actions.mapHomeActions.searchFlightsWithMoreFilters(from, month, budget, type);
        });
        it(sprintf("Verify the search results are correctly shown in terms of budget %s and currency type %s", budget, currency), function () {
            actions.mapHomeActions.verifyResultsListUnderBudget(budget, currency);
        });

    });

}

describe(specId + " | Search flights with more filters, from, month, budget, type, currency", function () {
    sharedDescribe("London (STN)", "1", "20", "0", "\u00A3");
    sharedDescribe("London (STN)", "2", "40", "0", "\u00A3");
    sharedDescribe("Dublin", "2", "44", "0", "\u20ac");
    sharedDescribe("Gdansk", "3", "111", "0", "\u007A\u0142");

});

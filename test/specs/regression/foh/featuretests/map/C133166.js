var sprintf = require("sprintf").sprintf;
var specId ="FOH | "+ "133166"


function sharedDescribe(from, to, currency) {
    describe(sprintf(" %s to %s with budget %s", from, to, currency), function () {
        it('Given Im on map page', function () {
            actions.mapHomeActions.goToPage();
        });
        it(sprintf("When I search for flight from %s to %s", from, to), function () {
            actions.mapHomeActions.searchFlightWithBothAirportsSpecified(from, to);
        });

        it(sprintf("Book flight from map view, verify that in farefinder results page", from, to, currency), function () {
            actions.mapHomeActions.bookFlightFromChartViewVerifyInFfResultsDetailsPage(from, to, currency);
        });

    });

}

describe(specId + " | Book from chart view after search flights with departure and destination airport both selected ,from, to, currency(***Return when bug DF2372 fixed***)", function () {
    sharedDescribe("Dublin", "Alicante", "\u20ac");
    sharedDescribe("Dublin", "London (STN)", "\u20ac");
    sharedDescribe("London (STN)", "Alicante", "\u00A3");
    sharedDescribe("London (STN)", "Edinburgh", "\u00A3");

});

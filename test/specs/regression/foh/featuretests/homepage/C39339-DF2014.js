describe('C39339-DF2014 Recent search verification ', function () {

    var fohHomeActions = actions.fOHActions;
    var tripsHomeActions = actions.tripsHomeActions;

    describe('Search flight ALC STN 4', function () {
        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('When I search for one way flight with From and To Airports, verify fly out date field appears and fly Back date field does not appear', function () {
            fohHomeActions.searchFlightOneWay("ALC", "STN");
        });

        it('Verify available fields', function () {
            fohHomeActions.verifyPassengerFields();
        });

        it('When I select a flyout date and continue search, flight selection view page appears', function () {
            fohHomeActions.chooseDatesOneWay(4);
            fohHomeActions.clickLetsGoBtn();
        });

        it('Then verify trip home page appears', function () {
            tripsHomeActions.selectOneWayFlights();
        });

    });

    describe('Verify the recent search shows Alicante and London (STN)', function () {

        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('When I search flight with blank airport, I should get error message', function () {
            fohHomeActions.verifyRecentSearchRoute("Alicante", "London (STN)");

        });

    });

    describe('London (STN) Cork 4', function () {
        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('When I search for one way flight with From and To Airports, verify fly out date field appears and fly Back date field does not appear', function () {
            fohHomeActions.searchFlightOneWay("Pisa", "Liverpool");
        });

        it('Verify available fields', function () {
            fohHomeActions.verifyPassengerFields();
        });

        it('When I select a flyout date and continue search, flight selection view page appears', function () {
            fohHomeActions.chooseDatesOneWay(4);
            fohHomeActions.clickLetsGoBtn();
        });

    });

    describe('London (STN) Cork ', function () {

        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('When I search flight with blank airport, I should get error message', function () {
            //Verify that recent search shows the previous search
            fohHomeActions.verifyRecentSearchRoute("Pisa", "Liverpool");

        });

    });

});



describe('C37233-DF42 Reselect fly out date, countryFrom airportTo flyOutDate flyInDate reselectFlyOutDate', function () {
    var fohHomeActions = actions.fOHActions;

    describe(' ALC STN 2 3 4', function () {

        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('Fill in departure airport and destination airport', function () {
            fohHomeActions.searchFlight("ALC", "STN");
        });

        it('Verify available fields', function () {
            fohHomeActions.verifyPassengerFields();
        });

        it('Select departure and return date', function () {

            fohHomeActions.chooseDatesReturn(2, 3);
        });

        it('Reselect fly out date to be later than current fly back date, verify the change of fly back value', function () {
            fohHomeActions.verifyWhenReselectOutboundDate(3, 4);
        });

    });


    describe(' ALC STN 2 3 3', function () {

        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('Fill in departure airport and destination airport', function () {
            fohHomeActions.searchFlight("ALC", "STN");
        });

        it('Verify available fields', function () {
            fohHomeActions.verifyPassengerFields();
        });

        it('Select departure and return date', function () {

            fohHomeActions.chooseDatesReturn(2, 3);
        });

        it('Reselect fly out date to be same as current fly back date, verify the change of fly back value', function () {
            fohHomeActions.verifyWhenReselectOutboundDate(3, 3);
        });

    });


    describe('  ALC STN 3 5 3', function () {

        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('Fill in departure airport and destination airport', function () {
            fohHomeActions.searchFlight("ALC", "STN");
        });

        it('Verify available fields', function () {
            fohHomeActions.verifyPassengerFields();
        });

        it('Select departure and return date', function () {

            fohHomeActions.chooseDatesReturn(3, 5);
        });

        it('Reselect fly out date to be same as current flyout date, verify the change of fly back value', function () {
            fohHomeActions.verifyWhenReselectOutboundDate(5, 3);
        });

    });


    describe('  ALC STN 3 4 2', function () {

        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('Fill in departure airport and destination airport', function () {
            fohHomeActions.searchFlight("ALC", "STN");
        });

        it('Verify available fields', function () {
            fohHomeActions.verifyPassengerFields();
        });

        it('Select departure and return date', function () {

            fohHomeActions.chooseDatesReturn(3, 4);
        });

        it('Reselect fly out date to be earlier than original fly out date, verify the change of fly back value', function () {
            fohHomeActions.verifyWhenReselectOutboundDate(4, 2);
        });

    });


    describe('  ALC STN 2 5 3', function () {

        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('Fill in departure airport and destination airport', function () {
            fohHomeActions.searchFlight("ALC", "STN");
        });

        it('Verify available fields', function () {
            fohHomeActions.verifyPassengerFields();
        });

        it('Select departure and return date', function () {
            fohHomeActions.chooseDatesReturn(2, 5);
        });

        it('Reselect fly in date is later than original fly back date but earlier than fly in date , verify the change of fly out value', function () {
            fohHomeActions.verifyWhenReselectOutboundDate(5, 3);
        });

    });


})
;
var sprintf = require("sprintf").sprintf;


function sharedDescribe(from, nthResult) {
        describe(sprintf("Find best deal from %s, select one result %s", from, nthResult), function () {
            it('Given I on FOH flights page', function () {
                actions.fOHActions.goToPage();
            });
            it(sprintf("Search best best deals by selecting departure airport %s", from), function () {
                actions.fOHActions.searchBestDealsFrom(from);
            });
            it(sprintf("Select one of the search results %s", nthResult), function () {
                actions.fOHActions.selectOneLowestDeal(nthResult);
            });
            it(sprintf("Verify that selected search results show correctly for departure airport %s", from), function () {
                actions.fareFinderResultsDetailsActions.verifyFareFinderSearchResultsTitleFrom(from);
            });

        });


}

describe('C40547 DF-1860 Search deals', function () {
    sharedDescribe("Alghero", "1");
    sharedDescribe("Alghero", "2");
    sharedDescribe("Alghero", "3");
    sharedDescribe("Alghero", "4");
    sharedDescribe("Alghero", "5");
    sharedDescribe("Alicante", "1");
    sharedDescribe("Alicante", "2");
    sharedDescribe("Alicante", "3");
    sharedDescribe("Alicante", "4");
    sharedDescribe("Alicante", "5");

});
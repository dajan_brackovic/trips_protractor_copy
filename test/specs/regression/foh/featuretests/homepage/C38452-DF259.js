var sprintf = require("sprintf").sprintf;


function sharedDescribe(airportFrom, airportTo) {
        describe(sprintf("From Spain mainland airport toBalearic Island airport, airportFrom \"%s\ airportTo %s", airportFrom, airportTo), function () {
            it('Given I on FOH flights page', function () {
                actions.fOHActions.goToPage();
            });
            it(sprintf("Select from and to airports", airportFrom, airportTo), function () {
                //TODO: actions.fOHActions.verifyDefaultNonGeoDepartureAirport();
                actions.fOHActions.searchFlight(airportFrom, airportTo);
            });

            it('In the fly out and fly back fields, select a departure and arrival date from the dropdown calendar', function () {
                actions.fOHActions.chooseDatesReturn(7, 2);
            });

            it('Verify spanish discount option default value is, No discount applicable', function () {
                actions.fOHActions.verifySpanishDiscountDefaultValue("No discount applicable");
            });

            it('Verify spanish discount option dropdown shows', function () {
                actions.fOHActions.selectSpanishDiscount(0);
                actions.fOHActions.selectSpanishDiscount(5);
                actions.fOHActions.selectSpanishDiscount(10);
                actions.fOHActions.selectSpanishDiscount(50);
                actions.fOHActions.selectSpanishDiscount(55);
                actions.fOHActions.selectSpanishDiscount(60);
            });
        });
}

describe('C38452-DF259 Spanish Discount options for route From Spain mainland airport - toBalearic Island airport, - airportFrom - airportTo', function () {
    sharedDescribe("Barcelona", "Ibiza");
    sharedDescribe("Madrid", "Ibiza");
    sharedDescribe("Valencia", "Ibiza");
    //TODO: sharedDescribe("Barcelona", "Menorca");
    //TODO: sharedDescribe("Madrid", "Menorca");
    //TODO: sharedDescribe("Valencia", "Menorca");
    //TODO: sharedDescribe("Barcelona", "Palma");
    //TODO: sharedDescribe("Malaga", "Ibiza");
    //TODO: reduce test scenarios amount to avoid failures because of it
    //TODO: sharedDescribe("Girona", "Palma");
    //TODO: sharedDescribe("Malaga", "Palma");
    //TODO: sharedDescribe("Madrid", "Palma");
    //TODO: sharedDescribe("Reus", "Palma");
    //TODO: sharedDescribe("Santander", "Palma");
    //TODO: sharedDescribe("Santiago", "Palma");
    //TODO: sharedDescribe("Seville", "Palma");
    //TODO: sharedDescribe("Valencia", "Palma");
});
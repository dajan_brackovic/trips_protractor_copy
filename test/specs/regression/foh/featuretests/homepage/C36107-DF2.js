describe('C36107-DF2 Verify random selected departure country shows all available airports for this country', function () {
    var fohHomeActions = actions.fOHActions;
    describe('Round trip -Verify random selected departure country shows all available airports for this country', function () {
        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });
        it('Then select a random country from departure airport list and verify all its airports shown correctly', function () {
            fohHomeActions.verifyAirportsShownRoundTrip();
        });
    });

});
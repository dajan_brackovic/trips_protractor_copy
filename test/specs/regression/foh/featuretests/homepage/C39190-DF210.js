var sprintf = require("sprintf").sprintf;


function sharedDescribe(from, to, flyout, needCarin) {
    describe(sprintf("Flight search data transfer to car search- one way trip:  from %s to %s with flyout date %s and transferred to car hire's  pickup, dropoff and needCarIn %s", from, to, flyout, needCarin), function () {
        it('Given I on FOH flights page', function () {
            actions.fOHActions.goToPage();
        });

        it(sprintf("Fill in departure airport and destination airport from %s to %s", from, to), function () {
            actions.fOHActions.searchFlightOneWay(from, to);
        });
        it(sprintf("Select departure date %s", flyout), function () {
            actions.fOHActions.chooseDatesOneWay(flyout);
        });
        it(sprintf("Verify that car tabs fields values for needCarIn %s", to), function () {
            actions.fOHActions.clickCarTab();
            actions.fOHActions.verifyPickUpFieldValuesSingle();
            actions.fOHActions.verifyNeedCarInValues(needCarin);
        });
    });
}

describe('C39190-DF210 Flight search data transfer (single trip) - fromAirport - toAirport- flyout -needCarIn', function () {
    sharedDescribe("ALC", "STN", "3", "London (STN)");
    



});
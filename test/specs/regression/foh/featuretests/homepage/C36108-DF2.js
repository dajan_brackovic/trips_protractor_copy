describe('C36108-DF2  verify random selected departure country shows all available airports for this country', function () {
    var fohHomeActions = actions.fOHActions;
    describe('Single Way - verify random selected departure country shows all available airports for this country', function () {

        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('Verify all countries shown in departure list', function () {
            fohHomeActions.verifyAllCountriesShown();
        });
    });

});
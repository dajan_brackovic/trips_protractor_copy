var sprintf = require("sprintf").sprintf;


function sharedDescribe(needCarIn, returnLocation, country, pickUpDate, pickUpTimeHour, pickUpTimeMin, dropOffDate, dropAtTimeHour, dropAtTimeMin) {
         describe(sprintf("previously entered shown to to redirected page including needCarIn %s, returnLocation %s, country %s, pickUpDate %s,pickUpTimeHour %s, pickUpTimeMin %s, dropOffDate %s, dropAtTimeHour %s, dropAtTimeMin %s", needCarIn, returnLocation, country, pickUpDate, pickUpTimeHour, pickUpTimeMin, dropOffDate, dropAtTimeHour, dropAtTimeMin), function () {
            it('Given I on FOH flights page', function () {
                actions.fOHActions.goToPage();
            });
            it('When Click on Cars Tab', function () {
                actions.fOHActions.clickCarTab();
            });
            it(sprintf("Select valid data for car search", needCarIn, returnLocation, country, pickUpDate, pickUpTimeHour, pickUpTimeMin, dropOffDate, dropAtTimeHour, dropAtTimeMin), function () {
                actions.fOHActions.enterNeedCarIn(needCarIn);
                actions.fOHActions.enterReturnLocation(returnLocation);
                actions.fOHActions.selectCountryOfResidence(country);
                actions.fOHActions.selectPickUpDate(pickUpDate);
                actions.fOHActions.selectDropOffDate(dropOffDate);
                actions.fOHActions.selectPickUpHour(pickUpTimeHour);
                actions.fOHActions.selectPickUpMin(pickUpTimeMin);
                actions.fOHActions.selectDropOffHour(dropAtTimeHour);
                actions.fOHActions.selectDropOffMin(dropAtTimeMin);
            });
            it('When Click on Lests go button,verify car booking external page is opened with correct data brought over', function () {
                actions.fOHActions.letsGoCarSearch();
                browser.driver.sleep(6000);
                actions.fOHActions.verifyNewNonAngularPageOpened("car-hire.ryanair.com","Ryanair - Car Hire");
                //TODO: actions.fOHActions.verifySearchDataShownOnHertz(needCarIn, returnLocation, country, pickUpDate,dropOffDate);
            });
        });

}

describe('C39199-DF210 Car search with drop at location, - needCarIn - returnLocation - rpickUpDate(ValidIndex) - rpickUpTimeHour - rpickUpTimeMin - rdropOffDate(ValidIndex) - rdropAtTimeHour - dropAtTimeMin', function () {
    sharedDescribe("Bucharest", "STN", "Albania", "1", "01", "30", "4", "11", "30");

});
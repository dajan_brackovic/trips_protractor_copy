describe('C36105-DF1 One way trip, One way trip,  countryFrom airportTo flyOutDate', function () {

    var fohHomeActions = actions.fOHActions;
    var tripsHomeActions = actions.tripsHomeActions;

    describe('ALC STN 4', function () {
        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('When I search for one way flight with From and To Airports, verify fly out date field appears and fly Back date field does not appear', function () {
            fohHomeActions.searchFlightOneWay("ALC", "STN");
        });

        it('Verify available fields', function () {
            fohHomeActions.verifyPassengerFields();
        });

        it('When I select a flyout date and continue search, flight selection view page appears', function () {
            fohHomeActions.chooseDatesOneWay(4);
            fohHomeActions.clickLetsGoBtn();
        });

        it('Then verify trip home page appears', function () {
            tripsHomeActions.selectOneWayFlights();
        });

    });

    describe(' {blank}  STN ', function () {

        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('When I search flight with blank airport, I should get error message', function () {
            fohHomeActions.searchFlightOneWay(" ", "STN");
            fohHomeActions.clickLetsGoBtn();
            fohHomeActions.verifyErrorPopUpOutBoundAppears();

        });

    });


    describe(' STN {blank}', function () {
        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('When I search flight with blank airport, I should get error message', function () {
            fohHomeActions.searchFlightOneWay("STN", " ");
            fohHomeActions.clickContinue();
            fohHomeActions.clickLetsGoBtn();
            fohHomeActions.verifyErrorPopUpInBoundAppears();

        });

    });


    describe('GGGGGG  STN', function () {
        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('When I search flight with blank airport, I should get error message', function () {
            fohHomeActions.searchFlightOneWayForInvalidInput("GGGGGG", "STN");
            fohHomeActions.clickLetsGoBtn();
            fohHomeActions.VerifyErrorPopUpValidAirportFromAppears();

        });

    });

    describe(' ALC �$!$�!�$', function () {
        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('When I search flight with blank airport, I should get error message', function () {
            fohHomeActions.searchFlightOneWayForInvalidInput("ALC", "�$!$�!�$");
            fohHomeActions.clickContinue();
            fohHomeActions.clickLetsGoBtn();
            fohHomeActions.verifyErrorPopUpValidAirportToAppears();
        });

    });

    describe('ALC STN {blank}', function () {
        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('When I search for one way flight with From and To Airports, verify fly out date field appears and fly Back date field does not appear', function () {
            fohHomeActions.searchFlightOneWay("ALC", "STN");
        });

        it('When I select no date', function () {
            fohHomeActions.clickLetsGoBtn();
        });

        it('Then verify alert appears for departure date', function () {
            fohHomeActions.VerifyErrorPopUpFlyOutDateOneWayAppears();
        });

    });


});



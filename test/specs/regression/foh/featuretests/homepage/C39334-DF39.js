var sprintf = require("sprintf").sprintf;


function sharedDescribe(from, to, flyout, flyback, hotelLocation) {
    describe(sprintf("Flight search data transfer to car search- one way trip:  from %s to %s with flyout date %s and transferred to hotel search  pickup, dropoff and hotelLocation %s fields", from, to, flyout, flyback, hotelLocation), function () {
        it('Given I on FOH flights page', function () {
            actions.fOHActions.goToPage();
        });

        it('Fill in departure airport and destination airport', function () {
            actions.fOHActions.searchFlight(from, to);
        });

        it(sprintf("Select departure date %s and flyback date %s", flyout, flyback), function () {
            actions.fOHActions.chooseDatesReturn(flyout, flyback);

        });
        it(sprintf("Verify that car tabs fields values for hotelLocation %s", hotelLocation), function () {
            actions.fOHActions.verifyHotelsSearchFieldValues(hotelLocation);
        });
        it('Submit hotel search query, verify that the hotel search page opens', function () {
            actions.fOHActions.performHotelSearch();
            browser.driver.sleep(10000);
            actions.fOHActions.verifyNewNonAngularPageOpened("http://hotels.ryanair.com","Hotels in Alicante. Book your hotel now!");
        });
    });
}

describe('C39334-DF39 Flight search data transfer (single trip) - fromAirport - toAirport- flyout - hotelLocation', function () {
    sharedDescribe("STN", "ALC", "2", "3", "Alicante");

});
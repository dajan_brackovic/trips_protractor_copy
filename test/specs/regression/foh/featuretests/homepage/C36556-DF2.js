describe('C36556-DF2  One way trip, One way trip, countryFrom airportTo flyOutDate', function () {
    var fohHomeActions = actions.fOHActions;
    var tripsHomeActions = actions.tripsHomeActions;


    describe(' alc stn 2 ', function () {
        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('When I search for one way flight with From and To Airports, verify fly out date field appears and fly Back date field does not appear', function () {
            fohHomeActions.searchFlightOneWay("alc", "stn");
        });

        it('Verify available fields', function () {
            fohHomeActions.verifyPassengerFields();
        });

        it('When I select a flyout date and continue search, flight selection view page appears', function () {
            fohHomeActions.chooseDatesOneWay(2);
            fohHomeActions.clickLetsGoBtn();
        });

        it('Then verify trip home page appears', function () {
            tripsHomeActions.selectOneWayFlights();
        });

    });


});



describe('C36105-DF1 One way trip, One way trip,  countryFrom airportTo flyOutDate', function () {

    var fohHomeActions = actions.fOHActions;

    describe(' ALC STN {today} -', function () {
        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('When I search for one way flight with From and To Airports, verify fly out date field appears and fly Back date field does not appear', function () {
            fohHomeActions.searchFlightOneWay("ALC", "STN");

        });

        it('When I select a flyout date and continue search, flight selection view page appears', function () {
            fohHomeActions.chooseDatesOneWay(1);
            fohHomeActions.clickLetsGoBtn();
        });

        it('Then verify alert appears for today booking, click cancel the alert to say the same page, click ok to continue to proceed to trip', function () {
            fohHomeActions.cancelTodayAlert();
            fohHomeActions.clickLetsGoBtn();
            fohHomeActions.confirmTodayAlert();
            fohHomeActions.verifyPageOpened("/booking/home");
        });

    });

});



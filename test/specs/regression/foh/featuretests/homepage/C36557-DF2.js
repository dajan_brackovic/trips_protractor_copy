describe('C36557-DF2 Round trip,  countryFrom  airportTo  flyOutDate  flyInDate ', function () {
    var fohHomeActions = actions.fOHActions;

    describe(' alc stn 2 3 ', function () {
        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('Fill in departure airport and destination airport', function () {
            fohHomeActions.searchFlight("alc", "stn");
        });

        it('Verify available fields', function () {
            fohHomeActions.verifyPassengerFields();
        });

        it('Select departure and return date', function () {
            fohHomeActions.chooseDatesReturn(2, 3);
            fohHomeActions.clickLetsGoBtn();
        });

        it('Then verify trip home page appears', function () {
            fohHomeActions.verifyPageOpened("/booking/home");
        });

    });


});



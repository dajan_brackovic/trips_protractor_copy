describe('C36106-DF1 Round trip with invalid inputs, countryFrom airportTo flyOutDate flyInDate', function () {

    var fohHomeActions = actions.fOHActions;

    describe(' ALC STN 2 3 ', function () {
        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('Fill in departure airport and destination airport', function () {
            fohHomeActions.searchFlight("ALC", "STN");
        });

        it('Verify available fields', function () {
            fohHomeActions.verifyPassengerFields();
        });

        it('Select departure and return date', function () {
            fohHomeActions.chooseDatesReturn(2, 3);
            fohHomeActions.clickLetsGoBtn();
        });

        it('Then verify trip home page appears', function () {
            fohHomeActions.verifyPageOpened("/booking/home");
        });

    });

    describe('STN Dublin 1 2 ', function () {

        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('Fill in departure airport and destination airport', function () {
            fohHomeActions.searchFlight("STN", "Dublin");

        });

        it('Select departure and return date', function () {
            fohHomeActions.chooseDatesReturn(1, 2);
            fohHomeActions.clickLetsGoBtn();
        });

        it('Verify the today alert', function () {
            fohHomeActions.cancelTodayAlert();
            browser.sleep(4000);
            fohHomeActions.clickLetsGoBtn();
            fohHomeActions.confirmTodayAlert();
        });


    });

    describe('STN, Dublin 1 1 ', function () {
        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('Fill in departure airport and destination airport', function () {
            fohHomeActions.searchFlight("STN", "Dublin");

        });

        it('Select departure and return date', function () {
            fohHomeActions.chooseDatesReturn(1, 1);
            fohHomeActions.clickLetsGoBtn();
        });

        it('Verify the today alert', function () {
            fohHomeActions.verifyDepartTodayAlertAppears();
            fohHomeActions.verifyReturnTodayAlertAppears();
            fohHomeActions.cancelTodayAlert();
            browser.sleep(4000);
            fohHomeActions.clickLetsGoBtn();
            fohHomeActions.confirmTodayAlert();
        });


    });

    describe(' {blank} {blank} ', function () {
        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('Fill in departure airport and destination airport', function () {
            fohHomeActions.searchFlight("", "");
            fohHomeActions.clickContinue();
            fohHomeActions.clickLetsGoBtn();

        });

        it('Verify the missing departure airport error message', function () {
            fohHomeActions.verifyErrorPopUpOutBoundAppears();
        });
    });


    describe(' ALC {blank} ', function () {
        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('Fill in departure airport and destination airport', function () {
            fohHomeActions.searchFlight("ALC", "");
            fohHomeActions.clickContinue();
            fohHomeActions.clickLetsGoBtn();

        });

        it('Verify the missing departure airport error message', function () {
            fohHomeActions.verifyErrorPopUpInBound();
        });
    });


    describe('ALC ALC ', function () {
        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('Fill in departure airport and destination airport', function () {
            fohHomeActions.searchFlight("ALC", "ALC");
            fohHomeActions.clickContinue();
            fohHomeActions.clickLetsGoBtn();

        });

        it('Verify the missing departure airport error message', function () {
            fohHomeActions.verifyErrorPopUpValidAirportToAppears();
        });
    });


    describe(' GGGGGG ALC ', function () {
        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('Fill in departure airport and destination airport', function () {
            fohHomeActions.searchFlightInvalidInput("GGGGGG", "ALC");
            fohHomeActions.clickLetsGoBtn();
        });

        it('Verify the missing departure airport error message', function () {
            fohHomeActions.VerifyErrorPopUpValidAirportFromAppears();
        });
    });

    describe(' ALC �$!$�!�$ ', function () {
        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('Fill in departure airport and destination airport', function () {
            fohHomeActions.searchFlightInvalidInput("ALC", "�$!$�!�$ ");
            fohHomeActions.clickContinue();
            fohHomeActions.clickLetsGoBtn();

        });

        it('Verify the missing departure airport error message', function () {
            fohHomeActions.verifyErrorPopUpValidAirportToAppears();
        });
    });

    describe(' ALC STN 1 0{blank} ', function () {
        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('Fill in departure airport and destination airport', function () {
            fohHomeActions.searchFlight("ALC", "STN");

        });

        it('Select departure and return date', function () {
            fohHomeActions.chooseDatesReturn(1, 0);
            fohHomeActions.clickLetsGoBtn();
        });

        it('Verify the today alert', function () {
            fohHomeActions.verifyReturnDateRequiredAlertAppears();

        });

    });


    describe('STN  Dublin 0{blank} 1 ', function () {
        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('Fill in departure airport and destination airport', function () {
            fohHomeActions.searchFlight("STN", "Dublin");

        });

        it('Select departure and return date', function () {
            fohHomeActions.chooseDatesReturn(0, 1);
            fohHomeActions.clickLetsGoBtn();
        });

        it('Verify the today alert', function () {
            fohHomeActions.verifyDepartDateRequiredAlertAppears();
        });

    });


    describe(' ALC STN 0{blank} 0{blank} ', function () {
        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('Fill in departure airport and destination airport', function () {
            fohHomeActions.searchFlight("ALC", "STN");
        });

        it('Select departure and return date', function () {
            fohHomeActions.chooseDatesReturn(0, 0);
            fohHomeActions.clickLetsGoBtn();
        });

        it('Verify the today alert', function () {
            fohHomeActions.verifyDepartDateRequiredAlertAppears();

        });

    });

    describe(' ALC STN 1{today} 0{blank} ', function () {
        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('Fill in departure airport and destination airport', function () {
            fohHomeActions.searchFlight("ALC", "STN");
        });

        it('Select departure and return date', function () {
            fohHomeActions.chooseDatesReturn(1, 0);
            fohHomeActions.clickLetsGoBtn();
        });

        it('Verify the today alert', function () {
            fohHomeActions.verifyReturnDateRequiredAlertAppears();
        });

    });


    describe('  {default}  ALC 2 3', function () {
        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('Verify that the Departure airport defaults to London Stansted on first visit to the search widget for a nonRyanair geographical location', function () {
            //TODO: fohHomeActions.verifyDefaultNonGeoDepartureAirport();
            fohHomeActions.searchFlightWithDefaultDeparture("ALC");

        });

        it('Select departure and return date', function () {
            fohHomeActions.chooseDatesReturn(2, 3);
            fohHomeActions.clickLetsGoBtn();
        });

        it('Verify trip page is approached', function () {
            fohHomeActions.verifyPageOpened("/booking/home");
        });
    });

});



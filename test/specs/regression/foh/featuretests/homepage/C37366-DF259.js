var sprintf = require("sprintf").sprintf;


function sharedDescribe(airportFrom, airportTo) {
    describe(sprintf(" From Balearic Island airport to Spain mainland airport, airportFrom [%s] airportTo [%s]", airportFrom, airportTo), function () {
        it('Given I on FOH flights page', function () {
            actions.fOHActions.goToPage();
        });
        it(sprintf("Select from and to airports", airportFrom, airportTo), function () {
            //TODO: actions.fOHActions.verifyDefaultNonGeoDepartureAirport();
            actions.fOHActions.searchFlight(airportFrom, airportTo);
        });
        it('In the fly out and fly back fields, select a departure and arrival date from the dropdown calendar', function () {
            actions.fOHActions.chooseDatesReturn(2, 1);
        });

        it('Verify spanish discount option default value is, No discount applicable', function () {
            actions.fOHActions.verifySpanishDiscountDefaultValue("No discount applicable");
        });

        it('Verify spanish discount option dropdown shows', function () {
            actions.fOHActions.selectSpanishDiscount(0);
            actions.fOHActions.selectSpanishDiscount(5);
            actions.fOHActions.selectSpanishDiscount(10);
            actions.fOHActions.selectSpanishDiscount(50);
            actions.fOHActions.selectSpanishDiscount(55);
            actions.fOHActions.selectSpanishDiscount(60);
        });
    });


}


describe('C37366-DF259 Spanish Discount options for route from Balearic Island airport to Spain mainland airport - toBalearic Island airport, - airportFrom - airportTo', function () {
    sharedDescribe("Ibiza", "Barcelona");
    sharedDescribe("Ibiza", "Madrid");
    //sharedDescribe("Ibiza", "Malaga");
    //sharedDescribe("Ibiza", "Valencia");
    //sharedDescribe("Menorca", "Barcelona");
    //sharedDescribe("Menorca", "Madrid");
    //sharedDescribe("Menorca", "Valencia");
    //sharedDescribe("Palma", "Barcelona");
    //sharedDescribe("Palma", "Girona");
    //sharedDescribe("Palma", "Malaga");
    //sharedDescribe("Palma", "Madrid");
    //sharedDescribe("Palma", "Reus");
    //sharedDescribe("Palma", "Santander");
    //sharedDescribe("Palma", "Santiago");
    //sharedDescribe("Palma", "Valencia");


});
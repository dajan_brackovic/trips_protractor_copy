var sprintf = require("sprintf").sprintf;


function sharedDescribe(from, to, flyout, flyback, hotelLocation) {
    describe(sprintf("Flight search data transfer to car search- round trip:  from %s to %s with flyout date %s and transferred to hotel search  pickup, dropoff and hotelLocation %s fields", from, to, flyout, flyback, hotelLocation), function () {
        it('Given I on FOH flights page', function () {
            actions.fOHActions.goToPage();
        });

        it('Fill in departure airport and destination airport', function () {
            actions.fOHActions.searchFlight(from, to);
        });

        it(sprintf("Select departure date %s and flyback date %s", flyout, flyback), function () {
            actions.fOHActions.chooseDatesReturn(flyout, flyback);

        });
        it(sprintf("Verify that car tabs fields values for hotelLocation %s", hotelLocation), function () {
            actions.fOHActions.verifyHotelsSearchFieldValues(hotelLocation);
        });
        it('Submit hotel search query, verify that the hotel search page opens', function () {
            actions.fOHActions.performHotelSearch();

        });
    });
}

describe('C39334-DF39 Flight search data transfer (round trip) - fromAirport - toAirport- flyout - hotelLocation', function () {
    sharedDescribe("STN", "ALC", "2", "3", "Alicante");
    sharedDescribe("ALC", "STN", "1", "2", "London (STN)");
    sharedDescribe("ALC", "Gda", "1", "1", "Gdansk");
    sharedDescribe("ALC", "Gda", "3", "3", "Gdansk");
});
describe('C37252-DF42 Reselect fly in date, fromAirport  toAirport  flyOutDate  flyInDate reselect flyinDate', function () {
    var fohHomeActions = actions.fOHActions;

    describe(' ALC STN 3  4  5  ', function () {

        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('Fill in departure airport and destination airport', function () {
            fohHomeActions.searchFlight("ALC", "STN");
        });

        it('Verify available fields', function () {
            fohHomeActions.verifyPassengerFields();
        });

        it('Select departure and return date', function () {
            fohHomeActions.chooseDatesReturn(3, 4);
        });

        it('Reselect fly in date is later than original fly back date, verify the change of fly out value', function () {
            fohHomeActions.verifyWhenReselectInDate(5);
        });

    });


    describe(' ALC STN 3  4  3  ', function () {

        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('Fill in departure airport and destination airport', function () {
            fohHomeActions.searchFlight("ALC", "STN");
        });

        it('Verify available fields', function () {
            fohHomeActions.verifyPassengerFields();
        });

        it('Select departure and return date', function () {

            fohHomeActions.chooseDatesReturn(3, 4);
        });

        it('Reselect fly in date is same as current fly back date, verify no change of fly out value', function () {
            fohHomeActions.verifyWhenReselectInDate(3);
        });

    });


    describe(' ALC STN 3  5  4  ', function () {

        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('Fill in departure airport and destination airport', function () {
            fohHomeActions.searchFlight("ALC", "STN");
        });

        it('Verify available fields', function () {
            fohHomeActions.verifyPassengerFields();
        });

        it('Select departure and return date', function () {

            fohHomeActions.chooseDatesReturn(3, 5);
        });

        it('Reselect fly in date is same as current fly back date, verify no change of fly out value', function () {
            fohHomeActions.verifyWhenReselectInDate(4);
        });

    });

    describe(' ALC STN 3  4  4  ', function () {

        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
        });

        it('Fill in departure airport and destination airport', function () {
            fohHomeActions.searchFlight("ALC", "STN");
        });

        it('Verify available fields', function () {
            fohHomeActions.verifyPassengerFields();
        });

        it('Select departure and return date', function () {

            fohHomeActions.chooseDatesReturn(3, 4);
        });

        it('Reselect fly in date is same as current fly back date, verify no change of fly out value', function () {
            fohHomeActions.verifyWhenReselectInDate(4);
        });

    });


})
;
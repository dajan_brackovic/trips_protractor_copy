var sprintf = require("sprintf").sprintf;


function sharedDescribe(from, to, flyout, hotelLocation) {
        describe(sprintf("Flight search data transfer to car search- one way trip:  from %s to %s with flyout date %s and transferred to hotel search  pickup, dropoff and hotelLocation %s fields", from, to, flyout, hotelLocation), function () {
            it('Given I on FOH flights page', function () {
                actions.fOHActions.goToPage();
            });
            it(sprintf("Fill in departure airport and destination airport from %s to %s", from, to), function () {
                actions.fOHActions.searchFlightOneWay(from, to);
            });
            it(sprintf("Select departure date %s", flyout), function () {
                actions.fOHActions.chooseDatesOneWay(flyout);
            });
            //TODO: temp comment out
            //it(sprintf("Verify that car tabs fields values for hotelLocation %s", hotelLocation), function () {
            //    actions.fOHActions.verifyHotelsSearchCheckInCheckOutFields(hotelLocation);
            //});
            it('Submit hotel search query, verify that the hotel search page opens', function () {
                actions.fOHActions.clickHotelsTab();
                actions.fOHActions.performHotelSearch();
                browser.driver.sleep(10000);
                actions.fOHActions.verifyNewNonAngularPageOpened("http://hotels.ryanair.com","Book your hotel now!");
                //TODO: verify booking site data matched with previous data entry by users
            });

        });
}

describe('C39335-DF39 Flight search data transfer (single trip) - fromAirport - toAirport- flyout - hotelLocation ', function () {
    sharedDescribe("STN", "ALC", "2", "Alicante");
    sharedDescribe("ALC", "STN", "3", "London (STN)");
});
var sprintf = require("sprintf").sprintf;


function sharedDescribe(airportFrom, airportTo) {

        describe(sprintf("From Spain mainland airport toBalearic Island airport, airportFrom \"%s\ airportTo %s", airportFrom, airportTo), function () {
            it('Given I on FOH flights page', function () {
                actions.fOHActions.goToPage();
            });
            it(sprintf("Select from and to airports", airportFrom, airportTo), function () {
                //TODO: actions.fOHActions.verifyDefaultNonGeoDepartureAirport();
                actions.fOHActions.searchFlight(airportFrom, airportTo);
            });

            it('In the fly out and fly back fields, select a departure and arrival date from the dropdown calendar', function () {
                actions.fOHActions.chooseDatesReturn(7, 2);
            });
            it('Verify spanish discount option default value is, No discount applicable', function () {
                actions.fOHActions.verifySpanishDiscountDefaultValue("No discount applicable");
            });

            it('Verify spanish discount option shows correctly for between mainland airports', function () {
                actions.fOHActions.verifyMainlandsSpanishDiscountOptions();
            });
        });
}

describe('C38451-DF259 Spanish discount options for route from Spain mainland airports to Spain mainland airports,  - airportFrom - airportTo', function () {
    sharedDescribe("Madrid", "Santiago");
    sharedDescribe("Valencia", "Santander");


});


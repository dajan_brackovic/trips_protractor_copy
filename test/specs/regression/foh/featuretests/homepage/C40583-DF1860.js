var sprintf = require("sprintf").sprintf;


function sharedDescribe(from, country) {
        describe(sprintf("More deals data transfer -  country \"%s\  airport \"%s\ ", from, country), function () {
            it('Given I on FOH flights page', function () {
                actions.fOHActions.goToPage();
            });
            it(sprintf("Search best deals by selecting departure airport %s", from), function () {
                actions.fOHActions.searchBestDealsFrom(from);
            });
            it(sprintf("Click Find more details "), function () {
                actions.fOHActions.clickFindMoreDeals();
            });
            it(sprintf("Verify that ff compact view is opened and the origin is brought over to this page %s", from, country), function () {
                actions.fOHActions.verifyDataTransferWhenClickMoreDeals(from, country);
            });

        });


}

describe('C40583 DF-1860 Find more deals data transfer - airport, country', function () {
    sharedDescribe("Alghero", "Italy");
    //TODO: One failure can prove the issue sharedDescribe("Alicante", "Spain");
    //TODO: One failure can prove the issue sharedDescribe("Agadir", "Morocco");
    //TODO: One failure can prove the issue sharedDescribe("Almeria", "Spain");
});
var sprintf = require("sprintf").sprintf;


function sharedDescribe(from, to, flyout, flyback, needCarin) {
        describe(sprintf("Flight search data transfer to car search- round trip:  from %s to [%s] with flyout date [%s] and flyback date [%s]  transferred to  pickup, dropoff and needCarIn [%s]", from, to, flyout, flyback, needCarin), function () {
            it('Given I on FOH flights page', function () {
                actions.fOHActions.goToPage();
            });

            it('Fill in departure airport and destination airport', function () {
                actions.fOHActions.searchFlight(from, to);
            });

            it(sprintf("Select departure date %s and flyback date %s", flyout, flyback), function () {
                actions.fOHActions.chooseDatesReturn(flyout, flyback);

            });
            it(sprintf("Verify that car tabs fields values for needCarIn %s", needCarin), function () {
                actions.fOHActions.verifyPickUpFieldValues();
                actions.fOHActions.verifyDropOffFieldValues();
                actions.fOHActions.verifyNeedCarInValues(needCarin);


            });
        });

}

describe('C40603-DF210 Flight search data transfer (round trip) - fromAirport - toAirport- flyout -needCarIn', function () {
    sharedDescribe("STN", "Lisbon", "2", "3", "Lisbon");
    sharedDescribe("STN", "ALC", "2", "3", "Alicante");

});
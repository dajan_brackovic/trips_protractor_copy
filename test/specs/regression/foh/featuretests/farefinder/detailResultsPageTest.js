var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "detailResultsPageTest"

function sharedDescribe(from, to, budgetAmount) {
    describe(sprintf(" %s to %s with budget %s", from, to, budgetAmount), function () {
        it('Given Im on the fare finder page', function () {
            actions.fareFinderActions.goToPage();
        });

        it(sprintf("When I search for Fare finder flight from %s to %s with budget %s", from, to, budgetAmount), function () {
            actions.fareFinderActions.searchFareFinderFlight(from, to, budgetAmount);
        });

        it(sprintf("Then I should get list of flights on fare finder results page under %s", budgetAmount), function () {
            actions.fareFinderResultsActions.verifyListOfFlightsHaveValuesAndAreUnderBudget(budgetAmount);
        });

        it(sprintf("And I should get %s to %s with %s on results page", from, to, budgetAmount), function () {
            actions.fareFinderResultsActions.verifyFareFinderSearchResults(from, to, budgetAmount);
        });

        it("And I should select Great Deal", function () {
            //TODO: AEM method which only can be uncomment if it is always shown actions.fareFinderResultsActions.clickGreatDealLink();
            actions.fareFinderResultsActions.clickTopResult();
        });

        it(sprintf("Verify that selected search results show correctly for departure airport %s", from), function () {
            actions.fareFinderResultsDetailsActions.verifyFareFinderSearchResultsTitleFrom(from);
        });

        it("I want to select outbound chart view, switch to month view and verify that price changed", function () {
            actions.fareFinderResultsDetailsActions.selectChartViewOutbound();
        });

        it("I want to select second month in inbound chart view", function () {
            actions.fareFinderResultsDetailsActions.addReturnFlight();
            actions.fareFinderResultsDetailsActions.selectSecondInboundMonth();
        });

        it("I want to select highest price in outbound chart view and lowest in month view and verify that price changed", function () {
            actions.fareFinderResultsDetailsActions.verifyPriceChanged("outbound");
        });

        it("I want to select inbound chart view, switch to month view and verify that price changed", function () {
            actions.fareFinderResultsDetailsActions.selectChartViewInInbound();
        });

        it("I want to select highest price in inbound chart view and lowest in month view and verify that price changed", function () {
            actions.fareFinderResultsDetailsActions.verifyPriceChanged("inbound");
        });

    });
}

describe(specId + " | FareFinder Initial Search", function () {
    sharedDescribe("Dublin", "Anywhere", "150");
    sharedDescribe("London (STN)", "Anywhere", "150");
    sharedDescribe("Dublin", "London (STN)", "150");
    sharedDescribe("Dublin", "Madrid", "150");
});

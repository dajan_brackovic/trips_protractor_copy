var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "C38867"

function sharedDescribe(from, to, budgetAmount, flyOut, tripLength) {
    describe('Ensure that selecting a result leads to histogram/chart view of fares', function () {
        it('Given Im on the fare finder results page', function () {
            actions.fareFinderResultsActions.goToPage();
        });

        it(sprintf("When I search for Fare finder flight from %s to %s with budget %s flyOut %s and length %s", from, to, budgetAmount, flyOut, tripLength), function () {
            actions.fareFinderResultsActions.searchFareFinderFlightWithTripTime(from, to, budgetAmount, flyOut, tripLength);
        });

        it(sprintf("Verify fields %s to %s with budget %s flyOut %s and length %s are filled as expected", from, to, budgetAmount, flyOut, tripLength), function () {
            actions.fareFinderResultsActions.verifyAllFareFinderSearchResults(from, to, budgetAmount, flyOut, tripLength);
        });

        it("Select first flight from results and verify next page shows correct information", function () {
            actions.fareFinderResultsActions.clickFlightResultVerifyRecentView("1", from);
        });
    });
}

describe(specId + " | FareFinder Initial Search | Ensure that selecting a result leads to histogram/chart view of fares", function () {
    sharedDescribe("Alicante", "Anywhere", "150", "entireMonth", "8-11");
    sharedDescribe("Dublin", "Anywhere", "150", "entireMonth", "8-11");
});





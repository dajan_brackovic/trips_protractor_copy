var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "C40492"

function sharedDescribe(from, to, budgetAmount, flightIndex, flyOut, tripLength) {
    describe("Ensure that the Back to Fare finder link brings the user back to the previous list of results", function () {
        it('Given Im on the fare finder results page', function () {
            actions.fareFinderResultsActions.goToPage();
        });

        it(sprintf("When I search for Fare finder flight from %s to %s with budget %s flyOut %s and length %s", from, to, budgetAmount, flyOut, tripLength), function () {
            actions.fareFinderResultsActions.searchFareFinderFlightWithTripTime(from, to, budgetAmount, flyOut, tripLength);
        });

        it('Verify all results keep the same when enter one result and return back', function () {
            actions.fareFinderResultsActions.clickFlightResultByIndexThenReturnToPreviousListView(flightIndex);
        });

    });
}

describe(specId + " | FareFinder Initial Search | Ensure that the Back to Fare finder link brings the user back to the previous list of results", function () {
    sharedDescribe("Alicante", "Anywhere", "150", "1", "next3Months", "8-11");
});

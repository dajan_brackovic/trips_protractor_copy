var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "C58580"

function sharedDescribe(from, to, budgetAmount, flyOut, tripLength, type) {
    describe(sprintf("Search from farefinder and view results and book flight from map view %s %s %s %s %s", from, to, budgetAmount, flyOut, tripLength), function () {
        it('Given Im on the fare finder page', function () {
            actions.fareFinderActions.goToPage();
        });

        it(sprintf("Search by default"), function () {
            actions.fareFinderActions.goDefaultSearch();
        });

        it(sprintf("Change the country to be %s, verify the results number is reduced", to), function () {
            actions.fareFinderResultsActions.changeToCountryByMouseClickVerifyTheChangeInListView(to);
        });

    });
}

describe(specId + " | Search from farefinder and view results and book flight from map view", function () {
    sharedDescribe("null", "Alicante", "null", "null", "null", "null");
});



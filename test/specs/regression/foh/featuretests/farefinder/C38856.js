var sprintf = require("sprintf").sprintf;
var specId ="FOH | "+ "38856";


function sharedDescribe(from, to, budgetAmount) {
    describe(sprintf(" %s to %s with budget %s", from, to, budgetAmount), function () {
        it('Given Im on the fare finder page', function () {
            actions.fareFinderActions.goToPage();
        });

        it(sprintf("When I search for Fare finder flight from %s to %s with budget %s", from, to, budgetAmount), function () {
            actions.fareFinderActions.searchFareFinderFlight(from, to, budgetAmount);
        });

        it(sprintf("Then I should get list of flights on fare finder results page under %s", budgetAmount), function () {
            actions.fareFinderResultsActions.verifyListOfFlightsHaveValuesAndAreUnderBudget(budgetAmount);
        });

        it(sprintf("And I should get %s to %s with %s on results page", from, to, budgetAmount), function () {
            actions.fareFinderResultsActions.verifyFareFinderSearchResults(from, to, budgetAmount)
        });

    });


}


describe(specId + " | FareFinder Initial Search", function () {
    sharedDescribe("Dublin", "Anywhere", "150");
    sharedDescribe("Dublin", "London Stansted", "505");
    sharedDescribe("London Stansted", "Dublin", "400");
    sharedDescribe("Cork", "Anywhere", "50");
    sharedDescribe("Berlin Schonefeld", "Anywhere", "600");
    sharedDescribe("Berlin Schonefeld", "Anywhere", "30");
    sharedDescribe("Berlin Schonefeld", "Dublin", "130");
    sharedDescribe("Paris Beauvais", "Anywhere", "505");
});

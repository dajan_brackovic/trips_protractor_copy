var sprintf = require("sprintf").sprintf;

var specId ="FOH | "+ "C39273-DF1876"


function sharedDescribe(from, to, budgetAmount, nthResult) {
    describe(sprintf(" %s to %s with budget %s and select the %sth result", from, to, budgetAmount, nthResult), function () {
        it('Given Im on the fare finder page', function () {
            actions.fareFinderActions.goToPage();
        });

        it(sprintf("When I search for Fare finder flight from %s to %s with budget %s", from, to, budgetAmount), function () {
            actions.fareFinderActions.searchFareFinderFlight(from, to, budgetAmount);
        });


        it(sprintf("Then I should get list of flights on fare finder results page under %s", budgetAmount), function () {
            actions.fareFinderResultsActions.verifyListOfFlightsHaveValuesAndAreUnderBudget(budgetAmount);
        });

        it(sprintf("And I should get %s to %s with %s on results page", from, to, budgetAmount), function () {
            actions.fareFinderResultsActions.verifyFareFinderSearchResults(from, to, budgetAmount)
        });

        it(sprintf("Select %s nth result in the result list and verify amount and currency in cost transfered in details page correctly", nthResult), function () {
            actions.fareFinderResultsActions.verifyCostTransferredCorrectlyIntoDetailPage(nthResult)
        });

    });


}

describe(specId + " total cost shown correct value and currency -From-to-budget-resultIndex", function () {
    sharedDescribe("London (STN)", "Dublin", "400", "0");
    sharedDescribe("Cork", "Anywhere", "500", "1");
    sharedDescribe("Berlin (SXF)", "Anywhere", "600", "5");
    sharedDescribe("Berlin (SXF)", "Anywhere", "900", "0");
    sharedDescribe("Berlin (SXF)", "Dublin", "900", "0");
    sharedDescribe("Paris (BVA)", "Anywhere", "505", "2");
    sharedDescribe("Dublin", "Anywhere", "111", "3");
    sharedDescribe("Dublin", "London (STN)", "1000", "0");
});

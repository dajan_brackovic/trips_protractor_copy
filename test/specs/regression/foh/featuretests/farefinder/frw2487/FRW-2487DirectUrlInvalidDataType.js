var sprintf = require("sprintf").sprintf;
var specId ="FOH | "+ "FRW-2487DirectUrlInvalidDataType"
var today = new Date();

function formatDateToString(date) {
    var dd = (date.getDate() < 10 ? '0' : '') + date.getDate();
    var MM = ((date.getMonth() + 1) < 10 ? '0' : '') + (date.getMonth() + 1);
    var yyyy = date.getFullYear();
    return (yyyy + "-" + MM + "-" + dd);
}


function getSomeDate(years, months, days) {
    var CurrentDate = new Date();
    var date = CurrentDate.getDate();
    CurrentDate.setYear(CurrentDate.getFullYear() + years);
    CurrentDate.setMonth(CurrentDate.getMonth() + months);
    CurrentDate.setDate(CurrentDate.getDate() + days);
    return CurrentDate;
}

function getLastDayOfTheMonth() {
    var today = new Date();
    var lastDay = new Date(today.getFullYear(), today.getMonth() + 1, 0);
    return lastDay;
}


function sharedDescribe(from, to, invalidInput) {
    describe(sprintf('Use invalid parameter data value in the dynamic url, from %s to %s outDateStart  %s', from, to, invalidInput), function () {

        it('Given Im on the fare finder page', function () {
            actions.fareFinderResultsActions.goToUrlWithParameters(from, to, invalidInput);
        });
        it('C149199 C149198 Verify list of results still been given', function () {
            var someDate = getSomeDate(0, 0, 0);
            actions.fareFinderResultsActions.verifyUrlContains("out-from-date=" + formatDateToString(someDate));
            var someDate2 = getSomeDate(0, 3, 0);
            actions.fareFinderResultsActions.verifyUrlContains("out-to-date=" + formatDateToString(someDate2));
        });

    });
}

describe(specId + " | Verify functionallity if a user clicks on a link that contains date in the past - Results page", function () {
    sharedDescribe("DUB", "ALC", "111111");
    sharedDescribe("DUB", "ALC", "2015-10-05");

});


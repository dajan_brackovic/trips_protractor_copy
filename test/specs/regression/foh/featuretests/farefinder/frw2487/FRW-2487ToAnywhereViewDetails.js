var sprintf = require("sprintf").sprintf;
var specId ="FOH | "+ "FRW-2487ToAnywhereByMarkets.js"
var request = require('request');

function formatDateToString(date) {
    var dd = (date.getDate() < 10 ? '0' : '') + date.getDate();
    var MM = ((date.getMonth() + 1) < 10 ? '0' : '') + (date.getMonth() + 1);
    var yyyy = date.getFullYear();
    return (yyyy + "-" + MM + "-" + dd);
}


function getSomeDate(years, months, days) {
    var CurrentDate = new Date();
    var date = CurrentDate.getDate();
    CurrentDate.setYear(CurrentDate.getFullYear() + years);
    CurrentDate.setMonth(CurrentDate.getMonth() + months);
    CurrentDate.setDate(CurrentDate.getDate() + days);
    return CurrentDate;
}

function getLastDayOfTheMonth() {
    var today = new Date();
    var lastDay = new Date(today.getFullYear(), today.getMonth() + 1, 0);
    return lastDay;
}

function sharedDescribe(country, language, marketDir, from, to, budgetAmount, fromCode, toCode, flyout, tripLength, indexOfResults) {
    describe(sprintf("From %s to %s with budget %s", from, to, budgetAmount), function () {

        it('Given Im on the fare finder page', function () {
            actions.fareFinderResultsActions.goToPageByMarket(country, language, marketDir);
            actions.fareFinderResultsActions.verifyMarketsPageOpens(country);
            browser.waitForAngular();
        });

        it(sprintf("When I search for Fare finder flight from %s to %s with budget %s", from, to, budgetAmount), function () {
            actions.fareFinderActions.searchFareFinderFlight(from, to, budgetAmount);
        });

        it(sprintf("C146572 Verify if url contains from=%s", fromCode), function () {
            actions.fareFinderResultsActions.verifyUrlContains("from=" + fromCode);
        });

        it(sprintf("Verify if url contains out-date-start=%s by default"), function () {
            actions.fareFinderResultsActions.verifyUrlContains("out-from-date=");
        });
        it(sprintf("C149548 Select %s nth result in the result list and verify url", indexOfResults, marketDir), function () {
            actions.fareFinderResultsActions.selectResultFromSearchResults(from, indexOfResults, marketDir);
        });
        it(sprintf("C149549-2 Verify if url contains out-date-start"), function () {
            var someDate = getSomeDate(0, 0, 0);
            actions.fareFinderResultsDetailsActions.verifyUrlContains("out-from-date=" + formatDateToString(someDate));
        });
        it(sprintf("C149550 Verify if /cheap-flights URL contain optional parameter in to date - in format YYYY-MM-DD"), function () {
            actions.fareFinderResultsDetailsActions.verifyUrlContains("out-to-date=");
        });
        it(sprintf("C149551 Verify if /cheap-flights URL contain optional parameter in from date - in format YYYY-MM-DD"), function () {
            var someDate = getSomeDate(0, 0, 0);//TODO, to be decided, leave 1 here for the time being
            actions.fareFinderResultsDetailsActions.verifyUrlContains("out-from-date=" + formatDateToString(someDate));
        });
        it(sprintf("C149552 Verify if /cheap-flights URL contain optional parameter in to date - in format YYYY-MM-DD"), function () {
            actions.fareFinderResultsDetailsActions.verifyUrlContains("out-to-date=");
        });

    });


}

describe(specId + " | To Anywhere", function () {
    //sharedDescribe("it", "it", "voli-economici", "aarhus", "Ovunque", "299", "AAR", "Anywhere", "6", "8-11", 0);
    sharedDescribe("es", "es", "vuelos-baratos", "alicante", "Cualquier Destino", "299", "ALC", "Anywhere", "6", "8-11", 0);
    sharedDescribe("pl", "pl", "tanie-loty", "alicante", "Gdziekolwiek", "299", "ALC", "Anywhere", "6", "8-11", 0);
    //TODO: verify more
    //sharedDescribe("es", "ca", "vols-barats", "alicante", "Qualsevol Lloc", "299", "ALC", "Anywhere", "6", "8-11", 0);
    //sharedDescribe("es", "ca", "vols-barats", "alicante", "Qualsevol Lloc", "299", "ALC", "Anywhere", "6", "8-11", 0);
    //sharedDescribe("pt", "pt", "voos-baratos", "alicante", "Em Qualquer S�tio", "299", "ALC", "Anywhere", "6", "8-11", 0);
    //sharedDescribe("pt", "pt", "voos-baratos", "alicante", "Em Qualquer S�tio", "299", "ALC", "Anywhere", "6", "8-11", 0);
    //sharedDescribe("de", "de", "preiswerte-fluge", "alicante", "�berall", "299", "ALC", "Anywhere", "6", "8-11", 0);
    //sharedDescribe("nl", "nl", "goedkope-vluchten", "alicante", "Overal", "299", "ALC", "Anywhere", "6", "8-11", 0);
    //sharedDescribe("fr", "fr", "vols-a-bas-prix", "alicante", "N�importe O�", "299", "ALC", "Anywhere", "6", "8-11", 0);
    //sharedDescribe("dk", "da", "billige-flyrejser", "alicante", "Hvor Som Helst", "299", "ALC", "Anywhere", "6", "8-11", 0);//not localized cheap flights
    //sharedDescribe("no", "no", "billige-flyvninger", "alicante", "Hvor Som Helst", "299", "ALC", "Anywhere", "6", "8-11", 0);//
    //sharedDescribe("se", "sv", "Var Som Helst", "alicante", "Hvor Som Helst", "299", "ALC", "Anywhere", "6", "8-11", 0);//
    //sharedDescribe("se", "sv", "Var Som Helst", "alicante", "Hvor Som Helst", "299", "ALC", "Anywhere", "6", "8-11", 0);//
    //sharedDescribe("lt", "lt", "pigus-skrydziai", "alicante", "Bet Kur", "299", "ALC", "Anywhere", "6", "8-11", 0);//
    //sharedDescribe("no", "no", "billige-flyvninger", "alicante", "B�rhol", "299", "ALC", "Anywhere", "6", "8-11", 0);//
    //sharedDescribe("hu", "hu", "olcso-jaratok", "alicante", "B�rhol", "299", "ALC", "Anywhere", "6", "8-11", 0);//
    //sharedDescribe("gr", "el", "cheap-flights", "????????", "??????????", "299", "ALC", "Anywhere", "6", "8-11", 0);//
    //sharedDescribe("gr", "en", "cheap-flights", "alicante", "B�rhol", "299", "ALC", "Anywhere", "6", "8-11", 0);//
    //sharedDescribe("gr", "en", "cheap-flights", "alicante", "B�rhol", "299", "ALC", "Anywhere", "6", "8-11", 0);//
    //sharedDescribe("be", "nl", "goedkope-vluchten", "alicante", "Overal", "299", "ALC", "Anywhere", "6", "8-11", 0);
    //sharedDescribe("be", "fr", "vols-a-bas-prix", "alicante", "N�importe O�", "299", "ALC", "Anywhere", "6", "8-11", 0);
    //sharedDescribe("ma", "fr", "vols-a-bas-prix", "alicante", "N�importe O�", "299", "ALC", "Anywhere", "6", "8-11", 0);
    //sharedDescribe("at", "de", "preiswerte-fluge", "alicante", "�berall", "299", "ALC", "Anywhere", "6", "8-11", 0);

});



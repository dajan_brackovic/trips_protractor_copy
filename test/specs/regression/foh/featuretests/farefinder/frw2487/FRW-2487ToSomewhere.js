var sprintf = require("sprintf").sprintf;
var specId ="FOH | "+ "FRW-2487ToAnywhere"

function formatDateToString(date) {
    var dd = (date.getDate() < 10 ? '0' : '') + date.getDate();
    var MM = ((date.getMonth() + 1) < 10 ? '0' : '') + (date.getMonth() + 1);
    var yyyy = date.getFullYear();
    return (yyyy + "-" + MM + "-" + dd);
}


function getSomeDate(years, months, days) {
    var CurrentDate = new Date();
    var date = CurrentDate.getDate();
    CurrentDate.setYear(CurrentDate.getFullYear() + years);
    CurrentDate.setMonth(CurrentDate.getMonth() + months);
    CurrentDate.setDate(CurrentDate.getDate() + days);
    return CurrentDate;
}

function getLastDayOfTheMonth() {
    var today = new Date();
    var lastDay = new Date(today.getFullYear(), today.getMonth() + 1, 0);
    return lastDay;
}


function sharedDescribe(from, to, budgetAmount, fromCode, toCode, flyout, tripLength, indexOfResults) {
    describe(sprintf("From %s to %s with budget %s", from, to, budgetAmount), function () {
        it('Given Im on the fare finder page', function () {
            actions.fareFinderActions.goToPage();
        });

        it(sprintf("C146570 Verify if url contains /cheap-flights"), function () {
            actions.fareFinderResultsActions.verifyUrlContains("/cheap-flights");
        });

        it(sprintf("When I search for Fare finder flight from %s to %s with budget %s", from, to, budgetAmount), function () {
            actions.fareFinderActions.searchFareFinderFlight(from, to, budgetAmount);
        });

        it(sprintf("C146572 Verify if url contains from=%s", fromCode), function () {
            actions.fareFinderResultsActions.verifyUrlContains("from=" + fromCode);
        });

        it(sprintf("C146574 Verify if url contains to=%s", toCode), function () {
            actions.fareFinderResultsActions.verifyUrlContains("to=" + toCode);
        });

        it(sprintf("Verify if url contains out-date-start=%s by default"), function () {
            var someDate = getSomeDate(0, 0, 0);
            actions.fareFinderResultsActions.verifyUrlContains("out-from-date=" + formatDateToString(someDate));
        });

        it(sprintf("Select a specific flyout date", flyout), function () {
            actions.fareFinderResultsActions.selectSpecificDate(flyout)
        });

        it(sprintf("C146575 Verify if /cheap-flights URL contain required parameter out from date - in format YYYY-MM-DD, flyout"), function () {
            var addDays = parseInt(flyout) - 1;
            reporter.addMessageToSpec("addDays" + addDays);
            var someDate = getSomeDate(0, 0, addDays);
            reporter.addMessageToSpec("someDate" + formatDateToString(someDate));
            actions.fareFinderResultsActions.verifyUrlContains("out-from-date=" + formatDateToString(someDate));
        });

        it(sprintf("Select a specific flyout date range from tomorrow to the end of current month"), function () {
            actions.fareFinderResultsActions.selectSpecificDateRangeTomorrowToLastDay()
        });

        it(sprintf("C146579 Verify if /cheap-flights URL contain optional parameter out to date - in format YYYY-MM-DD"), function () {
            var lastDayOfMonth = getLastDayOfTheMonth();
            actions.fareFinderResultsActions.verifyUrlContains("out-to-date=" + formatDateToString(lastDayOfMonth));
        });

        it(sprintf("Select a trip length %s", tripLength), function () {
            actions.fareFinderResultsActions.selectTripLength(tripLength)
        });

        it(sprintf("C146584 Verify if /cheap-flights URL contain optional parameter in from date - in format YYYY-MM-DD"), function () {
            var someDate = getSomeDate(0, 0, 1);
            actions.fareFinderResultsActions.verifyUrlContains("out-from-date=" + formatDateToString(someDate));
        });
        it(sprintf("C146585 Verify if /cheap-flights URL contain optional parameter in to date - in format YYYY-MM-DD"), function () {
            var lastDayOfMonth = getLastDayOfTheMonth();
            actions.fareFinderResultsActions.verifyUrlContains("out-to-date=" + formatDateToString(lastDayOfMonth));
        });
        it(sprintf("C149193 C149194 Verify if /cheap-flights URL contain optional parameter trip length from & to - numeric", tripLength), function () {
            var tripLengthsFromTo = tripLength.split("-");
            actions.fareFinderResultsActions.verifyUrlContains("trip-length-from=" + tripLengthsFromTo[0]);
            actions.fareFinderResultsActions.verifyUrlContains("trip-length-to=" + tripLengthsFromTo[1]);
        });
        it(sprintf("C149195 Verify if /cheap-flights URL contain optional parameter budget - XXXXX numeric digits", budgetAmount), function () {
            actions.fareFinderResultsActions.verifyUrlContains("budget=" + budgetAmount);
        });

    });


}

describe(specId + " | From somehwere to somewhere", function () {
    sharedDescribe("alicante", "dublin", "40", "ALC", "DUB", "1", "8-11", 1);
    sharedDescribe("bristol", "dublin", "60", "BRS", "DUB", "1", "8-11", 1);
    sharedDescribe("poznan", "dublin", "169", "POZ", "DUB", "1", "8-11", 1);
    sharedDescribe("cork", "alicante", "40", "ORK", "ALC", "1", "8-11", 1);
    sharedDescribe("almeria", "dublin", "40", "LEI", "DUB", "1", "8-11", 1);
    sharedDescribe("amsterdam", "dublin", "40", "AMS", "DUB", "1", "8-11", 1);
    sharedDescribe("malaga", "dublin", "40", "AGP", "DUB", "1", "8-11", 1);
    sharedDescribe("bari", "dublin", "40", "BRI", "DUB", "1", "8-11", 1);
});



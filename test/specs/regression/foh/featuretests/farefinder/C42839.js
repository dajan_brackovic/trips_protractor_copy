var sprintf = require("sprintf").sprintf;


var specId ="FOH | "+ "42839";

function sharedDescribe(filter) {
    var from, to, budgetAmount, flyOut, tripLength;
    from = "Dublin";
    to = "Anywhere";
    budgetAmount = '50';
    flyOut = "Anytime";
    tripLength = "oneWay";


    describe(sprintf('FareFinder Search From Results Page: %s to %s with budget %s, %s , %s', from, to, budgetAmount, flyOut, tripLength), function () {
        it('Given Im on the fare finder page', function () {
            actions.fareFinderResultsActions.goToPage();
        });

        it(sprintf("When I search for Fare finder flight from %s to %s with budget %s flyOut %s and length %s", from, to, budgetAmount, flyOut, tripLength), function () {
            actions.fareFinderResultsActions.searchFareFinderFlightWithTripTime(from, to, budgetAmount, flyOut, tripLength);
        });

        it(sprintf("And I set Fare finder filter %s", filter ), function () {
            actions.fareFinderResultsActions.setSearchFilter(filter);
        });

        it(sprintf("Then I should get list of flights on fare finder results page under %s", budgetAmount), function () {
            actions.fareFinderResultsActions.verifyListOfFlightsHaveValuesAndAreUnderBudget(budgetAmount);
        });
    });
}

describe(specId + " | FareFinder Results - Flight Search - Filters", function () {
    sharedDescribe("Romance");
    sharedDescribe("City Break");
    sharedDescribe("Family");
    sharedDescribe("Nightlife");
    sharedDescribe("Culture");
    sharedDescribe("Golf");
});


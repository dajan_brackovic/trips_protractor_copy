var sprintf = require("sprintf").sprintf;


var specId ="FOH | "+ "38860";

function sharedDescribe(from, to, budgetAmount, flyOut, tripLength) {
    describe(sprintf('FareFinder Search From Results Page: %s to %s with budget %s, %s , %s', from, to, budgetAmount, flyOut, tripLength), function () {

        it('Given Im on the fare finder page', function () {
            actions.fareFinderResultsActions.goToPage();
        });

        it(sprintf("When I search for Fare finder flight from %s to %s with budget %s flyOut %s and length %s", from, to, budgetAmount, flyOut, tripLength), function () {
            actions.fareFinderResultsActions.searchFareFinderFlightWithTripTime(from, to, budgetAmount, flyOut, tripLength);
        });

        it(sprintf("Then I should get list of flights on fare finder results page under %s", budgetAmount), function () {
            actions.fareFinderResultsActions.verifyListOfFlightsHaveValuesAndAreUnderBudget(budgetAmount);
        });
    });
}

describe(specId + " | FareFinder Results - Flight Search", function () {
    sharedDescribe("Dublin", "Anywhere", "50", "next3Months", "oneWay");
    sharedDescribe("Dublin", "Anywhere", "50", "specificDate", "8-11");
    sharedDescribe("London Stansted", "Anywhere", "50", "entireMonth", "8-11");
    sharedDescribe("London Stansted", "Anywhere", "50", "dateRange", "oneWay");
    sharedDescribe("London Stansted", "Anywhere", "50", "Anytime", "Anytime");
    sharedDescribe("London Stansted", "Anywhere", "50", "Anytime", "customRange");
});


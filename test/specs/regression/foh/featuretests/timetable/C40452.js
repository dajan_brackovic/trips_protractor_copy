var sprintf = require("sprintf").sprintf;


function sharedDescribe(from, to, monthIndex, dayIndex) {
    describe(sprintf("Select a valid fare from fly back calendar in month view after search flight from %s to %s, using month index %s and  day index  %s", from, to, monthIndex, dayIndex), function () {
        it('Given I on FOH timetable page', function () {
            actions.timetableActions.goToPage();
        });

        it(sprintf("Fill in from and to airports and search"), function () {
            actions.timetableActions.searchTimeTable(from, to);
        });

        it(sprintf("Verify calendar header to contain correct airport"), function () {
            actions.timetableActions.verifyCalenderHeader(from, to);

        });

        it(sprintf("Verify calendar default selected day"), function () {
            actions.timetableActions.verifyDefaultHighlightedDatesInMonthlyView();
        });

        it(sprintf("Select a valid day which is not today, verify the fare widget shows the fare of this selected flyout day"), function () {
            actions.timetableActions.selectFlyBackMonthlyCalendarVerifyInWidget(monthIndex, dayIndex);
        });


    });


}

describe('C40452  Select a valid fare option from fly back available fares in monthly view - from, to, monthIndex, dayIndex', function () {
    sharedDescribe("Dublin", "Alicante", "3", "1");
    sharedDescribe("Dublin", "Alicante", "3", "2");
    sharedDescribe("Dublin", "Alicante", "3", "3");
    sharedDescribe("Dublin", "Alicante", "4", "4");
    sharedDescribe("Dublin", "Alicante", "5", "5");
    sharedDescribe("Dublin", "Alicante", "6", "6");

});
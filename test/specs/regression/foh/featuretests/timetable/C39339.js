var sprintf = require("sprintf").sprintf;


function sharedDescribe(from, to, monthIndex, dayIndex, currency) {
    describe(sprintf("Select a valid fare from fly out monthly calendar  from %s to %s, using month index %s and  day index  %s", from, to, monthIndex, dayIndex, currency), function () {
        it('Given I on FOH timetable page', function () {
            actions.timetableActions.goToPage();
        });

        it(sprintf("Fill in from and to airports and search"), function () {
            actions.timetableActions.searchTimeTable(from, to);
        });

        it(sprintf("Move to next month %s times and then select a month %s and a day %s", from, to, monthIndex, dayIndex, currency), function () {
            actions.timetableActions.selectNextMonthInFlyout();
            actions.timetableActions.selectFlyOutMonthlyCalendarVerifyInBookingDetails(from, to, monthIndex, dayIndex, currency);

        });

    });

}

describe('C39339  Select flight from monthly view fly out calendar and verify in booking, from, to, monthIndex, dayIndex, currency', function () {
    sharedDescribe("Dublin", "Alicante", "2", "2", "EUR");
    sharedDescribe("Dublin", "Alicante", "3", "1", "EUR");
    sharedDescribe("Dublin", "Alicante", "4", "2", "EUR");
    sharedDescribe("Liverpool", "Alicante", "1", "1", "GBP");
    sharedDescribe("Dublin", "Alicante", "2", "2", "EUR");
    sharedDescribe("Dublin", "Alicante", "3", "2", "EUR");
    sharedDescribe("Dublin", "Alicante", "4", "2", "EUR");
});
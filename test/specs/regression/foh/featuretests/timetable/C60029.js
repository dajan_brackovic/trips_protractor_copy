var sprintf = require("sprintf").sprintf;


function sharedDescribe(from, to, next, previous, weekday) {
    describe(sprintf("Search from %s to %s, move to next %s weeks and then move to previous %s weeks in fly out calendar, then select day %s", from, to, next, previous, weekday), function () {

        it('Given I on FOH timetable page', function () {
            actions.timetableActions.goToPage();
        });

        it(sprintf("Fill in from and to airports and search"), function () {
            actions.timetableActions.searchTimeTable(from, to);
        });

        it(sprintf("Click on weekly view in fly back section"), function () {
            actions.timetableActions.clickWeeklyViewButtonFlyout();
        });

        it(sprintf("Move to next week %s times and then select a week day %s", next, weekday), function () {
            var count;
            for (count = 0; count < next; count++) {
                actions.timetableActions.selectNextWeekInFlyout();
                actions.timetableActions.selectFlyOutWeeklyCalendarVerifyInWidget(weekday);
            }
        });

        it(sprintf("Move back to previous week %s times and then select a week day %s", previous, weekday), function () {
            var count;
            for (count = 0; count < previous; count++) {
                actions.timetableActions.selectPreviousWeekInFlyout();
                actions.timetableActions.selectFlyOutWeeklyCalendarVerifyInWidget(weekday);
            }
        });

    });

}
describe('C60029 Flyout calendar weekly Carousel , -from -to -Next- Previous- weekday', function () {
    sharedDescribe("Dublin", "Alicante", "3", "2", "2");
    sharedDescribe("Dublin", "Alicante", "2", "1", "1");

});
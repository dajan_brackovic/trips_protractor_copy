var sprintf = require("sprintf").sprintf;

describe('C40451 DF-218 Timetable invalid search input - from - to', function () {

    describe(sprintf("Invalid From"), function () {
        it('Given I on FOH timetable page', function () {
            actions.timetableActions.goToPage();
        });

        it(sprintf("Fill in invalid"), function () {
            actions.timetableActions.searchTimeTable("yy", "Gdansk");
        });

        it(sprintf("Verify the alert message for invalid input"), function () {
            actions.timetableActions.verifyAlertMessageForInvalidFromAirport();

        });


    });

    describe(sprintf("Invalid To"), function () {
        it('Given I on FOH timetable page', function () {
            actions.timetableActions.goToPage();
        });

        it(sprintf("Fill in invalid"), function () {
            actions.timetableActions.searchTimeTable("Gdansk", "yy");
        });

        it(sprintf("Verify the alert message for invalid input"), function () {
            actions.timetableActions.verifyAlertMessageForInvalidFromAirport();

        });

    });


    describe(sprintf("Both Invalid, From and To"), function () {
        it('Given I on FOH timetable page', function () {
            actions.timetableActions.goToPage();
        });

        it(sprintf("Fill in invalid"), function () {
            actions.timetableActions.searchTimeTable("yy", "yy");
        });

        it(sprintf("Verify the alert message for invalid input"), function () {
            actions.timetableActions.verifyAlertMessageForInvalidFromAirport();

        });

    });

    describe(sprintf("Blank From"), function () {
        it('Given I on FOH timetable page', function () {
            actions.timetableActions.goToPage();
        });

        it(sprintf("Fill in invalid"), function () {
            actions.timetableActions.searchTimeTable("", "Gdansk");
        });

        it(sprintf("Verify the return view of the timetable"), function () {
            actions.timetableActions.verifyAlertForMissingFromField();

        });

    });


    describe(sprintf("Blank To"), function () {
        it('Given I on FOH timetable page', function () {
            actions.timetableActions.goToPage();
        });

        it(sprintf("Fill in invalid"), function () {
            actions.timetableActions.searchTimeTable("Gdansk", "");
        });

        it(sprintf("Verify the return view of the timetable"), function () {
            actions.timetableActions.verifyAlertForMissingToField();

        });

    });


    describe(sprintf("Blank both From and To"), function () {
        it('Given I on FOH timetable page', function () {
            actions.timetableActions.goToPage();
        });

        it(sprintf("Fill in invalid"), function () {
            actions.timetableActions.searchTimeTable("", "");
        });

        it(sprintf("Verify the return view of the timetable"), function () {
            actions.timetableActions.verifyAlertForMissingFromField();

        });

    });


});



var sprintf = require("sprintf").sprintf;


function sharedDescribe(from, to, column, currency) {
    describe(sprintf("Select flight from weekly view fly out calendar and verify in booking, [%s][%s][%s][%s]  ", from, to, column, currency), function () {
        it('Given I on FOH timetable page', function () {
            actions.timetableActions.goToPage();
        });

        it(sprintf("Fill in from and to airports"), function () {
            actions.timetableActions.searchTimeTable(from, to);

        });
        it(sprintf("Click on weekly view in fly out section "), function () {
            actions.timetableActions.clickWeeklyViewButtonFlyback();

        });
        it(sprintf("Select a valid day then verify the data including currency"), function () {
            actions.timetableActions.selectNextWeekInFlyback();
            actions.timetableActions.selectFlyBackWeeklyCalendarVerifyInBookingDetails(from, to, column, currency);

        });

    });

}

describe('C75724  Select flight from weekly view fly back calendar and verify in booking, from, to, column, currency ', function () {
   //TODO: not always available sharedDescribe("Dublin", "Alicante", "3", "EUR");
   //TODO: not always available sharedDescribe("Dublin", "Alicante", "3", "EUR");
    sharedDescribe("Liverpool", "Alicante", "2", "EUR");
    sharedDescribe("Liverpool", "Alicante", "3", "EUR");
    sharedDescribe("Alicante", "Liverpool", "2", "GBP");
    sharedDescribe("Alicante", "Liverpool", "3", "GBP");
});
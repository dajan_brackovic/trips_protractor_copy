var sprintf = require("sprintf").sprintf;


function sharedDescribe(from, to, weekday) {
            describe(sprintf("Select a valid fare in fly out week view after search flight from %s to %s, the selection week day index  %s", from, to, weekday), function () {

                it('Given I on FOH timetable page', function () {
                    actions.timetableActions.goToPage();
                });

                it(sprintf("Fill in from and to airports and search"), function () {
                    actions.timetableActions.searchTimeTable(from, to);
                });

                it(sprintf("Click on weekly view in fly out section "), function () {
                    actions.timetableActions.clickWeeklyViewButtonFlyout();

                });
                it(sprintf("Verify the day and date are same as monthly view shows"), function () {
                    actions.timetableActions.verifyDefaultWeeklyView();

                });
                it(sprintf("Select a valid flyout day which is not today, verify the fare widget shows the fare of this selected flyout day%s", weekday), function () {
                    actions.timetableActions.selectNextWeekInFlyout();
                    actions.timetableActions.selectFlyOutWeeklyCalendarVerifyInWidget(weekday);
                });

            });
    }

describe('C40535-Select a valid fare option from fly out available fares in weekly view- From- to-WeekDayNubmer(***Due wont fix bug DF-2534 until the time when trips api and foh api match***)', function () {
    sharedDescribe("Dublin", "Alicante", "2");
    sharedDescribe("Dublin", "Alicante", "3");
    sharedDescribe("Dublin", "Alicante", "4");//DF-2534
    sharedDescribe("Dublin", "Alicante", "5");
    sharedDescribe("Dublin", "Alicante", "6");
    sharedDescribe("Dublin", "Alicante", "7");
    sharedDescribe("Dublin", "Alicante", "8");

});
var sprintf = require("sprintf").sprintf;


function sharedDescribe(from, to, next, previous, monthIndex, dayIndex) {
    describe(sprintf("Select a valid fare from fly out  calendar in month view after search flight from %s to %s, using month index %s and  day index  %s", from, to, next, previous, monthIndex, dayIndex), function () {
        it('Given I on FOH timetable page', function () {
            actions.timetableActions.goToPage();
        });

        it(sprintf("Fill in from and to airports and search"), function () {
            actions.timetableActions.searchTimeTable(from, to);
        });

        it(sprintf("Move to next month %s times and then select a month %s and a day %s", next, monthIndex, dayIndex), function () {
            for (var count = 0; count < next; count++) {
                actions.timetableActions.selectNextMonthInFlyout();
            }
            actions.timetableActions.selectFlyOutMonthlyCalendarVerifyInWidget(monthIndex, dayIndex);

        });

        it(sprintf("Move back to previous month %s times and then select a month %s and a  day %s", previous, monthIndex, dayIndex), function () {
                actions.timetableActions.selectMonthInFlyoutByIndex(0);
                actions.timetableActions.selectFlyOutMonthlyCalendarVerifyInWidget(monthIndex, dayIndex);
        });

    });

}

describe('C40534  Fly out calendar Carousel monthly, from, to, next,previous, monthIndex, dayIndex', function () {
    sharedDescribe("Dublin", "Alicante", "3", "2", "3", "1");
    sharedDescribe("Dublin", "Alicante", "4", "2","3", "3");
    //TODO: To cover more later when we get  data matching builds
    //sharedDescribe("Dublin", "Alicante", "5", "1","4", "4");
    //sharedDescribe("Dublin", "Alicante", "6", "3","5", "5");
    //sharedDescribe("Dublin", "Alicante", "6", "4","6", "6");

});
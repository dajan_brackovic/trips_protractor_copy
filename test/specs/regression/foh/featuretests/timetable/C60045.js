var sprintf = require("sprintf").sprintf;


function sharedDescribe(from, to, next, previous, monthIndex, dayIndex) {
    describe(sprintf("Search from %s to %s, move to next %s months and then move to previous %s months in fly out calendar, then select month %s and day %s", from, to, next, previous, monthIndex, dayIndex), function () {
        it('Given I on FOH timetable page', function () {
            actions.timetableActions.goToPage();
        });

        it(sprintf("Fill in from and to airports and search"), function () {
            actions.timetableActions.searchTimeTable(from, to);
        });

        it(sprintf("Move to next month %s times and then select a month %s and a day %s", next, monthIndex, dayIndex), function () {
            var count;
            for (count = 0; count < next; count++) {
                actions.timetableActions.selectNextMonthInFlyback();
                actions.timetableActions.selectFlyBackMonthlyCalendarVerifyInWidget(monthIndex, dayIndex);
            }
        });

        it(sprintf("Move back to previous month %s times and then select a month %s and a  day %s", previous, monthIndex, dayIndex), function () {
            var count;
            for (count = 0; count < previous; count++) {
                actions.timetableActions.selectPreviousMonthInFlyback();
                actions.timetableActions.selectFlyBackMonthlyCalendarVerifyInWidget(monthIndex, dayIndex);
            }
        });

    });

}

describe('C60045  Flyback calendar Carousel monthly, from, to, next,previous, monthIndex, dayIndex (***Due wont fix bug DF-2534 until the time when trips api and foh api match***)', function () {
    sharedDescribe("Dublin", "Alicante", "3", "2", "3", "8");
    sharedDescribe("Dublin", "Liverpool", "3", "2","3", "5");
    sharedDescribe("Dublin", "Liverpool", "5", "5","4", "4");
    sharedDescribe("Dublin", "Alicante", "6", "3","5", "5");
    sharedDescribe("Dublin", "Alicante", "7", "4","6", "6");

});
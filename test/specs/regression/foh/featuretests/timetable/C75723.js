var sprintf = require("sprintf").sprintf;


function sharedDescribe(from, to, column, currency) {
    describe(sprintf("Select flight from weekly view fly out calendar and verify in booking, [%s][%s][%s][%s]  ", from, to, column, currency), function () {
        it('Given I on FOH timetable page', function () {
            actions.timetableActions.goToPage();
        });

        it(sprintf("Fill in from and to airports and search"), function () {
            actions.timetableActions.searchTimeTable(from, to);

        });
        it(sprintf("Click on weekly view in fly out section "), function () {
            actions.timetableActions.clickWeeklyViewButtonFlyout();

        });
        it(sprintf("Select a valid day then verify the data including currency"), function () {
            actions.timetableActions.selectNextWeekInFlyout();
            actions.timetableActions.selectFlyOutWeeklyCalendarVerifyInBookingDetails(from, to, column, currency);

        });

    });

}

describe('C75723  Select flight from weekly view fly out view and verify in booking, from, to, column, currency ', function () {
    sharedDescribe("Alicante", "Liverpool", "2", "EUR");
    sharedDescribe("Alicante", "Liverpool", "3", "EUR");
    sharedDescribe("Liverpool", "Alicante", "3", "GBP");
    //TODO: option not always available sharedDescribe("Dublin", "Alicante", "2", "EUR");
    //TODO: option not always available  sharedDescribe("Dublin", "Alicante", "1", "EUR");
    //TODO: option not always available sharedDescribe("Liverpool", "Alicante", "2", "GBP");

});
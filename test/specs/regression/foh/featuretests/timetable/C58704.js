var sprintf = require("sprintf").sprintf;


function sharedDescribe(from, to, weekday) {
    describe(sprintf("Select a valid fare in fly out week view after search flight from %s to %s, the selection week day index  %s", from, to, weekday), function () {

        it('Given I on FOH timetable page', function () {
            actions.timetableActions.goToPage();
        });

        it(sprintf("Fill in from and to airports and search"), function () {
            actions.timetableActions.searchTimeTable(from, to);
        });

        it(sprintf("Click on weekly view in fly back section"), function () {
            actions.timetableActions.clickWeeklyViewButtonFlyback();
        });

        it(sprintf("Select a valid flyout day which is not today, verify the fare widget shows the fare of this selected flyout day %s", weekday), function () {
            actions.timetableActions.selectNextWeekInFlyback();
            actions.timetableActions.selectFlyBackWeeklyCalendarVerifyInWidget(weekday);
        });

    });
}

describe('C58704-Select a valid fare option from flyback available fares in weekly view- From- to-WeekDayNubmer ', function () {
    sharedDescribe("Dublin", "Alicante", "2");
    sharedDescribe("Dublin", "Alicante", "3");
    sharedDescribe("Dublin", "Alicante", "8");

});
var sprintf = require("sprintf").sprintf;


function sharedDescribe(from, to, monthIndex, currency) {
    describe(sprintf("Select a valid fare from fly back monthly calendar  from %s to %s, using month index %s and  day index  %s", from, to, monthIndex, currency), function () {
        it('Given I on FOH timetable page', function () {
            actions.timetableActions.goToPage();
        });

        it(sprintf("Fill in from and to airports and search"), function () {
            actions.timetableActions.searchTimeTable(from, to);
        });
        it(sprintf("Select next month in Flyback section"), function () {
            actions.timetableActions.selectNextMonthInFlyback();
        });
        it(sprintf("Verify booking details correctly shown"), function () {
            actions.timetableActions.selectFlyBackMonthlyCalendarVerifyInBookingDetails(from, to, monthIndex, currency);
        });
    });

}

describe('C73854 	Select flight from monthly view fly back calendar and verify in booking, from, to, monthIndex, currency', function () {
    sharedDescribe("London (STN)", "Alicante", "2", "EUR");
    sharedDescribe("Alicante", "London (STN)", "2", "GBP");
    //TODO: keep typical ones and leave these as add up tests
    //sharedDescribe("Alicante", "London (STN)", "3", "GBP");
    //sharedDescribe("London (STN)", "Alicante", "3", "EUR");
    //sharedDescribe("Dublin", "Paris (BVA)", "2", "EUR");
    //sharedDescribe("Dublin", "Paris (BVA)", "3", "EUR");


});
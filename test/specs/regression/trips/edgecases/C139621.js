var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;


function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
    describe("(shared)", function () {
        var outBoundDaysFromNow = 3;//today index === 1
        var returnDaysFromNow = 7; //today index === 1
        var origin = "mad";
        var destination = "dub";
        var fareType = "standard";
        var tripWay = "twoway";

        var bookFlight = function (paxMap) {
            var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType,tripWay);
            actions.extrasActions.skipExtras();
            actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
            actions.addPaxActions.clickBtnAddPaxSave();
            actions.addPaxActions.addContact();
            actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
        }


        describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

            it(sprintf('Given I make a standard return trip with outbound > 1 < 7Days and return = 7 days from now with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
                var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
                bookFlight(paxMap);
            });

            it('Then I should get a booking ref', function () {
                actions.bookingSummaryActions.verifyConfirmationMessage();
            });

            it('When on Manage Trip Page I click on Ryanair Home Page logo to return to home page', function () {
                actions.bookingSummaryActions.clickRyanairLogo();
            });

            it(sprintf('Then I make another booking - a Standard return trip with Outbound = 7 day and Return = 30 days from now with 1 adult, 0 teen, 0 children, 0 infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
                outBoundDaysFromNow = 4;//today index === 1
                returnDaysFromNow = 6; //today index === 1
                origin = "stn";
                destination = "opo";

                var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
                bookFlight(paxMap);
            });

            it('Then I should get a booking ref', function () {
                actions.bookingSummaryActions.verifyConfirmationMessage();
            });

        });

    });
}

describe('TRIPS | Edge Case | C139621 |  2 x Standard TwoWay bookings in one session | Standard', function () {
    sharedDescribe(1, 0, 0, 0);
});
var Trip = require('../../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;


function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
        var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE");
        var origin = "Stn";
        var destination = actions.tripHelper.getRandomAirportFromArray();
        var fareType = "business";
        var tripWay = "oneway";
        var trip;

        var bookFlight = function (paxMap) {
            trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
            actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        };

        describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

            it(sprintf('Given I make a business oneWay trip with outbound 7 days from now with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
                var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
                bookFlight(paxMap);
            });

            it('When I select a seat', function () {
                actions.seatsActions.selectMultipleSeats(tripWay, adultNumber, teenNumber, childrenNumber, infantsNumber);
            });

            it('Then I should confirm business seat price', function () {
                actions.seatsActions.confirmBusinessSeatPrice();
            });

            it('Then I should pay for booking', function () {
                actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
                actions.addPaxActions.addContact();
                actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
                actions.addPaxActions.enterBillingAddress();
                actions.addPaxActions.clickPaymentContinue();
            });

            it('Then I should get a booking ref', function () {
                actions.bookingSummaryActions.verifyConfirmationMessage();
            });

        });

}

describe('TRIPS | Simple Booking | C28738 | Outbound = 7 Days | Business', function () {

    sharedDescribe(3, 0, 0, 0);

    sharedDescribe(1, 1, 0, 1);

});
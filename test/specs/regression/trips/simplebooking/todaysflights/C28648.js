var Trip = require('../../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;


function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
    describe("(shared)", function () {
        var outBoundDaysFromNow = 1;//today index === 1
        var returnDaysFromNow = 5; //today index === 1
        var origin = "dub";
        var destination = "stn";
        var fareType = "standard";
        var tripWay = "twoway";

        var bookFlight = function (paxMap) {
            var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.fOHActions.confirmTodayAlert();
            actions.tripsHomeActions.selectAFlight(0, 0, fareType,tripWay);
            actions.extrasActions.skipExtras();
            actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
            actions.addPaxActions.addContact();
            actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        };

        describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

            it(sprintf('Given I make a standard return trip with outbound 0 and return < 7days from now with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
                var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
                bookFlight(paxMap);
            });

            it('Then I should get a booking ref', function () {
                actions.bookingSummaryActions.verifyConfirmationMessage();
            });

        });

    });
}

describe('TRIPS | Todays Flight | Simple Booking | C28648 | OB Today | Return < 7 Days | Standard', function () {

    sharedDescribe(1, 0, 0, 0);

    sharedDescribe(1, 1, 0, 1);

});
var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
        var outBoundDaysFromNow = 4; //today index === 1
        var origin = "Dub";
        var destination = "bcn";
        var fareType = "standard";
        var tripWay = "oneway";
        var trip;

        var bookFlight = function (paxMap) {
            trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
            actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        };

        describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

            it(sprintf('Given I make a standard one way trip with outbound 4 days from now with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
                var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
                bookFlight(paxMap);
            });

            it('Then I continue on extras page', function () {
                actions.extrasActions.skipExtras();
            });

            it('Then I should enter duplicate passenger names', function () {
                trip.journey.paxList[0].title = 'Mr';
                trip.journey.paxList[0].firstName = 'TestFirst';
                trip.journey.paxList[0].lastName = 'TestLast';
                trip.journey.paxList[1].title = 'Mr';
                trip.journey.paxList[1].firstName = 'TestFirst';
                trip.journey.paxList[1].lastName = 'TestLast';
                actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
                //actions.addPaxActions.clickBtnAddPaxSave();
                actions.addPaxActions.addContact();
                actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
                actions.addPaxActions.enterBillingAddress();
                actions.addPaxActions.clickPaymentContinue();
            });

            it('Then I assert on duplicate passenger name error is displayed', function () {
                actions.addPaxActions.assertOnDuplicatePaxNameError();
            });
        });
}


describe('TRIPS | Payment Validation | C599507 | Ensure Duplicate Pax name can not be entered', function () {

    sharedDescribe(2, 0, 0, 0);

});
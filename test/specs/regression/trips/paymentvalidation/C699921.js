var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {

    var outBoundDaysFromNow = 3; //today index === 1
    var returnDaysFromNow = 5;
    var origin = "dub";
    var destination = "stn";
    var fareType = "standard";
    var tripWay = "twoway";
    var trip;

    var bookFlight = function (paxMap) {
        trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        actions.extrasActions.skipExtras();
        actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
        actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
        actions.addPaxActions.enterBillingAddress();
    }

    describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

        it(sprintf('Given I make a Standard TWOWAY trip from ORIGIN to DESTINATION with Outbound x days from now with 1 adult, 0 teen, 0 children, 1 infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
            var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
            bookFlight(paxMap);
        });

        it('When I enter registered email address at Payment Page', function () {
            actions.addPaxActions.addContact();
        });

        it('Then I verify that the Sign-up to MyFr Dialog and Checkbox does NOT appear at bottom of payment page.  ', function () {
            actions.addPaxActions.assertOnMyFrSignupNotPresent();
        });

        it('Then I Click Pay Now', function () {
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
        });

    });
};

describe('TRIPS | Payment Validation | C699921 | Assert MyFr SignUp Pop Up is Not Present on checkout page when registered email is entered', function () {

    sharedDescribe(1, 0, 0, 1);
});


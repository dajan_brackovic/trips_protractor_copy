//var Pages = require('../../../../Pages')
//var pages = new Pages();
var Trip = require('../../../../shared/model/Trip');

describe('TRIPS | PaymentValidation | DCC validation | C37280', function () {
    var outBoundDaysFromNow = 2; //today index === 1
    var origin = "dub";
    var destination = "stn";
    var fareType = "standard";
    var tripWay = "oneway";
    var originCurrency = "EUR";
    var cardCurrency = "AUD";
    var trip;


    var bookFlight = function (paxMap) {
        trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
        actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        actions.extrasActions.skipExtras();
        actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
        //actions.addPaxActions.clickBtnAddPaxSave();
        actions.addPaxActions.addContact();
        trip.bookingContact.card.cardNumber = "5407581111111115";

        actions.addPaxActions.makeDCCCardPayment(trip.bookingContact.card);
        actions.addPaxActions.enterBillingAddress();


    }

    describe('1 adult, 0 teen, 0 children, 0 infants', function () {

        it('Given I make a standard oneWay trip with outbound 2 days from now with 1 adult, 0 teen, 0 children, 0 infants', function () {
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            bookFlight(paxMap);
        });

        it('Then Dcc field should appear containing both original and converted currencies', function () {
            actions.addPaxActions.verifyDccField(originCurrency, cardCurrency);
        });

        it('Then I click pay now ', function () {
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
        });
    });
});
var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {

    var outBoundDaysFromNow = 2; //today index === 1
    var origin = "opo";
    var destination = "stn";
    var fareType = "standard";
    var tripWay = "oneway";
    var trip;

    var bookFlight = function (paxMap) {
        trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
        actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        actions.extrasActions.skipExtras();
        actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
        actions.addPaxActions.addContact();
        trip.bookingContact.card.cardNumber = "5100000014101198";
        actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
        actions.addPaxActions.enterBillingAddress();
        actions.addPaxActions.clickPaymentContinue();

    }

    describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

        it(sprintf('Given I make a standard oneWay trip with 7 < outbound < 30days from now with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
            var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
            bookFlight(paxMap);
        });

        it('When I pay with Declined Card details then I see Payment Validations error message', function () {
            actions.addPaxActions.assertOnDeclinedPaymentError();
        });

        it('Then I enter Declined Card details for second time then I see Payment Validations error message', function () {
            actions.addPaxActions.makeCardPaymentWithoutContinuing(trip.bookingContact.card);
            actions.addPaxActions.clickPaymentContinueOnly();
            actions.addPaxActions.assertOnPaymentDeclinedPopUpButton();
        });

        it('Then I click on Payment Declined button', function () {
            actions.addPaxActions.clickOnPaymentDeclinedPopUpButton();
        });

        it('Then I redirected to homepage', function () {
            actions.fOHActions.assertOnHomePage();
        });

    });
};

describe('TRIPS | Payment Validation | C635028 | Declined Card Validation 2 Attempts', function () {

    sharedDescribe(1, 0, 0, 0);
});
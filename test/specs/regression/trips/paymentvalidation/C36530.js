var Trip = require('../../../../shared/model/Trip');

describe('TRIPS | PaymentValidation | Declined card validation | C36530', function () {
    var outBoundDaysFromNow = 2; //today index === 1
    var origin = "opo";
    var destination = "stn";
    var fareType = "standard";
    var tripWay = "oneway";
    var trip;
  
    var bookFlight = function (paxMap) {
        trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
        actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        actions.extrasActions.skipExtras();
        actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
        //actions.addPaxActions.clickBtnAddPaxSave();
        actions.addPaxActions.addContact();
        trip.bookingContact.card.cardNumber = "5100000014101198";
        actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
        actions.addPaxActions.enterBillingAddress();
        actions.addPaxActions.clickPaymentContinue();

    }

    describe('1 adult, 0 teen, 0 children, 0 infants', function () {

        it('Given I make a standard oneWay trip with outbound 2 days from now with 1 adult, 0 teen, 0 children, 0 infants', function () {
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            bookFlight(paxMap);
        });

        it('When I pay with Declined Card details then I see Payment Validations error message', function () {
            actions.addPaxActions.assertOnDeclinedPaymentError();
        });

        //TODO - Clarify Declined Payments Expected behaviour after second payment attempt
        //it('Then I enter Declined Card details for second time then I see Payment Validations error message', function () {
        //    actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
        //    actions.addPaxActions.assertOnDeclinedPaymentError();
        //});

    });
});
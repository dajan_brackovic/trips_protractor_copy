var Trip = require('../../../../shared/model/Trip');


describe('TRIPS | PaymentValidation | Invalid card validation | C37322', function () {
    var outBoundDaysFromNow = 2; //today index === 1
    var origin = "brs";
    var destination = "dub";
    var fareType = "standard";
    var tripWay = "oneway";
    
    var bookFlight = function (paxMap) {
        var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
        actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        actions.extrasActions.skipExtras();
        actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
        //actions.addPaxActions.clickBtnAddPaxSave();
        actions.addPaxActions.addContact();
        trip.bookingContact.card.cardNumber = "5210000010001009";
        actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
        actions.addPaxActions.enterBillingAddress();
        actions.addPaxActions.clickPaymentContinue();

    }

    var verifyBookingRef = function () {
        actions.bookingSummaryActions.verifyConfirmationMessage();
    }

    describe('1 adult, 0 teen, 0 children, 0 infants', function () {

        it('Given I make a standard oneWay trip with outbound 2 days from now with 1 adult, 0 teen, 0 children, 0 infants', function () {
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            bookFlight(paxMap);
        });

        it('When I pay with Invalid Card details then I see Payment Validations error message', function () {
            actions.addPaxActions.assertOnPresenceOfInvalidCardMessage();
        });
    });
});
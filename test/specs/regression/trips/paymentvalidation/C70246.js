var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;


function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber, origin, destination) {
        var outBoundDaysFromNow = 4; //today index === 1
        var returnDaysFromNow = 6; //today index === 1
        var fareType = "standard";
        var tripWay = "twoway";

        var bookFlight = function (paxMap) {
            var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
            actions.extrasActions.skipExtras();
            actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
            //actions.addPaxActions.clickBtnAddPaxSave();
            actions.addPaxActions.addContact();

            actions.addPaxActions.makeCardPaymentWithoutContinuing(trip.bookingContact.card);
            actions.addPaxActions.enterBillingAddress();
        }

        describe(sprintf('from %s to %s with %s adult, %s teen, %s children, %s infants', origin, destination, adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

            it(sprintf('Given I create a RT flight from %s to %s with %s adult, %s teen, %s children, %s infants', origin, destination, adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
                var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
                bookFlight(paxMap, origin, destination);
            });

            it('When I enter VAT Details at Payment screen', function () {
                actions.addPaxActions.enterVatDetails();
                actions.addPaxActions.enterVatNumber();
                actions.addPaxActions.enterBusinessName();
                actions.addPaxActions.enterAddressDetailsForVat();
            });

            it('And click pay now', function () {
                actions.addPaxActions.clickPaymentContinue();
            });

            it('Then I should get a booking ref', function () {
                actions.bookingSummaryActions.verifyConfirmationMessage();
            });
        });

}

describe('TRIPS | VAT Domestic Route | C70246 | Outbound & Inbound < 7 | Standard', function () {

    //sharedDescribe(1, 0, 0, 0, "STN", "GLA");
    sharedDescribe(1, 0, 0, 0, "CTA", "FCO");

});
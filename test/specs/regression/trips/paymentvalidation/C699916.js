var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {

    var outBoundDaysFromNow = 2; //today index === 1
    var returnDaysFromNow = 4;
    var origin = "dub";
    var destination = "stn";
    var fareType = "standard";
    var tripWay = "twoway";
    var trip;

    var bookFlight = function (paxMap) {
        trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        actions.extrasActions.skipExtras();
        actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
        actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
        actions.addPaxActions.enterBillingAddress();
    }

    describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

        it(sprintf('Given I make a Standard TWOWAY trip from ORIGIN to DESTINATION with Outbound x days from now with 2 adult, 1 teen, 0 children, 1 infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
            var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
            bookFlight(paxMap);
        });

        it('When I enter unregistered email address at Payment Page', function () {
            actions.addPaxActions.addContactUnregisteredEmail();
        });

        it('Then I verify that the Sign-up to MyFr Dialog and Checkbox appears at bottom of payment page.  ', function () {
            actions.addPaxActions.assertMyFrSignupOptionPopUp();
            actions.addPaxActions.clickMyFrSignupPopUp();
        });

        it('Then I Click Pay Now', function () {
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
        });

    });
};

describe('TRIPS | Payment Validation | C699916 | Assert MyFr SignUp Pop Up is Present on checkout page when unregistered email is entered', function () {

    sharedDescribe(2, 1, 0, 1);
});


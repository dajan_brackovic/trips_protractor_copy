var Trip = require('../../../../../shared/model/Trip');

describe("TRIPS | Potential Trip Baby Equipment | C37483 | Decrease quantity for baby equipment | Return Trip", function () {
    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE");
    var returnDaysFromNow = actions.tripHelper.getRandomDate("PARTIAL");
    var origin = "Ltn";
    var destination = "Dub";
    var fareType = "standard";
    var tripWay = "twoway";

    describe("Case 1: C37483", function () {

        it("Given that the user has at least a quantity of 1 for a baby equipment added", function () {
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
            actions.potentialTripActions.clickAddBabyEquipment();
            browser.sleep(1000);
            actions.babyEquipmentActions.expectBabyEquipmentAmount0(0, "0");
            actions.babyEquipmentActions.selectPlusButtonSameForBothFlights(0);
            actions.babyEquipmentActions.expectBabyEquipmentAmount1(0, "1");
        });
        it("When the - button is selected", function () {
            actions.babyEquipmentActions.selectMinusButtonSameForBothFlights(0);
        });
        it("Then decrease the quantity number by 1 for that baby equipment", function () {
            actions.babyEquipmentActions.expectBabyEquipmentAmount0(0, "0");
        });
    });

    describe("Case 2: C37483", function () {

        it("Given that the user has at least a quantity of 1 for a baby equipment added", function () {
            var paxMap = {ADT: 1, TEEN: 1, CHD: 0, INF: 0};
            actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
            actions.potentialTripActions.clickAddBabyEquipment();
            browser.sleep(1000);
            actions.babyEquipmentActions.expectBabyEquipmentAmount0(0, "0");
            actions.babyEquipmentActions.selectPlusButtonSameForBothFlights(0);
            actions.babyEquipmentActions.expectBabyEquipmentAmount1(0, "1");
        });
        it("When the - button is selected", function () {
            actions.babyEquipmentActions.selectMinusButtonSameForBothFlights(0);
        });
        it("Then decrease the quantity number by 1 for that baby equipment", function () {
            actions.babyEquipmentActions.expectBabyEquipmentAmountNN(0, "0");
        });
    });

    describe("Case 3: C37483", function () {

        it("Given that the user has at least a quantity of 1 for a baby equipment added", function () {
            var paxMap = {ADT: 1, TEEN: 1, CHD: 1, INF: 0};
            actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
            actions.potentialTripActions.clickAddBabyEquipment();
            browser.sleep(1000);
            actions.babyEquipmentActions.expectBabyEquipmentAmount0(0, "0");
            actions.babyEquipmentActions.selectPlusButtonSameForBothFlights(0);
            actions.babyEquipmentActions.expectBabyEquipmentAmount1(0, "1");
        });
        it("When the - button is selected", function () {
            actions.babyEquipmentActions.selectMinusButtonSameForBothFlights(0);
        });

        it("Then decrease the quantity number by 1 for that baby equipment", function () {
            actions.babyEquipmentActions.expectBabyEquipmentAmount0(0, "0");
        });
    });

    describe("Case 4: C37483", function () {

        it("Given that the user has at least a quantity of 1 for a baby equipment added", function () {
            var paxMap = {ADT: 1, TEEN: 1, CHD: 1, INF: 1};
            actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
            actions.potentialTripActions.clickAddBabyEquipment();
            browser.sleep(1000);
            actions.babyEquipmentActions.expectBabyEquipmentAmount0(0, "0");
            actions.babyEquipmentActions.selectPlusButtonSameForBothFlights(0);
            actions.babyEquipmentActions.expectBabyEquipmentAmount1(0, "1");
        });
        it("When the - button is selected", function () {
            actions.babyEquipmentActions.selectMinusButtonSameForBothFlights(0);
        });
        it("Then decrease the quantity number by 1 for that baby equipment", function () {
            actions.babyEquipmentActions.expectBabyEquipmentAmount0(0, "0");
        });
    });
});
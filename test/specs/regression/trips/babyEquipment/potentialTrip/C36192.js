var Trip = require('../../../../../shared/model/Trip');

describe("TRIPS | Potential Trip Baby Equipment | C36192 | Ensure that user already add equipment, remove baby equipment by clicking on the CTA | Return Trip ", function () {
    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE");
    var returnDaysFromNow = actions.tripHelper.getRandomDate("PARTIAL");
    var origin = "Ltn";
    var destination = "Dub";
    var fareType = "standard";
    var tripWay = "twoway";

    describe("Case:C36192 | 1 Adults, 1 Teens, 0 Children, 0 Infants | TwoWay LTN-DUB ", function () {

        it("Given that the user has baby equipment added to the shopping basket already", function () {
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
            actions.potentialTripActions.clickAddBabyEquipment();
            browser.sleep(2000);
            actions.babyEquipmentActions.selectPlusButtonSameForBothFlights(0);
            actions.babyEquipmentActions.selectPlusButtonSameForBothFlights(0);
            actions.babyEquipmentActions.clickConfirmButton();
        });
        it("When the user clicks on the baby card", function () {
            actions.potentialTripActions.clickAddBabyEquipment(5);
        });
        it("Then the drawer opens with the passenger's baby equipment selections", function () {
            actions.babyEquipmentActions.expectConfirmButtonIsDisabled();

        });
        it("When the user decreases the current quantity of baby equipment to 0", function () {
            browser.sleep(2000);
            actions.babyEquipmentActions.selectMinusButtonSameForBothFlights(0);
            actions.babyEquipmentActions.selectMinusButtonSameForBothFlights(0);
            actions.babyEquipmentActions.expectBabyEquipmentAmount0(0,"0");
        });
        it("Then display the CTA in its active state", function () {
            actions.babyEquipmentActions.expectConfirmButtonIsEnabled();
        });
        it("Then enable the user to remove baby equipment by clicking on the CTA", function () {
            actions.babyEquipmentActions.clickConfirmButton();
        });
    });
});
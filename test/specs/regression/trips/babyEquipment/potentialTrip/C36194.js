var Trip = require('../../../../../shared/model/Trip');

describe("TRIPS | Potential Trip Baby Equipment | C36194 | Ensure that when user click CTA button, price will be updated  | Return Trip ", function () {
    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE");
    var returnDaysFromNow = actions.tripHelper.getRandomDate("PARTIAL");
    var origin = "Dub";
    var destination = "Cia";
    var fareType = "standard";
    var tripWay = "twoway";

    describe("Case:C36194 | 1 Adults, 1 Teens, 0 Children, 0 Infants | TwoWay DUB-CIA", function () {

        it("Given that the user has clicked the CTA", function () {
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
            actions.potentialTripActions.clickAddBabyEquipment();
        });
        it("Then the equipment selection(s) will be added to the user's trip", function () {
            browser.sleep(2000);
            actions.babyEquipmentActions.selectPlusButtonSameForBothFlights(0);
            browser.waitForAngular();
        });
        it("Then side drawer will close", function () {
            actions.babyEquipmentActions.clickConfirmButton();
            browser.waitForAngular();
        });
        it("Then shopping basket is updated with the equipment prices", function () {
            actions.potentialTripActions.expectThatShoppingBasketPriceIsUpdatedBaby();
        });
    });
});
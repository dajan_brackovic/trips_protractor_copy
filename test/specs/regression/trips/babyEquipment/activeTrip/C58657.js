describe("TRIPS | Active Trip Baby Equipment | C58657 | Ensure that equipment is added to price brake down", function () {

    var params = browser.params.conf;
    var Trip = require('../../../../../shared/model/Trip');

    describe("Case: C58657 | 1 Adults, 0 Teens, 0 Children, 0 Infants", function () {

        it("Given that user have booked flight and it's on active trip page", function () {
            actions.manageTripsActions.manageTripAccess(params.payment.refNumber1,params.payment.payMail);
        });
        it("Then baby equipment side drawer should be opened", function () {
            actions.potentialTripActions.clickAddBabyEquipment();
        });
        it("When user click to plus button and add baby equipment", function () {
            browser.sleep(2000);
            actions.babyEquipmentActions.selectPlusButtonSameForBothFlights(0);
            actions.babyEquipmentActions.clickConfirmButton();
        });
        it("Then equipment is added to price brake down", function () {
            actions.potentialTripActions.openPriceBreakDownClick();
            actions.potentialTripActions.expectProductCardUpdated(0,1,"20.00","Baby equipment");
        });
        it("Then on baby card should be information that equipment is added", function () {
            actions.potentialTripActions.expectInformationLabelOnCardBaby("2 baby equipment");
            actions.potentialTripActions.modifyButtonCheckBaby();
        });
    });
});
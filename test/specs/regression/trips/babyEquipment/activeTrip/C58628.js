describe("TRIPS | Active Trip Baby Equipment | C58628 | Ensure that passnager names are displayed on baby side drawer", function () {

    var params = browser.params.conf;
    var Trip = require('../../../../../shared/model/Trip');

    describe("Case: C58628 | 1 Adults, 0 Teens, 0 Children, 0 Infants", function () {

        it("Given that user have booked flight and it's on active trip page", function () {
            actions.manageTripsActions.manageTripAccess(params.payment.refNumber1,params.payment.payMail);
        });
        it("Then baby equipment side drawer should be opened", function () {
            actions.potentialTripActions.clickAddBabyEquipment();
        });
        it("Then passanger name should be displayed", function () {
            actions.babyEquipmentActions.expectPassengerTypeAndNumber(0,"arpit test");
        });
    });
});
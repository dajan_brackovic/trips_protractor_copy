describe("TRIPS | Active Trip Baby Equipment | C58635 |  Ensure that check box 'Same for both flights' are checked by default", function () {

    var params = browser.params.conf;
    var Trip = require('../../../../../shared/model/Trip');

    describe("Case: C58635 | 1 Adults, 0 Teens, 0 Children, 0 Infants", function () {

        it("Given that user have booked flight and it's on active trip page", function () {
            actions.manageTripsActions.manageTripAccess(params.payment.refNumber1,params.payment.payMail);
        });
        it("Then baby equipment side drawer should be opened", function () {
            actions.potentialTripActions.clickAddBabyEquipment();
        });
        it("Then checkbox same for both flights should be checked by default", function () {
            //TODO
            //User story has been changed for active trip, so we dont have checkbox there now.
            //This script will be edited when we update test cases
            //actions.babyEquipmentActions.expectCheckBoxSFBFisChecked(0);
        });
    });
});
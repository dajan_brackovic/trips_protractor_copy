var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

// TODO Remove x when Change Flight Feature introduce

function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
        var outBoundDaysFromNow = 2; //today index === 1
        var returnDaysFromNow = 3; //today index === 1
        var origin = "DUB";
        var destination = "Krk";
        var fareType = "standard";
        var tripWay = "twoway";
        var bookingRefActiveTrip;
        var trip;

        var bookFlight = function (paxMap) {
            trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
            actions.extrasActions.skipExtras();
            actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
            actions.addPaxActions.clickBtnAddPaxSave();
            actions.addPaxActions.addContact();
            actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
        };

        xdescribe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

            it(sprintf('Given I make a standard return trip with outbound < 7 Days and return < 7 days from now with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
                var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
                bookFlight(paxMap);
            });

            it('Then I should get a booking ref', function () {
                actions.bookingSummaryActions.verifyConfirmationMessage();
                bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
            });

            it('When I click nmanage your trip button', function () {
                browser.sleep(1000);
                actions.bookingSummaryActions.clickManageYourTripButton();

            });

            it('Then I click on change flight link', function () {
                browser.sleep(1000);
                actions.manageYourTripActions.clickChangeYourFlightLink();
                browser.sleep(1000);
            });


            it('Then I select the flight I want to change', function () {
                actions.manageYourTripActions.clickChangeFlightOutBoundCheckBox();
                actions.manageYourTripActions.clickChangeFlightInBoundCheckBox();
                actions.manageYourTripActions.clickChangeFlightsSearchButton();
            });

            it('Then I change dates', function () {
                actions.fOHActions.chooseDatesReturn(4, 6);
                browser.sleep(1000);
                actions.manageYourTripActions.clickChangeFlightsSearchButton();
            });

            it('Then I select flights dates', function () {
                browser.sleep(10000);
                actions.manageYourTripActions.selectAFlight(0, 0, fareType, tripWay);
                browser.sleep(10000);
                actions.manageYourTripActions.clickChangeFlightsSearchButton();

            });

            it('Then I review my flights', function () {
                browser.sleep(10000);
                // TODO some assertions
                actions.manageYourTripActions.clickChangeFlightsSearchButton();
            });

            it('Then I click continue', function () {
                actions.extrasActions.skipExtras();
            });

            it('Then I pay for price difference', function () {
                actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
            });

            it('Then I should get a booking ref of changed flight', function () {
                actions.bookingSummaryActions.verifyConfirmationMessage();
            });


        });

}

xdescribe('TRIPS | Change Flight | C224637 | outbound < 7 Days | Return < 7 Days | Standard | Not Checked In', function () {

    sharedDescribe(1, 0, 0, 0);

});
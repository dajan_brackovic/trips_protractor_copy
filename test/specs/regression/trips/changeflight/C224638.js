var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

// TODO complete spec when Change Flight Feature introduce

function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
        var outBoundDaysFromNow = 2; //today index === 1
        var origin = "Krk";
        var destination = "Dub";
        var fareType = "standard";
        var tripWay = "oneway";
        var bookingRefActiveTrip;

        var bookFlight = function (paxMap) {
            var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
            actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType,tripWay);
            actions.extrasActions.skipExtras();
            actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
            actions.addPaxActions.clickBtnAddPaxSave();
            actions.addPaxActions.addContact();
            actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
        };

       xdescribe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

            it(sprintf('Given I make a standard oneway trip with outbound < 7 Days from now with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
                var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
                bookFlight(paxMap);
            });

            it('Then I should get a booking ref', function () {
                actions.bookingSummaryActions.verifyConfirmationMessage();
                bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
            });

            it('When I click checkIn button', function () {
                browser.sleep(1000);
                actions.bookingSummaryActions.clickManageYourTripButton();
                browser.sleep(1000);
                actions.manageYourTripActions.clickChangeYourFlightLink();
                browser.sleep(1000);
                actions.fOHActions.chooseDatesOneWay(4);
                browser.sleep(1000);
                actions.manageYourTripActions.clickChangeFlightsSearchButton();
                browser.sleep(10000);
                actions.manageYourTripActions.selectAFlight(0, 0, fareType,tripWay);
                browser.sleep(10000);
            });

            //it('Then I fill Check In id documents', function () {
            //    actions.checkInActions.selectNationalityDropDown();
            //    actions.checkInActions.enterDateOfBirth();
            //    actions.checkInActions.enterDocumentType(1);
            //    actions.checkInActions.enterDocumentNumberField();
            //    actions.checkInActions.enterCountryOfIssueDropDown();
            //    actions.checkInActions.enterExpiryDate();
            //});
            //
            //it('Then I click continue after adding id documents on check-in', function () {
              //  actions.checkInActions.clickBtnContinueCheckIn();
            //});
            //
            //it('Then I complete second step of check-in', function () {
            //    actions.checkInActions.clickSelectFlightCheckbox();
            //    actions.checkInActions.clickBtnContinueCheckIn();
            //});
            //
            //it('Then I get Boarding Pass Ref', function () {
            //    actions.checkInActions.assertOnBookingRef(bookingRefActiveTrip);
            //});
            //
            //it('Then I get auto allocated seat', function () {
            //    actions.checkInActions.assertOnAutoAllocation();
            //});
        });

}

xdescribe('TRIPS | Change Flight | C224638 | outbound  < 7 Days | Standard | Not Checked In', function () {

    sharedDescribe(1, 0, 0, 0);

});
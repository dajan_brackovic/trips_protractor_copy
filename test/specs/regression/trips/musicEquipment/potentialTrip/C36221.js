describe("TRIPS | Potential Trip Music Equipment | C36221 | Ensure that when user click decrease button take down quantity one | Return Trip", function () {

    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE");
    var returnDaysFromNow = actions.tripHelper.getRandomDate("PARTIAL");
    var origin = "LTN";
    var destination = "DUB";
    var fareType = "standard";
    var tripWay = "twoway";

    describe("Case:C36221 | 1 Adults, 0 Teens, 0 Children, 0 Infants | Return DUB-CIA ", function () {

        it("Given that user is on equipment side drawer for music card", function () {
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
            actions.potentialTripActions.clickAddMusicEquipment();
        });
        it("And that the user has at least 1 music equipment added", function () {
            browser.sleep(1000);
            actions.musicEquipmentActions.expectMusicEquipmentAmount0(0, "0");
            actions.musicEquipmentActions.selectPlusButtonSameForBothFlights(0);
            actions.musicEquipmentActions.expectMusicEquipmentAmount1(0, "1");
        });
        it("When the - button is selected", function () {
            actions.musicEquipmentActions.selectMinusButtonSameForBothFlights(0);
        });
        it("Then decrease the quantity number by 1 for music equipment", function () {
            browser.sleep(1000);
            actions.musicEquipmentActions.expectMusicEquipmentAmount0(0,"0");
        });
        it("Then disable - button", function () {
            actions.musicEquipmentActions.minusButtonDisabledSFBF(1);
        });
    });
});

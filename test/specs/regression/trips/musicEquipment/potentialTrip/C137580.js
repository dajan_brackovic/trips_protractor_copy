describe("TRIPS | Potential Trip Music Equipment | C137580 | Ensure that when user change equipment and click on CTA update total cost price | Return Trip", function () {

    var params = browser.params.conf;
    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE");
    var returnDaysFromNow = actions.tripHelper.getRandomDate("PARTIAL");
    var origin = "LTN";
    var destination = "DUB";
    var fareType = "standard";
    var tripWay = "twoway";

    describe("Case:C137580 | 1 Adults, 0 Teens, 0 Children, 0 Infants | Return DUB-CIA", function () {

        it("Given that changes have been made to the selections in music equipment", function () {
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
            actions.potentialTripActions.clickAddMusicEquipment();
            actions.musicEquipmentActions.expectMusicEquipmentAmount0(0,"0");
            actions.musicEquipmentActions.selectPlusButtonSameForBothFlights(0);
            actions.musicEquipmentActions.expectMusicEquipmentAmount1(0,"1");
            actions.musicEquipmentActions.clickConfirmButton();
        });
        it("When the user has modified their selection (Increased)", function () {
            actions.potentialTripActions.clickAddMusicEquipment();
            actions.musicEquipmentActions.expectMusicEquipmentAmount1(0, "1");
            actions.musicEquipmentActions.selectPlusButtonSameForBothFlights(0);
            actions.musicEquipmentActions.expectMusicEquipmentAmount1(0, "2");
            actions.musicEquipmentActions.clickConfirmButton();
        });
        it("Then shopping basket is updated with the equipment prices", function () {
            actions.potentialTripActions.openPriceBreakDownClick();
            actions.potentialTripActions.expectProductCardUpdated(0,3,"200.00","Music equipment");
        });
        it("Then information message appears on the '2nd line Header - ( Product has been added - anything else you need - Copy may not be correct)", function () {
            actions.potentialTripActions.expectInformationMessageAddedPriceShown();
            actions.potentialTripActions.expectInformationMessageAddedPriceMessageText(params.trip.messageAfterMusic);
        });
        it("Then product card has been update - with product that has been added", function () {
            actions.potentialTripActions.expectInformationLabelOnCardMusic("4 music equipment");
            actions.potentialTripActions.modifyButtonCheckMusic();
        });
    });

});
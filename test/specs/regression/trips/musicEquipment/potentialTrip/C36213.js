describe("TRIPS | Potential Trip Music Equipment | C36213 |  Ensure that it's allow the user to amend selections for music equipment (music)| Return Trip", function () {

    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE");
    var returnDaysFromNow = actions.tripHelper.getRandomDate("PARTIAL");
    var origin = "LTN";
    var destination = "DUB";
    var fareType = "standard";
    var tripWay = "twoway";

    describe("Case:C36213 | 1 Adults, 0 Teens, 0 Children, 0 Infants | Return LTN-DUB", function () {

        it("Allow the user to amend selections for music equipment", function () {
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
            actions.potentialTripActions.clickAddMusicEquipment();
            browser.sleep(1000);
            actions.musicEquipmentActions.expectMusicEquipmentAmount0(0,"0");
            actions.musicEquipmentActions.selectPlusButtonSameForBothFlights(0);
            actions.musicEquipmentActions.expectMusicEquipmentAmount1(0,"1");
            actions.musicEquipmentActions.clickConfirmButton();
            actions.potentialTripActions.clickAddMusicEquipment();
        });
        it("When increase quantity for music equipment DT-1392", function () {
            actions.musicEquipmentActions.expectMusicEquipmentAmount1(0,"1");
            browser.sleep(1000);
            actions.musicEquipmentActions.selectPlusButtonSameForBothFlights(0);
            actions.musicEquipmentActions.expectMusicEquipmentAmount1(0,"2");
            browser.sleep(1000);
            actions.musicEquipmentActions.selectMinusButtonSameForBothFlights(0);
            actions.musicEquipmentActions.expectMusicEquipmentAmount1(0,"1");
        });
        it("Then for round trips - to change states of Same for inbound flight DT-1382" , function () {
            actions.musicEquipmentActions.selectCheckBoxSFBF(0);
            browser.sleep(1000);
            actions.musicEquipmentActions.assertOnAmountOfEquipmentFlightOut(0, "1");
            actions.musicEquipmentActions.assertOnAmountOfEquipmentFlightBack(0, "0");
            actions.musicEquipmentActions.expectCentralPriceLeft(0,"50.00");
            actions.musicEquipmentActions.expectCentralPriceRight(0,"0.00");
        });
    });
});
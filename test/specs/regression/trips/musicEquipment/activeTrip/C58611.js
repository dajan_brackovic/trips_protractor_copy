describe("TRIPS | Active Trip Music Equipment | C58611 |  Ensure that total price is updated on music side drawer ", function () {

    var params = browser.params.conf;
    var Trip = require('../../../../../shared/model/Trip');

    describe("Case:C58611 | 1 Adults, 0 Teens, 0 Children, 0 Infants | DUB-CIA oneWay", function () {

        it("Given that user have booked flight and it's on active trip page", function () {
            actions.manageTripsActions.manageTripAccess(params.payment.refNumber1, params.payment.payMail);
        });
        it("Then music equipment side drawer is opened", function () {
            actions.potentialTripActions.clickAddMusicEquipment();
        });
        it("When add music equipment for passanger", function () {
            browser.sleep(2000);
            actions.musicEquipmentActions.selectPlusButtonSameForBothFlights(0);
        });
        it("Then total price should be updated", function () {
            actions.musicEquipmentActions.expectTotalPriceToBe(0, "100.00");
        });
    });

});
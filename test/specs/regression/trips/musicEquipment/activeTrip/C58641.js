describe("TRIPS | Active Trip Music Equipment | C58641 | Ensure that equipment is added to price brake down", function () {

    var params = browser.params.conf;
    var Trip = require('../../../../../shared/model/Trip');

    describe("Case:C58641 | 1 Adults, 0 Teens, 0 Children, 0 Infants | DUB-CIA oneWay", function () {

        it("Given that user have booked flight and it's on active trip page", function () {
            actions.manageTripsActions.manageTripAccess(params.payment.refNumber8, params.payment.payMail);
        });
        it("Then music equipment side drawer is opened", function () {
            actions.potentialTripActions.clickAddMusicEquipment();
        });
        it("When user click to plus button and add music equipment and CTA", function () {
            browser.sleep(2000);
            actions.musicEquipmentActions.selectPlusButtonSameForBothFlights(0);
            actions.musicEquipmentActions.clickConfirmButton();
        });
        it("Then equipment is added to price brake down", function () {
            actions.potentialTripActions.openPriceBreakDownClick();
            actions.potentialTripActions.expectProductCardUpdated(0, 1, "100.00", "Music equipment");
            actions.potentialTripActions.openPriceBreakDownClick();
        });
        it("Then on music card should be information that equipment is added", function () {
            actions.musicEquipmentActions.expectMusicEquipmentAdded("2");
        });
    });

});
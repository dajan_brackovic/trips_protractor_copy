describe("TRIPS | Active Trip Music Equipment | C58613 | Ensure that check box 'Same for both flights' are checked by default", function () {

    var params = browser.params.conf;
    var Trip = require('../../../../../shared/model/Trip');

    describe("Case:C58613 | 1 Adults, 0 Teens, 0 Children, 0 Infants | DUB-CIA oneWay", function () {

        it("Given that user have booked flight and it's on active trip page", function () {
            actions.manageTripsActions.manageTripAccess(params.payment.refNumber1, params.payment.payMail);
        });
        it("Then music equipment side drawer is opened", function () {
            actions.potentialTripActions.clickAddMusicEquipment();
        });
        it("Then checkbox same for both flights should be checked by default", function () {
            actions.musicEquipmentActions.expectCheckBoxSFBFisChecked(0);
        });
    });

});
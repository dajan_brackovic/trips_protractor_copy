describe("TRIPS | Active Trip Samsonite | C83986 | Ensure that user can add bags", function () {

    var params = browser.params.conf;
    var Trip = require('../../../../../shared/model/Trip');
    var Pages = require('../../../../../Pages')
    var pages = new Pages();

    describe("Case:C83986 | 1 Adults, 0 Teens, 0 Children, 0 Infants | DUB-CIA oneWay", function () {

        it("Given that user have booked flight and it's on active trip page", function () {
            actions.manageTripsActions.manageTripAccess(params.payment.refNumber1, params.payment.payMail);
        });
        it("Then user is on Samsonite product page", function () {
            actions.potentialTripActions.clickOpenSamsoniteInNewTab();
        });
        it("When user click plus button for one of the bags", function () {
            browser.sleep(2000);
            actions.samsoniteActions.clickAddSamsoniteBags(0);
        });
        it("Then amount should be incremented by 1", function () {
            actions.samsoniteActions.expectAmountSamsonite(0, "1");
        });
    });

});
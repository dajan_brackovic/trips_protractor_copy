describe("TRIPS | Active Trip Samsonite | C83996 | Ensure that total price is updated when user remove bag", function () {

    var params = browser.params.conf;
    var Trip = require('../../../../../shared/model/Trip');
    var Pages = require('../../../../../Pages')
    var pages = new Pages();

    describe("Case:C83996 | 1 Adults, 0 Teens, 0 Children, 0 Infants | DUB-CIA oneWay", function () {

        it("Given that user have booked flight and it's on active trip page", function () {
            actions.manageTripsActions.manageTripAccess(params.payment.refNumber1, params.payment.payMail);
        });
        it("Then user is on Samsonite product page", function () {
            actions.potentialTripActions.clickOpenSamsoniteInNewTab();
        });
        it("When user click minus button for one of the bags", function () {
            browser.sleep(2000);
            actions.samsoniteActions.clickAddSamsoniteBags(0);
            browser.sleep(1000);
            actions.samsoniteActions.clickRemoveSamsoniteBags(0);
        });
        it("Then subtotal price should be updated", function () {
            actions.samsoniteActions.expectSubtotalSamsonite("0.00");
        });
    });

});
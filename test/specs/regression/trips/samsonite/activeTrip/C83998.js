describe("TRIPS | Active Trip Samsonite | C83998 | Ensure that popup is displayed after user want to navigate away from page", function () {

    var params = browser.params.conf;
    var Trip = require('../../../../../shared/model/Trip');
    var Pages = require('../../../../../Pages');
    var pages = new Pages();

    describe("Case:C83998 | 1 Adults, 0 Teens, 0 Children, 0 Infants | DUB-CIA oneWay", function () {

        it("Given that user have booked flight and it's on active trip page", function () {
            actions.manageTripsActions.manageTripAccess(params.payment.refNumber1, params.payment.payMail);
        });
        it("Then user is on Samsonite product page", function () {
            actions.potentialTripActions.clickOpenSamsoniteInNewTab();
        });
        it("And user has added bag to basket", function () {
            actions.samsoniteActions.clickAddSamsoniteBags(1);
        });
        it("When user attempts to navigate away from page", function () {
            actions.samsoniteActions.navigateAway();
        });
        it("Then display warning pop up", function () {
            actions.samsoniteActions.expectPopUpIsDisplayed();
        });
    });

});
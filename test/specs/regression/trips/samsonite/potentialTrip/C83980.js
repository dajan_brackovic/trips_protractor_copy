var Trip = require('../../../../../shared/model/Trip');

function sharedDescribe(menuIndex, subMenuIndex) {
    describe("", function () {

        it("Given that user is on Samsonite product page from clicking on link in global header", function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.clickLinkFromMenuLinkOption(menuIndex, subMenuIndex);
            actions.waitHelper.waitForPageTitle("Ryanair");
        });
        it("When user select to view FAQ's", function () {
            actions.samsoniteActions.clickExpandQuestions(0);
        });
        it("Then FAQ's should be displayed", function () {
            actions.samsoniteActions.expectSamsoniteFAQAnswer1IsDisplayed();
        });

    });
}

describe("TRIPS | Potential trip Samsonite | C83980 | Ensure that user can select to view FAQ's", function () {
    //0 = Plan a trip menu, 16 = Samsonite option
    sharedDescribe(0, 16);
});
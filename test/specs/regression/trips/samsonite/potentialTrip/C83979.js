var Card = require('../../../../../shared/model/bookingContact/MasterCard.js');
var card = new Card("Successful");

describe("TRIPS | Potential trip Samsonite | C83979 | Ensure that upon selecting / removing bags the total price for all bags will be displayed beside the Subtotal label", function () {

    describe("Case: C83979 - Ensure that upon selecting / removing bags the total price for all bags will be displayed beside the Subtotal label", function () {


        it("Given that user is on Samsonite product page from clicking on link in footer", function () {
            actions.samsoniteActions.clickSamsoniteFromFooter();
            actions.waitHelper.waitForPageTitle("Ryanair");
        });
        it("When user click plus button for one of the bags", function () {
            actions.samsoniteActions.clickAddSamsoniteBags(1);
        });
        it("Then subtotal price should be updated", function () {
            actions.samsoniteActions.expectSubtotalSamsonite("39");
        });

        it("Then I proceed to purchase Samsonite bag", function () {
            actions.samsoniteActions.clickCheckOut();
            actions.samsoniteActions.fillDeliveryDetails();
            actions.samsoniteActions.fillPaymentDetails(card);
        });

        it("Then I receive Successful Order confirmation message", function () {
            actions.samsoniteActions.verifySuccessfulOrder();
        });

    });

    describe("Case: C83979 - Ensure that upon selecting / removing bags the total price for all bags will be displayed beside the Subtotal label", function () {

        it("Given that user is on Samsonite product page", function () {
            actions.samsoniteActions.goToSamsonitePage();
        });
        it("When user click minus button for one of the bags", function () {
            actions.samsoniteActions.clickAddSamsoniteBags(1);
            actions.samsoniteActions.expectSubtotalSamsonite("39");
            actions.samsoniteActions.clickRemoveSamsoniteBags(1);
        });
        it("Then subtotal price should be updated", function () {
            actions.samsoniteActions.expectSubtotalSamsonite("0");
        });
    });

});
describe("TRIPS | Potential trip Samsonite | C83983 | Ensure that If user attempts to navigate away from page with item in basket then display warning pop up with OK / Cancel", function () {

    var Trip = require('../../../../../shared/model/Trip');

    describe("Case: C83983 - Ensure that If user attempts to navigate away from page with item in basket then display warning pop up with OK / Cancel", function () {


        it("Given that user is on Samsonite product page", function () {
            actions.samsoniteActions.goToSamsonitePage();
        });
        it("And user has added bag to basket", function () {
            actions.samsoniteActions.clickAddSamsoniteBags(1);
        });
        it("When user attempts to navigate away from page", function () {
            actions.samsoniteActions.navigateAway();
        });
        it("Then display warning pop up", function () {
            actions.samsoniteActions.expectPopUpIsDisplayed();
        });

    });

})
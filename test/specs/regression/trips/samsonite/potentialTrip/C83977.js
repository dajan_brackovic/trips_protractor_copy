describe("TRIPS | Potential trip Samsonite | C83977 | Ensure that user can select to view a larger image of a bag", function () {

    var Trip = require('../../../../../shared/model/Trip');

    describe("Case: C83977 - Ensure that user can select to view a larger image of a bag", function () {


        it("Given that user is on Samsonite product page", function () {
            actions.samsoniteActions.goToSamsonitePage();
        });
        it("When user select image of the bag", function () {
            browser.sleep(2000);
            actions.samsoniteActions.clickToGetLargerImage(0);
        });
        it("Then larger image of bag should be displayed", function () {
            browser.sleep(3000);
            actions.samsoniteActions.expectFullscreenBagImage();
        });

    });

});
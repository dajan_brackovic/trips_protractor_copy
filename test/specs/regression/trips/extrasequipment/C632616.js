var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;
var params = browser.params.conf;

function sharedDescribe(numAdult, numTeen, numChild, numInfants) {
    var outBoundDaysFromNow = 4; //today index === 1
    var returnDaysFromNow = 8; //today index === 1
    var origin = "Dub";
    var destination = "Ltn";
    var fareType = "standard";
    var tripWay = "twoway";
    var paxListMy;
    var cardMy;

    var bookFlight = function (paxMap) {
        var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        paxListMy = trip.journey.paxList;
        cardMy = trip.bookingContact.card;
    };

    describe(sprintf('%s adult, %s teen, %s children, %s infants', numAdult, numTeen, numChild, numInfants), function () {

        it(sprintf('Given I make a standard Two way trip with outbound 4 days and Rt 8 days from now with %s adult, %s teen, %s children, %s infants', numAdult, numTeen, numChild, numInfants), function () {
            var paxMap = {ADT: numAdult, CHD: numChild, INF: numInfants, TEEN: numTeen};
            bookFlight(paxMap);
        });

        it('When I am on potential trips page I add seats', function () {
            actions.extrasActions.addSeat();
            actions.seatsActions.selectOneSeat(tripWay);
            actions.seatsActions.selectDiffSeatReturn();
            actions.seatsActions.confirmSeat();
        });

        it('Then I add bags', function () {
            actions.extrasActions.addBag();
            actions.bagsActions.addOneNormalOneLargeBag();
            actions.bagsActions.assertOnBagsSubTotalAndTotal();
            actions.bagsActions.clickBagsBtnConfirm();
        });

        it('Then I add parking', function () {
            actions.potentialTripActions.clickAddParking();
            browser.sleep(2000);
            actions.lowCostParkingActions.enterRegistrationNumber(params.parking.registrationNumber1);
            actions.lowCostParkingActions.clickConfirmButton();
        });

        it('Then I add baby equipment', function () {
            actions.extrasActions.addBaby();
            actions.babyEquipmentActions.selectPlusButtonSameForBothFlights(0);
            actions.babyEquipmentActions.expectTotalPrice("20.00");
            actions.babyEquipmentActions.clickConfirmButton();
        });

        it('Then I add add music equipment', function () {
            actions.potentialTripActions.clickAddMusicEquipment();
            actions.musicEquipmentActions.selectPlusButtonSameForBothFlights(0);
            actions.musicEquipmentActions.expectTotalPrice("100.00");
            actions.musicEquipmentActions.clickConfirmButton();
        });

        it('Then I add sport equipment', function () {
            actions.potentialTripActions.clickAddSportEquipment();
            actions.sportEquipmentActions.selectSportEquipmentTypeClick(0,0);
            actions.sportEquipmentActions.selectSportEquipmentType(0,0,0);
            browser.sleep(2000);
            actions.sportEquipmentActions.selectPlusButtonSameForBothFlights(0);
            actions.sportEquipmentActions.expectTotalPrice("120.00");
            actions.sportEquipmentActions.clickConfirmButton();
        });

        it('Then I continue on extras page', function () {
            actions.extrasActions.skipExtras();
        });

        it('Then I should verify pricebreakdown', function () {
            actions.priceBreakDownActions.assertOnListOfSelectedItems("2 x bike equipment");
            actions.priceBreakDownActions.assertOnListOfSelectedItems("2 x baby equipment");
            actions.priceBreakDownActions.assertOnListOfSelectedItems("2 x music equipment");
            actions.priceBreakDownActions.assertOnParking();
            actions.addPaxActions.assertOnNoItemsOnPriceBreakdownIncludingFlights(8);
        });

        it('Then I remove added extras items on payment page', function () {
            actions.addPaxActions.clickOnXAddedItems();
        });

        it('Then I make sure that all the extras items are removed', function () {
            actions.addPaxActions.assertOnNoItemsOnPriceBreakdownIncludingFlights(2);
        });

        it('Then I should pay for booking', function () {
            actions.addPaxActions.addPaxNameForAllPAX(paxListMy);
            actions.addPaxActions.addContact();
            actions.addPaxActions.makeCardPayment(cardMy);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
        });
    });
}

describe('TRIPS | Extra Equipment | C632616 | Add All Extra on Potential Trip page | Remove All Extras on Payment Page', function () {

    sharedDescribe(1, 0, 0, 0);

});
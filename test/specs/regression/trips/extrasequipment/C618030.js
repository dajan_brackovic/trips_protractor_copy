var Trip = require('../../../../shared/model/Trip');

function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
    var outBoundDaysFromNow = 5;//actions.tripHelper.getRandomDate("PARTIAL") //3;//today index === 1
    var returnDaysFromNow = 7;//actions.tripHelper.getRandomDateGreaterThanOutbound("PARTIAL", outBoundDaysFromNow) //5; //today index === 1
    var origin = "Dub";
    var destination = "Edi";
    var fareType = "standard";
    var tripWay = "twoway";
    var paxListMy;
    var cardMy;
    var totalCarHirePrice;

    var bookFlight = function (paxMap) {
        var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        paxListMy = trip.journey.paxList;
        cardMy = trip.bookingContact.card;
    };

    describe('1 adult, 0 teen, 0 children, 0 infants', function () {

        it('Given I make a standard return trip with outbound 16 days and return 18 days from now with 1 adult, 0 teen, 0 children, 0 infants', function () {
            var paxMap = {ADT: 1, CHD: 0, INF: 0, TEEN: 0};
            bookFlight(paxMap);
        });

        it('When I am on potential trips page I add car', function () {
            actions.extrasActions.clickFirstCarInlist();
        });

        it('Then I add details for car hire', function () {
            totalCarHirePrice = actions.carHireActions.returnTotalPriceCarHire();
            actions.carHireActions.clickBtnConfirmCarHire();
        });

        it('Then I modify car hire', function () {
            actions.extrasActions.clickModifyCarHire();
            actions.carHireActions.clickCheckboxExtraLiability();
        });

        it('Then I continue on extras page', function () {
            actions.extrasActions.skipExtras();
        });

        it('Then I should pay for booking', function () {
            actions.addPaxActions.addPaxNameForAllPAX(paxListMy);
            actions.addPaxActions.enterFieldLeadDriverName();
            actions.priceBreakDownActions.assertOnCarHireInsuranceDetails();
            actions.addPaxActions.addContact();
            actions.addPaxActions.makeCardPayment(cardMy);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I should get a booking ref and car hire ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
            actions.bookingSummaryActions.verifyConfirmationMessageOnCarHireModify();
        });
    });

}

describe('TRIPS | Extra Equipment | C618030 | Car Hire Select Car | Modify | Pay', function () {

    sharedDescribe(1, 0, 0, 0);

});
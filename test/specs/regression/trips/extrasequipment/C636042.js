var Trip = require('../../../../shared/model/Trip');


function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
    var outBoundDaysFromNow = 4; //today index === 1
    var returnDaysFromNow = 8; //today index === 1
    var origin = "Dub";
    var destination = "Bud";
    var fareType = "standard";
    var tripWay = "twoway";
    var paxListMy;
    var cardMy;

    var bookFlight = function (paxMap) {
        var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        paxListMy = trip.journey.paxList;
        cardMy = trip.bookingContact.card;
    };

    describe('1 adult, 0 teen, 0 children, 0 infants', function () {

        it('Given I make a standard return trip with outbound 4 days and return 8 days from now with 1 adult, 0 teen, 0 children, 0 infants', function () {
            var paxMap = {ADT: 1, CHD: 0, INF: 0, TEEN: 0};
            bookFlight(paxMap);
        });

        it('When I am on potential trips page I check seats side drawer', function () {
            actions.extrasActions.addSeat();
            actions.seatsActions.clickBtnCancel();
        });

        it('Then I check bags side drawer', function () {
            actions.extrasActions.addBag();
            actions.bagsActions.clickBagsBtnCancel();
        });

        it("Then I check Insurance side drawer", function () {
            actions.potentialTripActions.clickAddInsurance();
            actions.bagsActions.clickBagsBtnCancel();
        });

        it('Then I check parking side drawer', function () {
            actions.potentialTripActions.clickAddParking();
            actions.bagsActions.clickBagsBtnCancel();
        });

        it("Then I check transfers side drawer", function(){
            actions.potentialTripActions.clickAirportTransfersCard();
            actions.selectTransferProviderActions.clickBtnCancelTransfer();
        });

        it('Then I check baby equipment side drawer', function () {
            actions.extrasActions.addBaby();
            actions.bagsActions.clickBagsBtnCancel();
        });

        it('Then I check music equipment side drawer', function () {
            actions.potentialTripActions.clickAddMusicEquipment();
            actions.bagsActions.clickBagsBtnCancel();
        });

        it('Then I check sport equipment side drawer', function () {
            actions.potentialTripActions.clickAddSportEquipment();
            actions.bagsActions.clickBagsBtnCancel();
        });

        it('Then I continue on extras page', function () {
            actions.extrasActions.skipExtras();
        });

        it('Then I should pay for booking', function () {
            actions.addPaxActions.addPaxNameForAllPAX(paxListMy);
            actions.addPaxActions.addContact();
            actions.addPaxActions.makeCardPayment(cardMy);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
        });

    });
}

describe('TRIPS | Extra Equipment | C636042 | Check Side Drawers on Potential Trip page', function () {

    sharedDescribe(1, 0, 0, 0);

});
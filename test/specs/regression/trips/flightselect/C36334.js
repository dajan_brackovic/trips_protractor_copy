
describe('TRIPS | Flight Select | C36334 | Dispaly Return Flight Card and check data on it', function () {
    var outBoundDaysFromNow = 5;//today index === 1
    var returnDaysFromNow = 6; //today index === 1
    var origin = "bud";
    var destination = "dub";
    var fareType = "business";
    var tripWay = "twoway";

    var bookFlight = function (paxMap) {
        actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
    }

    describe('Checking selected flight card information on RT flight', function () {

        it('Given I create a RT flight with 1 adults, 0 children, 0 infants, 0 teens, departing from STN and arriving in DUB', function () {
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            bookFlight(paxMap);
        });

        it(' When I select any of the outbound and inbound flight options that appear ', function () {
            actions.tripsHomeActions.selectAFlightOnly(0, 0, fareType, tripWay);
        });

        it('Then the flight card appears on the screen for the outbound with the correct details ' +
            '- Destination Airport, Arrival Airport, Dept and Arrival Time, Flight number  ', function () {
            actions.tripsHomeActions.assertFlightCardOutBoundInformation();
        });

        it('Then the flight card appears on the screen for the inbound with the correct details ' +
            '- Destination Airport, Arrival Airport, Dept and Arrival Time, Flight number ', function () {
            actions.tripsHomeActions.assertFlightCardInBoundInformation();
        });
    });
});


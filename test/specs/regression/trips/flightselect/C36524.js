var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(numAdults, numTeens, numChildren, numInfants) {
    var outBoundDaysFromNow = 1; //today index === 1
    var origin = "stn";
    var destination = "dub";
    var fareType = "standard";
    var tripWay = "oneway";

    var bookFlight = function (paxMap) {
        actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
        actions.fOHActions.confirmTodayAlert();
    };

    describe(sprintf('%s adult, %s teen, %s children, %s infants', numAdults, numTeens, numChildren, numInfants), function () {

        it(sprintf('Given I make a standard one way trip with outbound today with %s adult, %s teen, %s children, %s infants', numAdults, numTeens, numChildren, numInfants), function () {
            var paxMap = {ADT: numAdults, TEEN: numTeens, CHD: numChildren, INF: numInfants};
            bookFlight(paxMap);
        });

        it('Then I assert on six hour departure rule', function () {
            actions.tripsHomeActions.assertSixHoursDepartureRule();
        });

    });
}

describe('TRIPS | Flight Select | C36524 | Verify Flight Card 6 hour depart rule', function () {

    sharedDescribe(1, 0, 0, 0);

});

var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(numAdults, numTeens, numChildren, numInfants) {
    var outBoundDaysFromNow = 2; //today index === 1
    var returnDaysFromNow = 4; //today index === 1
    var origin = "stn";
    var destination = "dub";
    var fareType = "standard";
    var tripWay = "twoway";

    var bookFlight = function (paxMap) {
        actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
    };

    describe(sprintf('%s adult, %s teen, %s children, %s infants', numAdults, numTeens, numChildren, numInfants), function () {

        it(sprintf('Given I make a standard one way trip with outbound < 7 Days from now with %s adult, %s teen, %s children, %s infants', numAdults, numTeens, numChildren, numInfants), function () {
            var paxMap = {ADT: numAdults, TEEN: numTeens, CHD: numChildren, INF: numInfants};
            bookFlight(paxMap);
        });

        it('Then I make sure Continue button is disable when flight is not selected', function () {
            actions.tripsHomeActions.assertOnBtnHomeBottomContinue(false);
        });

        it('Then I select flight', function () {
            actions.tripsHomeActions.selectAFlightOnly(0, 0, fareType, tripWay);
        });

        it('Then I make sure Continue button is not disable when flight is selected', function () {
            actions.tripsHomeActions.assertOnBtnHomeBottomContinue(true);
        });

    });
}

describe('TRIPS | Flight Select | C29008 | RT | Continue button is disable when flight is not selected', function () {

    sharedDescribe(1, 0, 0, 0);

});


describe('TRIPS | Flight Select | C36363 | Verify No Flight available message', function () {
    var origin = "dub";
    var destination = "Byd";
    var fareType = "standard";
    var tripWay = "oneway";

    var bookFlight = function (paxMap) {
        actions.fOHActions.searchOneWayFlightWithPaxNextAvailableDate(paxMap, origin, destination);
    }

    describe('Verify "No Flight" available message', function () {

        it('Given I create a OW with 1 adults, 0 children, 0 infants, 0 teens, departing from DUB and arriving in TLL', function () {
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            bookFlight(paxMap);
        });

        it(' When I select any of the outbound and inbound flight options that appear with No flight icon ', function () {
            actions.tripsHomeActions.clickNoFlightCarousel();
        });

        it('Then I check message for inbound and outbound flights and Flight card appears with No Flights today error message ', function () {
            expect(actions.tripsHomeActions.getTextassertNoFlightsMessage().isDisplayed()).toBeTruthy();
        });
    });
});
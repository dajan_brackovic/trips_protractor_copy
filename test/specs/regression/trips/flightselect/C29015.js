var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(numAdults, numTeens, numChildren, numInfants) {
    var outBoundDaysFromNow = 2; //today index === 1
    var returnDaysFromNow = 5; //today index === 1
    var origin = "stn";
    var destination = "dub";
    var fareType = "standard";
    var tripWay = "twoway";

    var bookFlight = function (paxMap) {
        actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.tripsHomeActions.selectAFlightOnly(0, 0, fareType,tripWay);
    };

    describe(sprintf('%s adult, %s teen, %s children, %s infants', numAdults, numTeens, numChildren, numInfants), function () {

        it(sprintf('Given I make a standard RT trip with outbound and inbound with in 7 Days from now with %s adult, %s teen, %s children, %s infants', numAdults, numTeens, numChildren, numInfants), function () {
            var paxMap = {ADT: numAdults, TEEN: numTeens, CHD: numChildren, INF: numInfants};
            bookFlight(paxMap);
        });

        it('Then I assert on modify trip button', function () {
            actions.tripsHomeActions.assertOnModifyTripButton(true);
        });

        it('Then I make sure Save Trip Button is enabled', function () {
            actions.tripsHomeActions.assertOnBtnSaveTrip(true);
        });

        it('Then I assert on flight info box', function () {
            actions.tripsHomeActions.assertOnFlightInfoBox(true, tripWay);
        });

    });
}

describe('TRIPS | Flight Select | C29015 | Other UI Elements', function () {

    sharedDescribe(1, 0, 0, 0);

});

var sprintf = require("sprintf").sprintf;

function sharedDescribe(option, newurl) {
    describe('', function () {

        it(sprintf('Given I am on the %s culture home page', option), function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.selectMarketsMenu();
            actions.fOHActions.clickMarketList(option);

        });

        it(sprintf('Then I click on Cars link from global header and verify correct URL has opened', option, newurl), function () {
            actions.fOHActions.clickCarsAndVerifyUrl(newurl);
        });

    });
}

describe('TRIPS | C652493 | Global Header | Verify Cars links function correctly ', function () {
    sharedDescribe("Great Britain (English)", "car-hire.ryanair.com");
    sharedDescribe("(Greek)", "car-hire.ryanair.com");
    sharedDescribe("(Hungarian)", "car-hire.ryanair.com");
});


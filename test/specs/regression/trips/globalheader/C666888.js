var sprintf = require("sprintf").sprintf;

function sharedDescribe(option, newurl) {
    describe('', function () {

        it(sprintf('Given I am on the %s culture home page', option), function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.selectMarketsMenu();
            actions.fOHActions.clickMarketList(option);

        });

        it(sprintf('Then I click on Hotels link from global header and verify correct URL has opened', option, newurl), function () {
            actions.fOHActions.clickHotelsAndVerifyUrl(newurl);
        });

    });
}

describe('TRIPS | C666888 | Global Header | Verify Hotels link functions correctly ', function () {
    sharedDescribe("Great Britain (English)", "hotels.ryanair.com/");
    sharedDescribe("France (French)", "hotels.ryanair.com/");
    sharedDescribe("Spain (Spanish)", "hotels.ryanair.com/");
});



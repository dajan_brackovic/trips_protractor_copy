var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;


function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
    // test not running until  FRW - 2837 bug is resolved
        var outBoundDaysFromNow = 3; //today index === 1
        var origin = "Dub";
        var destination = "bud";
        var fareType = "standard";
        var tripWay = "oneway";
        var bookingRefActiveTrip;
        var selectedSeat;
        var trip;

        var bookFlight = function (paxMap) {
            trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
            actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType,tripWay);
            actions.extrasActions.skipExtras();
            actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
            actions.addPaxActions.clickBtnAddPaxSave();
            actions.addPaxActions.addContact();
            actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
        }

        xdescribe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

            it(sprintf('Given I make a standard oneway trip with outbound < 7 Days from now with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
                var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
                bookFlight(paxMap);
            });

            it('Then I should get a booking ref', function () {
                actions.bookingSummaryActions.verifyConfirmationMessage();
                bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
            });

            it('When I click checkIn button', function () {
                actions.bookingSummaryActions.clickCheckInButton();
            });

            it('Then I fill Check In id documents', function () {
                actions.checkInActions.selectNationalityDropDown();
                actions.checkInActions.enterDateOfBirth(50);
                actions.checkInActions.enterDocumentType(1);
                actions.checkInActions.enterDocumentNumberField();
                actions.checkInActions.enterCountryOfIssueDropDown();
                actions.checkInActions.enterExpiryDate();
            });

            it('Then I click continue after adding id documents on check-in', function () {
                actions.checkInActions.clickBtnContinueCheckIn();
            });

            it('Then I complete second step of check-in', function () {
                actions.checkInActions.clickSelectFlightCheckbox();
                actions.checkInActions.selectCheckboxSeat(tripWay);
                actions.checkInActions.clickBtnContinueCheckIn();
            });

            it('Then I get seat map', function () {
                actions.seatsActions.assertOnSeatMapDisplayed();
            });

            it('Then I select seats', function () {
                actions.seatsActions.selectOneSeat(tripWay);
                selectedSeat = actions.seatsActions.returnOneWaySeat();
                actions.seatsActions.clickBtnConfirm();
            });

            it('Then I pay for Seats with declined card', function () {
                trip.bookingContact.card.cardNumber = "5100000014101198";
                actions.addPaxActions.makeCardPaymentWithoutContinuing(trip.bookingContact.card);
                actions.checkInActions.clickBtnContinueCheckIn();
                actions.addPaxActions.assertOnDeclinedPaymentError();
                browser.sleep(10000);
            });

            //TODO Assert on auto allocation message

        });
}

xdescribe('TRIPS | Check In | C146512 | outbound  < 7 Days | Standard | Buy Seat at CheckIn when payment declined', function () {

    sharedDescribe(1, 0, 0, 0);

});
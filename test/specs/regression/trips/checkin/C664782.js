var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
    var outBoundDaysFromNow = 4; //today index === 1
    var returnDaysFromNow = 8; //today index === 1
    var origin = "Dub";
    var destination = "Bud";
    var fareType = "standard";
    var tripWay = "twoway";
    var bookingRefActiveTrip;
    var selectedSeatOut;
    var selectedSeatIn;
    var addedExtras = ['BABY , 15KG BAG, 20KG BAG, BIKE, MUSIC'];
    var paxListMy;
    var cardMy;

    var bookFlight = function (paxMap) {
        var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        paxListMy = trip.journey.paxList;
        cardMy = trip.bookingContact.card;
    };

    describe('1 adult, 0 teen, 0 children, 0 infants', function () {

        it('Given I make a standard return trip with outbound 4 days and return 8 days from now with 1 adult, 0 teen, 0 children, 0 infants', function () {
            var paxMap = {ADT: 1, CHD: 0, INF: 0, TEEN: 0};
            bookFlight(paxMap);
        });

        it('When I am on potential trips page I add seats', function () {
            actions.extrasActions.addSeat();
            actions.seatsActions.selectOneSeat(tripWay);
            actions.seatsActions.selectDiffSeatReturn();
            selectedSeatOut = actions.seatsActions.returnTwoWaySeatOut();
            selectedSeatIn = actions.seatsActions.returnTwoWaySeatIn();
            actions.seatsActions.confirmSeat();
        });

        it('Then I add bags', function () {
            actions.extrasActions.addBag();
            actions.bagsActions.addOneNormalOneLargeBag();
            actions.bagsActions.assertOnBagsSubTotalAndTotal();
            actions.bagsActions.clickBagsBtnConfirm();
        });

        it('Then I add baby equipment', function () {
            actions.extrasActions.addBaby();
            actions.babyEquipmentActions.selectPlusButtonSameForBothFlights(0);
            actions.babyEquipmentActions.expectTotalPrice("20.00");
            actions.babyEquipmentActions.clickConfirmButton();
        });

        it('Then I add add music equipment', function () {
            actions.potentialTripActions.clickAddMusicEquipment();
            actions.musicEquipmentActions.selectPlusButtonSameForBothFlights(0);
            actions.musicEquipmentActions.expectTotalPrice("100.00");
            actions.musicEquipmentActions.clickConfirmButton();
        });

        it('Then I add sports equipment', function () {
            actions.potentialTripActions.clickAddSportEquipment();
            actions.sportEquipmentActions.selectSportEquipmentTypeClick(0,0);
            actions.sportEquipmentActions.selectSportEquipmentType(0,0,0);
            browser.sleep(2000);
            actions.sportEquipmentActions.selectPlusButtonSameForBothFlights(0);
            actions.sportEquipmentActions.expectTotalPrice("120.00");
            actions.sportEquipmentActions.clickConfirmButton();
        });

        it('Then I continue on extras page', function () {
            actions.extrasActions.skipExtras();
        });

        it('Then I should pay for booking', function () {
            actions.addPaxActions.addPaxNameForAllPAX(paxListMy);
            actions.addPaxActions.addContact();
            actions.addPaxActions.makeCardPayment(cardMy);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
            bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
        });

        it('When I click checkIn button', function () {
            actions.bookingSummaryActions.clickCheckInButton();
        });

        it('Then I fill Check In id documents', function () {
            actions.checkInActions.selectNationalityDropDown();
            actions.checkInActions.enterDateOfBirth(50);
            actions.checkInActions.enterDocumentType(1);
            actions.checkInActions.enterDocumentNumberField();
            actions.checkInActions.enterCountryOfIssueDropDown();
            actions.checkInActions.enterExpiryDate();
        });

        it('Then I click continue after adding id documents on check-in', function () {
            actions.checkInActions.clickBtnContinueCheckIn();
        });

        it('Then I complete second step of check-in', function () {
            actions.checkInActions.assertSeatOnCheckInReturn(selectedSeatOut, selectedSeatIn);
            actions.checkInActions.clickBtnContinueCheckIn();
        });

        it('Then I get Boarding Pass Ref', function () {
            actions.checkInActions.assertOnBookingRef(bookingRefActiveTrip);
        });

        it('Then I assert on selected seat', function () {
            actions.checkInActions.assertSeatsOnBpReturn(selectedSeatOut, selectedSeatIn);
        });

        it('Then I assert extras on Boarding Pass', function () {
            actions.checkInActions.assertBagsOnBp(tripWay, addedExtras); // same action for extras items
        });

    });
}

describe('TRIPS | CheckIN | C664782 | Add Seat Bag Baby Sports Music On Potential Trip Page | Assert on BP', function () {

    sharedDescribe(1, 0, 0, 0);

});
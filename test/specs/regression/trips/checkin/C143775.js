var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;


function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
        var outBoundDaysFromNow = 2; //today index === 1
        var returnDaysFromNow = 32; //today index === 1
        var origin = "Dub";
        var destination = "Bud";
        var fareType = "standard";
        var tripWay = "twoway";
        var bookingRefActiveTrip;
        var NumberOfBoardingPass = 1;
        var totalPax = adultNumber + teenNumber + childrenNumber + infantsNumber;

        var bookFlight = function (paxMap) {
            var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType,tripWay);
            actions.extrasActions.skipExtras();
            actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
            actions.addPaxActions.addContact();
            actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        }

        describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

            it(sprintf('Given I make a standard return trip with outbound < 7 Days and return > 30 days from now with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
                var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
                bookFlight(paxMap);
            });

            it('Then I should get a booking ref', function () {
                actions.bookingSummaryActions.verifyConfirmationMessage();
                bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
            });

            it('When I click checkIn button', function () {
                actions.bookingSummaryActions.clickCheckInButton();
            });

            it('Then I fill Check In id documents', function () {
                actions.checkInActions.selectNationalityDropDown();
                actions.checkInActions.enterDateOfBirth(50);
                actions.checkInActions.enterDocumentType(2);
                actions.checkInActions.enterDocumentNumberField();
                actions.checkInActions.enterCountryOfIssueDropDown();
                actions.checkInActions.enterExpiryDate();
            });

            it('Then I click continue after adding id documents on check-in', function () {
                actions.checkInActions.clickBtnContinueCheckIn();
            });

            it('Then I complete second step and assert Flight not ready for check in', function () {
                actions.checkInActions.assertOnFlightNotReadyForCheckIn();
                actions.checkInActions.clickBtnContinueCheckIn();
            });

            it('Then I get Boarding Pass Ref', function () {
                actions.checkInActions.assertOnBookingRef(bookingRefActiveTrip);
            });

            it('Then I assert on number of boarding passes', function () {
                actions.checkInActions.assertOnNumberOfBoardingPass(NumberOfBoardingPass);
            });

            it('Then I get auto allocated seat', function () {
                tripWay ="oneway"; // In this case inbound flight is not ready to check in 30 days rules.
                actions.checkInActions.assertOnAutoAllocation(tripWay, totalPax);
            });
        });
}

describe('TRIPS | Check In | C143775 | outbound < 7 Days | Return > 30 Days | Standard | Auto Allocation Seat', function () {

    sharedDescribe(1, 0, 0, 0);

});
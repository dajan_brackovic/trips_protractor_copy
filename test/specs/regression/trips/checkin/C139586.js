var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;


function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
        var outBoundDaysFromNow = 2; //today index === 1
        var returnDaysFromNow = 5; //today index === 1
        var origin = "Stn";
        var destination = "bcn";
        var fareType = "standard";
        var tripWay = "twoway";
        var bookingRefActiveTrip;
        var selectedSeatOut;
        var selectedSeatIn;
        var trip;

        var bookFlight = function (paxMap) {
            trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType,tripWay);
            actions.extrasActions.skipExtras();
            actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
            actions.addPaxActions.addContact();
            actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        };

        describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

            it(sprintf('Given I make a standard return trip with outbound < 7 Days and return < 7 days from now with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
                var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
                bookFlight(paxMap);
            });

            it('Then I should get a booking ref', function () {
                actions.bookingSummaryActions.verifyConfirmationMessage();
                bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
            });

            it('When I click checkIn button', function () {
                actions.bookingSummaryActions.clickCheckInButton();
            });

            it('Then I fill Check In id documents', function () {
                actions.checkInActions.selectNationalityDropDown();
                actions.checkInActions.enterDateOfBirth(50);
                actions.checkInActions.enterDocumentType(1);
                actions.checkInActions.enterDocumentNumberField();
                actions.checkInActions.enterCountryOfIssueDropDown();
                actions.checkInActions.enterExpiryDate();
            });

            it('Then I click continue after adding id documents on check-in', function () {
                actions.checkInActions.clickBtnContinueCheckIn();
            });

            it('Then I complete second step of check-in', function () {
                actions.checkInActions.selectCheckboxSeat(tripWay);
                actions.checkInActions.clickBtnContinueCheckIn();
            });


            it('Then I purchase premium seats', function () {
                actions.seatsActions.selectOneSeatPremiumCheckIn(tripWay);
                actions.seatsActions.selectSamePremiumSeatReturn();
                selectedSeatOut = actions.seatsActions.returnTwoWaySeatOut();
                selectedSeatIn = actions.seatsActions.returnTwoWaySeatIn();
                actions.seatsActions.clickBtnConfirmCheckIn();
            });

            it('Then I pay for Seats', function () {
                actions.addPaxActions.makeCardPaymentWithoutContinuing(trip.bookingContact.card);
                actions.addPaxActions.enterBillingAddressAtCheckIn();
                actions.checkInActions.clickBtnContinueCheckIn();
            });

            it('Then I get Boarding Pass Ref', function () {
                actions.checkInActions.assertOnBookingRef(bookingRefActiveTrip);
            });

            it('Then I assert on selected seat', function () {
                actions.checkInActions.assertSeatsOnBpReturn(selectedSeatOut, selectedSeatIn);
            });
        });
}

describe('TRIPS | Check In | C139586 | outbound < 7 Days | Return < 7 Days | Standard | Buy Seat at CheckIn', function () {

    sharedDescribe(1, 0, 0, 0);

});

var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;


function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
    var outBoundDaysFromNow = 3;//today index === 1
    var returnDaysFromNow = 12; //today index === 1
    var origin = "stn";
    var destination = actions.tripHelper.getRandomAirportFromArray();
    var fareType = "business";
    var tripWay = "twoway";
    var bookingRefActiveTrip;
    var selectedSeatOut;
    var selectedSeatIn;
    var addedBags = "20KG BAG";
    var trip;

    var bookFlight = function (paxMap) {
        trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
    }

    describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

        it(sprintf('Given I make a business return trip with outbound > 1 < 7Days and return < 7 days from now with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
            var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
            bookFlight(paxMap);
        });

        it('When I select a seat', function () {
            actions.seatsActions.selectOneSeat(tripWay);
        });

        it('Then I select a same seat for return', function () {
            actions.seatsActions.selectSameSeatReturn();
            selectedSeatOut = actions.seatsActions.returnTwoWaySeatOut();
            selectedSeatIn = actions.seatsActions.returnTwoWaySeatIn();
        });

        it('Then I should confirm a seat price', function () {
            actions.seatsActions.clickBtnConfirm();
        });

        it('Then I continue on extras page', function () {
            actions.extrasActions.skipExtras();
        });

        it('Then I should pay for booking', function () {
            actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
            actions.addPaxActions.addContact();
            actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
            bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
        });

        it('When I click checkIn button', function () {
            actions.bookingSummaryActions.clickCheckInButton();
        });

        it('Then I fill Check In id documents', function () {
            actions.checkInActions.selectNationalityDropDown();
            actions.checkInActions.enterDateOfBirth(50);
            actions.checkInActions.enterDocumentType(2);
            actions.checkInActions.enterDocumentNumberField();
            actions.checkInActions.enterCountryOfIssueDropDown();
            actions.checkInActions.enterExpiryDate();
        });

        it('Then I click continue after adding id documents on check-in', function () {
            actions.checkInActions.clickBtnContinueCheckIn();
        });

        it('Then I complete second step of check-in', function () {
            actions.checkInActions.assertSeatOnCheckInReturn(selectedSeatOut, selectedSeatIn);
            actions.checkInActions.clickBtnContinueCheckIn();
        });

        it('Then I get Boarding Pass Ref', function () {
            actions.checkInActions.assertOnBookingRef(bookingRefActiveTrip);
        });

        it('Then I assert on selected seat', function () {
            actions.checkInActions.assertSeatsOnBpReturn(selectedSeatOut, selectedSeatIn);
        });

        it('Then I assert on free bag on Boarding Pass', function () {
            actions.checkInActions.assertBagsOnBp(tripWay, addedBags);
        });

        it('Then I verify that Biz Plus appears on Boarding Pass', function () {
            actions.checkInActions.verifyBizPlusOnBP();
        });

    });
}

describe('TRIPS | Check In | C609504 | Outbound < 7 Days | Return > 7Days | Business | Assert Bag on BP ', function () {

    sharedDescribe(1, 0, 0, 0);

});
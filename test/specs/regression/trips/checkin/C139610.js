var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
        var outBoundDaysFromNow = 9; //today index === 1
        var origin = "Brs";
        var destination = "Dub";
        var fareType = "standard";
        var tripWay = "oneway";
        var bookingRefActiveTrip;
        var selectedSeat;
        var paxListMy;
        var cardMy;

        var bookFlight = function (paxMap) {
            var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
            actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType,tripWay);
            paxListMy = trip.journey.paxList;
            cardMy = trip.bookingContact.card;
        };

        describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {



            it(sprintf('Given I make a standard one way trip with outbound < 7 Days from now with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
                var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
                bookFlight(paxMap);
            });

            it('When I select a seat on extras page', function () {
                actions.extrasActions.addSeat();
                actions.seatsActions.selectOneSeat(tripWay);
                selectedSeat = actions.seatsActions.returnOneWaySeat();
            });

            it('Then I should confirm a seat price', function () {
                actions.seatsActions.confirmSeat();
            });

            it('Then I continue on extras page', function () {
                actions.extrasActions.skipExtras();
            });

            it('Then I should pay for booking', function () {
                actions.addPaxActions.addPaxNameForAllPAX(paxListMy);
                actions.addPaxActions.addContact();
                actions.addPaxActions.makeCardPayment(cardMy);
                actions.addPaxActions.enterBillingAddress();
                actions.addPaxActions.clickPaymentContinue();
            });

            it('Then I should get a booking ref', function () {
                actions.bookingSummaryActions.verifyConfirmationMessage();
                bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
            });

            it('When I click checkIn button', function () {
                actions.bookingSummaryActions.clickCheckInButton();
            });

            it('Then I fill Check In id documents', function () {
                actions.checkInActions.selectNationalityDropDown();
                actions.checkInActions.enterDateOfBirth(50);
                actions.checkInActions.enterDocumentType(1);
                actions.checkInActions.enterDocumentNumberField();
                actions.checkInActions.enterCountryOfIssueDropDown();
                actions.checkInActions.enterExpiryDate();
            });

            it('Then I click continue after adding id documents on check-in', function () {
                actions.checkInActions.clickBtnContinueCheckIn();
            });

            it('Then I complete second step of check-in', function () {
                actions.checkInActions.assertSeatOnCheckIn(selectedSeat);
                actions.checkInActions.clickBtnContinueCheckIn();
            });

            it('Then I should get Boarding Pass', function () {
                actions.checkInActions.assertOnBordingPass();
            });

            it('Then I should get Boarding Pass reference', function () {
                actions.checkInActions.assertOnBookingRef(bookingRefActiveTrip);
            });

            it('Then I assert on selected seat', function () {
                actions.checkInActions.assertSeatsOnBp(selectedSeat);
            });
        });
}

describe('TRIPS | Check In | C139610 | outbound  > 7 Days | Standard | Buy Seat at Potential Trip ', function () {

    sharedDescribe(1, 0, 0, 0);

});
var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
        var outBoundDaysFromNow = 4; //today index === 1
        var origin = "Dub";
        var destination = "bcn";
        var fareType = "standard";
        var tripWay = "oneway";
        var paxListMy;
        var cardMy;
        var totalPax = adultNumber + teenNumber + childrenNumber + infantsNumber;
        var bookingRefActiveTrip;

        var bookFlight = function (paxMap) {
            var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
            actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
            paxListMy = trip.journey.paxList;
            cardMy = trip.bookingContact.card;
        }

        describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

            it(sprintf('Given I make a standard one way trip with outbound 4 days from now with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
                var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
                bookFlight(paxMap);
            });

            it('When I select one large and one normal bags for each passenger', function () {
                actions.extrasActions.addBag();
                actions.bagsActions.selectOneLargeAndOneNormalBagForEachPax();
            });

            it('Then I assert bags on pricebreakdown', function () {
                actions.bagsActions.assertBagsOnPriceBreakDown();
            });

            it('Then I continue on extras page', function () {
                actions.extrasActions.skipExtras();
            });

            it('Then I should pay for booking', function () {
                actions.addPaxActions.addPaxNameForAllPAX(paxListMy);
                actions.addPaxActions.addContact();
                actions.addPaxActions.makeCardPayment(cardMy);
                actions.addPaxActions.enterBillingAddress();
                actions.addPaxActions.clickPaymentContinue();
            });

            it('Then I should get a booking ref', function () {
                actions.bookingSummaryActions.verifyConfirmationMessage();
                bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
            });

            it('When I click checkIn button', function () {
                actions.bookingSummaryActions.clickCheckInButton();
            });

            it('Then I fill Check In id documents', function () {
                actions.checkInActions.selectAllPax();
                actions.checkInActions.addDocsForMultiPax(adultNumber, teenNumber, childrenNumber, infantsNumber, 3);
            });

            it('Then I complete second step of check-in', function () {
                actions.checkInActions.clickBtnContinueCheckIn();
            });

            it('Then I assert on correct number of Boarding Passes and click view all', function () {
                actions.checkInActions.countNumberOfBoardingPassesInList(totalPax);
            });

            it('Then I get Boarding Pass Ref', function () {
                actions.checkInActions.assertOnBookingRef(bookingRefActiveTrip);
            });

            it('Then I assert bags on boarding pass', function () {
                actions.checkInActions.assertBagsOnBpMultiPax(tripWay, totalPax);
            });

            it('Then I get auto allocated seat', function () {
                actions.checkInActions.assertOnAutoAllocation(tripWay, totalPax);
            });
        });
}


describe('TRIPS | Check In | C596868 | One Way | outbound < 7 Days | Add Bags on Potential Trip | Multi-Pax | Assert bag on check in | Auto Allocation Seat', function () {

    sharedDescribe(2, 2, 0, 0);

});
var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;
var specId = "Priorityboardingstandalone| C698535 ";


function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE"); //today index === 1
    var origin = "stn";
    var destination = actions.tripHelper.getRandomAirportFromArray();
    var fareType = "standard";
    var tripWay = "oneway";
    var bookingRefActiveTrip;
    var userName = actions.tripHelper.getRandomEmail();
    var password = "Testing123";
    var totalPax = adultNumber + teenNumber + childrenNumber + infantsNumber;
    var trip;

    var bookFlight = function (paxMap) {
        trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
        actions.fOHActions.goToPage();
        actions.myFrSignupHelper.createNewUser(userName, password);
        actions.fOHActions.login(userName, password);
        actions.fOHActions.searchOneWayFLightWithPaxMyFr(paxMap, origin, destination, outBoundDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
    };

    describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

        it(sprintf('Given I login to my ryanair and I make a standard oneWay trip with 1 < outbound < 7 days from now with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
            var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
            bookFlight(paxMap);
        });

        it('Add priority boarding', function () {
            actions.extrasActions.addPriorityBoarding();
            actions.extrasActions.verifyPriorityBoardingAdded();
        });

        it('Verify pricebreak down contains Priority Boarding price', function () {
            actions.extrasActions.skipExtras();
            actions.addPaxActions.verifyPriceBreakDownContainPriorityBoarding();

        });

        it('Continue on booking', function () {
            actions.addPaxActions.addSavedPaxNameNewUserMyRyanair(trip.journey.paxList);
            actions.addPaxActions.addContactForNewUserWithNoPhoneNumber();
            actions.addPaxActions.makeCardPaymentEnterBilling(trip.bookingContact.card);
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
            bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
        });

        it('When I click checkIn button', function () {
            actions.bookingSummaryActions.clickCheckInButton();
        });

        it('Then I fill Check In id documents', function () {
            actions.checkInActions.selectAllPax();
            actions.checkInActions.addDocsForMultiPax(adultNumber, teenNumber, childrenNumber, infantsNumber, 3);
        });

        it('Then I complete second step of check-in', function () {
            actions.checkInActions.clickBtnContinueCheckIn();
        });

        it('Then I get Boarding Pass Ref', function () {
            actions.checkInActions.assertOnBookingRef(bookingRefActiveTrip);
        });

        it('Then I get auto allocated seat', function () {
            actions.checkInActions.assertOnAutoAllocation(tripWay, totalPax);
        });

        it('Verify the priority mark shown on boarding pass', function () {
            actions.checkInActions.assertPriorityMorePassengers(tripWay, adultNumber, "PRIORITY");
        });
        it('Close boarding pass window', function () {
            actions.checkInActions.closeBoardingPassWindow();
        });
    });

    afterAll(function () {
        actions.fOHActions.logout();
    });

}

describe(specId + ' RW-2331| oneway| multiple passnegers |Ensure that the priority boarding card is updated after tapping on the CTA', function () {
    sharedDescribe(3, 0, 0, 0);

});
var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;
var specId = "Priorityboardingstandalone| C698532 ";


function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE");
    var returnDaysFromNow = 24;
    var origin = "STN";
    var destination = actions.tripHelper.getRandomAirportFromArray();
    var fareType = "standard";
    var tripWay = "twoway";
    var trip;


    var bookFlight = function (paxMap) {
        trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
        actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        actions.extrasActions.xOutReserveSeatPopUp();
    };

    describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
        it('Login and book first flight', function () {
            var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
            bookFlight(paxMap);
        });

        it('Add priority boarding', function () {
            actions.extrasActions.addPriorityBoarding();
            actions.extrasActions.verifyPriorityBoardingAdded();
        });

        it('Verify pricebreak down contains Priority Boarding price', function () {
            actions.extrasActions.skipExtras();
            actions.addPaxActions.verifyPriceBreakDownContainPriorityBoarding();

        });



    });
}

describe(specId+' | RW-2331 | Ensure that you can add priority boarding to a two way trip', function () {
    sharedDescribe(1, 0, 0, 0);
    sharedDescribe(1, 1, 1, 1);
    sharedDescribe(2, 2, 1, 1);

});
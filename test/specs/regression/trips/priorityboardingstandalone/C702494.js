var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;
var specId = "|priorityboardingstandalone| C702494 ";


function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE"); //today index === 1
    var origin = "STN";
    var destination = actions.tripHelper.getRandomAirportFromArray();
    var fareType = "standard";
    var tripWay = "oneway";
    var trip;
    var totalPax = adultNumber + teenNumber + childrenNumber + infantsNumber;
    var bookingRefActiveTrip;


    var bookFlight = function (paxMap) {
        trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
        actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        actions.extrasActions.xOutReserveSeatPopUp();
    };

    describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
        it('Login and book first flight', function () {
            var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
            bookFlight(paxMap);
        });

        it('Add priority boarding', function () {
            actions.extrasActions.addPriorityBoarding();
            actions.extrasActions.verifyPriorityBoardingAdded();
        });

        it('User can remove priority straight after adding priority boarding', function () {
            actions.extrasActions.removePriorityBoardingAndVerify();
        });

        it('Add priority boarding back', function () {
            actions.extrasActions.addPriorityBoarding();
            actions.extrasActions.verifyPriorityBoardingAdded();
        });

        it('Verify that priority boarding can be removed after adding bags', function () {
            actions.extrasActions.addBag();
            actions.bagsActions.addOneNormalBag();
            actions.bagsActions.clickBagsBtnConfirm();
            actions.extrasActions.removePriorityBoardingAndVerify();
        });

        it('Add priority boarding back', function () {
            actions.extrasActions.addPriorityBoarding();
            actions.extrasActions.verifyPriorityBoardingAdded();
        });

        it("Verify that priority boarding can be removed after adding insurance", function () {
            actions.potentialTripActions.clickAddInsurance();
            actions.insuranceActions.selectCountryOfResidenceForPassanger(0, "Ireland");
            actions.insuranceActions.clickInsurancePlusButton(0);
            actions.insuranceActions.clickConfirmButton();
            actions.extrasActions.removePriorityBoardingAndVerify();
        });

        it('Add priority boarding back', function () {
            actions.extrasActions.addPriorityBoarding();
            actions.extrasActions.verifyPriorityBoardingAdded();
        });

        it('Verify that priority boarding can be removed after adding seat(s)', function () {
            actions.extrasActions.addSeat();
            actions.seatsActions.selectOneSeat(tripWay);
            actions.seatsActions.confirmSeat();
            actions.extrasActions.removePriorityBoardingAndVerify();
        });

        it('Continue on booking', function () {
            actions.extrasActions.skipExtras();
            actions.addPaxActions.addSavedPaxNameNewUserMyRyanair(trip.journey.paxList);
            actions.addPaxActions.editEmailAddress();
            actions.addPaxActions.addContactForNewUserWithNoPhoneNumber();
            actions.addPaxActions.makeCardPaymentEnterBilling(trip.bookingContact.card);
        });


        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
            bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
        });

        it('When I click checkIn button', function () {
            actions.bookingSummaryActions.clickCheckInButton();
        });

        it('Then I fill Check In id documents', function () {
            actions.checkInActions.selectNationalityDropDown();
            actions.checkInActions.enterDateOfBirth(50);
            actions.checkInActions.enterDocumentType(1);
            actions.checkInActions.enterDocumentNumberField();
            actions.checkInActions.enterCountryOfIssueDropDown();
            actions.checkInActions.enterExpiryDate();
        });

        it('Then I click continue after adding id documents on check-in', function () {
            actions.checkInActions.clickBtnContinueCheckIn();
        });

        it('Then I complete second step of check-in', function () {
            actions.checkInActions.clickBtnContinueCheckIn();
        });

        it('Then I get Boarding Pass Ref', function () {
            actions.checkInActions.assertOnBookingRef(bookingRefActiveTrip);
        });

        it('Then I get auto allocated seat', function () {
            actions.checkInActions.assertOnAutoAllocation(tripWay, totalPax);

        });
        it('Verify the priority mark shown on boarding pass', function () {
            actions.checkInActions.assertOnPriorityBp(tripWay, "OTHER Q");
        });
        it('Close boarding pass window', function () {
            actions.checkInActions.closeBoardingPassWindow();
        });


    });
}

describe(specId + ' | RW-2331 | one way | anonymous| Ensure that you can remove priority boarding after adding seat/bag/insurance', function () {
    sharedDescribe(1, 0, 0, 0);


});
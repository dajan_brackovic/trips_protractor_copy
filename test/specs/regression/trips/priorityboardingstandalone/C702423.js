var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;
var specId = "Priorityboardingstandalone| C702423 ";

function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE");//today index === 1
    var returnDaysFromNow = 24; //today index === 1
    var origin = "dub";
    var destination = "lgw";
    var fareType = "standard";
    var tripWay = "twoway";
    var bookingRefActiveTrip;
    var selectedSeat;
    var userName = actions.tripHelper.getRandomEmail();
    var password = "Password1";
    var trip;


    var bookFlight = function (paxMap) {
        trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.fOHActions.goToPage();
        actions.myFrSignupHelper.createNewUser(userName, password);
        actions.fOHActions.login(userName, password);
        actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        actions.extrasActions.xOutReserveSeatPopUp();

    };

    describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

        it(sprintf('Given I make a standard return trip with outbound > 1 < 7Days and return = 30 days from now with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
            // TODO MAKE BOOKING TO SEATS
            var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
            bookFlight(paxMap);
        });

        it('Add priority boarding', function () {
            actions.extrasActions.addPriorityBoarding();
            actions.extrasActions.verifyPriorityBoardingAdded();
        });

        it('Verify pricebreak down contains Priority Boarding price', function () {
            actions.extrasActions.skipExtras();
            actions.addPaxActions.verifyPriceBreakDownContainPriorityBoarding();

        });

        it('Continue on booking', function () {
            actions.addPaxActions.addSavedPaxNameNewUserMyRyanair(trip.journey.paxList);
            actions.addPaxActions.addContactForNewUserWithNoPhoneNumber();
            actions.addPaxActions.makeCardPaymentEnterBilling(trip.bookingContact.card);
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
            bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
        });

        it('When I click checkIn button', function () {
            actions.bookingSummaryActions.clickCheckInButton();
        });

        it('Then I fill Check In id documents', function () {
            actions.checkInActions.selectNationalityDropDown();
            actions.checkInActions.enterDateOfBirth(50);
            actions.checkInActions.enterDocumentType(1);
            actions.checkInActions.enterDocumentNumberField();
            actions.checkInActions.enterCountryOfIssueDropDown();
            actions.checkInActions.enterExpiryDate();
        });

        it('Then I click continue after adding id documents on check-in', function () {
            actions.checkInActions.clickBtnContinueCheckIn();
        });

        it('Then I complete second step of check-in', function () {
            actions.checkInActions.selectCheckInSeatCheckboxInbound();
            actions.checkInActions.clickBtnContinueCheckIn();
        });

        it('Then I purchase seats', function () {
            actions.seatsActions.assertOnSeatMapDisplayed();
            actions.seatsActions.selectOneSeatCheckIn(tripWay);
            selectedSeat = actions.seatsActions.returnOneWaySeat();
            actions.seatsActions.clickBtnConfirmCheckIn();
        });

        it('Then I pay for Seats', function () {
            actions.addPaxActions.makeCardPaymentWithoutContinuing(trip.bookingContact.card);
            actions.addPaxActions.enterBillingAddressAtCheckIn();
            actions.checkInActions.clickBtnContinueCheckIn();
        });

        it('Then I get Boarding Pass Ref', function () {
            actions.checkInActions.assertOnBookingRef(bookingRefActiveTrip);
        });

        it('Then I assert on selected seat', function () {
            actions.checkInActions.assertSeatsOnBpReturnInbound(selectedSeat);
        });
        it('Verify the priority mark shown on boarding pass', function () {
            actions.checkInActions.assertOnPriorityBpByMarket(tripWay);
        });
        it('Close boarding pass window', function () {
            actions.checkInActions.closeBoardingPassWindow();
        });
        it('Then I Logout', function () {
            actions.fOHActions.logout();
        });

    });
}

describe(specId + ' | RW-2331 | return | Registered User |Ensure that priority boarding is shown on the boarding pass - possible to test against diff markets', function () {

    sharedDescribe(1, 0, 0, 0);


});
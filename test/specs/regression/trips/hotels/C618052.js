var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;



function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
    var outBoundDaysFromNow = 12; //today index === 1
    var returnDaysFromNow = 15; //today index === 1
    var origin = "Dub";
    var destination = "Lis";
    var fareType = "standard";
    var tripWay = "twoway";
    var paxListMy;
    var cardMy;
    var hotelName;
    var hotelPrice;

    var bookFlight = function (paxMap) {
        var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        paxListMy = trip.journey.paxList;
        cardMy = trip.bookingContact.card;
    };

    describe('1 adult, 0 teen, 0 children, 0 infants', function () {

        it('Given I make a standard return trip with outbound 9 days and return 13 days from now with 1 adult, 0 teen, 0 children, 0 infants', function () {
            var paxMap = {ADT: 1, CHD: 0, INF: 0, TEEN: 0};
            bookFlight(paxMap);
        });

        it('When I am on potential trips page I add Hotel', function () {
            actions.extrasActions.addHotels();
            hotelName = actions.hotelsActions.addHotelToBooking(1);
            hotelPrice = actions.hotelsActions.confirmHotelOnBooking();
        });

        it('Then I should verify pricebreakdown', function () {
            actions.priceBreakDownActions.assertOnHotelsInfoPriceBkDown(hotelName, hotelPrice);
        });

        it('When I am on potential trips page I remove Hotel and add a different Hotel', function () {
            actions.extrasActions.removeHotels();
            actions.priceBreakDownActions.clickOnOpenPriceBreakDown();
            actions.extrasActions.addHotels();
            hotelName = actions.hotelsActions.addHotelToBooking(0);
            hotelPrice = actions.hotelsActions.confirmHotelOnBooking();
        });

        it('Then I should verify pricebreakdown for new hotel details', function () {
            actions.priceBreakDownActions.assertOnHotelsInfoPriceBkDown(hotelName, hotelPrice);
        });

        it('Then I continue on extras page and assert Hotel Info on PriceBkDown Payment Page', function () {
            actions.extrasActions.skipExtras();
            actions.priceBreakDownActions.assertOnHotelsInfoPriceBkDownPaymentPage();
        });

        it('Then I should pay for booking', function () {
            actions.addPaxActions.addPaxNameForAllPAX(paxListMy);
            actions.addPaxActions.addContact();
            actions.addPaxActions.makeCardPayment(cardMy);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I should get a booking ref', function () {
            //TODO verify successful Hotel Booking when bug FRW-4018 is resolved
            actions.bookingSummaryActions.verifyConfirmationMessage();
        });
    });

}

describe('TRIPS | Hotels | C618052 | TwoWay | OB > 7, RT > 7 | Add, Remove and Add New Hotel on Potential Trip', function () {

    sharedDescribe(1, 0, 0, 0);

});

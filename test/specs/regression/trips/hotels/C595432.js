var Trip = require('../../../../shared/model/Trip');

describe('TRIPS | Hotels | C595432 | TwoWay |OB < 7, RT > 7 | Added on Active Trip', function () {
    var outBoundDaysFromNow = 3; //today index === 1
    var returnDaysFromNow = 13; //today index === 1
    var origin = "Stn";
    var destination = "Bud";
    var fareType = "standard";
    var tripWay = "twoway";

    var bookFlight = function (paxMap) {
        var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        actions.extrasActions.skipExtras();
        actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
        actions.addPaxActions.addContact();
        actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
        actions.addPaxActions.enterBillingAddress();
        actions.addPaxActions.clickPaymentContinue();
    };

    describe('1 adult, 0 teen, 0 children, 0 infants', function () {

        it('Given I make a standard Return trip with outbound 3 days and Inbound 13 days from now with 1 adult, 0 teen, 0 children, 0 infants', function () {
            var paxMap = {ADT: 1, CHD: 0, INF: 0, TEEN: 0};
            bookFlight(paxMap);
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
        });

        it("Then I add Hotel on Active Trip Page and verify new tab opens", function () {
            actions.hotelsActions.clickAddHotelActive();
        });
    });
});

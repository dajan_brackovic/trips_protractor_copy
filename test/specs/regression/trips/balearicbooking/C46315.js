var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber, origin, destination) {
        var outBoundDaysFromNow = 3;//today index === 1
        var returnDaysFromNow = 7; //today index === 1
        var fareType = "standard";
        var tripWay = "twoway";
        var amountOfDiscounts = 3;

        var bookFlight = function (paxMap, origin, destination) {
            var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.fOHActions.searchReturnFLightAirportsOnly(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        }

        describe(sprintf('from %s to %s with %s adult, %s teen, %s children, %s infants',origin, destination, adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

            it(sprintf('Given I create a RT flight from %s to %s with %s adult, %s teen, %s children, %s infants', origin, destination, adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
                var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
                bookFlight(paxMap, origin, destination);
            });

            it('Then I verify that only 5% and 10% discount is offered', function () {
                actions.fOHActions.verifyAmountOfSpanishDiscounts(amountOfDiscounts);
            });

        });
}

describe('TRIPS | Balearic Booking | C46315 | 2 Spanish Domestic Discounts Offered', function () {

    sharedDescribe(1, 0, 0, 0, "BCN", "SDR");
    sharedDescribe(1, 0, 0, 0, "BCN", "SCQ");
    sharedDescribe(1, 0, 0, 0, "BCN", "VLL");
    sharedDescribe(1, 0, 0, 0, "MAD", "SCQ");

});
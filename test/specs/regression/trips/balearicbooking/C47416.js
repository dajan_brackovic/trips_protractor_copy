var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber, origin, destination) {
        var outBoundDaysFromNow = 3;//today index === 1
        var returnDaysFromNow = 7; //today index === 1
        var fareType = "standard";
        var tripWay = "twoway";
        var amountOfDiscounts = 6;

        var bookFlight = function (paxMap, origin, destination) {
            var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.fOHActions.searchReturnFLightAirportsOnly(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        }

        describe(sprintf('from %s to %s with %s adult, %s teen, %s children, %s infants',origin, destination, adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

            it(sprintf('Given I create a RT flight from %s to %s with %s adult, %s teen, %s children, %s infants', origin, destination, adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
                var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
                bookFlight(paxMap, origin, destination);
            });

            it('Then I verify that all 5 discount options are offered', function () {
                actions.fOHActions.verifyAmountOfSpanishDiscounts(amountOfDiscounts);
            });

        });
}

describe('TRIPS | Balearic Booking | C47416 | 5 Spanish Domestic Discounts Offered', function () {
    //TODO Reducing number of scenarios as test also exists in FOH suite.
    sharedDescribe(1, 0, 0, 0, "BCN", "FUE");
    sharedDescribe(1, 0, 0, 0, "BCN", "LPA");
    sharedDescribe(1, 0, 0, 0, "BCN", "IBZ");
    sharedDescribe(1, 0, 0, 0, "BCN", "XRY");
    sharedDescribe(1, 0, 0, 0, "BCN", "ACE");
    //sharedDescribe(1, 0, 0, 0, "BCN", "AGP");
    //sharedDescribe(1, 0, 0, 0, "BCN", "MAH");
    sharedDescribe(1, 0, 0, 0, "BCN", "PMI");
    sharedDescribe(1, 0, 0, 0, "BCN", "SVQ");
    sharedDescribe(1, 0, 0, 0, "BCN", "TFN");
    sharedDescribe(1, 0, 0, 0, "BCN", "TFS");
    sharedDescribe(1, 0, 0, 0, "MAD", "FUE");
    sharedDescribe(1, 0, 0, 0, "MAD", "LPA");
    sharedDescribe(1, 0, 0, 0, "MAD", "IBZ");
    sharedDescribe(1, 0, 0, 0, "MAD", "ACE");
    sharedDescribe(1, 0, 0, 0, "MAD", "PMI");
    sharedDescribe(1, 0, 0, 0, "MAD", "TFN");
    sharedDescribe(1, 0, 0, 0, "MAD", "TFS");

});

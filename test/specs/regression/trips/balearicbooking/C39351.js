var Trip = require('../../../../shared/model/Trip');
var Pages = require('../../../../Pages');
var pages = new Pages();

xdescribe('TRIPS | Balearic Booking - C39351 - Spanish Domestic 60% Discount 003/004 Validation', function () {
    var outBoundDaysFromNow = 3;//today index === 1
    var origin = "Bcn";
    var destination = "Ibi";
    var fareType = "standard";
    var tripWay = "oneway";
    var option = 60;
    var dniVal = "30000831E";  // 003 DNI
    var muniVal = "Algaida";
    var commVal = "Melilla";
    var famCerVal = "test";
    var cardMy;

    var bookFlight = function (paxMap) {
        var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
        actions.fOHActions.searchSpanishOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, option);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType,tripWay);
        actions.extrasActions.skipExtras();
        actions.addPaxActions.spanishaddPaxNameForAllPAXFor60(trip.journey.paxList, dniVal, muniVal, commVal, famCerVal);
        cardMy = trip.bookingContact.card;
    }

    describe('1 adult, 0 teen, 0 children, 0 infants', function () {

        it('Given I create a RT flight from A to B, with x adults, and y children', function () {
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            bookFlight(paxMap);
        });

        it('when I click continue button of passenger details', function () {
            actions.addPaxActions.clickBtnAddPaxBalearic();
        });

        it('Then I complete Document Check ', function () {
            actions.addPaxActions.documentCheckFirstSecond();
        });

        it('Then I click continue button of passenger details', function () {
            actions.addPaxActions.clickBtnAddPaxBalearic();
        });
            //DT-2772 Bug
        it('Then I add Contact', function () {
            actions.addPaxActions.addContact();
        });

        it('Then I make a payment', function () {
            actions.addPaxActions.makeCardPayment(cardMy);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I should get a booking ref', function () {
            // TODO expect booking ref
            actions.bookingSummaryActions.verifyConfirmationMessage();
        });

        it('When I do checkIN', function () {
            // TODO Make a Simple Check IN
            actions.checkInActions.todoSimpleCeckIn();
        });

        it('Then verify check IN', function () {
            // TODO expect check IN
            actions.checkInActions.verifySimpleCheckIn();
        });
    });


});




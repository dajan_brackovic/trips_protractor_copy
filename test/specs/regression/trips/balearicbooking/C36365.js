var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(numAdults, numTeens, numChildren, numInfants) {
        var outBoundDaysFromNow = 7; //actions.tripHelper.getRandomDate("NONE");//today index === 1
        var origin = "Bcn";
        var destination = "Ibi";
        var fareType = "standard";
        var tripWay = "oneway";
        var option = 10;
        var dniVal = ["30000067V", "30000056Y", "51494398C", "39139807V"];
        var commVal = "Melilla";
        var famCerVal = "test";
        var bookingRefActiveTrip;
        var totalPax = numAdults + numTeens + numChildren + numInfants;

        var bookFlight = function (paxMap) {
            var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
            actions.fOHActions.searchSpanishOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, option);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
            actions.extrasActions.skipExtras();
            actions.addPaxActions.spanishaddPaxNameForAllPAX(trip.journey.paxList, dniVal, commVal, famCerVal, numInfants);
            actions.addPaxActions.addContact();
            actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        };

        describe(sprintf('%s adult, %s teen, %s children, %s infants', numAdults, numTeens, numChildren, numInfants), function () {

            it(sprintf('Given I make a standard oneway trip with outbound < 7 Days from now with %s adult, %s teen, %s children, %s infants', numAdults, numTeens, numChildren, numInfants), function () {
                var paxMap = {ADT: numAdults, TEEN: numTeens, CHD: numChildren, INF: numInfants};
                bookFlight(paxMap);
            });

            it('Then I should get a booking ref', function () {
                actions.bookingSummaryActions.verifyConfirmationMessage();
                bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
            });

            it('When I click checkIn button', function () {
                actions.bookingSummaryActions.clickCheckInButton();
            });

            it('Then I fill Check In id documents', function () {
                actions.checkInActions.selectAllPax();
                actions.checkInActions.addDocsForMultiPaxBal(totalPax);
            });

            it('Then I complete second step of check-in', function () {
                actions.checkInActions.clickBtnContinueCheckIn();
            });

            it('Then I assert on correct number of Boarding Passes and click view all', function () {
                actions.checkInActions.countNumberOfBoardingPassesInList(totalPax - numInfants);
            });

            it('Then I get Boarding Pass Ref', function () {
                actions.checkInActions.assertOnBookingRef(bookingRefActiveTrip);
            });

            it('Then I assert on discount on boarding pass', function () {
                actions.checkInActions.assertSpanishDiscountOnBpForMultiPax(option);
            });
        });
}

describe('TRIPS | Balearic Booking 10 Percent | C36365 | outbound < 7 Days | Standard | Multi-Pax | CheckIn', function () {

    sharedDescribe(2, 2, 2, 2);

    sharedDescribe(1, 0, 1, 0);

});





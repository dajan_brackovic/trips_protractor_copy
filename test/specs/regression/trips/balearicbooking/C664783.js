var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(numAdults, numTeens, numChildren, numInfants) {
    var outBoundDaysFromNow = 2;//today index === 1
    var returnDaysFromNow = 6; //today index === 1
    var origin = "Bcn";
    var destination = "Sev";
    var fareType = "standard";
    var tripWay = "twoway";
    var bookingRefActiveTrip;
    var totalPax = numAdults + numTeens + numChildren + numInfants;

    var bookFlight = function (paxMap) {
        var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.fOHActions.searchSpanishReturnFLightUSCulture(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        actions.extrasActions.skipExtras();
        actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
        actions.addPaxActions.addContact();
        actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
        actions.addPaxActions.enterBillingAddress();
        actions.addPaxActions.clickPaymentContinue();
    }


    describe(sprintf('%s adult, %s teen, %s children, %s infants', numAdults, numTeens, numChildren, numInfants), function () {

        it(sprintf('Given I make a standard return trip with outbound < 7 Days and return < 7 days from now with %s adult, %s teen, %s children, %s infants', numAdults, numTeens, numChildren, numInfants), function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.selectMarketsMenu();
            actions.fOHActions.clickMarketList("United States");
            actions.fOHActions.assertUrl("us/en");
            var paxMap = {ADT: numAdults, TEEN: numTeens, CHD: numChildren, INF: numInfants};
            bookFlight(paxMap);
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
            bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
        });

        it('When I click checkIn button', function () {
            actions.bookingSummaryActions.clickCheckInButton();
        });

        it('Then I fill Check In id documents', function () {
            actions.checkInActions.selectAllPax();
            actions.checkInActions.addDocsForMultiPaxBal(totalPax);
        });

        it('Then I complete second step of check-in', function () {
            actions.checkInActions.clickBtnContinueCheckIn();
        });

        it('Then I assert on correct number of Boarding Passes and click view all', function () {
            actions.checkInActions.countNumberOfBoardingPassesInList(totalPax - numInfants);
        });

        it('Then I get Boarding Pass Ref', function () {
            actions.checkInActions.assertOnBookingRef(bookingRefActiveTrip);
        });

    });
}

describe('TRIPS | Balearic Booking US Culture | C664783 | outbound < 7 Days | Return < 7 Days | Standard | Multi-Pax | CheckIn', function () {

    sharedDescribe(1, 1, 1, 0);

});



var Trip = require('../../../../shared/model/Trip');
var Pages = require('../../../../Pages');
var pages = new Pages();

describe('TRIPS | Balearic Booking | C39350 | Spanish Domestic 60% Discount 000/002 Validation | CheckIn', function () {
    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE");//today index === 1
    var origin = "Bcn";
    var destination = "Ibi";
    var fareType = "standard";
    var tripWay = "oneway";
    var option = 60;
    var dniVal = ["30000260A", "51494398C", "39139807V"];  // 000 DNI
    var muniVal = "Algaida";
    var commVal = "Melilla";
    var famCerVal = "test";
    var cardMy;
    var bookingRefActiveTrip;

    var bookFlight = function (paxMap) {
        var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
        actions.fOHActions.searchSpanishOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, option);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType,tripWay);
        actions.extrasActions.skipExtras();
        actions.addPaxActions.spanishaddPaxNameForAllPAXFor60(trip.journey.paxList, dniVal, muniVal, commVal, famCerVal);
        cardMy = trip.bookingContact.card;
    }

    describe('1 adult, 0 teen, 0 children, 0 infants', function () {

        it('Given I create a RT flight from A to B, with x adults, and y children', function () {
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            bookFlight(paxMap);
        });

        it('when I click continue button of passenger details', function () {
            actions.addPaxActions.clickBtnAddPaxBalearic();
        });

        it('Then I complete Document Check for first time', function () {
            actions.addPaxActions.documentCheckFirstSecond();
        });

        it('When I edit Passenger details and add 002 DNI number', function () {
            dniVal = "30000529L";  // 002 DNI
            pages.tripsPaxPage.balearicDocumentNumber(0).clear();
            pages.tripsPaxPage.balearicDocumentNumber(0).sendKeys(dniVal);
            reporter.addMessageToSpec("Changed DNI To: " + dniVal);
        });

        it('Then I click continue button of passenger details', function () {
            actions.addPaxActions.clickBtnAddPaxBalearic();
        });

        it('Then I complete Document Check for Second time', function () {
            actions.addPaxActions.documentCheckFirstSecond();
        });

        it('When I edit Passenger details ', function () {
            muniVal = "Campanet";
            pages.tripsPaxPage.balearicMunicipalityValue(0, muniVal).click();
            reporter.addMessageToSpec("Changed Municipality To: " + muniVal);
        });

        it('Then I click continue button of passenger details', function () {
            actions.addPaxActions.clickBtnAddPaxBalearic();
        });

        it('Then I complete Document Check for Third time', function () {
            actions.addPaxActions.documentCheckThird();
        });

        it('Then I add Contact', function () {
            actions.addPaxActions.addContact();
        });

        it('Then I make a payment', function () {
            actions.addPaxActions.makeCardPayment(cardMy);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
            bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
        });

        it('When I click checkIn button', function () {
            actions.bookingSummaryActions.clickCheckInButton();
        });

        it('Then I fill Check In id documents', function () {
            actions.checkInActions.selectNationalityDropDown();
            actions.checkInActions.enterDateOfBirth(50);
        });

        it('Then I click continue after adding id documents on check-in', function () {
            actions.checkInActions.clickBtnContinueCheckIn();
        });

        it('Then I complete second step of check-in', function () {
            actions.checkInActions.clickBtnContinueCheckIn();
        });

        it('Then I get Boarding Pass Ref', function () {
            actions.checkInActions.assertOnBookingRef(bookingRefActiveTrip);
        });

        it('Then I assert on discount on boarding pass', function () {
            actions.checkInActions.assertSpanishDiscountOnBp(tripWay, option);
        });
    });


});




var Trip = require('../../../../shared/model/Trip');


describe('TRIPS | Balearic Booking | C39194 | Spanish Domestic oneway 60% Discount Validations', function () {
    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE");//today index === 1
    var origin = "Bcn";
    var destination = "Ibi";
    var fareType = "standard";
    var tripWay = "oneway";
    var option = 60;

    var bookFlight = function (paxMap) {
        var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
        actions.fOHActions.searchSpanishOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, option);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType,tripWay);
        actions.extrasActions.skipExtras();
    }

    describe('1 adult, 0 teen, 0 children, 0 infants', function () {

        it('Given I create a oneway flight from BCN to Ibiza with 60% Discount with 1 adults, 0 Teen, 0 children, o infant', function () {
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            bookFlight(paxMap);
        });

        it('When I click continue button on Add Pax page', function () {
            actions.addPaxActions.clickBtnAddPaxBalearic();
        });

        it('Then I catch mandatory fields of passenger details', function () {
            actions.addPaxActions.assertOnPaxDetailsRequired();
        });
    });
});





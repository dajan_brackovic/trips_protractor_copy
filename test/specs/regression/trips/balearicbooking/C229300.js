var Trip = require('../../../../shared/model/Trip');

describe('TRIPS | C229300 | Spanish Domestic | 5 % return within 7 days | Buy Bags and Seat at potential trip | CheckIn', function () {
    var outBoundDaysFromNow = 3;//today index === 1
    var returnDaysFromNow = 7; //today index === 1
    var origin = "Bcn";
    var destination = "Ibi";
    var fareType = "standard";
    var tripWay = "twoway";
    var option = 5;
    var dniVal = ["30000067V", "30000056Y", "51494398C", "39139807V"];
    var commVal = "Melilla";
    var famCerVal = "test";
    var bookingRefActiveTrip;
    var selectedSeatOut;
    var selectedSeatIn;
    var paxListMy;
    var cardMy;
    var addedBags = "15KG BAG";

    var bookFlight = function (paxMap) {
        var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.fOHActions.searchSpanishReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow, option);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType,tripWay);
        paxListMy = trip.journey.paxList;
        cardMy = trip.bookingContact.card;
    }

    describe('1 adult, 0 teen, 0 children, 0 infants', function () {

        it('Given I create a RT flight from A to B, with x adults, and y children', function () {
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            bookFlight(paxMap);
        });

        it('When I select bags', function () {
            actions.extrasActions.addBag();
            actions.bagsActions.addOneNormalBag();
            actions.bagsActions.addOneNormalBagForInboundSpanishDomestic();
        });

        it('Then I assert on bags total and sub total', function () {
            actions.bagsActions.assertOnBagsSubTotalAndTotal();
            actions.bagsActions.clickBagsBtnConfirm();
        });

        it('When I select a seat', function () {
            actions.extrasActions.addSeat();
            actions.seatsActions.selectOneSeat(tripWay);
        });

        it('Then I select a same seat for return', function () {
            actions.seatsActions.selectSameSeatReturn();
            selectedSeatOut = actions.seatsActions.returnTwoWaySeatOut();
            selectedSeatIn = actions.seatsActions.returnTwoWaySeatIn();
        });

        it('Then I should confirm a seat price', function () {
            actions.seatsActions.confirmSeat();
        });

        it('Then I continue on extras page', function () {
            actions.extrasActions.skipExtras();
        });

        it('Then I should pay for booking', function () {
            actions.addPaxActions.spanishaddPaxNameForAllPAX(paxListMy, dniVal, commVal, famCerVal);
            actions.addPaxActions.addContact();
            actions.addPaxActions.makeCardPayment(cardMy);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
            bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
        });

        it('When I click checkIn button', function () {
            actions.bookingSummaryActions.clickCheckInButton();
        });

        it('Then I fill Check In id documents', function () {
            actions.checkInActions.selectNationalityDropDown();
            actions.checkInActions.enterDateOfBirth(50);
        });

        it('Then I click continue after adding id documents on check-in', function () {
            actions.checkInActions.clickBtnContinueCheckIn();
        });

        it('Then I complete second step of check-in', function () {
            actions.checkInActions.assertSeatOnCheckInReturn(selectedSeatOut, selectedSeatIn);
            actions.checkInActions.clickBtnContinueCheckIn();
        });

        it('Then I get Boarding Pass Ref', function () {
            actions.checkInActions.assertOnBookingRef(bookingRefActiveTrip);
        });

        it('Then I assert on selected seat', function () {
            actions.checkInActions.assertSeatsOnBpReturn(selectedSeatOut, selectedSeatIn);
        });

        it('Then I assert on selected bags', function () {
            actions.checkInActions.assertBagsOnBp(tripWay, addedBags);
        });

        it('Then I assert on discount on boarding pass', function () {
            actions.checkInActions.assertSpanishDiscountOnBp(tripWay, option);
        });
    });
});





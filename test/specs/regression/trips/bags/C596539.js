var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(numAdult, numTeen, numChild, numInfants) {
    describe('(shared)', function () {
        var outBoundDaysFromNow = 4; //today index === 1
        var origin = "stn";
        var destination = "bcn";
        var fareType = "standard";
        var tripWay = "oneway";
        var paxListMy;
        var cardMy;

        var bookFlight = function (paxMap) {
            var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
            actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
            paxListMy = trip.journey.paxList;
            cardMy = trip.bookingContact.card;
        }

        describe(sprintf('%s adult, %s teen, %s children, %s infants', numAdult, numTeen, numChild, numInfants), function () {

            it(sprintf('Given I make a standard one way trip with outbound 4 days from now with %s adult, %s teen, %s children, %s infants', numAdult, numTeen, numChild, numInfants), function () {
                var paxMap = {ADT: numAdult, CHD: numChild, INF: numInfants, TEEN: numTeen};
                bookFlight(paxMap);
            });

            it('When I select one large and one normal bags for each passenger', function () {
                actions.extrasActions.addBag();
                actions.bagsActions.selectOneLargeAndOneNormalBagForEachPax();
            });

            it('Then I make sure that child bag price is half of adult bag price', function () {
                actions.bagsActions.assertOnDiscountMessageForChildBag();
                actions.bagsActions.assertOnAdultAndChildBagPrice();
            });

            it('Then I assert bags on pricebreakdown', function () {
                actions.bagsActions.assertBagsOnPriceBreakDown();
            });

            it('Then I continue on extras page', function () {
                actions.extrasActions.skipExtras();
            });

            it('Then I should pay for booking', function () {
                actions.addPaxActions.addPaxNameForAllPAX(paxListMy);
                actions.addPaxActions.addContact();
                actions.addPaxActions.makeCardPayment(cardMy);
                actions.addPaxActions.enterBillingAddress();
                actions.addPaxActions.clickPaymentContinue();
            });

            it('Then I should get a booking ref', function () {
                actions.bookingSummaryActions.verifyConfirmationMessage();
            });
        });
    });
}


describe('TRIPS | Bags | C596539 | OneWay Flight | Add Bags for muti pax', function () {

    sharedDescribe(2, 2, 0, 0);

});
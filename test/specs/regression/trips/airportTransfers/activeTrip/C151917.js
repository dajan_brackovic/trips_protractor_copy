describe("Active Trip Airport transfers | C151917 ", function () {

    var params = browser.params.conf;
    var Trip = require('../../../../../shared/model/Trip');

    describe("Case: 1 | User has selected single transfer for departure airport", function () {

        it("Given that user have booked flight and it's on active trip page", function () {
            actions.manageTripsActions.manageTripAccess(params.payment.refNumber12,params.payment.payMail2);
        });
        it("And that the user has selected single transfer for departure airport", function(){

        });
        it("Then Airport transfers side drawer should be opened", function () {
            actions.potentialTripActions.clickAirportTransfersCard();
            actions.selectTransferProviderActions.expectGreenCheckIcon(0);
            actions.selectTransferProviderActions.expectTransferFormMessageText(0);
            actions.selectTransferProviderActions.expectTransferFormMessageAmount(0, "1", "Single");
        });
    });

    describe("Case: 2 | User has selected single transfer for destination airport", function () {

        it("Given that user have booked flight and it's on active trip page", function () {
            actions.manageTripsActions.manageTripAccess(params.payment.refNumber13,params.payment.payMail2);
        });
        it("And that the user has selected single transfer for destination airport", function(){

        });
        it("Then Airport transfers side drawer should be opened", function () {
            actions.potentialTripActions.clickAirportTransfersCard();
            actions.selectTransferProviderActions.expectGreenCheckIcon(0);
            actions.selectTransferProviderActions.expectTransferFormMessageText(0);
            actions.selectTransferProviderActions.expectTransferFormMessageAmount(0, "1", "Single");
        });
    });

    describe("Case: 3 | User has selected return transfer for departure airport", function () {

        it("Given that user have booked flight and it's on active trip page", function () {
            actions.manageTripsActions.manageTripAccess(params.payment.refNumber14,params.payment.payMail2);
        });
        it("And that the user has selected return transfer for departure airport", function(){

        });
        it("Then Airport transfers side drawer should be opened", function () {
            actions.potentialTripActions.clickAirportTransfersCard();
            actions.selectTransferProviderActions.expectGreenCheckIcon(0);
            actions.selectTransferProviderActions.expectTransferFormMessageText(0);
            actions.selectTransferProviderActions.expectTransferFormMessageAmount(0, "1", "Return");
        });
    });

    describe("Case: 4 | User has selected return transfer for destination airport", function () {

        it("Given that user have booked flight and it's on active trip page", function () {
            actions.manageTripsActions.manageTripAccess(params.payment.refNumber15,params.payment.payMail2);
        });
        it("And that the user has selected return transfer for destination airport", function(){

        });
        it("Then Airport transfers side drawer should be opened", function () {
            actions.potentialTripActions.clickAirportTransfersCard();
            actions.selectTransferProviderActions.expectGreenCheckIcon(0);
            actions.selectTransferProviderActions.expectTransferFormMessageText(0);
            actions.selectTransferProviderActions.expectTransferFormMessageAmount(0, "1", "Return");
        });
    });
});

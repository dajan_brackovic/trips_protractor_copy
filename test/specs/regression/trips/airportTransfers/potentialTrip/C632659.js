var Trip = require('../../../../../shared/model/Trip');

describe("Potential Trip Airport Transfers | C632659 | User can Add and Modify Transfers Oneway and Twoway", function () {
    var params = browser.params.conf;
    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE");
    var returnDaysFromNow = actions.tripHelper.getRandomDateGreaterThanOutbound("NONE", outBoundDaysFromNow);
    var origin = "Lis";
    var destination = "Stn";
    var fareType = "standard";
    var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
    var tripWay;
    var trip;

    describe("Case 1:", function () {
        it("Given that the user has booked a return flight", function () {
            tripWay = "twoway";
            trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        });
        it("And that the user has the Transfer card opened", function () {
            actions.potentialTripActions.clickAirportTransfersCard();
            actions.selectTransferProviderActions.expectTitleAirportTransfers(params.airportTransfers.title);
            actions.selectTransferProviderActions.expectTotalPriceUpdate();
        });
        it("When the user select transfer route", function () {
            actions.selectTransferProviderActions.clickSelectProviderDropDown();
            actions.selectTransferProviderActions.clickSelectRouteRadioMarker(0);
            actions.selectTransferProviderActions.clickSelectRouteRadioMarker(2);
        });
        it("Then price should be updated", function () {
            actions.selectTransferProviderActions.expectTotalPriceUpdate();
        });
        it("When user remove passenger", function () {
            actions.selectTransferProviderActions.clickMinusButtonDecrement(0);
        });
        it("Then price should be updated", function () {
            actions.selectTransferProviderActions.expectTotalPriceUpdate();
        });

        it('Then I assert on Transfer on pricebreakdown on Potential Trip Page', function () {
            actions.selectTransferProviderActions.assertOnTransfersOnPriceBreakdown();
        });

        it('Then I assert on Transfer on pricebreakdown on Payment Page', function () {
            actions.selectTransferProviderActions.assertOnTransfersOnPaymentPage();
        });
    });

    describe("Case 2:", function () {
        it("Given that the user has booked a return flight", function () {
            tripWay = "twoway";
            trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        });

        it("And that the user has the Transfer card opened", function () {
            actions.potentialTripActions.clickAirportTransfersCard();
            actions.selectTransferProviderActions.expectTitleAirportTransfers(params.airportTransfers.title);
            actions.selectTransferProviderActions.expectTotalPriceUpdate();
            actions.selectTransferProviderActions.clickSelectProviderDropDown();
            actions.selectTransferProviderActions.clickSelectRouteRadioMarker(0);
            actions.selectTransferProviderActions.clickSelectRouteRadioMarker(2);
            actions.selectTransferProviderActions.expectTotalPriceUpdate();
            actions.selectTransferProviderActions.clickMinusButtonDecrement(0);
            actions.selectTransferProviderActions.expectTotalPriceUpdate();
        });

        it("When user add passenger", function () {
            actions.selectTransferProviderActions.clickPlusButtonIncrement(0);
        });

        it("Then price should be updated", function () {
            actions.selectTransferProviderActions.expectTotalPriceUpdate();
        });

        it('Then I assert on Transfer on pricebreakdown on Potential Trip Page', function () {
            actions.selectTransferProviderActions.assertOnTransfersOnPriceBreakdown();
        });

        it('Then I assert on Transfer on pricebreakdown on Payment Page', function () {
            actions.selectTransferProviderActions.assertOnTransfersOnPaymentPage();
        });

        it('Then I should pay for booking', function () {
            actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
            actions.addPaxActions.addContact();
            actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
        });
    });

    //one way

    describe("Case 3:", function () {
        it("Given that the user has booked a one way flight", function () {
            tripWay = "oneway";
            trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
            actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        });
        it("And that the user has the Transfer card opened", function () {
            actions.potentialTripActions.clickAirportTransfersCard();
        });
        it("Then title heading should be displayed", function () {
            actions.selectTransferProviderActions.expectTitleAirportTransfers(params.airportTransfers.title);
        });
        it("And total price should  not be displayed", function () {
            actions.selectTransferProviderActions.expectTotalPriceUpdate();
        });
    });

    describe("Case 4:", function () {
        it("Given that the user has booked a one way flight", function () {
            tripWay = "oneway";
            trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
            actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        });
        it("And that the user has the Transfer card opened", function () {
            actions.potentialTripActions.clickAirportTransfersCard();
            actions.selectTransferProviderActions.expectTitleAirportTransfers(params.airportTransfers.title);
            actions.selectTransferProviderActions.expectTotalPriceUpdate();
        });
        it("When the user select transfer route", function () {
            actions.selectTransferProviderActions.clickSelectProviderDropDown();
            actions.selectTransferProviderActions.clickSelectRouteRadioMarker(0);
            actions.selectTransferProviderActions.clickSelectRouteRadioMarker(2);
        });
        it("Then price should be updated", function () {
            actions.selectTransferProviderActions.expectTotalPriceUpdate();
        });
        it("When user remove passenger", function () {
            actions.selectTransferProviderActions.clickMinusButtonDecrement(0);
        });
        it("Then price should be updated", function () {
            actions.selectTransferProviderActions.expectTotalPriceUpdate();
        });
        it('Then I assert on Transfer on pricebreakdown on Potential Trip Page', function () {
            actions.selectTransferProviderActions.assertOnTransfersOnPriceBreakdown();
        });
        it('Then I assert on Transfer on pricebreakdown on Payment Page', function () {
            actions.selectTransferProviderActions.assertOnTransfersOnPaymentPage();
        });
    });

    describe("Case 5:", function () {
        it("Given that the user has booked a one way flight", function () {
            tripWay = "oneway";
            trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
            actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        });

        it("And that the user has the Transfer card opened", function () {
            actions.potentialTripActions.clickAirportTransfersCard();
            actions.selectTransferProviderActions.expectTitleAirportTransfers(params.airportTransfers.title);
            actions.selectTransferProviderActions.expectTotalPriceUpdate();
            actions.selectTransferProviderActions.clickSelectProviderDropDown();
            actions.selectTransferProviderActions.clickSelectRouteRadioMarker(0);
            actions.selectTransferProviderActions.clickSelectRouteRadioMarker(2);
            actions.selectTransferProviderActions.expectTotalPriceUpdate();
            actions.selectTransferProviderActions.clickMinusButtonDecrement(0);
            actions.selectTransferProviderActions.expectTotalPriceUpdate();
        });

        it("When user add passenger", function () {
            actions.selectTransferProviderActions.clickPlusButtonIncrement(0);
        });

        it("Then price should be updated", function () {
            actions.selectTransferProviderActions.expectTotalPriceUpdate();
        });

        it('Then I assert on Transfer on pricebreakdown on Potential Trip Page', function () {
            actions.selectTransferProviderActions.assertOnTransfersOnPriceBreakdown();
        });

        it('Then I assert on Transfer on pricebreakdown on Payment Page', function () {
            actions.selectTransferProviderActions.assertOnTransfersOnPaymentPage();
        });
    });

});

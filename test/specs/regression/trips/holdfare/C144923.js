var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

describe('TRIPS | Hold Fare | C144923 | Flight Select Screen | PNR Generated for Hold Fare', function () {
    var outBoundDaysFromNow = 15;//today index === 1
    var returnDaysFromNow = 18; //today index === 1
    var origin = "stn";
    var destination = "dub";
    var fareType = "standard";
    var tripWay = "twoway";
    var trip;
    var bookingRefHoldFare;
    var bookingRefActiveTrip;
    var lastFourCreditCardDigits = "1001";

    var bookFlight = function (paxMap) {
        trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
    };

    describe('Checking selected flight card information on RT flight', function () {

        it('Given I create a RT flight with 1 adults, 0 children, 0 infants, 0 teens, departing from STN and arriving in DUB', function () {
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            bookFlight(paxMap);
        });

        it(' When on Flight Select Page I verify that Hold Fare is disabled until after selecting flights ', function () {
            actions.tripsHomeActions.assertOnBtnHoldFareisNotPresent();
            actions.tripsHomeActions.selectAFlightOnly(0, 0, fareType,tripWay);
            actions.tripsHomeActions.assertOnBtnHoldFareisPresent();
        });

        it('And I select Hold Fare and Confirm selection', function () {
            actions.tripsHomeActions.clickBtnHoldFare();
            actions.tripsHomeActions.assertOnHoldFareDrowerTotal(tripWay);
            actions.holdFareActions.clickBtnConfirmHoldFare();
        });

        it('And I pay Hold Fare fee', function () {
            actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
            actions.addPaxActions.addContact();
            actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I get a booking Ref for Hold Fare', function () {
            actions.bookingSummaryActions.verifyConfirmationMessageOnHoldFare();
            bookingRefHoldFare = actions.bookingSummaryActions.returnHoldFare();
        });

        it('When on Manage Trip Page I click on Ryanair Home Page logo to return to home page', function () {
            actions.bookingSummaryActions.clickRyanairLogo();
        });

        it('Then I retrieve booking from manage trips section', function () {
            actions.manageTripsActions.manageTripAccessCardFromBookingRef(bookingRefHoldFare, lastFourCreditCardDigits);
        });

        it('When I click checkout button', function () {
            actions.bookingSummaryActions.clickCheckOutButton();
        });

        it('Then I should pay for the fare', function () {
            actions.addPaxActions.addContact();
            actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
            bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
        });

        it('Then I assert on PNR', function () {
            actions.bookingSummaryActions.assertOnHoldFarePnr(bookingRefHoldFare, bookingRefActiveTrip);
        });
    });

});


var Trip = require('../../../../shared/model/Trip');

describe('TRIPS | PriceBreakDown | C34504 | Assert total price with standard flight card OW ', function () {
    var outBoundDaysFromNow = 5; //today index === 1
    var origin = "stn";
    var destination = "edi";
    var fareType = "standard";
    var tripWay = "oneway";

    var bookFlight = function (paxMap) {
        actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
        actions.tripsHomeActions.selectAFlightOnly(0, 0, fareType, tripWay);
    }

    describe('1 adult, 0 teen, 0 children, 0 infants', function () {

        it('Given I have created a OW flight with 1 adults, 0 children, 0 infants, 0 teens from STN and to DUB and select first flight', function () {
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            bookFlight(paxMap);
        });

        it(' I click the the price breakdown', function () {
            actions.priceBreakDownActions.clickOnOpenPriceBreakDown();
        });

        it(' Then I compare total price at breakdown and total price selected flight card.', function () {
            actions.priceBreakDownActions.assertStandardPriceFlightCartAndPriceBeakDown("outbound");
        });
    });
});

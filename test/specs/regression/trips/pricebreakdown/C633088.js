var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(numAdults, numTeens, numChildren, numInfants) {
    var outBoundDaysFromNow = 5; //today index === 1
    var origin = "stn";
    var destination = "dub";
    var fareType = "standard";
    var tripWay = "oneway";

    var bookFlight = function (paxMap) {
        actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
        actions.tripsHomeActions.selectAFlightOnly(0, 0, fareType, tripWay);
    };

    describe(sprintf('%s adult, %s teen, %s children, %s infants', numAdults, numTeens, numChildren, numInfants), function () {

        it(sprintf('Given I make a standard one way trip with outbound < 7 Days from now with %s adult, %s teen, %s children, %s infants', numAdults, numTeens, numChildren, numInfants), function () {
            var paxMap = {ADT: numAdults, TEEN: numTeens, CHD: numChildren, INF: numInfants};
            bookFlight(paxMap);
        });

        it('When I open price breakdown', function () {
            actions.priceBreakDownActions.clickOnOpenPriceBreakDown();
        });

        it(' Then I assert on Pay by methods Fee', function () {
            actions.priceBreakDownActions.assertOnPayByMethodFee();
        });
    });
}

describe('TRIPS | Price BreakDown | C633088 | OW | Assert On Payment Methods', function () {

    sharedDescribe(1, 0, 0, 0);

});

var Trip = require('../../../../shared/model/Trip');

function sharedDescribe(option, url) {
    describe('', function () {
        var outBoundDaysFromNow = 5; //actions.tripHelper.getRandomDate("PARTIAL"); //today index === 1
        var returnDaysFromNow = 10; //actions.tripHelper.getRandomDate("PARTIAL"); //today index === 1
        var origin = "stn";
        var destination = "sxf";
        var fareType = "standard";
        var tripWay = "twoway";
        var originalTotalPrice;

        var bookFlight = function (paxMap) {
            actions.fOHActions.searchReturnFLightWithPaxMyFr(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlightOnly(0, 0, fareType, tripWay);
        };

        it('Given I have created a OW flight with 1 adults, 0 children, 0 infants, 0 teens from STN and to DUB and select first flight', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.selectMarketsMenu();
            actions.fOHActions.clickMarketList(option);
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            bookFlight(paxMap);
        });

        it(' I click the the price breakdown', function () {
            actions.priceBreakDownActions.clickOnOpenPriceBreakDown();
        });

        it(' Then I compare total price at breakdown and total price selected flight card.', function () {
            actions.priceBreakDownActions.assertStandardPriceFlightCartAndPriceBeakDown("outbound");
            actions.priceBreakDownActions.assertStandardPriceFlightCartAndPriceBeakDownReturn("inbound");
            originalTotalPrice = actions.priceBreakDownActions.returnTotal();
        });

        it(' Then I assert on Pay by methods Fee for Italian and French cultures', function () {
            actions.priceBreakDownActions.assertOnItalianFrenchPayByMethodFee(url);
        });

        it(' Then I select Debit Card and verify that Total price is 2% less', function () {
            actions.priceBreakDownActions.verify2PerCentDebitCardDiscount(originalTotalPrice);
        });

    });
}

describe('TRIPS | C636442 | Price BreakDown | 2% CC Fee displayed on Flight Price in IT Culture | TwoWay', function () {
    sharedDescribe("Italy", "it/it");
    sharedDescribe("France", "fr/fr");
});


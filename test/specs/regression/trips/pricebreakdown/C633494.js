var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(numAdults, numTeens, numChildren, numInfants) {
    var outBoundDaysFromNow = 2;//today index === 1
    var returnDaysFromNow = 5; //today index === 1
    var origin = "dub";
    var destination = "krk";
    var fareType = "standard";
    var tripWay = "twoway";
    var creditCardFee;
    var originalTotalPrice;
    var newTotalPrice;

    var bookFlight = function (paxMap) {
        actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.tripsHomeActions.selectAFlightOnly(0, 0, fareType, tripWay);
    }

    describe(sprintf('%s adult, %s teen, %s children, %s infants', numAdults, numTeens, numChildren, numInfants), function () {

        it(sprintf('Given I make a standard Rt trip with outbound < 7 Days and inbound < 7 days from now with %s adult, %s teen, %s children, %s infants', numAdults, numTeens, numChildren, numInfants), function () {
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            bookFlight(paxMap);
        });

        it('When I open price breakdown', function () {
            actions.priceBreakDownActions.clickOnOpenPriceBreakDown();
        });

        it('Then I assert on total price and click on credit card', function () {
            actions.priceBreakDownActions.assertOnTotalPriceBeakDownReturn();
            originalTotalPrice = actions.priceBreakDownActions.returnTotal();
            //actions.priceBreakDownActions.assertOnPaymentFeeLabel(false);
            actions.priceBreakDownActions.clickCheckBoxCreditCardOnPriceBreakDown();
            browser.sleep(2000);
            actions.priceBreakDownActions.assertOnPaymentFeeLabel(true);
        });

        it('Then I assert on updated total price and return new total and credit card fee', function () {
            actions.priceBreakDownActions.assertTotalPriceWhenCcOptionSelected(originalTotalPrice);
            newTotalPrice = actions.priceBreakDownActions.returnTotal();
            creditCardFee = actions.priceBreakDownActions.returnCreditCardFee();
        });

        it('Then I navigate to payment page', function () {
            actions.tripsHomeActions.clickBtnHomeBottomContinue();
            actions.extrasActions.skipExtras();
        });

        it('Then I assert on total price and credit card fee on payment page', function () {
            actions.addPaxActions.assertOnTotalPriceOnPaymentPage(newTotalPrice);
            actions.addPaxActions.assertOnCreditCardFeeOnPaymentPage(tripWay, creditCardFee);
        });

    });
}

describe('TRIPS | Price BreakDown | C633494 | RT | Select Credit Card on Flight Select Page', function () {

    sharedDescribe(1, 0, 0, 0);

});
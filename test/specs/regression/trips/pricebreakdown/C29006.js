var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(numAdults, numTeens, numChildren, numInfants, origin, destination, currency) {
    var outBoundDaysFromNow = 2; //today index === 1
    var fareType = "standard";
    var tripWay = "oneway";

    var bookFlight = function (paxMap) {
        actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
        actions.tripsHomeActions.selectAFlightOnly(0, 0, fareType, tripWay);
    };

    describe(sprintf('from %s to %s with %s adult, %s teen, %s children, %s infants', origin, destination, numAdults, numTeens, numChildren, numInfants), function () {

        it(sprintf('Given I create a one way flight from %s to %s with %s adult, %s teen, %s children, %s infants', origin, destination, numAdults, numTeens, numChildren, numInfants), function () {
            var paxMap = {ADT: numAdults, TEEN: numTeens, CHD: numChildren, INF: numInfants};
            bookFlight(paxMap, origin, destination);
        });


        it('When I open price breakdown', function () {
            actions.priceBreakDownActions.clickOnOpenPriceBreakDown();
        });

        it(' Then I assert on currency', function () {
            actions.priceBreakDownActions.assertOnCurrency(currency);
        });
    });
}

describe('TRIPS | Price BreakDown | C29006 | Display correct currency', function () {

    sharedDescribe(1, 0, 0, 0, "Dub", "Stn", "\u20ac");
    sharedDescribe(1, 0, 0, 0, "Stn", "Dub", "\u00A3");
    sharedDescribe(1, 0, 0, 0, "Krk", "Dub", "\u007a\u0142");
    sharedDescribe(1, 0, 0, 0, "Pra", "Stn", "\u004b\u010d");
    sharedDescribe(1, 0, 0, 0, "Cop", "Ltn", "Dk\u0072");
    sharedDescribe(1, 0, 0, 0, "Oslo", "Stn", "\u006b\u0072");
    sharedDescribe(1, 0, 0, 0, "Basel", "Stn", "SFr");    // \u0043\u0048\u0046  unicode for chf

});

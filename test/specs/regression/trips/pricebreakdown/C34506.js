var Trip = require('../../../../shared/model/Trip');

describe('TRIPS | PriceBreakDown | C34506 | Assert total price with business flight card RT ', function () {
    var outBoundDaysFromNow = 2;//today index === 1
    var returnDaysFromNow = 3; //today index === 1
    var origin = "dub";
    var destination = "krk";
    var fareType = "business";
    var tripWay = "twoway";

    var bookFlight = function (paxMap) {
        actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.tripsHomeActions.selectAFlightOnly(0, 0, fareType, tripWay);
    }

    describe('1 adult, 0 teen, 0 children, 0 infants', function () {

        it('Given I have created a RT flight with 1 adults, 0 children, 0 infants, 0 teens from STN and to DUB and  select first flight', function () {
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            bookFlight(paxMap);
        });

        it('When I click the the price breakdown', function () {
            actions.priceBreakDownActions.clickOnOpenPriceBreakDown();
        });

        it('Then I should compare flight price is correct in price breakdown', function(){
            actions.priceBreakDownActions.assertBusinessPriceFlightCartAndPriceBeakDownReturn("inbound");
            actions.priceBreakDownActions.assertOnTotalPriceBeakDownBusinessReturn();
        });
    });
});

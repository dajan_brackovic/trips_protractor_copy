var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(numAdults, numTeens, numChildren, numInfants, origin, destination) {
    var outBoundDaysFromNow = 2; //today index === 1
    var returnDaysFromNow = 5; //today index === 1
    var fareType = "standard";
    var tripWay = "twoway";

    var bookFlight = function (paxMap) {
        actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.tripsHomeActions.selectAFlightOnly(0, 0, fareType, tripWay);
    };

    describe(sprintf('from %s to %s with %s adult, %s teen, %s children, %s infants', origin, destination, numAdults, numTeens, numChildren, numInfants), function () {

        it(sprintf('Given I create a one way flight from %s to %s with %s adult, %s teen, %s children, %s infants', origin, destination, numAdults, numTeens, numChildren, numInfants), function () {
            var paxMap = {ADT: numAdults, TEEN: numTeens, CHD: numChildren, INF: numInfants};
            bookFlight(paxMap, origin, destination);
        });

        it('When I open price breakdown', function () {
            actions.priceBreakDownActions.clickOnOpenPriceBreakDown();
        });

        it('Then I assert on uk child discount', function () {
            actions.priceBreakDownActions.assertOnUkChildDiscountLabel(true);
        });

        it('Then I assert on total price', function () {
            actions.priceBreakDownActions.assertOnTotalWhenUkChildDiscountPresentReturn();
        });

        it('Then I navigate to payment page', function () {
            actions.tripsHomeActions.clickBtnHomeBottomContinue();
            actions.extrasActions.skipExtras();
        });

        it('Then I assert UK child discount present on payment page', function () {
            actions.addPaxActions.assertOnUkChildDiscountOnPaymentPage("oneway"); // passing oneway because Dub-Stn is not child discount route
        });
    });
}

describe('TRIPS | Price BreakDown | C29017 | Rt | Uk Child Discount', function () {

    sharedDescribe(1, 0, 1, 0, "Stn", "bcn");

});

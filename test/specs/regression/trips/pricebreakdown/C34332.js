var Trip = require('../../../../shared/model/Trip');

describe('TRIPS | PriceBreakDown | C34332 | No Flight Selected message verification', function () {
    var outBoundDaysFromNow = 5;//today index === 1
    var returnDaysFromNow = 6; //today index === 1
    var origin = "stn";
    var destination = "dub";

    var bookFlight = function (paxMap) {
        actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
    }

    describe('1 adult, 0 teen, 0 children, 0 infants', function () {

        it('Given I have created a RT flight with 1 adults, 0 children, 0 infants, 0 teens from STN and to DUB and do not select any flight', function () {
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            bookFlight(paxMap);
        });

        it(' I click the the price breakdown', function () {
            actions.priceBreakDownActions.clickOnOpenPriceBreakDown();
        });

        it(' Then the menu should have no flight display image ', function () {
            actions.priceBreakDownActions.assertOnNoFlightIcon();
        });
    });
});

describe("TRIPS | Active Trip Insurance | C133101 | Ensure that total price is updated after modifying insurance", function () {

    var params = browser.params.conf;
    var Trip = require('../../../../../shared/model/Trip');

    describe("Case:C133101 | 1 Adults, 0 Teens, 0 Children, 0 Infants | DUB-CIA oneWay", function () {

        it("Given that user have booked flight and it's on active trip page", function () {
            actions.manageTripsActions.manageTripAccess(params.payment.refNumber7,params.payment.payMail);
        });
        it("Then user is on insurance side drawer and have choosen insurance", function () {
            actions.potentialTripActions.clickAddInsurance();
        });
        it("When user want to modify insurance type", function () {
            actions.insuranceActions.selectCountryOfResidenceForPassanger(0, "Ireland");
            actions.insuranceActions.clickStandardInsuranceButton(0);
        });
        it("Then insurance type is modified and total price is updated", function () {

        });
    });
});
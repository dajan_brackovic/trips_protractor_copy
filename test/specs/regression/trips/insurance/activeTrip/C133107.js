describe("TRIPS | Active Trip Insurance | C133107 | Ensure that use can view policy pdf ", function () {

    var params = browser.params.conf;
    var Trip = require('../../../../../shared/model/Trip');

    describe("Case:C133107 | 1 Adults, 0 Teens, 0 Children, 0 Infants | DUB-CIA oneWay", function () {

        it("Given that user have booked flight and it's on active trip page", function () {
            actions.manageTripsActions.manageTripAccess(params.payment.refNumber7, params.payment.payMail);
        });
        it("Then the user has the insurance side drawer opened", function () {
            actions.potentialTripActions.clickAddInsurance();
        });
        it("When user click to View Policy '+' button", function () {
            browser.sleep(1000);
            actions.insuranceActions.clickViewPolicyPlusButton();
        });
        it("Then user get a drop down list to choose policy for residents", function () {
            actions.insuranceActions.expectPolicyDropDown();
            browser.sleep(1000);
            actions.insuranceActions.chooseCountryOfResidence("Ireland");
        });
        it("Then open View Policy PDF", function () {
            actions.insuranceActions.clickToOpenInsurancePDFinNewTab("ie");
            actions.insuranceActions.expectTitleForInsurancePolicy("Ryanair Travel Insurance");
        });
    });
});
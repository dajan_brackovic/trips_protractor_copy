describe("TRIPS | Active Trip Insurance | C133100 | Ensure that user can modify insurance", function () {

    var params = browser.params.conf;
    var Trip = require('../../../../../shared/model/Trip');

    describe("Case:C133100 | 1 Adults, 0 Teens, 0 Children, 0 Infants | DUB-CIA oneWay", function () {

        it("Given that user have booked flight and it's on active trip page", function () {
            actions.manageTripsActions.manageTripAccess(params.payment.refNumber7,params.payment.payMail);
            //TODO ref number with two passengers and one insurance purchased
        });
        it("That user is on insurance side drawer and have choosen insurance", function () {
            actions.potentialTripActions.clickAddInsurance();
        });
        it("When user want to modify insurance type", function () {
            browser.sleep(1000);
            actions.insuranceActions.expectTotalPriceToBe("0.00");
            browser.sleep(1000);
            actions.insuranceActions.selectCountryOfResidenceForPassanger(0, "Ireland");
            actions.insuranceActions.clickStandardInsuranceButton(0);
            actions.insuranceActions.expectTotalPriceToBeGreaterThenZero();
            actions.insuranceActions.clickConfirmButton();
        });
        it("Then insurance type is modified", function () {

        });
    });
});
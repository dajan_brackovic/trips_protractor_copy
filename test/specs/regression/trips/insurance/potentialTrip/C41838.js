describe("TRIPS | Potential Trip Insurance | C41838 | Ensure that total price is updated | Return Trip ", function () {
    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE");
    var returnDaysFromNow = actions.tripHelper.getRandomDate("PARTIAL");
    var origin = "Ltn";
    var destination = "Dub";
    var fareType = "standard";
    var tripWay = "twoway";
    var params = browser.params.conf;
    var currentPrice;

    describe("Case:C41838 | 1 Adults, 0 Teens, 0 Children, 0 Infants | LTN-DUB ReturnTrip", function () {

        it("Given that user is on insurance side drawer and have choosen insurance", function () {
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
            actions.potentialTripActions.clickAddInsurance();
            actions.insuranceActions.selectCountryOfResidenceForPassanger(0, "Ireland");
            actions.insuranceActions.clickInsurancePlusButton(0);
            currentPrice = actions.insuranceActions.getPriceFromCorner();
            //actions.insuranceActions.expectTotalPriceToBe("14.70");
            actions.insuranceActions.clickConfirmButton();
        });
        it("Then shopping basket is updated with the equipment prices", function () {
            actions.potentialTripActions.openPriceBreakDownClick();
            actions.potentialTripActions.expectProductCardUpdatedInsurance(0,3,"Insurance");
            actions.potentialTripActions.openPriceBreakDownClick();
        });
        it("When user want to modify insurance type", function () {
            actions.potentialTripActions.clickAddInsurance();
            browser.sleep(1000);
            actions.insuranceActions.clickStandardInsuranceButton(0);
            browser.sleep(1000);
            actions.insuranceActions.expectCurrentPriceIsDifferentThenChanged(currentPrice);
            //actions.insuranceActions.expectTotalPriceToBe("12.11");
            actions.insuranceActions.clickConfirmButton();
        });
        it("Then pop up with information added to price break down", function () {
            browser.sleep(1000);
        });
        it("Then shopping basket is updated with the equipment prices", function () {
            actions.potentialTripActions.openPriceBreakDownClick();
            actions.potentialTripActions.expectProductCardUpdatedInsurance(0,3,"Insurance");
            actions.potentialTripActions.openPriceBreakDownClick();
        });
        it("Then information message appears on the '2nd line Header -  Product has been added - anything else you need - Copy may not be correct'", function () {
            actions.potentialTripActions.expectInformationMessageAddedPriceShown();
        });
        it("Then product card has been update - with product that has been added", function () {
            actions.potentialTripActions.expectInformationLabelOnCardInsurance("1 Insurance");
            actions.potentialTripActions.modifyButtonCheckInsurance();
        });
    });
});
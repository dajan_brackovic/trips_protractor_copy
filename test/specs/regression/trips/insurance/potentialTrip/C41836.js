describe("TRIPS | Potential Trip Insurance | C41836 | Ensure that user is able to modify insurance options for each passenger | Return Trip ", function () {

    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE");
    var returnDaysFromNow = actions.tripHelper.getRandomDate("PARTIAL");
    var origin = "Dub";
    var destination = "Cia";
    var fareType = "standard";
    var tripWay = "twoway";
    var currentPrice;

    describe("Case:C41836 | 1 Adults, 0 Teens, 0 Children, 0 Infants | DUB-CIA ReturnTrip", function () {

        it("Given that user is on insurance side drawer and have choose insurance", function () {
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
            actions.potentialTripActions.clickAddInsurance();
            actions.insuranceActions.selectCountryOfResidenceForPassanger(0, "Ireland");
            actions.insuranceActions.clickStandardInsuranceButton(0);
            currentPrice = actions.insuranceActions.getPriceFromCorner();
            //actions.insuranceActions.expectTotalPriceToBe("12.11");
            actions.insuranceActions.clickConfirmButton();
        });
        it("When user want to modify insurance type", function () {
            actions.potentialTripActions.clickAddInsurance();
            browser.sleep(1000);
            actions.insuranceActions.expectEqualPrices(currentPrice);
            //actions.insuranceActions.expectTotalPriceToBe("12.11");
            actions.insuranceActions.clickInsurancePlusButton(0);
            browser.sleep(1000);
            actions.insuranceActions.expectCurrentPriceIsDifferentThenChanged(currentPrice);
            //actions.insuranceActions.expectTotalPriceToBe("14.70");
            actions.insuranceActions.clickConfirmButton();
        });
        it("Then insurance type is modified", function () {
            actions.potentialTripActions.clickAddInsurance();
            browser.sleep(1000);
            actions.insuranceActions.expectCurrentPriceIsDifferentThenChanged(currentPrice);
            //actions.insuranceActions.expectTotalPriceToBe("14.70");
        });
    });
});
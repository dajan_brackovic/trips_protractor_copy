var Trip = require('../../../../../shared/model/Trip');

describe("TRIPS | Potential Trip Insurance | C42176 | Ensure that when pop up asking for multiple passengers Yes is selected by default | Return Trip", function () {
    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE");
    var returnDaysFromNow = actions.tripHelper.getRandomDate("PARTIAL");
    var origin = "Dub";
    var destination = "Cia";
    var fareType = "standard";
    var tripWay = "twoway";

    describe("Case:C42176 | 1 Adults, 0 Teens, 0 Children, 0 Infants | DUB-CIA TwoWay", function () {

        it("Given that user is on potential trips page", function () {
            var paxMap = {ADT: 2, TEEN: 0, CHD: 0, INF: 0};
            actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        });
        it("Given that user open insurance card drawer", function () {
            actions.potentialTripActions.clickAddInsurance();
        });
        it("When user selects insurance option for the first passenger travelling", function () {
            actions.insuranceActions.selectCountryOfResidenceForPassanger(0, "Ireland");
        });
        it("When user wishes to add insurance for Multiple passengers travelling", function () {
            actions.insuranceActions.clickStandardInsuranceButton(0);
        });
        it("Then displays Pop Up Modal - asking the passenger to make a selection if they wish to add the same country of residence for all passengers travelling", function () {
            actions.insuranceActions.expectQuestionTextOnInsurancePopup();
        });
        it("Then Yes please (This option is selected as a default)", function () {
            actions.insuranceActions.expectYesThanksIsSelected();
        });
    });
});
var Trip = require('../../../../shared/model/Trip');

function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
    var outBoundDaysFromNow = 2; //today index === 1
    var returnDaysFromNow = 8; //today index === 1
    var origin = "Dub";
    var destination = "Cologne";
    var fareType = "standard";
    var tripWay = "twoway";
    var paxListMy;
    var cardMy;
    var bookingRefActiveTrip;


    var bookFlight = function (paxMap) {
        var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        paxListMy = trip.journey.paxList;
        cardMy = trip.bookingContact.card;
    };

    describe('1 adult, 0 teen, 0 children, 0 infants', function () {

        it('Given I make a standard return trip with outbound 4 days and return 8 days from now with 1 adult, 0 teen, 0 children, 0 infants', function () {
            var paxMap = {ADT: 1, CHD: 0, INF: 0, TEEN: 0};
            bookFlight(paxMap);
        });

        it('Then I continue on extras page', function () {
            actions.extrasActions.skipExtras();
        });

        it('Then I should pay for booking', function () {
            actions.addPaxActions.addPaxNameForAllPAX(paxListMy);
            actions.addPaxActions.addContact();
            actions.addPaxActions.makeCardPayment(cardMy);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
            bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
        });

        it('When on Manage Trip Page I click on Ryanair Home Page logo to return to home page', function () {
            actions.bookingSummaryActions.clickRyanairLogo();
        });

        it('Then I retrieve booking using flight details from manage trips section', function () {
            actions.manageTripsActions.manageTripAccessCardFromFlightDetails(outBoundDaysFromNow, origin, destination, bookingRefActiveTrip);
        });

        it('Then I assert on retrieved booking ref', function () {
            actions.bookingSummaryActions.assertOnRetrieveBooking(bookingRefActiveTrip);
        });

    });
}

describe('TRIPS | Active Trip | C618068 | Simple Booking | Retrieve Booking From Flight Details ', function () {

    sharedDescribe(1, 0, 0, 0);
});

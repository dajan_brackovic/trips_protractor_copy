var Trip = require('../../../../shared/model/Trip');
var params = browser.params.conf;

describe('TRIPS | Active Trip | C144763 | Two Way | Outbound < 7 days | Inbound > 7 days | CheckIn Outbound and buy seat at checkIn | Active trip buy Seat for Inbound', function () {

    var outBoundDaysFromNow = 4; //today index === 1
    var returnDaysFromNow = 10; //today index === 1
    var origin = "stn";
    var destination = "bud";
    var fareType = "standard";
    var tripWay = "twoway";
    var bookingRefActiveTrip;
    var lastFourCreditCardDigits = "1001";
    var selectedSeat;
    var trip;

    var bookFlight = function (paxMap) {
        trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType,tripWay);
        actions.extrasActions.skipExtras();
        actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
        actions.addPaxActions.addContact();
        actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
        actions.addPaxActions.enterBillingAddress();
        actions.addPaxActions.clickPaymentContinue();
    }

    describe('1 adult, 0 teen, 0 children, 0 infants', function () {

        it('Given I make a standard return trip with outbound 4 days and return 8 days from now with 1 adult, 0 teen, 0 children, 0 infants', function () {
            var paxMap = {ADT: 1, CHD: 0, INF: 0, TEEN: 0};
            bookFlight(paxMap);
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
            bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
        });

        it('When I click checkIn button', function () {
            actions.bookingSummaryActions.clickCheckInButton();
        });

        it('Then I fill Check In id documents', function () {
            actions.checkInActions.selectNationalityDropDown();
            actions.checkInActions.enterDateOfBirth(60);
            actions.checkInActions.enterDocumentType(1);
            actions.checkInActions.enterDocumentNumberField();
            actions.checkInActions.enterCountryOfIssueDropDown();
            actions.checkInActions.enterExpiryDate();
        });

        it('Then I click continue after adding id documents on check-in', function () {
            actions.checkInActions.clickBtnContinueCheckIn();
        });

        it('Then I complete second step of check-in', function () {
            actions.checkInActions.unSelectFlightCheckboxRtIn();
            tripWay = "oneway"; // here we are checking in outbound flight only
            actions.checkInActions.selectCheckboxSeat(tripWay);
            actions.checkInActions.clickBtnContinueCheckIn();
        });

        it('Then I purchase premium seats', function () {
            actions.seatsActions.selectOneSeatPremiumCheckIn(tripWay);
            selectedSeat = actions.seatsActions.returnOneWaySeat();
            actions.seatsActions.clickBtnConfirmCheckIn();
        });

        it('Then I pay for Seats', function () {
            actions.addPaxActions.makeCardPaymentWithoutContinuing(trip.bookingContact.card);
            actions.addPaxActions.enterBillingAddressAtCheckIn();
            actions.checkInActions.clickBtnContinueCheckIn();
        });

        it('Then I get Boarding Pass Ref', function () {
            // TODO failing due to - PRP-1032 and PRP-879
            actions.checkInActions.assertOnBookingRef(bookingRefActiveTrip);
        });

        it('Then I assert on selected seat', function () {
            actions.checkInActions.assertSeatsOnBp(selectedSeat);
            actions.checkInActions.clickBtnContinueCheckIn();
        });

        it('When on Manage Trip Page I click on Ryanair Home Page logo to return to home page', function () {
            actions.bookingSummaryActions.clickRyanairLogo();
        });

        it('Then I retrieve booking from manage trips section', function () {
            actions.manageTripsActions.manageTripAccessCardFromBookingRef(bookingRefActiveTrip, lastFourCreditCardDigits);
        });

        it('Then I click on add seat button on active trips page', function () {
            actions.extrasActions.addSeat();
        });

        it('Then I select inbound seat on active trip page', function () {
            browser.sleep(2000);
            actions.seatsActions.selectOneSeat(tripWay);
            actions.seatsActions.clickBtnConfirm();
        });

        it('Then I click continue on active trip page', function () {
            actions.bookingSummaryActions.clickTopContinueBtn();
        });

        it('Then I should pay for seat', function () {
            actions.addPaxActions.makeCardPaymentActive(trip.bookingContact.card);
        });

        it('Then I make sure that add seat button is not present at active trip page', function () {
            actions.bookingSummaryActions.assertOnAddSeatButtonNotPresent();
        });

    });
});

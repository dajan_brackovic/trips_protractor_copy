var Trip = require('../../../../shared/model/Trip');

function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
    var outBoundDaysFromNow = 3;//actions.tripHelper.getRandomDate("NONE"); //today index === 1
    var returnDaysFromNow = 5;//actions.tripHelper.getRandomDateGreaterThanOutbound("PARTIAL", outBoundDaysFromNow); //today index === 1
    var origin = "Dub";
    var destination = "krk";
    var fareType = "standard";
    var tripWay = "twoway";
    var paxListMy;
    var cardMy;
    var bookingRefActiveTrip;

    var bookFlight = function (paxMap) {
        var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        paxListMy = trip.journey.paxList;
        cardMy = trip.bookingContact.card;
    };

    describe('1 adult, 0 teen, 0 children, 0 infants', function () {

        it('Given I make a standard return trip with outbound 3 days and return 5 days from now with 1 adult, 0 teen, 0 children, 0 infants', function () {
            var paxMap = {ADT: 1, CHD: 0, INF: 0, TEEN: 0};
            bookFlight(paxMap);
        });

        it('Then I continue on extras page', function () {
            actions.extrasActions.skipExtras();
        });

        it('Then I should pay for booking', function () {
            actions.addPaxActions.addPaxNameForAllPAX(paxListMy);
            actions.addPaxActions.addContact();
            actions.addPaxActions.makeCardPayment(cardMy);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
            bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
        });

        it('Then CheckIn should be available', function () {
            actions.bookingSummaryActions.assertOnCheckInButton(true);
            actions.bookingSummaryActions.assertOnCheckInUnavailable(false);
        });

        it('Then I select bag on active trips page', function () {
            browser.executeScript('window.scrollTo(0,1500);');
            browser.sleep(2000);
            actions.extrasActions.addBag();
            actions.bagsActions.addOneNormalBag();
            actions.bagsActions.clickBagsBtnConfirm();
        });

        it('Then CheckIn should be unavailable', function () {
            actions.bookingSummaryActions.assertOnCheckInButton(false);
            actions.bookingSummaryActions.assertOnCheckInUnavailable(true);
        });

        it('Then I click continue on active trip page', function () {
            actions.bookingSummaryActions.clickTopContinueBtn();
        });

        it('Then I should pay for bags', function () {
            actions.addPaxActions.makeCardPaymentActive(cardMy);
        });

        it('Then I make sure that CheckIn become available again', function () {
            actions.bookingSummaryActions.assertOnCheckInButton(true);
            actions.bookingSummaryActions.assertOnCheckInUnavailable(false);
        });
    });
}

describe('TRIPS | Active Trip | C634080 | Simple Booking | CheckIn not available when product is selected', function () {

    sharedDescribe(1, 0, 0, 0);
});

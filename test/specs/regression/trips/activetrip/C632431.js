var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(numAdult, numTeen, numChild, numInfants) {
    var outBoundDaysFromNow = 4; //today index === 1
    var origin = "stn";
    var destination = "bcn";
    var fareType = "standard";
    var tripWay = "oneway";
    var paxListMy;
    var cardMy;
    var bookingRefActiveTrip;
    var lastFourCreditCardDigits = "1001";

    var bookFlight = function (paxMap) {
        var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
        actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        paxListMy = trip.journey.paxList;
        cardMy = trip.bookingContact.card;
    };

    describe(sprintf('%s adult, %s teen, %s children, %s infants', numAdult, numTeen, numChild, numInfants), function () {

        it(sprintf('Given I make a standard one way trip with outbound 4 days from now with %s adult, %s teen, %s children, %s infants', numAdult, numTeen, numChild, numInfants), function () {
            var paxMap = {ADT: numAdult, CHD: numChild, INF: numInfants, TEEN: numTeen};
            bookFlight(paxMap);
        });

        it('When I select one large and one normal bags for two passengers', function () {
            actions.extrasActions.addBag();
            actions.bagsActions.selectOneLargeAndOneNormalBagForMultiPax(0, 2);
        });

        it('Then I assert bags on pricebreakdown', function () {
            actions.bagsActions.assertBagsOnPriceBreakDown();
        });

        it('Then I continue on extras page', function () {
            actions.extrasActions.skipExtras();
        });

        it('Then I should pay for booking', function () {
            actions.addPaxActions.addPaxNameForAllPAX(paxListMy);
            actions.addPaxActions.addContact();
            actions.addPaxActions.makeCardPayment(cardMy);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
            bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
        });

        it('Then I make sure bags are added for 2 pax ', function () {
            actions.bookingSummaryActions.assertOnInfoMessageNumberOfBagsAddedOnBagsCard(4);
        });

        it('When on Manage Trip Page I click on Ryanair Home Page logo to return to home page', function () {
            actions.bookingSummaryActions.clickRyanairLogo();
        });

        it('Then I retrieve booking from manage trips section', function () {
            actions.manageTripsActions.manageTripAccessCardFromBookingRef(bookingRefActiveTrip, lastFourCreditCardDigits);
        });

        it('Then I select bags for 3rd passenger on active trip page', function () {
            actions.extrasActions.addBag();
            actions.bagsActions.selectOneLargeAndOneNormalBagForMultiPax(4, 0);
            actions.bagsActions.clickBagsBtnConfirm();
        });

        it('Then I click continue on active trip page', function () {
            actions.bookingSummaryActions.clickTopContinueBtn();
        });

        it('Then I should pay for bags', function () {
            actions.addPaxActions.makeCardPaymentActive(cardMy);
        });

        it('Then I make sure bags for third passenger is added ', function () {
            actions.bookingSummaryActions.assertOnInfoMessageNumberOfBagsAddedOnBagsCard(6);
        });

    });

}


describe('TRIPS | Active Trip | C632431 | Add Bags for 2 Pax on potential Trip | Add bag for 3rd Pax on Active Trip', function () {

    sharedDescribe(3, 0, 0, 0);

});
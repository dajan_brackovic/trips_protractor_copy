var Trip = require('../../../../shared/model/Trip');
var params = browser.params.conf;

describe('TRIPS | Active Trip | C144702 | One Way | Buy Seat at Potential Trip | Make Sure Add Seat Button is disabled on Active Trip | Buy bags at active Trip', function () {

    var outBoundDaysFromNow = 3; //today index === 1
    var origin = "dub";
    var destination = "edi";
    var fareType = "standard";
    var tripWay = "oneway";
    var paxListMy;
    var cardMy;
    var bookingRefActiveTrip;
    var lastFourCreditCardDigits = "1001";

    var bookFlight = function (paxMap) {
        var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
        actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        paxListMy = trip.journey.paxList;
        cardMy = trip.bookingContact.card;
    }

    describe('1 adult, 0 teen, 0 children, 0 infants', function () {

        it('Given I make a standard one way trip with outbound 2 days from now with 1 adult, 0 teen, 0 children, 0 infants', function () {
            var paxMap = {ADT: 1, CHD: 0, INF: 0, TEEN: 0};
            bookFlight(paxMap);
        });

        it('When I select a seat', function () {
            actions.extrasActions.addSeat();
            actions.seatsActions.selectOneSeat(tripWay);
        });

        it('Then I should confirm a seat price', function () {
            actions.seatsActions.confirmSeat();
        });

        it('Then I continue on extras page', function () {
            actions.extrasActions.skipExtras();
        });

        it('Then I should pay for booking', function () {
            actions.addPaxActions.addPaxNameForAllPAX(paxListMy);
            actions.addPaxActions.addContact();
            actions.addPaxActions.makeCardPayment(cardMy);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
            bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
        });

        it('When on Manage Trip Page I click on Ryanair Home Page logo to return to home page', function () {
            actions.bookingSummaryActions.clickRyanairLogo();
        });

        it("Then I retrieve booking from manage trips section", function () {
            actions.manageTripsActions.manageTripAccessCardFromBookingRef(bookingRefActiveTrip, lastFourCreditCardDigits);
        });

        it("Then I make sure that add seat button is not present", function () {
            actions.bookingSummaryActions.assertOnAddSeatButtonNotPresent();
        });

        it('Then I select bags on active trips page', function () {
            actions.extrasActions.addBag();
            actions.bagsActions.addOneNormalOneLargeBag();
        });

        it('Then I assert on bags total and sub total', function () {
            actions.bagsActions.assertOnBagsSubTotalAndTotal();
        });

        it('Then I assert on bags on pricebreakdown on active trips page', function () {
            actions.bagsActions.assertBagsOnPriceBreakDown();
        });

        it('Then I click continue on active trip page', function () {
            actions.bookingSummaryActions.clickTopContinueBtn();
        });

        it('Then I should pay for bags', function () {
            actions.addPaxActions.makeCardPaymentActive(cardMy);
        });

        it('Then I make sure that bags are added', function () {
            actions.bookingSummaryActions.assertOnAddedBags();
        });
    });
});

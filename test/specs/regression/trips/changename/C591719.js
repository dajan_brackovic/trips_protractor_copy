var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;


function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
        var outBoundDaysFromNow = 2; //today index === 1
        var returnDaysFromNow = 5; //today index === 1
        var origin = "DUB";
        var destination = "Krk";
        var fareType = "standard";
        var tripWay = "twoway";
        var bookingRefActiveTrip;
        var trip;
        var changeFirstName = "NewFirstName";
        var changeLastName = "NewLastName";
        var totalPax = adultNumber + teenNumber + childrenNumber + infantsNumber;

        var bookFlight = function (paxMap) {
            trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
            actions.extrasActions.skipExtras();
            actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
            actions.addPaxActions.addContact();
            actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        };

        describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

            it(sprintf('Given I make a standard return trip with outbound < 7 Days and return < 7 days from now with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
                var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
                bookFlight(paxMap);
            });

            it('Then I should get a booking ref', function () {
                actions.bookingSummaryActions.verifyConfirmationMessage();
                bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
            });

            it('When I click nmanage your trip button', function () {
                actions.bookingSummaryActions.clickManageYourTripButton();
            });

            it('Then I click on change name link', function () {
                actions.manageYourTripActions.clickChangeNameLink();
            });

            it('Then I select change name checkbox', function () {
                actions.manageYourTripActions.clickSelectNameToChangeCheckBox();
            });

            it('Then I change name', function () {
                actions.manageYourTripActions.selectChangeNameTitleDropdown(1); // 1 is "Mr" "Title index"
                actions.manageYourTripActions.enterChangeNameFirstNameField(changeFirstName);
                actions.manageYourTripActions.enterChangeNameLastNameField(changeLastName);
            });

            it('Then I assert on price for change name', function () {
                actions.manageYourTripActions.changeNameIsCharedCorrectly();
            });

            it('Then I click continue on active trip page', function () {
                actions.bookingSummaryActions.clickTopContinueBtn();
            });

            it('Then I should pay for seat', function () {
                actions.addPaxActions.makeCardPaymentActive(trip.bookingContact.card);
            });

            it('When I click checkIn button', function () {
                actions.bookingSummaryActions.clickCheckInButton();
            });

            it('Then I fill Check In id documents', function () {
                actions.checkInActions.selectNationalityDropDown();
                actions.checkInActions.enterDateOfBirth(60);
                actions.checkInActions.enterDocumentType(1);
                actions.checkInActions.enterDocumentNumberField();
                actions.checkInActions.enterCountryOfIssueDropDown();
                actions.checkInActions.enterExpiryDate();
            });

            it('Then I click continue after adding id documents on check-in', function () {
                actions.checkInActions.clickBtnContinueCheckIn();
            });

            it('Then I complete second step of check-in', function () {
                actions.checkInActions.clickBtnContinueCheckIn();
            });

            it('Then I get Boarding Pass Ref', function () {
                actions.checkInActions.assertOnBookingRef(bookingRefActiveTrip);
            });

            it('Then I get auto allocated seat', function () {
                actions.checkInActions.assertOnAutoAllocation(tripWay, totalPax);
            });

            it('Then I assert on changed name at checkIn', function () {
                actions.checkInActions.assertOnChangedName(changeFirstName,changeLastName);
            });

        });

}

describe('TRIPS | Change Name | C591719 | outbound < 7 Days | Return < 7 Days | Standard | Change Name | Check In', function () {

    sharedDescribe(1, 0, 0, 0);

});
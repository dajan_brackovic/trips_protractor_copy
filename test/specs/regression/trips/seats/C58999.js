var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(numAdult, numTeen, numChild, numInfants) {
    var outBoundDaysFromNow = 4; //today index === 1
    var origin = "Dub";
    var destination = "edi";
    var fareType = "standard";
    var tripWay = "oneway";
    var paxListMy;
    var cardMy;

    var bookFlight = function (paxMap) {
        var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
        actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        paxListMy = trip.journey.paxList;
        cardMy = trip.bookingContact.card;
    };

    describe('1 adult, 0 teen, 0 children, 0 infants', function () {

        it(sprintf('Given I make a standard oneway trip with outbound 2 days from now with %s adult, %s teen, %s children, %s infants', numAdult, numTeen, numChild, numInfants), function () {
            var paxMap = {ADT: numAdult, CHD: numChild, INF: numInfants, TEEN: numTeen};
            bookFlight(paxMap);
        });

        it('When I select a seat', function () {
            actions.extrasActions.addSeat();
            actions.seatsActions.selectMultipleSeats(tripWay, numAdult, numTeen, numChild, numInfants);

        });

        it('Then I should confirm a seat price', function () {
            actions.seatsActions.confirmSeat();
        });

        it('Then I continue on extras page', function () {
            actions.extrasActions.skipExtras();
        });

        it('Then I should pay for booking', function () {
            actions.addPaxActions.addPaxNameForAllPAX(paxListMy);
            actions.addPaxActions.addContact();
            actions.addPaxActions.makeCardPayment(cardMy);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
        });
    });
}

describe('TRIPS | Multiple Seats | C58999 | OneWay Flight | MultiPax | Booking Ref', function () {

    sharedDescribe(2, 1, 1, 1);

    //TODO Too many tests filling up planes and causing tests to fail
    //sharedDescribe(2, 0, 0, 2);
});


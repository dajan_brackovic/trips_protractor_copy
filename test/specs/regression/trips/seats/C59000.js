var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(numAdult, numTeen, numChild, numInfants) {
    var outBoundDaysFromNow = 4; //today index === 1
    var returnDaysFromNow = 8; //today index === 1
    var origin = "Dub";
    var destination = "krk";
    var fareType = "standard";
    var tripWay = "twoway";
    var paxListMy;
    var cardMy;

    var bookFlight = function (paxMap) {
        var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        paxListMy = trip.journey.paxList;
        cardMy = trip.bookingContact.card;
    };

    describe(sprintf('%s adult, %s teen, %s children, %s infants', numAdult, numTeen, numChild, numInfants), function () {

        it(sprintf('Given I make a standard return trip with outbound 4 days and return 8 days from now with %s adult, %s teen, %s children, %s infants', numAdult, numTeen, numChild, numInfants), function () {
            var paxMap = {ADT: numAdult, CHD: numChild, INF: numInfants, TEEN: numTeen};
            bookFlight(paxMap);
        });

        it('When I select a seat', function () {
            actions.extrasActions.addSeat();
            actions.seatsActions.selectMultipleSeats(tripWay, numAdult, numTeen, numChild, numInfants);
        });

        it('Then I select a same seat for return', function () {
            actions.seatsActions.selectMultiSameSeatReturn(numAdult, numTeen, numChild, numInfants)
        });

        it('Then I should confirm a seat price', function () {
            actions.seatsActions.confirmSeat();
        });

        it('Then I continue on extras page', function () {
            actions.extrasActions.skipExtras();
        });

        it('Then I should pay for booking', function () {
            actions.addPaxActions.addPaxNameForAllPAX(paxListMy);
            actions.addPaxActions.addContact();
            actions.addPaxActions.makeCardPayment(cardMy);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
        });
    });
}

describe('TRIPS | Multiple Seats | Same Seats Return | C59000 | TwoWay Flight | MultiPax | Booking Ref', function () {

    sharedDescribe(2, 1, 0, 1);

    //TODO Too many tests filling up planes and causing tests to fail
    //sharedDescribe(2, 0, 0, 1);
    //sharedDescribe(2, 0, 0, 2);

});

var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;


function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber, origin) {
    var outBoundDaysFromNow = 6; //today index === 1
    var fareType = "standard";
    var tripWay = "oneway";
    var destination = actions.tripHelper.getRandomAirportFromArray();
    var cardMy;
    var bookingRefActiveTrip;

    var bookFlight = function (paxMap) {
        var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
        actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        actions.extrasActions.skipExtras();
        actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
        cardMy = trip.bookingContact.card;
    };

    describe(sprintf('from %s to %s with %s adult, %s teen, %s children, %s infants', origin, destination, adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

        it(sprintf('Given I create a OneWay flight from %s to %s with %s adult, %s teen, %s children, %s infants', origin, destination, adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
            var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
            bookFlight(paxMap, origin, destination);
        });

        it('And I add Special Assistance, Travel with Guide Dog, at Pax Details Screen', function () {
            actions.addPaxActions.clickLabelSpecialAssistance();
            actions.addPaxActions.clickViewMoreInfoOpen();
            actions.addPaxActions.clickViewMoreInfoClose();
            actions.addPaxActions.clickSpecialAssistancePaxCheckBox();
            actions.addPaxActions.countAndSelectListOfOptionsInSpecialAssistanceDropDown(tripWay);
        });

        it('And I select Require an Airport wheelchair', function () {
            actions.addPaxActions.clickSpecialAssistanceWheelChairQuestionYes();
        });

        it('And I pay for booking', function () {
            actions.addPaxActions.addContact();
            actions.addPaxActions.makeCardPayment(cardMy);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
            bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
        });

        it('When I click checkIn button', function () {
            actions.bookingSummaryActions.clickCheckInButton();
        });

        it('Then I fill Check In id documents', function () {
            actions.checkInActions.selectNationalityDropDown();
            actions.checkInActions.enterDateOfBirth(30);
            actions.checkInActions.enterDocumentType(1);
            actions.checkInActions.enterDocumentNumberField();
            actions.checkInActions.enterCountryOfIssueDropDown();
            actions.checkInActions.enterExpiryDate();
        });

        it('Then I click continue after adding id documents on check-in', function () {
            actions.checkInActions.clickBtnContinueCheckIn();
        });

        it('Then I complete second step of check-in', function () {
            actions.checkInActions.clickBtnContinueCheckIn();
        });

        it('Then I get Boarding Pass Ref', function () {
            actions.checkInActions.assertOnBookingRef(bookingRefActiveTrip);
        });

        it('Then I verify that Special Assistance requirements appears on Boarding Pass', function () {
            actions.checkInActions.verifySpecialAssistanceOnBP();
        });
    });

}

describe('TRIPS | Special Assistance | C49929 | OB < 7 Days | Standard', function () {
    sharedDescribe(1, 0, 0, 0, "Stn");

});
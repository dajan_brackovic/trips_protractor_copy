var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {

    var outBoundDaysFromNow = 14; //today index === 1
    var origin = "dub";
    var destination = "brs";
    var fareType = "standard";
    var tripWay = "oneway";

    var bookFlight = function (paxMap) {
        var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
        actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        actions.extrasActions.skipExtras();
        actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
        actions.addPaxActions.addContact();
    };

    describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

        it(sprintf('Given I make a standard oneWay trip with 7 < outbound < 30days from now with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
            var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
            bookFlight(paxMap);
        });

        it('Then I should choose to pay with paypal', function () {
            actions.addPaxActions.makePayPalPayment();
            browser.ignoreSynchronization = true;
        });

        it('Then I should fill valid paypal details', function () {
            //actions.payPalActions.clickPayWithMyPayPalAccount();
            actions.payPalActions.enterEmailAndPassword();
            actions.payPalActions.clickBtnContinue();
        });

        it('Then I should ignore synchronization to false', function () {
            browser.ignoreSynchronization = false;
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
        });

    });


};

describe('TRIPS | C221228 | Simple Booking | One Way | Pay by PayPal', function () {

    sharedDescribe(1, 0, 0, 0);
});
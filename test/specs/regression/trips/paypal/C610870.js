var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {

    var outBoundDaysFromNow = 9; //today index === 1
    var origin = "dub";
    var destination = "brs";
    var fareType = "standard";
    var tripWay = "oneway";
    var trip;

    var bookFlight = function (paxMap) {
        trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
        actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
        actions.tripsHomeActions.selectAFlightOnly(0, 0, fareType, tripWay);
    };

    describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

        it(sprintf('Given I make a standard oneWay trip with 7 < outbound < 30days from now with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
            var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
            bookFlight(paxMap);
        });

        it('Then I should choose to pay with paypal on flight select page', function () {
            actions.priceBreakDownActions.clickOnOpenPriceBreakDown();
            actions.priceBreakDownActions.clickCheckBoxPayPalOnPriceBreakDown();
            actions.priceBreakDownActions.clickOnOpenPriceBreakDown(); // close price breakdown dropdown
            actions.tripsHomeActions.clickBtnHomeBottomContinue();
        });

        it('Then I continue on extras page', function () {
            actions.extrasActions.skipExtras();
        });

        it('Then I should add pax name and contact details', function () {
            actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
            actions.addPaxActions.addContact();
        });

        it('Then I should accept terms and conditions and click continue', function () {
            actions.addPaxActions.clickPaymentContinue();
            browser.ignoreSynchronization = true;
        });

        it('Then I should fill valid paypal details', function () {
            //actions.payPalActions.clickPayWithMyPayPalAccount();
            actions.payPalActions.enterEmailAndPassword();
            actions.payPalActions.clickBtnContinue();
        });

        it('Then I should ignore synchronization to false', function () {
            browser.ignoreSynchronization = false;
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
        });

    });


};

describe('TRIPS | PayPal | C610870 | Select Payment Method PayPal at Flight Select Page', function () {

    sharedDescribe(1, 0, 0, 0);
});
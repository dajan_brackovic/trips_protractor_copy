describe("TRIPS | Parking Active | C139630 | Ensure that input checking vehicle registration number with alphanumeric (hyphen, spaces)", function () {

    var params = browser.params.conf;
    var Trip = require('../../../../../shared/model/Trip');

    describe("Case:C139630 | 1 Adults, 0 Teens, 0 Children, 0 Infants | DUB-CIA oneWay", function () {

        it("Given that user is on active trip page", function () {
            actions.manageTripsActions.manageTripAccess(params.payment.refNumberPA5, params.payment.payMail2);
        });
        it("And has not added parking", function () {
            actions.potentialTripActions.expectParkingCardIsNotDisabled();
        });
        it("When user click on parking product card", function () {
            actions.potentialTripActions.clickAddParking();
        });
        it("And user should input vehicle registration number", function () {
            actions.lowCostParkingActions.enterRegistrationNumber(params.parking.registrationNumber1);
        });
        it("Then user should check is there vehicle registration number alphanumeric(hyphen, spaces)", function () {
            actions.lowCostParkingActions.informationMessageRegistrationNumber(params.parking.registrationNumber3, params.parking.messageRegistartionNum0, params.parking.messageRegistartionNum1);
        });

    });
});
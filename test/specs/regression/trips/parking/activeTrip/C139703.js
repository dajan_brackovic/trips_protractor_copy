describe("TRIPS | Parking Active | C139703 | Ensure the total price updates when selecting the different option", function () {

    var params = browser.params.conf;
    var Trip = require('../../../../../shared/model/Trip');

    describe("Case:C139703 | 1 Adults, 0 Teens, 0 Children, 0 Infants | DUB-CIA oneWay", function () {

        it("Given that user is on active trip page", function () {
            actions.manageTripsActions.manageTripAccess(params.payment.refNumberPA5, params.payment.payMail2);
        });
        it("And has not added parking", function () {
            actions.potentialTripActions.checkAddToTripButtonCheckParking();
        });
        it("When user click on parking product card", function () {
            actions.potentialTripActions.clickAddParking();
        });
        it("And that the user can see default total price", function () {
            actions.lowCostParkingActions.expectTotalPriceToBe("21.50");
        });
        it("And that the user has changed the parking option", function () {
            browser.sleep(2000);
            actions.lowCostParkingActions.changeParkingProviderDown();
        });
        it("Then update the total price with the price of that parking option", function () {
            actions.lowCostParkingActions.expectTotalPriceToBe("25.00");
        });

    });
});
describe("TRIPS | Parking | C139628 | Ensure that the user can select a date from the calendar on parking form - Active Trip", function () {

    var params = browser.params.conf;
    var Trip = require('../../../../../shared/model/Trip');

    describe("Case C139628: 1 Adults, 0 Teens, 0 Children, 0 Infants", function () {

        it("Given that user is on active trip page", function () {
            actions.manageTripsActions.manageTripAccess(params.payment.refNumberPA6, params.payment.payMail2);
        });
        it("And has not added parking", function () {
            actions.potentialTripActions.expectParkingCardIsNotDisabled();
        });
        it("When user click on parking product card", function () {
            actions.bookingSummaryActions.clickAddParking();
        });
        it("Then user should check is there display calendar field", function () {
            actions.lowCostParkingActions.expectDatePickerValidation();
        });
        it("Then user should check is there allow the user to select a return date", function () {
            actions.lowCostParkingActions.selectDate(3);
        });
    });
});
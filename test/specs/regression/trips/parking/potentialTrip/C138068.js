var Trip = require('../../../../../shared/model/Trip');

describe("TRIPS | Potential Trip Parking | C138068 | Ensure that information message is displayed when return flight is more than 28 days later than outbound", function () {
    var outBoundDaysFromNow = 2;
    var returnDaysFromNow = 32;
    var origin = "Ltn";
    var destination = "Dub";
    var fareType = "standard";
    var tripWay = "twoway";

    describe("Case C138068 : 1 adult, 0 teen, 0 children, 0 infants ", function(){
        it("Given that user has booked a return trip with return date more than 28 days later than outbound", function () {
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        });
        it("When user is on parking side drawer", function(){
            actions.potentialTripActions.clickAddParking();
        });
        it("Then display information message - You still can book parking for up to 28 days", function(){
            actions.lowCostParkingActions.expectParking28DaysMessage();
        });
    });

});


var Trip = require('../../../../../shared/model/Trip');

describe("TRIPS | Parking Potential Trip | C36152 | Ensure the total price updates when selecting the different option - oneWay", function () {
    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE");
    var origin = "Stn";
    var destination = "Dub";
    var fareType = "standard";
    var tripWay = "oneway";
    var firstState;

    describe("Case C36152: 1 adult, 0 teen, 0 children, 0 infants", function () {
        it("Given that the user can see default total price", function () {
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
            actions.potentialTripActions.clickAddParking();
        });
        it("Given that the user has changed the parking option", function () {
            actions.lowCostParkingActions.expectTotalPriceValidation();
            actions.lowCostParkingActions.changeParkingProviderDown();
        });
        it("Then update the total price with the price of that parking option", function () {
            firstState = actions.lowCostParkingActions.returnfirstStateParkingPrice();
            actions.lowCostParkingActions.clickOptionStayParkingProvider(1);
            actions.lowCostParkingActions.assertOnUpdateParkingPrice(firstState);
        });
    });
});
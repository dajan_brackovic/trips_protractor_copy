var Trip = require('../../../../../shared/model/Trip');

describe("TRIPS | Parking Potential Trip | C36154 | Ensure that the total price is updated when one way flight dates have been changed", function () {
    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE");
    var origin = "Dub";
    var destination = "Cia";
    var fareType = "standard";
    var tripWay = "oneway";

    describe("Case C36154: 1 adult, 0 teen, 0 children, 0 infants", function () {
        it("Given that the user has booked a one way flight", function () {
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
            actions.potentialTripActions.clickAddParking();
        });
        it("When the user selects a date", function () {
            actions.lowCostParkingActions.selectDate(0);
            actions.lowCostParkingActions.expectTotalPriceValidation();
        });
        it("Then update the total price with the price for parking for the selected time period (from the date of the outbound flight to the selected date)", function () {
            actions.lowCostParkingActions.expectParkingDateTotalPriceIsChanged(1);
        });
    });

});
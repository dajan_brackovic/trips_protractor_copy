var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;


function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE"); //today index === 1
    var origin = "Dub";
    var destination = "Bcn";
    var fareType = "standard";
    var tripWay = "oneway";
    var bookingRefActiveTrip;
    var selectedSeat;
    var trip;
    var text = "Keep everyone sitting together by choosing your seats";

    var bookFlight = function (paxMap) {
        trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
        actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType,tripWay);
        actions.extrasActions.skipExtras();
        actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
        actions.addPaxActions.addContact();
        actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
        actions.addPaxActions.enterBillingAddress();
        actions.addPaxActions.clickPaymentContinue();
    };

    describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

        it(sprintf('Given I make a standard oneway trip with outbound < 7 Days from now with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
            var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
            bookFlight(paxMap);
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
            bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
        });

        it('When I click checkIn button', function () {
            actions.bookingSummaryActions.clickCheckInButton();
        });

        it('Then I fill Check In id documents', function () {
            actions.checkInActions.selectAllPax();
            actions.checkInActions.addDocsForMultiPax(adultNumber, teenNumber, childrenNumber, infantsNumber, 3);
        });

        it('Then I assert on seat messaging at check-in', function () {
            actions.checkInActions.selectCheckboxSeat(tripWay);
            actions.checkInActions.assertOnSeatMessaging(text);
        });
    });
}

describe('TRIPS | Seats messaging  | C698539 | RW-1582 | more than 2 ADT ', function () {

    sharedDescribe(3, 1, 0, 0);

});
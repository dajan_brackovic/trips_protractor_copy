var Pages = require('../../../../Pages');
var pages = new Pages();
var Trip = require('../../../../shared/model/Trip');


describe('TRIPS | Voucher | C35666 | Redeem a Voucher', function () {
    var outBoundDaysFromNow = 3;//today index === 1
    var returnDaysFromNow = 5; //today index === 1
    var origin = "dub";
    var destination = "stn";
    var fareType = "standard";
    var tripWay = "twoway";
    var cardMy;
    var voucherNumber = "211773052663700001";


    var bookFlight = function (paxMap) {
        var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        actions.extrasActions.skipExtras();
        trip.journey.paxList[0].title = 'Mr';
        trip.journey.paxList[0].firstName = 'TestFirst';
        trip.journey.paxList[0].lastName = 'TestLast';
        actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
        cardMy = trip.bookingContact.card;
    };


    describe('Given I make booking from A to B', function () {

        it('When I reach to Addpax page', function () {
            var paxMap = {ADT: 1, CHD: 0, INF: 0, TEEN: 0}
            bookFlight(paxMap);
        });

        it('Then I click continue button of passenger details', function () {
            //actions.addPaxActions.clickBtnAddPaxSave();
        });

        it('When I  enter a valid voucher number Voucher is accepted', function () {
            actions.voucherActions.clickRedeemVoucher();
            actions.voucherActions.enterVoucherNumber(voucherNumber);
            actions.voucherActions.clickRedeemBtn();
            actions.voucherActions.assertOnValidVoucher("25.00");
        });

        it('When I  enter an invalid voucher number Voucher is not accepted', function () {
            actions.voucherActions.clickRedeemVoucher();
            actions.voucherActions.enterVoucherNumber("21146021654140");
            expect(pages.vouchersPage.invalidVoucherError().isDisplayed());
        });
    });
});





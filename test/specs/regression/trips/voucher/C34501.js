var Trip = require('../../../../shared/model/Trip');


describe('TRIPS | Voucher | C34501 | Create a Voucher', function () {

    it('Given I navigate to voucher page', function () {
        actions.voucherActions.goToPage();
    });

    it('When I select one of the themes', function () {
        actions.voucherActions.selectTheme(2);
    });

    it('Then I should count the no of voucher amount and select each of them', function() {
        element.all(by.repeater('amount in selectedBase.amounts')).count().then(function (originalCount) {
            for (var i = 1; i <= originalCount; i++) {
                actions.voucherActions.selectVchAmount(i);
            }
        });
    });

    it('Then I should count the number of currency present in dropdown', function(){
        actions.voucherActions.countCurrenyOptions();
    });

    it('Then I Choose an amount and currency', function () {
        actions.voucherActions.selectVchAmount(3);
        actions.voucherActions.selectVchCurrency(1);
    });

    it('Then I include a special message', function () {
        actions.voucherActions.enterMessageTextBox("Test message");
    });

    it('Then I click Continue Button', function () {
        actions.voucherActions.clickBtnContinue();
        actions.voucherActions.assertOnFirstNameField();
    });

    it('Then I should enter From Delivery Details', function () {
        actions.voucherActions.enterYourName("Automation User");
        actions.voucherActions.enterYourEmail("auto@ryanair.ie");
        actions.voucherActions.selectOfferLabel();
        actions.voucherActions.selectCountryCode(93);
        actions.voucherActions.enterPhoneNumber("123456789");
    });

    it('Then I Should enter To Delivery Details', function () {
        actions.voucherActions.enterFirstName("TestFirst");
        actions.voucherActions.enterLastName("TestLast");
        actions.voucherActions.enterEmailVoucher("TestEmail@ryanair.ie");
    });

    it('When I Should enter Payment Details', function () {
        var card = new Trip( {ADT: 1, TEEN: 0, CHD: 0, INF: 0});
        actions.addPaxActions.makeCardPaymentVoucher(card.bookingContact.card);
        actions.addPaxActions.enterBillingAddress();
        actions.addPaxActions.clickPaymentContinue();
        actions.voucherActions.assertOnConfirmationText();

    });

    it('Then I Should get order summary', function () {
        actions.voucherActions.confirmationTextValue();
        actions.voucherActions.giftFromValue();
        actions.voucherActions.giftToValue();
        actions.voucherActions.sendToValue();
        actions.voucherActions.referenceValue();
    });

    it('Then I Should get Purchased voucher Details', function () {
        actions.voucherActions.giftVoucherValue();
        actions.voucherActions.giftVoucherHandlingFeeValue();
        actions.voucherActions.totalPaid1Value();
    });
});


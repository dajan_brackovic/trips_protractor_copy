var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;
var params = browser.params.conf;


function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
        var outBoundDaysFromNow = 3; //today index === 1
        var returnDaysFromNow = 6; //today index === 1
        var origin = "Dub";
        var destination = actions.tripHelper.getRandomAirportFromArray();
        var fareType = "standard";
        var tripWay = "twoway";
        var paxListMy;
        var cardMy;

        var bookFlight = function (paxMap) {
            var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
            paxListMy = trip.journey.paxList;
            cardMy = trip.bookingContact.card;
        };

        describe('1 adult, 0 teen, 0 children, 0 infants', function () {

            it('Given I make a standard return trip with outbound 2 days and return 4 days from now with 1 adult, 0 teen, 0 children, 0 infants', function () {
                var paxMap = {ADT: 1, CHD: 0, INF: 0, TEEN: 0};
                bookFlight(paxMap);
            });

            it('When I am on potential trips page I add seats', function () {
                actions.extrasActions.addSeat();
                actions.seatsActions.selectOneSeat(tripWay);
                actions.seatsActions.selectDiffSeatReturn();
                actions.seatsActions.confirmSeat();
            });

            it('Then I add bags', function () {
                actions.extrasActions.addBag();
                actions.bagsActions.addOneNormalOneLargeBag();
                actions.bagsActions.assertOnBagsSubTotalAndTotal();
                actions.bagsActions.clickBagsBtnConfirm();
            });

            it('Then I add parking', function () {
                actions.potentialTripActions.clickAddParking();
                browser.sleep(2000);
                actions.lowCostParkingActions.enterRegistrationNumber(params.parking.registrationNumber1);
                actions.lowCostParkingActions.clickConfirmButton();
            });

            it('Then I add baby equipment', function () {
                actions.extrasActions.addBaby();
                actions.babyEquipmentActions.selectPlusButtonSameForBothFlights(0);
                actions.babyEquipmentActions.expectTotalPrice("20.00");
                actions.babyEquipmentActions.clickConfirmButton();
            });

            it('Then I add add music equipment', function () {
                actions.potentialTripActions.clickAddMusicEquipment();
                actions.musicEquipmentActions.selectPlusButtonSameForBothFlights(0);
                actions.musicEquipmentActions.expectTotalPrice("100.00");
                actions.musicEquipmentActions.clickConfirmButton();
            });

            it('Then I add sports equipment', function () {
                actions.potentialTripActions.clickAddSportEquipment();
                actions.sportEquipmentActions.selectSportEquipmentTypeClick(0,0);
                actions.sportEquipmentActions.selectSportEquipmentType(0,0,0);
                browser.sleep(2000);
                actions.sportEquipmentActions.selectPlusButtonSameForBothFlights(0);
                actions.sportEquipmentActions.expectTotalPrice("120.00");
                actions.sportEquipmentActions.clickConfirmButton();
            });

            it('Then I continue on extras page', function () {
                actions.extrasActions.skipExtras();
            });

            it('Then I should verify pricebreakdown', function () {
                actions.priceBreakDownActions.assertOnListOfSelectedItems("2 x baby equipment");
                actions.priceBreakDownActions.assertOnListOfSelectedItems("2 x bike equipment");
                actions.priceBreakDownActions.assertOnListOfSelectedItems("2 x music equipment");
                actions.priceBreakDownActions.assertOnParking();
            });

            it('Then I should pay for booking', function () {
                actions.addPaxActions.addPaxNameForAllPAX(paxListMy);
                actions.addPaxActions.addContact();
                actions.addPaxActions.makeCardPayment(cardMy);
                actions.addPaxActions.enterBillingAddress();
                actions.addPaxActions.clickPaymentContinue();
            });

            it('Then I should get a booking ref', function () {
                actions.bookingSummaryActions.verifyConfirmationMessage();
            });

            it('Then I should assert on reservation number on itinerary', function () {
                actions.itineraryActions.assertOnReservationNumber();
            });

            it('Then I should assert on all the extras present on itinerary', function () {
                actions.itineraryActions.assertOnAddedExtrasItemsList("2 x Assigned seats");
                actions.itineraryActions.assertOnAddedExtrasItemsList("2 x 15Kg bag");
                actions.itineraryActions.assertOnAddedExtrasItemsList("2 x Large bag");
                actions.itineraryActions.assertOnAddedExtrasItemsList("2 x baby equipment");
                actions.itineraryActions.assertOnAddedExtrasItemsList("2 x Bike");
                actions.itineraryActions.assertOnAddedExtrasItemsList("2 x Music equipment");
                actions.itineraryActions.assertOnAddedExtrasItemsList("1 x Airport parking");
            });
        });
}

describe('TRIPS | Itinerary | C591707 | TwoWay | Bags | Seats | Music | Sports | Baby | Parking | Assert on added items', function () {

    sharedDescribe(1, 0, 0, 0);

});
var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;


function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
        var outBoundDaysFromNow = 2;//today index === 1
        var returnDaysFromNow = 6; //today index === 1
        var origin = "Edi";
        var destination = "Stn";
        var fareType = "standard";
        var tripWay = "twoway";
        var myPaxlist;
        var myCard;

        var bookFlight = function (paxMap) {
            var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            myPaxlist = trip.journey.paxList;
            myCard = trip.bookingContact.card;
        };

        describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

            it(sprintf('Given I make a standard return trip with outbound = 5 Days and return = 6 days from now with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
                var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
                bookFlight(paxMap);
            });

            it('When I am on flight select page I should click on modify trip', function () {
                actions.tripsHomeActions.clickBtnModifyTrip();
            });

            it('Then I edit trip from twoway to oneway', function () {
                actions.tripsHomeActions.modifyFromTwoWayToOneWay();
                tripWay = "oneway";
                actions.fOHActions.chooseDatesOneWayModifyTrip(outBoundDaysFromNow, origin, destination);
            });

            it('Then I click on lets go button', function () {
                actions.fOHActions.clickLetsGoBtn();
            });

            it('Then I assert that option to select return flight is no longer available', function () {
                actions.tripsHomeActions.assertOnModifyFromTwoWayToOneWay();
            });

            it('Then I should carry on  and pay for the booking', function () {
                actions.tripsHomeActions.selectAFlight(0, 0, fareType,tripWay);
                actions.extrasActions.skipExtras();
                actions.addPaxActions.addPaxNameForAllPAX(myPaxlist);
                actions.addPaxActions.addContact();
                actions.addPaxActions.makeCardPayment(myCard);
                actions.addPaxActions.enterBillingAddress();
                actions.addPaxActions.clickPaymentContinue();
            });

            it('Then I should get a booking ref', function () {
                actions.bookingSummaryActions.verifyConfirmationMessage();
            });

        });

}

describe('TRIPS | Modify Trip | C138078  | Change from TwoWay to OneWay | Standard', function () {

    sharedDescribe(1, 0, 0, 0);

});

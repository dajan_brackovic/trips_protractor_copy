describe("TRIPS | Active Trip Sport Equipment | C143782 | Ensure that purchased sport equipment is showed on side drawer", function () {

    var params = browser.params.conf;
    var Trip = require('../../../../../shared/model/Trip');

    describe("Case:C143782 | 1 Adults, 0 Teens, 0 Children, 0 Infants | DUB-CIA oneWay", function () {

        it("Given that user have booked flight and it's on active trip page", function () {
            actions.manageTripsActions.manageTripAccess(params.payment.refNumberPA7, params.payment.payMail2);
        });
        it("Then sport equipment side drawer is opened", function () {
            actions.bookingSummaryActions.clickAddSportEquipment();
        });
        it("Then purchased sport equipment is showed on side drawer", function () {
            actions.sportEquipmentActions.expectSportActiveEquipmentPurchased("large_sports_item","2 x large sports item (flight back)", "2 x large sports item (flight out)");
        });
    });
});
describe("TRIPS | Active Trip Sport Equipment | C59699 | Ensure that check box 'Same for both flights' are checked by default", function () {

    var params = browser.params.conf;
    var Trip = require('../../../../../shared/model/Trip');

    describe("Case:C59699 | 1 Adults, 0 Teens, 0 Children, 0 Infants | DUB-CIA oneWay", function () {

        it("Given that user have booked flight and it's on active trip page", function () {
            actions.manageTripsActions.manageTripAccess(params.payment.refNumber7,params.payment.payMail);
        });
        it("Then sport equipment side drawer should be opened", function () {
            actions.bookingSummaryActions.clickAddSportEquipment();
        });
        it("Then checkbox same for both flights should be checked by default", function () {
            actions.sportEquipmentActions.expectCheckBoxSFBFisCheckedHigh(0,1);
        });
    });
});
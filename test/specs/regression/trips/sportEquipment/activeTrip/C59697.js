describe("TRIPS | Active Trip Sport Equipment | C59697 | Ensure that total price is updated on sport side drawer", function () {

    var params = browser.params.conf;
    var Trip = require('../../../../../shared/model/Trip');

    describe("Case:C59697 | 1 Adults, 0 Teens, 0 Children, 0 Infants | DUB-CIA oneWay", function () {

        it("Given that user have booked flight and it's on active trip page", function () {
            actions.manageTripsActions.manageTripAccess(params.payment.refNumber7,params.payment.payMail);
        });
        it("Then sport equipment side drawer should be opened", function () {
            actions.bookingSummaryActions.clickAddSportEquipment();
        });
        it("When sport equipment type is selected", function () {
            actions.sportEquipmentActions.selectSportEquipmentTypeClick(0,0);
            actions.sportEquipmentActions.selectSportEquipmentType(0,0,2);
        });
        it("When add sport equipment for passanger", function () {
            actions.sportEquipmentActions.expectTotalPrice("0.00");
            browser.sleep(2000);
            actions.sportEquipmentActions.selectPlusButtonSameForBothFlights(0);

        });
        it("Then total price should be updated", function () {
            actions.sportEquipmentActions.expectTotalPrice("60.00");
        });
    });
});
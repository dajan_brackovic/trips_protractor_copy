describe("Case: C60003", function () {

    var Trip = require('../../../../../shared/model/Trip');

    describe("DT-1107	Case: C60003 - Ensure that correct price is displayed for Golf Equipment - Return Trip - ", function () {

        var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};

        it("Given that user has booked return flight ", function () {
            actions.bookingActions.bookReturnFlight("LTN", "DUB", "standard", 3, 9, paxMap);
            trip = new Trip(paxMap, "LTN", "DUB", 3);
        });
        it("And has opened Sport Equipment side drawer", function () {
            actions.potentialTripActions.clickAddSportEquipment();
            browser.sleep(2000);
        });
        it("When user select Golf Equipment from drop down menu for sport equipment types", function () {
            actions.sportEquipmentActions.selectSportEquipmentTypeClick(0,0);
        });
        it("Then display price of 60.00 GBP", function () {
            actions.sportEquipmentActions.expectSportEquipmentTypeSinglePrice(2, '60.00')
        });

    });
});
describe("TRIPS | Potential Trip Sport Equipment | C41711 | Ensure that when user change equipment and click on CTA update product card | Return Trip ", function () {

    var params = browser.params.conf;
    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE");
    var returnDaysFromNow = actions.tripHelper.getRandomDate("PARTIAL");
    var origin = "DUB";
    var destination = "KRK";
    var fareType = "standard";
    var tripWay = "twoway";

    describe("Case:C41711 | 1 Adults, 0 Teens, 0 Children, 0 Infants | Return DUB-KRK", function () {

        it("Given that the user has made sport equipment selections and clicked the CTA", function () {
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
            actions.potentialTripActions.clickAddSportEquipment();
            browser.sleep(2000);
            actions.sportEquipmentActions.selectSportEquipmentTypeClick(0,0);
            actions.sportEquipmentActions.selectSportEquipmentType(0,0,0);
            actions.sportEquipmentActions.selectPlusButtonSameForBothFlights(0);
            actions.sportEquipmentActions.expectSportEquipmentAmount1(0,"1");
            actions.sportEquipmentActions.clickConfirmButton();
            actions.potentialTripActions.openPriceBreakDownClick();
            actions.potentialTripActions.expectProductCardUpdated(0,3,"120.00","Sports equipment");
            actions.potentialTripActions.openPriceBreakDownClick();
            actions.potentialTripActions.clickAddSportEquipment();
            actions.sportEquipmentActions.expectSportEquipmentAmount1(0,"1");
            actions.sportEquipmentActions.selectPlusButtonSameForBothFlights(0);
            actions.sportEquipmentActions.expectSportEquipmentAmount1(0,"2");
            actions.sportEquipmentActions.clickConfirmButton();
        });
        it("Then shopping basket is updated with the equipment prices", function () {
            actions.potentialTripActions.openPriceBreakDownClick();
            actions.potentialTripActions.expectProductCardUpdated(0,3,"240.00","Sports equipment");
        });
        it("Then information message appears on the '2nd line Header - ( Product has been added - anything else you need - Copy may not be correct)", function () {
            actions.potentialTripActions.expectInformationMessageAddedPriceShown();
            actions.potentialTripActions.expectInformationMessageAddedPriceMessageText(params.trip.messageAfterSport);
        });
        it("Then product card has been update - with product that has been added", function () {
            actions.potentialTripActions.expectInformationLabelOnCard("4 sports equipment");
            actions.potentialTripActions.modifyButtonCheckSport();
        });

    });

});
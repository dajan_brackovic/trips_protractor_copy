describe("TRIPS | Potential Trip Sport Equipment | C41708 | Ensure that when user modify his selection 'Increased', than enable CTA | Return Trip ", function () {

    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE");
    var returnDaysFromNow = actions.tripHelper.getRandomDate("PARTIAL");
    var origin = "LTN";
    var destination = "DUB";
    var fareType = "standard";
    var tripWay = "twoway";

    describe("Case:C41708 | 1 Adults, 0 Teens, 0 Children, 0 Infants | Return DUB-CIA", function () {

        it("Given that the user has made sport equipment selections and clicked the CTA", function () {
            var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
            actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
            actions.potentialTripActions.clickAddSportEquipment();
            browser.sleep(2000);
            actions.sportEquipmentActions.selectSportEquipmentTypeClick(0,0);
            actions.sportEquipmentActions.selectSportEquipmentType(0,0,0);
            actions.sportEquipmentActions.selectPlusButtonSameForBothFlights(0);
            actions.sportEquipmentActions.expectSportEquipmentAmount1(0,"1");
            actions.sportEquipmentActions.clickConfirmButton();
            actions.potentialTripActions.clickAddSportEquipment(6);
        });
        it("When the user has modified their selection (Increased)", function () {
            actions.sportEquipmentActions.expectSportEquipmentAmount1(0,"1");
            actions.sportEquipmentActions.selectPlusButtonSameForBothFlights(0);
            actions.sportEquipmentActions.expectSportEquipmentAmount1(0,"2");
        });
        it("Then Enable the CTA", function () {
            actions.sportEquipmentActions.expectConfirmButtonIsEnabled();
            actions.sportEquipmentActions.clickConfirmButton();
            actions.potentialTripActions.clickAddSportEquipment(6);
            actions.sportEquipmentActions.expectConfirmButtonIsDisabled();
            actions.sportEquipmentActions.expectSportEquipmentAmount1(0,"2");
        });

    });
});
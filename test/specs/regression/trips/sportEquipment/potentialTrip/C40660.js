describe("TRIPS | Potential Trip Sport Equipment | C40660 | Display passenger logo per passenger | Return Trip ", function () {

    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE");
    var returnDaysFromNow = actions.tripHelper.getRandomDate("PARTIAL");
    var origin = "LTN";
    var destination = "DUB";
    var fareType = "standard";
    var tripWay = "twoway";

    describe("Case:C40660 | 2 Adults, 0 Teens, 0 Children, 0 Infants | LUT-DUB Return Trip", function () {

        it("Given that user has booked return trip", function () {
            var paxMap = {ADT: 2, TEEN: 0, CHD: 0, INF: 0};
            actions.fOHActions.searchReturnFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        });
        it("Given that user open sport equipment card", function () {
            actions.potentialTripActions.clickAddSportEquipment();
            browser.sleep(2000);
        });
        it("Then display passenger name per passenger", function () {
            actions.sportEquipmentActions.expectPassengerTypeAndNumber(0, "Adult1");
            actions.sportEquipmentActions.expectPassengerTypeAndNumber(1, "Adult2");
        });
        it("Then display passenger logo per passenger", function () {
            actions.sportEquipmentActions.expectPassengerLogoIsDisplayed(1);
            actions.sportEquipmentActions.expectPassengerLogoIsDisplayed(2);
        });
        it("Then display quantity card", function () {
            actions.sportEquipmentActions.expectQuantityCardisDisplayed(0);
            actions.sportEquipmentActions.expectQuantityCardisDisplayed(1);
        });
        it("Then display 'same for both flights' checkbox", function () {
            actions.sportEquipmentActions.expectCheckBoxIsDisplayed(0);
            actions.sportEquipmentActions.expectCheckBoxIsDisplayed(1);
        });
        it("Then Display drop down button for equipment field", function () {
            actions.sportEquipmentActions.expectDropdownButtonIsDisplayed(0);
            actions.sportEquipmentActions.expectDropdownButtonIsDisplayed(1);
        });

    });
});
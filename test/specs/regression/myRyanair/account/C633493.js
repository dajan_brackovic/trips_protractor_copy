var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;


function sharedDescribe() {
    var emailAddress = "unverifieduser@ryanair.ie";
    var password = "Password1";

    describe(('User Tries To Login With Non Verified Account'), function () {

        it(sprintf('Given I create a new login and click sign-up' ), function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.login(emailAddress, password);
        });

        it('Then I should see and verify the Welcome Back message', function () {
            actions.fOHActions.verifyWelcomeBackMessage(emailAddress);
        });

    });

}

describe('MYFR | C633493 | SignUp Flows Enhancement | Test 2 | User Tries To Login With Non Verified Account', function () {

    sharedDescribe();

});
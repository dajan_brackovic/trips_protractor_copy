var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;


function sharedDescribe() {
    var emailAddress = actions.tripHelper.getRandomEmail();
    var password = "Password1";

    describe(('Verify Form After SignUp'), function () {

        it(sprintf('Given I create a new login and click sign-up' ), function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.createAccount(emailAddress, password);
        });

        it('Then I should see and verify the Nearly There form', function () {
            actions.fOHActions.verifyNearlyThereMessage(emailAddress);
        });

    });

}

describe('MYFR | C632864 | SignUp Flows Enhancement | Test 1 | Verify Form After SignUp', function () {

    sharedDescribe();

});
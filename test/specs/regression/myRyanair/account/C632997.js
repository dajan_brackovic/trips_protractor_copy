
function sharedDescribe() {

    describe('MRFR FORGOT PASSWOD ', function () {

        it('Given I am on homepage ', function () {
            actions.fOHActions.goToPage();
        });

        it('When I click on MyRyanair', function () {
            actions.fOHActions.clickMyRyanair();
        });

        it('Then I click on Forgot Password', function () {
            actions.fOHActions.clickForgotYourPasswordMyFr();
        });

        it('Then I assert on forgot password form', function () {
            actions.myRyanairActions.assertOnFormForgotPassword();
        });

    });
}

describe('MYFR | C632997 | MyRyanair Signin Signup Landing Page | Test 4', function () {
    sharedDescribe();
});

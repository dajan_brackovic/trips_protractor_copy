
function sharedDescribe() {

    describe('MYFR Assert Privacy Policy is checked by default', function () {
        it('Given I select create account ', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.selectCreateAccountBtn();
        });

        it('Then I assert that the privacy policy radio button is present and selected by default ', function () {
        actions.fOHActions.assertPrivacyPolicyRadioBtn();
    });

  });
}

describe('MYFR | C633745 | MYFR Assert Privacy Policy is checked by default | Test 1 ', function () {
    sharedDescribe();

});

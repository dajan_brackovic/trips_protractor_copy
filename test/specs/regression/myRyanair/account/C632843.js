var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(password) {

    var email = "automation@ryanair.com";

    describe(sprintf('password %s ', password), function () {

        it(sprintf('Given I try to create an account from password %s', password), function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.createAccount(email, password);
            actions.fOHActions.validatePasswordFieldMyFr();
        });

    });
}

describe('MYFR | C632843 | Display Password Requirements', function () {
    sharedDescribe("short");
    sharedDescribe("onlylowercase");
    sharedDescribe("ONLYUPPERCASE");
    sharedDescribe("1MIXed");
    sharedDescribe("123%^&*((-");

});


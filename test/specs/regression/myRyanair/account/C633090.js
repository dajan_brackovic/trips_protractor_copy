
function sharedDescribe() {

    describe('MYFR Assert Privacy Policy Open', function () {
        it('Given I select create account and select privacy policy link ', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.selectCreateAccountBtn();
            actions.fOHActions.clickPrivacyPolicy();
        });

        it('Then I verify privacy policy page opens', function () {
            actions.fOHActions.openAndVerifyPrivacyPolicyPage();
        });


    });
}

describe('MYFR | C633090 | Add Agree Privacy Policy Text And Link To Sign up Form | Test 3 ', function () {
    sharedDescribe();

});


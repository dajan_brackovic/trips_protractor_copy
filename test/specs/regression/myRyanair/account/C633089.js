
function sharedDescribe() {

    var email = "automation@ryanair.com";
    var password = "Password1";

    describe(("create an account and uncheck privacy policy"), function () {

        it(('Given I try to create an account and uncheck privacy policy'), function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.createAccountPrivacyPolicyUnchecked(email, password);
        });

        it(('Then i assert agree privacy policy message appears'), function () {
            actions.fOHActions.uncheckPrivacyPolicy();
        });

    });

}

describe('MYFR | C633089 | Add Agree Privacy Policy Text And Link To Sign up Form | Test 2 ', function () {
    sharedDescribe()

});
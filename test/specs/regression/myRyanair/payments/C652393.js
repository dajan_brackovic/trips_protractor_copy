function sharedDescribe() {
    describe('', function () {
        var userName = actions.tripHelper.getRandomEmail();
        var password = "Password1";
        var cardType = 2;
        var cardNumber = "5210000010001001";
        var cardHolderName = "Cardholder Name";
        var cardNumber2 = "5100000014101198";

        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.myFrSignupHelper.createNewUser(userName, password);
            actions.fOHActions.login(userName, password);
        });

        it('When I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I assert on my ryanair page tabs', function () {
            actions.myRyanairActions.assertOnTabs();
        });

        it('Then I add two payments', function () {
            actions.myRyanairActions.clickPaymentsTab();
            actions.myRyanairActions.clickPaymentMethod();
            actions.myRyanairActions.enterPaymentDetails(cardType,cardNumber,cardHolderName);
            actions.myRyanairActions.enterBillingAddress();
            actions.myRyanairActions.confirmDetails();
            actions.myRyanairActions.addNewPayment(cardType,cardNumber2,cardHolderName);
        });

        it('Then I assert on removing default payment method', function () {
            actions.myRyanairActions.assertOnDeletingDefaultPayment();
        });

    });
};

describe('MYFR | C652393 | MYC - 909 | Login | Add 2 Payment Details | Remove Default Payment And Verify New One Is Set', function () {
    sharedDescribe();

});

afterAll(function () {
    actions.fOHActions.logout();
});

function sharedDescribe() {
    describe('', function () {
        var userName = actions.tripHelper.getRandomEmail();;
        var password = "Password1";
        var cardType = 2;
        var cardNumber = "5210000010001001";
        var cardHolderName = "Cardholder Name";
        var country = 17;
        var city= "Dublin";
        var postCode= "D4";
        var AddressLine1= "Ryanair Hq, Swords";
        var AddressLine2= "";

        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.myFrSignupHelper.createNewUser(userName, password);
            actions.fOHActions.login(userName, password);
        });

        it('When I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I assert on my ryanair page tabs', function () {
            actions.myRyanairActions.assertOnTabs();
        });

        it('Then I add a payment with only one Address line and verify that it is saved', function () {
            actions.myRyanairActions.clickPaymentsTab();
            actions.myRyanairActions.clickPaymentMethod();
            actions.myRyanairActions.enterPaymentDetails(cardType,cardNumber,cardHolderName);
            actions.myRyanairActions.enterCustomBillingAddress(country, city, postCode, AddressLine1, AddressLine2);
            actions.myRyanairActions.confirmDetails();
            actions.myRyanairActions.assertOnPaymentsNumber(1);
        });

    });
};

describe('MYFR | C652398 | MYC - 787 | Login | Add Payment Details | Verify Payment Is saved', function () {
    sharedDescribe();

});

afterAll(function () {
    actions.fOHActions.logout();
});

var Trip = require('../../../../shared/model/Trip');

function sharedDescribe() {
    describe('', function () {
        var progressPercentage;
        var userName = "load53a20@ryanair.ie";
        var password = "Testing123";
        var cardType = 2;
        var cardNumber = "5210000010001001";
        var cardHolderName = "Cardholder Name";

        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it('When I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I assert on my ryanair page tabs', function () {
            actions.myRyanairActions.assertOnTabs();
        });

        it('Then I note MyRyanair Progress Percentage', function () {
            progressPercentage = actions.myRyanairActions.returnMyRyanairProgressinPercentage();
        });

        it('Then I add payments', function () {
            actions.myRyanairActions.clickPaymentsTab();
            actions.myRyanairActions.clickPaymentMethod();
            actions.myRyanairActions.enterPaymentDetails(cardType,cardNumber,cardHolderName);
            actions.myRyanairActions.enterBillingAddress();
            actions.myRyanairActions.confirmDetails();
        });

        it('Then I make sure that myRyanair progress percentage is greater than before adding payment', function () {
            actions.myRyanairActions.clickDashboardTab();
            actions.myRyanairActions.assertOnMyRyanairPercentageIsGreater(progressPercentage);
        });

        it('Then I remove payment details', function () {
            actions.myRyanairActions.clickPaymentsTab();
            actions.myRyanairActions.enterFiledPaymentTabSecurityPassword(password);
            actions.myRyanairActions.clickBtnRemoveSavedPaymentDetails();
        });

        it('Then I make sure that myryanair progress percentage is same as before adding payment', function () {
            actions.myRyanairActions.clickDashboardTab();
            actions.myRyanairActions.assertOnMyRyanairPercentage(progressPercentage);
        });
    });
};

describe('MYFR | C615149 | Login | Dashboard | Add Payment Details | Remove', function () {
    sharedDescribe();

});

afterAll(function () {
    actions.fOHActions.logout();
});
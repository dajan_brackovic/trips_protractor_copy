function sharedDescribe() {
    describe('', function () {
        var progressPercentage;
        var userName = actions.tripHelper.getRandomEmail();
        var password = "Password1";
        var cardType = 2;
        var cardNumber = "5210000010001001";
        var cardHolderName = "Cardholder Name";
        var newCardNumber = "5100000014101198";
        var newCardType = 2;

        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.myFrSignupHelper.createNewUser(userName, password);
            actions.fOHActions.login(userName, password);
        });

        it('When I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I assert on my ryanair page tabs', function () {
            actions.myRyanairActions.assertOnTabs();
        });

        it('Then I note MyRyanair Progress Percentage', function () {
            progressPercentage = actions.myRyanairActions.returnMyRyanairProgressinPercentage();
        });

        it('Then I add payments', function () {
            actions.myRyanairActions.clickPaymentsTab();
            actions.myRyanairActions.clickPaymentMethod();
            actions.myRyanairActions.enterPaymentDetails(cardType,cardNumber,cardHolderName);
            actions.myRyanairActions.enterBillingAddress();
            actions.myRyanairActions.confirmDetails();
        });

        it('Then I make sure that myRyanair progress percentage is greater than before adding payment', function () {
            actions.myRyanairActions.clickDashboardTab();
            actions.myRyanairActions.assertOnMyRyanairPercentageIsGreater(progressPercentage);
        });

        it('Then I edit payment details', function () {
            actions.myRyanairActions.clickPaymentsTab();
            actions.myRyanairActions.enterFiledPaymentTabSecurityPassword(password);
            actions.myRyanairActions.editPaymentDetails(newCardNumber,newCardType);
            actions.myRyanairActions.assertOnAddedCardNumber();
        });

    });
};

describe('MYFR | C652390 | MYC - 595 | Login | Add Payment Details | Edit Payment Details', function () {
    sharedDescribe();

});

afterAll(function () {
    actions.fOHActions.logout();
});

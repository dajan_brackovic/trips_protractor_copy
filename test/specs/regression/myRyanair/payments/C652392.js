var sprintf = require("sprintf").sprintf;

function sharedDescribe(cardType,cardPrefix) {
    describe(sprintf('Card: %s, Prefix: %s', cardType, cardPrefix), function () {

        var userName = actions.tripHelper.getRandomEmail();//"myfrtest1@ryanair.ie";
        var password = "Password1";

        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.myFrSignupHelper.createNewUser(userName, password);
            actions.fOHActions.login(userName, password);
        });

        it('When I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I assert on my ryanair page tabs', function () {
            actions.myRyanairActions.assertOnTabs();
        });

        it('Then assert card types in the dropDown list', function () {
            actions.myRyanairActions.clickPaymentsTab();
            actions.myRyanairActions.clickPaymentMethod();
            actions.myRyanairActions.assertOnCardType(cardType,cardPrefix);
        });

        it('Then I Log out', function () {
            actions.fOHActions.logout();
        });

    });
};

describe('MYFR | C652392 | MYC - 808 | Login | Add Payment Details | Identify card type on a basic card validation | Log out', function () {
    sharedDescribe('American Express',3400);// 34,37 american express
    sharedDescribe('MasterCard',5100);// 51-55 mastercard
    sharedDescribe('Visa',4000);// 4 visa
});

function sharedDescribe() {
    describe('', function () {
        var userName = actions.tripHelper.getRandomEmail();
        var password = "Password1";
        var cardType = 2;
        var cardNumber = "5210000010001001";
        var cardHolderName = "Cardholder Name";
        var cardNumber2 = "5100000014101198";
        var cardNumber3 = "5100000014105561";

        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.myFrSignupHelper.createNewUser(userName, password);
            actions.fOHActions.login(userName, password);
        });

        it('When I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I assert on my ryanair page tabs', function () {
            actions.myRyanairActions.assertOnTabs();
        });

        it('Then I add three payments', function () {
            actions.myRyanairActions.clickPaymentsTab();
            actions.myRyanairActions.clickPaymentMethod();
            actions.myRyanairActions.enterPaymentDetails(cardType,cardNumber,cardHolderName);
            actions.myRyanairActions.enterBillingAddress();
            actions.myRyanairActions.confirmDetails();
            actions.myRyanairActions.addNewPayment(cardType,cardNumber2,cardHolderName);
            actions.myRyanairActions.addNewPayment(cardType,cardNumber3,cardHolderName);
        });

        it('Then I assert on changing default payment method', function () {
            actions.myRyanairActions.assertOnChangeDefaultPayment(cardType,cardNumber3,2);
        });

    });
};

describe('MYFR | C652399 | MYC - 595 | Login | Add 3 Payment Details | Change Default Payment | Verify New Default Payment', function () {
    sharedDescribe();
});

afterAll(function () {
    actions.fOHActions.logout();
});

function sharedDescribe() {
    describe('', function () {
        var userName = actions.tripHelper.getRandomEmail();
        var password = "Password1";
        var cardType = 2;
        var cardNumber = "5210000010001001";
        var cardHolderName = "Cardholder Name";

        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.myFrSignupHelper.createNewUser(userName, password);
            actions.fOHActions.login(userName, password);
        });

        it('When I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I assert on my ryanair page tabs', function () {
            actions.myRyanairActions.assertOnTabs();
        });

        it('Then I add a payment', function () {
            actions.myRyanairActions.clickPaymentsTab();
            actions.myRyanairActions.clickPaymentMethod();
            actions.myRyanairActions.enterPaymentDetails(cardType,cardNumber,cardHolderName);
            actions.myRyanairActions.enterBillingAddress();
            actions.myRyanairActions.confirmDetails();
        });

        it('Then I assert that default payment method is present', function () {
            actions.myRyanairActions.assertExistingDefaultPayment();
        });

    });
};

describe('MYFR | C652396 | MYC - 786 | Login | Add Payment Details | Verify Default Payment', function () {
    sharedDescribe();

});

afterAll(function () {
    actions.fOHActions.logout();
});
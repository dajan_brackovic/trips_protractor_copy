var Trip = require('../../../../shared/model/Trip');

function sharedDescribe() {
    describe('', function () {
        var userName = "load577b5@ryanair.ie";
        var password = "Testing123";

        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it('Then I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I select passengers', function () {
            actions.myRyanairActions.clickOnPassengersTab();
        });

        it('Then I select edit companions', function () {
            actions.myRyanairActions.clickOnEditCompanionsField();
        });

        it('Then I input random names', function () {
            actions.myRyanairActions.fillAddRandomCompanionFields();
        });

        it('Then I save companion changes', function () {
            actions.myRyanairActions.clickSaveCompanionField();
        });
    });

};

describe('MYFR | C652468 | MYC - 322 | Login | Dashboard | Edit Companion basic', function () {
    sharedDescribe();
});

afterAll(function () {
    actions.fOHActions.logout();
});
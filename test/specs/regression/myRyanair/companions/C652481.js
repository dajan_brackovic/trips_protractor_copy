function sharedDescribe(option) {
    describe('', function () {
        var userName = "load9a12b@ryanair.ie";
        var password = "Testing123";

        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it('Then I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I add a companion', function () {
            actions.myRyanairActions.clickBtnAddACompanion();
            actions.myRyanairActions.fillAddCompanionFieldsUnsaved();
        });

        it('Then I add Travel Documents', function () {
            actions.myRyanairActions.addTravelDocument();
            actions.myRyanairActions.selectDropDownNationalityDoc(option);
            actions.myRyanairActions.selectDropDownDocumentType(2);//Passport
            actions.myRyanairActions.inputRandomDocumentNumber();
            actions.myRyanairActions.selectDocumentExpiryDate();
        });

        it('Then I assert only one option for document type ', function () {
            actions.myRyanairActions.assertDropDownDocumentType(2);
        });

        it('Then I should save companion', function () {
            actions.myRyanairActions.clickBtnSaveCompanion();
        });

        it('Then I remove companion', function () {
            actions.myRyanairActions.clickBtnRemoveCompanion();
        });

        it('Then I logout companion', function () {
            actions.fOHActions.logout();
        });
    });

};

describe('MYFR | C652481 | MYC - 321 | Login | Dashboard | Add a companion | Add travel document | Assert only one document type available for non EU citizens | Remove companion', function () {
    sharedDescribe(2); //Afghan
    sharedDescribe(5); //American
    sharedDescribe(11); //Argentinian
    sharedDescribe(14); //Australian
    sharedDescribe(96); //Iraqi
});


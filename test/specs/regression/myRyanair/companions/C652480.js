var Trip = require('../../../../shared/model/Trip');

function sharedDescribe(option) {
    describe('', function () {
        var userName = "load5c9d6@ryanair.ie";
        var password = "Testing123";

        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it('Then I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I add a companion', function () {
            actions.myRyanairActions.clickBtnAddACompanion();
            actions.myRyanairActions.fillAddCompanionFieldsUnsaved();
        });

        it('Then I add Travel Documents', function () {
            actions.myRyanairActions.addTravelDocument();
            actions.myRyanairActions.selectDropDownNationalityDoc(option);
            actions.myRyanairActions.selectDropDownDocumentType(3);//Passport
            actions.myRyanairActions.inputRandomDocumentNumber();
            actions.myRyanairActions.selectDocumentExpiryDate();
        });

        it('Then I assert two options for document type ', function () {
            actions.myRyanairActions.assertDropDownDocumentType(3);
        });

        it('Then I should save companion', function () {
            actions.myRyanairActions.clickBtnSaveCompanion();
        });

        it('Then I remove companion', function () {
            actions.myRyanairActions.clickBtnRemoveCompanion();
        });

        it('Then I logout companion', function () {
            actions.fOHActions.logout();
        });
    });

};

describe('MYFR | C652480 | MYC - 321 | Login | Dashboard | Add a companion | Add travel document | Assert two document types available for EU citizens | Remove companion', function () {
    sharedDescribe(33); //Bulgarian
    sharedDescribe(23); //Belgian
    sharedDescribe(52); //Croatian
    sharedDescribe(55); //Czech
    sharedDescribe(56); //Danish
});

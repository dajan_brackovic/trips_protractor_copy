var Trip = require('../../../../shared/model/Trip');

function sharedDescribe() {
    describe('', function () {


        it('Given I login to my ryanair', function () {
            var userName = "loade7377@ryanair.ie";
            var password = "Testing123";
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it('Then I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I select passengers tab', function () {
            actions.myRyanairActions.clickOnPassengersTab();
        });

        it('Then I assert companions order', function () {
            actions.myRyanairActions.assertOnFavouritePassengerList();
        });

    });
};

describe('MYFR | C652472 | Login | Dashboard | Passengers | Verify companions order', function () {
    sharedDescribe();

});

afterAll(function () {
    actions.fOHActions.logout();
});

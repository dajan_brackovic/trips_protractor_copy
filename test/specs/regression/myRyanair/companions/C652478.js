var Trip = require('../../../../shared/model/Trip');

function sharedDescribe(option) {
    describe('', function () {
        var userName = "loaddea49@ryanair.ie";
        var password = "Testing123";

        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it('Then I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I add a companion', function () {
            actions.myRyanairActions.clickBtnAddACompanion();
            actions.myRyanairActions.fillAddCompanionFieldsUnsaved();
        });

        it('Then I add Travel Documents', function () {
            actions.myRyanairActions.addTravelDocument();
            actions.myRyanairActions.selectDropDownNationalityDoc(option);
            actions.myRyanairActions.selectDropDownDocumentType(2);
            actions.myRyanairActions.inputRandomDocumentNumber();
            actions.myRyanairActions.selectDropDownCountryOfIssue();
        });

        it('Then I assert expiry date for EEA card is not present', function () {
            actions.myRyanairActions.assertDocumentExpiryDateIsNotPresent();
        });

        it('Then I should save companion', function () {
            actions.myRyanairActions.clickBtnSaveCompanion();
        });

        it('Then I remove companion', function () {
            actions.myRyanairActions.clickBtnRemoveCompanion();
        });

        it('Then I logout companion', function () {
            actions.fOHActions.logout();
        });
    });

};

describe('MYFR | C652478 | MYC - 321 | Login | Dashboard | Add a companion | Add travel document | Assert expiry date is not present for french and greek | Remove companion', function () {
    sharedDescribe(73); //French nationalities
    sharedDescribe(80); //Greek nationalities
});

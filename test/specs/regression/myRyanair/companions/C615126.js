var Trip = require('../../../../shared/model/Trip');

function sharedDescribe() {
    describe('', function () {
        var title = "Mr";
        var FirstName = "Myryanair";
        var LastName = "User";
        var dayIndex = 10;
        var monthIndex = 2;
        var yearIndex = 27;
        var nationality = "Irish";
        var progressPercentage;

        it('Given I login to my ryanair', function () {
            var userName = "myfrtest1@ryanair.ie";
            var password = "Password1";
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it('When I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I assert on my ryanair page tabs', function () {
            actions.myRyanairActions.assertOnTabs();
        });

        it('Then I note MyRyanair Progress Persentage', function () {
            progressPercentage = actions.myRyanairActions.returnMyRyanairProgressinPercentage();
        });

        it('Then I add companion', function () {
            actions.myRyanairActions.clickBtnAddACompanion();
            actions.myRyanairActions.clickTypeFriendCompanion();
            actions.myRyanairActions.selectDropDownTitle(title);
            actions.myRyanairActions.inputFieldFirstName(FirstName);
            actions.myRyanairActions.inputFieldLastName(LastName);
            actions.myRyanairActions.selectDropDownDateOfBirthDay(dayIndex);
            actions.myRyanairActions.selectDropDownDateOfBirthMonth(monthIndex);
            actions.myRyanairActions.selectDropDownDateOfBirthYear(yearIndex);
            actions.myRyanairActions.selectDropDownNationality(nationality);
            actions.myRyanairActions.clickBtnSaveCompanion();
        });

        it('Then I make sure that myRyanair progress percentage is greater than before adding companion', function () {
            actions.myRyanairActions.clickDashboardTab();
            actions.myRyanairActions.assertOnMyRyanairPercentageIsGreater(progressPercentage);
        });

        it('Then I remove companion', function () {
            actions.myRyanairActions.clickPassengersTab();
            actions.myRyanairActions.clickBtnRemoveCompanion();
        });

        it('Then I make sure that myryanair progress percentage is same as before adding companion', function () {
            actions.myRyanairActions.clickDashboardTab();
            actions.myRyanairActions.assertOnMyRyanairPercentage(progressPercentage);
        });
    });
};

describe('MYFR | C615126 | Login | Dashboard | Add Companion | Remove Companion', function () {
    sharedDescribe();

});

afterAll(function () {
    actions.fOHActions.logout();
});
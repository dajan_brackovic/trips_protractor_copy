var Trip = require('../../../../shared/model/Trip');

function sharedDescribe() {
    describe('', function () {
        var userName = "loadcdb5c@ryanair.ie";
        var password = "Testing123";

        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it('Then I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I select passengers', function () {
            actions.myRyanairActions.clickOnPassengersTab();
        });

        it('Then I select edit companions', function () {
            actions.myRyanairActions.clickOnEditCompanionsField();
        });

        it('Then I add Travel Documents', function () {
            actions.myRyanairActions.addTravelDocument();
            actions.myRyanairActions.selectDropDownNationalityDoc(97);//Ireland
            actions.myRyanairActions.selectDropDownDocumentType(3);//Passport card
            actions.myRyanairActions.inputRandomDocumentNumber();
            actions.myRyanairActions.selectDocumentExpiryDate();
        });

        it('Then I verify there are two dropdown options', function () {
            actions.myRyanairActions.assertDropDownDocumentType(3);
        });

        it('Then I should save companion', function () {
            actions.myRyanairActions.clickBtnSaveCompanion();
            actions.myRyanairActions.clickOnEditCompanionsField();
            actions.myRyanairActions.clickOnRemoveCompanionsTravelDoc();
        });

        it('Then I logout companion', function () {
            actions.fOHActions.logout();
        });

    });

};

describe('MYFR | C652479 | MYC - 321 | Login | Dashboard | Edit a companion | Add travel document | Verify Passport Card is available for Irish nationality | Remove document', function () {
    sharedDescribe();
});

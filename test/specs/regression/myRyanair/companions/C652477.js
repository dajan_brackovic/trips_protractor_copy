var Trip = require('../../../../shared/model/Trip');

function sharedDescribe(option) {
    describe('', function () {
        var userName = "loadba6f2@ryanair.ie";
        var password = "Testing123";

        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it('Then I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I add a companion', function () {
            actions.myRyanairActions.clickBtnAddACompanion();
            actions.myRyanairActions.fillAddCompanionFieldsUnsaved();
        });

        it('Then I add Travel Documents', function () {
            actions.myRyanairActions.addTravelDocument();
            actions.myRyanairActions.selectDropDownNationalityDoc(option);
            actions.myRyanairActions.selectDropDownDocumentType(2);
            actions.myRyanairActions.inputRandomDocumentNumber();
            actions.myRyanairActions.selectDocumentExpiryDate();
        });

        it('Then I assert only one option for document type ', function () {
            actions.myRyanairActions.assertDropDownDocumentType(2);
        });

        it('Then I should save companion', function () {
            actions.myRyanairActions.clickBtnSaveCompanion();
        });

        it('Then I remove companion', function () {
            actions.myRyanairActions.clickBtnRemoveCompanion();
        });

        it('Then I logout companion', function () {
            actions.fOHActions.logout();
        });
    });

};

describe('MYFR | C652477 | MYC - 321 | Login | Dashboard | Add a companion | Add travel document | Assert only passport available in document type | Remove companion', function () {
    sharedDescribe(31); //this is for British nationality in add document
    sharedDescribe(7); //Andorra
});


var Trip = require('../../../../shared/model/Trip');

function sharedDescribe() {
    describe('', function () {
        var userName = "load97458@ryanair.ie";
        var password = "Testing123";

        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it('Then I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I add a companion', function () {
            actions.myRyanairActions.clickBtnAddACompanion();
            actions.myRyanairActions.fillAddRandomCompanionFields();
        });

        it('Then I add Travel Documents', function () {
            actions.myRyanairActions.addTravelDocument();
            actions.myRyanairActions.selectDropDownNationalityDoc(5);
            actions.myRyanairActions.selectDropDownDocumentType(2);
            actions.myRyanairActions.inputRandomDocumentNumber();
            actions.myRyanairActions.selectDocumentExpiryDate();
        });

        it('Then I should save companion', function () {
            actions.myRyanairActions.clickBtnSaveCompanion();
        });

        it('Then I should remove companions travel documents', function () {
            actions.myRyanairActions.clickOnEditCompanionsField();
            actions.myRyanairActions.clickOnRemoveCompanionsTravelDoc();
            actions.myRyanairActions.verifyAddDocumentButtonIsPresent();
        });

        it('Then I should save companion', function () {
            actions.myRyanairActions.clickBtnSaveCompanion();
        });

        it('Then I remove companion', function () {
            actions.myRyanairActions.clickBtnRemoveCompanion();
        });
    });

};

describe('MYFR | C652470 | MYC - 567 | Login | Dashboard | Add a companion | Add and Delete travel document | Assert delete | Remove companion', function () {
    sharedDescribe();
});

afterAll(function () {
    actions.fOHActions.logout();
});

var Trip = require('../../../../shared/model/Trip');

function sharedDescribe() {
    describe('', function () {

        var userName = "loadac30c@ryanair.ie";
        var password = "Testing123";

        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it('Then I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I add a companion of each Pax type and verify correct Title options', function () {
            actions.myRyanairActions.clickBtnAddACompanion();
            actions.myRyanairActions.addSpecificPaxTypeCompanion("adult", 20);
            actions.myRyanairActions.clickBtnAddCompanionsCompanionPanel();
            actions.myRyanairActions.addSpecificPaxTypeCompanion("teen", 14);
            actions.myRyanairActions.clickBtnAddCompanionsCompanionPanel();
            actions.myRyanairActions.addSpecificPaxTypeCompanion("child", 7);
            actions.myRyanairActions.clickBtnAddCompanionsCompanionPanel();
            actions.myRyanairActions.addSpecificPaxTypeCompanion("infant", 2);
        });

        it('Then I remove all companions', function () {
            actions.myRyanairActions.removeAllCompanions();
        });

    });
}

describe('MYFR | C643834 | MYC - 1911 |Verify Correct Passenger Titles when adding/editing Companions', function () {
    sharedDescribe();

});

afterAll(function () {
    actions.fOHActions.logout();
});


var sprintf = require("sprintf").sprintf;
var specId = "|myRyanair |fohintegration| C652440 | samepage ";

function sharedDescribe(menuIndex, subMenuIndex, pageUrl, sideMenuIdex, sideSubMenuIndex) {
    describe("", function () {
        it('Navigate to the homepage', function () {
            actions.fOHActions.goToPage();
        });

        it('Clic the menu link option', function () {
            actions.fOHActions.clickLinkFromMenuLinkOption(menuIndex, subMenuIndex);
        });
        it(sprintf("Verify the page url is not changed", pageUrl), function () {
            actions.fOHActions.verifyPageNotChanged(pageUrl);
        });
        it(sprintf("Resize the window to be 768px to 991px"), function () {
            browser.driver.manage().window().setSize(768, 991);
            browser.waitForAngular();
        });
        it('Click the corresponding side menu item', function () {
            actions.fOHActions.clickLinkFromSideMenu(sideMenuIdex, sideSubMenuIndex);
        });
        it(sprintf("Verify the page url is not changed", pageUrl), function () {
            actions.fOHActions.verifyPageNotChanged(pageUrl);
            browser.driver.manage().window().maximize();
        });

    });
}

describe(specId + "MYC 1128 QA Verify global header -same page", function () {
    //plan - search for flight
    sharedDescribe(0, 1, "", 0, 0);
    //manage
    sharedDescribe(1, -1, "", 3, -1);
});










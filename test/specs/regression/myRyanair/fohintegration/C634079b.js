function sharedDescribe(option, url) {

    describe('MYFR Assert all markets are optional and direct you to correct URL ', function () {

        it('Given I select market drop down list ', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.selectMarketsMenu();
        });

        it('Then I select market and I am directed to correct URL ', function () {
            actions.fOHActions.clickMarketList(option);
            actions.fOHActions.assertUrl(url);

        });
    });

}

describe('MYFR | C634079 | Part B | Display Market And Language ', function () {
    sharedDescribe("Germany", "de/de");
    sharedDescribe("Great Britain", "gb/en");
    sharedDescribe("Greece (English)", "gr/en");
    sharedDescribe("Greece (Greek)", "gr/el");
    sharedDescribe("Hungary (English)", "hu/en");
    sharedDescribe("Hungary (Hungarian)", "hu/hu");
    sharedDescribe("Ireland", "ie/en");
    sharedDescribe("Italy", "it/it");
    sharedDescribe("Latvia", "lv/en");
    sharedDescribe("Lithuania (English)", "lt/en");
    sharedDescribe("Lithuania (Lithuanian)", "lt/lt");
    sharedDescribe("Malta", "mt/en");
});


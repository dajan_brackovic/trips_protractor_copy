var sprintf = require("sprintf").sprintf;
var specId = "|myRyanair |fohintegration| C652440 |sametablink ";

function sharedDescribe(menuIndex, subMenuIndex, pageUrl, sideMenuIdex, sideSubMenuIndex) {
    describe("", function () {
        it('Navigate to the homepage', function () {
            actions.fOHActions.goToPage();
        });

        it('Click the top menu options', function () {
            actions.fOHActions.clickLinkFromMenuLinkOption(menuIndex, subMenuIndex);
        });
        it(sprintf("Verify the page url changes and page is shown", pageUrl), function () {
            actions.fOHActions.verifyPageSuccessOpened(pageUrl);
        });
        it(sprintf("Resize the window to be 768px to 991px"), function () {
            browser.driver.manage().window().setSize(768, 991);
            browser.waitForAngular();
        });
        it('Click the corresponding side menu options', function () {
            actions.fOHActions.clickLinkFromSideMenu(sideMenuIdex, sideSubMenuIndex);
        });
        it(sprintf("Verify the page url changes and page is shown", pageUrl), function () {
            actions.fOHActions.verifyPageSuccessOpened(pageUrl);
            actions.fOHActions.goToPage();
            browser.driver.manage().window().maximize();
        });

    });
}

describe(specId + "MYC 1128 QA  Verify global header -same tab", function () {
    sharedDescribe(0, 2, "/cheap-flights", 0, 1);
    sharedDescribe(0, 3, "/cheap-flight-destinations", 0, 2);
    sharedDescribe(0, 9, "/samsonite/buy-now", 1, 4);
    sharedDescribe(0, 10, "/plan-trip/travel-extras/gift-vouchers", 1, 5);
    sharedDescribe(0, 11, "/plan-trip/flying-with-us/family-extra", 2, 0);
    sharedDescribe(0, 12, "/plan-trip/flying-with-us/business-plus", 2, 1);
    sharedDescribe(0, 13, "/plan-trip/flying-with-us/groups", 2, 2);
    sharedDescribe(0, 14, "/plan-trip/flying-with-us/our-app", 2, 3);
    sharedDescribe(0, 15, "/plan-trip/flying-with-us/my-ryanair", 2, 4);
    sharedDescribe(0, 16, "/plan-trip/travel-extras/reserved-seating", 2, 5);
    sharedDescribe(1, -1, "", 3, -1);
    //check-in
    sharedDescribe(2, -1, "", 4, -1);
});










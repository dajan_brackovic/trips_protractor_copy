var sprintf = require("sprintf").sprintf;
var specId = "|myRyanair |fohintegration| C652440 | newtab";

function sharedDescribe(newurl, sidemenuindex) {
    describe('', function () {

        it(sprintf('Given I am on the %s culture home page, resize the window to be 768 * 991'), function () {
            actions.fOHActions.goToPage();
            browser.driver.manage().window().setSize(768, 991);
            browser.waitForAngular();
        });

        it(sprintf('Then I click side menu and verify the newly opened tab has the correct url', newurl, sidemenuindex), function () {
            actions.fOHActions.clickSideMenuVerifyNewTabUrl(newurl, sidemenuindex);
        });

    });
}

describe(specId + "MYC 1128 QA Verify global header -newtab", function () {
    sharedDescribe("car-hire.ryanair.com", 0);
    sharedDescribe("hotels.ryanair.com/", 1);

});


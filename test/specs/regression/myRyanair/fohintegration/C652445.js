var sprintf = require("sprintf").sprintf;
var specId = "|myRyanair |fohintegration| C652445 ";
describe(specId + "MYC 1756 Display 5 most recent searches", function () {
    var fohHomeActions = actions.fOHActions;
    var tripsHomeActions = actions.tripsHomeActions;

    describe('', function () {
        it('Given I on FOH flights page', function () {
            fohHomeActions.goToPage();
            fohHomeActions.searchFlightOneWay("ALC", "STN");
            fohHomeActions.verifyPassengerFields();
            fohHomeActions.chooseDatesOneWay(4);
            fohHomeActions.clickLetsGoBtn();
            fohHomeActions.goToPage();
            fohHomeActions.verifyRecentSearchRouteByIndex("Alicante", "London (STN)", 0);
            fohHomeActions.verifyRecentSearchNoMoreThanFive();
        });
        it('Search flight 2', function () {
            fohHomeActions.goToPage();
            fohHomeActions.searchFlightOneWay("Dublin", "Birmingham");
            fohHomeActions.verifyPassengerFields();
            fohHomeActions.chooseDatesOneWay(4);
            fohHomeActions.clickLetsGoBtn();
            fohHomeActions.goToPage();
            fohHomeActions.verifyRecentSearchRouteByIndex("Dublin", "Birmingham", 0);
            fohHomeActions.verifyRecentSearchRouteByIndex("Alicante", "London (STN)", 1);
            fohHomeActions.verifyRecentSearchNoMoreThanFive();


        });
        it('Search flight 3', function () {
            fohHomeActions.goToPage();
            fohHomeActions.searchFlightOneWay("Dublin", "Glasgow");
            fohHomeActions.verifyPassengerFields();
            fohHomeActions.chooseDatesOneWay(4);
            fohHomeActions.clickLetsGoBtn();
            fohHomeActions.goToPage();
            fohHomeActions.verifyRecentSearchRouteByIndex("Dublin", "Glasgow", 0);
            fohHomeActions.verifyRecentSearchRouteByIndex("Dublin", "Birmingham", 1);
            fohHomeActions.verifyRecentSearchRouteByIndex("Alicante", "London (STN)", 2);
            fohHomeActions.verifyRecentSearchNoMoreThanFive();

        });
        it('Search flight 4', function () {
            fohHomeActions.goToPage();
            fohHomeActions.searchFlightOneWay("Pisa", "Liverpool");
            fohHomeActions.verifyPassengerFields();
            fohHomeActions.chooseDatesOneWay(4);
            fohHomeActions.clickLetsGoBtn();
            fohHomeActions.goToPage();
            fohHomeActions.verifyRecentSearchRouteByIndex("Pisa", "Liverpool", 0);
            fohHomeActions.verifyRecentSearchRouteByIndex("Dublin", "Glasgow", 1);
            fohHomeActions.verifyRecentSearchRouteByIndex("Dublin", "Birmingham", 2);
            fohHomeActions.verifyRecentSearchRouteByIndex("Alicante", "London (STN)", 3);
            fohHomeActions.verifyRecentSearchNoMoreThanFive();

        });
        it('Search flight 5', function () {
            fohHomeActions.goToPage();
            fohHomeActions.searchFlightOneWay("Dublin", "Liverpool");
            fohHomeActions.verifyPassengerFields();
            fohHomeActions.chooseDatesOneWay(4);
            fohHomeActions.clickLetsGoBtn();
            fohHomeActions.goToPage();
            fohHomeActions.verifyRecentSearchRouteByIndex("Dublin", "Liverpool", 0);
            fohHomeActions.verifyRecentSearchRouteByIndex("Pisa", "Liverpool", 1);
            fohHomeActions.verifyRecentSearchRouteByIndex("Dublin", "Glasgow", 2);
            fohHomeActions.verifyRecentSearchRouteByIndex("Dublin", "Birmingham", 3);
            fohHomeActions.verifyRecentSearchRouteByIndex("Alicante", "London (STN)", 4);
            fohHomeActions.verifyRecentSearchNoMoreThanFive();

        });
        it('Search flight 6', function () {
            fohHomeActions.goToPage();
            fohHomeActions.searchFlightOneWay("Dublin", "Manchester");
            fohHomeActions.verifyPassengerFields();
            fohHomeActions.chooseDatesOneWay(4);
            fohHomeActions.clickLetsGoBtn();
            fohHomeActions.goToPage();
            fohHomeActions.verifyRecentSearchRouteByIndex("Dublin", "Manchester", 0);
            fohHomeActions.verifyRecentSearchRouteByIndex("Dublin", "Liverpool", 1);
            fohHomeActions.verifyRecentSearchRouteByIndex("Pisa", "Liverpool", 2);
            fohHomeActions.verifyRecentSearchRouteByIndex("Dublin", "Glasgow", 3);
            fohHomeActions.verifyRecentSearchRouteByIndex("Dublin", "Birmingham", 4);
            fohHomeActions.verifyRecentSearchNoMoreThanFive();
        });
    });

});



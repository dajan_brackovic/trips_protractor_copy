function sharedDescribe(option, url) {

    describe('MYFR Assert all markets are optional and direct you to correct URL ', function () {

        it('Given I select market drop down list ', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.selectMarketsMenu();
        });

        it('Then I select market and I am directed to correct URL ', function () {
            actions.fOHActions.clickMarketList(option);
            actions.fOHActions.assertUrl(url);

        });
    });

}

describe('MYFR | C634079 | Part C | Display Market And Language ', function () {
    sharedDescribe("Morocco", "ma/fr");
    sharedDescribe("Netherlands", "nl/nl");
    sharedDescribe("Norway (English)", "no/en");
    sharedDescribe("Norway (Norwegian)", "no/no");
    sharedDescribe("Poland", "pl/pl");
    sharedDescribe("Portugal", "pt/pt");
    sharedDescribe("Romania (English)", "ro/en");
    sharedDescribe("Romania (Romanian)", "ro/ro");
    sharedDescribe("Slovakia", "sk/en");
    sharedDescribe("Spain (Spanish)", "es/es");
    sharedDescribe("Spain (Catalan)", "es/ca");
    sharedDescribe("Sweden (English)", "se/en");
    sharedDescribe("Sweden (Swedish)", "se/sv");
    sharedDescribe("United States", "us/en");
});


var sprintf = require("sprintf").sprintf;
var specId = "|myRyanair |fohintegration| C652440 sametabuser ";

function sharedDescribe(menuIndex, subMenuIndex, pageUrl, sideMenuIdex, sideSubMenuIndex) {
    describe("", function () {
        it('Navigate to the homepage', function () {
            actions.fOHActions.goToPage();
        });

        it('Click the top menu options', function () {
            actions.fOHActions.clickLinkFromMenuLinkUser(menuIndex, subMenuIndex);
        });
        it(sprintf("Verify the page url changes and page is shown", pageUrl), function () {
            actions.fOHActions.verifyPageSuccessOpened(pageUrl);
            actions.fOHActions.goToPage();
        });
        it(sprintf("Resize the window to be 768px to 991px"), function () {
            browser.driver.manage().window().setSize(768, 991);
            browser.waitForAngular();
        });
        it('Click the corresponding side menu options', function () {
            actions.fOHActions.clickLinkFromSideMenu(sideMenuIdex, sideSubMenuIndex);
        });
        it(sprintf("Verify the page url changes and page is shown", pageUrl), function () {
            actions.fOHActions.verifyPageSuccessOpened(pageUrl);
            browser.driver.manage().window().maximize();
        });

    });
}

describe(specId + "MYC 1128 QA Verify global header -same tab registered user link", function () {
    //myryanair
    sharedDescribe(0, -1, "/", -2, -1);
    //My info:
    sharedDescribe(1, 0, "useful-info/help-centre", 3, 0);
    sharedDescribe(1, 1, "useful-info/help-centre/faq-overview", 3, 1);
    sharedDescribe(1, 2, "useful-info/help-centre/terms-and-conditions", 3, 2);
    sharedDescribe(1, 3, "useful-info/help-centre/faq-overview/contact-us/Contacting-customer-service", 3, 3);
    sharedDescribe(1, 4, "useful-info/help-centre/fees", 3, 4);
    sharedDescribe(1, 5, "timetable", 4, 0);
    sharedDescribe(1, 6, "flight-info/route", 4, 1);
    sharedDescribe(1, 7, "useful-info/help-centre/travel-updates", 4, 2);
    sharedDescribe(1, 8, "useful-info/about-ryanair/inflight-magazine", 4, 3);
    sharedDescribe(1, 9, "useful-info/about-ryanair/about-us", 5, 0);
    sharedDescribe(1, 10, "useful-info/about-ryanair/fleet", 5, 1);
    sharedDescribe(1, 11, "useful-info/about-ryanair/our-guarantees", 5, 2);
    sharedDescribe(1, 12, "useful-info/about-ryanair/customer-testimonials", 5, 3);
    //markets
    sharedDescribe(2, -1, "/", -1, -1);

});










function sharedDescribe(option, url) {

    describe('MYFR Assert all markets are optional and direct you to correct URL ', function () {

        it('Given I select market drop down list ', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.selectMarketsMenu();
        });

        it('Then I select market and I am directed to correct URL ', function () {
            actions.fOHActions.clickMarketList(option);
            actions.fOHActions.assertUrl(url);

        });
    });

}

describe('MYFR | C634079 | Part A | Display Market And Language ', function () {
    sharedDescribe("Austria", "at/de");
    sharedDescribe("Belgium (French)", "be/fr");
    sharedDescribe("Belgium (Dutch)", "be/nl");
    sharedDescribe("Bulgaria", "bg/en");
    sharedDescribe("Croatia", "hr/en");
    sharedDescribe("Cyprus", "cy/en");
    sharedDescribe("Czech Republic", "cz/en");
    sharedDescribe("Denmark (English)", "dk/en");
    sharedDescribe("Denmark (Danish)", "dk/da");
    sharedDescribe("Estonia", "ee/en");
    sharedDescribe("Finland", "fi/en");
    sharedDescribe("France", "fr/fr");
});

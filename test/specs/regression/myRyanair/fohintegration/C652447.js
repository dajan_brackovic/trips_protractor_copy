var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;
var specId = "|myRyanair |fohintegration| C652447 ";

function sharedDescribe() {

    var origin = "dub";
    var destination;
    var outBoundDaysFromNow = 3;//today index === 1
    var returnDaysFromNow;
    var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
    var fareType = "standard";
    var tripWay = "oneway";

    describe(specId+"MYC 1755 Display view all saved trips page on CTA = ViewAll", function () {
        it('Given I login to my ryanair', function () {
            var userName = "load93002@ryanair.ie";
            var password = "Testing123";
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it('Then I make a standard one way trip with outbound < 7 Days from now', function () {
            destination = "krk";
            actions.fOHActions.searchOneWayFLightWithPaxMyFr(paxMap, origin, destination, outBoundDaysFromNow);
        });

        it('Then I select flight', function () {
            actions.tripsHomeActions.selectAFlightOnly(0, 0, fareType, tripWay);
        });

        it('Then I click on save trip icon', function () {
            actions.tripsHomeActions.clickBtnSaveTrip();
        });

        it('Then I click on Ryanair Home Page logo on flight select page', function () {
            actions.bookingSummaryActions.clickRyanairLogo();    // This action is happening on flight select page
        });

        it('Then I make a standard Rt trip with outbound < 7 and inbound < 7 Days from now', function () {
            destination = "STN";
            actions.fOHActions.goToPage();
            actions.fOHActions.searchOneWayFLightWithPaxMyFr(paxMap, origin, destination, outBoundDaysFromNow);
            actions.tripsHomeActions.selectAFlightOnly(0, 0, fareType, tripWay);
        });

        it('Then I click on save trip icon', function () {
            actions.tripsHomeActions.clickBtnSaveTrip();
        });

        it('Then I click on Ryanair Home Page logo on flight select page', function () {
            actions.bookingSummaryActions.clickRyanairLogo();    // This action is happening on flight select page
        });

        it("Verify that user is redirected to the potential trip page for that saved trip", function () {
            actions.fOHActions.clickViewAllSaved();
        });

        it("Then I assert on saved trips", function () {
            actions.manageTripsActions.assertOnSavedTripCard(true, 2);
        });

        it("Then I assert on recent saved trips is displayed first", function () {
            actions.manageTripsActions.assertOnSavedTripDestination(destination);
        });

        it("Then I remove saved trips", function () {
            actions.manageTripsActions.clickBtnRemoveSavedTrip();
            actions.manageTripsActions.clickBtnRemoveSavedTrip();
            actions.fOHActions.goToPage();
        });

    });
}

describe(specId+'MYC 1755 Display view all saved trips page', function () {
    sharedDescribe();

});

afterAll(function () {
    actions.fOHActions.logout();
});
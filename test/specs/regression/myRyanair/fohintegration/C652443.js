var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;
var specId = "|myRyanair |fohintegration| C652443 ";


function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE"); //today index === 1
    var origin = "STN";
    var destination = actions.tripHelper.getRandomAirportFromArray();
    var fareType = "standard";
    var tripWay = "oneway";
    var userName = actions.tripHelper.getRandomEmail();
    var password = "Testing123";
    var trip;
    var countBefore;
    var countAfter;


    var bookFlight = function (paxMap) {
        trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
        actions.fOHActions.searchOneWayFLightWithPaxMyFr(paxMap, origin, destination, outBoundDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        actions.extrasActions.skipExtras();
    };

    describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
        it('Login and book first flight', function () {
            var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
            actions.fOHActions.goToPage();
            actions.myFrSignupHelper.createNewUser(userName, password);
            actions.fOHActions.login(userName, password);
            bookFlight(paxMap);
            actions.addPaxActions.addSavedPaxNameNewUserMyRyanair(trip.journey.paxList);
            actions.addPaxActions.addContactForNewUserWithNoPhoneNumber();
            actions.addPaxActions.makeCardPaymentActive(trip.bookingContact.card);
            actions.bookingSummaryActions.verifyConfirmationMessage();
            actions.fOHActions.goToPage();
        });

        it('Book one ticket and verify the amount change in upcoming trips', function () {
            actions.fOHActions.getNumUpcomingTrips().then(function (result) {
                countBefore = result.num;
                var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
                bookFlight(paxMap);
                actions.addPaxActions.addSavedPaxNameNewUserMyRyanair(trip.journey.paxList);
                actions.addPaxActions.addContactForNewUserWithNoPhoneNumber();
                actions.addPaxActions.makeCardPaymentActive(trip.bookingContact.card);
                actions.bookingSummaryActions.verifyConfirmationMessage();
                actions.fOHActions.goToPage();
                actions.fOHActions.getNumUpcomingTrips().then(function (result) {
                    countAfter = result.num;
                    expect(countAfter > countBefore).toBeTruthy();
                });
            });
        });

        it('Check the one in homepage is the earliest trip', function () {
            actions.fOHActions.verifyNearestTripShown();
        });
    });
}

describe(specId+' | MYC 1754 | Display nearest approaching upcoming trip card when user is on upcoming trip view', function () {
    sharedDescribe(1, 0, 0, 0);

});
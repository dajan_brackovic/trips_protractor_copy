function sharedDescribe() {
    describe('', function () {
        var userName = "loadbe751@ryanair.ie";
        var password = "Testing123";

        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it('Then I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I navigate to my profile', function () {
            actions.myRyanairActions.clickEditProfile();
            actions.myRyanairActions.clickBtnShowPassportDetails();
        });

        it('Then modify passport expiry date', function () {
            actions.myRyanairActions.updateExpiryDatePassportDetails();
        });

        it('Then I update details', function () {
            actions.myRyanairActions.clickBtnAddDocumentDetails();
        });

        it('Then I logout companion', function () {
            actions.fOHActions.logout();
        });
    });

};

describe('MYFR | C652466 | MYC - 141 | Login | Dashboard | Edit profile | Edit travel document | Modify expiration date', function () {
    sharedDescribe();
});

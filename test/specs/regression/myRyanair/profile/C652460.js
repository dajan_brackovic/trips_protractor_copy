function sharedDescribe(option) {
    describe('', function () {
        var userName = "loada2166@ryanair.ie";
        var password = "Testing123";

        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it('Then I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I navigate to my profile and add document', function () {
            actions.myRyanairActions.clickEditProfile();
            actions.myRyanairActions.clickProfileAddDocButton();
        });

        it('Then add document details', function () {
            actions.myRyanairActions.selectProfileDropDownNationalityDoc(option);
            actions.myRyanairActions.addProfileDropDownDocumentType(2);
            actions.myRyanairActions.addDocumentDetails();
        });

        it('Then I assert expiry date for EEA national card is not present', function () {
            actions.myRyanairActions.assertDocumentExpiryDateIsNotPresent();
        });

        it('Then I add details entered ', function () {
            actions.myRyanairActions.clickBtnAddDocumentDetails();
        });

        it('Then I remove document ', function () {
            actions.myRyanairActions.selectProfileDocumentDetails();
            actions.myRyanairActions.removeProfileDocumentDetails();
        });

        it('Then I logout', function () {
            actions.fOHActions.logout();
        });
    });

};

describe('MYFR | C652460 | MYC - 315 | Login | Dashboard | Edit Profile and assert no expiry date on EEA National card for French and Greek ', function () {
    sharedDescribe(73); //French nationalities
    sharedDescribe(80); //Greek nationalities
});


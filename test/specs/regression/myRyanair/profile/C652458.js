function sharedDescribe() {
    describe('', function () {
        var userName = "load5d79d@ryanair.ie";
        var password = "Testing123";

        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it('Then I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I navigate to my profile', function () {
            actions.myRyanairActions.clickEditProfile();
            actions.myRyanairActions.clickEditProfileButton();
        });

        it('Then I edit and save my profile', function () {
            actions.myRyanairActions.editProfileFields();
        });
    });

};

describe('MYFR | C652458 | MYC - 315 | Login | Dashboard | Edit Profile', function () {
    sharedDescribe();
});

afterAll(function () {
    actions.fOHActions.logout();
});

function sharedDescribe(option) {
    describe('', function () {
        var userName = "loadf048a@ryanair.ie";
        var password = "Testing123";

        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it('Then I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I navigate to my profile and add document', function () {
            actions.myRyanairActions.clickEditProfile();
            actions.myRyanairActions.clickProfileAddDocButton();
        });

        it('Then add document details', function () {
            actions.myRyanairActions.selectProfileDropDownNationalityDoc(option);
            actions.myRyanairActions.addProfileDropDownDocumentType(2);
            actions.myRyanairActions.addDocumentDetails();
        });

        it('Then I assert two options for document type ', function () {
            actions.myRyanairActions.assertProfileDropDownDocumentType(3);
        });
        it('Then I add details entered ', function () {
            actions.myRyanairActions.clickBtnAddDocumentDetails();
        });

        it('Then I remove document ', function () {
            actions.myRyanairActions.selectProfileDocumentDetails();
            actions.myRyanairActions.removeProfileDocumentDetails();
        });

        it('Then I logout', function () {
            actions.fOHActions.logout();
        });
    });

};

describe('MYFR | C652462 | MYC - 318 | Login | Dashboard | Edit Profile | Add travel document | Assert two document types available for EU citizens | Remove Document', function () {
    sharedDescribe(32); //Bulgarian
    sharedDescribe(22); //Belgian
    sharedDescribe(51); //Croatian
    sharedDescribe(54); //Czech
    sharedDescribe(55); //Danish
});


function sharedDescribe() {
    describe('', function () {
        var userName = "loade1985@ryanair.ie";
        var password = "Testing123";

        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it('Then I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I navigate to my profile and add document', function () {
            actions.myRyanairActions.clickEditProfile();
            actions.myRyanairActions.clickProfileAddDocButton();
        });

        it('Then add document details', function () {
            actions.myRyanairActions.selectRandomDropDownNationality();
            actions.myRyanairActions.addProfileDropDownDocumentType(2);
            actions.myRyanairActions.addDocumentDetails();
        });


        it('Then I add details entered ', function () {
            actions.myRyanairActions.clickBtnAddDocumentDetails();
        });

        it('Then I remove document ', function () {
            actions.myRyanairActions.clickBtnShowPassportDetails();
            actions.myRyanairActions.removeProfileDocumentDetails();
        });

        it('Then I logout companion', function () {
            actions.fOHActions.logout();
        });
    });

};

describe('MYFR | C652465 | MYC - 318 | Login | Dashboard | Edit Profile | Add Travel Document ', function () {
    sharedDescribe();
});


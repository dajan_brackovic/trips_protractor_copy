function sharedDescribe(option) {
    describe('', function () {
        var userName = "conneelyp@ryanair.com";
        var password = "Testing123";

        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it('Then I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I navigate to my profile', function () {
            actions.myRyanairActions.clickEditProfile();
        });

        it('Then I edit Travel Document', function () {
            actions.myRyanairActions.clickProfileAddDocButton();
        });

        it('Then add document details', function () {
            actions.myRyanairActions.selectProfileDropDownNationalityDoc(option);
            actions.myRyanairActions.addProfileDropDownDocumentType(2);
            actions.myRyanairActions.addDocumentDetails();
        });

        it('Then I assert only one option for document type ', function () {
            actions.myRyanairActions.assertProfileDropDownDocumentType(2);
        });

        it('Then I add details entered ', function () {
            actions.myRyanairActions.clickBtnAddDocumentDetails();
        });

        it('Then I remove document ', function () {
            actions.myRyanairActions.selectProfileDocumentDetails();
            actions.myRyanairActions.removeProfileDocumentDetails();
        });

        it('Then I logout', function () {
            actions.fOHActions.logout();
        });
    });

};

describe('MYFR | C652459 | MYC - 315 | Login | Dashboard | Assert Travel Document Types for UK and Andorran Citizens', function () {
    sharedDescribe(31); //this is for British nationality in add document
    sharedDescribe(7); //Andorra
});


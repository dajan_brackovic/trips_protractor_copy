function sharedDescribe(option) {
    describe('', function () {
        var userName = "load8196f@ryanair.ie";
        var password = "Testing123";

        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it('Then I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I navigate to my profile', function () {
            actions.myRyanairActions.clickEditProfile();
            actions.myRyanairActions.clickProfileAddDocButton();
        });

        it('Then add document details', function () {
            actions.myRyanairActions.selectProfileDropDownNationalityDoc(option);
            actions.myRyanairActions.addProfileDropDownDocumentType(2);
            actions.myRyanairActions.addDocumentDetails();
        });

        it('Then I assert only one option for document type ', function () {
            actions.myRyanairActions.assertProfileDropDownDocumentType(2);
        });

        it('Then I add details entered ', function () {
            actions.myRyanairActions.clickBtnAddDocumentDetails();
        });

        it('Then I remove document ', function () {
            actions.myRyanairActions.selectProfileDocumentDetails();
            actions.myRyanairActions.removeProfileDocumentDetails();
        });

        it('Then I logout', function () {
            actions.fOHActions.logout();
        });
    });

};

describe('MYFR | C652461 | MYC - 318 | Login | Dashboard | Edit profile | Add travel document | Assert only one document type available for non EU citizens | Remove Document', function () {
    sharedDescribe(1);  //Afghan
    sharedDescribe(4);  //American
    sharedDescribe(10); //Argentinian
    sharedDescribe(13); //Australian
    sharedDescribe(95); //Iraqi
});

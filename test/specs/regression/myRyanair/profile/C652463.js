function sharedDescribe() {
    describe('', function () {
        var userName = "load926c7@ryanair.ie";
        var password = "Testing123";

        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it('Then I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I navigate to my profile and add document', function () {
            actions.myRyanairActions.clickEditProfile();
            actions.myRyanairActions.clickProfileAddDocButton();
        });

        it('Then add document details', function () {
            actions.myRyanairActions.selectProfileDropDownNationalityDoc(96);
            actions.myRyanairActions.addProfileDropDownDocumentType(3);
            actions.myRyanairActions.addDocumentDetails();
        });

        it('Then I assert Passport card is available for selection', function () {
            actions.myRyanairActions.assertProfileDropDownDocumentType(3);
            actions.myRyanairActions.assertProfileDropDownDocumentTypeText();
        });

        it('Then I add details entered ', function () {
            actions.myRyanairActions.clickBtnAddDocumentDetails();
        });

        it('Then I remove document ', function () {
            actions.myRyanairActions.selectProfileDocumentDetails();
            actions.myRyanairActions.removeProfileDocumentDetails();
        });

        it('Then I logout', function () {
            actions.fOHActions.logout();
        });
    });

};

describe('MYFR | C652463 | MYC - 318 | Login | Dashboard | Edit Profile | Add document and assert passport card is available for Irish nationality ', function () {
    sharedDescribe();
});

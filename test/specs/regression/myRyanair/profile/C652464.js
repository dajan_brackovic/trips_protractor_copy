function sharedDescribe() {
    describe('', function () {
        var userName = "loada2166@ryanair.ie";
        var password = "Testing123";

        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it('Then I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I navigate to my profile and add document', function () {
            actions.myRyanairActions.clickEditProfile();
            actions.myRyanairActions.selectProfileDocumentDetails();
        });

        it('Then add document details', function () {
            actions.myRyanairActions.selectRandomDropDownNationality();
            actions.myRyanairActions.editProfileDropDownDocumentType(2);
            actions.myRyanairActions.editDocumentDetails();
        });


        it('Then I add details entered ', function () {
            actions.myRyanairActions.clickBtnUpdateDocumentDetails();
        });

        it('Then I logout companion', function () {
            actions.fOHActions.logout();
        });
    });

};

describe('MYFR | C652464 | MYC - 318 | Login | Dashboard | Edit Profile | Edit Existing Document Details', function () {
    sharedDescribe();
});

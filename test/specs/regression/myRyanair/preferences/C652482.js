var Trip = require('../../../../shared/model/Trip');

function sharedDescribe() {
    var userName = "load26e08@ryanair.ie";
    var password = "Testing123";
    var myAnswer;
    var questionText = "Are you a shopper?";
    var beforeAnswer;
    var currAnswer;
    var currValue;

    describe('Validate User Preferences', function () {
        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it('When I navigate to dashboard open Preferences and click Show Advanced Preferences', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
            actions.myRyanairActions.openPreferences();
            actions.myRyanairActions.clickShowAdvancedPreferences();
        });

        it('Then Questions are displayed in list view', function () {
            actions.myRyanairActions.getPreferenceAnswer(questionText).then(function(textVal){
                beforeAnswer = textVal;
            });
            actions.myRyanairActions.clickPreferenceChangeorComplete(questionText);
        });

        it('And user can change question by selecting answer and clicking on Complete or Change link', function () {
            actions.myRyanairActions.clickPreferenceAnswer("Yes, at the airport",false);
            actions.myRyanairActions.clickPreferenceAnswer("Yes, in the duty free store",false);
            actions.myRyanairActions.clickPreferenceAnswer("Yes, inflight duty free",false);
            actions.myRyanairActions.clickPreferenceAnswer("Not a shopper",false);
            actions.myRyanairActions.clickPreferenceSaveAndClose();
            actions.myRyanairActions.getPreferenceAnswer(questionText).then(function(textVal){
                currAnswer = textVal;
            });
        });

        it('Verify previous answer and current answer', function () {
            //If we don't use the verification in seperate 'it' getting an error hence used in a seperate 'it'
            actions.myRyanairActions.verifyPreferenceAnswers(beforeAnswer, currAnswer);
            actions.myRyanairActions.getPreferenceAnswer(questionText).then(function(textVal){
                beforeAnswer = textVal;
            });
        });
    });
}

describe('MYFR | C652482 | MYC - 105 | MYC - 447 | Answer a preference question in the list view', function () {
    sharedDescribe();
});

afterAll(function () {
    actions.fOHActions.logout();
});

var Trip = require('../../../../shared/model/Trip');

function sharedDescribe() {
    var userName = "load26e08@ryanair.ie";
    var password = "Testing123";
    var currentQuestionOnHeader="";
    var currentQuestion="";

    describe('Given user selects -Planning a trip- preferences - Display first unanswered question', function () {
        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it('Given user is on Preferences', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
            actions.myRyanairActions.openPreferences();
            actions.myRyanairActions.clickShowAdvancedPreferences();
        });

        it('And he already answered several questions in (e.g. "Planning a trip" preference group)', function () {
            actions.myRyanairActions.clickPlanningATripPreference();
        });

        it('When user clicks "Edit preferences" on (e.g. "Planning a trip" preference group)', function () {
            actions.myRyanairActions.clickEditPreferences();
        });

        it('Then there is displayed first unanswered question automatically', function () {
            actions.myRyanairActions.getEditPreferencesActiveQuestionOnHeader().then(function(textVal){
                currentQuestionOnHeader = textVal;
            });

            actions.myRyanairActions.getEditPreferencesCurrentQuestion().then(function(textVal){
                currentQuestion = textVal;
            });

        });

        it('Verify Header question and current question are same', function () {
            //If we don't use the verification in seperate 'it' getting an error hence used in a seperate 'it'
            actions.myRyanairActions.verifyPreferenceQuestionsAreSame(currentQuestionOnHeader, currentQuestion);
        });

    });
}

describe('MYFR | C652486 | MYC - 448 | Display first unanswered question', function () {
    sharedDescribe();
});

afterAll(function () {
    actions.fOHActions.logout();
});

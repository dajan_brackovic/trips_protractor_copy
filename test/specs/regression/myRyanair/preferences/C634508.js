var Trip = require('../../../../shared/model/Trip');

function sharedDescribe() {
    var userName = "preferencetest@ryanair.ie";
    var password = "Password1";
    var originalAnswer1 = "Carry-on only";
    var modifiedAnswer1 = "Check-in luggage";
    var originalAnswer2 = "Purchase with Ryanair";
    var modifiedAnswer2 = "Travel without insurance";
    var yes = "Yes";
    var no = "No";


    describe('Validate User Preferences', function () {
        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it('When I navigate to dashboard and open users Upcoming Trip Preferences', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
            actions.myRyanairActions.openPreferences();
            actions.myRyanairActions.openUpcomingTripPreferences();
        });

        it('Then I assert on correct saved answers', function () {
            actions.myRyanairActions.validateSavedPreference(0, originalAnswer1);
            actions.myRyanairActions.validateSavedPreference(1, originalAnswer2);
            actions.myRyanairActions.validateSavedPreference(2, yes);
            actions.myRyanairActions.validateSavedPreference(3, yes);
        });

        it('When I modify all answers and logout', function () {
            actions.myRyanairActions.modifyUpcomingTripPreferences();
            actions.fOHActions.logout();
        });

        it('And I log-in again and re-open Upcoming Trip preferences tab', function () {
            actions.fOHActions.login(userName, password);
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
            actions.myRyanairActions.openPreferences();
            actions.myRyanairActions.openUpcomingTripPreferences();
        });

        it('Then I assert on correct modified answers', function () {
            actions.myRyanairActions.validateSavedPreference(0, modifiedAnswer1);
            actions.myRyanairActions.validateSavedPreference(1, modifiedAnswer2);
            actions.myRyanairActions.validateSavedPreference(2, no);
            actions.myRyanairActions.validateSavedPreference(3, no);
        });

        it('Then I revert preferences to original answers', function () {
            actions.myRyanairActions.revertUpcomingTripPreferences();
        });

    });
}

describe('MYFR | C634508 | Enter and Validate saved User Preferences', function () {
    sharedDescribe();

});

afterAll(function () {
    actions.fOHActions.logout();
});

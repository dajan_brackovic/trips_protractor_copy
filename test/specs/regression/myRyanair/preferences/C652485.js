var Trip = require('../../../../shared/model/Trip');

function sharedDescribe() {
    var userName = "load26e08@ryanair.ie";
    var password = "Testing123";
    var questionsCompleteMessage = "preferences complete.";

    describe('Validate User Preferences', function () {
        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it('When user clicks Edit preferences', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
            actions.myRyanairActions.openPreferences();
            actions.myRyanairActions.clickHolidayEditPreferences();
        });

        it('Then user can answer basic Holiday questions in this group', function () {
            actions.myRyanairActions.clickPreferenceAnswer("Yes",false);
            actions.myRyanairActions.clickPreferenceStar(0);//Clicks any random star
            actions.myRyanairActions.clickNextPreference();
            actions.myRyanairActions.clickPreferenceAnswer("Yes",false);
            actions.myRyanairActions.clickPreferenceAnswer("When booking with Ryanair");
        });

        it('Verify previous answer and current answer', function () {
            actions.myRyanairActions.validatePreferencesHolidayQuestionsComplete(questionsCompleteMessage);
        });
    });
}

describe('MYFR | C652485 | MYC - 450  | Edit Holiday preferences (edit)', function () {
    sharedDescribe();
});

afterAll(function () {
    actions.fOHActions.logout();
});

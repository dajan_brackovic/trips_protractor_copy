var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;


function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE"); //today index === 1
    var origin = "stn";
    var destination = actions.tripHelper.getRandomAirportFromArray();
    var fareType = "standard";
    var tripWay = "oneway";
    var bookingRefActiveTrip;
    var userName = "load0dc4c@ryanair.ie";
    var password = "Testing123";
    var trip;

    var bookFlight = function (paxMap) {
        trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
        actions.fOHActions.goToPage();
        actions.fOHActions.login(userName, password);
        actions.fOHActions.searchOneWayFLightWithPaxMyFr(paxMap, origin, destination, outBoundDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        actions.extrasActions.skipExtras();
    };

    describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

        it(sprintf('Given I login to my ryanair and I make a standard oneWay trip with 1 < outbound < 7 days from now with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
            var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
            bookFlight(paxMap);
        });

        it('Then I add saved details in passenger details and contact section', function () {
            actions.addPaxActions.addSavedPaxNameMyRyanair(trip.journey.paxList);
        });

        it('Then I assert on contact details', function () {
            actions.addPaxActions.assertOnSavedEmailAddress(userName);
            actions.addPaxActions.assertOnSavedPhoneNumber();
        });

        it('Then I edit contact details', function () {
            actions.addPaxActions.clickChangeEmailLink();
            actions.addPaxActions.editEmailAddress();
            actions.addPaxActions.clickAddNewNumberLink();
            actions.addPaxActions.editPhoneNumber();
        });

        it('Then I pay for booking', function () {
            actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
        });
    });

    afterAll(function () {
        actions.fOHActions.logout();
    });

}

describe('MYFR | C652377 | MYC - 970 | Display saved contact details and Edit on Payment Page', function () {

    sharedDescribe(1, 0, 0, 0);

});
var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("PARTIAL"); //today index === 1
    var origin = "stn";
    var destination = "dub";
    var fareType = "standard";
    var tripWay = "oneway";
    var userName = "loadc4029@ryanair.ie";
    var password = "Testing123";
    var totalPax = adultNumber + teenNumber + childrenNumber + infantsNumber;
    var trip;

    var bookFlight = function (paxMap) {
        trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
        actions.fOHActions.goToPage();
        actions.fOHActions.login(userName, password);
        actions.fOHActions.searchOneWayFLightWithPaxMyFr(paxMap, origin, destination, outBoundDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        actions.extrasActions.skipExtras();
    };

    describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

        it(sprintf('Given I login to my ryanair and I make a standard oneWay trip with 1 < outbound < 7 days from now with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
            var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
            bookFlight(paxMap);
        });

        it('Then I add saved details in passenger details', function () {
            actions.addPaxActions.addSavedPaxNameMyRyanair(trip.journey.paxList);
        });

        it('Then I edit saved  passenger details', function () {
            actions.addPaxActions.editLastSavedPaxNameMyRyanair(totalPax);
        });

        it('Then I pay with saved card', function () {
            actions.addPaxActions.makeCardPaymentMyRyanairSavedCard(trip.bookingContact.card);
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
        });
    });

    afterAll(function () {
        actions.fOHActions.logout();
    });

}

describe('MYFR | C641323  | Retrieve passenger details from My Companions and Edit | MYC - 966 | LogOut', function () {

    sharedDescribe(4, 0, 0, 0);

});
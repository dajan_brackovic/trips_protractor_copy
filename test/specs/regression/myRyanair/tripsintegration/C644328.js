var Trip = require('../../../../shared/model/Trip');


function sharedDescribe() {

    var userName = "omahonyj@ryanair.com";
    var password = "Password1";

    describe('My Fr Login from Manage Trip', function () {
        it('Given I navigate to environment', function () {
            actions.fOHActions.goToPage();
        });

        it("When I click on manage trips tab on FOH page", function () {
            actions.manageTripsActions.clickManageTripDropDown();
        });

        it('Then I Log-in from manage trip', function () {
            actions.fOHActions.enterUsername(userName);
            actions.fOHActions.enterPassword(password);
            actions.fOHActions.clickLoginBtn();
        });

        it('Then I make sure Manage trip page is loaded', function () {
            actions.manageTripsActions.assertOnManageMyTripContent();
        });

    });
}

describe('MYFR | C644328 | MYC - 1493 | MYC - 1497 | Log-in from myTrips login form', function () {
    sharedDescribe();

});

afterAll(function () {
    actions.fOHActions.logout();
});

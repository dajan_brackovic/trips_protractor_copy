var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {

    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("PARTIAL"); //today index === 1
    var origin = "stn";
    var destination = "Dublin";
    var fareType = "standard";
    var tripWay = "oneway";
    var userName = "load12e62@ryanair.ie";
    var password = "Testing123";
    var trip;

    var bookFlight = function (paxMap) {
        trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
        actions.fOHActions.goToPage();
        actions.fOHActions.login(userName, password);
        actions.fOHActions.searchOneWayFLightWithPaxMyFr(paxMap, origin, destination, outBoundDaysFromNow);
        actions.tripsHomeActions.selectAFlightOnly(0, 0, fareType, tripWay);
    };

    describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

        it(sprintf('Given I login to my ryanair and I make a standard oneWay trip with 1 < outbound < 7 days from now with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
            var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
            bookFlight(paxMap);
        });

        it('Then I click on save trip icon', function () {
            actions.tripsHomeActions.assertOnColoredSavedTripIcon(false);
            actions.tripsHomeActions.clickBtnSaveTrip();
            actions.tripsHomeActions.assertOnColoredSavedTripIcon(true);
        });

        it("Then I click on manage trips tab on FOH page", function () {
            actions.bookingSummaryActions.clickRyanairLogo();    // This action is happening on flight select page
            actions.manageTripsActions.clickManageTripDropDown();
        });

        it("Then I assert on saved trips", function () {
            actions.manageTripsActions.assertOnSavedTripCard(true, 1);
        });

        it("Then I click on saved trip card", function () {
            actions.manageTripsActions.clickOnSavedTripDestination();
        });

        it("Then I skip extras page", function () {
            actions.extrasActions.xOutReserveSeatPopUp();
            browser.sleep(2000);
            actions.extrasActions.skipExtras();
        });

        it('Then I add saved details in passenger details, contact and pay for booking', function () {
            actions.addPaxActions.addSavedPaxNameMyRyanair(trip.journey.paxList);
            actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
        });

        it("Then I click on manage trips tab on FOH page", function () {
            actions.bookingSummaryActions.clickRyanairLogo();
            actions.manageTripsActions.clickManageTripDropDown();
        });

        it("Then I assert that saved trip card is empty", function () {
            // This is failing due to bug PRP-1037
            actions.manageTripsActions.assertSavedTripCardIsEmpty();
        });

        it("Then I assert on upcoming trips card", function () {
            actions.manageTripsActions.assertOnUpcomingTripCardIsPresent(destination);
        });

    });

    afterAll(function () {
        actions.fOHActions.logout();
    });

}

describe('MYFR | C644439 | MYC - 1564 | MYC - 1767 | Saved trip card becomes upcoming trip card', function () {

    sharedDescribe(1, 0, 0, 0);

});


var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE"); //today index === 1
    var origin = "Dub";
    var destination = "Bud";
    var fareType = "standard";
    var tripWay = "oneway";
    var bookingRefActiveTrip;
    var userName = "khuranaa@ryanair.com";
    var password = "Password1";
    var totalPax = adultNumber + teenNumber + childrenNumber + infantsNumber;
    var trip;

    var bookFlight = function (paxMap) {
        trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
        actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        actions.extrasActions.skipExtras();
    };

    describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

        it(sprintf('Given I login to my ryanair and I make a standard oneWay trip with 1 < outbound < 7 days from now with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
            var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
            bookFlight(paxMap);
        });

        it('When I Am on Payment Page I logIn to My Ryanair', function () {
            actions.addPaxActions.clickLogInMyFr(userName, password);

        });

        it('Then I add saved details in passenger details and contact section', function () {
            actions.addPaxActions.addSavedPaxNameMyRyanair(trip.journey.paxList);
        });

        it('Then I pay with saved card', function () {
            actions.addPaxActions.makeCardPaymentMyRyanairSavedCard(trip.bookingContact.card);
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
            bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
        });

        it('When I click checkIn button', function () {
            actions.bookingSummaryActions.clickCheckInButton();
        });

        it('Then I fill Check In id documents', function () {
            actions.checkInActions.selectAllPax();
            actions.checkInActions.clickContinueBtnForMyRyanairSavedPax(totalPax);
        });

        it('Then I complete second step of check-in', function () {
            actions.checkInActions.clickBtnContinueCheckIn();
        });

        it('Then I get Boarding Pass Ref', function () {
            actions.checkInActions.assertOnBookingRef(bookingRefActiveTrip);
        });

        it('Then I get auto allocated seat', function () {
            actions.checkInActions.assertOnAutoAllocation(tripWay, totalPax);
            actions.checkInActions.closeBoardingPassWindow();
        });
    });

    afterAll(function () {
        actions.fOHActions.logout();
    });

}

describe('MYFR | C632719 | LogIn On Payment Page and Pay with Saved Card | CheckIn | LogOut', function () {

    sharedDescribe(2, 0, 0, 0);

});
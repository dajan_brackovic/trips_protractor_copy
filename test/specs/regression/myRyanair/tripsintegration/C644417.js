var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;


function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
    var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE"); //today index === 1
    var origin = "stn";
    var destination = actions.tripHelper.getRandomAirportFromArray();
    var fareType = "standard";
    var tripWay = "oneway";
    var bookingRefActiveTrip;
    var userName = "load56b9c@ryanair.ie";
    var password = "Testing123";
    var totalPax = adultNumber + teenNumber + childrenNumber + infantsNumber;
    var trip;

    var bookFlight = function (paxMap) {
        trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
        actions.fOHActions.goToPage();
        actions.fOHActions.login(userName, password);
        actions.fOHActions.searchOneWayFLightWithPaxMyFr(paxMap, origin, destination, outBoundDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        actions.extrasActions.skipExtras();
    };

    describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

        it(sprintf('Given I login to my ryanair and I make a standard oneWay trip with 1 < outbound < 7 days from now with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
            var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
            bookFlight(paxMap);
        });

        it('Then I add saved details in passenger details and contact section', function () {
            actions.addPaxActions.addSavedPaxNameMyRyanair(trip.journey.paxList);
        });

        it('Then I pay for booking', function () {
            actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
            bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
        });

        it('When I click checkIn button', function () {
            actions.bookingSummaryActions.clickCheckInButton();
        });

        it('Then I fill Check In id documents', function () {
            actions.checkInActions.selectAllPax();
            actions.checkInActions.addDocsForMultiPax(adultNumber, teenNumber, childrenNumber, infantsNumber, 3);
        });

        it('Then I complete second step of check-in', function () {
            actions.checkInActions.clickBtnContinueCheckIn();
        });

        it('Then I get Boarding Pass Ref', function () {
            actions.checkInActions.assertOnBookingRef(bookingRefActiveTrip);
        });

        it('Then I get auto allocated seat', function () {
            actions.checkInActions.assertOnAutoAllocation(tripWay, totalPax);
            actions.checkInActions.closeBoardingPassWindow();
        });
    });

    afterAll(function () {
        actions.fOHActions.logout();
    });

}

describe('MYFR | C644417 | MYC - 969 | Display ID document screen when there is no saved travel document on Check-In', function () {

    sharedDescribe(3, 0, 0, 0);

});

function sharedDescribe() {
    describe('MYFR LOGIN DOC', function () {
        it('Given I login to my ryanair', function () {
            var userName = "omahonyj@ryanair.com";
            var password = "Password1";
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it("Then I click on manage trips tab on FOH page", function () {
            actions.manageTripsActions.clickManageTripDropDown();
        });

        it("Then I assert on upcoming trips card", function () {
            actions.manageTripsActions.assertOnUpcomingTripsCards();
        });

        it("Then after clicking on upcoming trip card active trip page shows up", function () {
            actions.manageTripsActions.clickOnUpcomingTripsCardImage();
            actions.bookingSummaryActions.assertOnActiveTripMainImage();
        });
    });
}

describe('MYFR | C636233 | Display upcoming Trip Card | MYC - 1497 | MYC-1770 | SmokeIntegration Tests', function () {
    sharedDescribe();

});

afterAll(function () {
    actions.fOHActions.logout();
});
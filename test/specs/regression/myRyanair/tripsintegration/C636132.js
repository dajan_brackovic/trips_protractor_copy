var Trip = require('../../../../shared/model/Trip');

function sharedDescribe() {

    var origin = "dub";
    var destination;
    var outBoundDaysFromNow = 3;//today index === 1
    var returnDaysFromNow;
    var paxMap = {ADT: 1, TEEN: 0, CHD: 0, INF: 0};
    var fareType = "standard";
    var tripWay = "oneway";

    describe('MYFR LOGIN DOC', function () {
        it('Given I login to my ryanair', function () {
            var userName = "load58daf@ryanair.ie";
            var password = "Testing123";
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it('Then I make a standard one way trip with outbound < 7 Days from now', function () {
            destination = "krk";
            actions.fOHActions.searchOneWayFLightWithPaxMyFr(paxMap, origin, destination, outBoundDaysFromNow);
        });

        it('Then I select flight', function () {
            actions.tripsHomeActions.selectAFlightOnly(0, 0, fareType, tripWay);
        });

        it('Then I click on save trip icon', function () {
            actions.tripsHomeActions.clickBtnSaveTrip();
        });

        it('Then I click on Ryanair Home Page logo on flight select page', function () {
            actions.bookingSummaryActions.clickRyanairLogo();    // This action is happening on flight select page
        });

        it("Then I click on manage trips tab on FOH page", function () {
            actions.manageTripsActions.clickManageTripDropDown();
        });

        it("Then I assert on saved trips", function () {
            actions.manageTripsActions.assertOnSavedTripCard(true, 1);
        });

        it('Then I click on Ryanair Home Page logo', function () {
            actions.bookingSummaryActions.clickRyanairLogo();    // This action is happening on My trips page
        });

        it('Then I make a standard Rt trip with outbound < 7 and inbound < 7 Days from now', function () {
            destination = "STN";
            tripWay = "twoway";
            returnDaysFromNow = 7;
            actions.fOHActions.goToPage();
            actions.fOHActions.searchReturnFLightWithPaxMyFr(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlightOnly(0, 0, fareType, tripWay);
        });

        it('Then I click on save trip icon', function () {
            actions.tripsHomeActions.clickBtnSaveTrip();
        });

        it('Then I click on Ryanair Home Page logo on flight select page', function () {
            actions.bookingSummaryActions.clickRyanairLogo();    // This action is happening on flight select page
        });

        it("Then I click on manage trips tab on FOH page", function () {
            actions.manageTripsActions.clickManageTripDropDown();
        });

        it("Then I assert on saved trips", function () {
            actions.manageTripsActions.assertOnSavedTripCard(true, 2);
        });

        it("Then I assert on recent saved trips is displayed first", function () {
            actions.manageTripsActions.assertOnSavedTripDestination(destination);
        });

        it("Then I assert saved trips has departure destination date and price", function () {
            actions.manageTripsActions.assertOnSavedTripContent(true);
        });

        it("Then I remove saved trips", function () {
            actions.manageTripsActions.clickBtnRemoveSavedTrip();
        });

    });
}

describe('MYFR | C636132 | Sort Saved Trip Cards | MYC - 1697  MYC - 1764 SmokeIntegration Tests', function () {
    sharedDescribe();

});

afterAll(function () {
    actions.fOHActions.logout();
});
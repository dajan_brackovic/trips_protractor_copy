var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;

function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
    var outBoundDaysFromNow = actions.tripHelper.getRandomHoldFareDate(); //today index === 1
    var returnDaysFromNow = actions.tripHelper.getRandomDateGreaterThanOutbound("PARTIAL", outBoundDaysFromNow); //today index === 1
    var origin = "Stn";
    var destination = "Dub";
    var fareType = "standard";
    var tripWay = "twoway";
    var userName = "loada002f@ryanair.ie";
    var password = "Testing123";
    var bookingRefActiveTrip;
    var bookingRefHoldFare;
    var trip;

    var bookFlight = function (paxMap) {
        trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
        actions.fOHActions.goToPage();
        actions.fOHActions.login(userName, password);
        actions.fOHActions.searchReturnFLightWithPaxMyFr(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
    };

    describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

        it(sprintf('Given I login to my ryanair and I make a standard oneWay trip with 1 < outbound < 7 days from now with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
            var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
            bookFlight(paxMap);
        });


        it('When on Flight Select Page I verify that Hold Fare is disabled until after selecting flights ', function () {
            actions.tripsHomeActions.assertOnBtnHoldFareisNotPresent();
            actions.tripsHomeActions.selectAFlightOnly(0, 0, fareType, tripWay);
            actions.tripsHomeActions.assertOnBtnHoldFareisPresent();
        });

        it('And I select Hold Fare and Confirm selection', function () {
            actions.tripsHomeActions.clickBtnHoldFare();
            actions.tripsHomeActions.assertOnHoldFareDrowerTotal(tripWay);
            actions.holdFareActions.clickBtnConfirmHoldFare();
        });

        it('And I pay Hold Fare fee', function () {
            actions.addPaxActions.addSavedPaxNameMyRyanair(trip.journey.paxList);
            actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I get a booking Ref for Hold Fare', function () {
            actions.bookingSummaryActions.verifyConfirmationMessageOnHoldFare();
            bookingRefHoldFare = actions.bookingSummaryActions.returnHoldFare();
        });

        it('Then I make sure that products cards disabled', function () {
            actions.bookingSummaryActions.assertOnAddSeatButtonNotPresent();
            actions.bookingSummaryActions.assertOnAddBagButtonNotPresent();
        });

        it('When on Manage Trip Page I click on Ryanair Home Page logo to return to home page', function () {
            actions.bookingSummaryActions.clickRyanairLogo();
        });

        it("Then I click on manage trips tab on FOH page", function () {
            actions.manageTripsActions.clickManageTripDropDown();
        });

        it("Then I assert that Hold Fare is present in upcoming trip", function () {
            actions.manageTripsActions.assertOnHeldFareCard(1);
        });

        it("Then I assert time on Hold Fare Card", function () {
            actions.manageTripsActions.assertOnHeldFareTimeLeft(true);
        });

        it("Then I click on held fare card", function () {
            actions.manageTripsActions.clickHeldFareCard();
        });

        it('When I click checkout button', function () {
            actions.bookingSummaryActions.clickCheckOutButton();
        });

        it('Then I should pay for the fare', function () {
            actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
            actions.addPaxActions.enterBillingAddress();
            actions.addPaxActions.clickPaymentContinue();
        });

        it('Then I should get a booking ref', function () {
            actions.bookingSummaryActions.verifyConfirmationMessage();
            bookingRefActiveTrip = actions.bookingSummaryActions.returnPnr();
        });

        it('Then I assert on PNR', function () {
            actions.bookingSummaryActions.assertOnHoldFarePnr(bookingRefHoldFare, bookingRefActiveTrip);
        });

        it("Then I click on manage trips tab on active trip page", function () {
            actions.manageTripsActions.clickManageTripDropDown();
        });

        it("Then I assert that Hold Fare is not present in upcoming trip", function () {
            actions.manageTripsActions.assertOnHeldFareCard(0);
        });

    });

    afterAll(function () {
        actions.fOHActions.logout();
    });

}

describe('MYFR | C643660 | Pay For Held Fare | MYC - 2237  MYC - 1492  MYC - 1968', function () {

    sharedDescribe(1, 0, 0, 0);

});
var Trip = require('../../../../shared/model/Trip');
var sprintf = require("sprintf").sprintf;


function sharedDescribe(adultNumber, teenNumber, childrenNumber, infantsNumber) {
        var outBoundDaysFromNow = actions.tripHelper.getRandomDate("NONE");//today index === 1
        var returnDaysFromNow = 22; //today index === 1
        var origin = "dub";
        var destination = "lgw";
        var fareType = "business";
        var tripWay = "twoway";
        var trip;
        var userName = "omahonyj@ryanair.com";
        var password = "Password1";

        var bookFlight = function (paxMap) {
            trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName,password);
            actions.fOHActions.searchReturnFLightWithPaxMyFr(paxMap, origin, destination, outBoundDaysFromNow, returnDaysFromNow);
            actions.tripsHomeActions.selectAFlight(0, 0, fareType,tripWay);
        };

        describe(sprintf('%s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {

            it(sprintf('Given I make a business return trip with outbound > 1 < 7 Days and return=30days from now with %s adult, %s teen, %s children, %s infants', adultNumber, teenNumber, childrenNumber, infantsNumber), function () {
                // TODO MAKE BOOKING TO SEATS
                var paxMap = {ADT: adultNumber, TEEN: teenNumber, CHD: childrenNumber, INF: infantsNumber};
                bookFlight(paxMap);
            });

            it('When I select a seat', function () {
                actions.seatsActions.selectMultipleSeats(tripWay,  adultNumber, teenNumber, childrenNumber, infantsNumber);
            });

            it('Then I select a same seat for return', function () {
                actions.seatsActions.selectMultiSameSeatReturn( adultNumber, teenNumber, childrenNumber, infantsNumber)
            });

            it('Then I should confirm business seat price', function () {
                actions.seatsActions.confirmBusinessSeatPrice();
            });

            it('Then I should pay for booking', function () {
                actions.addPaxActions.addPaxNameForAllPAX(trip.journey.paxList);
                actions.addPaxActions.makeCardPayment(trip.bookingContact.card);
                actions.addPaxActions.enterBillingAddress();
                actions.addPaxActions.clickPaymentContinue();
            });

            it('Then I should get a booking ref', function () {
                // TODO expect booking ref
                actions.bookingSummaryActions.verifyConfirmationMessage();
            });

            it('When I do checkIN', function () {
                // TODO Make a Simple Check IN
                actions.checkInActions.todoSimpleCeckIn();
            });

            it('Then I verify check IN', function () {
                // TODO expect check IN
                actions.checkInActions.verifySimpleCheckIn();
            });
        });

        afterAll(function(){
            actions.fOHActions.logout();
        });

}

describe('MYFR | Simple Booking | C601956 |  OB > 1 < 7 Days | Return = 30Days | Business', function () {

    sharedDescribe(1, 0, 0, 0);

    //sharedDescribe(1, 1, 0, 1);
    //
    //sharedDescribe(1, 0, 1, 1);

});
var Trip = require('../../../../shared/model/Trip');
//C652438 MYC-347 Verify Dashboard elements when basic data added
var specId = "|myRyanair|dashboard| C652437 ";
//This test case also covers C652439

function sharedDescribe() {
    var userName = actions.tripHelper.getRandomEmail();
    var title = "Mr";
    var password = "Password1";
    var FirstName = "Myryanair";
    var LastName = "Maytest";
    var dayIndex = 10;
    var monthIndex = 2;
    var yearIndex = 27;
    var nationality = "Irish";


    describe('PROFILE QUESTION PROGRESS REWARDS CARDS', function () {
        it('Given I am a new user with phone number and travel document added', function () {
            actions.fOHActions.goToPage();
            actions.myFrSignupHelper.createNewUser(userName, password);
            actions.fOHActions.login(userName, password);
        });

        it('When I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
            browser.sleep(5000);
            actions.myRyanairActions.verifyProfileProgressInPercentage("10%");
        });

        it('Add names, nationalities and birth date', function () {
            actions.myRyanairActions.selectDropDownTitle(title);
            actions.myRyanairActions.inputFieldFirstName(FirstName);
            actions.myRyanairActions.inputFieldLastName(LastName);
            actions.myRyanairActions.clickSaveUserInfoContinue();
            actions.myRyanairActions.selectDropDownNationality(nationality);
            actions.myRyanairActions.clickSaveUserInfoContinue();
            actions.myRyanairActions.selectDropDownDateOfBirthDay(dayIndex);
            actions.myRyanairActions.selectDropDownDateOfBirthMonth(monthIndex);
            actions.myRyanairActions.selectDropDownDateOfBirthYear(yearIndex);
            actions.myRyanairActions.clickSaveUserInfoContinue();

        });


        it('Verify checklist before phone number is filled', function () {
            actions.myRyanairActions.verifyCheckListStatusKeyDetails(1);
            actions.myRyanairActions.verifyCheckListStatusPhoneNumber(0);
            actions.myRyanairActions.verifyCheckListStatusTravelDocument(0);
            actions.myRyanairActions.verifyProfileProgressInPercentage("40%");
        });

        it('Keep adding profile by adding phone number', function () {
            actions.myRyanairActions.keepBuildProfileByAddingPhone();
        });

        it('Verify checklist after phone number is filled', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
            actions.myRyanairActions.verifyCheckListStatusKeyDetails(1);
            actions.myRyanairActions.verifyCheckListStatusPhoneNumber(1);
            actions.myRyanairActions.verifyCheckListStatusTravelDocument(0);
            actions.myRyanairActions.verifyProfileProgressInPercentage("45%");
        });

        it('Then I add Travel Documents', function () {
            actions.myRyanairActions.addTravelDocumentFromDashboard();
            actions.myRyanairActions.profileSelectDropDownNationalityDoc(5);
            actions.myRyanairActions.profileSelectDropDownDocumentType(1);
            actions.myRyanairActions.profileInputRandomDocumentNumber();
            actions.myRyanairActions.profileSelectDocumentExpiryDate();
            actions.myRyanairActions.profileSelectDropDownCountryOfIssue(5);
            actions.myRyanairActions.profileClickAddButton();
            actions.myRyanairActions.clickDashboardTab();
            actions.myRyanairActions.verifyProfileProgressInPercentage("50%");

        });

        it('Then I logout', function () {
            actions.fOHActions.logout();
        });

    });
}

describe(specId+' | MYC 346 | Verify Dashboard Wizard state: Title, Name, Lastname', function () {
    sharedDescribe();

});

var Trip = require('../../../../shared/model/Trip');
var specId = "|myRyanair|dashboard| C652434  ";

function sharedDescribe() {
    var userName = actions.tripHelper.getRandomEmail();
    var password = "Password1";

    describe('NEW USER COMPLETE ALL PROFILE QUESTIONS', function () {
        it('Given I am a new user and log into the system', function () {
            actions.fOHActions.goToPage();
            actions.myFrSignupHelper.createNewUser(userName, password);
            browser.sleep(5000);
            actions.myFrSignupHelper.activateUser(userName, password);
            actions.fOHActions.login(userName, password);

        });

        it('When I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I start to answer all questions, verify the questions complete answered shown ', function () {
            actions.myRyanairActions.answerAllQuestions();
        });

        it('message is configurable from AEM', function () {
            actions.myRyanairActions.openPreferences();
            actions.myRyanairActions.verifyPreferenceAfterAnsweringQuestions();
        });

        it('Rewards section is unlocked on Preferences page and in left menu', function () {
            actions.myRyanairActions.clickUnlockedRewardCardFromPreference();
        });

        it('Then I logout', function () {
            actions.fOHActions.logout();
        });

    });
}

describe(specId+' | MYC 452 | Answering profile questions on Dashboard', function () {
    sharedDescribe();

});

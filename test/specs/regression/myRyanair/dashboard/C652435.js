var Trip = require('../../../../shared/model/Trip');
var specId = "|myRyanair|dashboard| C652435 ";

function sharedDescribe() {
    var userName = actions.tripHelper.getRandomEmail();
    var password = "Password1";

    describe('PROFILE QUESTION PROGRESS REWARDS CARDS', function () {
        it('Given I am a new user with phone number and travel document added', function () {
            actions.fOHActions.goToPage();
            actions.myFrSignupHelper.createNewUser(userName, password);
            browser.sleep(5000);
            actions.myFrSignupHelper.activateUser(userName, password);
            actions.fOHActions.login(userName, password);
        });

        it('When I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Verify that 4 cards PROFILE QUESTION PROGRESS REWARDS shown', function () {
            actions.myRyanairActions.verify4CardsInDashboard();
        });

        it('Then I logout', function () {
            actions.fOHActions.logout();
        });
    });
}

describe(specId+' | MYC 348 | Display Dashboard on profile completeness', function () {
    sharedDescribe();

});

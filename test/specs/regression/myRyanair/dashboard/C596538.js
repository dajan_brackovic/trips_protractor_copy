var Trip = require('../../../../shared/model/Trip');

function sharedDescribe() {
    var userName = actions.tripHelper.getRandomEmail();
    var password = "Password1";

    describe('MYFR LOGIN DOC', function () {
        it('Given I login to my ryanair', function () {
            actions.fOHActions.goToPage();
            actions.myFrSignupHelper.createNewUser(userName, password);
            browser.sleep(2000);
            actions.myFrSignupHelper.activateUser(userName, password);
            actions.fOHActions.goToPage();
            actions.fOHActions.login(userName, password);
        });

        it('When I navigate to dashboard', function () {
            actions.myRyanairActions.clickUserName();
            actions.myRyanairActions.clickLinkDashboard();
        });

        it('Then I assert on my ryanair page tabs', function () {
            actions.myRyanairActions.assertOnTabs();
        });

        it('Then I remove documents in passengers tab', function () {
            actions.myRyanairActions.clickPassengersTab();
            actions.myRyanairActions.clickBtnShowPassportDetails();
            actions.myRyanairActions.clickBtnRemove();
        });

        it('Then I add documents in passengers tab', function () {
            actions.myRyanairActions.clickPassengersTab();
            actions.myRyanairActions.clickAddDocumentBtn();
            actions.myRyanairActions.selectDropDownNationality();
            actions.myRyanairActions.addProfileDropDownDocumentType(2);
            actions.myRyanairActions.addDocumentDetails();
            actions.myRyanairActions.clickBtnAddDocumentDetails();
        });

        it('Then I make sure that myRyanair progress percentage is edited', function () {
            actions.myRyanairActions.clickDashboardTab();
            var percentage = 50;
            actions.myRyanairActions.assertOnMyRyanairProgressinPercentage(percentage);
        });

        it('Then I remove documents in passengers tab', function () {
            actions.myRyanairActions.clickPassengersTab();
            actions.myRyanairActions.clickBtnShowPassportDetails();
            actions.myRyanairActions.clickBtnRemove();
        });

        it('Then I make sure that myryanair progress percentage is edited', function () {
            actions.myRyanairActions.clickDashboardTab();
            var percentage = 45;
            actions.myRyanairActions.assertOnMyRyanairProgressinPercentage(percentage);
        });

        it('Then I logout', function () {
            actions.fOHActions.logout();
        });
    });
}

describe('MYFR | C596538 | Login | Dashboard | Add Document | Remove Document', function () {
    sharedDescribe();

});

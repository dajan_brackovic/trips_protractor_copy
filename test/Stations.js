var stationsData = [
        {
            "code": "AAR",
            "name": "Aarhus",
            "countryCode": "DK",
            "countryName": "Denmark",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "DK",
            "latitude": "561837N",
            "longitude": "0103705E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ALC"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "AGA",
            "name": "Agadir Al Massria Airport ",
            "countryCode": "MA",
            "countryName": "Morocco",
            "countryGroupCode": "1",
            "countryGroupName": "non EU/EEA",
            "timeZoneCode": "MA",
            "latitude": "302250N",
            "longitude": "0093245W",
            "mobileBoardingPass": false,
            "markets": [
                {
                    "code": "CRL"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "AHO",
            "name": "Alghero",
            "countryCode": "IT",
            "countryName": "Italy",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IT",
            "latitude": "403750N",
            "longitude": "0081720E",
            "mobileBoardingPass": false,
            "markets": [
                {
                    "code": "AOI"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BTS"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "CUF"
                },
                {
                    "code": "DTM"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "FMM"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "GSE"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TRN"
                },
                {
                    "code": "TRS"
                },
                {
                    "code": "TSF"
                }
            ],
            "notices": null
        },
        {
            "code": "ALC",
            "name": "Alicante",
            "countryCode": "ES",
            "countryName": "Spain",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "ES1",
            "latitude": "381700N",
            "longitude": "0003400W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AAR"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BHX"
                },
                {
                    "code": "BLL"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BOH"
                },
                {
                    "code": "BRE"
                },
                {
                    "code": "BRS"
                },
                {
                    "code": "BRU"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CGN"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "FKB"
                },
                {
                    "code": "FMM"
                },
                {
                    "code": "GDN"
                },
                {
                    "code": "GSE"
                },
                {
                    "code": "HAU"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "KIR"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "KTW"
                },
                {
                    "code": "KUN"
                },
                {
                    "code": "LBA"
                },
                {
                    "code": "LDY"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "MST"
                },
                {
                    "code": "NOC"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "NUE"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "ORK"
                },
                {
                    "code": "PIK"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "SCQ"
                },
                {
                    "code": "SNN"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TMP"
                },
                {
                    "code": "TRF"
                },
                {
                    "code": "VST"
                },
                {
                    "code": "VXO"
                },
                {
                    "code": "WMI"
                },
                {
                    "code": "WRO"
                }
            ],
            "notices": null
        },
        {
            "code": "LEI",
            "name": "Almeria",
            "countryCode": "ES",
            "countryName": "Spain",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "ES1",
            "latitude": "365040N",
            "longitude": "0022220W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "AOI",
            "name": "Ancona",
            "countryCode": "IT",
            "countryName": "Italy",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IT",
            "latitude": "433700N",
            "longitude": "0133100E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AHO"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TPS"
                }
            ],
            "notices": null
        },
        {
            "code": "ATH",
            "name": "Athens",
            "countryCode": "GR",
            "countryName": "Greece",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "GR",
            "latitude": "375611N",
            "longitude": "0235640E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BGY"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "PFO"
                },
                {
                    "code": "RHO"
                },
                {
                    "code": "SKG"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "WMI"
                }
            ],
            "notices": null
        },
        {
            "code": "BCN",
            "name": "Barcelona El Prat",
            "countryCode": "ES",
            "countryName": "Spain",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "ES1",
            "latitude": "411749N",
            "longitude": "0020442E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ACE"
                },
                {
                    "code": "AGP"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BHX"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BRU"
                },
                {
                    "code": "BUD"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CGN"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "FCO"
                },
                {
                    "code": "FEZ"
                },
                {
                    "code": "FUE"
                },
                {
                    "code": "IBZ"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "MAH"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "NDR"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "PIK"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "SCQ"
                },
                {
                    "code": "SDR"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "SVQ"
                },
                {
                    "code": "TFN"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TRN"
                },
                {
                    "code": "TSF"
                },
                {
                    "code": "VLL"
                },
                {
                    "code": "VNO"
                },
                {
                    "code": "WMI"
                },
                {
                    "code": "XRY"
                }
            ],
            "notices": null
        },
        {
            "code": "GRO",
            "name": "Barcelona Girona",
            "countryCode": "ES",
            "countryName": "Spain",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "ES1",
            "latitude": "415403N",
            "longitude": "0024538E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AAR"
                },
                {
                    "code": "AHO"
                },
                {
                    "code": "BDS"
                },
                {
                    "code": "BLL"
                },
                {
                    "code": "BOH"
                },
                {
                    "code": "BRE"
                },
                {
                    "code": "BRS"
                },
                {
                    "code": "BTS"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CAG"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DTM"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "FEZ"
                },
                {
                    "code": "FKB"
                },
                {
                    "code": "FMM"
                },
                {
                    "code": "GSE"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "LPP"
                },
                {
                    "code": "LTN"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "MST"
                },
                {
                    "code": "NDR"
                },
                {
                    "code": "NOC"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "ORK"
                },
                {
                    "code": "PEG"
                },
                {
                    "code": "PIK"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "POZ"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "PSR"
                },
                {
                    "code": "RAK"
                },
                {
                    "code": "RBA"
                },
                {
                    "code": "SFT"
                },
                {
                    "code": "SKG"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TLL"
                },
                {
                    "code": "TPS"
                },
                {
                    "code": "VNO"
                },
                {
                    "code": "WRO"
                }
            ],
            "notices": null
        },
        {
            "code": "REU",
            "name": "Barcelona Reus",
            "countryCode": "ES",
            "countryName": "Spain",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "ES1",
            "latitude": "410900N",
            "longitude": "0011010E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BRS"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "ORK"
                },
                {
                    "code": "PIK"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "BRI",
            "name": "Bari",
            "countryCode": "IT",
            "countryName": "Italy",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IT",
            "latitude": "410828N",
            "longitude": "0164718E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BGY"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CAG"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "FKB"
                },
                {
                    "code": "GOA"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "KGS"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "MST"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "SVQ"
                },
                {
                    "code": "TRN"
                },
                {
                    "code": "TRS"
                },
                {
                    "code": "TSF"
                },
                {
                    "code": "VLC"
                }
            ],
            "notices": null
        },
        {
            "code": "BSL",
            "name": "Basel",
            "countryCode": "CH",
            "countryName": "Switzerland",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "CH",
            "latitude": "473524N",
            "longitude": "0073148E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "DUB"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "EGC",
            "name": "Bergerac",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "445100N",
            "longitude": "0002900E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BRS"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "SXF",
            "name": "Berlin",
            "countryCode": "DE",
            "countryName": "Germany",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "DE",
            "latitude": "522200N",
            "longitude": "0133000E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BGY"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "SNN"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "BZR",
            "name": "Beziers",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "431927N",
            "longitude": "0032116E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BRS"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "LTN"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "RYG"
                }
            ],
            "notices": null
        },
        {
            "code": "BIQ",
            "name": "Biarritz",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "432811N",
            "longitude": "0013122W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "BIO",
            "name": "Bilbao",
            "countryCode": "ES",
            "countryName": "Spain",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "ES1",
            "latitude": "431810N",
            "longitude": "0025518W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "NRN"
                }
            ],
            "notices": null
        },
        {
            "code": "BLL",
            "name": "Billund",
            "countryCode": "DK",
            "countryName": "Denmark",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "DK",
            "latitude": "554418N",
            "longitude": "0090900E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AGP"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BUD"
                },
                {
                    "code": "CFU"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TLL"
                },
                {
                    "code": "TSF"
                },
                {
                    "code": "VLC"
                }
            ],
            "notices": null
        },
        {
            "code": "BHX",
            "name": "Birmingham",
            "countryCode": "GB",
            "countryName": "United Kingdom",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "GB",
            "latitude": "522711N",
            "longitude": "0014447W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ACE"
                },
                {
                    "code": "AGP"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "BCN"
                },
                {
                    "code": "BTS"
                },
                {
                    "code": "BZG"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "FUE"
                },
                {
                    "code": "GDN"
                },
                {
                    "code": "IBZ"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "KTW"
                },
                {
                    "code": "KUN"
                },
                {
                    "code": "LDY"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "MJV"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "PGF"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "TFS"
                }
            ],
            "notices": null
        },
        {
            "code": "BLQ",
            "name": "Bologna Main Terminal",
            "countryCode": "IT",
            "countryName": "Italy",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IT",
            "latitude": "443150N",
            "longitude": "0111733E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ACE"
                },
                {
                    "code": "AGP"
                },
                {
                    "code": "AHO"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "BCN"
                },
                {
                    "code": "BDS"
                },
                {
                    "code": "BOD"
                },
                {
                    "code": "BRI"
                },
                {
                    "code": "BRS"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "CTA"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "IBZ"
                },
                {
                    "code": "KGS"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "PMO"
                },
                {
                    "code": "RHO"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "SUF"
                },
                {
                    "code": "SVQ"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TPS"
                },
                {
                    "code": "VLC"
                },
                {
                    "code": "WMI"
                },
                {
                    "code": "WRO"
                }
            ],
            "notices": null
        },
        {
            "code": "BOD",
            "name": "Bordeaux",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "444946N",
            "longitude": "0004252W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BLQ"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "ORK"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "SVQ"
                }
            ],
            "notices": null
        },
        {
            "code": "BOH",
            "name": "Bournemouth",
            "countryCode": "GB",
            "countryName": "United Kingdom",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "GB",
            "latitude": "504650N",
            "longitude": "0015013W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AGP"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "MJV"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "TFS"
                }
            ],
            "notices": null
        },
        {
            "code": "BTS",
            "name": "Bratislava",
            "countryCode": "SK",
            "countryName": "Slovakia",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "SK",
            "latitude": "481000N",
            "longitude": "0171300E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AGP"
                },
                {
                    "code": "AHO"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BHX"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "LTN"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TPS"
                }
            ],
            "notices": null
        },
        {
            "code": "BRE",
            "name": "Bremen",
            "countryCode": "DE",
            "countryName": "Germany",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "DE",
            "latitude": "530248N",
            "longitude": "0084724E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AGP"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "CFU"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "FUE"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "GSE"
                },
                {
                    "code": "HAU"
                },
                {
                    "code": "LIS"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "RIX"
                },
                {
                    "code": "SKG"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TLL"
                },
                {
                    "code": "TMP"
                },
                {
                    "code": "TSF"
                },
                {
                    "code": "VNO"
                }
            ],
            "notices": null
        },
        {
            "code": "BES",
            "name": "Brest",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "482650N",
            "longitude": "0042514W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "MRS"
                }
            ],
            "notices": null
        },
        {
            "code": "BDS",
            "name": "Brindisi",
            "countryCode": "IT",
            "countryName": "Italy",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IT",
            "latitude": "403929N",
            "longitude": "0175648E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BGY"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TRN"
                },
                {
                    "code": "TSF"
                }
            ],
            "notices": null
        },
        {
            "code": "BRS",
            "name": "Bristol",
            "countryCode": "GB",
            "countryName": "United Kingdom",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "GB",
            "latitude": "512254N",
            "longitude": "0024232W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ACE"
                },
                {
                    "code": "AGP"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BUD"
                },
                {
                    "code": "BZR"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EGC"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "GDN"
                },
                {
                    "code": "GNB"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "IBZ"
                },
                {
                    "code": "KUN"
                },
                {
                    "code": "LIG"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "NOC"
                },
                {
                    "code": "NTE"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "POZ"
                },
                {
                    "code": "REU"
                },
                {
                    "code": "RZE"
                },
                {
                    "code": "SNN"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TSF"
                },
                {
                    "code": "VLC"
                },
                {
                    "code": "WMI"
                },
                {
                    "code": "WRO"
                }
            ],
            "notices": null
        },
        {
            "code": "BVE",
            "name": "Brive-Souillac",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "451000N",
            "longitude": "0013200E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "BRQ",
            "name": "Brno",
            "countryCode": "CZ",
            "countryName": "Czech Republic",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "CZ",
            "latitude": "490905N",
            "longitude": "0164140E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "BRU",
            "name": "Brussels Airport",
            "countryCode": "BE",
            "countryName": "Belgium",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "BE",
            "latitude": "505405N",
            "longitude": "0042904E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AGP"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "BCN"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "FCO"
                },
                {
                    "code": "IBZ"
                },
                {
                    "code": "LIS"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "TSF"
                },
                {
                    "code": "VLC"
                }
            ],
            "notices": null
        },
        {
            "code": "CRL",
            "name": "Brussels Charleroi",
            "countryCode": "BE",
            "countryName": "Belgium",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "BE",
            "latitude": "502733N",
            "longitude": "0042714E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ACE"
                },
                {
                    "code": "AGA"
                },
                {
                    "code": "AGP"
                },
                {
                    "code": "AHO"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "AOI"
                },
                {
                    "code": "ATH"
                },
                {
                    "code": "BCN"
                },
                {
                    "code": "BDS"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BIQ"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BOD"
                },
                {
                    "code": "BRI"
                },
                {
                    "code": "BTS"
                },
                {
                    "code": "BUD"
                },
                {
                    "code": "CAG"
                },
                {
                    "code": "CCF"
                },
                {
                    "code": "CFU"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CIY"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "EGC"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "FEZ"
                },
                {
                    "code": "FNI"
                },
                {
                    "code": "FSC"
                },
                {
                    "code": "FUE"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "IBZ"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "KUN"
                },
                {
                    "code": "LEI"
                },
                {
                    "code": "LIG"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "LRH"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "MPL"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "NDR"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "OTP"
                },
                {
                    "code": "OUD"
                },
                {
                    "code": "PEG"
                },
                {
                    "code": "PFO"
                },
                {
                    "code": "PGF"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "PRG"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "PSR"
                },
                {
                    "code": "PUY"
                },
                {
                    "code": "RAK"
                },
                {
                    "code": "RBA"
                },
                {
                    "code": "RDZ"
                },
                {
                    "code": "REU"
                },
                {
                    "code": "RHO"
                },
                {
                    "code": "RIX"
                },
                {
                    "code": "RJK"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "SDR"
                },
                {
                    "code": "SKG"
                },
                {
                    "code": "SUF"
                },
                {
                    "code": "SVQ"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TGD"
                },
                {
                    "code": "TNG"
                },
                {
                    "code": "TPS"
                },
                {
                    "code": "TRN"
                },
                {
                    "code": "TSF"
                },
                {
                    "code": "VLC"
                },
                {
                    "code": "VLL"
                },
                {
                    "code": "VNO"
                },
                {
                    "code": "WMI"
                },
                {
                    "code": "ZAD"
                },
                {
                    "code": "ZAZ"
                },
                {
                    "code": "ZTH"
                }
            ],
            "notices": null
        },
        {
            "code": "BUD",
            "name": "Budapest T2B",
            "countryCode": "HU",
            "countryName": "Hungary",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "HU",
            "latitude": "472600N",
            "longitude": "0191400E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BCN"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BLL"
                },
                {
                    "code": "BRS"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "FMM"
                },
                {
                    "code": "LBC"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TMP"
                },
                {
                    "code": "TSF"
                }
            ],
            "notices": null
        },
        {
            "code": "BZG",
            "name": "Bydgoszcz",
            "countryCode": "PL",
            "countryName": "Poland",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "PL",
            "latitude": "530548N",
            "longitude": "0175843E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BHX"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "GLA"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "CAG",
            "name": "Cagliari",
            "countryCode": "IT",
            "countryName": "Italy",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IT",
            "latitude": "391450N",
            "longitude": "0090340E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BGY"
                },
                {
                    "code": "BRI"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "CUF"
                },
                {
                    "code": "FKB"
                },
                {
                    "code": "GOA"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "KUN"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "PEG"
                },
                {
                    "code": "PMF"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "TPS"
                },
                {
                    "code": "TSF"
                }
            ],
            "notices": null
        },
        {
            "code": "CCF",
            "name": "Carcassonne",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "431300N",
            "longitude": "0021900E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "GLA"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "ORK"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "CWL",
            "name": "Cardiff",
            "countryCode": "GB",
            "countryName": "United Kingdom",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "GB",
            "latitude": "512351N",
            "longitude": "0032048W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "DUB"
                },
                {
                    "code": "SNN"
                },
                {
                    "code": "TFS"
                }
            ],
            "notices": null
        },
        {
            "code": "CTA",
            "name": "Catania-Fontanarossa ",
            "countryCode": "IT",
            "countryName": "Italy",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IT",
            "latitude": "372410N",
            "longitude": "0150600E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BGY"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "FCO"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "SUF"
                },
                {
                    "code": "TRN"
                },
                {
                    "code": "TSF"
                }
            ],
            "notices": null
        },
        {
            "code": "CFE",
            "name": "Clermont Ferrand",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "454709N",
            "longitude": "0031003E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "OPO"
                }
            ],
            "notices": null
        },
        {
            "code": "CGN",
            "name": "Cologne/Bonn",
            "countryCode": "DE",
            "countryName": "Germany",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "DE",
            "latitude": "505157N",
            "longitude": "0070834E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AGP"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "BCN"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "RIX"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TFS"
                }
            ],
            "notices": null
        },
        {
            "code": "CIY",
            "name": "Comiso Airport",
            "countryCode": "IT",
            "countryName": "Italy",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IT",
            "latitude": "365930N",
            "longitude": "0143625E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "CPH",
            "name": "Copenhagen Airport",
            "countryCode": "DK",
            "countryName": "Denmark",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "DK",
            "latitude": "553629N",
            "longitude": "0123800E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BGY"
                },
                {
                    "code": "LTN"
                },
                {
                    "code": "WMI"
                }
            ],
            "notices": null
        },
        {
            "code": "CFU",
            "name": "Corfu",
            "countryCode": "GR",
            "countryName": "Greece",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "GR",
            "latitude": "393607N",
            "longitude": "0195442E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BGY"
                },
                {
                    "code": "BLL"
                },
                {
                    "code": "BRE"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "LBA"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "PIK"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "ORK",
            "name": "Cork",
            "countryCode": "IE",
            "countryName": "Ireland",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IE",
            "latitude": "515036N",
            "longitude": "0082925W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ACE"
                },
                {
                    "code": "AGP"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BOD"
                },
                {
                    "code": "CCF"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "FUE"
                },
                {
                    "code": "GDN"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "LGW"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "LRH"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "REU"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "WRO"
                }
            ],
            "notices": null
        },
        {
            "code": "CHQ",
            "name": "Crete (Chania)",
            "countryCode": "GR",
            "countryName": "Greece",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "GR",
            "latitude": "353154N",
            "longitude": "0240859E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ATH"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BLL"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BRE"
                },
                {
                    "code": "BRS"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "FMM"
                },
                {
                    "code": "GLA"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "KTW"
                },
                {
                    "code": "LBA"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "PFO"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "SKG"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TSF"
                },
                {
                    "code": "VNO"
                },
                {
                    "code": "WMI"
                },
                {
                    "code": "WRO"
                }
            ],
            "notices": null
        },
        {
            "code": "CRV",
            "name": "Crotone Airport",
            "countryCode": "IT",
            "countryName": "Italy",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IT",
            "latitude": "390000N",
            "longitude": "0170500E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BGY"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "PSA"
                }
            ],
            "notices": null
        },
        {
            "code": "CUF",
            "name": "Cuneo",
            "countryCode": "IT",
            "countryName": "Italy",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IT",
            "latitude": "443207N",
            "longitude": "0073703E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AHO"
                },
                {
                    "code": "CAG"
                },
                {
                    "code": "TPS"
                }
            ],
            "notices": null
        },
        {
            "code": "LDY",
            "name": "Derry",
            "countryCode": "GB",
            "countryName": "United Kingdom",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "GB",
            "latitude": "550236N",
            "longitude": "0070912W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ALC"
                },
                {
                    "code": "BHX"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "GLA"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "DNR",
            "name": "Dinard",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "483516N",
            "longitude": "0020501W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "EMA"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "DLE",
            "name": "Dole ",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "470300N",
            "longitude": "0052600E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "FEZ"
                },
                {
                    "code": "LIS"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "RAK"
                }
            ],
            "notices": null
        },
        {
            "code": "DTM",
            "name": "Dortmund Airport",
            "countryCode": "DE",
            "countryName": "Germany",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "DE",
            "latitude": "513100N",
            "longitude": "0073600E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AGP"
                },
                {
                    "code": "AHO"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "DUB",
            "name": "Dublin ",
            "countryCode": "IE",
            "countryName": "Ireland",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IE",
            "latitude": "532557N",
            "longitude": "0061508W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ACE"
                },
                {
                    "code": "AGP"
                },
                {
                    "code": "AHO"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "BCN"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BHX"
                },
                {
                    "code": "BIQ"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BRE"
                },
                {
                    "code": "BRI"
                },
                {
                    "code": "BRS"
                },
                {
                    "code": "BRU"
                },
                {
                    "code": "BSL"
                },
                {
                    "code": "BTS"
                },
                {
                    "code": "BUD"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "BZG"
                },
                {
                    "code": "CCF"
                },
                {
                    "code": "CGN"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CIY"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "CWL"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "FMM"
                },
                {
                    "code": "FUE"
                },
                {
                    "code": "GDN"
                },
                {
                    "code": "GLA"
                },
                {
                    "code": "GNB"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "IBZ"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "KTW"
                },
                {
                    "code": "KUN"
                },
                {
                    "code": "LBA"
                },
                {
                    "code": "LCJ"
                },
                {
                    "code": "LEI"
                },
                {
                    "code": "LGW"
                },
                {
                    "code": "LIS"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "LRH"
                },
                {
                    "code": "LTN"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "MJV"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "NCE"
                },
                {
                    "code": "NCL"
                },
                {
                    "code": "NTE"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "OTP"
                },
                {
                    "code": "PIS"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "PMO"
                },
                {
                    "code": "POZ"
                },
                {
                    "code": "PRG"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "RAK"
                },
                {
                    "code": "RDZ"
                },
                {
                    "code": "REU"
                },
                {
                    "code": "RIX"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "RZE"
                },
                {
                    "code": "SDR"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "SVQ"
                },
                {
                    "code": "SXF"
                },
                {
                    "code": "SZG"
                },
                {
                    "code": "SZZ"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TLL"
                },
                {
                    "code": "TLN"
                },
                {
                    "code": "TRN"
                },
                {
                    "code": "TSF"
                },
                {
                    "code": "TUF"
                },
                {
                    "code": "VLC"
                },
                {
                    "code": "VNO"
                },
                {
                    "code": "WMI"
                },
                {
                    "code": "WRO"
                },
                {
                    "code": "ZAD"
                }
            ],
            "notices": null
        },
        {
            "code": "NRN",
            "name": "Dusseldorf (Weeze)",
            "countryCode": "DE",
            "countryName": "Germany",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "DE",
            "latitude": "513620N",
            "longitude": "0060900E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ACE"
                },
                {
                    "code": "AGA"
                },
                {
                    "code": "AGP"
                },
                {
                    "code": "AHO"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "AOI"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BIO"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BRI"
                },
                {
                    "code": "BZG"
                },
                {
                    "code": "BZR"
                },
                {
                    "code": "CAG"
                },
                {
                    "code": "CFU"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "FEZ"
                },
                {
                    "code": "FUE"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "IBZ"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "LPP"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "OUD"
                },
                {
                    "code": "PEG"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "PMO"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "PSR"
                },
                {
                    "code": "RAK"
                },
                {
                    "code": "RIX"
                },
                {
                    "code": "SDR"
                },
                {
                    "code": "SKG"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "SUF"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TLL"
                },
                {
                    "code": "TSF"
                },
                {
                    "code": "VLC"
                },
                {
                    "code": "VXO"
                },
                {
                    "code": "ZAD"
                }
            ],
            "notices": null
        },
        {
            "code": "EMA",
            "name": "East Midlands",
            "countryCode": "GB",
            "countryName": "United Kingdom",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "GB",
            "latitude": "524948N",
            "longitude": "0011946W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ACE"
                },
                {
                    "code": "AGP"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "BCN"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BTS"
                },
                {
                    "code": "CCF"
                },
                {
                    "code": "CFU"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "DNR"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EGC"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "FUE"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "IBZ"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "LCJ"
                },
                {
                    "code": "LIG"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "MAH"
                },
                {
                    "code": "MJV"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "NOC"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "RHO"
                },
                {
                    "code": "RIX"
                },
                {
                    "code": "RZE"
                },
                {
                    "code": "SXF"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TSF"
                },
                {
                    "code": "VLC"
                },
                {
                    "code": "WMI"
                },
                {
                    "code": "WRO"
                }
            ],
            "notices": null
        },
        {
            "code": "EDI",
            "name": "Edinburgh",
            "countryCode": "GB",
            "countryName": "United Kingdom",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "GB",
            "latitude": "555655N",
            "longitude": "0032101W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ACE"
                },
                {
                    "code": "AGP"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "BCN"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BOD"
                },
                {
                    "code": "BRE"
                },
                {
                    "code": "BTS"
                },
                {
                    "code": "BZR"
                },
                {
                    "code": "CFU"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "FUE"
                },
                {
                    "code": "GDN"
                },
                {
                    "code": "GNB"
                },
                {
                    "code": "GSE"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "KUN"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "MPL"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "NTE"
                },
                {
                    "code": "PIS"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "POZ"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "SDR"
                },
                {
                    "code": "SNN"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TMP"
                },
                {
                    "code": "WRO"
                }
            ],
            "notices": null
        },
        {
            "code": "EIN",
            "name": "Eindhoven",
            "countryCode": "NL",
            "countryName": "Netherlands",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "NL",
            "latitude": "512712N",
            "longitude": "0052320E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ACE"
                },
                {
                    "code": "AGP"
                },
                {
                    "code": "AHO"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "BDS"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "CCF"
                },
                {
                    "code": "CFU"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CTA"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "FEZ"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "IBZ"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "LIS"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "MJV"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "RAK"
                },
                {
                    "code": "REU"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "SVQ"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TPS"
                },
                {
                    "code": "TRN"
                },
                {
                    "code": "TSF"
                },
                {
                    "code": "WMI"
                }
            ],
            "notices": null
        },
        {
            "code": "ESU",
            "name": "Essaouira Airport",
            "countryCode": "MA",
            "countryName": "Morocco",
            "countryGroupCode": "1",
            "countryGroupName": "non EU/EEA",
            "timeZoneCode": "MA",
            "latitude": "313200N",
            "longitude": "0094200W",
            "mobileBoardingPass": false,
            "markets": [
                {
                    "code": "MRS"
                }
            ],
            "notices": null
        },
        {
            "code": "FAO",
            "name": "Faro",
            "countryCode": "PT",
            "countryName": "Portugal",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "PT1",
            "latitude": "370052N",
            "longitude": "0075815W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BHX"
                },
                {
                    "code": "BOH"
                },
                {
                    "code": "BRE"
                },
                {
                    "code": "BRS"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CGN"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DTM"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "FMM"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "KIR"
                },
                {
                    "code": "LBA"
                },
                {
                    "code": "LDY"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "MST"
                },
                {
                    "code": "NOC"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "ORK"
                },
                {
                    "code": "PIK"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "SNN"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "FEZ",
            "name": "Fez",
            "countryCode": "MA",
            "countryName": "Morocco",
            "countryGroupCode": "1",
            "countryGroupName": "non EU/EEA",
            "timeZoneCode": "MA",
            "latitude": "335600N",
            "longitude": "0045800W",
            "mobileBoardingPass": false,
            "markets": [
                {
                    "code": "BCN"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DLE"
                },
                {
                    "code": "EBU"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "FNI"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "NTE"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "FSC",
            "name": "Figari",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "413000N",
            "longitude": "0090600E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BVA"
                },
                {
                    "code": "CRL"
                }
            ],
            "notices": null
        },
        {
            "code": "HHN",
            "name": "Frankfurt-Hahn",
            "countryCode": "DE",
            "countryName": "Germany",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "DE",
            "latitude": "495700N",
            "longitude": "0071600E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ACE"
                },
                {
                    "code": "AGP"
                },
                {
                    "code": "AHO"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "AOI"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BRI"
                },
                {
                    "code": "CAG"
                },
                {
                    "code": "CFU"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CIY"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "FEZ"
                },
                {
                    "code": "FUE"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "IBZ"
                },
                {
                    "code": "KGS"
                },
                {
                    "code": "KIR"
                },
                {
                    "code": "KUN"
                },
                {
                    "code": "LIS"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "MPL"
                },
                {
                    "code": "NDR"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "PDV"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "PSR"
                },
                {
                    "code": "PUY"
                },
                {
                    "code": "RAK"
                },
                {
                    "code": "REU"
                },
                {
                    "code": "RHO"
                },
                {
                    "code": "RIX"
                },
                {
                    "code": "SCQ"
                },
                {
                    "code": "SDR"
                },
                {
                    "code": "SKG"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "SUF"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TMP"
                },
                {
                    "code": "TPS"
                },
                {
                    "code": "TRF"
                },
                {
                    "code": "TSF"
                },
                {
                    "code": "VLC"
                },
                {
                    "code": "VNO"
                },
                {
                    "code": "XRY"
                },
                {
                    "code": "ZAD"
                }
            ],
            "notices": null
        },
        {
            "code": "FUE",
            "name": "Fuerteventura",
            "countryCode": "ES",
            "countryName": "Spain",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "ES2",
            "latitude": "282709N",
            "longitude": "0135149W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BCN"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BHX"
                },
                {
                    "code": "BRE"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "LBA"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "LTN"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "ORK"
                },
                {
                    "code": "PIK"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "SNN"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "WMI"
                }
            ],
            "notices": null
        },
        {
            "code": "GDN",
            "name": "Gdansk",
            "countryCode": "PL",
            "countryName": "Poland",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "PL",
            "latitude": "542200N",
            "longitude": "0183900E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ALC"
                },
                {
                    "code": "BHX"
                },
                {
                    "code": "BRS"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "LBA"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "ORK"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "WMI"
                },
                {
                    "code": "WRO"
                }
            ],
            "notices": null
        },
        {
            "code": "GOA",
            "name": "Genoa",
            "countryCode": "IT",
            "countryName": "Italy",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IT",
            "latitude": "442447N",
            "longitude": "0085016E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BRI"
                },
                {
                    "code": "CAG"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TPS"
                }
            ],
            "notices": null
        },
        {
            "code": "GLA",
            "name": "Glasgow International Airport",
            "countryCode": "GB",
            "countryName": "United Kingdom",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "GB",
            "latitude": "555200N",
            "longitude": "0042600W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BZG"
                },
                {
                    "code": "CCF"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "LDY"
                },
                {
                    "code": "RIX"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "WMI"
                },
                {
                    "code": "WRO"
                }
            ],
            "notices": null
        },
        {
            "code": "PIK",
            "name": "Glasgow Prestwick ",
            "countryCode": "GB",
            "countryName": "United Kingdom",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "GB",
            "latitude": "553011N",
            "longitude": "0043432W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ACE"
                },
                {
                    "code": "AGP"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "BCN"
                },
                {
                    "code": "CFU"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "FUE"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "IBZ"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "MJV"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "REU"
                },
                {
                    "code": "TFS"
                }
            ],
            "notices": null
        },
        {
            "code": "GSE",
            "name": "Gothenburg City",
            "countryCode": "SE",
            "countryName": "Sweden",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "SE",
            "latitude": "574606N",
            "longitude": "0115204E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AGP"
                },
                {
                    "code": "AHO"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BRE"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "ZAD"
                }
            ],
            "notices": null
        },
        {
            "code": "GRZ",
            "name": "Graz",
            "countryCode": "AT",
            "countryName": "Austria",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "AT",
            "latitude": "465937N",
            "longitude": "0152624E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "GNB",
            "name": "Grenoble Lyon",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "452146N",
            "longitude": "0051947E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BRS"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "HAM",
            "name": "Hamburg Airport",
            "countryCode": "DE",
            "countryName": "Germany",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "DE",
            "latitude": "533749N",
            "longitude": "0095918E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "LIS"
                },
                {
                    "code": "OPO"
                }
            ],
            "notices": null
        },
        {
            "code": "LBC",
            "name": "Hamburg-Lübeck",
            "countryCode": "DE",
            "countryName": "Germany",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "DE",
            "latitude": "534819N",
            "longitude": "0104309E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BGY"
                },
                {
                    "code": "BUD"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "PSA"
                }
            ],
            "notices": null
        },
        {
            "code": "HAU",
            "name": "Haugesund",
            "countryCode": "NO",
            "countryName": "Norway",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "NO",
            "latitude": "592420N",
            "longitude": "0051645E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AGP"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "BRE"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "IBZ",
            "name": "Ibiza",
            "countryCode": "ES",
            "countryName": "Spain",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "ES1",
            "latitude": "385225N",
            "longitude": "0012222E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AGP"
                },
                {
                    "code": "BCN"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BHX"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BRS"
                },
                {
                    "code": "BRU"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "LBA"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "PIK"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "VLC"
                }
            ],
            "notices": null
        },
        {
            "code": "XRY",
            "name": "Jerez",
            "countryCode": "ES",
            "countryName": "Spain",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "ES1",
            "latitude": "364441N",
            "longitude": "0060336W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BCN"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "KLX",
            "name": "Kalamata Airport",
            "countryCode": "GR",
            "countryName": "Greece",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "GR",
            "latitude": "370406N",
            "longitude": "0220132E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BGY"
                }
            ],
            "notices": null
        },
        {
            "code": "KTW",
            "name": "Katowice",
            "countryCode": "PL",
            "countryName": "Poland",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "PL",
            "latitude": "502900N",
            "longitude": "0190500E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ALC"
                },
                {
                    "code": "BHX"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "KUN",
            "name": "Kaunas",
            "countryCode": "LT",
            "countryName": "Lithuania",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "LT",
            "latitude": "545400N",
            "longitude": "0235500E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ALC"
                },
                {
                    "code": "BHX"
                },
                {
                    "code": "BRS"
                },
                {
                    "code": "CAG"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "KGS"
                },
                {
                    "code": "LBA"
                },
                {
                    "code": "LGW"
                },
                {
                    "code": "LTN"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "PFO"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "RHO"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "SNN"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TPS"
                }
            ],
            "notices": null
        },
        {
            "code": "EFL",
            "name": "Kefalonia",
            "countryCode": "GR",
            "countryName": "Greece",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "GR",
            "latitude": "380712N",
            "longitude": "0203002E",
            "mobileBoardingPass": false,
            "markets": [
                {
                    "code": "BGY"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "KIR",
            "name": "Kerry",
            "countryCode": "IE",
            "countryName": "Ireland",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IE",
            "latitude": "520300N",
            "longitude": "0093000W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ALC"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "LTN"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "NOC",
            "name": "Knock Ireland West",
            "countryCode": "IE",
            "countryName": "Ireland",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IE",
            "latitude": "535500N",
            "longitude": "0084900W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ACE"
                },
                {
                    "code": "AGP"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BRS"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "LTN"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TFS"
                }
            ],
            "notices": null
        },
        {
            "code": "KGS",
            "name": "Kos",
            "countryCode": "GR",
            "countryName": "Greece",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "GR",
            "latitude": "364736N",
            "longitude": "0270530E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BGY"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BRI"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "KUN"
                },
                {
                    "code": "LBA"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "KRK",
            "name": "Krakow",
            "countryCode": "PL",
            "countryName": "Poland",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "PL",
            "latitude": "500500N",
            "longitude": "0194700E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AGP"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BHX"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CAG"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DTM"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "GDN"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "KGS"
                },
                {
                    "code": "LBA"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "PFO"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "SNN"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TPS"
                }
            ],
            "notices": null
        },
        {
            "code": "LRH",
            "name": "La Rochelle",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "461050N",
            "longitude": "0011109W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "ORK"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "SUF",
            "name": "Lamezia",
            "countryCode": "IT",
            "countryName": "Italy",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IT",
            "latitude": "385400N",
            "longitude": "0161500E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BGY"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "CTA"
                },
                {
                    "code": "FCO"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TSF"
                }
            ],
            "notices": null
        },
        {
            "code": "ACE",
            "name": "Lanzarote",
            "countryCode": "ES",
            "countryName": "Spain",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "ES2",
            "latitude": "285615N",
            "longitude": "0133613W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BCN"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BHX"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BRS"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "FKB"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "LBA"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "LTN"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "NOC"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "ORK"
                },
                {
                    "code": "PIK"
                },
                {
                    "code": "SCQ"
                },
                {
                    "code": "SDR"
                },
                {
                    "code": "SNN"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "SVQ"
                },
                {
                    "code": "VLL"
                },
                {
                    "code": "ZAZ"
                }
            ],
            "notices": null
        },
        {
            "code": "LPP",
            "name": "Lappeenranta",
            "countryCode": "FI",
            "countryName": "Finland",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FI",
            "latitude": "610246N",
            "longitude": "0280924E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BGY"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "NRN"
                }
            ],
            "notices": null
        },
        {
            "code": "LPA",
            "name": "Las Palmas Gran Canaria",
            "countryCode": "ES",
            "countryName": "Spain",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "ES2",
            "latitude": "275555N",
            "longitude": "0152312W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BCN"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BHX"
                },
                {
                    "code": "BOH"
                },
                {
                    "code": "BRE"
                },
                {
                    "code": "BRS"
                },
                {
                    "code": "BTS"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "FKB"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "LTN"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "ORK"
                },
                {
                    "code": "OVD"
                },
                {
                    "code": "PIK"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "SCQ"
                },
                {
                    "code": "SDR"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "SVQ"
                },
                {
                    "code": "VLC"
                },
                {
                    "code": "WMI"
                }
            ],
            "notices": null
        },
        {
            "code": "LBA",
            "name": "Leeds Bradford",
            "countryCode": "GB",
            "countryName": "United Kingdom",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "GB",
            "latitude": "535155N",
            "longitude": "0013917W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ACE"
                },
                {
                    "code": "AGP"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "CFU"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "FUE"
                },
                {
                    "code": "GDN"
                },
                {
                    "code": "IBZ"
                },
                {
                    "code": "KGS"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "KUN"
                },
                {
                    "code": "LIG"
                },
                {
                    "code": "MJV"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "MPL"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "RIX"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TSF"
                }
            ],
            "notices": null
        },
        {
            "code": "LEJ",
            "name": "Leipzig Halle",
            "countryCode": "DE",
            "countryName": "Germany",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "DE",
            "latitude": "512557N",
            "longitude": "0121430E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "LIL",
            "name": "Lille Airport ",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "503352N",
            "longitude": "0030517E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "MRS"
                },
                {
                    "code": "OPO"
                }
            ],
            "notices": null
        },
        {
            "code": "LIG",
            "name": "Limoges",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "455143N",
            "longitude": "0011056E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BRS"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "LBA"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "LNZ",
            "name": "Linz",
            "countryCode": "AT",
            "countryName": "Austria",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "AT",
            "latitude": "481400N",
            "longitude": "0141115E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "STN"
                },
                {
                    "code": "TFS"
                }
            ],
            "notices": null
        },
        {
            "code": "LIS",
            "name": "Lisbon Airport",
            "countryCode": "PT",
            "countryName": "Portugal",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "PT1",
            "latitude": "384655N",
            "longitude": "0090800W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BGY"
                },
                {
                    "code": "BRE"
                },
                {
                    "code": "BRU"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "DLE"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "HAM"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "WMI"
                }
            ],
            "notices": null
        },
        {
            "code": "LPL",
            "name": "Liverpool",
            "countryCode": "GB",
            "countryName": "United Kingdom",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "GB",
            "latitude": "532018N",
            "longitude": "0025200W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ACE"
                },
                {
                    "code": "AGP"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "BCN"
                },
                {
                    "code": "BTS"
                },
                {
                    "code": "CCF"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EGC"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "FNI"
                },
                {
                    "code": "FUE"
                },
                {
                    "code": "IBZ"
                },
                {
                    "code": "KGS"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "LDY"
                },
                {
                    "code": "LIG"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "NOC"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "ORK"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "POZ"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "REU"
                },
                {
                    "code": "RHO"
                },
                {
                    "code": "SDR"
                },
                {
                    "code": "SZZ"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TMP"
                },
                {
                    "code": "TRF"
                },
                {
                    "code": "VLC"
                },
                {
                    "code": "VNO"
                },
                {
                    "code": "WMI"
                },
                {
                    "code": "WRO"
                }
            ],
            "notices": null
        },
        {
            "code": "LCJ",
            "name": "Lodz",
            "countryCode": "PL",
            "countryName": "Poland",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "PL",
            "latitude": "514319N",
            "longitude": "0192354E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "DUB"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "LGW",
            "name": "London Gatwick",
            "countryCode": "GB",
            "countryName": "United Kingdom",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "GB",
            "latitude": "510853N",
            "longitude": "0001148W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "DUB"
                },
                {
                    "code": "KUN"
                },
                {
                    "code": "ORK"
                },
                {
                    "code": "SNN"
                },
                {
                    "code": "SVQ"
                }
            ],
            "notices": null
        },
        {
            "code": "LTN",
            "name": "London Luton",
            "countryCode": "GB",
            "countryName": "United Kingdom",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "GB",
            "latitude": "515300N",
            "longitude": "0002106W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ACE"
                },
                {
                    "code": "BTS"
                },
                {
                    "code": "BZR"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CPH"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "FNI"
                },
                {
                    "code": "FUE"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "KIR"
                },
                {
                    "code": "KUN"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "MJV"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "NOC"
                },
                {
                    "code": "RAK"
                },
                {
                    "code": "RZE"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "VNO"
                }
            ],
            "notices": null
        },
        {
            "code": "STN",
            "name": "London-Stansted",
            "countryCode": "GB",
            "countryName": "United Kingdom",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "GB",
            "latitude": "515300N",
            "longitude": "0001400E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AAR"
                },
                {
                    "code": "ACE"
                },
                {
                    "code": "AGA"
                },
                {
                    "code": "AGP"
                },
                {
                    "code": "AHO"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "AOI"
                },
                {
                    "code": "ATH"
                },
                {
                    "code": "BCN"
                },
                {
                    "code": "BDS"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BIQ"
                },
                {
                    "code": "BLL"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BOD"
                },
                {
                    "code": "BRE"
                },
                {
                    "code": "BRI"
                },
                {
                    "code": "BRQ"
                },
                {
                    "code": "BSL"
                },
                {
                    "code": "BTS"
                },
                {
                    "code": "BUD"
                },
                {
                    "code": "BVE"
                },
                {
                    "code": "BZG"
                },
                {
                    "code": "CCF"
                },
                {
                    "code": "CFU"
                },
                {
                    "code": "CGN"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CIY"
                },
                {
                    "code": "DNR"
                },
                {
                    "code": "DTM"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "EFL"
                },
                {
                    "code": "EGC"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "FEZ"
                },
                {
                    "code": "FKB"
                },
                {
                    "code": "FMM"
                },
                {
                    "code": "FUE"
                },
                {
                    "code": "GDN"
                },
                {
                    "code": "GLA"
                },
                {
                    "code": "GNB"
                },
                {
                    "code": "GOA"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "GRZ"
                },
                {
                    "code": "GSE"
                },
                {
                    "code": "HAU"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "IBZ"
                },
                {
                    "code": "KGS"
                },
                {
                    "code": "KIR"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "KTW"
                },
                {
                    "code": "KUN"
                },
                {
                    "code": "LCJ"
                },
                {
                    "code": "LDE"
                },
                {
                    "code": "LDY"
                },
                {
                    "code": "LEI"
                },
                {
                    "code": "LEJ"
                },
                {
                    "code": "LIG"
                },
                {
                    "code": "LIS"
                },
                {
                    "code": "LNZ"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "LRH"
                },
                {
                    "code": "LUZ"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MJV"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "NOC"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "NUE"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "ORK"
                },
                {
                    "code": "OSI"
                },
                {
                    "code": "OSR"
                },
                {
                    "code": "OTP"
                },
                {
                    "code": "PDV"
                },
                {
                    "code": "PEG"
                },
                {
                    "code": "PFO"
                },
                {
                    "code": "PGF"
                },
                {
                    "code": "PIS"
                },
                {
                    "code": "PMF"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "PMO"
                },
                {
                    "code": "POZ"
                },
                {
                    "code": "PRG"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "PSR"
                },
                {
                    "code": "PUY"
                },
                {
                    "code": "RAK"
                },
                {
                    "code": "RBA"
                },
                {
                    "code": "RDZ"
                },
                {
                    "code": "REU"
                },
                {
                    "code": "RHO"
                },
                {
                    "code": "RIX"
                },
                {
                    "code": "RJK"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "RZE"
                },
                {
                    "code": "SCQ"
                },
                {
                    "code": "SDR"
                },
                {
                    "code": "SFT"
                },
                {
                    "code": "SKG"
                },
                {
                    "code": "SNN"
                },
                {
                    "code": "SUF"
                },
                {
                    "code": "SVQ"
                },
                {
                    "code": "SXB"
                },
                {
                    "code": "SXF"
                },
                {
                    "code": "SZG"
                },
                {
                    "code": "SZZ"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TGD"
                },
                {
                    "code": "TLL"
                },
                {
                    "code": "TLN"
                },
                {
                    "code": "TMP"
                },
                {
                    "code": "TRF"
                },
                {
                    "code": "TRN"
                },
                {
                    "code": "TRS"
                },
                {
                    "code": "TSF"
                },
                {
                    "code": "TUF"
                },
                {
                    "code": "VLC"
                },
                {
                    "code": "VLL"
                },
                {
                    "code": "VNO"
                },
                {
                    "code": "VST"
                },
                {
                    "code": "WMI"
                },
                {
                    "code": "WRO"
                },
                {
                    "code": "XRY"
                },
                {
                    "code": "ZAD"
                },
                {
                    "code": "ZAZ"
                }
            ],
            "notices": null
        },
        {
            "code": "LDE",
            "name": "Lourdes Pyrenees",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "431055N",
            "longitude": "0000013W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BGY"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "LUZ",
            "name": "Lublin",
            "countryCode": "PL",
            "countryName": "Poland",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "PL",
            "latitude": "301400N",
            "longitude": "1025900E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "MST",
            "name": "Maastricht",
            "countryCode": "NL",
            "countryName": "Netherlands",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "NL",
            "latitude": "505440N",
            "longitude": "0054625E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ALC"
                },
                {
                    "code": "BRI"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "TFS"
                }
            ],
            "notices": null
        },
        {
            "code": "MAD",
            "name": "Madrid T1",
            "countryCode": "ES",
            "countryName": "Spain",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "ES1",
            "latitude": "402820N",
            "longitude": "0033348W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ACE"
                },
                {
                    "code": "AHO"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BRE"
                },
                {
                    "code": "BTS"
                },
                {
                    "code": "BUD"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CAG"
                },
                {
                    "code": "CGN"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "CTA"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "FEZ"
                },
                {
                    "code": "FUE"
                },
                {
                    "code": "GSE"
                },
                {
                    "code": "IBZ"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "MAH"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "OTP"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "PMO"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "RAK"
                },
                {
                    "code": "RBA"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "SCQ"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "SXF"
                },
                {
                    "code": "TFN"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TNG"
                },
                {
                    "code": "VNO"
                },
                {
                    "code": "WMI"
                }
            ],
            "notices": null
        },
        {
            "code": "MAH",
            "name": "Mahon",
            "countryCode": "ES",
            "countryName": "Spain",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "ES1",
            "latitude": "395141N",
            "longitude": "0041255E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BCN"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "VLC"
                }
            ],
            "notices": null
        },
        {
            "code": "AGP",
            "name": "Malaga",
            "countryCode": "ES",
            "countryName": "Spain",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "ES1",
            "latitude": "364018N",
            "longitude": "0042953W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BCN"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BHX"
                },
                {
                    "code": "BLL"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BOH"
                },
                {
                    "code": "BRE"
                },
                {
                    "code": "BRS"
                },
                {
                    "code": "BRU"
                },
                {
                    "code": "BTS"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CGN"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DTM"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "FKB"
                },
                {
                    "code": "FMM"
                },
                {
                    "code": "FMO"
                },
                {
                    "code": "GSE"
                },
                {
                    "code": "HAU"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "IBZ"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "LBA"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "NOC"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "NUE"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "ORK"
                },
                {
                    "code": "PIK"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "SCQ"
                },
                {
                    "code": "SDR"
                },
                {
                    "code": "SNN"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TMP"
                },
                {
                    "code": "TRF"
                },
                {
                    "code": "TSF"
                },
                {
                    "code": "VST"
                },
                {
                    "code": "WRO"
                }
            ],
            "notices": null
        },
        {
            "code": "MLA",
            "name": "Malta",
            "countryCode": "MT",
            "countryName": "Malta",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "MT",
            "latitude": "355400N",
            "longitude": "0143100E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BGY"
                },
                {
                    "code": "BHX"
                },
                {
                    "code": "BLL"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BOH"
                },
                {
                    "code": "BRI"
                },
                {
                    "code": "BRS"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "GSE"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "KUN"
                },
                {
                    "code": "LBA"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "LTN"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "PIK"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TPS"
                },
                {
                    "code": "TRN"
                },
                {
                    "code": "TSF"
                },
                {
                    "code": "WRO"
                }
            ],
            "notices": null
        },
        {
            "code": "MAN",
            "name": "Manchester T3",
            "countryCode": "GB",
            "countryName": "United Kingdom",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "GB",
            "latitude": "532112N",
            "longitude": "0021636W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ACE"
                },
                {
                    "code": "AGP"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "BCN"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BIQ"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BRE"
                },
                {
                    "code": "BUD"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "BZR"
                },
                {
                    "code": "CFU"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "FUE"
                },
                {
                    "code": "GDN"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "IBZ"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "LIS"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MJV"
                },
                {
                    "code": "NCE"
                },
                {
                    "code": "PFO"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "REU"
                },
                {
                    "code": "RIX"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "RZE"
                },
                {
                    "code": "SNN"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TLL"
                },
                {
                    "code": "VLC"
                },
                {
                    "code": "WMI"
                },
                {
                    "code": "ZAD"
                }
            ],
            "notices": null
        },
        {
            "code": "RAK",
            "name": "Marrakesh",
            "countryCode": "MA",
            "countryName": "Morocco",
            "countryGroupCode": "1",
            "countryGroupName": "non EU/EEA",
            "timeZoneCode": "MA",
            "latitude": "313630N",
            "longitude": "0080218W",
            "mobileBoardingPass": false,
            "markets": [
                {
                    "code": "BGY"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DLE"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "LTN"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "PIS"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "SVQ"
                },
                {
                    "code": "TUF"
                },
                {
                    "code": "XCR"
                }
            ],
            "notices": null
        },
        {
            "code": "MRS",
            "name": "Marseille MP2",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "432608N",
            "longitude": "0051259E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AGA"
                },
                {
                    "code": "AGP"
                },
                {
                    "code": "BES"
                },
                {
                    "code": "CAG"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "CTA"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "ESU"
                },
                {
                    "code": "FEZ"
                },
                {
                    "code": "GSE"
                },
                {
                    "code": "IBZ"
                },
                {
                    "code": "LIL"
                },
                {
                    "code": "LIS"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "NDR"
                },
                {
                    "code": "NTE"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "OUD"
                },
                {
                    "code": "PIS"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "PMO"
                },
                {
                    "code": "RAK"
                },
                {
                    "code": "RBA"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "SVQ"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TNG"
                },
                {
                    "code": "TUF"
                },
                {
                    "code": "VLC"
                },
                {
                    "code": "WMI"
                },
                {
                    "code": "ZAD"
                }
            ],
            "notices": null
        },
        {
            "code": "FMM",
            "name": "Memmingen (Munich West)",
            "countryCode": "DE",
            "countryName": "Germany",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "DE",
            "latitude": "475933N",
            "longitude": "0101437E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AGP"
                },
                {
                    "code": "AHO"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "BUD"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "SNN"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TPS"
                }
            ],
            "notices": null
        },
        {
            "code": "BGY",
            "name": "Milan Bergamo",
            "countryCode": "IT",
            "countryName": "Italy",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IT",
            "latitude": "454006N",
            "longitude": "0094201E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ACE"
                },
                {
                    "code": "AGP"
                },
                {
                    "code": "AHO"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "ATH"
                },
                {
                    "code": "BCN"
                },
                {
                    "code": "BDS"
                },
                {
                    "code": "BLL"
                },
                {
                    "code": "BRE"
                },
                {
                    "code": "BRI"
                },
                {
                    "code": "BRS"
                },
                {
                    "code": "BTS"
                },
                {
                    "code": "BUD"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CAG"
                },
                {
                    "code": "CFU"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "CPH"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "CRV"
                },
                {
                    "code": "CTA"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "EFL"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "FEZ"
                },
                {
                    "code": "FUE"
                },
                {
                    "code": "GSE"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "IBZ"
                },
                {
                    "code": "KGS"
                },
                {
                    "code": "KLX"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "LBC"
                },
                {
                    "code": "LDE"
                },
                {
                    "code": "LIS"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "LPP"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "NOC"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "ORK"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "PMO"
                },
                {
                    "code": "PSR"
                },
                {
                    "code": "RAK"
                },
                {
                    "code": "RHO"
                },
                {
                    "code": "RIX"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "SCQ"
                },
                {
                    "code": "SDR"
                },
                {
                    "code": "SKG"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "SUF"
                },
                {
                    "code": "SVQ"
                },
                {
                    "code": "SXF"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TLL"
                },
                {
                    "code": "TMP"
                },
                {
                    "code": "TPS"
                },
                {
                    "code": "VLC"
                },
                {
                    "code": "VNO"
                },
                {
                    "code": "WMI"
                },
                {
                    "code": "WRO"
                },
                {
                    "code": "ZAZ"
                }
            ],
            "notices": null
        },
        {
            "code": "MPL",
            "name": "Montpellier",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "433451N",
            "longitude": "0035803E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "CRL"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "LBA"
                }
            ],
            "notices": null
        },
        {
            "code": "FMO",
            "name": "Munster Airport",
            "countryCode": "DE",
            "countryName": "Germany",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "DE",
            "latitude": "520805N",
            "longitude": "0074105E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AGP"
                }
            ],
            "notices": null
        },
        {
            "code": "MJV",
            "name": "Murcia",
            "countryCode": "ES",
            "countryName": "Spain",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "ES1",
            "latitude": "374629N",
            "longitude": "0004840W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BHX"
                },
                {
                    "code": "BOH"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "LBA"
                },
                {
                    "code": "LTN"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "PIK"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "NDR",
            "name": "Nador International Airport  ",
            "countryCode": "MA",
            "countryName": "Morocco",
            "countryGroupCode": "1",
            "countryGroupName": "non EU/EEA",
            "timeZoneCode": "MA",
            "latitude": "350900N",
            "longitude": "0025500W",
            "mobileBoardingPass": false,
            "markets": [
                {
                    "code": "BCN"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "MRS"
                }
            ],
            "notices": null
        },
        {
            "code": "NTE",
            "name": "Nantes",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "470930N",
            "longitude": "0013635W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BRS"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "FEZ"
                },
                {
                    "code": "MRS"
                }
            ],
            "notices": null
        },
        {
            "code": "NCL",
            "name": "Newcastle",
            "countryCode": "GB",
            "countryName": "United Kingdom",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "GB",
            "latitude": "550216N",
            "longitude": "0014112W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "DUB"
                }
            ],
            "notices": null
        },
        {
            "code": "NCE",
            "name": "Nice T1",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "433950N",
            "longitude": "0071303E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "DUB"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "SNN"
                }
            ],
            "notices": null
        },
        {
            "code": "FNI",
            "name": "Nimes",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "434500N",
            "longitude": "0042500E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "CRL"
                },
                {
                    "code": "FEZ"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "LTN"
                }
            ],
            "notices": null
        },
        {
            "code": "NUE",
            "name": "Nuremberg Airport",
            "countryCode": "DE",
            "countryName": "Germany",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "DE",
            "latitude": "492955N",
            "longitude": "0110401E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AGP"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "OSI",
            "name": "Osijek",
            "countryCode": "HR",
            "countryName": "Croatia",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "HR",
            "latitude": "453300N",
            "longitude": "0184100E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "RYG",
            "name": "Oslo Rygge",
            "countryCode": "NO",
            "countryName": "Norway",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "NO",
            "latitude": "592244N",
            "longitude": "0104708E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AGP"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "BZR"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "GDN"
                },
                {
                    "code": "IBZ"
                },
                {
                    "code": "KGS"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "KUN"
                },
                {
                    "code": "LCJ"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "PFO"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "POZ"
                },
                {
                    "code": "PUY"
                },
                {
                    "code": "RIX"
                },
                {
                    "code": "RJK"
                },
                {
                    "code": "RZE"
                },
                {
                    "code": "SKG"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "SXF"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TLL"
                },
                {
                    "code": "VNO"
                },
                {
                    "code": "WMI"
                },
                {
                    "code": "WRO"
                },
                {
                    "code": "ZAD"
                }
            ],
            "notices": null
        },
        {
            "code": "TRF",
            "name": "Oslo Torp",
            "countryCode": "NO",
            "countryName": "Norway",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "NO",
            "latitude": "591100N",
            "longitude": "0101600E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AGP"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "OSR",
            "name": "Ostrava Airport",
            "countryCode": "CZ",
            "countryName": "Czech Republic",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "CZ",
            "latitude": "494147N",
            "longitude": "0181700E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "OTP",
            "name": "Otopeni International Airport",
            "countryCode": "RO",
            "countryName": "Romania",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "RO",
            "latitude": "443400N",
            "longitude": "0260600E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "OUD",
            "name": "Oujda",
            "countryCode": "MA",
            "countryName": "Morocco",
            "countryGroupCode": "1",
            "countryGroupName": "non EU/EEA",
            "timeZoneCode": "MA",
            "latitude": "344700N",
            "longitude": "0015600W",
            "mobileBoardingPass": false,
            "markets": [
                {
                    "code": "BVA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "NRN"
                }
            ],
            "notices": null
        },
        {
            "code": "PMO",
            "name": "Palermo",
            "countryCode": "IT",
            "countryName": "Italy",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IT",
            "latitude": "381000N",
            "longitude": "0130600E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BGY"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "FCO"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "SVQ"
                },
                {
                    "code": "TSF"
                }
            ],
            "notices": null
        },
        {
            "code": "PMI",
            "name": "Palma",
            "countryCode": "ES",
            "countryName": "Spain",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "ES1",
            "latitude": "393306N",
            "longitude": "0024420E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AGP"
                },
                {
                    "code": "BCN"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BHX"
                },
                {
                    "code": "BLL"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BOH"
                },
                {
                    "code": "BRE"
                },
                {
                    "code": "BRS"
                },
                {
                    "code": "BRU"
                },
                {
                    "code": "BTS"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CGN"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DTM"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "FKB"
                },
                {
                    "code": "FMM"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "GSE"
                },
                {
                    "code": "HAU"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "KUN"
                },
                {
                    "code": "LBA"
                },
                {
                    "code": "LBC"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "ORK"
                },
                {
                    "code": "PIK"
                },
                {
                    "code": "POZ"
                },
                {
                    "code": "REU"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "SCQ"
                },
                {
                    "code": "SDR"
                },
                {
                    "code": "SNN"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "SVQ"
                },
                {
                    "code": "VLC"
                },
                {
                    "code": "WMI"
                }
            ],
            "notices": null
        },
        {
            "code": "PFO",
            "name": "Paphos International",
            "countryCode": "CY",
            "countryName": "Cyprus",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "CY",
            "latitude": "344305N",
            "longitude": "0322909E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ATH"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "GPA"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "KUN"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "SKG"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "BVA",
            "name": "Paris Beauvais",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "492721N",
            "longitude": "0020645E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ACE"
                },
                {
                    "code": "AGP"
                },
                {
                    "code": "AHO"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "BCN"
                },
                {
                    "code": "BDS"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BRI"
                },
                {
                    "code": "BTS"
                },
                {
                    "code": "BUD"
                },
                {
                    "code": "BZR"
                },
                {
                    "code": "CAG"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "FEZ"
                },
                {
                    "code": "FSC"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "GSE"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "LIS"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "NDR"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "OUD"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "PSR"
                },
                {
                    "code": "RAK"
                },
                {
                    "code": "RBA"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "SKG"
                },
                {
                    "code": "SNN"
                },
                {
                    "code": "SVQ"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TNG"
                },
                {
                    "code": "TPS"
                },
                {
                    "code": "TSF"
                },
                {
                    "code": "VLC"
                },
                {
                    "code": "VNO"
                },
                {
                    "code": "WMI"
                },
                {
                    "code": "WRO"
                },
                {
                    "code": "ZAD"
                },
                {
                    "code": "ZAZ"
                }
            ],
            "notices": null
        },
        {
            "code": "XCR",
            "name": "Paris Vatry ( Disneyland)",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "484604N",
            "longitude": "0041204E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "OPO"
                },
                {
                    "code": "RAK"
                }
            ],
            "notices": null
        },
        {
            "code": "PMF",
            "name": "Parma",
            "countryCode": "IT",
            "countryName": "Italy",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IT",
            "latitude": "444800N",
            "longitude": "0102000E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "CAG"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TPS"
                }
            ],
            "notices": null
        },
        {
            "code": "GPA",
            "name": "Patras, Araxos",
            "countryCode": "GR",
            "countryName": "Greece",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "GR",
            "latitude": "380904N",
            "longitude": "0212532E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "PFO"
                }
            ],
            "notices": null
        },
        {
            "code": "PGF",
            "name": "Perpignan",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "424427N",
            "longitude": "0025218E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BHX"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "PEG",
            "name": "Perugia",
            "countryCode": "IT",
            "countryName": "Italy",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IT",
            "latitude": "430800N",
            "longitude": "0122200E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "CAG"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TPS"
                }
            ],
            "notices": null
        },
        {
            "code": "PSR",
            "name": "Pescara",
            "countryCode": "IT",
            "countryName": "Italy",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IT",
            "latitude": "422602N",
            "longitude": "0141104E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BGY"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "PSA",
            "name": "Pisa",
            "countryCode": "IT",
            "countryName": "Italy",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IT",
            "latitude": "434050N",
            "longitude": "0102334E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AHO"
                },
                {
                    "code": "BDS"
                },
                {
                    "code": "BLL"
                },
                {
                    "code": "BRI"
                },
                {
                    "code": "BUD"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CAG"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "CIY"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "CRV"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "EFL"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "FEZ"
                },
                {
                    "code": "FUE"
                },
                {
                    "code": "GDN"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "GSE"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "IBZ"
                },
                {
                    "code": "KGS"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "LBA"
                },
                {
                    "code": "LBC"
                },
                {
                    "code": "LIS"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "ORK"
                },
                {
                    "code": "PIK"
                },
                {
                    "code": "PMO"
                },
                {
                    "code": "RAK"
                },
                {
                    "code": "RHO"
                },
                {
                    "code": "SKG"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "SUF"
                },
                {
                    "code": "SVQ"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TPS"
                },
                {
                    "code": "VLC"
                },
                {
                    "code": "WMI"
                },
                {
                    "code": "WRO"
                }
            ],
            "notices": null
        },
        {
            "code": "PDV",
            "name": "Plovdiv International Airport ",
            "countryCode": "BG",
            "countryName": "Bulgaria",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "BG",
            "latitude": "420404N",
            "longitude": "0245103E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "HHN"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "TGD",
            "name": "Podgorica Airport",
            "countryCode": "ME",
            "countryName": "Montenegro",
            "countryGroupCode": "1",
            "countryGroupName": "non EU/EEA",
            "timeZoneCode": "ME",
            "latitude": "422130N",
            "longitude": "0191507E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "CRL"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "PIS",
            "name": "Poitiers",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "463510N",
            "longitude": "0001823E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "RAK"
                },
                {
                    "code": "SNN"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "OPO",
            "name": "Porto",
            "countryCode": "PT",
            "countryName": "Portugal",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "PT1",
            "latitude": "411004N",
            "longitude": "0084000W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BCN"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BHX"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BOD"
                },
                {
                    "code": "BRE"
                },
                {
                    "code": "BRS"
                },
                {
                    "code": "BRU"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CCF"
                },
                {
                    "code": "CFE"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DLE"
                },
                {
                    "code": "DTM"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EBU"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "FKB"
                },
                {
                    "code": "FMM"
                },
                {
                    "code": "HAM"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "LIL"
                },
                {
                    "code": "LIS"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "LRH"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "MST"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "PIS"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "RNS"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "SXB"
                },
                {
                    "code": "SXF"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TUF"
                },
                {
                    "code": "VLC"
                },
                {
                    "code": "XCR"
                }
            ],
            "notices": null
        },
        {
            "code": "POZ",
            "name": "Poznan",
            "countryCode": "PL",
            "countryName": "Poland",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "PL",
            "latitude": "522501N",
            "longitude": "0165300E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BRS"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "PRG",
            "name": "Prague",
            "countryCode": "CZ",
            "countryName": "Czech Republic",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "CZ",
            "latitude": "500603N",
            "longitude": "0141536E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "PUY",
            "name": "Pula",
            "countryCode": "HR",
            "countryName": "Croatia",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "HR",
            "latitude": "445330N",
            "longitude": "0135525E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "CRL"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "RBA",
            "name": "Rabat ",
            "countryCode": "MA",
            "countryName": "Morocco",
            "countryGroupCode": "1",
            "countryGroupName": "non EU/EEA",
            "timeZoneCode": "MA",
            "latitude": "340300N",
            "longitude": "0064525W",
            "mobileBoardingPass": false,
            "markets": [
                {
                    "code": "BVA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        //TODO: does not exist in desktop version
        // {
        //    "code": "RNS",
        //    "name": "Rennes Airport",
        //    "countryCode": "FR",
        //    "countryName": "France",
        //    "countryGroupCode": "0",
        //    "countryGroupName": "EU/EEA",
        //    "timeZoneCode": "FR",
        //    "latitude": "480407N",
        //    "longitude": "0014353W",
        //    "mobileBoardingPass": true,
        //    "markets": [
        //        {
        //            "code": "OPO"
        //        }
        //    ],
        //    "notices": null
        //},
        {
            "code": "RHO",
            "name": "Rhodes",
            "countryCode": "GR",
            "countryName": "Greece",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "GR",
            "latitude": "362420N",
            "longitude": "0280510E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ATH"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "KUN"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "RIX",
            "name": "Riga",
            "countryCode": "LV",
            "countryName": "Latvia",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "LV",
            "latitude": "565530N",
            "longitude": "0235820E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BGY"
                },
                {
                    "code": "BRE"
                },
                {
                    "code": "CGN"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "GLA"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "LBA"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": [
                {
                    "title": "IMPORTANT",
                    "message": "Notice: In order to proceed through security at Riga Airport, passengers must pay Riga Airport a charge of €7. This charge is unlawful. Until Riga Airport is required by court judgement to cease collecting this charge, we advise passengers to pay it and retain their receipt."
                }
            ]
        },
        {
            "code": "RJK",
            "name": "Rijeka",
            "countryCode": "HR",
            "countryName": "Croatia",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "HR",
            "latitude": "451257N",
            "longitude": "0143406E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "CRL"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "RDZ",
            "name": "Rodez",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "442200N",
            "longitude": "0023100E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "FCO",
            "name": "Rome (Fiumicino)",
            "countryCode": "IT",
            "countryName": "Italy",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IT",
            "latitude": "414801N",
            "longitude": "0121420E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BCN"
                },
                {
                    "code": "BRU"
                },
                {
                    "code": "CTA"
                },
                {
                    "code": "PMO"
                },
                {
                    "code": "SUF"
                }
            ],
            "notices": null
        },
        {
            "code": "CIA",
            "name": "Rome Ciampino",
            "countryCode": "IT",
            "countryName": "Italy",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IT",
            "latitude": "414758N",
            "longitude": "0123536E",
            "mobileBoardingPass": false,
            "markets": [
                {
                    "code": "AGP"
                },
                {
                    "code": "AHO"
                },
                {
                    "code": "ATH"
                },
                {
                    "code": "BCN"
                },
                {
                    "code": "BDS"
                },
                {
                    "code": "BLL"
                },
                {
                    "code": "BOD"
                },
                {
                    "code": "BRI"
                },
                {
                    "code": "BTS"
                },
                {
                    "code": "BUD"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CAG"
                },
                {
                    "code": "CFU"
                },
                {
                    "code": "CGN"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "CIY"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "CRV"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "FEZ"
                },
                {
                    "code": "FKB"
                },
                {
                    "code": "FMM"
                },
                {
                    "code": "GDN"
                },
                {
                    "code": "GSE"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "IBZ"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "LIS"
                },
                {
                    "code": "LTN"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "PFO"
                },
                {
                    "code": "PIK"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "POZ"
                },
                {
                    "code": "RAK"
                },
                {
                    "code": "SCQ"
                },
                {
                    "code": "SDR"
                },
                {
                    "code": "SKG"
                },
                {
                    "code": "SNN"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "SVQ"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TMP"
                },
                {
                    "code": "TPS"
                },
                {
                    "code": "TRF"
                },
                {
                    "code": "VLC"
                },
                {
                    "code": "VNO"
                },
                {
                    "code": "WMI"
                },
                {
                    "code": "WRO"
                }
            ],
            "notices": null
        },
        {
            "code": "RZE",
            "name": "Rzeszow",
            "countryCode": "PL",
            "countryName": "Poland",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "PL",
            "latitude": "500700N",
            "longitude": "0220100E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BRS"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "LTN"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "SZG",
            "name": "Salzburg",
            "countryCode": "AT",
            "countryName": "Austria",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "AT",
            "latitude": "474735N",
            "longitude": "0130020E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "DUB"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "SDR",
            "name": "Santander",
            "countryCode": "ES",
            "countryName": "Spain",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "ES1",
            "latitude": "432545N",
            "longitude": "0034935W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ACE"
                },
                {
                    "code": "AGP"
                },
                {
                    "code": "BCN"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "SVQ"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "VLC"
                }
            ],
            "notices": null
        },
        {
            "code": "SCQ",
            "name": "Santiago Di Comp",
            "countryCode": "ES",
            "countryName": "Spain",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "ES1",
            "latitude": "425347N",
            "longitude": "0082455W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ACE"
                },
                {
                    "code": "AGP"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "BCN"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "SVQ"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "VLC"
                }
            ],
            "notices": null
        },
        {
            "code": "SVQ",
            "name": "Seville",
            "countryCode": "ES",
            "countryName": "Spain",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "ES1",
            "latitude": "372505N",
            "longitude": "0055335W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ACE"
                },
                {
                    "code": "BCN"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BOD"
                },
                {
                    "code": "BRI"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "LGW"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "PMO"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "RAK"
                },
                {
                    "code": "SCQ"
                },
                {
                    "code": "SDR"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TSF"
                }
            ],
            "notices": null
        },
        {
            "code": "SNN",
            "name": "Shannon",
            "countryCode": "IE",
            "countryName": "Ireland",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IE",
            "latitude": "524202N",
            "longitude": "0085507W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ACE"
                },
                {
                    "code": "AGP"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "BRS"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CWL"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "FMM"
                },
                {
                    "code": "FUE"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "KUN"
                },
                {
                    "code": "LGW"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "NCE"
                },
                {
                    "code": "PIS"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "SXF"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "WMI"
                },
                {
                    "code": "WRO"
                }
            ],
            "notices": null
        },
        {
            "code": "SFT",
            "name": "Skelleftea",
            "countryCode": "SE",
            "countryName": "Sweden",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "SE",
            "latitude": "643730N",
            "longitude": "0210442E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "GRO"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "VXO",
            "name": "Smaland Vaxjo",
            "countryCode": "SE",
            "countryName": "Sweden",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "SE",
            "latitude": "565600N",
            "longitude": "0144400E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ALC"
                },
                {
                    "code": "NRN"
                }
            ],
            "notices": null
        },
        {
            "code": "EBU",
            "name": "St Etienne",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "453229N",
            "longitude": "0041749E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "FEZ"
                },
                {
                    "code": "OPO"
                }
            ],
            "notices": null
        },
        {
            "code": "NYO",
            "name": "Stockholm Skavsta",
            "countryCode": "SE",
            "countryName": "Sweden",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "SE",
            "latitude": "584500N",
            "longitude": "0170000E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AGP"
                },
                {
                    "code": "AHO"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "AOI"
                },
                {
                    "code": "BCN"
                },
                {
                    "code": "BDS"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BIQ"
                },
                {
                    "code": "BRE"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "BZR"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "FAO"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "PFO"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "RHO"
                },
                {
                    "code": "RJK"
                },
                {
                    "code": "SKG"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TSF"
                },
                {
                    "code": "VLC"
                },
                {
                    "code": "WMI"
                },
                {
                    "code": "ZAD"
                }
            ],
            "notices": null
        },
        {
            "code": "SXB",
            "name": "Strasbourg Airport",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "483200N",
            "longitude": "0073800E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "OPO"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "SZZ",
            "name": "Szczecin",
            "countryCode": "PL",
            "countryName": "Poland",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "PL",
            "latitude": "533500N",
            "longitude": "0145400E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "DUB"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "TLL",
            "name": "Tallinn",
            "countryCode": "EE",
            "countryName": "Estonia",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "EE",
            "latitude": "592448N",
            "longitude": "0244957E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BGY"
                },
                {
                    "code": "BLL"
                },
                {
                    "code": "BRE"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "TMP",
            "name": "Tampere",
            "countryCode": "FI",
            "countryName": "Finland",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FI",
            "latitude": "612750N",
            "longitude": "0234420E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AGP"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BRE"
                },
                {
                    "code": "BUD"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "TNG",
            "name": "Tangier Batouta Airport ",
            "countryCode": "MA",
            "countryName": "Morocco",
            "countryGroupCode": "1",
            "countryGroupName": "non EU/EEA",
            "timeZoneCode": "MA",
            "latitude": "354300N",
            "longitude": "0055500W",
            "mobileBoardingPass": false,
            "markets": [
                {
                    "code": "BVA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MRS"
                }
            ],
            "notices": null
        },
        {
            "code": "TFN",
            "name": "Tenerife",
            "countryCode": "ES",
            "countryName": "Spain",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "ES2",
            "latitude": "282858N",
            "longitude": "0162030W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BCN"
                },
                {
                    "code": "MAD"
                }
            ],
            "notices": null
        },
        {
            "code": "TFS",
            "name": "Tenerife South",
            "countryCode": "ES",
            "countryName": "Spain",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "ES2",
            "latitude": "280240N",
            "longitude": "0163421W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BCN"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BHX"
                },
                {
                    "code": "BLL"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BOH"
                },
                {
                    "code": "BRE"
                },
                {
                    "code": "BRS"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CGN"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "CWL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "FKB"
                },
                {
                    "code": "FMM"
                },
                {
                    "code": "FUE"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "LBA"
                },
                {
                    "code": "LNZ"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "LTN"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "MST"
                },
                {
                    "code": "NOC"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "ORK"
                },
                {
                    "code": "OVD"
                },
                {
                    "code": "PIK"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "SCQ"
                },
                {
                    "code": "SDR"
                },
                {
                    "code": "SNN"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "SVQ"
                },
                {
                    "code": "VLC"
                },
                {
                    "code": "WMI"
                },
                {
                    "code": "ZAZ"
                }
            ],
            "notices": null
        },
        {
            "code": "SKG",
            "name": "Thessaloniki",
            "countryCode": "GR",
            "countryName": "Greece",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "GR",
            "latitude": "403111N",
            "longitude": "0225815E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ATH"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BRE"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "FKB"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "PFO"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "WMI"
                }
            ],
            "notices": null
        },
        {
            "code": "TLN",
            "name": "Toulon",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "430600N",
            "longitude": "0060900E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "DUB"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "TUF",
            "name": "Tours",
            "countryCode": "FR",
            "countryName": "France",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "FR",
            "latitude": "472542N",
            "longitude": "0004339E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "DUB"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "RAK"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "TPS",
            "name": "Trapani",
            "countryCode": "IT",
            "countryName": "Italy",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IT",
            "latitude": "375442N",
            "longitude": "0122911E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AOI"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BTS"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CAG"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "CUF"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "FKB"
                },
                {
                    "code": "FMM"
                },
                {
                    "code": "GOA"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "KRK"
                },
                {
                    "code": "KUN"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "PEG"
                },
                {
                    "code": "PMF"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "TRN"
                },
                {
                    "code": "TRS"
                },
                {
                    "code": "TSF"
                },
                {
                    "code": "WMI"
                }
            ],
            "notices": null
        },
        {
            "code": "TRS",
            "name": "Trieste",
            "countryCode": "IT",
            "countryName": "Italy",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IT",
            "latitude": "455000N",
            "longitude": "0132800E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AHO"
                },
                {
                    "code": "BRI"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TPS"
                },
                {
                    "code": "VLC"
                }
            ],
            "notices": null
        },
        {
            "code": "TRN",
            "name": "Turin",
            "countryCode": "IT",
            "countryName": "Italy",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IT",
            "latitude": "451204N",
            "longitude": "0073853E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AHO"
                },
                {
                    "code": "BCN"
                },
                {
                    "code": "BDS"
                },
                {
                    "code": "BRI"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "CTA"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TPS"
                }
            ],
            "notices": null
        },
        {
            "code": "VLC",
            "name": "Valencia",
            "countryCode": "ES",
            "countryName": "Spain",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "ES1",
            "latitude": "392925N",
            "longitude": "0002843W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BGY"
                },
                {
                    "code": "BLL"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BRI"
                },
                {
                    "code": "BRS"
                },
                {
                    "code": "BRU"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "IBZ"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "MAH"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "OPO"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "SCQ"
                },
                {
                    "code": "SDR"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TRS"
                },
                {
                    "code": "TSF"
                }
            ],
            "notices": null
        },
        {
            "code": "VLL",
            "name": "Valladolid",
            "countryCode": "ES",
            "countryName": "Spain",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "ES1",
            "latitude": "414222N",
            "longitude": "0045107W",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ACE"
                },
                {
                    "code": "BCN"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "VST",
            "name": "Vasteras",
            "countryCode": "SE",
            "countryName": "Sweden",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "SE",
            "latitude": "593521N",
            "longitude": "0163750E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AGP"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "TSF",
            "name": "Venice Treviso",
            "countryCode": "IT",
            "countryName": "Italy",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "IT",
            "latitude": "454000N",
            "longitude": "0121500E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AGP"
                },
                {
                    "code": "AHO"
                },
                {
                    "code": "BCN"
                },
                {
                    "code": "BDS"
                },
                {
                    "code": "BLL"
                },
                {
                    "code": "BRE"
                },
                {
                    "code": "BRI"
                },
                {
                    "code": "BRS"
                },
                {
                    "code": "BRU"
                },
                {
                    "code": "BUD"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CAG"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "CTA"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "LBA"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "PMO"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "SUF"
                },
                {
                    "code": "SVQ"
                },
                {
                    "code": "TPS"
                },
                {
                    "code": "VLC"
                }
            ],
            "notices": null
        },
        {
            "code": "VNO",
            "name": "Vilnius",
            "countryCode": "LT",
            "countryName": "Lithuania",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "LT",
            "latitude": "543800N",
            "longitude": "0251700E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BCN"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BRE"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "LTN"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "WMI",
            "name": "Warsaw Modlin",
            "countryCode": "PL",
            "countryName": "Poland",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "PL",
            "latitude": "522704N",
            "longitude": "0203906E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "ALC"
                },
                {
                    "code": "ATH"
                },
                {
                    "code": "BCN"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BRS"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "CPH"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EIN"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "FUE"
                },
                {
                    "code": "GDN"
                },
                {
                    "code": "GLA"
                },
                {
                    "code": "LIS"
                },
                {
                    "code": "LPA"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "MAD"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "PMI"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "SKG"
                },
                {
                    "code": "SNN"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TFS"
                },
                {
                    "code": "TPS"
                },
                {
                    "code": "WRO"
                }
            ],
            "notices": null
        },
        {
            "code": "WRO",
            "name": "Wroclaw",
            "countryCode": "PL",
            "countryName": "Poland",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "PL",
            "latitude": "510600N",
            "longitude": "0165300E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "AGP"
                },
                {
                    "code": "ALC"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BLQ"
                },
                {
                    "code": "BRS"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CHQ"
                },
                {
                    "code": "CIA"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "EDI"
                },
                {
                    "code": "EMA"
                },
                {
                    "code": "GDN"
                },
                {
                    "code": "GLA"
                },
                {
                    "code": "GRO"
                },
                {
                    "code": "LPL"
                },
                {
                    "code": "MLA"
                },
                {
                    "code": "ORK"
                },
                {
                    "code": "PSA"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "SNN"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "WMI"
                }
            ],
            "notices": null
        },
        {
            "code": "ZAD",
            "name": "Zadar",
            "countryCode": "HR",
            "countryName": "Croatia",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "HR",
            "latitude": "440552N",
            "longitude": "0152124E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "BVA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "DUB"
                },
                {
                    "code": "FKB"
                },
                {
                    "code": "GSE"
                },
                {
                    "code": "HHN"
                },
                {
                    "code": "MAN"
                },
                {
                    "code": "MRS"
                },
                {
                    "code": "NRN"
                },
                {
                    "code": "NYO"
                },
                {
                    "code": "RYG"
                },
                {
                    "code": "STN"
                }
            ],
            "notices": null
        },
        {
            "code": "ZTH",
            "name": "Zakinthos",
            "countryCode": "GR",
            "countryName": "Greece",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "GR",
            "latitude": "374503N",
            "longitude": "0205303E",
            "mobileBoardingPass": true,
            "markets": [
                {
                    "code": "CRL"
                }
            ],
            "notices": null
        },
        {
            "code": "ZAZ",
            "name": "Zaragoza",
            "countryCode": "ES",
            "countryName": "Spain",
            "countryGroupCode": "0",
            "countryGroupName": "EU/EEA",
            "timeZoneCode": "ES1",
            "latitude": "413958N",
            "longitude": "0010230W",
            "mobileBoardingPass": false,
            "markets": [
                {
                    "code": "ACE"
                },
                {
                    "code": "BGY"
                },
                {
                    "code": "BVA"
                },
                {
                    "code": "CRL"
                },
                {
                    "code": "STN"
                },
                {
                    "code": "TFS"
                }
            ],
            "notices": null
        }]
    ;
var stations = function () {
    this.getAirportsNamesByCountry = function (country) {
        var airports = [];
        var count = 0;
        for (var i in stationsData) {
            if (!stationsData.hasOwnProperty(i)) continue;
            if (typeof stationsData[i] == 'object') {
                if (stationsData[i].countryName == country) {
                    airports.push(stationsData[i].name);
                    count++
                }

            }
        }
        return airports

    }

    this.getAirportCodeByCountry = function (country) {
        var airports = [];
        var count = 0;
        for (var i in stationsData) {
            if (!stationsData.hasOwnProperty(i)) continue;
            if (typeof stationsData[i] == 'object') {
                if (stationsData[i].countryName == country) {
                    airports.push(stationsData[i].code);
                    count++
                }

            }
        }
        //console.log(airports);
        //console.log(count);
        return airports

    };


    this.getRandomCountry = function () {
        var item = stationsData[Math.floor(Math.random() * stationsData.length)];
        //console.log("stationsData[Math.floor(Math.random()*stationsData.length)]");
        //console.log(item.countryName);
        return item.countryName

    };

    this.getAllCountries = function () {
        var countires = [];
        for (i = 0; i < stationsData.length; i++) {
            countires[i] = stationsData[i].countryName;
            //console.log(countires[i]);
        }
        var item = stationsData[Math.floor(Math.random() * stationsData.length)];
        return countires
    };


};
module.exports = stations;

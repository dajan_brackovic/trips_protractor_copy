var HtmlScreenshotReporter = require('../lib/modifiedJasmine2ScreenShotReporter'),
    ReportCollector = require('../lib/ReportCollector'),
    helpers = require('./helpers.js'),
    REPORT_DEST = 'test-reports',
    PROTRACTOR_ERROR_REPORT_NAME = 'protractorErrorLog';

console.log("***************************************");
console.log("********* FR DESKTOP TRIPS ************");
console.log("*********   JS FRAMEWORK   ************");
console.log("******** FR AUTOMATION TEAM ***********");
console.log("***************************************\n\n");


var frGridUrl = 'http://10.11.17.50:4440/wd/hub';

var protractorErrors = [];

var uat = 'https://uat-aem.ryanair.com/ie/en/';
var sit = 'https://sit-aem.ryanair.com/ie/en/';
var oat = 'https://oat-aem.ryanair.com/ie/en/';
var prod = 'https://ryanair.com/ie/en/';
var dev = 'https://dev-aem.ryanair.com/ie/en/';
var test06 = 'http://choaemtst06.cho.corp.ryanair.com:4503/ie/en.html/';
var dev1 = 'http://52.29.227.66/gb/en/';

var browsers = {
    firefox: {
        name: 'Firefox',
        browserName: 'firefox'
    },
    chrome: {
        name: 'Chrome',
        browserName: 'chrome'
    },
    ios: {
        name: 'iOS 8 - iPhone',
        platformName: 'iOS',
        platformVersion: '8.3',
        deviceName: 'iPhone Simulator',
        browserName: 'Safari',
        orientation: 'portrait'
    },
    iosPad: {
        name: 'iOS 8 - iPad',
        platformName: 'iOS',
        platformVersion: '8.3',
        deviceName: 'iPad Simulator',
        browserName: 'Safari'
    },
    android: {
        name: 'Android - 4.4',
        platformName: 'Android',
        platformVersion: '4.4',
        deviceName: '192.168.56.101:5555',
        browserName: 'Chrome'
    },
    chromeMulti: {
        name: 'Chrome',
        browserName: 'chrome',
        platform: 'WINDOWS',
        shardTestFiles: true,
        maxInstances: 45
    },
    fireFoxMulti: {
        name: 'Firefox',
        browserName: 'firefox',
        platform: 'WINDOWS',
        shardTestFiles: true,
        maxInstances: 45
    },
    ieMulti: {
        name: 'IE',
        browserName: 'internet explorer',
        platform: 'WINDOWS',
        shardTestFiles: true,
        maxInstances: 10
    }
};

function registerProtractorErrorLogger() {
    protractor.promise.controlFlow().on('uncaughtException', function (e) {
        protractorErrors.push(e);
    });
}

exports.config = {
    framework: 'jasmine2',

    plugins: [{
        path: '../lib/waitPlugin.js'
    }],

    beforeLaunch: function () {
        helpers.deleteFolderRecursive(REPORT_DEST);
    },

    onPrepare: function () {
        registerProtractorErrorLogger();
        browser.driver.manage().window().maximize();
        browser.params = require('./suites/Trips3Data.js');
        var reporter = new HtmlScreenshotReporter({
            dest: REPORT_DEST,
            filename: 'report.html',
            runNumber: process.argv[6]
        });
        global.reporter = reporter;
        jasmine.getEnv().addReporter(reporter);
        var Actions = require('../test/Actions');
        global.actions = new Actions();
    },

    onComplete: function () {
        console.log("suite tests complete --> ");

    },

    afterLaunch: function () {
        if (protractorErrors.length) {
            var dest = REPORT_DEST + '/' + PROTRACTOR_ERROR_REPORT_NAME;
            var stackTrace = helpers.buildStackTraces(protractorErrors);
            helpers.saveFile(dest, stackTrace);
        }
        console.log("Report Collector running...")
        var collector = new ReportCollector(REPORT_DEST);
        collector.collect();

    },

    allScriptsTimeout: 165000,

    // Options to be passed to Jasmine.
    jasmineNodeOpts: {
        showColors: true,
        includeStackTrace: true,
        defaultTimeoutInterval: 160000
    }

};
console.log('Number of arguments ---->  ' + process.argv.length);
console.log('****runSuite**** ---->  ' + process.argv[3]);
// jenkins config
if (process.argv.length === 7) {
    var runSuite = process.argv[3];
    var runBrowser = process.argv[4];
    var runBaseUrl = process.argv[5];

    exports.config.seleniumAddress = frGridUrl;
    exports.config.directConnect = false;
    var runSuiteName = runSuite.substring(2);
    console.log('****runSuite**** ---->  ' + runSuiteName);
    var suiteConfig = require('./suites/' + runSuiteName + '.js').conf.suite;
    exports.config.suites = {
        suite: suiteConfig
    };

    if (runBrowser === '--firefox') {
        exports.config.capabilities = browsers.fireFoxMulti;

    } else if (runBrowser === '--chrome') {
        exports.config.capabilities = browsers.chromeMulti;

    } else if (runBrowser === '--ie') {
        exports.config.capabilities = browsers.ieMulti;
    } else {
        exports.config.capabilities = browsers.chromeMulti;
    }

    if (runBaseUrl === '--sit') {
        exports.config.baseUrl = sit
    } else if (runBaseUrl === '--uat') {
        exports.config.baseUrl = uat
    } else if (runBaseUrl === '--oat') {
        exports.config.baseUrl = oat
    } else if (runBaseUrl === '--prod') {
        exports.config.baseUrl = prod
    } else if (runBaseUrl === '--dev') {
        exports.config.baseUrl = dev
    } else if (runBaseUrl === '--dev1') {
        exports.config.baseUrl = dev1
    } else if (runBaseUrl === '--test06') {
        exports.config.baseUrl = test06
    } else {
        exports.config.baseUrl = sit
    }

    console.log("running jenkins regression on Ryanair Grid with " + runSuite + " | " + runBrowser + " | " + runBaseUrl);
} else {

    var devUrl = require('./suites/DevSuite.js').conf.baseUrl;
    setLocalOptions('DevSuite.js', devUrl);
    console.log("running on Local machine on :" + devUrl)

}

function setGridOptions(suite, env) {
    exports.config.seleniumAddress = frGridUrl;
    exports.config.directConnect = false;
    var suiteConfig = require('./suites/' + suite).conf.suite;
    exports.config.suites = {
        suite: suiteConfig
    };

    if (process.argv[4] === '--firefox') {
        exports.config.capabilities = browsers.fireFoxMulti;
        console.log("running grid with " + process.argv[4])

    } else if (process.argv[4] === '--chrome') {
        exports.config.capabilities = browsers.chromeMulti;
        console.log("running grid with " + process.argv[4])

    } else if (process.argv[4] === '--ie') {
        exports.config.capabilities = browsers.ieMulti;
    } else {
        exports.config.capabilities = browsers.chromeMulti;
    }
    console.log("running tests on : " + env);

    exports.config.baseUrl = env
}

function setLocalOptions(suite, env) {
    exports.config.directConnect = require('./suites/DevSuite.js').conf.directConnect;
    var suiteConfig = require('./suites/' + suite).conf.suite;
    exports.config.suites = {
        suite: suiteConfig
    };
    exports.config.capabilities = require('./suites/DevSuite.js').conf.browsers;
    exports.config.baseUrl = env

}
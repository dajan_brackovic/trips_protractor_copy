exports.conf = {
    suite: [
        // TRIPS
        './specs/regression/trips/simplebooking/onewayflight/C28715.js',
        './specs/regression/trips/simplebooking/twowayflight/C28719.js',
        './specs/regression/trips/simplebooking/twowayflight/C28722.js',
        './specs/regression/trips/extrasequipment/C58707.js',
        './specs/regression/trips/checkin/C139787.js',
        './specs/regression/trips/checkin/C58661.js',
        './specs/regression/trips/balearicbooking/C36364.js',
        './specs/regression/trips/balearicbooking/C30463.js',
        './specs/regression/trips/activetrip/C144712.js',
        './specs/regression/trips/paypal/C221228.js',
        './specs/regression/trips/holdfare/C144923.js',
        './specs/regression/trips/seats/C44753.js',
        './specs/regression/trips/seats/C44759.js',

        // MYFR
        './specs/regression/myRyanair/account/C632843.js',
        './specs/regression/myRyanair/companions/C615126.js',
        './specs/regression/myRyanair/dashboard/C596538.js',
        './specs/regression/myRyanair/fohintegration/C634079a.js',

        // FOH
        './specs/regression/foh/smoketests/homepage/C43168.js',
        './specs/regression/foh/smoketests/farefinder/C40503.js',
        './specs/regression/foh/smoketests/timetable/C43134.js',
        './specs/regression/foh/smoketests/flightinfo/C43142.js',

        // add extras
        './specs/regression/trips/airportTransfers/potentialTrip/C632659.js',
        './specs/regression/trips/insurance/potentialTrip/C41838.js',
    ]
};
exports.conf = {
    url: {
        flights: 'http://localhost:5000/ie/en/'
    },
    music: {
        header: 'Musical equipment',
        info: 'Any musical instruments that are larger than the cabin baggage allowance will need to be purchased and placed in the aircraft hold. If you wish to carry a musical instrument with you onboard the aircraft an extra seat must be purchased.',
        extraInfo: 'To book an extra seat for an item the word "ITEM SEAT" must be entered as the surname and "EXTRA" must be entered at the first name. EXTRA ITEM SEAT will then be displayed on the reservation and online boarding pass. The accompanying passenger\'s seat travel document details must be entered during the online check-in process. Spanish Resident and Large Family Discount is not applicable on EXTRA seats. There is no checked-in or cabin baggage allowance associated with the purchase of an extra seat.',
        inbound: 'Flight out',
        outbound: 'Flight back',
        maximum: 'This passenger has reached the maximum number of musical equipment items'
    },
    baby: {
        header: 'Baby equipment',
        info: 'If you travel with an infant or child you can carry one fully collapsible pushchair and another smaller item such as a car seat or travel cot free. Any other items (maximum 20kg) will need to be purchased and placed in the aircraft hold.',
        inbound: 'Flight out',
        outbound: 'Flight back',
        maximum: 'This passenger has reached the maximum number of baby equipment items'
    },
    sport: {
        header: 'Sports equipment',
        info: 'Any sports equipment that will not fit into your cabin bag will need to be checked-in and will be placed in the hold.',
        inbound: 'Flight out',
        outbound: 'Flight back',
        maximum: 'This passenger has reached the maximum allowed number of equipment items.'
    },
    passenger: {
        firstName: 'James',
        lastName: 'Harden'
    },
    trip: {
        messageBefore: 'Your flights are sorted, now what else do you need?',
        messageAfterSport: 'Sports equipment added. Anything else you need?',
        messageAfterBaby: 'Baby equipment added. Anything else you need?',
        messageAfterMusic: 'Music equipment added. Anything else you need?',
        messageAfterInsurance: 'Insurance added. Anything else you need?',
        messageAfterParking: 'Parking added. Anything else you need?',
        messageAfterTransfer: 'Transfer added. Anything else you need?'
    },
    parking: {
        parkingProvider1: 'ParkVia',
        parkingProvider2: 'Looking 4 Parking',
        title: 'Low cost parking',
        openingTimes: 'Opening times:',
        infoTitle: 'Park Mark Award. CCTV, gated entry and perimeter fence',
        infoTitle2: 'CC Car Check Covered parking',
        infoDistance: 'Distance from the airport:',
        infoDistance2: 'Car keys will be stored in a secure safe',
        infoDuration: 'Transfer time to the airport:',
        infoDuration2: 'Individual transfer available on request: : 08:30-18:00',
        infoFrequnecy: 'Bus frequency:',
        infoFrequnecy2: 'Distance from the airport: 800 m',
        km: ' km',
        mins: ' mins',
        year: '-2015',
        year2: '-2016',
        registrationNumber1: 'nesto7689nesto',
        registrationNumber2: 'nesto7689nestoadada4daddad443',
        registrationNumber3: '()"/....',
        registrationNumber4: 'dasda_da',
        registrationNumber5: '.dadsa5s',
        registrationNumber6: '+dasdasd',
        registrationNumber7: ';l[pouzm',
        messageRegistartionNum0: 'Only alphanumerics are allowed',
        messageRegistartionNum1: 'Registration number is required',
        urlPDF: '/content/dam/ryanair/parking-pdfs-and-logos/tnc/',
        urlPDF2: 'https://sit-aem.ryanair.com/content/dam/ryanair/parking-pdfs-and-logos/tnc/',
        info28Message: 'You can book parking for up to a maximum of 28 days.',
        termAndConditionsLinkSTN: 'https://parkcloud.blob.core.windows.net/assets-ryanair-terms/STN.pdf',
        termAndConditionsLinkLTN: 'https://parkcloud.blob.core.windows.net/assets-ryanair-terms/LTN.pdf',
        termAndConditionsLinkBUD: 'https://parkcloud.blob.core.windows.net/assets-ryanair-terms/BUD.pdf',
        termAndConditionsLinkHHN: 'https://parkcloud.blob.core.windows.net/assets-ryanair-terms/HHN.pdf',
    },
    airportTransfers: {
        title: 'Airport transfers',
        textMessage: "We've transfers available all day, everyday whether you're a solo traveller, a family or a large group.",
        outboundCardTitle: "Select transfer for departure airport",
        inboundCardTitle: "Select transfer for destination airport",
        transferProvider: "Transfer provider:",
        single: "Single",
        return: "Return",
        noTransferProvided: "Sorry, we have no transfer provider in ",
        defaultProvider: "Please select provider",
        downloadServiceDescription: "Download service description",
        maxnumberofpassengers: "Max number of passengers reached.",
        addedTransfersForAllPassengers: "You have added transfers for all passengers on this trip",
        amountOfTransfers: "2 transfers on your trip",
        amountOfTransfers1: "1 transfers on your trip",
    },
    insurance: {
        header: 'Travel insurance',
        messageBefore: 'Get insurance and go carefree',
        messageAfter: "Sort out some travel insurance and leave your worries behind.",
        informationChildTeen: "Get 50% off children’s and teen's travel insurance.",
        discountMessage: "50% discount on insurance",
        countryOfResidenceMessage: 'Would you like to choose same country of residence for all passengers',
        insurancePlusPurchasedText: 'Insurance plus added',
        standardInsurancePurchasedText: 'Insurance added',
        headerPotentialInsurance: 'Insurance added. Anything else you need?',
    },
    payment: {
        refNumber1: 'ZN6B8T', // Two Way Standard Euro Simple Booking 1 Adult
        refNumber2: 'JWF1UA', //2 adult
        refNumber3: 'TK228M', //1 adult, 1 infant
        refNumber4: 'UHQQMV', //1 adult, 1 child
        refNumber5: 'WN91XG', //1 adult, 1 teen
        refNumber6: 'GHQ7JS', // 1 adult, 1 teen, 1 child, 1 infant
        refNumber7: 'XWZEHD', //reference number without any insurance
        refNumber8: 'TSRMUE', //just ref number with 2 passengers
        refNumber9: 'CPV7GH', //3 passengers, one insurance added
        refNumber10: 'BG1VHV', //2 passengers, one passenger with insurance plus
        refNumber11: 'XE7D5B', //2 passengers one passenger with standard insurance
        refNumberPA1: 'HL3UHW', //return trip with parking added
        refNumberPA2: 'SSLTJR', //one way trip with parking added
        refNumberPA3: 'SLFRJJ', //disabled parking card - no parking providers
        refNumberPA4: 'PHV9RF', //return trip with parking added
        refNumberPA5: 'QQSH9U', //return trip, no parking added
        refNumberPA6: 'DQHY5E', //one way trip, no parking added
        refNumberPA7: 'AW7VSN', //return trip, maximum added combo

        refNumber12: 'YNZMXW', //return trip, airport transfer added with single route for departure airport
        refNumber13: 'TJ4TGC', //return trip, airport transfer added with single route for destination airport
        refNumber14: 'QWCIKA', //return trip, airport transfer added with return route for departure airport
        refNumber15: 'CGKT6S', //return trip, airport transfer added with return route for destination airport
        refNumber16: 'UI5JVV', //return trip, , no airport transfers added
        refNumber17: 'OIJH3P', //return trip, maximum airport transfers added
        refNumber18: 'YLUPHJ', //one way trip, airport transfer added with single route for departure airport
        refNumber19: 'MHTLSF', //one way trip, airport transfer added with single route for destination airport
        refNumber20: 'JW75PA', //one way trip, airport transfer added with return route for departure airport
        refNumber21: 'ZW7MFQ', //one way trip, airport transfer added with return route for destination airport
        refNumber22: 'SRH79E', //one way trip, , no airport transfers added
        refNumber23: 'BMW2UD', //one way trip, airport transfer added with single route for departure airport and single route for destination airport
        refNumber24: 'GM5FVG', //one way trip, airport transfer added with single route for departure airport and return route for destination airport
        refNumber25: 'WS6IGE', //one way trip, airport transfer added with return route for departure airport and single route for destination airport
        refNumber26: 'BLCD9T', //one way trip, airport transfer added with return route for departure airport and return route for destination airport


        payMail: 'auto@ryanair.ie',
        payMail2: 'aa@aa.com',
        payCreditCard1: '1110',
    },
    regEx: {
        price: /\d+(\.\d{2})/,
    }
}
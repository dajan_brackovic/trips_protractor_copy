
exports.conf = {
    suite: [
        // MYFR
        './specs/regression/myRyanair/account/*.js',
        './specs/regression/myRyanair/companions/*.js',
        './specs/regression/myRyanair/dashboard/*.js',
        './specs/regression/myRyanair/fohintegration/*.js',
        './specs/regression/myRyanair/payments/*.js',
        './specs/regression/myRyanair/preferences/*.js',
        './specs/regression/myRyanair/tripsintegration/*.js',
    ]
};
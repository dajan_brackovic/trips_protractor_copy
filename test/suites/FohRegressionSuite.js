exports.conf = {
    suite: [
        // FOH
        './specs/regression/foh/smoketests/homepage/*.js',
        './specs/regression/foh/smoketests/farefinder/*.js',
        './specs/regression/foh/smoketests/timetable/*.js',
        './specs/regression/foh/smoketests/flightinfo/*.js',
        './specs/regression/foh/smoketests/map/*.js',
    ]
};

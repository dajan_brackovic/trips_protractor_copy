exports.conf = {
    suite: [
        './specs/foh/smoketests/homepage/*.js',
        './specs/foh/smoketests/farefinder/*.js',
        './specs/foh/smoketests/timetable/*.js',
        './specs/foh/smoketests/flightinfo/*.js',
        './specs/foh/smoketests/map/*.js',
    ]

}
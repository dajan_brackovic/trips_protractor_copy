exports.conf = {
    suite: [
        './specs/regression/foh/featuretests/homepage/*.js',
        './specs/regression/foh/featuretests/flightinfo/*.js',
        './specs/regression/foh/featuretests/map/*.js',
        './specs/regression/foh/featuretests/timetable/*.js',
        './specs/regression/foh/featuretests/farefinder/*.js',
    ]
}

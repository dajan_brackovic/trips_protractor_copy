exports.conf = {
    suite: [
        './specs/regression/trips/pricebreakdown/*.js',
        './specs/regression/trips/paymentvalidation/*.js',
        './specs/regression/trips/flightselect/*.js',
        './specs/regression/myryanair/*.js'
    ]
};

exports.conf = {
    suite: [
        './specs/regression/trips/seats/*.js',
        './specs/regression/trips/activetrip/*.js'
    ]
};

exports.conf = {
    suite: [
        './specs/regression/trips/simplebooking/onewayflight/C28769.js',
        './specs/regression/trips/simplebooking/onewayflight/C28715.js',
        './specs/regression/trips/simplebooking/onewayflight/C28738.js',
        './specs/regression/trips/simplebooking/onewayflight/C28779.js'
    ]
};

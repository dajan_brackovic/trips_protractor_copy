exports.conf = {
    suite: [
        // TRIPS
        './specs/regression/trips/extrasequipment/C58707.js',
        './specs/regression/trips/checkin/C139787.js',
        './specs/regression/trips/balearicbooking/C36364.js',

        // MYFR
        './specs/regression/myRyanair/C601955.js',

        // FOH
        './specs/regression/foh/smoketests/homepage/C43168.js',
        './specs/regression/foh/smoketests/farefinder/C40503.js',
        './specs/regression/foh/smoketests/timetable/C43134.js',
        './specs/regression/foh/smoketests/flightinfo/C43142.js',
        './specs/regression/foh/smoketests/map/C49986.js'
    ]
};

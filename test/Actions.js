var AddPaxActions = require('./trips/actions/AddPaxActions');
var BabyEquipmentActions = require('./trips/actions/BabyEquipmentActions');
var BookingSummaryActions = require('./trips/actions/BookingSummaryActions');
var ExtrasActions = require('./trips/actions/ExtrasActions');
var FOHHomeActions = require('./foh/actions/FOHHomeActions');
var LowCostParkingActions = require('./trips/actions/LowCostParkingActions');
var PotentialTripActions = require('./trips/actions/PotentialTripActions');
var TripsHomeActions = require('./trips/actions/TripsHomeActions');
var VoucherActions = require('./trips/actions/VoucherActions');
var DateActions = require('./trips/actions/helpers/DateActions');
var MusicEquipmentActions = require('./trips/actions/MusicEquipmentActions');
var CheckInActions = require('./trips/actions/CheckInActions.js');
var CarHireActions = require('./trips/actions/CarHireActions');
var PriceBreakDownActions = require('./trips/actions/PriceBreakDownActions.js');
var InsuranceActions = require('./trips/actions/InsuranceActions.js');
var SportEquipmentActions = require('./trips/actions/SportEquipmentActions.js');
var PaymentActions = require ('./trips/actions/PaymentActions.js');
var SeatsActions = require('./trips/actions/SeatsActions.js');
var BagsActions = require('./trips/actions/BagsActions.js');
var ItineraryActions = require('./trips/actions/ItineraryActions.js');
var ManageTripsActions = require('./trips/actions/ManageTripsActions.js');
var ManageYourTripActions = require('./trips/actions/ManageYourTripActions');
var TransfersActiveTripActions = require('./trips/actions/TransfersActiveTripActions.js');
var SamsoniteActions = require('./trips/actions/SamsoniteActions.js');
var PayPalActions = require('./trips/actions/PayPalActions.js');
var HoldFareActions = require('./trips/actions/HoldFareActions.js');
var SelectTransferProviderActions = require('./trips/actions/SelectTransferProviderActions.js');
var HotelsActions = require('./trips/actions/HotelsActions.js');
var MyRyanairActions = require('./trips/actions/MyRyanairActions.js');
var TripHelper = require('./trips/actions/helpers/TripHelper.js');
var MyFrSignupHelper = require('./trips/actions/helpers/MyFrSignupHelper.js');
var WaitHelper = require('./trips/actions/helpers/WaitHelper.js');

// FOH
var FareFinderActions = require('./foh/actions/farefinder/FareFinderActions');
var FareFinderResultsActions = require('./foh/actions/farefinder/FareFinderResultsActions');
var FareFinderResultsDetailsActions = require('./foh/actions/farefinder/FareFinderResultsDetailsActions');
var TimeTableActions = require('./foh/actions/timetable/TimeTableActions');
var MapHomeActions = require('./foh/actions/map/MapHomeActions');
var FlightInfoByRouteActions = require('./foh/actions/flightinfo/FlightInfoByRouteActions');
var FlightInfoByNumberActions = require('./foh/actions/flightinfo/FlightInfoByNumberActions');





// shared

var Actions = function () {
    this.addPaxActions = new AddPaxActions();
    this.babyEquipmentActions = new BabyEquipmentActions();
    this.sportEquipmentActions = new SportEquipmentActions();
    this.insuranceActions = new InsuranceActions();
    this.bookingSummaryActions = new BookingSummaryActions();
    this.extrasActions = new ExtrasActions();
    this.fOHActions = new FOHHomeActions();
    this.lowCostParkingActions = new LowCostParkingActions();
    this.potentialTripActions = new PotentialTripActions();
    this.tripsHomeActions = new TripsHomeActions();
    this.voucherActions = new VoucherActions();
    this.daysActions = new DateActions();
    this.musicEquipmentActions = new MusicEquipmentActions();
    this.checkInActions = new CheckInActions();
    this.carHireActions = new CarHireActions();
    this.priceBreakDownActions = new PriceBreakDownActions();
    this.fareFinderActions = new FareFinderActions();
    this.fareFinderResultsActions = new FareFinderResultsActions();
    this.seatsActions = new SeatsActions();
    this.fareFinderResultsDetailsActions=new FareFinderResultsDetailsActions();
    this.bagsActions = new BagsActions();
    this.timetableActions=new TimeTableActions();
    this.itineraryActions = new ItineraryActions();
    this.paymentActions = new PaymentActions();
    this.manageTripsActions = new ManageTripsActions();
    this.transferActiveTripsActions = new TransfersActiveTripActions();
    this.mapHomeActions=new MapHomeActions();
    this.samsoniteActions = new SamsoniteActions();
    this.samsoniteActions = new SamsoniteActions();
    this.flightInfoByRouteActions = new FlightInfoByRouteActions();
    this.flightInfoByNumberActions = new FlightInfoByNumberActions();
    this.payPalActions = new PayPalActions();
    this.selectTransferProviderActions = new SelectTransferProviderActions();
    this.manageYourTripActions = new ManageYourTripActions();
    this.holdFareActions = new HoldFareActions();
    this.hotelsActions = new HotelsActions();
    this.myRyanairActions = new MyRyanairActions();
    this.tripHelper = new TripHelper();
    this.myFrSignupHelper = new MyFrSignupHelper();
    this.waitHelper = new WaitHelper();

};


module.exports = Actions;

this.airport;
this.country;
this.flightPrice;
this.flightType;

function FareFinderResultFlightCard(airport, country, flightPrice, flightType) {
    this.airport = airport;
    this.country = country;
    this.flightPrice = flightPrice;
    this.flightType = flightType;
}

module.exports = FareFinderResultFlightCard;
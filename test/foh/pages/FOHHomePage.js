var FOHHomePage = function () {

    this.get = function () {
        browser.get('');
    };

    //FLIGHT DESTINATION AND DATES
    this.openFlightFromCalendar = function () {
        return element.all(by.css("div.date-input")).get(0);
    };
    this.closeFlightFromCalendar = function () {
        return element.all(by.css("div>label[ng-click='hideDatePlaceholder()']")).get(0);
    };

    this.flyBackCalendar = function () {
        return element(by.css("#row-dates-pax div[class~='container-to']"));
    };

    this.openFlightToCalendar = function () {
        return element(by.css("div[unavailable-dates='dateRange.unavailabiltyEndDates']>label"));
    }

    this.closeFlightToCalendar = function () {
        return element(by.css("div[class*='popup-end-date ng-isolate-scope opened']"));
    };

    this.flightFrom = function () {
        return element(by.css("input[aria-labelledby='label-airport-selector-from']"));
    };

    this.flightTo = function () {
        return element(by.css("input[aria-labelledby='label-airport-selector-to']"));
    };

    this.openCloseToFieldPopup = function () {
        return element(by.css("#label-airport-selector-to"));
    };

    this.openCloseFromFieldPopup = function () {
        return element(by.css("#label-airport-selector-from"));
    };

    this.rightArrow = function () {
        return element(by.css("button.arrow.right"));
    };


    this.selectedFromAirport = function () {
        return element(by.css("div.col-departure-airport div[class='pane right'] div[class~='clicked']"));
    };

    this.selectedToAirport = function () {
        return element(by.css("div.col-destination-airport div[class='pane right'] div[class~='clicked']"));
    };

    this.flightDefaultFromCountry = function () {
        return element(by.xpath("//div[contains(@class, 'clicked') and contains(@bo-class,'isFirstSelected(option)') and contains(text(),'United Kingdom')]"));
    };

    this.selectFlightFromCountry = function (name) {
        return element(by.xpath("(//div[@class='pane left']/div/div[contains(text(),'" + name + "')])[1]"));
    };

    this.selectFlightFromAirport = function (name) {
        return element(by.xpath("(//div[@class='pane right']/div/div[contains(text(),'" + name + "')])[1]"));
    };

    this.flightDefaultFromAirport = function () {
        return element(by.xpath("//div[contains(@class, 'clicked') and contains(@bo-class,'isSecondSelected(option)') and contains(text(),'London Stansted')]"));
    };

    this.flightOutNextAvailableDateAfterToday = function () {
        return element(by.xpath("(//core-datepicker[contains(@class,'start-date')]/div/ul/li/ul/li[not(contains(@class,'disabled')) and not(contains(@class,'blank')) and not(contains(@class,'unavailable'))]/span)[2]"));
    };

    this.selectedFlightOutDate = function () {
        return element(by.css("ul.days>li.start.range-selected"));
    };

    this.selectedFlightOutDateReturn = function () {
        return element(by.css("li.start.range-selected"));
    };

    this.flightInDate = function (validDay) {
        return element(by.xpath("(//core-datepicker[contains(@class,'end-date')]//li[not(contains(@class,'disabled')) and not(contains(@class,'blank')) and not(contains(@class,'unavailable'))]/span)[" + validDay + "]"));
    };

    this.flightOutDate = function (validDay) {
        return element(by.xpath("(//core-datepicker[contains(@class,'start-date')]//li[not(contains(@class,'disabled')) and not(contains(@class,'blank')) and not(contains(@class,'unavailable'))]/span)[" + validDay + "]"));
    };


    this.selectedFlightInDate = function () {
        return element(by.xpath("(//core-datepicker[contains(@class,'end-date')]/div/ul/li/ul/li[contains(@class,'selected') and not(contains(@class,'disabled')) and not(contains(@class,'blank'))])[1]"));

    };
    this.flyOutDateToday = function () {
        return element(by.xpath("//li[contains(@class,'today') and not(contains(@class,'start'))]/span"));
    };

    this.flybackDateToday = function () {
        return element(by.xpath("//li[contains(@class,'start today')]/span"));
    };

    this.selectedCountry = function (country) {
        return element(by.xpath("//div[contains(@class,'clicked') and contains(text(),'" + country + "')]"));
    };

    this.selectedCountryAirport = function (airport) {
        return element(by.xpath("//div[contains(@class,'clicked') and contains(text(),'" + airport + "')]"));
    };

    this.lblFlyBack = function () {
        return element(by.css("div[label='Fly Back:']"));
    };

    this.firstAirportInList = function () { //Picks first Airport in the list
        return element(by.css("div.pane.right div.option:not(.ng-hide)"));
    };

    //PASSENGERS
    this.openPAXDropDown = function () {
        return element(by.css("div[form-field-id='pax-input'] core-icon.chevron"));
    };

    this.paxPlusButton = function (index) {
        return element.all(by.css("button.core-btn.inc")).get(index);
    };

    //BUTTONS
    this.letsGoBtnFlightSearch = function () {
        return element(by.css("button[ng-click='searchFlights()']"));
    };

    this.continueBtnFlightSearch = function () {
        return element(by.css("button[ng-click='extendForm($event)']"));
    };

    this.btnReturn = function () {
        return element(by.css("#flight-search-type-option-return"));
    };

    this.btnOneWay = function () {
        return element(by.css("#lbl-flight-search-type-one-way"));
    };

    this.widgetDatesPassengersSelection = function () {
        return element(by.css("#row-dates-pax"));
    };

    //ERROR POPUPS FOH
    this.errorPopUpOutBound = function () {
        return element.all(by.css("span[translate='foh.home.flight_search_errors.departure_required']")).get(1);
    };
    this.errorPopUpInBound = function () {
        return element.all(by.css("span[translate='foh.home.flight_search_errors.destination_required']")).get(1);
    };

    this.errorPopUpFlyOutDateRound = function () {
        return element.all(by.css("span[translate='foh.home.flight_search_errors.start_date_required']")).get(1);
    };

    this.errorPopUpFlyBackDateRound = function () {
        return element.all(by.css("span[translate='foh.home.flight_search_errors.end_date_required']")).get(1);

    };

    this.errorPopUpFlyOutDateOneWay = function () {
        return element.all(by.css("span[translate='foh.home.flight_search_errors.start_date_required']")).get(1);

    };

    this.errorPopUpValidAirportTo = function () {
        return element.all(by.css("span[translate='foh.home.flight_search_errors.valid_airport']")).get(1);
    };

    this.errorPopUpValidAirportFrom = function () {
        return element.all(by.css("span[translate='foh.home.flight_search_errors.valid_airport']")).get(1);
    };
    this.errorMessageMaxPAX = function () {
        return element(by.css("div .info-box"));
    };
    this.alertMessageDepartureToday = function () {
        return element(by.css("span[translate='foh.home.flight_search_errors.departs_today']"));
    };
    this.alertMessageReturnToday = function () {
        return element(by.css("span[translate='foh.home.flight_search_errors.same_date']"));
    };
    this.AlertMessageBookTodayCancelButton = function () {
        return element(by.css(".core-btn-secondary"));
    };
    this.AlertMessageBookTodayOkButton = function () {
        return element(by.xpath("//button[contains(@ng-click,'confirm')]"));
    };
    this.travellingWithInfantOkButton = function () {
        return element(by.css("button[ng-click='confirm()']"));
    };
    this.bisPlusAlertMessageOk = function () {
        return element(by.css("div.ngdialog-content"));
    };
    //other tabs
    this.flightsTab = function () {
        return element(by.css("label.flights"));
    };
    this.hotelsTab = function () {
        return element(by.css("label.hotels[for='search-type-selector-hotels']"));
    };
    this.carsTab = function () {
        return element(by.css("label.cars"));
    };

    this.isFlightsTabSelected = function () {
        return element(by.css("li[class~='flights-tab']"));
    };

    this.isHotelsTabSelected = function () {
        return element(by.css("li[class~='hotels-tab']"));
    };

    this.isCarsTabSelected = function () {
        return element(by.css("li[class~='cars-tab']"));
    };

    this.flightsWidget = function () {
        return element(by.css("div[aria-labelledby='tab-flights-text']"));
    };

    this.hotelsWidget = function () {
        return element(by.css("div[aria-labelledby='tab-hotels-text']"));
    };

    this.carsWidget = function () {
        return element(by.css("div[aria-labelledby='tab-cars-text']"));
    };

    this.title = function () {
        return element(by.css("h2:contains('Low Fares Made Simple')"));
    };

    this.letsGoBtnHotelSearch = function () {
        return element(by.css("div.col-hotel-search-go> button"));
    };
    //other popup needs to be closed before other actions
    this.popupDepartureClosePopup = function () {
        return element(by.css("div[class*='opened popup-start-date')]"));

    };

    this.popupReturnClosePopup = function () {
        return element(by.css("div[class* = 'opened popup-end-date')]"));
    };


    //links
    this.termsLink = function () {
        return element(by.css("a:contains('Website Terms of Use')"));
    };

    //Error Popups
    this.todayFlightPopUp = function () {
        return element(by.css(".btn.btn-primary-yellow"));

    };

    //scrollable div
    this.selectDepartureAirportDiv = function () {
        return element(by.xpath("//div[contains(@class, 'pane right')]/div"));
    };

    //spanish discount dropdown
    this.openSpanishDiscountDropdown = function () {
        return element(by.xpath("//*[@id='row-discount']/div/div/div[2]/div[2]/div/div[1]"));
        //return element(by.css("div.col-discount > div > div:nth-of-type(2) > div:nth-of-type(2) > div > div.value"));

    };

    this.spanishDiscountOptions0Discount = function () {
        return element(by.css("div[options='flightModel.discounts.options'] li")); //first li in the div is 'No Spanish Discount'
    };

    this.spanishDiscountOptions5Percent = function () {
        return element(by.xpath("//div[contains(@class, 'content')]/ul/li[contains(text(),'5%')]"));
    };

    this.spanishDiscountOptions10Percent = function () {
        return element(by.xpath("//div[contains(@class, 'content')]/ul/li[contains(text(),'10%')]"));
    };
    this.spanishDiscountOptions50Percent = function () {
        return element(by.xpath("//div[contains(@class, 'content')]/ul/li[contains(text(),'50%')]"));
    };
    this.spanishDiscountOptions55Percent = function () {
        return element(by.xpath("//div[contains(@class, 'content')]/ul/li[contains(text(),'55%')]"));
    };
    this.spanishDiscountOptions60Percent = function () {
        return element(by.xpath("//div[contains(@class, 'content')]/ul/li[contains(text(),'60%')]"));
    };

    this.RoughspanishDiscountOptions5Percent = function (index) {
        return element(by.css(".content.ng-scope > ul > li:nth-child(" + index + ")"));
    };

    this.listSpanishDiscountOptions = function () {
        return element.all(by.css("#row-discount [ng-repeat='option in options']"));
    }

    //car search fields
    this.selectedPickUpDate = function () {
        return element(by.xpath("//ul[@class='days']/li[contains(@class,'start')]"));
    };

    this.selectedDropOffDate = function () {
        return element(by.xpath("//ul[@class='days']/li[contains(@class,'selected') and contains(@class,'end')]"));
    };
    this.openPickUpCalendar = function () {
        return element(by.css("div[form-field-id='dates-selector-start']"));

    };
    this.openDropOffCalendar = function () {
        return element(by.css("div.col-cal-to>div[form-field-id='dates-selector-end']>label"));
    };

    this.needCarIn = function () {
        return element(by.css("div[car-search-locations] input[aria-labelledby='label-airport-selector-pickup-from']"));
    };

    this.countryOfResidence = function () {
        return element(by.css("div[car-search-residence] div[class~='dropdown-handle']"));
    };

    this.selectCountryOfResidence = function (country) {
        return element(by.css("div[popup-id~='car-search-residence'] ul > li[aria-label='" + country + "']"));
    };

    this.selectReturnCarRadioBtn = function () {
        return element(by.css("div[class~='row-car-search-type']>div[class~='one-way'] span[class='rad']"));
    };

    this.returnLocation = function () {
        return element(by.css("div[car-search-locations] input[aria-labelledby='label-airport-selector-dropoff-from']"));
    };

    this.selectPickUpDateDropDown = function () {
        return element(by.css("div[label^='Pick up date']"));
    };

    this.selectPickUpHourDropdown = function () {
        return element(by.css("div[class~='container-from'] div[label='Hour'] div[class='dropdown-handle']"));
    };

    this.closePickUpHour = function () {
        return element(by.css("div[ng-click='__morphPopup__.closePopup()']"));
    };

    this.selectPickUpHourOption = function (hourIndex) {
        return element(by.css("div[popup-id='from-hour-list-popup'] > div > div > div > div >ul> li[aria-label='" + hourIndex + "']"));
    };

    this.selectPickUpMinDropDown = function () {
        return element(by.css("div[class~='container-from'] div[label='Min'] div[class='dropdown-handle']"));
    };

    this.selectPickUpMinOption = function (minIndex) {
        return element(by.css("div[popup-id='from-min-list-popup']>div > div > div > div >ul> li[aria-label='" + minIndex + "']"));
    };

    this.selectDropOffDate = function () {
        return element(by.css("input[aria-label='Drop off date - YYYY']"));
    };

    this.selectDropOffHourDropDown = function () {
        return element(by.css("div[class~='container-to'] div[label='Hour'] div[class='dropdown-handle']"));
    };

    this.selectDropOffHourOption = function (hourIndex) {
        return element(by.css("div[popup-id='to-hour-list-popup'] > div > div > div >ul> li[aria-label='" + hourIndex + "']"));
    };

    this.selectDropOffMinDropDown = function () {
        return element(by.css("div[class~='container-to'] div[label='Min'] div[class='dropdown-handle']"));
    };

    this.selectDropOffMinOption = function (minIndex) {
        return element(by.css("div[popup-id='to-min-list-popup'] > div >  div > div >ul> li[aria-label='" + minIndex + "']"));
    };

    this.carSearchLetsGoButton = function () {
        return element(by.css("button[translate='foh.home.car_search.lets_go']"));
    };
    //miscellaneous Divs
    this.searchContainerDiv = function () {
        return element(by.css("div.search-container"));
    };
    this.smallerSearchContainerDiv = function () {
        return element(by.css(".clearfix"));
    };


    //selected
    this.hotelLocation = function () {
        return element(by.css("input[aria-labelledby='label-destination-input']"));
    };

    this.selectedCheckInDate = function () {
        return element(by.xpath("(//core-datepicker[contains(@class,'start-date')]/div/ul/li/ul/li[contains(@class,'start') and not(contains(@class,'disabled')) and not(contains(@class,'blank'))])[1]"));
    };

    this.selectedCheckOutDate = function () {
        return element(by.xpath("(//core-datepicker[contains(@class,'end-date')]/div/ul/li/ul/li[contains(@class,'selected') and not(contains(@class,'disabled')) and not(contains(@class,'blank'))])[1]"));
    };

    this.selectCheckInDropDown = function () {
        return element.all(by.css("label[ng-click='hideDatePlaceholder()']")).get(0);
    };

    this.selectCheckOutDropDown = function () {
        return element(by.css("form[name='formHotelSearch']>div>div>div.container-to>div.col-cal-to>div>label"));
    };

    this.setCheckInDate = function (validDay) {
        return element(by.xpath("(//core-datepicker[contains(@class,'start-date')]/div/ul/li/ul/li[not(contains(@class,'disabled')) and not(contains(@class,'blank')) and not(contains(@class,'unavailable'))]/span)[" + validDay + "]"));
    };

    this.setCheckOutDate = function (validDay) {
        return element(by.xpath("(//core-datepicker[contains(@class,'end-date')]/div/ul/li/ul/li[not(contains(@class,'disabled')) and not(contains(@class,'blank'))]/span)[" + validDay + "]"));
    };

    this.clickCheckInDropDown = function () {
        return element(by.css("div[label^='Check-in']"));
    };

    this.clickCheckOutDropDown = function () {
        return element(by.css("div[label^='Check out']"));
    };

    //bestDeals
    this.cheapFlightsFrom = function () {
        return element(by.css(".core-link[data-popup-trigger='airport-selector-compact']"));
    };

    this.flightDealCategory = function (category) {
        return element(by.xpath("//div[@search-criteria]//div[contains(@class,'criteria-deals-list')]/span[text()='" + category + "']"));
    };

    this.categoryResults = function () {
        return element(by.css("div.home-content > farefinder-widget"));
    };

    this.openBestDealDepartDropdown = function () {
        return element(by.css("span[ng-click='vmAirportSelector.togglePopup()']"));
    };
    this.selectDealFromAirport = function (airport) {
        return element(by.xpath("//li[@ng-click='select(option)']/span[contains(@class,'airport') and text()='" + airport + "']"));
        //TODO: the following is not working, haven't found why yet
        // return element(by.css("li[ng-click='select(option)'] > span :contains('"+airport+"')"));
    };
    this.lowestPriceDeal = function (searchResultsIndex) {
        return element(by.css("div:nth-child(" + searchResultsIndex + ")> a.list-with-image"));
    };
    //Dismiss Cookie Policy popup
    this.dismissCookiePolicy = function () {
        return element(by.css("core-icon.close-icon"));
    };

    this.moreDealsLink = function () {
        return element(by.css(".core-btn-ghost[ng-click='vm.onMoreDealsClick()']"));
    };
    //Below are elements on the myRyanair Dropdown tab
    this.myRyanair = function () {
        return element(by.css("a[ng-class=\"\{'active':template === authTemplate}\"\]"));
    };

    this.myRyanairMenu = function () {
        return element(by.css("#largeMenu"));
    };

    this.loginRadioBtn = function () {
        return element(by.css("input[id^='login']"));
    };

    this.createAnAccountRadioBtn = function () {
        return element(by.css("input[id^='makeaccount']"));
    };

    this.clickAgreeRadioBtn = function () {
        return element(by.model("vm.register.subscribe"));
    };

    this.privacyPolicyRadioBtn = function () {
        return element(by.css("input.core-checkbox"));
    };

    this.policyLink = function () {
        return element(by.css("[href='corporate/privacy-policy']"));
    };

    this.privacyPageHeading = function () {
        return element(by.css("div.title-wrapper h1"));
    };

    this.ryanairIcons = function () {
        return element(by.css("a.ryanair-logo"));
    };

    this.userName = function () {
        return element(by.css("#username"));
    };

    this.emailFieldCreateAnAccountMyFr = function () {
        return element(by.css("input[id^='email']"));
    };

    this.password = function () {
        return element(by.css("#password"));
    };

    this.passwordFieldCreateAnAccountMyFr = function () {
        return element(by.css("input[id^='password']"));
    };

    this.loginBtn = function () {
        return element(by.css("div.form-group button.core-btn-primary")); // Same locator for SignUp in Create an Account
    };

    this.passwordTermsMyFr = function () {
        return element(by.css("[translate='MYRYANAIR.PASSWORD_TERMS']"));
    };

    this.forgotYourPasswordMyFr = function () {
        return element(by.css("[ng-click='vm.forgotPassword()']"));
    };

    this.loggedUserName = function () {
        return $("span[class~='username']");
    };

    this.logoutButton = function () {
        return $("[ng-click='logout()']");
    };

    this.nearlyThereMessage = function () {
        return element(By.css("div [translate='MYRYANAIR.AUTHORIZATION.LANDING.SIGNED']"));
    };

    this.welcomeBackMessage = function () {
        return element(By.css("div [translate='MYRYANAIR.AUTHORIZATION.LANDING.NOT_VERIFIED']"));
    };

    this.resendEmail = function () {
        return element(By.css("a[ng-click='resendActivation()']"));
    };

    //recent search
    this.recentSearchRoute = function (index) {
        return element.all(by.css("div[recent-searches='true']>a>div.ff-city-list>span")).get(index);
    };

    this.recentSearchRoutes = function () {
        return element.all(by.css(".airport-route"));
    };


    //links accessibility
    this.menuLink = function (index) {
        return element.all(by.css(".menu-links>li")).get(index);//from 0
    };
    this.userMenuLink = function (index) {
        return element.all(by.css(".menu-user>li")).get(index);//from 0
    };
    this.menuManage = function () {
        return element.all(by.css("#manage-trips>a")).get(index);//from 0
    };

    this.menuCheckin = function () {
        return element.all(by.css("#check-in-menu>a")).get(index);//from 0
    };
    this.subMenuLink = function (index) {
        return element.all(by.css(".core-link-inline")).get(index);//from 0
    };
    this.userSubMenuLink = function (index) {
        return element.all(by.css(".nav-bar-ul>li>a")).get(index);//from 0
    };
    this.ctabuttons = function (index) {
        return element.all(by.css("div.homepagetoolcard-cta>div>div>div>a[id^=ctabutton]")).get(index);//from 1
    };

    this.wholeCardContentLinks = function (index) {
        return element.all(by.css(".whole-card-content-link")).get(index);//from 1
    };

    //Selecting Markets

    this.marketsMenu = function () {
        return element(by.css("core-icon.chevron.icon-arrowdown.closer.desktop-laptop"));
    };

    this.marketList = function (option) {
        return element(by.cssContainingText('a.core-link-inline', option));
    };

    // Global Header buttons
    this.btnCarHire = function () {
        return element(by.css("li#car-hire"));
    };
    this.btnCarHireLink = function () {
        return element(by.css("li#car-hire>a"));
    };

    this.btnHotels = function () {
        return element(by.css("li#hotels"));
    };

    this.btnHotelsLink = function () {
        return element(by.css("li#hotels>a"));
    };

    //External Linked Car Hire page
    this.carHireWidget = function () {
        return element(by.css("div.ct_s1_search"));
    };
    //US Balearic Flight warning Dialog
    this.btnUsBalearicWarningDialogOk = function () {
        return element(by.css("span[translate='foh.home.flight_search_spanish_discount_us_market_popup.ok']"));
    };

    this.lblUsBalearicWarningDialogHeader = function () {
        return element(by.css("span[translate='foh.home.flight_search_spanish_discount_us_market_popup.header']"));
    };

    //saved trip
    this.savedTripsTitle = function () {
        return element(by.css("[translate='MYRYANAIR.MY_TRIPS.EMPTY_SAVED_TRIPS.SAVED_TRIPS']"));
    };

    this.savedTripContent = function () {
        return element.all(by.css("saved-trip-card"));
    };

    this.savedTripDestination = function () {
        return element.all(by.css("saved-trip-card .card-trip-content-destination"));
    };

    this.savedTripDeparture = function () {
        return element.all(by.css("saved-trip-card .card-trip-content-departure"));
    };

    this.savedTripDate = function () {
        return element.all(by.css("saved-trip-card .card-trip-content-date"));
    };

    this.savedTripPrice = function () {
        return element.all(by.css("saved-trip-card .card-trip-footer-price"));
    };

    this.btnRemoveSavedTrip = function () {
        return element.all(by.css("[icon-id='icons.glyphs.trash']"));
    };
    this.viewAllSaved = function () {
        return element.all(by.css(".core-btn-ghost[ng-click='vm.openAllSaved()']"));
    };
    //small window navigation locators
    this.sideMenuOpenClose = function () {
        return element(by.css(".btn-phone-menu"));//from 0
    };
    this.sideMenuLink = function (index) {
        return element.all(by.css(".title>div>a")).get(index);//from 0
    };
    this.sideMenuNewTabLink = function (index) {
        return element.all(by.css(".no-link-style")).get(index);//from 0
    };

    this.sideMenuMyRyanair = function () {
        return element(by.css("#toggle-myr"));
    };
    this.sideMenuMarket = function () {
        return element(by.css(".btn-markets"));
    };

    this.sideSubMenuLink = function (index) {
        return element.all(by.css(".sublinks>li>a")).get(index);//from 1
    };
    this.sideMarketOptions = function () {
        return element.all(by.css(".markets>li>a"));
    };
    this.sideViewRyanairLogo = function () {
        return element.all(by.css("svg>use[ng-href~=ryanair-logo]"));
    };
    this.sideMarketList = function (index) {
        return element.all(by.css(".markets>li>a")).get(index);//from 1
    };
    this.upcomingTrips = function () {
        return element(by.css(".criteria-deals-list.active>span"));
    };
    this.btnViewAllUpcomings = function () {
        return element(by.css(".criteria-deals-list.active>span"));
    };
    this.earliestUpcomingTripDepart = function () {
        return element(by.css(".card-trip-content-departure"));
    };
    this.earliestUpcomingTripDestination = function () {
        return element(by.css(".card-trip-content-destination"));
    };
    this.earliestUpcomingTripDate = function () {
        return element(by.css(".card-trip-content-date"));
    };
    this.earliestUpcomingTripImage = function () {
        return element(by.css(".card-trip-image-circle>picture>img"));
    };
    this.cheapFlightsFromWidget = function (index) {
        return element.all(by.repeater("fare in vm.fares")).get(index);
    };

    //footer - Samsonite Link
    this.footerSamsoniteLink = function (index) {
        return element(by.css("div.useful-links a[href='/ie/en.html/samsonite/buy-now']"));
    };


};

module.exports = FOHHomePage;


var FareFinderHomePage = function () {
    var ES = "es";
    var PL = "pl";
    var IT = "it";

    this.get = function () {
        browser.get('cheap-flights');//en
    };

    this.clickAblePageDiv = function () {
        return element(by.css("body > div.FR > main > div > div.header-container"))
    }

    this.closeDepartPopup = function () {
        return element(by.id("label-departure-input"))
    }

    this.closeDestinationPopup = function () {
        return element(by.id("label-destination-input"))
    }

    this.ffSearchFromField = function () {
        return element(by.css("body > div.FR > main > div > div.header-container > div > form > div:nth-child(1) > div:nth-child(2) > div:nth-child(5) > div > div.disabled-wrap > input"));
    };

    this.ffSearchToField = function () {
        return element(by.css("body > div.FR > main > div > div.header-container > div > form > div:nth-child(2) > div:nth-child(2) > div:nth-child(5) > div > div.disabled-wrap > input"));
    };

    this.ffSearchBudgetField = function () {
        return element(by.css("div[farefinder-budget-input]> div[core-rich-input]"));
    };

    this.ffSearchLetsGoBtn = function () {
        return element(by.css("button[type='submit']"));
    };

    this.ffBudgetAmountList = function (index) {
        return element.all(by.xpath("(//li[@ng-repeat='option in options']/label)[" + index + "]"));
    };


    this.ffBudgetDropDownCustomValue = function () {
        return element(by.css("div[popup-id='farefinderBudgetInput'] input[name='custom-budget']"));
    };

    this.ffBudgetDropDownOkBtn = function () {
        var fullCss = "body > div.FR > main > div > div.header-container > div > form > div:nth-child(3) > div.popup-budget.ng-isolate-scope.opened > div > div.content-box.arrow_box > div.content.ng-scope > div.custom-value-box > button";
        return element(by.css("div.FR div.custom-value-box button"));
    }

    this.placeSelection = function (name) {
        return element(by.cssContainingText("div.name.ng-binding.ng-scope", name));
    }

    this.selectedFromAirport = function (airport) {
        return element(by.cssContainingText(".item.ng-scope.selected>span.airport", airport));
    }
    this.selectedFromCountry = function (country) {
        return element(by.cssContainingText(".item.ng-scope.selected>span.country", country));
    }

    this.ffBudgetValue = function () {
        return element(by.css("div[data-popup-trigger='farefinderBudgetInput'] div[class~='value']"));
    };


}

module.exports = FareFinderHomePage;
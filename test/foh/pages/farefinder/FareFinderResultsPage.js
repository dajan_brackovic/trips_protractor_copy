var sprintf = require("sprintf").sprintf;


var FareFinderResultsPage = function () {

    this.get = function () {
        browser.get('cheap-flights/');
    };

    this.clickAblePageDiv = function () {
        return element(by.css("body > div.FR > main > div > div.fare-finder-search-container > div"))
    }

    this.clickFromFieldDiv = function () {
        return element(by.css("form[name='formFaresSearch']>div[form-field-id='departure-input']"))
    }

    this.clickToFieldDiv = function () {
        return element(by.css("#label-destination-input"))
    }
    this.regionName = function () {
        return element(by.css("span[translate='foh.farefinder.categories.regions']"))
    }
    this.airportName = function (airport) {
        return element(by.xpath("//span[@ng-bind='item.name' and text()='" + airport + "']"))
    }

    this.ffSearchFromField = function () {
        return element(by.css("form > div:nth-child(1) > div:nth-child(2) > div:nth-child(5) > div > div.disabled-wrap > input"));
    };

    this.ffSearchToField = function () {
        return element(by.css("form > div:nth-child(2) > div:nth-child(2) > div:nth-child(5) > div > div.disabled-wrap > input"));
    };

    this.closeDepartPopup = function () {
        return element(by.id("label-departure-input"))
    }

    this.closeDestinationPopup = function () {
        return element(by.id("label-destination-input"))
    }

    this.ffSearchBudgetField = function () {
        return element(by.css(".value.ng-binding"));
    };


    this.ffSearchBudgetFieldValue = function () {
        return element(by.css("form > div:nth-child(3) > div:nth-child(2) > div:nth-child(5) > div > div.value.ng-binding"));
    };


    this.flyOutDateBtn = function () {
        return element(by.xpath("//div[@farefinder-departure-date-input]//label[@id='label--start']"));
    }


    this.specificDateBtn = function () {
        return element(by.css("span[translate='foh.farefinder.results.date_input.specific_date']"))
    }

    this.dateRangeBtn = function () {
        return element(by.css("form > div.ng-scope > div:nth-child(1) > div.popup-departure-date.ng-isolate-scope.opened > div > div.content-box.arrow_box > div.content.ng-scope > div.left-panel > ul > li:nth-child(2)"));
    }


    this.entireMonthBtn = function () {
        return element(by.css("span[translate='foh.farefinder.results.date_input.entire_month']"));
    }

    this.lastMonthBtn = function () {
        return element.all(by.css(".option-item>label")).get(9);
    }

    this.nextThreeMonthsBtn = function () {
        return element(by.css("form > div.ng-scope > div:nth-child(1) > div.popup-departure-date.ng-isolate-scope.opened > div > div.content-box.arrow_box > div.content.ng-scope > div.left-panel > ul > li:nth-child(4)"));
    }


    this.flyOutAnyTimeBtn = function () {
        return element(by.css("div.popup-departure-date.opened div.left-panel > ul > li:nth-child(5)"));
    }

    this.flyBackBtn = function () {
        return element(by.css("div[data-popup-trigger='fareFinderReturnDate'] > label#label--start"));
    }


    this.flyBackOneWayBtn = function () {
        return element(by.css("span[translate='foh.farefinder.results.date_input.oneway_title']"));
    }

    this.ffBudgetAmountList = function (index) {
        return element.all(by.xpath("//li[@ng-repeat='option in options']//div[contains(@class,'value')]")).get(index);
    };


    this.ffBudgetDropDownCustomValue = function () {
        return element(By.model('vm.customValue'));
    };

    this.ffBudgetDropDownOkBtn = function () {
        var fullCss = "body > div.FR > main > div > div.header-container > div > form > div:nth-child(3) > div.popup-budget.ng-isolate-scope.opened > div > div.content-box.arrow_box > div.content.ng-scope > div.custom-value-box > button";
        return element(by.css("div.FR div.custom-value-box button"));
    }


    // results fare list
    //fare in vm.fares
    this.ffFaresListResults = function () {
        return element.all(by.css('div.farefinder-list>a'));
    }

    this.ffFaresListResultsDestinations = function () {
        return element.all(by.css("div[ng-repeat='fare in vm.fares']>a>div>span[ng-if='::!vm.optionsMap.route']"));
    }

    // fareCards
    this.fareCardAirport = function () {
        return element(by.css("body div.FR  span.airport.ng-binding"));
    }


    this.fareCardAirportCountry = function () {
        return element(by.css("body > div.FR > main > div > div.results-container > farefinder-widget > div > div.inner-wrapper > div:nth-child(1) > div > div > div:nth-child(1) > div > a > div.ff-city-list > span.country.ng-binding"));
    }


    this.fareCardFlightPrice = function () {
        return element(by.css("body > div.FR > main > div > div.results-container > farefinder-widget > div > div.inner-wrapper > div:nth-child(1) > div > div > div:nth-child(1) > div > a > div.ff-price-type > div.price"));
    }

    this.fareCardFlightType = function () {
        return element(by.css("body > div.FR > main > div > div.results-container > farefinder-widget > div > div.inner-wrapper > div:nth-child(1) > div > div > div:nth-child(1) > div > a > div.ff-price-type > div.type"));
    }


    // dateSelection
    this.specificDateSelectionCalendar = function (index) {
        return element(by.xpath("(//core-datepicker[contains(@class,'specific-date')]/div/ul/li/ul/li[not(contains(@class, 'disabled')) and not(contains(@class, 'blank'))]/span)[" + index + "]"));
    }

    this.entireMonthSelection = function (index) {
        return element(by.css("div.popup-departure-date.opened div.content.second-panel > div.right-panel > div > core-option-selector > ul > li:nth-child(" + index + ") > label > div"));
    }

    this.dateRangeSelectionCalendarToday = function () {
        return element(by.css("form ul.days > li.ng-scope.today"));

    }
    this.dateRangeSelectionCalendarTomorrow = function () {
        return element(by.css("form ul.days > li.ng-scope.today+li"));

    }

    this.dateRange2SelectionCalendar = function () {
        return element(by.xpath("//core-datepicker//ul[@class='days']/li[last()]"));
    }

    this.flyBackTripLengthMenu = function () {
        return element(by.css("span[translate='foh.farefinder.results.date_input.trip_length']"));
    }

    this.flyBackNextThreeMonthsMenu = function () {
        return element(by.css("span[translate='foh.farefinder.results.date_input.next_3_months_title']"));
    }

    this.tripLength1to3DaysSelection = function () {
        return element(by.css("div.popup-departure-date.opened div.content > div.right-panel > div:nth-child(2) > div > div > div.container > div.pane.list > div:nth-child(1)"));
    }

    this.tripLength4to7DaysSelection = function () {
        return element(by.css("div.popup-departure-date.opened div.content > div.right-panel > div:nth-child(2) > div > div > div.container > div.pane.list > div:nth-child(2)"));
    }

    this.tripLength8to11DaysSelection = function () {
        return element(by.css("div.popup-departure-date.opened div.content > div.right-panel > div:nth-child(2) > div > div > div.container > div.pane.list > div:nth-child(3)"));
    }

    this.tripLength12to14DaysSelection = function () {
        return element(by.css("div.popup-departure-date.opened div.content > div.right-panel > div:nth-child(2) > div > div > div.container > div.pane.list > div:nth-child(4)"));
    }
    this.tripLength15to21DaysSelection = function () {
        return element(by.css("div.popup-departure-date.opened div.content > div.right-panel > div:nth-child(2) > div > div > div.container > div.pane.list > div:nth-child(5)"));
    }

    this.tripLengthNextThreeMonthsSelection = function () {
        return element(by.css("div.popup-departure-date.opened div.content > div.left-panel > ul > li:nth-child(4)"));
    }

    this.tripLengthAnytimeSelection = function () {
        return element(by.css("span[translate='foh.farefinder.results.date_input.anytime_title']"));
    }

    this.tripLengthNextCustomFromSelection = function () {
        return element(by.css("div.popup-departure-date.opened div.content > div.right-panel > div:nth-child(2) > div > div > div.container > div.date-selector > ng-form > div:nth-child(2) > input"));
    }

    this.tripLengthNextCustomToSelection = function () {
        return element(by.css("div.popup-departure-date.opened div.content > div.right-panel > div:nth-child(2) > div > div > div.container > div.date-selector > ng-form > div:nth-child(4) > input"));
    }

    this.tripLengthNextCustomOKBtn = function () {
        return element(by.css("div.popup-departure-date.opened div.content > div.right-panel > div:nth-child(2) > div > div > div.container > div.date-selector > ng-form > div:nth-child(6) > button"));
    }

    this.tripTypesFilters = function (index) {
        //return element(by.css(sprintf("form > div.trip-types-row > div.ng-isolate-scope > div.type-categories > div:nth-child(%s)", index + 1)));
        return element.all(by.css(".type-categories-element")).get(index);
    }

    this.tripAirportName = function () {
        return element(by.css("div[ng-repeat~='fare'] span[class~='airport']"));
    }

    this.resultGreatDealLink = function () {
        return element(by.css("div[class^='list-card'] > div[class^='ff-top-offer'] > a"));
    }

    this.listViewBtn = function () {
        return element(by.css("button>span[translate='foh.farefinder.view_types.list']"));
    }

    this.tileViewBtn = function () {
        return element(by.css("button>span[translate='foh.farefinder.view_types.tile']"));
    }

    this.mapViewBtn = function () {
        return element(by.css("button>span[translate='foh.farefinder.view_types.map']"));
    }
    this.zoomIn = function () {
        return element(by.css(".zoom-control-button.zoom-control-plus"));
    }
    this.zoomOut = function () {
        return element(by.css(".zoom-control-button.zoom-control-minus"));
    }

    this.viewResults = function (viewName) {
        return element(by.xpath("//farefinder-widget[contains(@view-type,'" + viewName + "')]"));
    }

    this.mapViewResults = function () {
        return element(by.css("div[class~='results-container-map'] div[ui-view='mapView']"));
    }

    this.recentlyViewedBtn = function () {
        return element(by.css("div[class~=trip-types-row] > div[class~=recently-viewed-desktop]"));
    }

    this.recentlyViewedResults = function (from, to) {
        return element(by.xpath("//a[contains(@class,'core-link-inline') and contains(text(),'" + from + " - " + to + "')]"));

    }

    this.deleteLastRecentView = function () {
        return element(by.css("core-icon.ff-close-icon.ng-isolate-scope > div > svg.ng-scope > use"));
    }

    this.flightResult = function (index) {
        return element(by.xpath("//div[contains(@class,'list-card')]/div/div[" + index + "]/a"));
    }

    this.flightResultTo = function (index) {
        return element(by.xpath("//div[contains(@class,'list-card')]/div/div[" + index + "]/a/div[1]/span[1]"));
    }

    this.messageForInvalid = function () {//TODO: to be confirmed
        return element(by.xpath("//*[contains(text(),'We are sorry but your dates are no longer valid. We are instead showing you the lowest fare for this route.')]"));
    }

    //market links
    this.marketLink = function (market) {
        return element.all(by.xpath("//a[contains(@href,'" + market + "')]")).get(0);
    }
    this.marketFlag = function (market) {
        return element(by.css("li#markets>a>span." + market));
    }
    //map view elements
    this.selectedAirportOnMapByAiportCode = function (airportCode) {
        return element.all(by.css("#" + airportCode + ">image")).get(0);
    }
    this.firstSelectedAirportOnMap = function () {
        return element(by.css("svg:not(.hidden)>image+text"));
    }
    this.selectedAirportsOnMap = function () {
        return element.all(by.css("svg:not(.hidden)>image+text"));
    }
    this.unselectedAiportOnMap = function (airportCode) {
        return element.all(by.css("#" + airportCode + ">text")).get(0);
    }
    this.yellowDots = function () {
        return element.all(by.xpath("//*[@style='opacity: 1;']"));
    }
    this.moreRoutes = function () {
        return element(by.css("div.more-airports.bottom_left_ff > div.inner-circle > div.text.more"));
    }
    this.activeMapViewTab = function () {
        return element(by.css(".core-btn-switch.active>span[translate='foh.farefinder.view_types.map']"));
    }

}

module.exports = FareFinderResultsPage;
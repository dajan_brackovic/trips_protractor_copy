var FareFinderResultsDetailsPage = function () {
    this.get = function (from, to) {
        browser.get("cheap-flights/" + from + "-to-" + to + "/");
    };

    this.getUpdatedUrl = function () {
        browser.get('cheap-flights');
    };
    //ff results title
    this.flightTitleTo = function () {
        return element(by.css("span.destination"));
    };
    this.flightTitleFrom = function () {
        return element(by.css("span.starting-point"));
    };
    this.tripSummeryType = function () {
        return element(by.css("span[translate='trips.one_way']"));
    };
    this.tripSummeryPassengerInfo = function () {
        return element(by.css("div.passenger-breakdown>span[ng-if='pax.num>0']"));
    };
    //outbound and inbound sections
    this.barChartHighlightedOutbound = function () {
        return element(by.css(".bar.selected"));
    };
    this.resultHeaderOutbound = function () {
        return element(by.css("[translate='foh.farefinder.histogram.inbound_title']"));
    };

    this.chartViewTabOutbound = function () {
        return element.all(by.css("button>span[translate='foh.farefinder.view_types.chart']")).get(0);
    };
    this.activeChartViewTabOutbound = function () {
        return element(by.css("button.active>span[translate='foh.farefinder.view_types.chart']"));
    };

    this.chartViewTabInInbound = function () {
        return element.all(by.css("button>span[translate='foh.farefinder.view_types.chart']")).get(1);
    };

    this.monthViewTabOutbound = function () {
        return element(by.css("button>span[translate='foh.farefinder.view_types.month']"));
    };
    this.activeMonthViewTabOutbound = function () {
        return element(by.css("button.active>span[translate='foh.farefinder.view_types.month']"));
    };

    this.monthViewTabInInbound = function () {
        return element(by.css("div[ng-show='showInboundFares']>div>div>button>span[translate='foh.farefinder.view_types.month']"));
    };

    this.rightCarouselInOutbound = function () {
        return element(by.css("div.histogram-container > div:nth-of-type(1) > div[months-to-show*='monthsAvailable'] button.arrow-right"));
    };

    this.leftCarouselInOutbound = function () {
        return element(by.css(".arrow-left[ng-click='goToPrev()']:not([disabled='disabled'])"));
    };

    this.leftCarouselDisabled = function () {
        return element.all(by.css(".arrow-left[ng-click='goToPrev()'][ng-disabled='farefinderCarousel.isPrevDisabled || farefinderCarousel.isCentered']")).get(0);
    };

    this.rightCarouselInOutbound = function () {
        return element.all(by.css(".arrow-right[ng-click='goToNext()'][ng-disabled='farefinderCarousel.isNextDisabled || farefinderCarousel.isCentered']")).get(0);
    };

    this.rightCarouselDisabled = function () {
        return element.all(by.css(".arrow-right[ng-disabled='farefinderCarousel.isNextDisabled || farefinderCarousel.isCentered']")).get(0);
    };

    this.outboundMonths = function () {
        return element.all(by.css("div.histogram-container > div:nth-of-type(1) li[class~='slide']"));
    };

    this.secondOutboundMonth = function () {
        return element(by.css("div.histogram-container > div:nth-of-type(1) > div[months-to-show*='monthsAvailable'] div.viewport li:nth-of-type(2)"));
    };
    this.thirdOutboundMonth = function () {
        return element(by.css("div.histogram-container > div:nth-of-type(1) > div[months-to-show*='monthsAvailable'] div.viewport li:nth-of-type(3)"));
    };

    this.thirdInboundMonth = function () {
        return element(by.css("div.histogram-container > div[ng-show='showInboundFares'] > div[months-to-show='monthsAvailable'] div.viewport li:nth-of-type(3)"));
    };
    this.secondInboundMonth = function () {
        return element(by.css("div.histogram-container > div[ng-show='showInboundFares'] > div[months-to-show='monthsAvailable'] div.viewport li:nth-of-type(2)"));
    };

    this.fourthInboundMonth = function () {
        return element(by.css("div.histogram-container > div[ng-show='showInboundFares'] > div[months-to-show='monthsAvailable'] div.viewport li:nth-of-type(4)"));
    };

    this.fifthInboundMonth = function () {
        return element(by.css("div.histogram-container > div[ng-show='showInboundFares'] > div[months-to-show='monthsAvailable'] div.viewport li:nth-of-type(5)"));
    };

    this.highestPriceInOutboundChart = function () {
        return element(by.css("div.histogram-container > div:nth-of-type(1) div.ff-chart div[class*='bar-holder'] > div[style*='height: 100%; max-height: 100%;']"));
    };

    this.highestPriceInInboundChart = function () {
        return element(by.css("div.histogram-container > div[ng-show='showInboundFares'] div div.ff-chart div[class*='bar-holder'] > div[style*='height: 100%; max-height: 100%;']"));
    };

    this.lowestPriceOutboundMonthView = function () {
        return element(by.xpath("//div[@class='histogram-container']/div[1]//div[contains(@class,'ff-calendar-container')]//li[contains(@class,'column')]//li[not(contains(@class,'disabled'))and .//div[contains(@class,'lowest-fare-label')]]"));
    };

    this.lowestPriceInInboundMonthView = function () {
        return element(by.xpath("//div[@class='histogram-container']//div[@ng-show='showInboundFares']//div[contains(@class,'ff-calendar-container')]//li[contains(@class,'column')]//li[not(contains(@class,'disabled'))and .//div[contains(@class,'lowest-fare-label')]]"));
    };

    this.totalCost = function () {
        return element(by.css("div[translate='trips.summary.heading.total_cost']+div.price-number"));
    };

    this.calendarElement = function () {
        return element(by.css("div[class~='ff-calendar-container']"))
    };

    this.chartElement = function () {
        return element(by.css("div[class~='ff-chart-container']"))
    };

    this.continueBtn = function () {
        return element(by.css(".fare-continue"));
    };

    this.backToFFLink = function () {
        return element(by.css("span[translate='foh.farefinder.histogram.back']"));
    };

    this.messageForInvalid = function () {//TODO: to be confirmed
        return element(by.xpath("//*[contains(text(),'We are sorry but your dates are no longer valid. We are instead showing you the lowest fare for this route.')]"));
    }
    //map view element
    this.mapViewToolTipBookButton = function () {
        return element(by.css(".core-btn-primary"));
    };
    this.viewByMonth = function () {
        return element.all(by.css("button>span[translate='foh.farefinder.view_types.month']"));
    };
    //first valid date
    this.selectedFareInChartView = function () {
        return element.all(by.css("div.bar.selected"));
    };

    this.selectedFareDateInMonthView = function () {
        return element.all(by.css("li.cell.selected")).get(0);
    }

    this.selectedFarePriceInMonthView = function () {
        return element.all(by.css("li.cell.selected>div>div>p.calendar-price")).get(0);
    }

    this.addReturnFlightButton = function () {
        return element(by.css("button[ng-click='addReturnFlight()']"));
    }

};
module.exports = FareFinderResultsDetailsPage;


var TimeTableHomepage = function () {

    var validDayPathOutbound = "(//li[not(contains(@class,'disabled')) and @ng-repeat='cell in cells']/div/ul[@ng-if='data[cell.day][0]'])";
    var validDayPathInbound = "(//li[not(contains(@class,'disabled')) and @ng-repeat='cell in cells']/div/ul[@ng-if='data[cell.day][0]'])";

    var validFarePath = "/li[@ng-repeat='item in data[cell.day] | limitTo:4']";

    this.get = function () {
        browser.get('timetable');
    };
    //search field
    this.timeTableSearchFromField = function () {
        return element(by.css("input[placeholder='Departure airport:']"));
    };

    this.timeTableSearchToField = function () {
        return element(by.css("input[placeholder='Destination airport:']"));
    };

    this.timeTableSearchButton = function () {
        return element(by.css("button[translate='foh.timetable.search']"));
    };

    this.alertMessageInvalidAirport = function () {
        return element(by.css("span[translate='foh.timetable.search_by_airport.errors.valid_airport']"));
    };

    this.clickableDiv = function () {
        return element(by.css(".timetable.ng-scope"));
    }

    this.alertMessageMissingFromAirport = function () {
        return element(by.css("span[translate='foh.timetable.search_by_airport.errors.departure_required']"));
    }

    this.alertMessageMissingToAirport = function () {
        return element(by.css("span[translate='foh.timetable.search_by_airport.errors.destination_required']"));
    }
    //headers
    this.flyOutHeader = function () {
        return element(by.css("span[translate='foh.timetable.results.flying_out']+span[translate='foh.timetable.route_from_to']"));
    }

    this.flyBackHeader = function () {
        return element(by.css("span[translate='foh.timetable.results.flying_back']+[translate='foh.timetable.route_from_to']"));
    }

    //monthly view
    this.monthlyViewTabFlyout = function () {
        return element.all(by.css("span[translate='foh.timetable.results.controls.view_by_month']")).get(0);
    }

    this.pastDates = function () {
        return element.all(by.css("ul>li.disabled"));
    }

    this.monthlyViewTabFlyback = function () {
        return element.all(by.css("span[translate='foh.timetable.results.controls.view_by_month']")).get(1);
    }
    this.defaultSelectedFlyoutDate = function () {
        return element(by.xpath("(//li[contains(@class,'selected today')])[1]"));
    }
    this.defaultSelectedFlybackDate = function () {
        return element(by.xpath("(//li[contains(@class,'selected today')])[2]"));
    }
    this.selectFlyoutMonthIncludingCurrent = function (monthIndex) {
        return element(by.xpath("(//ul[@class='scroller']/li[" + monthIndex + "]/div)[1]"));
    }
    this.selectFlybackMonthIncludingCurrent = function (monthIndex) {
        return element(by.xpath("(//ul[@class='scroller']/li[" + monthIndex + "]/div)[2]"));
    }
    this.validDays = function () {
        return element.all(by.xpath(validDayPathOutbound));
    }
    this.validDayIncludingToday = function (indexDay) {
        return element(by.xpath(validDayPathOutbound + "[" + indexDay + "]"));
    }
    this.allFaresEachValidDay = function (indexDay) {
        return element.all(by.xpath(validDayPathOutbound + "[" + indexDay + "]" + validFarePath));
    }
    this.validFareByDay = function (indexDay) {
        return element.all(by.xpath("(" + validDayPathOutbound + "[" + indexDay + "]" + validFarePath + ")"));
    }
    this.validDepartureTimeByDayAndOrder = function (indexDay, indexFare) {
        return element(by.xpath("(" + validDayPathOutbound + "[" + indexDay + "]" + validFarePath + ")[" + indexFare + "]/span[1]"));
    }
    this.validArriveTimeByDayAndOrder = function (indexDay, indexFare) {
        return element(by.xpath("(" + validDayPathOutbound + "[" + indexDay + "]" + validFarePath + ")[" + indexFare + "]/span[2]"));
    }
    this.fareWidgetDepartureTime = function (index) {
        return element(by.xpath("(//span[@class='fare-widget-flight-time']/span[contains(@class,'fare-widget-flight-departure-time')])[" + index + "]"));
    }
    this.fareWidgetArriveTime = function (index) {
        return element(by.xpath("(//span[@class='fare-widget-flight-time']/span[contains(@class,'fare-widget-flight-arrival-time')])[" + index + "]"));
    }

    this.fareWidgetDepartureTimeInbound = function (index) {
        return element(by.xpath("(//div[@type='inbound']//span[@class='fare-widget-flight-time']/span[contains(@class,'fare-widget-flight-departure-time')])[" + index + "]"));
    }
    this.fareWidgetDepartureTimeOutbound = function (index) {
        return element(by.xpath("(//div[@type='outbound']//span[@class='fare-widget-flight-time']/span[contains(@class,'fare-widget-flight-departure-time')])[" + index + "]"));
    }

    this.fareWidgetArrivalTimeInbound = function (index) {
        return element(by.xpath("(//div[@type='inbound']//span[@class='fare-widget-flight-time']/span[contains(@class,'fare-widget-flight-arrival-time')])[" + index + "]"));
    }
    this.fareWidgetArrivalTimeOutbound = function (index) {
        return element(by.xpath("(//div[@type='outbound']//span[@class='fare-widget-flight-time']/span[contains(@class,'fare-widget-flight-arrival-time')])[" + index + "]"));
    }

    this.fareWidgetDepartureOutboundPrice = function (index) {
        return element(by.xpath("(//div[@type='outbound']/div/div/ul/li/span[@class='fare-widget-flight-price'])[" + index + "]/span"));
    }
    this.fareWidgetDepartureInboundPrice = function (index) {
        return element(by.xpath("(//div[@type='inbound']/div/div/ul/li/span[@class='fare-widget-flight-price'])[" + index + "]/span"));
    }

    this.fareWidgetWidgetTitle = function () {
        return element.all(by.css("h1.fare-widget-title > span.fare-widget-flights-date"));
    }

    //weekly view related
    this.weeklyViewTabFlyout = function () {
        return element.all(by.css("span[translate='foh.timetable.results.controls.view_by_week']")).get(0);
    }

    this.weeklyViewTabFlyback = function () {
        return element.all(by.css("span[translate='foh.timetable.results.controls.view_by_week']")).get(1);
    }

    this.weeklyViewTabs = function () {
        return element.all(by.css("button[ng-click='vm.selectViewType(viewType)']:not(.active)"));
    }

    this.highlightedDayColumn = function (column) {
        return element(by.xpath("//ul[contains(@class, 'weekly')]/li[position()=" + column + " and contains(@class,'selected')]"));
    }

    this.selectDayColumn = function (column) {
        return element(by.xpath("//ul[contains(@class, 'weekly')]/li[position()=" + column + "]"));
    }
    this.allFaresInSelectedColum = function (column) {
        return element.all(by.xpath("//ul[contains(@class, 'weekly')]/li[position()=" + column + " and contains(@class,'selected')]/ul"));
    }

    this.eachFareDepartureFlyout = function (column, fareIndex) {
        return element(by.xpath("(//ul[contains(@class, 'weekly')]/li[position()=" + column + " and contains(@class,'selected')]/ul/li/div/ul)[" + fareIndex + "]/li/span[1]"));
    }
    this.eachFareArrivalFlyout = function (column, fareIndex) {
        return element(by.xpath("(//ul[contains(@class, 'weekly')]/li[position()=" + column + " and contains(@class,'selected')]/ul/li/div/ul)[" + fareIndex + "]/li/span[2]"));
    }

    this.eachFareDepartureFlyback = function (column, fareIndex) {
        return element(by.xpath("(//div/div/ul/li[position()=" + column + "]/ul/li/div/ul)[" + fareIndex + "]/li/span[1]"));
    }
    this.eachFareArrivalFlyback = function (column, fareIndex) {
        return element(by.xpath("(//div/div/ul/li[position()=" + column + "]/ul/li/div/ul)[" + fareIndex + "]/li/span[2]"));
    }

    this.eachFareDepartureFlybackWeekly = function (column, fareIndex) {
        return element(by.xpath("//div/div/ul/li[" + column + "]/ul/li/div[@ng-include='_weeklyTemplateId']/ul/li/span[1]"));

    }
    this.eachFareArrivalFlybackWeekly = function (column, fareIndex) {
        return element(by.xpath("//div/div/ul/li[" + column + "]/ul/li/div[@ng-include='_weeklyTemplateId']/ul/li/span[2]"));
    }
    //Carousel

    this.nextWeekLink = function () {
        return element.all(by.css("a.next-week"));
    }
    this.previousWeekLink = function () {
        return element.all(by.css("a.previous-week"));
    }
    this.currentWeekText = function () {
        return element.all(by.css("span.current-week"));
    }

    this.nextMonthLink = function () {
        return element.all(by.css("ul.scroller>li.item.active+li"));
    }

    this.unselectedAvailableMonth = function () {
        return element.all(by.css("ul.scroller>li.item"));
    }

    this.rightArrowMonthlyOutbound = function () {
        return element.all(by.css("div[type='outbound']>div>div>div>div>div>div>div>button.arrow.right"));
    }

    this.leftArrowMonthlyOutbound = function () {
        return element.all(by.css("div[type='outbound']>div>div>div>div>div>div>div>button.arrow.left"));
    }
    this.rightArrowMonthlyInbound = function () {
        return element.all(by.css("div[type='inbound']>div>div>div>div>div>div>div>button.arrow.right"));
    }

    this.leftArrowMonthlyInbound = function () {
        return element.all(by.css("div[type='inbound']>div>div>div>div>div>div>div>button.arrow.left"));
    }
    this.firstAirportInAirportList = function () { //Picks first Airport in the list
        return element.all(by.css("div.pane.right div.option:not(.ng-hide)")).get(0);
    };
    //miscellaneous Divs
    this.searchContainerDiv = function () {
        return element(by.css("h1.timetable-header"));
    };

};

module.exports = TimeTableHomepage;
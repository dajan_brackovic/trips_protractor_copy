var FlightInfoByNumber = function () {
    this.get = function () {
        browser.get('flight-info/number');
    };

    this.flightNumber = function () {
        return element(by.css("input.ng-pristine.ng-untouched.ng-valid"));
    };

    this.searchByNumberSubmitButton = function () {
        return element(by.css("[translate='foh.flight_info.search_by_number.search']"));

    }

    this.alertForEmptyFlight = function () {
        return element(by.css("[translate='foh.flight_info.search_by_number.errors.valid_number']"));
    }

    //search results
    this.resultsRows = function () {
        return element.all(by.css("li[ng-repeat='flight in flights']"));
    };

    this.resultFromAirport = function (index) {
        return element.all(by.css("li.from")).get(index);
    };
    this.resultToAirport = function (index) {
        return element.all(by.css("li.to")).get(index);
    };

    this.resultFlightNo = function (index) {
        return element.all(by.css("li.flight-num")).get(index);
    };


};

module.exports = FlightInfoByNumber;
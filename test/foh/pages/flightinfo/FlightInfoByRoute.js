var FlightInfoByRoute = function () {
    this.get = function () {
        browser.get('flight-info/route');
    };
    //Search Flight Info
    this.flightFrom = function () {
        return element(by.css("input[aria-labelledby='label-airport-selector-from']"));
    };
    this.flightTo = function () {
        return element(by.css("input[aria-labelledby='label-airport-selector-to']"));
    };
    this.submitSearchButton = function () {
        return element(by.css("button[translate='foh.flight_info.search_by_airport.search']"));
    };

    //search results
    this.resultsRows = function () {
        return element.all(by.css("li[ng-repeat='flight in flights']"));
    };

    this.resultFromAirport = function (index) {
        return element.all(by.css("li.from")).get(index);
    };
    this.resultToAirport = function (index) {
        return element.all(by.css("li.to")).get(index);
    };

    this.resultFlightNo = function (index) {
        return element.all(by.css("li.flight-num")).get(index);
    };

    //Alerts
    this.alertForEmptyFrom = function () {
        return element(by.css("span[translate='foh.flight_info.search_by_airport.errors.departure_required']"));
    };
    this.alertForEmptyTo = function () {
        return element(by.css("span[translate='foh.flight_info.search_by_airport.errors.destination_required']"));
    };
    this.verifyAlertForInvalidFrom = function () {
        return element(by.css("span[translate='foh.flight_info.search_by_airport.errors.valid_airport']"));
    };
    this.verifyAlertForInvalidTo = function () {
        return element(by.css("span[translate='foh.flight_info.search_by_airport.errors.valid_airport']"));
    };
    //miscellaneous Divs
    this.searchContainerDiv = function () {
        return element(by.css(".flight-info-search-row"));
    };
    //Switch to by  number view
    this.switchToNumberViewButton = function () {
        return element(by.css("button.number"));
    };

    //filter by time range
    this.allDay = function () {
        return element(by.css("button>span[translate='foh.flight_info.results.filter.all_day']"));
    };
    this.morning = function () {
        return element(by.css("button>span[translate='foh.flight_info.results.filter.morning']"));
    };
    this.afternoon = function () {
        return element(by.css("button>span[translate='foh.flight_info.results.filter.afternoon']"));
    };
    this.evening = function () {
        return element(by.css("button>span[translate='foh.flight_info.results.filter.evening']"));
    };

    this.departureTime = function () {
        return element.all(by.css("li.departure"));
    };

    //pagination
    this.laterFlights = function () {
        return element(by.css("div.next>a"));
    };
    this.earlierFlights = function () {
        return element(by.css("div.prev>a"));
    };

    //header
    this.headerDate = function () {
        return element(by.css(".date"));
    };


};

module.exports = FlightInfoByRoute;
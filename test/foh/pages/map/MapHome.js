var MapHome = function () {
    this.get = function () {
        browser.get('cheap-flight-destinations');
    };
    //Search Flight form
    this.fromField = function () {
        return element(by.css("input.map-departures-input"));
    };
    this.toField = function () {
        return element(by.css("input.map-arrivals-input"));
    };
    this.switchFromAndToAirports = function () {
        return element(by.css("span.searchbox-transfer>svg#Layer_1"));
    };

    this.clearSelectionLink = function () {
        return element(by.css("span[translate='foh.routemap.searchbox.clear_selection']"));
    };


    //bar chart view
    this.barChartBalloonPrice = function () {
        return element(by.css("div.bar.selected>div>div>div"));
    };
    this.barChartHighlightedResult = function () {
        return element(by.css("div.bar.selected"));
    };

    this.barChartMonthYear = function () {
        return element(by.css(".month-label.ng-binding"));
    };
    this.barChartAirportInfo = function () {
        return element(by.css("div[ng-bind='vm.destination.name']"));
    };
    this.barChartCountryInfo = function () {
        return element(by.css("div[ng-bind='vm.destination.country']"));
    };
    this.barChartBookFlightsButton = function () {
        return element(by.css(".core-btn-primary"));
    };
    this.barChartShowMorePricesLink = function () {
        return element(by.css(".see-more-button.ng-scope"));   // go to the farefinder/results/details page with lowest highlighted
    };
    this.barChartListAllResultsLink = function () {
        return element(by.css("span[translate='common.components.farefinder-card.details.back_to_all_results']"));
    };

    // List view
    this.listViewDateDropDown = function () {
        return element(by.css("div[data-popup-trigger='compactDepartureDate']"));
    };
    this.listViewBudgetDropDown = function () {
        return element(by.css("div[data-popup-trigger='farefinderBudgetInput']"));
    };
    this.listViewTripTypeDropDown = function () {
        return element(by.css("div[data-popup-trigger='listDropdownPopupId']"));
    };
    this.mapViewToolTipPrice = function () {
        return element(by.css(".booking_tooltip>div >span.price"));
    };

    this.mapViewToolTipBookButton = function () {
        return element(by.css(".booking_tooltip"));
    };

    this.switchToListView = function () {
        return element(by.css(".list-view-button-control"));
    };

    this.switchToMapView = function () {
        return element(by.css(".map-view-button-control"));
    };
    this.listViewSearchResultsLinks = function () {
        return element.all(by.css(".ng-scope.ng-isolate-scope>a"));
    };
    this.listViewSearchResultsCurrency = function () {
        return element.all(by.css(".symbol.ng-binding:not(.table-cell-xs)"));
    };

    this.listViewSearchResultsPriceInteger = function () {
        return element.all(by.css(".price-units"));

    };
    this.listViewSearchResultsPriceDecimal = function () {
        return element.all(by.css(".price-decimals"));

    };
    this.listViewSearchResultsToAirports = function () {
        return element.all(by.css("div[ng-repeat='fare in vm.fares']>a>div>span.airport"));
    };
    this.listViewSearchResultsToCountries = function () {
        return element.all(by.css("div[ng-repeat='fare in vm.fares']>a>div>span.country"));

    };
    this.clearFilterLink = function () {
        return element(by.xpath("//span[@translate='common.components.farefinder-card.filter_list.clear_filters']"));
    };

    //farefinder search frame
    this.availableMonth = function (index) {
        return element(by.xpath("//div[1]/div[3]/div/div/div[2]/core-option-selector/ul/li[" + index + "]/label/div/div[contains(@class,'value')]"));
    };

    this.availableYear = function (index) {
        return element(by.xpath("//div[1]/div[3]/div/div/div[2]/core-option-selector/ul/li[" + index + "]/label/div/div[contains(@class,'label')]"));
    };

    this.availableFixedBudgets = function (index) {
        return element.all(by.css("div[translate='foh.farefinder.and_under']")).get(index);
    };

    this.budgetInputField = function () {
        return element(by.name("custom-budget"));
    };

    this.budgetValueOkButton = function () {
        return element(by.css("button[translate='common.buttons.OK']"));
    };
    // search type of trip
    this.typeGolf = function () {
        return element(by.css("li[aria-label='Golf']"));
    };
    this.typeNightlife = function () {
        return element(by.css("li[aria-label='Nightlife']"));
    };

    this.typeCityBreak = function () {
        return element(by.css("li[aria-label='City Break']"));
    };
    this.typeOutdoor = function () {
        return element(by.css("li[aria-label='Outdoor']"));
    };
    this.typeBeach = function () {
        return element(by.css("li[aria-label='Beach']"));
    };
    this.typeFamily = function () {
        return element(by.css("li[aria-label='Family']"));
    };

    this.closeToDropDown = function () {
        return element(by.css("h3.header"));
    };

    this.zoomIn = function () {
        return element(by.css("div.zoom-control-button.zoom-control-plus > core-icon > div > svg"));
    }
    this.zoomOut = function () {
        return element(by.css("div.zoom-control-button.zoom-control-minus > core-icon > div > svg"));
    }

    this.listViewBtn = function () {
        return element(by.css(".list-view-button-control-outer>span"));
    }
    this.mapViewBtn = function () {
        return element(by.css(".map-view-button-control-outer>span"));

    }

};

module.exports = MapHome;
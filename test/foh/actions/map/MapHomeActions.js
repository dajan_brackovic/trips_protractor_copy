var Pages = require('../../../Pages');
var pages = new Pages();

var BUDGET20EURO = "20";
var BUDGET40EURO = "40";
var BUDGET60EURO = "60";
var BUDGET80EURO = "80";
var BUDGET100EURO = "100";
var BUDGET150EURO = "150";


var MapHomeActions = function () {
    var mapHomepage = pages.mapHomepage;
    var fareFinderResultsDetailPage = pages.fareFinderResultsDetailsPage;


    this.goToPage = function () {
        mapHomepage.get();
        actions.fOHActions.handlePreloader();
    }


    this.searchFlightWithBothAirportsSpecified = function (from, to) {
        this.fillTextField(mapHomepage.fromField(), from);
        this.fillTextField(mapHomepage.toField(), to);
    }

    this.switchBetweenListAndMapView = function () {
        mapHomepage.listViewBtn().click();
        mapHomepage.mapViewBtn().click();

    }
    this.zoomInZoomOut = function () {
        mapHomepage.zoomIn().click();
        mapHomepage.zoomOut().click();

    }


    this.searchFlightsWithMoreFilters = function (from, month, budget, type) {
        this.fillTextField(mapHomepage.fromField(), from);
        this.fillTextField(mapHomepage.toField(), "  ");
        mapHomepage.closeToDropDown().click();
        browser.waitForAngular();

        if (month !== "0") {
            mapHomepage.listViewDateDropDown().click();
            mapHomepage.availableMonth(month).click();
        }
        browser.waitForAngular();

        if (budget !== "0") {
            mapHomepage.listViewBudgetDropDown().click();
            switch (budget) {
                case BUDGET20EURO:
                    mapHomepage.availableFixedBudgets(0).click();
                    break;
                case BUDGET40EURO:
                    mapHomepage.availableFixedBudgets(1).click();
                    break;
                case BUDGET60EURO:
                    mapHomepage.availableFixedBudgets(2).click();
                    break;
                case BUDGET80EURO:
                    mapHomepage.availableFixedBudgets(3).click();
                    break;
                case BUDGET100EURO:
                    mapHomepage.availableFixedBudgets(4).click();
                    break;
                case BUDGET150EURO:
                    mapHomepage.availableFixedBudgets(5).click();
                    break;
                default:
                    browser.driver.actions().mouseDown(mapHomepage.listViewBudgetDropDown()).mouseMove(mapHomepage.budgetInputField()).mouseUp().perform();
                    mapHomepage.budgetInputField().sendKeys(budget);
                    mapHomepage.budgetValueOkButton().click();
                    break;
            }
        }
        browser.waitForAngular();
        if (type !== "0") {
            mapHomepage.listViewTripTypeDropDown().click();
            switch (type) {
                case 1:
                    mapHomepage.typeGolf().click();
                    break;
                case 2:
                    mapHomepage.typeNightlife().click();
                    break;
                case 3:
                    mapHomepage.typeCityBreak().click();
                    break;
                case 4:
                    mapHomepage.typeOutdoor().click();
                    break;
                case 5:
                    mapHomepage.typeBeach().click();
                    break;
                case 6:
                    mapHomepage.typeFamily().click();
                    break;
                default:
                    mapHomepage.listViewTripTypeDropDown().click();
                    break;
            }

        }
        browser.waitForAngular();

    }

    this.setThenClearFilter = function (from, month, budget, type) {
        var beforeFilterAndSelection;
        var afterFilterResults;
        var clearFilterResults;
        var clearSelectionResults;

        mapHomepage.listViewSearchResultsLinks().count().then(function (count) {
            beforeFilterAndSelection = count;
            reporter.addMessageToSpec("beforeFilterAndSelection" + beforeFilterAndSelection);
        });
        this.searchFlightsWithMoreFilters(from, month, budget, type);
        mapHomepage.listViewSearchResultsLinks().count().then(function (count1) {
            afterFilterResults = count1;
            reporter.addMessageToSpec("afterFilterResults" + afterFilterResults);

        });
        this.clearFilter();
        mapHomepage.listViewSearchResultsLinks().count().then(function (count2) {
            clearFilterResults = count2;
            reporter.addMessageToSpec("clearFilterResults" + clearFilterResults);

        });
        this.clearSlection();
        mapHomepage.listViewSearchResultsLinks().count().then(function (count3) {
            clearSelectionResults = count3;
            reporter.addMessageToSpec("clearSelectionResults" + clearSelectionResults);
            expect(beforeFilterAndSelection === clearSelectionResults).toBeTruthy();
            expect(beforeFilterAndSelection).toBeGreaterThan(afterFilterResults);
            expect(beforeFilterAndSelection).toBeGreaterThan(clearFilterResults);

        });

    }

    this.selectResultAndVerifyFieldsInChartViewFields = function (from, month, resultIndex, currency) {
        if (month !== "0") {
            mapHomepage.listViewDateDropDown().click();
            mapHomepage.availableMonth(month).getText().then(function (actualListViewMonth) {
                mapHomepage.availableYear(month).getText().then(function (actualListViewYear) {
                    mapHomepage.listViewDateDropDown().click();
                    mapHomepage.listViewSearchResultsToAirports().get(resultIndex).getText().then(function (listViewToAirport) {
                        mapHomepage.listViewSearchResultsToCountries().get(resultIndex).getText().then(function (listViewToCountry) {
                            mapHomepage.listViewSearchResultsPriceInteger().get(resultIndex).getText().then(function (listViewInteger) {
                                mapHomepage.listViewSearchResultsPriceDecimal().get(resultIndex).getText().then(function (listViewDecimal) {
                                    mapHomepage.listViewSearchResultsLinks().get(resultIndex).click();
                                    mapHomepage.barChartListAllResultsLink().click();
                                    mapHomepage.listViewSearchResultsLinks().get(resultIndex).click();
                                    //browser.sleep(10000);
                                    mapHomepage.barChartCountryInfo().getText().then(function (barChartToCountry) {
                                        expect(barChartToCountry).toBe(listViewToCountry);
                                    })
                                    mapHomepage.barChartMonthYear().getText().then(function (barChartMonthYear) {
                                        expect(barChartMonthYear).toContain(actualListViewMonth);
                                        expect(barChartMonthYear).toContain(actualListViewYear);
                                    })

                                });
                            });


                        });
                    });
                });
            });
        }

    }

    this.verifyResultsListUnderBudget = function (budget, currency) {
        mapHomepage.listViewSearchResultsCurrency().count().then(function (count) {
            for (var i = 0; i < count - 1; i++) {
                mapHomepage.listViewSearchResultsCurrency().get(i).getText().then(function (resultCurrency) {
                    reporter.addMessageToSpec("resultCurrency" + resultCurrency);
                    reporter.addMessageToSpec("currency" + currency);
                    expect(resultCurrency).toEqual(currency);
                });
            }

            for (var i = 0; i < count - 1; i++) {
                mapHomepage.listViewSearchResultsPriceInteger().get(i).getText().then(function (resultInteger) {
                    reporter.addMessageToSpec("resultInteger:" + resultInteger);
                    reporter.addMessageToSpec("budget:" + budget);
                    mapHomepage.listViewSearchResultsPriceDecimal().get(i).getText().then(function (resultDecimal) {
                        if (resultDecimal === "00") {
                            reporter.addMessageToSpec("resultInteger00:" + resultInteger);
                            checkPriceIsNotGreaterThan(resultInteger, budget);
                        }
                        else {
                            checkPriceIsLessThan(resultInteger, budget);
                        }
                    });

                });
            }
            ;

        });
    }


    this.clearFilter = function () {
        mapHomepage.clearFilterLink().click();

    }
    this.clearSlection = function () {
        mapHomepage.clearSelectionLink().click();
        //TODO: Verify the list of results contain all possible destinations

    }


    var checkPriceIsLessThan = function (actualFarePrice, budget) {
        var searchResultPrice = parseInt(actualFarePrice);
        var budgetPrice = parseInt(budget);
        expect(searchResultPrice).toBeLessThan(budgetPrice);
    }

    var checkPriceIsNotGreaterThan = function (actualFarePrice, budget) {
        var searchResultPrice = parseInt(actualFarePrice);
        var budgetPrice = parseInt(budget);
        reporter.addMessageToSpec("searchResultPrice00:" + searchResultPrice);
        reporter.addMessageToSpec("budgetPrice00:" + budgetPrice);
        expect(searchResultPrice <= budgetPrice).toBeTruthy();
    }


    this.switchSearchFieldsValues = function () {
        mapHomepage.switchFromAndToAirports().click();

    }


    this.bookFlightFromMapViewVerifyInFfResultsDetailsPage = function (from, to, currency) {
        mapHomepage.barChartBalloonPrice().getText().then(function (barChartPrice) {
            mapHomepage.barChartHighlightedResult().getAttribute('date-id').then(function (barChartDate) {
                reporter.addMessageToSpec("barChartPrice" + barChartPrice);
                mapHomepage.mapViewToolTipPrice().getText().then(function (mapViewPrice) {
                    reporter.addMessageToSpec("mapViewPrice" + mapViewPrice);
                    expect("Price: " + barChartPrice).toContain(mapViewPrice);
                    mapHomepage.mapViewToolTipBookButton().click();
                    fareFinderResultsDetailPage.resultHeaderOutbound().getText().then(function (titleText) {
                        expect(titleText).toContain(to);
                        expect(titleText).toContain(from);
                    });

                    fareFinderResultsDetailPage.flightTitleTo().getText().then(function (flightTo) {
                        expect(flightTo).toBe(to);
                    });

                    fareFinderResultsDetailPage.flightTitleFrom().getText().then(function (flightFrom) {
                        expect(flightFrom).toBe(from);
                    });

                    fareFinderResultsDetailPage.tripSummeryType().getText().then(function (tripSummery) {
                        expect(tripSummery).toBe("One way");
                    });
                    fareFinderResultsDetailPage.tripSummeryPassengerInfo().getText().then(function (passengerInfo) {
                        expect(passengerInfo).toBe("1 Adult");
                    });

                });
                fareFinderResultsDetailPage.barChartHighlightedOutbound().getAttribute('date-id').then(function (mapDate) {
                    expect(mapDate).toBe(barChartDate);
                });
            });


        });
    }


    this.bookFlightFromChartViewVerifyInFfResultsDetailsPage = function (from) {
        //TODO: no longer works due to element locator changes
        //mapHomepage.barChartBalloonPrice().getText().then(function (barChartPrice) {
        mapHomepage.barChartHighlightedResult().getAttribute('date-id').then(function (barChartDate) {
            mapHomepage.mapViewToolTipPrice().getText().then(function (mapViewPrice) {
                reporter.addMessageToSpec("mapViewPrice" + mapViewPrice);
                mapHomepage.barChartAirportInfo().getText().then(function (listViewToAirport) {
                    mapHomepage.barChartBookFlightsButton().click();
                    fareFinderResultsDetailPage.resultHeaderOutbound().getText().then(function (titleText) {
                        expect(titleText).toContain(from);
                        expect(titleText).toContain(listViewToAirport);
                    });
                    fareFinderResultsDetailPage.flightTitleTo().getText().then(function (flightTo) {
                        reporter.addMessageToSpec("toAirport" + listViewToAirport);
                        expect(flightTo).toBe(listViewToAirport);
                    });
                    fareFinderResultsDetailPage.flightTitleFrom().getText().then(function (flightFrom) {
                        expect(flightFrom).toBe(from);
                    });

                    fareFinderResultsDetailPage.tripSummeryType().getText().then(function (tripSummery) {
                        expect(tripSummery).toBe("One way");
                    });
                    fareFinderResultsDetailPage.tripSummeryPassengerInfo().getText().then(function (passengerInfo) {
                        expect(passengerInfo).toBe("1 Adult");
                    });

                });
                fareFinderResultsDetailPage.barChartHighlightedOutbound().getAttribute('date-id').then(function (mapDate) {
                    expect(mapDate).toBe(barChartDate);
                });
            });
        });
        //});
    }

    this.fillTextField = function (element, text) {
        element.clear();
        element.sendKeys(text);
        element.sendKeys(protractor.Key.ENTER);

    }


}

module.exports = MapHomeActions;


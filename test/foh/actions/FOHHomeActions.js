var Pages = require('../../Pages')
var pages = new Pages();
var Stations = require('../../Stations.js');
var stations = new Stations();
var DateActions = require('./../../trips/actions/helpers/DateActions')
var configFile = require('./../../protractor.conf.js')

var FOHHomeActions = function () {
    var fohHomePage = pages.fOHHomePage;
    var fohHomeActions = this;
    var toSelectOutbound = '';
    var toSelectInbound = '';
    var fareFinderPage = pages.fareFinderHomePage;
    var tripsPaxPage = pages.tripsPaxPage;
    var tripsExtraPage = pages.tripsExtrasPage;
    var tripsSummaryPage = pages.tripsSummaryPage;
    var EC = protractor.ExpectedConditions;
    var FLIGHTS = "Flights";
    var HOTELS = "Hotels";
    var CARS = "Cars";
    var self = this;
    var hotelsPage = pages.hotelsPage;
    var manageTripsPage = pages.manageTripsPage;


    this.goToPage = function () {
        fohHomePage.get();
        this.handlePreloader();

        browser.wait(EC.presenceOf(fohHomePage.dismissCookiePolicy()), 5000);

        fohHomePage.dismissCookiePolicy().isDisplayed().then(function (isDisplayed) {
            if (isDisplayed) {
                fohHomePage.dismissCookiePolicy().click();
            }
        })
    };

    this.handlePreloader = function () {
        browser.executeScript("document.querySelector('.core-page-loader').style.display = 'none';");
    }

    this.searchFlightOneWay = function (origin, destination) {
        this.goToPage();
        fohHomePage.btnOneWay().click();
        if (origin === " ") {
            this.clearField(fohHomePage.flightFrom());
            fohHomePage.searchContainerDiv().click();
        }
        else {
            this.enterAirportField(fohHomePage.flightFrom(), origin);
        }
        this.enterAirportField(fohHomePage.flightTo(), destination);
    };

    this.searchFlightOneWayForInvalidInput = function (origin, destination) {
        this.goToPage();
        fohHomePage.btnOneWay().click();
        if (origin === " ") {
            this.clearField(fohHomePage.flightFrom());
            fohHomePage.searchContainerDiv().click();
        }
        else {
            this.enterField(fohHomePage.flightFrom(), origin);
        }
        this.enterAirportField(fohHomePage.flightTo(), destination);
    };

    this.verifyReturnBtnSelected = function () {
        fohHomePage.btnReturn().getAttribute("aria-checked").then(function (checked) {
            expect(checked).toBe("true");
            reporter.addMessageToSpec("Actual: is checked = " + checked);
        });
        ;
    };

    this.verifyDatesPassengersWidgetExpanded = function () {
        fohHomePage.widgetDatesPassengersSelection().getAttribute("aria-expanded").then(function (checked) {
            expect(checked).toBe("true");
            reporter.addMessageToSpec("Actual: is widget expanded = " + checked);
        });
    };

    this.searchFlightReturn = function (origin, destination) {
        this.enterAirportField(fohHomePage.flightFrom(), origin);
        this.enterAirportField(fohHomePage.flightTo(), destination);
    };

    this.searchFlight = function (origin, destination) {
        this.enterAirportField(fohHomePage.flightFrom(), origin);
        if (destination == "") {
            fohHomePage.searchContainerDiv().click();

        }
        this.enterAirportField(fohHomePage.flightTo(), destination);
    };

    this.searchFlightInvalidInput = function (origin, destination) {
        if (origin === " ") {
            this.clearField(fohHomePage.flightFrom());
            fohHomePage.searchContainerDiv().click();
        }
        else {
            this.enterField(fohHomePage.flightFrom(), origin);
        }
        this.enterAirportField(fohHomePage.flightTo(), destination);
    };

    this.searchFlightSpanishDiscount = function (origin, destination) {
        this.enterAirportField(fohHomePage.flightFrom(), origin);
        if (destination == "") {
            fohHomePage.searchContainerDiv().click();

        }
        this.enterAirportField(fohHomePage.flightTo(), destination);
        fohHomePage.openCloseToFieldPopup().click();

    };

    this.searchFlightWithDefaultDeparture = function (destination) {
        this.enterAirportField(fohHomePage.flightTo(), destination);
    };

    this.verifyDefaultNonGeoDepartureAiport = function () {
        fohHomePage.flightFrom().click();
        expect(fohHomePage.flightDefaultFromCountry().isDisplayed()).toBeTruthy();
        expect(fohHomePage.flightDefaultFromAirport().isDisplayed()).toBeTruthy();
        fohHomePage.searchContainerDiv().click();

    }


    this.searchFlightDepartureOnly = function (origin) {
        this.enterAirportField(fohHomePage.flightFrom(), origin);
        fohHomePage.searchContainerDiv().click();
        fohHomePage.continueBtnFlightSearch().click();
    };

    this.searchFlightReturnOnly = function (destination) {
        fohHomePage.flightFrom().clear();
        fohHomePage.searchContainerDiv().click();
        this.enterAirportField(fohHomePage.flightTo(), destination);
        //fohHomePage.searchContainerDiv().click();
    };


    this.clickLetsGoBtn = function () {
        fohHomePage.letsGoBtnFlightSearch().click();
        this.confirmBizPlusAlert();
    };

    this.clickopenSpanishDiscountDropdown = function () {
        fohHomePage.openSpanishDiscountDropdown().click();
    };

    this.clickRoughspanishDiscountOptions5Percent = function (index) {
        fohHomePage.RoughspanishDiscountOptions5Percent(index).click();
    };

    this.cancelTodayAlertIfPresent = function () {
        if (fohHomePage.AlertMessageBookTodayCancelButton().isDisplayed()) {
            fohHomePage.AlertMessageBookTodayCancelButton().click();
        }
    };

    this.cancelTodayAlert = function () {
        fohHomePage.AlertMessageBookTodayCancelButton().click();
        //fohHomePage.searchContainerDiv().click();
        //expect(browser.getCurrentUrl()).toContain("/#/home/flights")

    };

    this.confirmTodayAlert = function () {
        fohHomePage.AlertMessageBookTodayOkButton().click();
    };

    this.confirmBizPlusAlert = function () {
        fohHomePage.bisPlusAlertMessageOk().isPresent().then(function (isPresent) {
            if (isPresent) {
                fohHomePage.bisPlusAlertMessageOk().click();
            }
        })
    };

    this.chooseDatesReturn = function (outBoundDate, inBoundDate) {
        if (outBoundDate > 0) {
            fohHomePage.openFlightFromCalendar().click();
            toSelectOutbound = fohHomePage.flightOutDate(outBoundDate).getText();
            fohHomePage.flightOutDate(outBoundDate).click();
            if (inBoundDate <= 0) {
                fohHomePage.closeFlightToCalendar().click();
            }
            else {
                toSelectInbound = fohHomePage.flightInDate(inBoundDate).getText();
                fohHomePage.flightInDate(inBoundDate).click();
            }
        }

        else if (outBoundDate == 0) {
            if (inBoundDate > 0) {
                fohHomePage.openFlightToCalendar().click();
                toSelectInbound = fohHomePage.flightInDate(inBoundDate).getText();
                fohHomePage.flightInDate(inBoundDate).click();
                fohHomePage.searchContainerDiv().click();
            }
        }
    };

    this.verifyWhenReselectOutboundDate = function (firstInboundDate, secondOutBoundDate) {
        fohHomePage.openFlightFromCalendar().click();
        reporter.addMessageToSpec(fohHomePage.selectedFlightOutDate().getText());
        var secondSelectOutbound = fohHomePage.flightOutDate(secondOutBoundDate).getText();
        fohHomePage.flightOutDate(secondOutBoundDate).click();
        if (secondOutBoundDate > firstInboundDate) {
            expect(fohHomePage.selectedFlightInDate().getText()).toEqual(secondSelectOutbound);

        }
        else {
            expect(fohHomePage.selectedFlightInDate().getText()).toEqual(toSelectInbound);
        }
    };


    this.verifyWhenReselectInDate = function (secondInBoundDate) {
        fohHomePage.openFlightToCalendar().click();
        fohHomePage.flightInDate(secondInBoundDate).click();
        fohHomePage.openFlightToCalendar().click();
        var secondSelectInbound = fohHomePage.flightInDate(secondInBoundDate).getText();
        expect(fohHomePage.selectedFlightInDate().getText()).toEqual(secondSelectInbound);
    };

    var hasClass = function (element, cls) {
        return element.getAttribute('class').then(function (classes) {
            return classes.split(' ').indexOf(cls) !== -1;
        });
    };


    this.addPAXNumber = function (paxNum) {
        fohHomePage.openPAXDropDown().click();
        if (paxNum.ADT > 1) {
            for (i = 0; i < paxNum.ADT - 1; i++) {
                fohHomePage.paxPlusButton(0).click();
            }
        }
        if (paxNum.TEEN > 0) {
            for (i = 0; i < paxNum.TEEN; i++) {
                fohHomePage.paxPlusButton(1).click();
            }
        }
        if (paxNum.CHD > 0) {
            for (i = 0; i < paxNum.CHD; i++) {
                fohHomePage.paxPlusButton(2).click();
            }
        }
        if (paxNum.INF > 0) {
            fohHomePage.paxPlusButton(3).click().then(function () {
                fohHomePage.travellingWithInfantOkButton().click();
                browser.sleep(2000);
            })
            for (i = 1; i < paxNum.INF; i++) {
                fohHomePage.paxPlusButton(3).click()
            }
        }
        if (configFile.config.capabilities.browserName === 'chrome') {
            fohHomePage.searchContainerDiv().click();
        }
        else {
        }
    }

    this.enterField = function (fieldElement, text) {
        fieldElement.clear();
        fieldElement.sendKeys(text);
        fieldElement.sendKeys(protractor.Key.ENTER);
        fohHomePage.searchContainerDiv().click();
    }

    this.enterAirportField = function (fieldElement, text) {
        fieldElement.clear();
        fieldElement.sendKeys(text);
        fieldElement.sendKeys(protractor.Key.ARROW_DOWN);
        fohHomePage.firstAirportInList().click();
    }

    this.clearField = function (fieldElement) {
        fieldElement.clear();

    }


    this.searchReturnFLightWithPax = function (paxMap, origin, destination, outDaysFromNow, returnDaysFromNow) {
        this.goToPage();
        this.searchFlightReturn(origin, destination);
        this.chooseDatesReturn(outDaysFromNow, (returnDaysFromNow - outDaysFromNow) + 1);
        this.addPAXNumber(paxMap);
        this.clickLetsGoBtn();
        reporter.addMessageToSpec("Origin: " + origin + " Destination: " + destination);
    }

    this.searchReturnFLightWithPaxMyFr = function (paxMap, origin, destination, outDaysFromNow, returnDaysFromNow) {
        this.searchFlightReturn(origin, destination);
        this.chooseDatesReturn(outDaysFromNow, (returnDaysFromNow - outDaysFromNow) + 1);
        this.addPAXNumber(paxMap);
        this.clickLetsGoBtn();
        reporter.addMessageToSpec("Origin: " + origin + " Destination: " + destination);
    };

    this.searchReturnFLightAirportsOnly = function (paxMap, origin, destination, outDaysFromNow, returnDaysFromNow) {
        this.goToPage();
        this.searchFlightReturn(origin, destination);
        this.chooseDatesReturn(outDaysFromNow, (returnDaysFromNow - outDaysFromNow) + 1);
        reporter.addMessageToSpec("Origin: " + origin + " Destination: " + destination);
    }

    this.searchSpanishReturnFLightWithPax = function (paxMap, origin, destination, outDaysFromNow, returnDaysFromNow, option) {
        this.goToPage();
        this.searchFlightReturn(origin, destination);
        this.chooseDatesReturn(outDaysFromNow, (returnDaysFromNow - outDaysFromNow) + 1);
        this.addPAXNumber(paxMap);
        this.selectSpanishDiscountBalearic(option);
        this.clickLetsGoBtnBalearic();
    };

    this.searchSpanishReturnFLightUSCulture = function (paxMap, origin, destination, outDaysFromNow, returnDaysFromNow) {
        this.searchFlightReturn(origin, destination);
        this.assertOnUsBalearicWarning();
        browser.sleep(2000);
        this.chooseDatesReturn(outDaysFromNow, (returnDaysFromNow - outDaysFromNow) + 1);
        this.addPAXNumber(paxMap);
        this.clickLetsGoBtnBalearic();
    };

    this.searchOneWayFLightWithPax = function (paxMap, origin, destination, outDaysFromNow) {
        this.goToPage();
        fohHomePage.btnOneWay().click();
        this.searchFlight(origin, destination);
        this.chooseDatesOneWay(outDaysFromNow);
        this.addPAXNumber(paxMap);
        this.clickLetsGoBtn();
        reporter.addMessageToSpec("Origin: " + origin + " Destination: " + destination);
    }

    this.searchOneWayFLightWithPaxMyFr = function (paxMap, origin, destination, outDaysFromNow) {
        fohHomePage.btnOneWay().click();
        this.searchFlight(origin, destination);
        this.chooseDatesOneWay(outDaysFromNow);
        this.addPAXNumber(paxMap);
        this.clickLetsGoBtn();
        reporter.addMessageToSpec("Origin: " + origin + " Destination: " + destination);
    }

    this.searchOneWayFlightWithPaxNextAvailableDate = function (paxMap, origin, destination) {
        this.goToPage();
        fohHomePage.btnOneWay().click();
        this.searchFlight(origin, destination);
        this.chooseNextAvailableDateOneWay();
        this.addPAXNumber(paxMap);
        this.clickLetsGoBtn();
        reporter.addMessageToSpec("Origin: " + origin + " Destination: " + destination);
    }


    this.searchSpanishOneWayFLightWithPax = function (paxMap, origin, destination, outDaysFromNow, option) {
        this.goToPage();
        fohHomePage.btnOneWay().click();
        this.searchFlightReturn(origin, destination);
        this.chooseDatesOneWay(outDaysFromNow);
        this.addPAXNumber(paxMap);
        this.selectSpanishDiscountBalearic(option);
        this.clickLetsGoBtnBalearic();
        reporter.addMessageToSpec("Origin: " + origin + " Destination: " + destination);
    }


    this.chooseDatesOneWay = function (outBoundDate) {
        fohHomePage.openFlightFromCalendar().click();
        browser.waitForAngular();
        fohHomePage.flightOutDate(outBoundDate).click();
    };

    this.chooseDatesOneWayModifyTrip = function (outBoundDate, origin, destination) {
        this.searchFlight(origin, destination);
        fohHomePage.openFlightFromCalendar().click();
        fohHomePage.flightOutDate(outBoundDate).click();
    };

    this.chooseDatesAndAirportsModifyTrip = function (outBoundDate, returnDate, origin, destination) {
        fohHomePage.btnReturn().click();
        this.searchFlightReturn(origin, destination);
        fohHomePage.openFlightFromCalendar().click();
        this.chooseDatesReturn(outBoundDate, returnDate);
    };

    this.verifyFlyBackDisappeared = function () {
        fohHomePage.flyBackCalendar().getAttribute("aria-hidden").then(function (isHidden) {
            expect(isHidden).toBe("true");
            reporter.addMessageToSpec("Actual: is widget hidden = " + isHidden);
        });
    };

    this.chooseNextAvailableDateOneWay = function () {
        fohHomePage.openFlightFromCalendar().click();
        fohHomePage.flightOutNextAvailableDateAfterToday().click();
    };

    this.verifyDepartTodayAlertAppears = function () {
        expect(fohHomePage.alertMessageDepartureToday().isDisplayed()).toBeTruthy();
    };

    this.verifyPassengerFields = function () {
        expect(fohHomePage.openPAXDropDown().isDisplayed()).toBeTruthy();
    };
    this.verifyReturnTodayAlertAppears = function () {
        expect(fohHomePage.alertMessageReturnToday().isDisplayed()).toBeTruthy();
    };

    this.verifyReturnDateRequiredAlertAppears = function () {
        expect(fohHomePage.errorPopUpFlyBackDateRound().isDisplayed()).toBeTruthy();
    };

    this.verifyDepartDateRequiredAlertAppears = function () {
        expect(fohHomePage.errorPopUpFlyOutDateRound().isDisplayed()).toBeTruthy();
    };

    this.verifyErrorPopUpValidAirportToAppears = function () {
        expect(fohHomePage.errorPopUpValidAirportTo().isDisplayed()).toBeTruthy();

    };

    this.verifyErrorPopUpInBoundAppears = function () {
        expect(fohHomePage.errorPopUpInBound().isDisplayed()).toBeTruthy();

    };


    this.verifyErrorPopUpOutBoundAppears = function () {
        expect(fohHomePage.errorPopUpOutBound().isDisplayed()).toBeTruthy();

    };

    this.verifyErrorPopUpInBound = function () {
        expect(fohHomePage.errorPopUpInBound().isDisplayed()).toBeTruthy();


    };

    this.VerifyErrorPopUpValidAirportFromAppears = function () {
        expect(fohHomePage.errorPopUpValidAirportFrom().isDisplayed()).toBeTruthy();

    };

    this.VerifyErrorPopUpFlyOutDateOneWayAppears = function () {
        expect(fohHomePage.errorPopUpFlyOutDateOneWay().isDisplayed()).toBeTruthy();

    };
    this.verifyPageOpened = function (pagedir) {
        expect(browser.driver.getCurrentUrl()).toContain(pagedir);
    };

    this.verifyPageSuccessOpened = function (pagedir) {
        expect(browser.driver.getCurrentUrl()).toContain(pagedir);
        browser.getCurrentUrl().then(function (pagedir) {
            httpGet(pagedir).then(function (result) {
                expect(result.statusCode).toBe(200);
            });
        });
    };
    this.verifyPageStatusWithoutOpen = function (pagedir) {
        console.log("pagedir:::" + pagedir);
        httpGet(pagedir).then(function (result) {
            expect(result.statusCode).toBe(200);
            expect(result.statusCode).toBeLessThan(400);
        });
    };


    function httpGet(siteUrl) {
        var http = require('https');
        var defer = protractor.promise.defer();
        http.get(siteUrl, function (response) {

            var bodyString = '';

            response.setEncoding('utf8');

            response.on("data", function (chunk) {
                bodyString += chunk;
            });

            response.on('end', function () {
                defer.fulfill({
                    statusCode: response.statusCode,
                    bodyString: bodyString
                });

                console.log("response.statusCode" + response.statusCode);
                currentStatus = response.statusCode;
            });

        }).on('error', function (e) {
            defer.reject("Got http.get error: " + e.message);
        });

        return defer.promise;


    }

    this.verifyNewNonAngularPageOpened = function (pagedir, pagetitle) {
        browser.getAllWindowHandles().then(function (handles) {
            var secondWindowHandle = handles[1]
            var firstWindowHandle = handles[0];
            browser.switchTo().window(secondWindowHandle).then(function () {
                browser.sleep(1000);
                browser.ignoreSynchronization = true;
                expect(browser.driver.getCurrentUrl()).toContain(pagedir);
                expect(browser.getTitle()).toContain(pagetitle);
                browser.driver.close();
                browser.ignoreSynchronization = false;
                browser.switchTo().window(firstWindowHandle).then(function () {
                });
            });

        });

    };
    this.verifyNonAngularPageOpened = function (pagedir, pagetitle) {
        browser.getAllWindowHandles().then(function (handles) {
            browser.sleep(1000);
            oldWindowHandle = handles[0];
            newWindowHandle = handles[1];
            browser.switchTo().window(newWindowHandle).then(function () {
                expect(browser.getCurrentUrl()).toContain(pagedir);
                expect(browser.getTitle()).toContain(pagetitle);
                browser.driver.close();
            });
        });
    };


    this.clickContinue = function () {
        fohHomePage.continueBtnFlightSearch().click();

    };

    this.verifySpanishDiscountAppeared = function () {
        expect(fohHomePage.openSpanishDiscountDropdown().isDisplayed()).toBeTruthy();

    };
    this.selectSpanishDiscount = function (option) {
        fohHomePage.openSpanishDiscountDropdown().click();

        switch (option) {
            case 0:
                fohHomePage.spanishDiscountOptions0Discount(0).click();
                break;
            case 5:
                fohHomePage.spanishDiscountOptions5Percent(5).click();
                break;
            case 10:
                fohHomePage.spanishDiscountOptions10Percent(10).click();
                break;
            case 50:
                fohHomePage.spanishDiscountOptions50Percent(50).click();
                break;
            case 55:
                fohHomePage.spanishDiscountOptions55Percent(55).click();
                break;
            case 60:
                fohHomePage.spanishDiscountOptions60Percent(60).click();
                break;

        }
    };

    this.selectSpanishDiscountBalearic = function (option) {
        fohHomePage.openSpanishDiscountDropdown().click();


        switch (option) {
            case 0:
                browser.executeScript('arguments[0].scrollIntoView()', fohHomePage.spanishDiscountOptions0Discount().getWebElement());
                fohHomePage.spanishDiscountOptions0Discount(0).click();
                break;
            case 5:
                browser.executeScript('arguments[0].scrollIntoView()', fohHomePage.spanishDiscountOptions5Percent().getWebElement());
                fohHomePage.spanishDiscountOptions5Percent(5).click();
                break;
            case 10:
                browser.executeScript('arguments[0].scrollIntoView()', fohHomePage.spanishDiscountOptions10Percent().getWebElement());
                fohHomePage.spanishDiscountOptions10Percent(10).click();
                break;
            case 50:
                browser.executeScript('arguments[0].scrollIntoView()', fohHomePage.spanishDiscountOptions50Percent().getWebElement());
                fohHomePage.spanishDiscountOptions50Percent(50).click();
                break;
            case 55:
                browser.executeScript('arguments[0].scrollIntoView()', fohHomePage.spanishDiscountOptions55Percent().getWebElement());
                fohHomePage.spanishDiscountOptions55Percent(55).click();
                break;
            case 60:
                browser.executeScript('arguments[0].scrollIntoView()', fohHomePage.spanishDiscountOptions60Percent().getWebElement());
                fohHomePage.spanishDiscountOptions60Percent(60).click();
                break;

        }
    };

    this.clickLetsGoBtnBalearic = function () {
        browser.executeScript('arguments[0].scrollIntoView()', fohHomePage.letsGoBtnFlightSearch().getWebElement());
        fohHomePage.letsGoBtnFlightSearch().click();
    };

    this.verifyAmountOfSpanishDiscounts = function (amountOfDiscounts) {
        fohHomePage.openSpanishDiscountDropdown().click();
        expect(fohHomePage.listSpanishDiscountOptions().count()).toEqual(amountOfDiscounts);

        browser.executeScript('window.scrollTo(0,300);');
        fohHomePage.listSpanishDiscountOptions().count().then(function (count) {
            reporter.addMessageToSpec("Amount of discounts offered: " + (count - 1));
        })
    };

    this.verifyAirportsShownRoundTrip = function () {
        var randomCountryname = stations.getRandomCountry();
        reporter.addMessageToSpec(randomCountryname);
        fohHomePage.flightFrom().click();
        fohHomePage.selectFlightFromCountry(randomCountryname).click();
        var airportsByCountry = stations.getAirportsNamesByCountry(randomCountryname);
        for (var i in airportsByCountry) {
            reporter.addMessageToSpec(airportsByCountry[i]);
            var firstpartOfAirportName = airportsByCountry[i].substring(0, 4);
            fohHomePage.selectFlightFromAirport(firstpartOfAirportName).click();
            fohHomePage.searchContainerDiv().click();
            fohHomePage.flightFrom().click();
            expect(fohHomePage.selectedCountry(randomCountryname).isDisplayed()).toBeTruthy();
            expect(fohHomePage.selectedCountryAirport(firstpartOfAirportName).isDisplayed()).toBeTruthy();
        }
    }

    this.verifyAirportsShownOneWay = function () {
        var randomCountryname = stations.getRandomCountry();
        fohHomePage.btnOneWay().click();
        fohHomePage.flightFrom().click();
        fohHomePage.selectFlightFromCountry(randomCountryname).click();
        var airportsByCountry = stations.getAirportsNamesByCountry(randomCountryname);
        for (var i in airportsByCountry) {
            var firstpartOfAirportName = airportsByCountry[i].substring(0, 4);
            fohHomePage.selectFlightFromAirport(firstpartOfAirportName).click();
            fohHomePage.searchContainerDiv().click();
            fohHomePage.flightFrom().click();
            expect(fohHomePage.selectedCountry(randomCountryname).isDisplayed()).toBeTruthy();
            expect(fohHomePage.selectedCountryAirport(firstpartOfAirportName).isDisplayed()).toBeTruthy();

        }
    }


    this.verifyAllCountriesShown = function () {
        fohHomePage.btnOneWay().click();
        fohHomePage.flightFrom().click();
        var allCountries = stations.getAllCountries();
        for (var i in allCountries) {
            expect(fohHomePage.selectFlightFromCountry(allCountries[i]).isDisplayed()).toBeTruthy();
        }
    }


    this.verifySpanishDiscountDefaultValue = function (value) {
        fohHomePage.openSpanishDiscountDropdown().getText().then(function (txt) {
            expect(txt).toContain(value);
        });
    };


    this.verifyMainlandsSpanishDiscountOptions = function (option) {
        fohHomePage.openSpanishDiscountDropdown().click();
        expect(fohHomePage.spanishDiscountOptions0Discount(0).isDisplayed()).toBeTruthy();
        expect(fohHomePage.spanishDiscountOptions5Percent(5).isDisplayed()).toBeTruthy();
        expect(fohHomePage.spanishDiscountOptions10Percent(10).isDisplayed()).toBeTruthy();
        expect(fohHomePage.spanishDiscountOptions50Percent(50).isPresent()).toBe(false);
        expect(fohHomePage.spanishDiscountOptions55Percent(55).isPresent()).toBe(false);
        expect(fohHomePage.spanishDiscountOptions60Percent(60).isPresent()).toBe(false);
    };


    this.clickCarTab = function () {
        fohHomePage.carsTab().click();
    };

    this.clickFlightsTab = function () {
        fohHomePage.flightsTab().click();
    };

    this.isTabSelected = function (tab) {
        tab.getAttribute('class').then(function (classes) {
            expect(classes).toContain("selected");
        });
    };

    this.verifyTabSelected = function (tabName) {
        var tab;
        switch (tabName) {
            case FLIGHTS :
                tab = fohHomePage.isFlightsTabSelected();
                break;
            case HOTELS :
                tab = fohHomePage.isHotelsTabSelected();
                break;
            case CARS :
                tab = fohHomePage.isCarsTabSelected();
                break;
        }
        fohHomeActions.isTabSelected(tab);
        fohHomeActions.verifyWidgetAppered(tabName);
    };

    this.verifyWidgetNotHidden = function (widget) {
        widget.getAttribute('aria-hidden').then(function (isHidden) {
            expect(isHidden).toEqual("false");
        });
    };

    this.verifyWidgetAppered = function (widgetName) {
        var widget;
        switch (widgetName) {
            case FLIGHTS :
                widget = fohHomePage.flightsWidget();
                break;
            case HOTELS :
                widget = fohHomePage.hotelsWidget();
                break;
            case CARS :
                widget = fohHomePage.carsWidget();
                break;
        }
        fohHomeActions.verifyWidgetNotHidden(widget);
    };

    this.verifyPickUpFieldValues = function () {
        fohHomePage.openFlightFromCalendar().click();
        fohHomePage.selectedFlightOutDateReturn().getAttribute("data-id").then(function (value) {
            reporter.addMessageToSpec("value=" + value);
            fohHomePage.closeFlightFromCalendar().click();

            fohHomePage.carsTab().click();
            fohHomePage.selectPickUpDateDropDown().click();
            fohHomePage.selectedPickUpDate().getAttribute("data-id").then(function (value2) {
                reporter.addMessageToSpec("value2=" + value2);
                expect(value2).toEqual(value);
            });
            fohHomePage.closeFlightFromCalendar().click();

        });
    };
    this.verifyPickUpFieldValuesSingle = function () {
        fohHomePage.openFlightFromCalendar().click();
        browser.sleep(4000);
        fohHomePage.selectedFlightOutDate().getAttribute("data-id").then(function (value) {
            reporter.addMessageToSpec("value=" + value);
            fohHomePage.closeFlightFromCalendar().click();

            fohHomePage.carsTab().click();
            fohHomePage.selectPickUpDateDropDown().click();
            fohHomePage.selectedPickUpDate().getAttribute("data-id").then(function (value2) {
                reporter.addMessageToSpec("value2=" + value2);
                expect(value2).toEqual(value);
            });
            fohHomePage.closeFlightFromCalendar().click();

        });
    };


    this.verifyDropOffFieldValues = function () {
        fohHomePage.flightsTab().click();
        fohHomePage.openFlightToCalendar().click();
        fohHomePage.selectedFlightInDate().getAttribute("data-id").then(function (value) {
            reporter.addMessageToSpec("value=" + value);
            fohHomePage.closeFlightToCalendar().click();
            fohHomePage.carsTab().click();
            fohHomePage.openDropOffCalendar().click();
            fohHomePage.selectedDropOffDate().getAttribute("data-id").then(function (value2) {
                reporter.addMessageToSpec("value2=" + value);
                expect(value2).toEqual(value);
            });
            fohHomePage.closeFlightToCalendar().click();
        });
    };


    this.verifyNeedCarInValues = function (needCarIn) {
        fohHomePage.needCarIn().click();
        expect(fohHomePage.selectedCountryAirport(needCarIn).isDisplayed()).toBeTruthy();
        fohHomePage.searchContainerDiv().click();

    };

    this.verifyCarSearchFieldValuesSingleTrip = function (flyout, flyback, needCarIn) {
        fohHomePage().selectPickUpDateDropDown().click();
        fohHomePage.selectedCheckInDate().getAttribute("data-id").then(function (value) {
            reporter.addMessageToSpec("selectedCheckInDate=" + value);
            reporter.addMessageToSpec("flyout=" + flyout);
            expect(value).toBe(flyout);
        });
        fohHomePage().selectDropOffDate().click();
        expect(fohHomePage.selectedDropOffDate().isPresent()).toBe(false)
        fohHomePage().needCarIn().click();
        expect(fohHomePage.selectedCountryAirport(needCarIn).isDisplayed()).toBeTruthy();

    };

    this.enterNeedCarIn = function (needCarIn) {
        this.enterField(fohHomePage.needCarIn(), needCarIn);

    };

    this.selectCountryOfResidence = function (country) {
        fohHomePage.countryOfResidence().click();
        fohHomePage.selectCountryOfResidence(country).click();
    };

    this.enterReturnLocation = function (returnCarTo) {
        fohHomePage.selectReturnCarRadioBtn().click();
        this.enterField(fohHomePage.returnLocation(), returnCarTo);

    };

    this.selectPickUpDate = function (validDateIndex) {
        fohHomePage.selectPickUpDateDropDown().click();
        browser.driver.actions().mouseDown(fohHomePage.selectPickUpDateDropDown()).mouseUp().perform();
        fohHomePage.flightOutDate(validDateIndex).click();
    };

    this.selectPickUpHour = function (validHourIndex) {
        fohHomePage.selectPickUpHourDropdown().click();
        fohHomePage.selectPickUpHourOption(validHourIndex).click();
    };

    this.selectPickUpTime = function (validTimeIndex) {
        fohHomePage.selectPickUpTimeDropdown().click();
        fohHomePage.selectPickUpTimeOption(validTimeIndex).click();
    };

    this.selectDropTime = function (validTimeIndex) {
        fohHomePage.selectDropOffTimeDropdown().click();
        fohHomePage.selectDropOffTimeOption(validTimeIndex).click();
    };


    this.selectPickUpMin = function (validMinIndex) {
        fohHomePage.selectPickUpMinDropDown().click();
        fohHomePage.selectPickUpMinOption(validMinIndex).click();
    };

    this.selectDropOffDate = function (validDateIndex) {
        fohHomePage.flightInDate(validDateIndex).click();

    };

    this.selectDropOffHour = function (validHourOption) {
        fohHomePage.selectDropOffHourDropDown().click();
        fohHomePage.selectDropOffHourOption(validHourOption).click();
    };

    this.selectDropOffMin = function (validMinIndex) {
        fohHomePage.selectDropOffMinDropDown().click();
        fohHomePage.selectDropOffMinOption(validMinIndex).click();
    };

    this.letsGoCarSearch = function (validMinIndex) {
        fohHomePage.carSearchLetsGoButton().click();
    };
    //hotel tab actions
    this.clickHotelsTab = function () {
        fohHomePage.hotelsTab().click();
    }

    this.verifyHotelsSearchFieldValues = function (hotelLocation) {
        fohHomePage.openFlightFromCalendar().click();
        fohHomePage.selectedFlightOutDateReturn().getAttribute("data-id").then(function (flightOutDate) {
            fohHomePage.closeFlightFromCalendar().click();
            fohHomePage.hotelsTab().click();
            fohHomePage.selectCheckInDropDown().click();
            fohHomePage.selectedCheckInDate().getAttribute("data-id").then(function (checkInDate) {
                expect(checkInDate).toEqual(flightOutDate);
            });
        });
        fohHomePage.flightsTab().click();
        fohHomePage.openFlightToCalendar().click();
        fohHomePage.selectedFlightInDate().getAttribute("data-id").then(function (flightInDate) {
            fohHomePage.closeFlightToCalendar().click();
            fohHomePage.hotelsTab().click();
            fohHomePage.selectCheckOutDropDown().click();
            fohHomePage.selectedCheckOutDate().getAttribute("data-id").then(function (checkOutDate) {
                expect(flightInDate).toEqual(checkOutDate);
            });
        });
        fohHomePage.searchContainerDiv().click();

    };

    this.chooseHotelBookingDates = function (outBoundDate, inBoundDate) {
        if (outBoundDate > 0) {
            fohHomePage.clickCheckInDropDown().click();
            browser.driver.actions().mouseDown(fohHomePage.clickCheckInDropDown()).mouseUp().perform();
            toSelectOutbound = fohHomePage.setCheckInDate(outBoundDate).getText();
            fohHomePage.setCheckInDate(outBoundDate).click();
            if (inBoundDate <= 0) {
                fohHomePage.clickCheckOutDropDown().click();
            }
            else {
                toSelectInbound = fohHomePage.setCheckOutDate(inBoundDate).getText();
                fohHomePage.setCheckOutDate(inBoundDate).click();
            }
        }

        else if (outBoundDate == 0) {
            if (inBoundDate > 0) {
                fohHomePage.clickCheckOutDropDown().click();
                toSelectInbound = fohHomePage.setCheckOutDate(inBoundDate).getText();
                fohHomePage.setCheckOutDate(inBoundDate).click();
                fohHomePage.performHotelSearch.click();
            }
        }
    };

    this.verifyHotelsSearchCheckInCheckOutFields = function (hotelLocation) {
        fohHomePage.openFlightFromCalendar().click();
        fohHomePage.selectedFlightOutDate().getAttribute("data-id").then(function (value) {
            fohHomePage.closeFlightFromCalendar().click();
            fohHomePage.hotelsTab().click();
            fohHomePage.selectCheckInDropDown().click();
            fohHomePage.selectedCheckInDate().getAttribute("data-id").then(function (value2) {
                expect(value2).toEqual(value);
            });
        });
        fohHomePage.searchContainerDiv().click();
        fohHomePage.selectCheckOutDropDown().click();
        expect(fohHomePage.selectedCheckOutDate().isPresent()).toBe(true);
        fohHomePage.searchContainerDiv().click();
        //TODO: verify populated hotel location field
    };

    this.setDestinationHotelName = function (destination) {
        fohHomePage.hotelLocation().sendKeys(destination);
    };


    this.performHotelSearch = function () {
        fohHomePage.letsGoBtnHotelSearch().click();
    };


    // best deals
    this.verifyBestFlightDealSelection = function (expectedAirport) {
        fohHomePage.cheapFlightsFrom().getText().then(function (selectedAirport) {
            expect(selectedAirport).toContain(expectedAirport);
        });
    };

    this.selectFlightDealCategory = function (category) {
        fohHomePage.flightDealCategory(category).click();
    };

    this.verifyResultsChangedForCategory = function (category) {
        fohHomePage.categoryResults().getAttribute("trip-type").then(function (tripCategory) {
            fohHomeActions.selectFlightDealCategory(category);

            fohHomePage.categoryResults().getAttribute("trip-type").then(function (currentTripCategory) {
                expect(currentTripCategory).not.toEqual(tripCategory);
            });
        });
    };

    this.searchBestDealsFrom = function (airport) {
        fohHomePage.openBestDealDepartDropdown().click();
        fohHomePage.selectDealFromAirport(airport).click();

    };

    this.selectOneLowestDeal = function (searchResultsIndex) {
        fohHomePage.lowestPriceDeal(searchResultsIndex).click();

    };

    this.clickFindMoreDeals = function () {
        browser.executeScript('arguments[0].scrollIntoView()', fohHomePage.moreDealsLink().getWebElement());
        fohHomePage.moreDealsLink().click();

    };

    this.verifyFromAirportSelected = function (airport) {
        fohHomePage.flightFrom().click();
        fohHomePage.selectedFromAirport().getText().then(function (selectedAirport) {
            expect(selectedAirport).toEqual(airport);
        });
        fohHomePage.flightFrom().click();
    };

    this.verifyToAirportSelected = function (airport) {
        fohHomePage.flightTo().click();
        fohHomePage.selectedToAirport().getText().then(function (selectedAirport) {
            expect(selectedAirport).toEqual(airport);
        });
        fohHomePage.flightTo().click();
    };

    this.verifyRouteSelected = function (origin, destination) {
        fohHomeActions.verifyFromAirportSelected(origin);
        fohHomeActions.verifyToAirportSelected(destination);
    };

    this.verifyDataTransferWhenClickMoreDeals = function (airport, country) {
        fareFinderPage.ffSearchFromField().click();
        expect(fareFinderPage.selectedFromAirport(airport).isDisplayed()).toBeTruthy();
        expect(fareFinderPage.selectedFromCountry(country).isDisplayed()).toBeTruthy();
    };

    this.clickOneWayBtn = function () {
        fohHomePage.btnOneWay().click();
    };
    //Below are actions for the myRyanair Dropdown tab
    this.clickMyRyanair = function () {
        fohHomePage.myRyanair().click();
        expect(fohHomePage.myRyanairMenu().isDisplayed()).toBeTruthy();
    };

    this.clickLoginRadioBtn = function () {
        fohHomePage.loginRadioBtn().click();
    };

    this.enterUsername = function (userName) {
        fohHomePage.userName().sendKeys(userName);
    };

    this.enterPassword = function (password) {
        fohHomePage.password().clear();
        fohHomePage.password().sendKeys(password);
    };

    this.clickLoginBtn = function () {
        fohHomePage.loginBtn().click();
    };

    this.getUserName = function () {
        fohHomePage.loggedUserName().getText().then(function (userName) {
            reporter.addMessageToSpec("Logged in Username: " + userName);
        });
    };

    this.login = function (userName, password) {
        fohHomeActions.clickMyRyanair();
        fohHomeActions.clickLoginRadioBtn();
        fohHomeActions.enterUsername(userName);
        fohHomeActions.enterPassword(password);
        fohHomeActions.clickLoginBtn();
        fohHomeActions.getUserName();
    };

    this.createAccount = function (email, password) {
        fohHomeActions.clickMyRyanair();
        fohHomePage.createAnAccountRadioBtn().click();
        browser.wait(EC.presenceOf(fohHomePage.emailFieldCreateAnAccountMyFr()), 5000);
        fohHomePage.emailFieldCreateAnAccountMyFr().sendKeys(email);
        fohHomePage.passwordFieldCreateAnAccountMyFr().sendKeys(password);
        fohHomeActions.clickLoginBtn();
    };

    this.uncheckPrivacyPolicy = function () {
        fohHomePage.clickAgreeRadioBtn().click();
        fohHomeActions.clickLoginBtn();
    };

    this.createAccountPrivacyPolicyUnchecked = function (email, password) {
        fohHomeActions.clickMyRyanair();
        fohHomePage.createAnAccountRadioBtn().click();
        fohHomePage.emailFieldCreateAnAccountMyFr().sendKeys(email);
        fohHomePage.passwordFieldCreateAnAccountMyFr().sendKeys(password);
    };

    this.selectCreateAccountBtn = function (email, password) {
        fohHomeActions.clickMyRyanair();
        fohHomePage.createAnAccountRadioBtn().click();
    };

    this.clickPrivacyPolicy = function () {
        fohHomePage.policyLink().click();
    };

    this.validatePasswordFieldMyFr = function (email, password) {
        expect(fohHomePage.passwordTermsMyFr().isPresent()).toBe(true);
    };

    this.clickLoggedUsername = function () {
        browser.wait(EC.presenceOf(fohHomePage.loggedUserName()), 5000);
        fohHomePage.loggedUserName().click();
    };

    this.clickLogout = function () {
        fohHomePage.logoutButton().click();
    };

    this.clickForgotYourPasswordMyFr = function () {
        fohHomePage.forgotYourPasswordMyFr().click();
    };

    this.logout = function () {
        fohHomeActions.clickLoggedUsername();
        fohHomeActions.clickLogout();
    };

    this.verifyNearlyThereMessage = function (email) {
        fohHomePage.nearlyThereMessage().getText().then(function (nearlyThereText) {
            expect(nearlyThereText).toContain(email);
        });
        expect(fohHomePage.resendEmail().isPresent()).toBe(true);
    };

    this.verifyWelcomeBackMessage = function (email) {
        fohHomePage.welcomeBackMessage().getText().then(function (welcomeBackText) {
            expect(welcomeBackText).toContain(email);
        });
        expect(fohHomePage.resendEmail().isPresent()).toBe(true);
    };

    this.verifyRecentSearchRoute = function (origin, destination) {
        fohHomePage.recentSearchRoute().getText().then(function (route) {
            expect(route).toContain(origin);
            expect(route).toContain(destination);
        });
    }

    this.verifyRecentSearchRouteByIndex = function (origin, destination, index) {
        fohHomePage.recentSearchRoutes(index).getText().then(function (route) {
            expect(route.toString()).toContain(origin);
            expect(route.toString()).toContain(destination);
        });
    }

    this.verifyRecentSearchNoMoreThanFive = function () {
        fohHomePage.recentSearchRoute(5).isDisplayed().then(function () {
            expect(false).toBeTruthy();
        }).thenCatch(function (error) {
            console.log(error);
            expect(true).toBeTruthy();
        });
    }

    this.assertOnHomePage = function () {
        expect(fohHomePage.flightFrom().isDisplayed()).toBeTruthy();
    };

    this.clickSomeWhereToClosePopup = function () {
        fohHomePage.searchContainerDiv().click();
    };

    this.selectMarket = function (market) {
        fohHomePage.selectCountryDropDown().click();
        fohHomePage.countryOption(market).click();
    };


    this.clickLinkFromMenuOption = function (menuIndex, submenuIndex) {
        fohHomePage.menuLink(menuIndex).click();
        fohHomePage.subMenuLink(submenuIndex).click();
    };

    this.assertPrivacyPolicyRadioBtn = function () {
        expect(fohHomePage.privacyPolicyRadioBtn().isSelected()).toBeTruthy();
    };

    this.clickCtaButton = function (index) {
        browser.executeScript('arguments[0].scrollIntoView()', fohHomePage.ctabuttons(index).getWebElement());
        fohHomePage.ctabuttons(index).click();
    };

    this.clickWholeCardLink = function (index) {
        browser.executeScript('arguments[0].scrollIntoView()', fohHomePage.wholeCardContentLinks(index).getWebElement());
        fohHomePage.wholeCardContentLinks(index).click();
    };

    this.openAndVerifyPrivacyPolicyPage = function () {

        browser.getAllWindowHandles().then(function (handles) {
            var secondWindowHandle = handles[1];
            var firstWindowHandle = handles[0];
            //the focus moves on the newest opened tab
            browser.switchTo().window(secondWindowHandle).then(function () {
                //check if the right page is opened
                expect(browser.driver.getCurrentUrl()).toContain("corporate/privacy-policy");

                expect(fohHomePage.privacyPageHeading().getText()).toContain("Privacy Policy");
                expect(fohHomePage.ryanairIcons().isPresent()).toBe(true);

                fohHomePage.privacyPageHeading().getText().then(function (text) {
                    reporter.addMessageToSpec("Page heading = " + text)
                });

            });

        });
    };

    //Market Actions

    this.selectMarketsMenu = function () {
        fohHomePage.marketsMenu().click();
    };

    this.clickMarketList = function (option) {
        fohHomePage.marketList(option).click();
        reporter.addMessageToSpec("Selected Market: " + option)
    };

    this.assertUrl = function (url) {
        expect(browser.driver.getCurrentUrl()).toContain(url);
        browser.getCurrentUrl().then(function (browserUrl) {
            reporter.addMessageToSpec("Browser Url: " + browserUrl)
        })
    };


    //Global header actions

    this.clickCars = function () {
        fohHomePage.btnCarHire().click();
    };

    this.clickCarsAndVerifyUrl = function (newurl) {
        fohHomePage.btnCarHireLink().getAttribute('href').then(function (hrefvalue) {
            fohHomePage.btnCarHire().click();
            self.verifyPageStatusWithoutOpen(hrefvalue);
            browser.sleep(1000);
            browser.getAllWindowHandles().then(function (handles) {
                var secondWindowHandle = handles[1];
                var firstWindowHandle = handles[0];
                //the focus moves on the newest opened tab
                browser.switchTo().window(secondWindowHandle).then(function () {
                    //check if the right page is opened with correct url
                    expect(browser.driver.getCurrentUrl()).toContain(newurl);
                    browser.driver.close();
                    browser.switchTo().window(firstWindowHandle).then(function () {
                    });
                });
            });
        });

    };

    this.clickHotelsAndVerifyUrl = function (newurl) {
        fohHomePage.btnHotelsLink().getAttribute('href').then(function (hrefvalue) {
            fohHomePage.btnHotels().click();
            reporter.addMessageToSpec("Link URL: " + hrefvalue);
            browser.sleep(2000);
            browser.driver.getAllWindowHandles().then(function (handles) {
                var secondWindowHandle = handles[1];
                var firstWindowHandle = handles[0];
                //the focus moves on the newest opened tab
                browser.driver.switchTo().window(secondWindowHandle).then(function () {
                    expect(browser.driver.getCurrentUrl()).toContain("hotels.ryanair.com/");
                    browser.ignoreSynchronization = true;

                    expect(hotelsPage.ryanairLogo().isPresent()).toBeTruthy();
                    expect(hotelsPage.partnershipLogo().isPresent()).toBeTruthy();

                    browser.driver.switchTo().window(firstWindowHandle).then(function () {
                        browser.driver.close();
                    });
                    browser.driver.switchTo().window(secondWindowHandle);
                    browser.ignoreSynchronization = false;
                });
            });
        });

    };

    //US Balearic Warning Dialog actions
    this.assertOnUsBalearicWarning = function () {
        browser.wait(EC.presenceOf(fohHomePage.lblUsBalearicWarningDialogHeader()), 5000);
        expect(fohHomePage.lblUsBalearicWarningDialogHeader().isPresent()).toBe(true);

        fohHomePage.lblUsBalearicWarningDialogHeader().getText().then(function (text) {
            reporter.addMessageToSpec("US Balearic Warning Message: " + text);
        });

        browser.wait(EC.presenceOf(fohHomePage.btnUsBalearicWarningDialogOk()), 5000);
        fohHomePage.btnUsBalearicWarningDialogOk().click();
    };

    this.clickSavedTripCard = function () {
        fohHomePage.savedTripContent().click();
    };

    this.clickSavedTripCardThenVerifyBroughtOverInfomation = function () {
        var fromAirport;
        var toAirport;
        var dateinfo;
        var priceofticket;
        fohHomePage.savedTripDeparture().getText().then(function (departure) {
            fromAirport = departure;
            fohHomePage.savedTripDestination().getText().then(function (destination) {
                toAirport = destination;
                fohHomePage.savedTripDate().getText().then(function (date) {
                    dateinfo = date;
                    fohHomePage.savedTripPrice().getText().then(function (price) {
                        priceofticket = price;
                        fohHomePage.savedTripContent().click();
                        actions.extrasActions.xOutReserveSeatPopUp();
                        tripsExtraPage.departureAirport().getText().then(function (dep) {
                            expect(fromAirport.toString()).toContain(dep.toString());
                            tripsExtraPage.destinationAirport().getText().then(function (des) {
                                expect(des).toContain(toAirport);
                                tripsExtraPage.priceAmount().getText().then(function (pri) {
                                    expect(priceofticket).toContain(pri);
                                    tripsExtraPage.checkOutButton().click();
                                    tripsPaxPage.date().getText().then(function (d) {
                                        expect(d).toContain(dateinfo.toString().substring(0, 5));
                                        tripsPaxPage.price().getText().then(function (p) {
                                            expect(priceofticket).toContain(p);
                                        });
                                    });

                                });
                            });
                        });
                    });
                });
            });
        });
    };


    this.clickBtnRemoveSavedTrip = function () {
        fohHomePage.btnRemoveSavedTrip().count().then(function (originalCount) {
            for (var i = 0; i < originalCount; i++) {
                fohHomePage.btnRemoveSavedTrip().get(0).click();
            }
        });
    };

    this.clickSideMenuVerifyNewTabUrl = function (newurl, sidemenuindex) {
        fohHomePage.sideMenuOpenClose().click();
        fohHomePage.sideMenuNewTabLink(sidemenuindex).getAttribute('href').then(function (hrefvalue) {
            fohHomePage.sideMenuNewTabLink(sidemenuindex).click();
            reporter.addMessageToSpec("Link URL: " + hrefvalue);
            browser.sleep(2000);
            browser.driver.getAllWindowHandles().then(function (handles) {
                var secondWindowHandle = handles[1];
                var firstWindowHandle = handles[0];
                browser.driver.switchTo().window(secondWindowHandle).then(function () {
                    expect(browser.driver.getCurrentUrl()).toContain(newurl);
                    browser.ignoreSynchronization = true;
                    browser.driver.switchTo().window(firstWindowHandle).then(function () {
                        browser.driver.close();
                    });
                    browser.driver.switchTo().window(secondWindowHandle);
                    browser.ignoreSynchronization = false;
                });
            });
        });

    };

    this.clickLinkFromSideMenu = function (sideMenuIdex, sideSubMenuIndex) {
        browser.sleep(3000);
        fohHomePage.sideMenuOpenClose().click();
        if (sideMenuIdex === -2) {
            fohHomePage.sideMenuMyRyanair().click();
            if (sideSubMenuIndex >= 0) {
                browser.sleep(1000);
                fohHomePage.sideSubMenuLink(sideSubMenuIndex).click();
            }
        }
        else if (sideMenuIdex === -1) {
            fohHomePage.sideMenuMarket().click();
            var EC = protractor.ExpectedConditions;
            browser.wait(EC.presenceOf(fohHomePage.sideMarketOptions().get(1)), 5000);
            if (sideSubMenuIndex >= 0) {
                browser.sleep(1000);
                fohHomePage.sideMarketList(sideSubMenuIndex).click();
            }
        }
        else {
            browser.sleep(1000);
            fohHomePage.sideMenuLink(sideMenuIdex).click();
            if (sideSubMenuIndex >= 0) {
                browser.sleep(1000);
                fohHomePage.sideSubMenuLink(sideSubMenuIndex).click();
            }
        }


    };
    this.verifyPageNotChanged = function () {
        browser.getCurrentUrl().then(function (pageurl) {
            httpGet(pageurl).then(function (result) {
                expect(result.statusCode).toBe(200);
            });
            actions.fOHActions.goToPage();
            browser.getCurrentUrl().then(function (homeurl) {
                expect(pageurl).toBe(homeurl);
            });

        });
    };

    this.clickLinkFromMenuLinkOption = function (menuIndex, submenuIndex) {
        fohHomePage.menuLink(menuIndex).click();
        if (submenuIndex > 0) {
            fohHomePage.subMenuLink(submenuIndex).click();
        }
    };

    this.clickLinkFromMenuLinkUser = function (userMenuIndex, submenuIndex) {
        browser.sleep(1000);
        fohHomePage.userMenuLink(userMenuIndex).click();
        if (submenuIndex >= 0) {
            browser.sleep(1000);
            fohHomePage.userSubMenuLink(submenuIndex).click();
        }

    };

    this.clickViewAllSaved = function () {
        fohHomePage.viewAllSaved().click();
    };


    this.getNumUpcomingTrips = function () {
        var deferred = protractor.promise.defer();
        var numtrips;
        fohHomePage.upcomingTrips().getText().then(function (upcomingtext) {
            if (upcomingtext == null) {
                deferred.reject();
            }
            else {
                console.log(upcomingtext);
                var numOfTripsString = upcomingtext.match(/\((.*)\)/);
                numtrips = parseInt(numOfTripsString[1]);
                deferred.fulfill({
                    num: numtrips
                });
            }
        });

        return deferred.promise;

    }


    this.viewAllUpcomings = function () {
        fohHomePage.btnViewAllUpcomings().click();
    };


    this.verifyNearestTripShown = function () {
        fohHomePage.upcomingTrips().click();
        fohHomePage.earliestUpcomingTripDate().getText().then(function (date) {
            var earliestTripDate = Date.parse(date);
            fohHomePage.btnViewAllUpcomings().click();
            for (var i = 0; i < manageTripsPage.upcomingTripsCards().length; i++) {
                manageTripsPage.upcomingTripsCardsJourneyDate().get(i).then(function (datevalue) {
                    var oneofdates = Date.parse(datevalue);
                    expect(oneofdates >= earliestTripDate).toBe(true);
                });
            }
        });
    };

    this.clickEarliestUpcomingCardAndVerify = function () {
        fohHomePage.upcomingTrips().click();
        fohHomePage.earliestUpcomingTripDate().getText().then(function (date) {
            fohHomePage.earliestUpcomingTripDepart().getText().then(function (depart) {
                fohHomePage.earliestUpcomingTripDestination().getText().then(function (destination) {
                    fohHomePage.earliestUpcomingTripImage().click();
                    browser.wait(EC.presenceOf(tripsSummaryPage.activeTripRoute()), 5000);
                    tripsSummaryPage.activeTripRoute().getText().then(function (route) {
                        var routeparts = route.split('(');
                        var departparts = depart.split('(');
                        var departparts2 = departparts[0].split(' ');
                        var destinationparts = destination.split('(');
                        var destinationparts2 = destinationparts[0].split(' ');
                        expect(routeparts[0]).toContain(departparts2[0]);
                        expect(routeparts[1]).toContain(destinationparts2[0]);
                        tripsSummaryPage.activeTripDate().getText().then(function (tripdate) {
                            var tripdateparts = tripdate.split(/(\s+)/);
                            var dateparts = date.split(/(\s+)/);
                            var datenum = tripdateparts[0].split("th");
                            var datenum2=datenum[0].split("st");
                            var datenum3=datenum2[0].split("nd");
                            var datenum4=datenum3[0].split("rd");
                            //compare month
                            expect(tripdateparts[2]).toBe(dateparts[4]);
                            //compare date
                            expect(datenum4[0]).toBe(dateparts[2]);
                        });
                    });
                });
            });
        });
    };

    this.clickOnCheapFlightsWidget = function (index) {
        fohHomePage.cheapFlightsFromWidget(index).click();
        browser.driver.wait(EC.titleContains("Cheap Flights"), 5000);
        browser.driver.getCurrentUrl().then(function(url){
            reporter.addMessageToSpec("Current Url: " + url);
        });
    };

};

module.exports = FOHHomeActions;


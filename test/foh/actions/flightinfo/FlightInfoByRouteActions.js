var Pages = require('../../../Pages');
var pages = new Pages();
var request = require('request');


var ALL_DAY = "All Day";
var MORNING = "Morning";
var AFTERNOON = "Afternoon";
var EVENING = "Evening";


var FlightInfoByRouteActions = function () {
    var flightInfoByRoutePage = pages.flightInfoByRoute;
    var numResultsInt = 0;

    this.goToPage = function () {
        flightInfoByRoutePage.get();
        actions.fOHActions.handlePreloader();

    }

    this.searchFlight = function (origin, destination) {
        this.enterField(flightInfoByRoutePage.flightFrom(), origin);
        this.enterField(flightInfoByRoutePage.flightTo(), destination);
    };

    this.submitSearch = function () {
        flightInfoByRoutePage.submitSearchButton().click();
    };

    this.verifyAlertForEmptyFrom = function () {
        expect(flightInfoByRoutePage.alertForEmptyFrom().isDisplayed()).toBeTruthy();

    }

    this.verifyAlertForEmptyTo = function () {
        expect(flightInfoByRoutePage.alertForEmptyTo().isDisplayed()).toBeTruthy();

    }

    this.verifyAlertForInvalidFrom = function () {
        expect(flightInfoByRoutePage.verifyAlertForInvalidFrom().isDisplayed()).toBeTruthy();

    }

    this.verifyAlertForInvalidTo = function () {
        expect(flightInfoByRoutePage.verifyAlertForInvalidTo().isDisplayed()).toBeTruthy();

    }
    this.verifyResultsAirportsNameCorrectly = function (from, to, fromAirport, toAirport) {
        flightInfoByRoutePage.resultsRows().count().then(function (numResults) {
            reporter.addMessageToSpec("numResults***************:" + numResults);
            numResultsInt = parseInt(numResults);
        });
        for (var i = 0; i < numResultsInt; ++i) {
            reporter.addMessageToSpec("numResults***************:" + numResultsInt);
            reporter.addMessageToSpec("index***************:" + i);
            flightInfoByRoutePage.resultFromAirport(i).getText().then(function (resultFrom) {
                expect(resultFrom).toContain(fromAirport);
                reporter.addMessageToSpec("resultFrom: " + resultFrom + ". Expected: " + fromAirport);
            });
            flightInfoByRoutePage.resultToAirport(i).getText().then(function (resultTo) {
                expect(resultTo).toContain(toAirport);
                reporter.addMessageToSpec("resultTo: " + resultTo + ". Expected: " + toAirport);
            });

        }

    }


    this.verifyResultsDepartureAirports = function (from, fromAirport) {
        flightInfoByRoutePage.resultsRows().count().then(function (numResults) {
            reporter.addMessageToSpec("numResults***************:" + numResults);
            numResultsInt = parseInt(numResults);
        });
        for (var i = 0; i < numResultsInt; ++i) {
            reporter.addMessageToSpec("numResults***************:" + numResultsInt);
            reporter.addMessageToSpec("index***************:" + i);
            flightInfoByRoutePage.resultFromAirport(i).getText().then(function (resultFrom) {
                expect(resultFrom).toContain(fromAirport);
                reporter.addMessageToSpec("resultFrom: " + resultFrom + ". Expected: " + fromAirport);
            });
        }

    }


    this.changeFilterSameTotalFlights = function () {
        var total;
        var morning;
        var afternoon;
        var evening;
        flightInfoByRoutePage.allDay().click();
        flightInfoByRoutePage.departureTime().count().then(function (length) {
            total = parseInt(length) - 1;
            reporter.addMessageToSpec("total" + total);
            flightInfoByRoutePage.morning().click();
            flightInfoByRoutePage.departureTime().count().then(function (lengthMorning) {
                morning = parseInt(lengthMorning) - 1;
                reporter.addMessageToSpec("morning" + morning);
                flightInfoByRoutePage.afternoon().click();
                flightInfoByRoutePage.departureTime().count().then(function (lengthAfernoon) {
                    afternoon = parseInt(lengthAfernoon) - 1;
                    reporter.addMessageToSpec("afternoon" + afternoon);
                    flightInfoByRoutePage.evening().click();
                    flightInfoByRoutePage.departureTime().count().then(function (lengthEvening) {
                        evening = parseInt(lengthEvening) - 1;
                        reporter.addMessageToSpec("evening" + evening);
                        var total2 = 0 + morning + afternoon + evening;
                        reporter.addMessageToSpec("total2" + total2);
                        expect(total).toBe(0 + morning + afternoon + evening);
                    });

                });

            });
        });

    }

    this.changeFilterThenVerifyTheChange = function (filter) {
        switch (filter) {
            case ALL_DAY:
                flightInfoByRoutePage.allDay().click();
                this.verifyTimeWithinTimeRange(0000, 2359);
                break;
            case MORNING:
                flightInfoByRoutePage.morning().click();
                this.verifyTimeWithinTimeRange(0000, 1200);

                break;
            case AFTERNOON:
                flightInfoByRoutePage.afternoon().click();
                this.verifyTimeWithinTimeRange(1200, 1800);

                break;
            case EVENING:
                flightInfoByRoutePage.evening().click();
                this.verifyTimeWithinTimeRange(1800, 2359);
                break;
            default:
                flightInfoByRoutePage.allDay().click();
                this.verifyTimeWithinTimeRange(0000, 2359);
                flightInfoByRoutePage.laterFlights().click();
                this.verifyTimeWithinTimeRange(0000, 2359);
                break;
        }
    }


    this.enterField = function (fieldElement, text) {
        fieldElement.clear();
        fieldElement.sendKeys(text);
        fieldElement.sendKeys(protractor.Key.ENTER);
        flightInfoByRoutePage.searchContainerDiv().click();
    }


    this.verifyTimeWithinTimeRange = function (startrange, laterrange) {
        var start = 1;
        flightInfoByRoutePage.departureTime().count().then(function (length) {
            flightInfoByRoutePage.departureTime().get(start).getText().then(function (departure) {
                var d = departure.replace(/[^0-9\.]+/g, "");
                expect(d >= startrange).toBeTruthy();
                reporter.addMessageToSpec("startrange" + startrange);
                reporter.addMessageToSpec("departure" + d);
            });
            var end = parseInt(length) - 1;
            flightInfoByRoutePage.departureTime().get(end).getText().then(function (departure2) {
                var d2 = departure2.replace(/[^0-9\.]+/g, "");
                expect(d2 < laterrange).toBeTruthy();
                reporter.addMessageToSpec("laterrange" + laterrange);
                reporter.addMessageToSpec("departure2" + d2);
            });

        });
    }

    this.checkDateOfHeader = function (dateString) {
        flightInfoByRoutePage.headerDate().getText().then(function (date) {
            var dateParts = date.split(" ");//e.g. Thu 22 Oct
            var dateString = date.split(" ");//e.g. Thu Oct 22 2015
            expect(dateString[0]).toBe(dateParts[0]);
            expect(dateString[1]).toBe(dateParts[1]);
            expect(dateString[2]).toBe(dateParts[2]);
        });
    }

    this.verifyPagination = function () {
        expect(flightInfoByRoutePage.earlierFlights().isDisplayed()).toBeFalsy();
        var start = 1;
        flightInfoByRoutePage.departureTime().count().then(function (length) {
            var numLength = parseInt(length);
            expect(numLength).toBe(51);
            flightInfoByRoutePage.departureTime().get(start).getText().then(function (departure) {
                var d1 = departure.replace(/[^0-9\.]+/g, "");
                reporter.addMessageToSpec(d1);
                flightInfoByRoutePage.laterFlights().click();
                flightInfoByRoutePage.departureTime().get(start).getText().then(function (departureNext) {
                    var d2 = departureNext.replace(/[^0-9\.]+/g, "");
                    reporter.addMessageToSpec(d2);
                    expect(d1 < d2);
                    expect(flightInfoByRoutePage.earlierFlights().isDisplayed()).toBeTruthy();
                });
            });
        });

    }
}

module.exports = FlightInfoByRouteActions;

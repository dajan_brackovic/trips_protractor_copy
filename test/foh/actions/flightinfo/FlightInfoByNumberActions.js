var Pages = require('../../../Pages');
var pages = new Pages();
var request = require('request');


var FlightInfoByNumberActions = function () {
    var flightInfoByNumber = pages.flightInfoByNumber;
    var numResultsInt = 0;


    this.goToPage = function () {
        flightInfoByNumber.get();
        actions.fOHActions.handlePreloader();

    }

    this.searchFlightByFlightNumber = function (flightNumber) {
        this.enterFieldInputOnly(flightInfoByNumber.flightNumber(), flightNumber);
    };
    this.verifyAlertForEmptyFlight = function () {
        expect(flightInfoByNumber.alertForEmptyFlight().isDisplayed()).toBeTruthy();

    };


    this.submitSearch = function () {
        flightInfoByNumber.searchByNumberSubmitButton().click();
        browser.waitForAngular();
    };


    this.verifyResultsCorrectlyShownCorrectly = function (flightNumber, from, to) {
        flightInfoByNumber.resultsRows().count().then(function (numResults) {
            reporter.addMessageToSpec("numResults***************:" + numResults);
            numResultsInt = parseInt(numResults);
            expect(numResultsInt).toBe(1);
        });
        for (var i = 1; i < numResultsInt; ++i) {
            reporter.addMessageToSpec("numResults***************:" + numResultsInt);
            reporter.addMessageToSpec("index***************:" + i);
            flightInfoByNumber.resultFlightNo(i).getText().then(function (number) {
                reporter.addMessageToSpec(i + "th result");
                expect(number).toContain(flightNumber);
                reporter.addMessageToSpec("actual results flight number: " + number + ". Expected: " + flightNumber);
            });

        }

        flightInfoByNumber.resultFlightNo(1).getText().then(function (resultFlightNumber) {
            expect(resultFlightNumber).toContain(flightNumber);
        });
        flightInfoByNumber.resultFromAirport(1).getText().then(function (resultFrom) {
            expect(resultFrom).toContain(from);
        });
        flightInfoByNumber.resultToAirport(1).getText().then(function (resultTo) {
            expect(resultTo).toContain(to);
        });


    }


    this.enterField = function (fieldElement, text) {
        fieldElement.clear();
        fieldElement.sendKeys(text);
        fieldElement.sendKeys(protractor.Key.ENTER);
        flightInfoByRoutePage.searchContainerDiv().click();
    }

    this.enterFieldInputOnly = function (fieldElement, text) {
        fieldElement.clear();
        fieldElement.sendKeys(text);

    }

}
module.exports = FlightInfoByNumberActions;


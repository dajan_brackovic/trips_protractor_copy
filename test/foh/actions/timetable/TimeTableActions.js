var Pages = require('../../../Pages');
var pages = new Pages();


var TimeTableActions = function () {
    var timeTableHomepage = pages.timeTableHomepage;
    var tripsHomepage = pages.tripsHomePage;


    this.goToPage = function () {
        timeTableHomepage.get();
        actions.fOHActions.handlePreloader();

    }
    //flight search
    this.searchTimeTable = function (from, to) {
        this.fillFRSearchFieldFrom(timeTableHomepage.timeTableSearchFromField(), from);
        this.fillFRSearchField(timeTableHomepage.timeTableSearchToField(), to);
        timeTableHomepage.timeTableSearchButton().click();

    }


    this.selectDateBeforeToday = function () {
        var now = Date.now();
        timeTableHomepage.pastDates().get(0).getAttribute("data-id").then(function (pastDateValue) {
            var d1 = Date.parse(pastDateValue);
            if (d1 > now) {
                alert("Error!");
            }
            else {
                try {
                    var EC = protractor.ExpectedConditions;
                    var isClickable = EC.elementToBeClickable(timeTableHomepage.pastDates().get(0));
                    browser.wait(isClickable, 5000);
                    element.click();
                }
                catch (err) {
                    reporter.addMessageToSpec("blabla message: " + err.message);
                }
            }
        });
    }

    this.verifyNothingChangesInFareWidgetDates = function () {
        var today = new Date();
        var todayCompose = today.toDateString();
        var todaySplits = [];
        todaySplits = todayCompose.split(" ");
        reporter.addMessageToSpec("today:" + today);
        reporter.addMessageToSpec(todayCompose);
        timeTableHomepage.fareWidgetWidgetTitle().get(0).getText().then(function (fareWidgetFlyoutDate) {
            timeTableHomepage.fareWidgetWidgetTitle().get(1).getText().then(function (fareWidgetFlybackDate) {
                reporter.addMessageToSpec(fareWidgetFlyoutDate);
                reporter.addMessageToSpec(fareWidgetFlybackDate);
                var d3 = Date.parse(fareWidgetFlyoutDate);
                var d4 = Date.parse(fareWidgetFlybackDate);
                expect(fareWidgetFlyoutDate).toBe(fareWidgetFlybackDate);
                expect(fareWidgetFlyoutDate).toContain(todaySplits[0]);
                expect(fareWidgetFlyoutDate).toContain(todaySplits[1]);
                var noFrontZeros = todaySplits[2].replace(/^0+/, '');
                expect(fareWidgetFlyoutDate).toContain(noFrontZeros);
            });
        });

    }


    //validation of invalid input
    this.verifyAlertMessageForInvalidFromAirport = function () {
        expect(timeTableHomepage.alertMessageInvalidAirport().isDisplayed()).toBeTruthy();
    }

    this.verifyAlertForMissingFromField = function () {
        expect(timeTableHomepage.alertMessageMissingFromAirport().isDisplayed()).toBeTruthy();
    }

    this.verifyAlertForMissingToField = function () {
        expect(timeTableHomepage.alertMessageMissingToAirport().isDisplayed()).toBeTruthy();
    }
    // calendar header actions and verification
    this.verifyCalenderHeader = function (from, to) {
        this.verifyElementValue(timeTableHomepage.flyOutHeader(), from + " to " + to);
        this.verifyElementValue(timeTableHomepage.flyBackHeader(), to + " to " + from);

    }

    // monthly view actions and verification
    this.verifyDefaultHighlightedDatesInMonthlyView = function () {
        var today = new Date();
        today = formatDateToString(today);
        reporter.addMessageToSpec(today);
        this.verifyElementAttribute(timeTableHomepage.defaultSelectedFlyoutDate(), "date-id", today);
        this.verifyElementAttribute(timeTableHomepage.defaultSelectedFlybackDate(), "date-id", today);

    }


    this.selectFlyOutMonthlyCalendarVerifyInWidget = function (monthIndex, dayIndex) {
        timeTableHomepage.selectFlyoutMonthIncludingCurrent(monthIndex).click();
        timeTableHomepage.validDayIncludingToday(dayIndex).click();
        timeTableHomepage.validFareByDay(dayIndex).count().then(function (length) {
            var count = 1;
            browser.sleep(3000);
            timeTableHomepage.validDepartureTimeByDayAndOrder(dayIndex, count).getText().then(function (departure) {
                timeTableHomepage.fareWidgetDepartureTime(count).getText().then(function (departureWidgetValue) {
                    expect(departure).toBe(departureWidgetValue);
                });

            });
            reporter.addMessageToSpec("first print out-----:" + count);
            timeTableHomepage.validArriveTimeByDayAndOrder(dayIndex, count).getText().then(function (arrival) {
                timeTableHomepage.fareWidgetArriveTime(count).getText().then(function (arrivalWidgetValue) {
                    browser.sleep(3000);
                    expect(arrival).toBe(arrivalWidgetValue);


                });
            });

            if (count < length) {
                var count1 = 2;
                timeTableHomepage.validDepartureTimeByDayAndOrder(dayIndex, count1).getText().then(function (departure) {
                    timeTableHomepage.fareWidgetDepartureTime(count1).getText().then(function (departureWidgetValue) {
                        expect(departure).toBe(departureWidgetValue);
                    });

                });
                reporter.addMessageToSpec("first print out-----:" + count1);
                timeTableHomepage.validArriveTimeByDayAndOrder(dayIndex, count1).getText().then(function (arrival) {
                    timeTableHomepage.fareWidgetArriveTime(count1).getText().then(function (arrivalWidgetValue) {
                        expect(arrival).toBe(arrivalWidgetValue);


                    });
                });
            }

        });


    }


    this.selectFlyOutMonthlyCalendarVerifyInBookingDetails = function (from, to, monthIndex, dayIndex, currencyType) {
        var currencySign;
        var currency;
        switch (currencyType) {
            case "EUR":
                currency = "EUR";
                currencySign = '€'
                break;
            case "GBP":
                currency = "GBP";
                currencySign = '£'
                break;
        }
        timeTableHomepage.selectFlyoutMonthIncludingCurrent(monthIndex).click();
        timeTableHomepage.validDayIncludingToday(dayIndex).click();
        var count = 1;
        timeTableHomepage.validDepartureTimeByDayAndOrder(dayIndex, count).getText().then(function (departure) {
            timeTableHomepage.fareWidgetArriveTime(count).getText().then(function (arrivalWidgetValue) {
                timeTableHomepage.fareWidgetDepartureOutboundPrice(count).getText().then(function (fareWidgetPrice) {
                    timeTableHomepage.fareWidgetWidgetTitle().get(0).getText().then(function (fareWidgetDate) {
                        timeTableHomepage.fareWidgetDepartureOutboundPrice(count).click();
                        expect(tripsHomepage.oneWayTripTypeText().isDisplayed()).toBeTruthy();
                        tripsHomepage.selectedTripSummary().get(0).getText().then(function (departAirport) {
                            expect(departAirport).toContain(from);
                        });
                        tripsHomepage.selectedTripSummary().get(1).getText().then(function (departAirport) {
                            expect(departAirport).toContain(to);

                        });
                        browser.sleep(5000);
                        tripsHomepage.selectedDepartureTime().getText().then(function (departureSelectedBookingTime) {
                            expect(departureSelectedBookingTime).toBe(departure);
                        });

                        tripsHomepage.selectedArrivalTime().getText().then(function (arrivalSelectedBookingTime) {
                            expect(arrivalSelectedBookingTime).toBe(arrivalWidgetValue);
                        });
                        tripsHomepage.selectedDate().getText().then(function (selectedDate) {
                            expect(selectedDate).toBe(fareWidgetDate);

                        });
                        tripsHomepage.selectedPriceInTable(count).getText().then(function (selectedPriceInTable) {
                            expect(selectedPriceInTable).toBe(fareWidgetPrice);
                        });

                    });
                });
            });
        });
    }

    this.selectFlyBackMonthlyCalendarVerifyInBookingDetails = function (from, to, monthIndex, currencyType) {
        var currencySign;
        var currency;
        switch (currencyType) {
            case "EUR":
                currency = "EUR";
                currencySign = '€'
                break;
            case "GBP":
                currency = "GBP";
                currencySign = '£'
                break;
        }
        timeTableHomepage.selectFlybackMonthIncludingCurrent(monthIndex).click();
        timeTableHomepage.validDays().count().then(function (length) {
            reporter.addMessageToSpec("length: " + length);
            timeTableHomepage.validDayIncludingToday(length).click();

            var count = 1;
            timeTableHomepage.validDepartureTimeByDayAndOrder(length, count).getText().then(function (departure) {
                timeTableHomepage.fareWidgetArriveTime(count).getText().then(function (arrivalWidgetValue) {
                    timeTableHomepage.fareWidgetDepartureInboundPrice(count).getText().then(function (fareWidgetPrice) {
                        timeTableHomepage.fareWidgetWidgetTitle().get(1).getText().then(function (fareWidgetDate) {
                            timeTableHomepage.fareWidgetDepartureInboundPrice(count).click();
                            expect(tripsHomepage.oneWayTripTypeText().isDisplayed()).toBeTruthy();
                            tripsHomepage.selectedTripSummary().get(0).getText().then(function (departAirport) {
                                expect(departAirport).toContain(to);
                            });
                            tripsHomepage.selectedTripSummary().get(1).getText().then(function (departAirport) {
                                expect(departAirport).toContain(from);

                            });
                            browser.sleep(5000);
                            tripsHomepage.selectedDate().getText().then(function (selectedDate) {
                                expect(selectedDate).toBe(fareWidgetDate);
                            });
                        });
                    });
                });
            });
        });
    }

    this.selectFlyBackMonthlyCalendarVerifyInWidget = function (monthIndex, dayIndex) {
        timeTableHomepage.selectFlybackMonthIncludingCurrent(monthIndex).click();
        timeTableHomepage.validDays().count().then(function (length) {
            var flyBackIndex = length;
            timeTableHomepage.validDayIncludingToday(flyBackIndex).click();
            timeTableHomepage.validFareByDay(flyBackIndex).count().then(function (length) {
                var count = 1;
                reporter.addMessageToSpec(count);
                timeTableHomepage.validDepartureTimeByDayAndOrder(flyBackIndex, count).getText().then(function (departure) {
                    timeTableHomepage.fareWidgetDepartureTime(count).getText().then(function (departureWidgetValue) {
                        reporter.addMessageToSpec("departureWidgetValue" + count);
                        reporter.addMessageToSpec(departureWidgetValue);
                        expect(departure).toBe(departureWidgetValue);
                    });

                });
                reporter.addMessageToSpec("first print out-----:" + count);
                timeTableHomepage.validArriveTimeByDayAndOrder(flyBackIndex, count).getText().then(function (arrival) {
                    timeTableHomepage.fareWidgetArriveTime(count).getText().then(function (arrivalWidgetValue) {
                        expect(arrival).toBe(arrivalWidgetValue);
                        reporter.addMessageToSpec("arrivalWidgetValue" + count);
                        reporter.addMessageToSpec(arrivalWidgetValue);

                    });
                });
                if (count < length) {
                    var count1 = 2;
                    reporter.addMessageToSpec(count1);
                    timeTableHomepage.validDepartureTimeByDayAndOrder(flyBackIndex, count1).getText().then(function (departure) {
                        timeTableHomepage.fareWidgetDepartureTime(count1).getText().then(function (departureWidgetValue) {
                            reporter.addMessageToSpec("departureWidgetValue:" + count1);
                            reporter.addMessageToSpec(departureWidgetValue);
                            expect(departure).toBe(departureWidgetValue);
                        });

                    });
                    reporter.addMessageToSpec("first print out-----:" + count1);
                    timeTableHomepage.validArriveTimeByDayAndOrder(flyBackIndex, count1).getText().then(function (arrival) {
                        timeTableHomepage.fareWidgetArriveTime(count1).getText().then(function (arrivalWidgetValue) {
                            expect(arrival).toBe(arrivalWidgetValue);
                            reporter.addMessageToSpec("arrivalWidgetValue:" + count1);
                            reporter.addMessageToSpec(arrivalWidgetValue);

                        });
                    });
                }
            });
        });


    }


    this.clickWeeklyViewButtonFlyout = function () {
        timeTableHomepage.weeklyViewTabFlyout().click();

    }

    this.clickWeeklyViewButtonFlyback = function () {
        timeTableHomepage.weeklyViewTabFlyback().click();

    }

    this.clickMonthlyViewButtonFlyback = function () {
        timeTableHomepage.monthlyViewTabFlyback().click();

    }

    this.verifyDefaultWeeklyView = function () {
        var today = new Date();
        var number = today.getDay();
        reporter.addMessageToSpec(number);
        today = formatDateToString(today);
        var offset = 1;
        var column = number + offset;
        expect(timeTableHomepage.highlightedDayColumn(column).isDisplayed()).toBeTruthy();
        this.verifyElementAttribute(timeTableHomepage.highlightedDayColumn(column), "date-id", today);

    };

    this.selectFlyOutWeeklyCalendarVerifyInWidget = function (column) {
        timeTableHomepage.selectDayColumn(column).click();
        timeTableHomepage.allFaresInSelectedColum(column).count().then(function (length) {
            var count;
            for (count = 1; count <= length; count++) {
                var departure = timeTableHomepage.eachFareDepartureFlyout(column, count).getText();
                timeTableHomepage.eachFareDepartureFlyout(column, count).getText().then(function (departure) {
                    count--;
                    timeTableHomepage.fareWidgetDepartureTime(count).getText().then(function (departureWidgetValue) {
                        reporter.addMessageToSpec("count" + count);
                        expect(departure).toBe(departureWidgetValue);
                        reporter.addMessageToSpec("departure" + departure);
                        reporter.addMessageToSpec("departure" + departureWidgetValue);
                    });

                });
                timeTableHomepage.eachFareArrivalFlyout(column, count).getText().then(function (arrival) {
                    timeTableHomepage.fareWidgetArriveTime(count).getText().then(function (arrivalWidgetValue) {
                        expect(arrival).toBe(arrivalWidgetValue);

                    });
                });
            }
            ;
        });

    };
    this.selectFlyBackWeeklyCalendarVerifyInBookingDetails = function (from, to, column, currencyType) {
        var currencySign;
        var currency;
        switch (currencyType) {
            case "EUR":
                currency = "EUR";
                currencySign = '€'
                break;
            case "GBP":
                currency = "GBP";
                currencySign = '£'
                break;
        }
        timeTableHomepage.selectDayColumn(column).click();
        var count = 1;

        timeTableHomepage.fareWidgetDepartureTimeInbound(count).getText().then(function (departureWidgetValue) {
            timeTableHomepage.fareWidgetArrivalTimeInbound(count).getText().then(function (arrivalWidgetValue) {
                timeTableHomepage.fareWidgetDepartureInboundPrice(count).getText().then(function (fareWidgetPrice) {
                    timeTableHomepage.fareWidgetWidgetTitle().get(1).getText().then(function (fareWidgetDate) {
                        timeTableHomepage.fareWidgetDepartureInboundPrice(count).click();
                        expect(tripsHomepage.oneWayTripTypeText().isDisplayed()).toBeTruthy();
                        tripsHomepage.selectedTripSummary().get(0).getText().then(function (departAirport) {
                            expect(departAirport).toContain(to);
                        });
                        tripsHomepage.selectedTripSummary().get(1).getText().then(function (departAirport) {
                            expect(departAirport).toContain(from);

                        });
                        browser.sleep(6000);
                        tripsHomepage.selectedDepartureTime().getText().then(function (departureSelectedBookingTime) {
                            expect(departureSelectedBookingTime).toBe(departureWidgetValue);
                        });

                        tripsHomepage.selectedArrivalTime().getText().then(function (arrivalSelectedBookingTime) {
                            expect(arrivalSelectedBookingTime).toBe(arrivalWidgetValue);
                        });
                        tripsHomepage.selectedDate().getText().then(function (selectedDate) {
                            expect(selectedDate).toBe(fareWidgetDate);

                        });

                    });
                });
            });

        });
    }


    this.selectFlyOutWeeklyCalendarVerifyInBookingDetails = function (from, to, column, currencyType) {
        var currencySign;
        var currency;
        switch (currencyType) {
            case "EUR":
                currency = "EUR";
                currencySign = '€'
                break;
            case "GBP":
                currency = "GBP";
                currencySign = '£'
                break;
        }
        timeTableHomepage.selectDayColumn(column).click();
        var count = 1;

        timeTableHomepage.fareWidgetDepartureTime(count).getText().then(function (departureWidgetValue) {
            timeTableHomepage.fareWidgetArriveTime(count).getText().then(function (arrivalWidgetValue) {
                timeTableHomepage.fareWidgetDepartureOutboundPrice(count).getText().then(function (fareWidgetPrice) {
                    timeTableHomepage.fareWidgetWidgetTitle().get(0).getText().then(function (fareWidgetDate) {

                        timeTableHomepage.fareWidgetDepartureOutboundPrice(count).click();
                        expect(tripsHomepage.oneWayTripTypeText().isDisplayed()).toBeTruthy();
                        tripsHomepage.selectedTripSummary().get(0).getText().then(function (departAirport) {
                            expect(departAirport).toContain(from);
                        });
                        tripsHomepage.selectedTripSummary().get(1).getText().then(function (departAirport) {
                            expect(departAirport).toContain(to);

                        });

                        tripsHomepage.selectedDepartureTime().getText().then(function (departureSelectedBookingTime) {
                            expect(departureSelectedBookingTime).toBe(departureWidgetValue);
                        });

                        tripsHomepage.selectedArrivalTime().getText().then(function (arrivalSelectedBookingTime) {
                            expect(arrivalSelectedBookingTime).toBe(arrivalWidgetValue);
                        });
                        tripsHomepage.selectedDate().getText().then(function (selectedDate) {
                            expect(selectedDate).toBe(fareWidgetDate);

                        });
                        tripsHomepage.selectedPriceInTable(count).getText().then(function (selectedPriceInTable) {
                            expect(selectedPriceInTable).toBe(fareWidgetPrice);
                        });

                    });
                });
            });

        });
    }


    this.selectFlyBackWeeklyCalendarVerifyInWidget = function (column) {
        timeTableHomepage.selectDayColumn(column).click();
        timeTableHomepage.allFaresInSelectedColum(column).count().then(function (length) {
            var count = 1;
            timeTableHomepage.eachFareDepartureFlybackWeekly(column, count).getText().then(function (departure) {
                timeTableHomepage.fareWidgetDepartureTimeInbound(count).getText().then(function (departureWidgetValue) {
                    expect(departure).toBe(departureWidgetValue);
                    reporter.addMessageToSpec("departure:" + departure);
                    reporter.addMessageToSpec("departureWidgetValue:" + departureWidgetValue);

                });

            });
            timeTableHomepage.eachFareArrivalFlybackWeekly(column, count).getText().then(function (arrival) {
                timeTableHomepage.fareWidgetArrivalTimeInbound(count).getText().then(function (arrivalWidgetValue) {
                    expect(arrival).toBe(arrivalWidgetValue);
                    reporter.addMessageToSpec("arrival:" + arrival);
                    reporter.addMessageToSpec("arrivalWidgetValue:" + arrivalWidgetValue);

                });
            });

        });

    };


    this.selectNextWeekInFlyout = function () {
        timeTableHomepage.nextWeekLink().get(0).click();
    };

    this.selectNextWeekInFlyback = function () {
        timeTableHomepage.nextWeekLink().get(1).click();
    };

    this.selectPreviousWeekInFlyout = function () {
        timeTableHomepage.previousWeekLink().get(0).click();
    };

    this.selectPreviousWeekInFlyback = function () {
        timeTableHomepage.previousWeekLink().get(1).click();
    };

    this.selectNextMonthInFlyout = function () {
        timeTableHomepage.nextMonthLink().get(0).click();
    };
    this.verifyNextMonthIsNotVisible = function () {
        expect(timeTableHomepage.nextMonthLink().get(0).isDisplayed()).toBeFalsy();
    };
    this.verifyNextMonthIsVisible = function () {
        expect(timeTableHomepage.nextMonthLink().get(0).isDisplayed()).toBeTruthy();
    };

    this.clickRightArrowOutbound = function () {
        timeTableHomepage.rightArrowMonthlyOutbound().click();
    }

    this.clickLeftArrowOutbound = function () {
        timeTableHomepage.leftArrowMonthlyOutbound().click();
    }
    this.selectNextMonthInFlyback = function () {
        timeTableHomepage.nextMonthLink().get(1).click();
    };


    this.selectAnyMonthInFlyOut = function (index) {
        timeTableHomepage.unselectedAvailableMonth().get(index).click();
    };


    this.selectMonthInFlyoutByIndex = function (index) {
        timeTableHomepage.unselectedAvailableMonth().get(index).click();
    };

    this.selectPreviousMonthInFlyback = function () {
        timeTableHomepage.unselectedAvailableMonth().get(1).click();
    };

//common method
    this.verifyElementValue = function (element, value) {
        element.getText().then(function (txt) {
            expect(txt).toContain(value);
        });
    };

    this.verifyElementAttribute = function (element, attribute, expectedValue) {
        element.getAttribute(attribute).then(function (value) {
            reporter.addMessageToSpec("value: " + value);
            reporter.addMessageToSpec("expectedValue: " + expectedValue);
            expect(value).toContain(expectedValue);
        });
    };

    function formatDateToString(date) {
        var dd = (date.getDate() < 10 ? '0' : '') + date.getDate();
        var MM = ((date.getMonth() + 1) < 10 ? '0' : '') + (date.getMonth() + 1);
        var yyyy = date.getFullYear();
        // create the format you want
        return (dd + "-" + MM + "-" + yyyy);
    }


    this.fillFRSearchField = function (element, text) {
        element.clear();
        element.sendKeys(text);
        element.sendKeys(protractor.Key.ARROW_DOWN);
        timeTableHomepage.firstAirportInAirportList().click();
    }

    this.fillFRSearchFieldFrom = function (fieldElement, text) {
        fieldElement.clear();
        fieldElement.sendKeys(text);
        fieldElement.sendKeys(protractor.Key.ENTER);
        timeTableHomepage.searchContainerDiv().click();
    }

    this.verifyDifferenceBetweenSelectedDefaultFlyOutAndFlyBack = function () {
        browser.sleep(4000);
        timeTableHomepage.defaultSelectedFlyoutDate().getText().then(function (defaultSelectedFlyoutTime) {
            timeTableHomepage.defaultSelectedFlybackDate().getText().then(function (defaultSelectedFlybackTime) {
                reporter.addMessageToSpec("defaultSelectedFlyoutTime:" + defaultSelectedFlyoutTime);
                reporter.addMessageToSpec("defaultSelectedFlybackTime:" + defaultSelectedFlybackTime);
                //TODO:
            });
        });
    }


}

module.exports = TimeTableActions;
var Pages = require('../../../Pages');
var pages = new Pages();
var FareFinderActions = require('../FOHHomeActions');

var INBOUND = "inbound";
var OUTBOUND = "outbound";

var FareFinderResultsDetailsActions = function () {
    var fareFinderResultsDetailsPage = pages.fareFinderResultsDetailsPage;
    var tripsHomepage = pages.tripsHomePage;

    var self = this;


    this.goToPage = function (from, to) {
        fareFinderResultsDetailsPage.get(from, to);
        actions.fOHActions.handlePreloader();
    };

    this.goToUrlWithParameters = function (from, to, outDateStart) {
        if (to !== "Anywhere") {
            browser.get("cheap-flights/?from=" + from + "&to=" + to + "&out-from-date=" + outDateStart);
        }
        else {
            browser.get("cheap-flights/?from=" + from + "&out-from-date=" + outDateStart);

        }
        actions.fOHActions.handlePreloader();

    }

    this.verifyDisplayForInvalid = function () {
        expect(fareFinderResultsDetailsPage.messageForInvalid().isDisplayed()).toBeTruthy()

    }

    this.verifyUrlContains = function (texts) {
        browser.sleep(6000);
        expect(browser.getCurrentUrl()).toContain(texts);
    }

    this.verifyFareFinderSearchResultsTitleFrom = function (from) {
        fareFinderResultsDetailsPage.resultHeaderOutbound().getText().then(function (actualValue) {
            expect(actualValue).toContain(from);
            reporter.addMessageToSpec("Actual: " + actualValue + ". Expected: " + from);
        });

    };

    this.verifyPriceChanged = function (section) {
        fareFinderResultsDetailsPage.totalCost().getText().then(function (previous) {
            switch (section) {
                case OUTBOUND:
                    self.selectOutboundChartViewPrice();
                    break;
                case INBOUND:
                    self.selectInboundChartViewPrice();
                    break;
            }
            fareFinderResultsDetailsPage.totalCost().getText().then(function (chartViewPrice) {
                reporter.addMessageToSpec("actual: " + chartViewPrice);
                reporter.addMessageToSpec("expected: " + chartViewPrice + " not equals " + previous);

                switch (section) {
                    case OUTBOUND:
                        self.clickLowestOutboundMonthViewPrice();
                        self.selectChartViewOutbound();
                        break;
                    case INBOUND:
                        self.clickLowestInboundMonthViewPrice();
                        self.selectChartViewInInbound();
                        break;
                }
                fareFinderResultsDetailsPage.totalCost().getText().then(function (monthViewPrice) {
                    reporter.addMessageToSpec("actual: " + monthViewPrice);
                    reporter.addMessageToSpec("expected: " + monthViewPrice + " not equals " + chartViewPrice);
                });
            });
        });
    };

    this.verifyClassNameContainsActive = function (element) {
        element.getAttribute('class').then(function (className) {
            expect(className).toContain('active');
        });
    };


    this.verifyAlternateBetweenViews = function () {
        self.selectMonthViewOutbound();
        expect(fareFinderResultsDetailsPage.activeMonthViewTabOutbound().isDisplayed()).toBeTruthy();
        expect(fareFinderResultsDetailsPage.calendarElement().isPresent()).toBeTruthy();

        self.selectChartViewOutbound();
        expect(fareFinderResultsDetailsPage.activeChartViewTabOutbound().isDisplayed()).toBeTruthy();
        expect(fareFinderResultsDetailsPage.chartElement().isPresent()).toBeTruthy();
    };

    this.verifyTotalCostTransferred = function (amount, currency) {
        fareFinderResultsDetailsPage.totalCost().getText().then(function (actualValue) {
            expect(actualValue).toBe(amount + ' ' + currency);
            reporter.addMessageToSpec("actual: " + actualValue);
            reporter.addMessageToSpec("expected: " + amount + ' ' + currency);
        });

    };

    this.verifyOutboundMonthSelection = function () {
        self.getSelectedOutboundMonthIndex(function (index1) {
            self.selectThirdOutboundMonth();
            self.getSelectedOutboundMonthIndex(function (index2) {
                expect(index1 < index2).toBeTruthy();
                reporter.addMessageToSpec("Expected " + index1 + " to be less than " + index2);
            });
        });
    };

    this.getSelectedOutboundMonthIndex = function (callback) {
        fareFinderResultsDetailsPage.outboundMonths().then(function (list) {
            list.forEach(function (item, i, list) {
                item.getAttribute('class').then(function (className) {
                    if (className.indexOf('active') >= 0) {
                        return callback(i);
                    }
                });
            });
        });
    };

    this.selectOutboundChartViewPrice = function () {
        this.clickHighestOutboundChartPrice();
        this.selectMonthViewOutbound();
    };

    this.clickNextOutboundMonthWithCarousel = function () {
        this.rightCarouselInOutbound().click();
    };

    this.clickPreviousOutboundMonthWithCarousel = function () {
        this.leftCarouselInOutbound().click();
    };

    this.selectInboundChartViewPrice = function () {
        this.clickHighestInboundChartPrice();
        this.selectMonthViewInbound();
    };

    this.selectChartViewInInbound = function () {
        fareFinderResultsDetailsPage.chartViewTabInInbound().click();
    };

    this.selectChartViewOutbound = function () {
        fareFinderResultsDetailsPage.chartViewTabOutbound().click();
    };

    this.selectMonthViewInbound = function () {
        fareFinderResultsDetailsPage.monthViewTabInInbound().click();
    };

    this.selectMonthViewOutbound = function () {
        fareFinderResultsDetailsPage.monthViewTabOutbound().click();
    };
    this.selectSecondInboundMonth = function () {
        fareFinderResultsDetailsPage.secondInboundMonth().click();
    };

    this.selectThirdInboundMonth = function () {
        fareFinderResultsDetailsPage.thirdInboundMonth().click();
    };

    this.selectFourthInboundMonth = function () {
        fareFinderResultsDetailsPage.fourthInboundMonth().click();
    };

    this.selectFifthInboundMonth = function () {
        fareFinderResultsDetailsPage.fifthInboundMonth().click();
    };

    this.selectSecondOutboundMonth = function () {
        fareFinderResultsDetailsPage.secondOutboundMonth().click();
    };
    this.selectThirdOutboundMonth = function () {
        fareFinderResultsDetailsPage.thirdOutboundMonth().click();
    };

    this.clickHighestInboundChartPrice = function () {
        fareFinderResultsDetailsPage.highestPriceInInboundChart().click();
    };

    this.clickHighestOutboundChartPrice = function () {
        fareFinderResultsDetailsPage.highestPriceInOutboundChart().click();
    };
    this.clickLowestInboundMonthViewPrice = function () {
        fareFinderResultsDetailsPage.monthViewTabInInbound().click();
        fareFinderResultsDetailsPage.lowestPriceInInboundMonthView().click();
    };

    this.clickLowestOutboundMonthViewPrice = function () {
        fareFinderResultsDetailsPage.lowestPriceOutboundMonthView().click();
    };

    this.clickContinueButton = function () {
        fareFinderResultsDetailsPage.continueBtn().click();
    };

    this.clickBackToFareFinder = function () {
        fareFinderResultsDetailsPage.backToFFLink().click();
    };

    this.verifyMonthViewShowsTheSameAsChartView = function () {
        var dateInListView = "";
        var dateInMapView = "";
        var priceInMapView = "";
        fareFinderResultsDetailsPage.selectedFareInChartView().get(0).getAttribute("data-id").then(function (actualDate1) {
            reporter.addMessageToSpec("actualDate1: " + actualDate1);
            dateInListView = actualDate1;
            fareFinderResultsDetailsPage.viewByMonth().get(0).click();
            fareFinderResultsDetailsPage.selectedFareDateInMonthView().getAttribute("date-id").then(function (actualDate2) {
                reporter.addMessageToSpec("actualDate2: " + actualDate2);
                dateInMapView = actualDate2;
                expect(dateInListView === dateInMapView).toBeTruthy();
                fareFinderResultsDetailsPage.selectedFarePriceInMonthView().getText().then(function (actualPrice) {
                    priceInMapView = actualPrice;

                });
            });

        });
    };

    this.clickMonthView = function () {
        fareFinderResultsDetailsPage.viewByMonth().get(0).click();
    };

    this.addReturnFlight = function () {
        fareFinderResultsDetailsPage.addReturnFlightButton().click();
    };


    this.verifyVisibilityOfCarousel = function () {
        expect(fareFinderResultsDetailsPage.leftCarouselDisabled().isDisplayed()).toBeTruthy()
        expect(fareFinderResultsDetailsPage.rightCarouselInOutbound().isDisplayed()).toBeTruthy();
        fareFinderResultsDetailsPage.rightCarouselInOutbound().click();
        expect(fareFinderResultsDetailsPage.leftCarouselInOutbound().isDisplayed()).toBeTruthy()
        expect(fareFinderResultsDetailsPage.rightCarouselInOutbound().isDisplayed()).toBeTruthy();
        fareFinderResultsDetailsPage.rightCarouselInOutbound().click();
        expect(fareFinderResultsDetailsPage.leftCarouselInOutbound().isDisplayed()).toBeTruthy()
        expect(fareFinderResultsDetailsPage.rightCarouselInOutbound().isDisplayed()).toBeTruthy();
        fareFinderResultsDetailsPage.rightCarouselInOutbound().click();
        expect(fareFinderResultsDetailsPage.leftCarouselInOutbound().isDisplayed()).toBeTruthy()
        expect(fareFinderResultsDetailsPage.rightCarouselInOutbound().isDisplayed()).toBeTruthy();
        fareFinderResultsDetailsPage.leftCarouselInOutbound().click();
        expect(fareFinderResultsDetailsPage.leftCarouselInOutbound().isDisplayed()).toBeTruthy()
        expect(fareFinderResultsDetailsPage.rightCarouselInOutbound().isDisplayed()).toBeTruthy();
        fareFinderResultsDetailsPage.leftCarouselInOutbound().click();
        expect(fareFinderResultsDetailsPage.leftCarouselInOutbound().isDisplayed()).toBeTruthy()
        expect(fareFinderResultsDetailsPage.rightCarouselInOutbound().isDisplayed()).toBeTruthy();
        fareFinderResultsDetailsPage.leftCarouselInOutbound().click();
        expect(fareFinderResultsDetailsPage.leftCarouselDisabled().isDisplayed()).toBeTruthy()
        expect(fareFinderResultsDetailsPage.rightCarouselInOutbound().isDisplayed()).toBeTruthy();

    }
    this.clickContinueVerifySelectionInTripSameAsFfMonthView = function () {
        var dateInMapView = "";
        var priceInMapView;
        fareFinderResultsDetailsPage.selectedFareDateInMonthView().getAttribute("date-id").then(function (date) {
            reporter.addMessageToSpec("actualDate2: " + date);
            dateInMapView = date;
            fareFinderResultsDetailsPage.selectedFarePriceInMonthView().getText().then(function (price) {
                reporter.addMessageToSpec("price: " + price);
                priceInMapView = price;
                browser.sleep(1000);
                fareFinderResultsDetailsPage.continueBtn().click();
                browser.sleep(6000);
                tripsHomepage.selectedDate().getText().then(function (selectedDate) {
                    reporter.addMessageToSpec("dateInMonthView" + dateInMapView);
                    var day = whichDay(selectedDate);
                    var month = whichMonth(selectedDate);
                    reporter.addMessageToSpec("month" + month);
                    reporter.addMessageToSpec("day" + day);

                });
                tripsHomepage.selectedPriceInTable().getText().then(function (selectedPriceInTable) {
                    expect(selectedPriceInTable).toBe(priceInMapView);
                });

            });
        });

    };

    function whichDay(dateString) {
        var daysOfWeek = new Array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
        reporter.addMessageToSpec(new Date(dateString).getDay());
        return daysOfWeek[new Date(dateString).getDay()];
    };
    function whichMonth(dateString) {
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];
        return monthNames[new Date(dateString).getMonth()];
    };

}

module.exports = FareFinderResultsDetailsActions;


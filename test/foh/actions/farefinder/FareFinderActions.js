var Pages = require('../../../Pages');
var pages = new Pages();

var BUDGET15EURO = "15";
var BUDGET40EURO = "40";
var BUDGET60EURO = "60";
var BUDGET80EURO = "80";
var BUDGET100EURO = "100";
var BUDGET150EURO = "150";


var FareFinderActions = function () {
    var fareFinderPage = pages.fareFinderHomePage;


    this.goToPage = function () {
        fareFinderPage.get();
        actions.fOHActions.handlePreloader();
    }

    this.goToMarketPage = function (market) {
        actions.fOHActions.selectMarket(market);
        fareFinderPage.goToFfByMarket(market);
    }

    this.searchFareFinderFlight = function (from, to, budgetAmount) {
        this.fillFRSearchField(fareFinderPage.ffSearchFromField(), from);
        this.fillFRSearchField(fareFinderPage.ffSearchToField(), to);
        if (budgetAmount !== BUDGET150EURO) {
            fareFinderPage.ffSearchBudgetField().click();
            switch (budgetAmount) {
                case BUDGET15EURO:
                    fareFinderPage.ffBudgetAmountList(1).click();
                    break;
                case BUDGET40EURO:
                    fareFinderPage.ffBudgetAmountList(2).click();
                    break;
                case BUDGET60EURO:
                    fareFinderPage.ffBudgetAmountList(3).click();
                    break;
                case BUDGET80EURO:
                    fareFinderPage.ffBudgetAmountList(4).click();
                    break;
                case BUDGET100EURO:
                    fareFinderPage.ffBudgetAmountList(5).click();
                    break;
                default:
                    fareFinderPage.ffBudgetDropDownCustomValue().sendKeys(budgetAmount);
                    fareFinderPage.ffBudgetDropDownOkBtn().click();
            }
        }
        browser.waitForAngular();
        fareFinderPage.ffSearchLetsGoBtn().click();
    }
    this.goDefaultSearch = function () {
        fareFinderPage.ffSearchLetsGoBtn().click();

    }
    this.fillFRSearchField = function (element, text) {
        element.clear();
        element.sendKeys(text);
        element.sendKeys(protractor.Key.ENTER);
    }

    this.verifyBudgetValue = function (expectedValue) {
        fareFinderPage.ffBudgetValue().getText().then(function (budget) {
            expect(budget).toContain(expectedValue);
        });

    };

    this.verifyAirportSelectedFF = function (airport) {
        fareFinderPage.ffSearchFromField().click();
        expect(fareFinderPage.selectedFromAirport(airport).isDisplayed()).toBeTruthy();
    };

}

module.exports = FareFinderActions;
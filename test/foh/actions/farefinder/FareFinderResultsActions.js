var Pages = require('../../../Pages');
var pages = new Pages();
var FareFinderActions = require('../FOHHomeActions');
var fareFinderActions = new FareFinderActions();
var sprintf = require("sprintf").sprintf;


var BUDGET20EURO = "20";
var BUDGET50EURO = "50";
var BUDGET60EURO = "60";
var BUDGET80EURO = "80";
var BUDGET100EURO = "100";
var BUDGET150EURO = "150";
var NEXT_THREE_MONTHS = "next3Months";
var SPECIFIC_DATE = "specificDate";
var DATE_RANGE = "dateRange";
var ANY_TIME = "Anytime";

var ONE_WAY = "oneWay";
var ONE_THREE = "1-3";
var FOUR_SEVEN = "4-7";
var EIGHT_ELEVEN = "8-11";
var TWELVE_FOURTEEN = "12-14";
var FIFTEEN_TWENTY_ONE = "15-21";
var CUSTOM_RANGE = "customRange";
var ENTIRE_MONTH = "entireMonth";


var BREACH = "Romance";
var CITY_BREAK = "City Break";
var FAMILY = "Family";
var NIGHTLIFE = "Nightlife";
var OUTDOOR = "Culture";
var GOLF = "Golf";

var LIST = "List";
var TILE = "Tile";
var MAP = "Map";

var FareFinderResultsActions = function () {
    var fareFinderResultsPage = pages.fareFinderResultsPage;
    var fareFinderResultsDetailsPage = pages.fareFinderResultsDetailsPage;
    var fareFinderResultsActions = this;

    this.goToPage = function () {
        fareFinderResultsPage.get();
        actions.fOHActions.handlePreloader();

    }

    this.goToUrlWithParameters = function (from, to, outDateStart) {
        if (to !== "Anywhere") {
            browser.get("cheap-flights/?from=" + from + "&to=" + to + "&out-from-date=" + outDateStart);
        }
        else {
            browser.get("cheap-flights/?from=" + from + "&out-from-date=" + outDateStart);

        }
        actions.fOHActions.handlePreloader();

    }

    this.goToPageByMarket = function (dir1, dir2, dir3) {
        this.goToPage();
        browser.driver.getCurrentUrl().then(function (url) {
            url.split("/").slice(3).join("/");
            reporter.addMessageToSpec("url" + url);
            var marketUrl = url.replace(/[\s\S][\s\S]\/[\s\S][\s\S]\/cheap-flights\//gi, dir1 + "/" + dir2 + "/" + dir3);
            reporter.addMessageToSpec("marketUrl" + marketUrl);
            browser.get(marketUrl);
            actions.fOHActions.handlePreloader();

        });
    };


    this.verifyMarketsPageOpens = function (dir1) {
        expect(fareFinderResultsPage.marketFlag(dir1).isDisplayed()).toBeTruthy();


    };

    this.goToUrlWithParameters = function (from, to, outDateStart) {
        if (to !== "Anywhere") {
            browser.get("cheap-flights/?from=" + from + "&to=" + to + "&out-from-date=" + outDateStart);
        }
        else {
            browser.get("cheap-flights/?from=" + from + "&out-from-date=" + outDateStart);

        }
        actions.fOHActions.handlePreloader();

    }
    this.verifyUrlContains = function (texts) {
        browser.sleep(6000);
        expect(browser.getCurrentUrl()).toContain(texts);
    }

    this.verifyFareFinderSearchResults = function (from, to, budget) {
        fareFinderResultsPage.ffSearchFromField().getAttribute('value').then(function (actualValue) {
            expect(actualValue).toEqual(from);
        });

        fareFinderResultsPage.ffSearchToField().getAttribute('value').then(function (actualValue) {
            expect(actualValue).toEqual(to);
        });

        fareFinderResultsPage.ffSearchBudgetFieldValue().getText().then(function (actualValue) {
            expect(actualValue).toMatch(/(?:£|€) \d{1,4}/);
            expect(actualValue.replace(/[^\d\.\-\ ]/g, '')).toMatch(/\d{2}/);
        });

    }

    this.verifyFareFinderSearchResultsWithoutCurrency = function (from, to, budget) {
        fareFinderResultsPage.ffSearchFromField().getAttribute('value').then(function (actualValue) {
            expect(actualValue).toEqual(from);
        });

        fareFinderResultsPage.ffSearchToField().getAttribute('value').then(function (actualValue) {
            expect(actualValue).toEqual(to);
        });

        fareFinderResultsPage.ffSearchBudgetFieldValue().getText().then(function (actualValue) {
            expect(actualValue).toMatch(/\d{2}/);
        });
    }

    this.verifyAllFareFinderSearchResults = function (from, to, budget, flyOut, tripLength) {
        fareFinderResultsActions.verifyFareFinderSearchResultsWithoutCurrency(from, to, budget);
        fareFinderResultsPage.flyOutDateBtn().click();

        var flyOutElement;
        switch (flyOut) {
            case NEXT_THREE_MONTHS:
                flyOutElement = fareFinderResultsPage.nextThreeMonthsBtn();
                break;
            case SPECIFIC_DATE:
                flyOutElement = fareFinderResultsPage.specificDateBtn();
                break;
            case ENTIRE_MONTH:
                flyOutElement = fareFinderResultsPage.entireMonthBtn();
                break;
            case DATE_RANGE:
                flyOutElement = fareFinderResultsPage.dateRangeBtn();
                break;
            case ANY_TIME:
                flyOutElement = fareFinderResultsPage.flyOutAnyTimeBtn();
                break;
        }


        fareFinderResultsPage.flyBackBtn().click();

        var tripLengthElement;
        switch (tripLength) {
            case ONE_WAY:
                tripLengthElement = fareFinderResultsPage.flyBackOneWayBtn();
                break;
            case NEXT_THREE_MONTHS:
                tripLengthElement = fareFinderResultsPage.flyBackNextThreeMonthsMenu();
                break;
            case ANY_TIME:
                tripLengthElement = fareFinderResultsPage.tripLengthAnytimeSelection();
                break;
            case DATE_RANGE:
                tripLengthElement = fareFinderResultsPage.dateRangeBtn();
                break;
            case ONE_THREE:
                tripLengthElement = fareFinderResultsPage.tripLength1to3DaysSelection();
                break;
            case FOUR_SEVEN:
                tripLengthElement = fareFinderResultsPage.tripLength4to7DaysSelection()
                break;
            case EIGHT_ELEVEN:
                tripLengthElement = fareFinderResultsPage.tripLength8to11DaysSelection();
                break;
            case TWELVE_FOURTEEN:
                tripLengthElement = fareFinderResultsPage.tripLength12to14DaysSelection();
                break;

            case FIFTEEN_TWENTY_ONE:
                tripLengthElement = fareFinderResultsPage.tripLength15to21DaysSelection();
                break;
        }

        tripLengthElement.click();
    }

    this.verifyListOfFlightsHaveValuesAndAreUnderBudget = function (budget) {
        fareFinderResultsPage.ffFaresListResults().count().then(function (count) {
            for (var i = 0; i < count; i++) {
                fareFinderResultsPage.ffFaresListResults().get(i).element(by.css('span.airport')).getText().then(function (txt) {
                    expect(txt).not.toBe(null);
                });
                fareFinderResultsPage.ffFaresListResults().get(i).element(by.css('span.country')).getText().then(function (txt) {
                    expect(txt).not.toBe(null);
                });

                fareFinderResultsPage.ffFaresListResults().get(i).element(by.css('span.price-units')).getText().then(function (txt) {
                    expect(txt).not.toBe(null);
                    checkPriceIsLessThen(txt, budget)

                });

                fareFinderResultsPage.ffFaresListResults().get(i).element(by.css("div.type")).getText().then(function (txt) {
                    expect(txt).not.toBe(null);
                });
                reporter.addMessageToSpec("Found Flights:  " + count);
            }
        });
    }

    this.verifyCostTransferredCorrectlyIntoDetailPage = function (nthResult) {
        fareFinderResultsPage.ffFaresListResults().get(nthResult).element(by.css('div.price')).getText().then(function (costInSearchResults) {
            fareFinderResultsPage.ffFaresListResults().get(nthResult).click();
            fareFinderResultsDetailsPage.totalCost().getText().then(function (costInSearchResultsDetails) {
                reporter.addMessageToSpec("costInSearchResults=" + costInSearchResults);
                reporter.addMessageToSpec("costInSearchResultsDetails=" + costInSearchResultsDetails);
                var fullPrice = costInSearchResultsDetails.split(" ");
                var amount = fullPrice[0];
                var currency = fullPrice[1];
                expect(costInSearchResults).toContain(amount);
            });
        });
    }

    this.selectResultFromSearchResults = function (from, nthResult, marketDir) {
        fareFinderResultsPage.ffFaresListResultsDestinations().get(nthResult).getText().then(function (destinationAirport) {
            fareFinderResultsPage.ffFaresListResults().get(nthResult).click();
            reporter.addMessageToSpec("destinationAirport=" + destinationAirport);
            //todo: expect(browser.getCurrentUrl()).toContain("/" + marketDir + "/" + from + "-to-" + destinationAirport + "/");
            expect(browser.getCurrentUrl()).toContain("/" + marketDir + "/" + from + "-to-");
        });
    }

    this.verifyLowestFareAirport = function (airportName) {
        fareFinderResultsPage.tripAirportName().getText().then(function (airport) {
            expect(airport.trim()).toEqual(airportName);
        });
    }

    this.verifyViewSelected = function (viewName) {
        var viewNameElement;

        switch (viewName) {
            case LIST:
                viewNameElement = fareFinderResultsPage.listViewBtn();
                break;
            case TILE:
                viewNameElement = fareFinderResultsPage.tileViewBtn();
                break;
            case MAP:
                viewNameElement = fareFinderResultsPage.mapViewBtn();
                break;
        }
        fareFinderResultsActions.verifyViewResults(viewName);

    }

    this.verifyViewResults = function (viewName) {
        var viewElement;
        switch (viewName) {
            case MAP:
                viewElement = fareFinderResultsPage.mapViewResults();
            default:
                viewElement = fareFinderResultsPage.viewResults(viewName.toLowerCase());
        }
        expect(viewElement.isPresent()).toBeTruthy();
    }

    this.verifyClassNameContainsActive = function (element) {
        element.getAttribute('class').then(function (className) {
            expect(className).toContain('active');
        });
    }

    this.verifyRecentlyViewedResults = function (from, to) {
        fareFinderResultsActions.clickRecentlyViewed();
        expect(fareFinderResultsPage.recentlyViewedResults(from, to).isPresent()).toBeTruthy();
    }

    this.deleteLastRecentViewItem = function () {
        fareFinderResultsPage.deleteLastRecentView().click;
    }

    this.searchFareFinderFlightWithoutTripTime = function (from, to, budgetAmount) {
        this.fillFRSearchField(fareFinderResultsPage.ffSearchFromField(), from);
        fareFinderResultsPage.closeDepartPopup().click;
        this.fillFRSearchField(fareFinderResultsPage.ffSearchToField(), to);
        fareFinderResultsPage.clickToFieldDiv().click();

        if (budgetAmount !== BUDGET150EURO) {
            fareFinderResultsPage.ffSearchBudgetField().click();
            switch (budgetAmount) {
                case BUDGET20EURO:
                    fareFinderResultsPage.ffBudgetAmountList(0).click();
                    break;
                case BUDGET50EURO:
                    fareFinderResultsPage.ffBudgetAmountList(1).click();
                    break;
                case BUDGET60EURO:
                    fareFinderResultsPage.ffBudgetAmountList(2).click();
                    break;
                case BUDGET80EURO:
                    fareFinderResultsPage.ffBudgetAmountList(3).click();
                    break;
                case BUDGET100EURO:
                    fareFinderResultsPage.ffBudgetAmountList(4).click();
                    break;
                case BUDGET150EURO:
                    fareFinderResultsPage.ffBudgetAmountList(5).click();
                    break;
                default:
                    fareFinderResultsPage.ffBudgetDropDownCustomValue().sendKeys(budgetAmount);
                    fareFinderResultsPage.ffBudgetDropDownOkBtn().click();
            }
        }

    }

    this.selectSpecificDate = function (days) {
        fareFinderResultsPage.flyOutDateBtn().click();
        fareFinderResultsPage.specificDateBtn().click();
        fareFinderResultsPage.specificDateSelectionCalendar(days).click();
    }

    this.selectSpecificDateRangeTomorrowToLastDay = function () {
        fareFinderResultsPage.flyOutDateBtn().click();
        fareFinderResultsPage.dateRangeBtn().click();
        fareFinderResultsPage.dateRangeSelectionCalendarTomorrow().click();
        fareFinderResultsPage.dateRange2SelectionCalendar().click();
    }


    this.selectTripLength = function (tripLength) {
        fareFinderResultsPage.flyBackBtn().click();

        switch (tripLength) {
            case ONE_WAY:
                browser.driver.actions().mouseDown(fareFinderResultsPage.flyBackOneWayBtn()).mouseUp().perform();
                break;

            case ONE_THREE:
                browser.driver.actions().mouseDown(fareFinderResultsPage.flyBackTripLengthMenu()).mouseUp().perform();
                fareFinderResultsPage.tripLength1to3DaysSelection().click();
                break;

            case FOUR_SEVEN:
                browser.driver.actions().mouseDown(fareFinderResultsPage.flyBackTripLengthMenu()).mouseUp().perform();
                fareFinderResultsPage.tripLength4to7DaysSelection().click();
                break;

            case EIGHT_ELEVEN:
                browser.driver.actions().mouseDown(fareFinderResultsPage.flyBackTripLengthMenu()).mouseUp().perform();
                fareFinderResultsPage.tripLength8to11DaysSelection().click();
                break;

            case TWELVE_FOURTEEN:
                browser.driver.actions().mouseDown(fareFinderResultsPage.flyBackTripLengthMenu()).mouseUp().perform();
                fareFinderResultsPage.tripLength12to14DaysSelection().click();
                break;

            case FIFTEEN_TWENTY_ONE:
                browser.driver.actions().mouseDown(fareFinderResultsPage.flyBackTripLengthMenu()).mouseUp().perform();
                fareFinderResultsPage.tripLength15to21DaysSelection().click();
                break;

            case NEXT_THREE_MONTHS:
                browser.driver.actions().mouseDown(fareFinderResultsPage.flyBackNextThreeMonthsMenu()).mouseUp().perform();
                break;

            case ANY_TIME:
                browser.driver.actions().mouseDown(fareFinderResultsPage.tripLengthAnytimeSelection()).mouseUp().perform();
                break;

            case SPECIFIC_DATE:
                browser.driver.actions().mouseDown(fareFinderResultsPage.specificDateBtn()).mouseUp().perform();
                fareFinderResultsPage.specificDateSelectionCalendar(9).click();
                break;

            case CUSTOM_RANGE:
                browser.driver.actions().mouseDown(fareFinderResultsPage.tripLengthAnytimeSelection()).mouseUp().perform();
                var searchFromRange = Math.floor(Math.random() * (20 - 5 + 1)) + 5;
                var searchToRange = Math.floor(Math.random() * (60 - 21 + 1)) + 21;
                fareFinderResultsPage.tripLengthNextCustomFromSelection().sendKeys(searchFromRange);
                fareFinderResultsPage.tripLengthNextCustomToSelection().sendKeys(searchToRange);
                fareFinderResultsPage.tripLengthNextCustomOKBtn().click();
                reporter.addMessageToSpec(sprintf("Custom Search from %s to %s", searchFromRange, searchToRange));
                break;

            default:
                fareFinderResultsPage.flyBackOneWayBtn().click();
        }

    }

    this.searchFareFinderFlightWithTripTime = function (from, to, budgetAmount, flyOut, tripLength) {
        fareFinderResultsActions.searchFareFinderFlightWithoutTripTime(from, to, budgetAmount);

        fareFinderResultsPage.flyOutDateBtn().click();

        switch (flyOut) {
            case NEXT_THREE_MONTHS:
                fareFinderResultsPage.nextThreeMonthsBtn().click();
                break;
            case SPECIFIC_DATE:
                fareFinderResultsPage.specificDateBtn().click();
                fareFinderResultsPage.specificDateSelectionCalendar(2).click();
                break;
            case ENTIRE_MONTH:
                fareFinderResultsPage.entireMonthBtn().click();
                var randButtonIndex = (Math.floor(Math.random() * (6)) + 1);
                fareFinderResultsPage.entireMonthSelection(randButtonIndex).click();
                break;
            case DATE_RANGE:
                fareFinderResultsPage.dateRangeBtn().click();
                fareFinderResultsPage.dateRangeSelectionCalendarToday().click();
                fareFinderResultsPage.dateRange2SelectionCalendar().click();
                break;
            case ANY_TIME:
                fareFinderResultsPage.flyOutAnyTimeBtn().click();
                break;
            default:
                fareFinderResultsPage.flyOutAnyTimeBtn().click();
        }

        fareFinderResultsPage.flyBackBtn().click();

        switch (tripLength) {
            case ONE_WAY:
                browser.driver.actions().mouseDown(fareFinderResultsPage.flyBackOneWayBtn()).mouseUp().perform();
                break;

            case ONE_THREE:
                browser.driver.actions().mouseDown(fareFinderResultsPage.flyBackTripLengthMenu()).mouseUp().perform();
                fareFinderResultsPage.tripLength1to3DaysSelection().click();
                break;

            case FOUR_SEVEN:
                browser.driver.actions().mouseDown(fareFinderResultsPage.flyBackTripLengthMenu()).mouseUp().perform();
                fareFinderResultsPage.tripLength4to7DaysSelection().click();
                break;

            case EIGHT_ELEVEN:
                browser.driver.actions().mouseDown(fareFinderResultsPage.flyBackTripLengthMenu()).mouseUp().perform();
                fareFinderResultsPage.tripLength8to11DaysSelection().click();
                break;

            case TWELVE_FOURTEEN:
                browser.driver.actions().mouseDown(fareFinderResultsPage.flyBackTripLengthMenu()).mouseUp().perform();
                fareFinderResultsPage.tripLength12to14DaysSelection().click();
                break;

            case FIFTEEN_TWENTY_ONE:
                browser.driver.actions().mouseDown(fareFinderResultsPage.flyBackTripLengthMenu()).mouseUp().perform();
                fareFinderResultsPage.tripLength15to21DaysSelection().click();
                break;

            case NEXT_THREE_MONTHS:
                browser.driver.actions().mouseDown(fareFinderResultsPage.flyBackNextThreeMonthsMenu()).mouseUp().perform();
                break;

            case ANY_TIME:
                browser.driver.actions().mouseDown(fareFinderResultsPage.tripLengthAnytimeSelection()).mouseUp().perform();
                break;

            case SPECIFIC_DATE:
                browser.driver.actions().mouseDown(fareFinderResultsPage.specificDateBtn()).mouseUp().perform();
                fareFinderResultsPage.specificDateSelectionCalendar(9).click();
                break;

            case CUSTOM_RANGE:
                browser.driver.actions().mouseDown(fareFinderResultsPage.tripLengthAnytimeSelection()).mouseUp().perform();
                var searchFromRange = Math.floor(Math.random() * (20 - 5 + 1)) + 5;
                var searchToRange = Math.floor(Math.random() * (60 - 21 + 1)) + 21;
                fareFinderResultsPage.tripLengthNextCustomFromSelection().sendKeys(searchFromRange);
                fareFinderResultsPage.tripLengthNextCustomToSelection().sendKeys(searchToRange);
                fareFinderResultsPage.tripLengthNextCustomOKBtn().click();
                reporter.addMessageToSpec(sprintf("Custom Search from %s to %s", searchFromRange, searchToRange));
                break;

            default:
                fareFinderResultsPage.flyBackOneWayBtn().click();
        }

    }

    this.fillFRSearchField = function (element, text) {
        element.clear();
        element.sendKeys(text);
        element.sendKeys(protractor.Key.ENTER);

    }

    this.setSearchFilter = function (filter) {
        var filterElement;
        switch (filter) {
            case 1:
                filterElement = fareFinderResultsPage.tripTypesFilters(1).click();
                break;

            case 2:
                filterElement = fareFinderResultsPage.tripTypesFilters(2).click();
                break;

            case 3:
                filterElement = fareFinderResultsPage.tripTypesFilters(3).click();
                break;

            case 4:
                filterElement = fareFinderResultsPage.tripTypesFilters(4).click();
                break;

            case 5:
                filterElement = fareFinderResultsPage.tripTypesFilters(5).click();
                break;

            case 6:
                filterElement = fareFinderResultsPage.tripTypesFilters(6).click();
                break;

            default:
                break;

        }
        reporter.addMessageToSpec(sprintf("Set Trip Type Filter %s", filter));
    }

    this.clickGreatDealLink = function () {
        fareFinderResultsPage.resultGreatDealLink().click();
    }

    this.clickFlightResultByIndex = function (index) {
        fareFinderResultsPage.flightResult(index).click();
    }

    this.clickFlightResultByIndexThenVerifyResultDetails = function (index) {
        var toInResult = "";
        fareFinderResultsPage.flightResultTo(index).getText().then(function (airport) {
            reporter.addMessageToSpec("fareFinderResultsPage.flightResultTo(index)::::::" + airport);
            toInResult = airport;

        });
        fareFinderResultsPage.flightResult(index).click();
        fareFinderResultsDetailsPage.flightTitleTo().getText().then(function (flightTo) {
            expect(flightTo).toBe(toInResult);
        });
    }

    this.clickFlightResultVerifyRecentView = function (index, from) {
        var toInResult = "";
        fareFinderResultsPage.flightResultTo(index).getText().then(function (airport) {
            reporter.addMessageToSpec("fareFinderResultsPage.flightResultTo(index)::::::" + airport);
            toInResult = airport;

        });
        fareFinderResultsPage.flightResult(index).click();
        actions.fareFinderResultsDetailsActions.clickBackToFareFinder();
        fareFinderResultsPage.recentlyViewedBtn().click();
        expect(fareFinderResultsPage.recentlyViewedResults(from, toInResult).isDisplayed()).toBeTruthy();

    }


    this.clickFlightResultByIndexThenReturnToPrevious = function (index) {
        var fromInResult = "";
        var fromInResultReturn = "";
        var firstNumTextBefore = 0;
        var firstNumTextAfter = 0;
        fareFinderResultsPage.yellowDots().count().then(function (numberValue) {
            reporter.addMessageToSpec("numberValue:" + numberValue);
            firstNumTextBefore = parseInt(numberValue);
            fareFinderResultsPage.flightResultTo(index).getText().then(function (airport) {
                reporter.addMessageToSpec("fareFinderResultsPage.flightResultTo(index)::::::" + airport);
                fromInResult = airport;
            });
            fareFinderResultsPage.flightResult(index).click();
            fareFinderResultsDetailsPage.flightTitleTo().getText().then(function (flightTo) {
                expect(flightTo).toBe(fromInResult);
            });
            fareFinderResultsDetailsPage.backToFFLink().click();
            fareFinderResultsPage.flightResultTo(index).getText().then(function (airport) {
                reporter.addMessageToSpec("fareFinderResultsPage.flightResultTo(index)2::::::" + airport);
                fromInResultReturn = airport;
                expect(fromInResult).toBe(fromInResultReturn);
            });
            fareFinderResultsPage.yellowDots().count().then(function (numberValue2) {
                reporter.addMessageToSpec("numberValue2:" + numberValue2);
                firstNumTextAfter = parseInt(numberValue2);
                expect(firstNumTextAfter).toBe(firstNumTextBefore);
            });
        });


    }

    this.clickFlightResultByIndexThenReturnToPreviousListView = function (index) {
        var numResultsBefore = 0;
        var numResultsAfter = 0;
        fareFinderResultsPage.ffFaresListResults().count().then(function (numberValue) {
            numResultsBefore = parseInt(numberValue);
            actions.fareFinderResultsActions.clickFlightResultByIndex(index);
            actions.fareFinderResultsDetailsActions.clickBackToFareFinder();
            fareFinderResultsPage.ffFaresListResults().count().then(function (numberValue2) {
                numResultsAfter = parseInt(numberValue2);
                expect(numResultsBefore).toBe(numResultsAfter);
            });
        });


    }


    this.selectView = function (viewName) {
        var viewNameElement;
        switch (viewName) {
            case LIST:
                viewNameElement = fareFinderResultsPage.listViewBtn();
                break;
            case TILE:
                viewNameElement = fareFinderResultsPage.tileViewBtn();
                break;
            case MAP:
                viewNameElement = fareFinderResultsPage.mapViewBtn();
                break;
        }
        ;
        viewNameElement.click();

    }
    this.verifyMapViewIsSelected = function () {
        expect(fareFinderResultsPage.activeMapViewTab().isDisplayed()).toBeTruthy();

    }


    this.clickRecentlyViewed = function () {
        fareFinderResultsPage.recentlyViewedBtn().click();
    }

    this.clickTopResult = function () {
        fareFinderResultsPage.ffFaresListResults().get(0).click();
    }

    this.verifyDisplayForInvalid = function () {
        expect(fareFinderResultsPage.messageForInvalid().isDisplayed()).toBeTruthy()

    }

    this.selectMapViewVerifyTheSelectionDefault = function () {
        this.selectView("Map");
        fareFinderResultsPage.ffSearchFromField().getAttribute('value').then(function (actualValue) {
            fareFinderResultsPage.firstSelectedAirportOnMap().getText().then(function (airport) {
                expect(actualValue).toBe(airport);
                reporter.addMessageToSpec(actualValue);
                reporter.addMessageToSpec(airport);

            });
        });
    };


    this.verifyTheSelection = function () {
        fareFinderResultsPage.ffSearchFromField().getAttribute('value').then(function (actualValue) {
            fareFinderResultsPage.selectedAirportsOnMap().get(0).getText().then(function (airport) {
                expect(actualValue).toBe(airport);
            });
        });
        fareFinderResultsPage.ffSearchToField().getAttribute('value').then(function (actualValue1) {
            fareFinderResultsPage.selectedAirportsOnMap().get(1).getText().then(function (airport1) {
                expect(actualValue1).toBe(airport1);
            });
        });
    };


    this.verifySelectionThenBookFromMap = function () {
        fareFinderResultsPage.selectedAirportsOnMap().get(0).getText().then(function (from) {
            fareFinderResultsPage.selectedAirportsOnMap().get(1).getText().then(function (to) {
                fareFinderResultsDetailsPage.mapViewToolTipBookButton().click();
                fareFinderResultsDetailsPage.resultHeaderOutbound().getText().then(function (titleText) {
                    expect(titleText).toContain(to);
                    expect(titleText).toContain(from);
                });
                fareFinderResultsDetailsPage.flightTitleTo().getText().then(function (flightTo) {
                    expect(flightTo).toBe(to);
                });
                fareFinderResultsDetailsPage.flightTitleFrom().getText().then(function (flightFrom) {
                    expect(flightFrom).toBe(from);
                });
                fareFinderResultsDetailsPage.tripSummeryPassengerInfo().getText().then(function (passengerInfo) {
                    expect(passengerInfo).toBe("1 Adult");
                });
            });
        });
    }

    this.clickAnyDestinationOnMap = function (toCode) {
        fareFinderResultsPage.unselectedAiportOnMap(toCode).click();

    };


    this.changeToSmallerBudgetAndVerifyTheChangeOfResults = function (budgetAmount) {
        fareFinderResultsPage.yellowDots().count().then(function (numberValue) {
            if (budgetAmount !== BUDGET150EURO) {
                fareFinderResultsPage.ffSearchBudgetField().click();
                switch (budgetAmount) {
                    case BUDGET20EURO:
                        fareFinderResultsDetailsPage.ffBudgetAmountList(0).click();
                        break;
                    case BUDGET50EURO:
                        fareFinderResultsPage.ffBudgetAmountList(1).click();
                        break;
                    case BUDGET60EURO:
                        fareFinderResultsPage.ffBudgetAmountList(2).click();
                        break;
                    case BUDGET80EURO:
                        fareFinderResultsPage.ffBudgetAmountList(3).click();
                        break;
                    case BUDGET100EURO:
                        fareFinderResultsPage.ffBudgetAmountList(4).click();
                        break;
                    case BUDGET150EURO:
                        fareFinderResultsPage.ffBudgetAmountList(5).click();
                        break;
                    default:
                        fareFinderResultsPage.ffBudgetDropDownCustomValue().sendKeys(budgetAmount);
                        fareFinderResultsPage.ffBudgetDropDownOkBtn().click();
                }
            }
            browser.sleep(6000);
            fareFinderResultsPage.yellowDots().count().then(function (numberValue2) {
                expect(numberValue2).toBeLessThan(numberValue);
            });
        });

    };


    this.changeDepartureDateVerifyTheChangeOfResults = function (flyOut) {
        var firstNumTextBefore = 0;
        var firstNumTextAfter = 0;
        fareFinderResultsPage.yellowDots().count().then(function (numberValue) {
            reporter.addMessageToSpec("numberValue:" + numberValue);
            firstNumTextBefore = parseInt(numberValue);

        });
        fareFinderResultsPage.flyOutDateBtn().click();

        var flyOutElement;
        switch (flyOut) {
            case NEXT_THREE_MONTHS:
                fareFinderResultsPage.nextThreeMonthsBtn().click();
                break;
            case SPECIFIC_DATE:
                fareFinderResultsPage.specificDateBtn().click();
                fareFinderResultsPage.specificDateSelectionCalendar(2).click();
                break;
            case ENTIRE_MONTH:
                fareFinderResultsPage.entireMonthBtn().click();
                var randButtonIndex = (Math.floor(Math.random() * (6)) + 1);
                fareFinderResultsPage.entireMonthSelection(randButtonIndex).click();
                break;
            case DATE_RANGE:
                fareFinderResultsPage.dateRangeBtn().click();
                fareFinderResultsPage.dateRangeSelectionCalendarToday().click();
                fareFinderResultsPage.dateRange2SelectionCalendar().click();
                break;
            case ANY_TIME:

                fareFinderResultsPage.flyOutAnyTimeBtn().click();
                break;
            default:
                fareFinderResultsPage.flyOutAnyTimeBtn().click();


        }
        browser.sleep(6000);
        fareFinderResultsPage.yellowDots().count().then(function (numberValue2) {
            reporter.addMessageToSpec("numberValue2:" + numberValue2);
            firstNumTextAfter = parseInt(numberValue2);
            expect(firstNumTextAfter !== firstNumTextBefore).toBeTruthy();
        });
    };

    this.changeFlyOutFlyBackVerifyTheChangeOfResults = function (flyOut, tripLength) {
        var firstNumTextBefore = 0;
        var firstNumTextAfter = 0;
        fareFinderResultsPage.yellowDots().count().then(function (numberValue) {
            reporter.addMessageToSpec("numberValue:" + numberValue);
            firstNumTextBefore = parseInt(numberValue);

        });
        fareFinderResultsPage.flyOutDateBtn().click();
        switch (flyOut) {
            case NEXT_THREE_MONTHS:
                fareFinderResultsPage.nextThreeMonthsBtn().click();
                break;
            case SPECIFIC_DATE:
                fareFinderResultsPage.specificDateBtn().click();
                fareFinderResultsPage.specificDateSelectionCalendar(6).click();
                break;
            case ENTIRE_MONTH:
                fareFinderResultsPage.entireMonthBtn().click();
                var randButtonIndex = (Math.floor(Math.random() * (6)) + 1);
                fareFinderResultsPage.entireMonthSelection(randButtonIndex).click();
                break;
            case DATE_RANGE:
                fareFinderResultsPage.dateRangeBtn().click();
                fareFinderResultsPage.dateRangeSelectionCalendarToday().click();
                fareFinderResultsPage.dateRange2SelectionCalendar().click();
                break;
            case ANY_TIME:

                fareFinderResultsPage.flyOutAnyTimeBtn().click();
                break;
            default:
                fareFinderResultsPage.flyOutAnyTimeBtn().click();
        }


        fareFinderResultsPage.flyBackBtn().click();
        var tripLengthElement;
        switch (tripLength) {
            case ONE_WAY:
                tripLengthElement = fareFinderResultsPage.flyBackOneWayBtn();
                break;
            case NEXT_THREE_MONTHS:
                tripLengthElement = fareFinderResultsPage.tripLengthNextThreeMonthsSelection();
                break;
            case ANY_TIME:
                tripLengthElement = fareFinderResultsPage.tripLengthAnytimeSelection();
                break;
            case DATE_RANGE:
                tripLengthElement = fareFinderResultsPage.dateRangeBtn();
                break;
            case EIGHT_ELEVEN:
                browser.driver.actions().mouseDown(fareFinderResultsPage.flyBackTripLengthMenu()).mouseUp().perform();
                tripLengthElement = fareFinderResultsPage.tripLength8to11DaysSelection();
                break;

        }
        tripLengthElement.click();
        fareFinderResultsPage.yellowDots().count().then(function (numberValue2) {
            reporter.addMessageToSpec("numberValue2:" + numberValue2);
            firstNumTextAfter = parseInt(numberValue2);
            expect(firstNumTextAfter !== firstNumTextBefore).toBeTruthy();
        });
    };

    this.changeFlyOutToLastMonthThenAnyMonth = function (flyOut) {
        var firstNumTextBefore = 0;
        var firstNumTextAfter = 0;
        fareFinderResultsPage.yellowDots().count().then(function (numberValue) {
            reporter.addMessageToSpec("numberValue:" + numberValue);
            firstNumTextBefore = parseInt(numberValue);

        });
        fareFinderResultsPage.flyOutDateBtn().click();
        switch (flyOut) {
            case NEXT_THREE_MONTHS:
                fareFinderResultsPage.nextThreeMonthsBtn().click();
                break;
            case SPECIFIC_DATE:
                fareFinderResultsPage.specificDateBtn().click();
                fareFinderResultsPage.specificDateSelectionCalendar(6).click();
                break;
            case ENTIRE_MONTH:
                fareFinderResultsPage.entireMonthBtn().click();
                fareFinderResultsPage.lastMonthBtn().click();
                break;
            case DATE_RANGE:
                fareFinderResultsPage.dateRangeBtn().click();
                fareFinderResultsPage.dateRangeSelectionCalendarToday().click();
                fareFinderResultsPage.dateRange2SelectionCalendar().click();
                break;
            case ANY_TIME:

                fareFinderResultsPage.flyOutAnyTimeBtn().click();
                break;
            default:
                fareFinderResultsPage.flyOutAnyTimeBtn().click();
        }


        fareFinderResultsPage.flyOutDateBtn().click();
        switch (flyOut) {
            case NEXT_THREE_MONTHS:
                fareFinderResultsPage.nextThreeMonthsBtn().click();
                break;
            case SPECIFIC_DATE:
                fareFinderResultsPage.specificDateBtn().click();
                fareFinderResultsPage.specificDateSelectionCalendar(6).click();
                break;
            case ENTIRE_MONTH:
                fareFinderResultsPage.entireMonthBtn().click();
                var randButtonIndex = (Math.floor(Math.random() * (6)) + 1);
                fareFinderResultsPage.entireMonthSelection(randButtonIndex).click();

                break;
            case DATE_RANGE:
                fareFinderResultsPage.dateRangeBtn().click();
                fareFinderResultsPage.dateRangeSelectionCalendarToday().click();
                fareFinderResultsPage.dateRange2SelectionCalendar().click();
                break;
            case ANY_TIME:

                fareFinderResultsPage.flyOutAnyTimeBtn().click();
                break;
            default:
                fareFinderResultsPage.flyOutAnyTimeBtn().click();
        }

        fareFinderResultsPage.yellowDots().count().then(function (numberValue2) {
            reporter.addMessageToSpec("numberValue2:" + numberValue2);
            firstNumTextAfter = parseInt(numberValue2);
            expect(firstNumTextAfter !== firstNumTextBefore).toBeTruthy();
        });


    };

    this.changeRegionVerifyTheChangeOfResults = function (to) {
        var firstNumTextBefore = 0;
        var firstNumTextAfter = 0;
        fareFinderResultsPage.yellowDots().count().then(function (numberValue) {
            reporter.addMessageToSpec("numberValue:" + numberValue);
            firstNumTextBefore = parseInt(numberValue);
            fareFinderResultsPage.clickToFieldDiv().click();
            fareFinderResultsPage.regionName().click();
            fareFinderResultsPage.airportName(to).click();
            fareFinderResultsPage.moreRoutes().click();
            fareFinderResultsPage.yellowDots().count().then(function (numberValue2) {
                reporter.addMessageToSpec("numberValue2:" + numberValue2);
                firstNumTextAfter = parseInt(numberValue2);
                expect(firstNumTextAfter).toBeLessThan(firstNumTextBefore);
            });
        });
    };


    this.changeTypeVerifyTheChangeOfResults = function (filter) {
        var firstNumTextBefore = 0;
        var firstNumTextAfter = 0;
        fareFinderResultsPage.yellowDots().count().then(function (numberValue) {
            reporter.addMessageToSpec("numberValue:" + numberValue);
            firstNumTextBefore = parseInt(numberValue);
            var filterElement;
            switch (filter) {
                case BREACH:
                    filterElement = fareFinderResultsPage.tripTypesFilters(1);
                    break;

                case CITY_BREAK:
                    filterElement = fareFinderResultsPage.tripTypesFilters(2);
                    break;

                case FAMILY:
                    filterElement = fareFinderResultsPage.tripTypesFilters(3);
                    break;

                case NIGHTLIFE:
                    filterElement = fareFinderResultsPage.tripTypesFilters(4);
                    break;

                case OUTDOOR:
                    filterElement = fareFinderResultsPage.tripTypesFilters(5);
                    break;

                case GOLF:
                    filterElement = fareFinderResultsPage.tripTypesFilters(6);
                    break;

                default:
                    reporter.addMessageToSpec("Unknown filter : " + filter + " Didnt select any filter")
                    break;

            }
            filterElement.click();
            reporter.addMessageToSpec(sprintf("Set Trip Type Filter %s", filter));
            browser.sleep(6000);
            fareFinderResultsPage.yellowDots().count().then(function (numberValue2) {
                reporter.addMessageToSpec("numberValue2:" + numberValue2);
                firstNumTextAfter = parseInt(numberValue2);
                expect(firstNumTextAfter).toBeLessThan(firstNumTextBefore);
            });
        });
    };


    this.changeToCountryByMouseClickVerifyTheChangeInMapView = function (to) {
        var beofreChangeToAirport = 0;
        var afterChangeToAirport = 0;
        fareFinderResultsPage.yellowDots().count().then(function (numberValue) {
            reporter.addMessageToSpec("befrreChangeToAirport" + numberValue);
            beofreChangeToAirport = numberValue;
            fareFinderResultsPage.clickToFieldDiv().click();
            fareFinderResultsPage.airportName(to).click();
            fareFinderResultsPage.zoomOut().click();
            fareFinderResultsPage.ffSearchToField().getAttribute('value').then(function (actualValue1) {
                fareFinderResultsPage.selectedAirportsOnMap().get(1).getText().then(function (to) {
                    browser.sleep(6000);
                    expect(actualValue1).toBe(to);
                });
            });
            fareFinderResultsPage.yellowDots().count().then(function (numberValue2) {
                reporter.addMessageToSpec("numberValue2:" + numberValue2);
                afterChangeToAirport = parseInt(numberValue2);
                reporter.addMessageToSpec("afterChangeToAirport" + afterChangeToAirport);
                expect(beofreChangeToAirport).toBeGreaterThan(afterChangeToAirport);
            });
        });

    };

    this.changeToCountryByManualTypeInVerifyChangeInMapView = function (to) {
        var beforeChangeToAirport = 0;
        var afterChangeToAirport = 0;
        fareFinderResultsPage.yellowDots().count().then(function (numberValue) {
            reporter.addMessageToSpec("befrreChangeToAirport" + numberValue);
            beforeChangeToAirport = numberValue;
            fareFinderResultsPage.clickToFieldDiv().click();
            fareFinderResultsPage.ffSearchToField().clear();
            fareFinderResultsPage.ffSearchToField().sendKeys(to);
            fareFinderResultsPage.ffSearchToField().sendKeys(protractor.Key.ENTER);
            fareFinderResultsPage.zoomOut().click();
            fareFinderResultsPage.ffSearchToField().getAttribute('value').then(function (actualValue1) {
                fareFinderResultsPage.selectedAirportsOnMap().get(1).getText().then(function (to) {
                    browser.sleep(6000);
                    expect(actualValue1).toBe(to);
                });
            });
            fareFinderResultsPage.yellowDots().count().then(function (numberValue2) {
                reporter.addMessageToSpec("numberValue2:" + numberValue2);
                afterChangeToAirport = parseInt(numberValue2);
                reporter.addMessageToSpec("afterChangeToAirport" + afterChangeToAirport);
                expect(beforeChangeToAirport).toBeGreaterThan(afterChangeToAirport);
            });
        });

    };


    this.changeToCountryByMouseClickVerifyTheChangeInListView = function (to) {
        var beforeChangeToAirport = 0;
        var afterChangeToAirport = 0;
        fareFinderResultsPage.ffFaresListResults().count().then(function (numberValue) {
            reporter.addMessageToSpec("befrreChangeToAirport" + numberValue);
            beforeChangeToAirport = numberValue;
            fareFinderResultsPage.clickToFieldDiv().click();
            fareFinderResultsPage.clickToFieldDiv().click();
            fareFinderResultsPage.airportName(to).click();
            fareFinderResultsPage.ffFaresListResults().count().then(function (numberValue2) {
                afterChangeToAirport = parseInt(numberValue2);
                expect(beforeChangeToAirport).toBeGreaterThan(afterChangeToAirport);
            });
        });

    };

    this.bookFlightFromMapViewVerifyInFfResultsDetailsPage = function (from, to) {
        fareFinderResultsDetailsPage.mapViewToolTipBookButton().click();
        fareFinderResultsDetailsPage.resultHeaderOutbound().getText().then(function (titleText) {
            expect(titleText).toContain(to);
            expect(titleText).toContain(from);
        });

        fareFinderResultsDetailsPage.flightTitleTo().getText().then(function (flightTo) {
            expect(flightTo).toBe(to);
        });

        fareFinderResultsDetailsPage.flightTitleFrom().getText().then(function (flightFrom) {
            expect(flightFrom).toBe(from);
        });

        fareFinderResultsDetailsPage.tripSummeryPassengerInfo().getText().then(function (passengerInfo) {
            expect(passengerInfo).toBe("1 Adult");
        });

    }

}

var checkPriceIsLessThen = function (actualFarePrice, budget) {
    var roundedPrice = parseInt(actualFarePrice); // remove currency symbol
    var searchPriceInt = parseInt(budget);
    expect(roundedPrice).toBeLessThan(searchPriceInt);

}


module.exports = FareFinderResultsActions;
var Actions = require('../Actions.js'),
    UserActions = actions.userActions;

/*
* @example
* */
describe('Example of usage userActions', function() {

     //done is needed to say jasmine that tests will be asynchronous
    it('user registration', function(done) {
        UserActions.signUp().then(function(user){
            console.log("User registered");
            console.log(user);// operations with user
            expect(true).toBe(true); // assertions
            done(); // say jasmine that we finished
        });
    });

    // done is needed to say jasmine that tests will be asynchronous
    it('registration and activation', function(done) {
        UserActions.signUp().then(function(user){
            console.log("User registered");
            console.log(user);// operations with user
            UserActions.activate(user).then(function(authInfo){
                console.log("User activated, and logged in");
                console.log(authInfo);// operations with logged in user
                expect(true).toBe(true); // assertions
                done(); // say jasmine that we finished
            });
        });
    });

    // done is needed to say jasmine that tests will be asynchronous
    it('registration, activation and login', function(done) {
        UserActions.signUp().then(function(user){
            console.log("User registered");
            console.log(user);// operations with user
            UserActions.activate(user).then(function(authInfo){
                console.log("User activated");
                console.log(authInfo);// operations with logged in user
                UserActions.login(user).then(function(authInfo){
                    console.log("User logged in");
                    console.log(authInfo);// operations with logged in user
                    expect(true).toBe(true); // assertions
                    done(); // say jasmine that we finished
                });
            });
        });
    });

});
var Actions = require('../Actions')
var actions = new Actions();
var Trip = require('../shared/model/Trip');

describe('Simple Booking - C28590 - Today - Return - Standard - ST SEATS', function () {

    var outBoundDaysFromNow = 2; //today index === 1
    var origin = "stn";
    var destination = "dub";
    var fareType = "standard";
    var tripWay = "oneway";
    var paxListMy;
    var cardMy;

    var bookFlight = function (paxMap) {
        var trip = new Trip(paxMap, origin, destination, outBoundDaysFromNow);
        actions.fOHActions.searchOneWayFLightWithPax(paxMap, origin, destination, outBoundDaysFromNow);
        actions.tripsHomeActions.selectAFlight(0, 0, fareType, tripWay);
        paxListMy = trip.journey.paxList;
        cardMy = trip.bookingContact.card;
    }

    describe('1 adult, 0 teen, 0 children, 0 infants', function () {

        it('Given I make a standard return trip with outbound 0 and return 0 days from now with 1 adult, 0 teen, 0 children, 0 infants', function () {
            // TODO MAKE BOOKING TO SEATS
            var paxMap = {ADT: 1, CHD: 0, INF: 0, TEEN: 0};
            bookFlight(paxMap);
        });

        it('Then I should add a seat', function () {
            // TODO expect booking ref
            actions.extrasActions.addSeat();
            actions.seatsActions.selectOneSeat();
            actions.extrasActions.skipExtras();
        });

        it('Then I should pay for booking', function () {
            // TODO expect booking ref
            actions.addPaxActions.addPaxNameForAllPAX(paxListMy);
            actions.addPaxActions.clickBtnAddPaxSave();
            actions.addPaxActions.addContact();
            actions.addPaxActions.makeCardPayment(cardMy);
        });


        it('Then I should get a booking ref', function () {
            // TODO expect booking ref
            actions.bookingSummaryActions.verifyConfirmationMessage();
        });

        //it('When I do checkIN', function () {
        //    // TODO Make a Simple Check IN
        //    actions.checkInActions.todoSimpleCeckIn();
        //});
        //
        //it('Then I verify check IN', function () {
        //    // TODO expect check IN
        //    actions.checkInActions.verifySimpleCheckIn();
        //});
    });

    //describe('5 adult, 0 teen, 0 children, 0 infants', function () {
    //
    //    it('Given I make a standard return trip with outbound 0 and return 0 days from now with 1 adult, 0 teen, 0 children, 0 infants', function () {
    //        var paxMap = {ADT: 5, CHD: 0, INF: 0, TEEN: 0}
    //        bookFlight(paxMap);
    //    });
    //
    //    it('Then I should get a booking ref', function () {
    //        // TODO expect booking ref
    //        actions.bookingSummaryActions.verifyConfirmationMessage();
    //    });
    //
    //    it('When I do checkIN', function () {
    //        // TODO Make a Simple Check IN
    //        actions.checkInActions.todoSimpleCeckIn();
    //    });
    //
    //    it('Then I verify check IN', function () {
    //        // TODO expect check IN
    //        actions.checkInActions.verifySimpleCheckIn();
    //    });
    //});
    //
    //describe('2 adult, 0 teen, 0 children, 2 infants', function () {
    //
    //    it('Given I make a standard return trip with outbound 0 and return 0 days from now with 2 adult, 0 teen, 0 children, 2 infants', function () {
    //        var paxMap = {ADT: 2, CHD: 0, INF: 0, TEEN: 2}
    //        bookFlight(paxMap);
    //    });
    //
    //    it('Then I should get a booking ref', function () {
    //        // TODO expect booking ref
    //        actions.bookingSummaryActions.verifyConfirmationMessage();
    //    });
    //
    //    it('When I do checkIN', function () {
    //        // TODO Make a Simple Check IN
    //        actions.checkInActions.todoSimpleCeckIn();
    //    });
    //
    //    it('Then I verify check IN', function () {
    //        // TODO expect check IN
    //        actions.checkInActions.verifySimpleCheckIn();
    //    });
    //});
});




